<?php
require_once "./src/Model/Assets.php";
require_once "./src/Model/Cliente.php";
require_once './src/Model/Category.php';
require_once './src/Model/Account.php';
require_once './src/Model/Empresas.php';
require_once './src/Model/Orcamento.php';
require_once "./src/Controller/CommonController.php";
require_once './src/Model/Usuario.php';
class AssetsController extends CommonController
{
    private $modulos = [];
    private $estados = [];
    private $categoriasContasReceber = array();
    private $contas = [];
    private $clientes = [];
    private $empresas = [];
    private $classe = "Assets";
    private $breadcrumb = [];
    private $titulo_principal = "";
    public $objCommon;
    private $objOrcamento;
    private $modelo_empresa = "";
    private $usuarios = array();
    private $permissoes;
    private $ambiente;
    public function __construct()
    {
        $this->objCommon = new CommonController();
        $objCategoriasFinanceiro = new Category();
        $objClientes = new Cliente();
        $objContas = new Account();
        $objEmpresa = new Empresas();
        $objUsuario = new Usuario();
        $this->objOrcamento = new Orcamento();
        $modulos = $this->objCommon->getModulos();
        $this->modulos = $modulos;

        $configuracoes = new Configuracoes();
        $this->modelo_empresa = $configuracoes->listarConfiguracao('modelo_empresa')->valor;

        $objGruposPermissoes = new GruposPermissoes();
        $handle_grupo = isset($_SESSION['handle_grupo']) ? $_SESSION['handle_grupo'] : 0;
        $this->permissoes = $objGruposPermissoes->listarPorGrupo($handle_grupo, 'Assets')[0];        

        $this->categoriasContasReceber = $objCategoriasFinanceiro->listarPorTipo(['tipo' => 0]);
        $this->clientes = $objClientes->listarTodos($pagina_atual = 0,$linha_inicial = 0,$coluna = '',$buscar = '', $quantidade = '10000', 'RazaoSocial ASC');
        $this->empresas = $objEmpresa->listarTodos();
        $this->contas = $objContas->listarTodos();

        $this->usuarios = $objUsuario->listarTodos(0,0, 'usuario_master', '0');

        $this->ambiente = $this->consultaAmbiente();

        $modulo_posicao = array_search(
            $this->classe,
            array_column($modulos, "modulo")
        );
        $this->titulo_principal = $modulos[$modulo_posicao];
        $this->breadcrumb = [
            "Maestria" => URL . "dashboard/index/",
            $this->titulo_principal["descricao"] =>
                URL . $this->classe . "/listar/",
        ];
    }
    public function listar()
    {
        $this->objCommon = new CommonController();
        // $objAssets = new Assets();

        // $arrParametros = []; // ['filtro' => ['situacao' => 0]];
        // if (!empty($_POST)) {
        //   $arrParametros['filtro'] = array_filter($_POST, 'strlen');
        // }

        $msg_sucesso = '';
        $tipo_mensagem = '';
        if (isset($_SESSION['mensagem']) && !empty($_SESSION['mensagem'])) {
            $msg_sucesso = $_SESSION['mensagem'];
      
            if (isset($_SESSION['tipoMensagem']) && !empty($_SESSION['tipoMensagem'])) {
              $tipo_mensagem = $_SESSION['tipoMensagem'];
            }
      
            unset($_SESSION['mensagem']);
            unset($_SESSION['tipoMensagem']);
        }

        // $arrAssets = $objAssets->listarTodos($arrParametros);
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        $metodo = $acao = "editar";
        require "./src/View/Assets/assets_listar.php";
    }

    public function listarDataTables()
    {
        $this->objCommon = new CommonController();
        $objAssets = new Assets();

        $draw = $this->validateGet('draw') ?: 1;
        $length = $this->validateGet('length') ?: 10;
        $start = $this->validateGet('start') ?: 0;
        $searchValue = isset($_GET['search']['value']) ? $_GET['search']['value'] : '';

        $ordem = 'assets.Date Desc';

        if (isset($_GET['order']) && is_array($_GET['order'])) {
          $direcao = strtoupper($_GET['order'][0]['dir']);
          switch ($_GET['columns'][$_GET['order'][0]['column']]['data']) {
            // case 'Id_Situacao':
            //   $ordem = 'Situacao.descricao ' . $direcao;
            //   break;
    
            default:
              $ordem = $_GET['columns'][$_GET['order'][0]['column']]['data'] . ' ' . $direcao;
          }
        }        

        $arrParametros = [
            'ordem' => $ordem
        ]; // ['filtro' => ['situacao' => 0]];

        $arrFiltrosPossiveis = [
            'CategoryId',
            'Title',
            'UserId',
            'vendedor',
            'situacao',
            'filtroDataLancamentoInicio',
            'filtroDataLancamentoFim',
            'filtroDataVencimentoInicio',
            'filtroDataVencimentoFim',
            'filtroDataPagamentoInicio',
            'filtroDataPagamentoFim',
            'ndoc'
        ];
        foreach ($arrFiltrosPossiveis as $filtro) {
            if ($this->validateGet($filtro) != '') {
                $arrParametros['filtro'][$filtro] = $this->validateGet($filtro);
            }
        }

        if (isset($_SESSION['mensagem']) && !empty($_SESSION['mensagem'])) {
            $msg_sucesso = $_SESSION['mensagem'];
      
            if (isset($_SESSION['tipoMensagem']) && !empty($_SESSION['tipoMensagem'])) {
              $tipo_mensagem = $_SESSION['tipoMensagem'];
            }
      
            unset($_SESSION['mensagem']);
            unset($_SESSION['tipoMensagem']);
        }

        $arrParametros['inicio'] = ($start);
        $arrParametros['quantidade'] = $length;

        $consultaContasReceber = $objAssets->listarTodos($arrParametros);
        $num_registros = $objAssets->listarTodosTotal($arrParametros);
        $arrAssets = [];

        foreach ($consultaContasReceber as $Assets) {
            $emitir_nfse = '';
            if ($this->modelo_empresa == 'gestao-interna') {
                if (!is_null($Assets->id_fila) && $Assets->situacao_nota_servico == 'Autorizado') {
                  $emitir_nfse = '<a class="btn btn-app" style="color:#000;text-decoration:none;" href="' . $Assets->link_pdf.'" target="_blank"><i class="fa fa-money"></i>Baixar NFS-e</a>';
                } else if (!is_null($Assets->id_fila) && $Assets->situacao_nota_servico == 'PROCESSANDO') {
                    $emitir_nfse = '<button class="btn btn-app btnRecarregarConsulta" style="color:#000;text-decoration:none;" data-tipo-documento="2"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Consultar NFS-e novamente</button>';
                } else {
                    $emitir_nfse = '<a class="btn btn-app emitirNotaServicoContaReceber" style="color:#000;text-decoration:none;" data-handle-conta-receber="' . $Assets->AssetsId . '" href="#"><i class="fa fa-money"></i>Emitir NFS-e</a>';
                }
            }

            $arrAssets[]  = [
                'ndoc' => sprintf('<a href="'.URL . 'Orcamento/salvar/%s" target="_blank">%1$s</a>',$Assets->ndoc),
                'Date' => date('d/m/Y', strtotime($Assets->Date)),
                'CategoryName' => $Assets->CategoryName,
                'RazaoSocial' => $Assets->RazaoSocial,
                'Amount' => number_format($Assets->Amount, 2, ',', '.'),
                'nomeConta' => $Assets->nomeConta,
                'Empresa' => (count($this->empresas) > 1 && (count($this->objOrcamento->arrPermissoesUsuarioEmpresa) < 1 || count($this->objOrcamento->arrPermissoesUsuarioEmpresa) == 0)) ? $Assets->Empresa : '',
                'NomeUsuario' => ($this->modelo_empresa != 'gestao-interna') ? $Assets->NomeUsuario : '',
                'Description' => $Assets->Description,
                'cdtvencimento' => $Assets->cdtvencimento != '' && $Assets->cdtvencimento != '0000-00-00' ? date('d/m/Y', strtotime($Assets->cdtvencimento)) : '-',
                'data_pagamento_loja' => ($this->modelo_empresa != 'gestao-interna') && $Assets->data_pagamento_loja != '' ? date('d/m/Y', strtotime($Assets->data_pagamento_loja)) : '-',
                'cdtpagamento' => $Assets->cdtpagamento != '' ? date('d/m/Y', strtotime($Assets->cdtpagamento)) : '-',
                'situacao_conta_receber' => $Assets->situacao_conta_receber == '1' ? 'PAGO' : 'EM ABERTO',
                'editar' => $this->permissoes->editar ? sprintf('<a class="btn btn-app" style="color:#000;text-decoration:none;" href="'.URL . $this->classe . '/editar/' . $Assets->AssetsId .'"><i class="fa fa-edit"></i>Editar</a>') : '',
                'excluir' => $this->permissoes->excluir ? sprintf('<a class="btn btn-app excluirContaReceber" style="color:#000;text-decoration:none;" data-handle-conta-receber="'.$Assets->AssetsId.'" href="#"><i class="fa fa-trash"></i>Excluir</a>') : '',
                'emitir_nfse' => $emitir_nfse
            ];
        }

        $arrRetorno = array(
            'draw' => $draw,
            'recordsTotal' => $num_registros,
            'recordsFiltered' => $num_registros,
            'data' => $arrAssets
        );

        echo json_encode($arrRetorno);
        return;

    }

    public function editar($handle)
    {
        $msg_sucesso = "";
        $metodo = "editar";
        $objAssets = new Assets();
        if (isset($_POST) && !empty($_POST)) {
            $retorno = $objAssets->editar($_POST);
            if ($retorno) {
                $msg_sucesso = "Conta a receber alterada com sucesso.";
            }
        }
        $Assets = $objAssets->listar($handle);
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        require "./src/View/Assets/assets_form.php";
    }
    public function cadastrar()
    {        
        $msg_sucesso = "";
        $moedas = "";
        $metodo = "cadastrar";
        $objAssets = new Assets();
        if (isset($_POST) && !empty($_POST)) {
            $retorno = $objAssets->cadastrar($_POST);
            if ($retorno) {
              $msg_sucesso = "Conta a receber cadastrada com sucesso.";

              if ($_GET['parametros'] == 'origem:orcamento') {
                echo json_encode([
                  'msg' => "Lançamento financeiro cadastrado com sucesso.",
                  'success' => '1'
                ]);
                return;
              }
            }
            $Assets = $objAssets->listar($retorno);
            $metodo = "editar";
        } else {
            $Assets = $objAssets;
        }
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        require "./src/View/Assets/assets_form.php";
    }
    public function excluir($handle)
    {
        $msg_sucesso = "";
        $produtos = "";
        $metodo = "cadastrar";
        $objAssets = new Assets();
        $objAssets->excluir($handle);

        echo json_encode(
            [
            'tipoMensagem' => "callout-success",
            'mensagem' => "Conta a receber excluído com sucesso."
        ]);
    }
}
