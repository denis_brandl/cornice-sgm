<?php
require_once './src/Model/Usuario.php';
require_once './src/Model/Vendedor.php';
require_once './src/Model/UsuarioEmpresa.php';
require_once './src/Model/LogsAcesso.php';
require_once './src/Model/GruposUsuario.php';
require_once './src/Controller/CommonController.php';
require_once './src/Model/Configuracoes.php';
require_once './src/Controller/DashboardController.php';
class UsuarioController extends CommonController
{
  private $modulos = array();
  private $classe = 'Usuario';
  public $NomeUsuario = '';

  private $breadcrumb = array();
  private $titulo_principal = '';
  private $gruposUsuario = array();
  private $objUsuario = array();

  public function __construct()
  {

    $this->objUsuario = new Usuario();
    $common = new CommonController();
    $objGruposUsuario = new GruposUsuario();
    $modulos = $common->getModulos();

    $this->gruposUsuario = $objGruposUsuario->listarTodos();

    $this->modulos = $modulos;

    $modulo_posicao = array_search($this->classe, array_column($this->modulos, 'modulo'));
    
    $this->titulo_principal = $this->modulos[$modulo_posicao];
    $this->breadcrumb = array('Maestria' => URL . 'dashboard/index/', $this->titulo_principal['descricao'] => URL . $this->classe . '/listar/');
  }

  public function listar()
  {
    
    $usuario = new Usuario();

    $coluna = '';
    $buscar = '';
    $pagina_atual = 1;
    if ($this->validateGet('parametros')) {
      $re = "/^[a-z]+=/";
      preg_match($re, $this->validateGet('parametros'), $matches);
      $acao = str_replace('=', '', $matches[0]);

      switch ($acao) {
        case 'buscar':
          $re = "/=([a-zA-Z].*)\|([A-Za-z0-9].*)$/";
          preg_match($re, $this->validateGet('parametros'), $matches);
          if (isset($matches[1])) {
            $coluna = str_replace('=', '', $matches[1]);
          }
          if (isset($matches[2])) {
            $buscar = str_replace('=', '', $matches[2]);
          }
          break;

        case 'pagina':
          $re = "/([a-zA-Z].*)=([A-Za-z0-9].*)$/";
          preg_match($re, $this->validateGet('parametros'), $matches);
          $pagina_atual = str_replace('=', '', $matches[2]);
      }
    }

    $linha_inicial = ($pagina_atual - 1) * QTDE_REGISTROS;

    $num_registros = $usuario->listarTodosTotal();

    /* Idêntifica a primeira página */
    $primeira_pagina = 1;

    /* Cálcula qual será a última página */
    $ultima_pagina  = ceil($num_registros / QTDE_REGISTROS);

    /* Cálcula qual será a página anterior em relação a página atual em exibição */
    $pagina_anterior = ($pagina_atual > 1) ? $pagina_atual - 1 : 0;

    /* Cálcula qual será a pŕoxima página em relação a página atual em exibição */
    $proxima_pagina = ($pagina_atual < $ultima_pagina) ? $pagina_atual + 1 : 0;

    /* Cálcula qual será a página inicial do nosso range */
    $range_inicial  = (($pagina_atual - RANGE_PAGINAS) >= 1) ? $pagina_atual - RANGE_PAGINAS : 1;

    /* Cálcula qual será a página final do nosso range */
    $range_final   = (($pagina_atual + RANGE_PAGINAS) <= $ultima_pagina) ? $pagina_atual + RANGE_PAGINAS : $ultima_pagina;

    /* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */
    $exibir_botao_inicio = ($range_inicial < $pagina_atual) ? 'mostrar' : 'esconder';

    /* Verifica se vai exibir o botão "Anterior" e "Último" */
    $exibir_botao_final = ($range_final > $pagina_atual) ? 'mostrar' : 'esconder';

    $arrCamposBusca = array('nome'  => 'Nome', 'status'    => 'Status',);

    $usuarios = $usuario->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar);
    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;
    $modulos = $this->modulos;
    $classe = $this->classe;
    require './src/View/Usuario/usuario_listar.php';
  }

  public function login($msg = '')
  {
   
   $configuracoes = new Configuracoes();
   $parceria_ruberti = filter_var($configuracoes->listarConfiguracao('parceria_ruberti')->valor, FILTER_VALIDATE_BOOLEAN);
    if (!empty($msg)) {
      $msg_error = $msg;
    }

    require './src/View/Usuario/usuario_login.php';
  }

  public function renovaSessao()
  {

    $ultima_atividade_anterior = 0;
    $sessao_renovada = false;
    $diferenca = 0;
    if (isset($_SESSION["ultima_atividade"])) {
        $ultima_atividade_anterior = $_SESSION["ultima_atividade"];
        $diferenca = time() - $_SESSION["ultima_atividade"];
        if (time() - $_SESSION["ultima_atividade"] > 300) {
          $_SESSION["ultima_atividade"] = time();
          $sessao_renovada = true;
		  session_regenerate_id();
        }
    }
    // $_SESSION['ultima_atividade'] = time();
    echo json_encode(['msg' => 'Sessao Renovada', 'ultima_atividade_anteriror' => date('Y-m-d h:i:s', $ultima_atividade_anterior), 'tempo_nova_sessao' => date('Y-m-d h:i:s', $_SESSION['ultima_atividade']), 'session.gc_maxlifetime' => ini_get('session.gc_maxlifetime'), 'sessao_renovada' => $sessao_renovada, 'diferenca' => $diferenca ]);
  }

  public function logar()
  {
    $usuario = new Usuario();
    $logAcesso = new LogsAcesso();
    $dashboard = new DashboardController();
    if (!empty($_POST)) {
      $usuarios = $usuario->logar($_POST);

      if ($usuarios !== FALSE) {
        $codigo_usuario = $usuarios->CodigoUsuario;
        if (isset($codigo_usuario) && !empty($codigo_usuario) && $codigo_usuario > 0) {
          if (!password_verify($_POST['senha'], $usuarios->SenhaUsuario)) {
            $this->login('Senha inválida!');
            $logAcesso->registraLog(
              [
                'login' => $_POST['usuario'],
                'sucesso' => 2,
                'data' => date('Y-m-d H:i:s'),
                'ip' => $_SERVER['REMOTE_ADDR'],
              ]
            );
          }
          
          /**
           * Valida se o acesso está disponível
           */
           $configuracoes = new Configuracoes();
           $sistema_bloqueado = filter_var($configuracoes->listarConfiguracao('sistema_bloqueado')->valor, FILTER_VALIDATE_BOOLEAN);
           $parceria_ruberti = filter_var($configuracoes->listarConfiguracao('parceria_ruberti')->valor, FILTER_VALIDATE_BOOLEAN);
           $usuario_master = $usuarios->usuario_master;
           
           if ($sistema_bloqueado && !$usuario_master) {
			  $logAcesso->registraLog(
				[
				  'login' => $usuarios->NomeUsuario,
				  'sucesso' => 3,
				  'data' => date('Y-m-d H:i:s'),
				  'ip' => $_SERVER['REMOTE_ADDR'],
				]
			  );
			  
			  require './src/View/Usuario/usuario_acesso_bloqueado.php';
			  exit;
		   }


          $token = md5(uniqid(rand() . $codigo_usuario, true));
          $_SESSION['token'] = $token;
          $_SESSION['ultima_atividade'] = time();
          $_SESSION['SessaoUsuario'] = $token;

          $usuario->registraSessao($codigo_usuario, $token);

          $logAcesso->registraLog(
            [
              'login' => $usuarios->NomeUsuario,
              'sucesso' => 1,
              'data' => date('Y-m-d H:i:s'),
              'ip' => $_SERVER['REMOTE_ADDR'],
            ]
          );

          // $modulos = 'dashboard';
          // $classe = $this->classe;
          // $dashboard->index();
          if ($usuarios->id_grupo_usuario == 4) {
            Header('Location: ' . URL . 'Producao/listar/');
            exit;
          }
          Header('Location: ' . URL . 'dashboard/index/');
          exit;
        } else {
          $this->login('Usuário ou senha inválido!');
          $logAcesso->registraLog(
            [
              'login' => $_POST['usuario'],
              'sucesso' => 2,
              'data' => date('Y-m-d H:i:s'),
              'ip' => $_SERVER['REMOTE_ADDR'],
            ]
          );
        }
      } else {
        $this->login('Usuário ou senha inválido!');
        $logAcesso->registraLog(
          [
            'login' => $_POST['usuario'],
            'sucesso' => 2,
            'data' => date('Y-m-d H:i:s'),
            'ip' => $_SERVER['REMOTE_ADDR'],
          ]
        );
      }
    } else {
      $this->login();
    }
  }

  public function logout()
  {
    $this->objUsuario->editarUsuario(['handle' => $_SESSION['handle'], 'SessaoUsuario' => '']);
    $helper = array_keys($_SESSION);
    foreach ($helper as $key){
        unset($_SESSION[$key]);
    }

    require_once './src/Controller/UsuarioController.php';
    $obj = new UsuarioController();
    $obj->login('Você saiu da sua sessão!');
    die();
  }

  public function editar($handle)
  {
    
    $msg_sucesso = '';
    $metodo = 'editar';
    $usuario = new Usuario();
    $objVendedor = new Vendedor();
    $objUsuarioEmpresa = new UsuarioEmpresa();
    $logAcesso = new LogsAcesso();
    if (isset($_POST) && !empty($_POST)) {
      $comissao = $this->validatePost('comissao');
      $usuario_vendedor = $this->validatePost('usuario_vendedor');

      unset($_POST['usuario_vendedor'], $_POST['comissao']);

      $_POST['handle'] = $handle;

      $objVendedor->editar(['usuario_vendedor' => $usuario_vendedor ?: 0, 'comissao' => $usuario_vendedor == 1 ? $this->monetaryValue($comissao) : 0, 'id_usuario' => $handle, 'afiliado' => 0]);

      $arrPermissaoEmpresa = [];
      
      $objUsuarioEmpresa->excluir($handle);
      if (isset($_POST['permissaoEmpresa'])) {
        $arrPermissaoEmpresa = $_POST['permissaoEmpresa'];
        unset($_POST['permissaoEmpresa']);
      }

      foreach ($arrPermissaoEmpresa as $key) {
        $objUsuarioEmpresa->cadastrar(['id_usuario' => $handle, 'id_empresa' => $key]);
      }

      $retorno = $usuario->editarUsuario($_POST);
      if ($retorno) {
        $msg_sucesso = ' Usuário alterado com sucesso.';
      }
    }

    $usuarios = new Usuario();
    $usuarios = $usuario->listarUsuario($handle);
    
    $objEmpresa = new Empresas();
    $arrDemaisEmpresas = $objEmpresa->listarTodos();

    $objUsuarioEmpresa = new UsuarioEmpresa();
    $consultaPermissoesUsuario = $objUsuarioEmpresa->listarEmpresasUsuario($handle);

    $permissoesEmpresaUsuario = array_column($consultaPermissoesUsuario, 'id_empresa');

    // print_r($usuarios);exit;

    $arrAcessos = $logAcesso->consultaAcessos($usuarios[0]->NomeUsuario);

    $arrListaGruposUsuario = $this->gruposUsuario;

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;
    $modulos = $this->modulos;
    $classe = $this->classe;
    require './src/View/Usuario/usuario_form.php';
  }

  public function cadastrar()
  {
    if ($_SESSION['usuario_master'] != 1) {
      die('Acesso negado');
    }
    $msg_sucesso = '';
    $usuarios = '';
    $metodo = 'cadastrar';
    $permissoesEmpresaUsuario = [];

    $usuario = new Usuario();
    $objEmpresa = new Empresas();
    $objUsuarioEmpresa = new UsuarioEmpresa();
    $objVendedor = new Vendedor();

    if (isset($_POST) && !empty($_POST)) {
      $arrPermissaoEmpresa = isset($_POST['permissaoEmpresa']) ? $_POST['permissaoEmpresa'] : [];
      unset($_POST['permissaoEmpresa']);

      
      $comissao = $this->validatePost('comissao');
      $usuario_vendedor = $this->validatePost('usuario_vendedor');
      
      unset($_POST['usuario_vendedor'], $_POST['comissao']);

      $retorno = $usuario->cadastrarUsuario($_POST);

      $objVendedor->editar(['usuario_vendedor' => $usuario_vendedor ?: 0, 'comissao' => $usuario_vendedor == 1 ? $this->monetaryValue($comissao) : 0, 'id_usuario' => $retorno, 'afiliado' => 0]);      

      foreach ($arrPermissaoEmpresa as $key) {
        $objUsuarioEmpresa->cadastrar(['id_usuario' => $retorno, 'id_empresa' => $key]);
      }      
      

      if ($retorno) {
        $msg_sucesso = 'Usuário cadastrado com sucesso.';
      }
      $metodo = 'editar';
      $usuarios = $usuario->listarUsuario($retorno);
    } else {
      $usuarios = array($usuario);
    }

    $arrListaGruposUsuario = $this->gruposUsuario;

    $arrDemaisEmpresas = $objEmpresa->listarTodos();

    $arrAcessos = [];
    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;
    $modulos = $this->modulos;
    $classe = $this->classe;
    require './src/View/Usuario/usuario_form.php';
  }
  
  public function perfil() {
	  
	$usuario = new Usuario();
	$sucesso = true;  
	if (isset($_POST) && !empty($_POST)) {
	  $_POST['handle'] = $_SESSION['handle'];
	  if ($_POST['SenhaUsuario'] !== $_POST['SenhaUsuario2']) {
		$msg_sucesso = ' As senhas são diferentes.';
		$sucesso = false;
	  } else {
		  unset($_POST['SenhaUsuario2']);
		$retorno = $usuario->editarUsuario($_POST);
		if ($retorno) {
			$msg_sucesso = ' Dados modificados com sucesso.';
		}
	  }
	}	  
	  
	  $usuarios = $usuario->listarUsuario('', $_SESSION['login']);	  
	  
	  $this->titulo_principal['descricao'] = 'Meu cadastro';
	  $titulo_principal = $this->titulo_principal;
	  $breadcrumb = array('Maestria' => URL . 'Usuario/perfil/', 'Meu cadastro' => URL . $this->classe . '/perfil/');;
	  $modulos = $this->modulos;
	  $metodo = 'perfil';
	  $classe = $this->classe;
	  require './src/View/Usuario/usuario_perfil.php';
  }
}
