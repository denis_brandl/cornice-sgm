<?php

require_once(dirname(__DIR__,1).'/Model/conexao.php');
require_once(dirname(__DIR__,1).'/Model/bd.php');
class DeveloperController {

  public function __construct() {
  }

  public function gerar($tabela) {
		$pdo = Conexao::getInstance();
		$crud = bd::getInstance($pdo);    
    $sql = "DESCRIBE `$tabela`";
		$dados = $crud->getSQLGeneric($sql);
    $strAtributos = '';
    $strConstrutor = '';
    $strCampos = '';
    $strColunasCabecalho = '';
    $strColunasListagem = '';
    $primary_key = '';

    $arrTabelaComposta = explode('_', $tabela);
    $nome_tabela_objeto = '';
    foreach ($arrTabelaComposta as $nome) {
      $nome_tabela_objeto .= ucwords($nome);
    }

    foreach ($dados as $dado) {
      if ($dado->Key == 'PRI') {
        $primary_key = $dado->Field;
      }

      if (!in_array($dado->Field,['data_criacao','data_atualizacao','id_usuario_criacao','id_usuario_edicao'])) {
        $strCampos .= '<div class="box-body">
          <div class="form-group col-xs-12 col-md-2">
            <label>'.$dado->Field.'</label>
            <input class="form-control" type="text" name="'.strtolower($dado->Field).'" value="<?php echo $'.$nome_tabela_objeto.'->'.strtolower($dado->Field).';?>">
          </div>
        </div>'.chr(13).chr(10);

        $strColunasCabecalho .= '
          <th>'.$dado->Field.'</th>
        '.chr(13).chr(10);
        $strColunasListagem .= '
          <td><?php echo $'.ucwords($tabela).'->'.$dado->Field.'; ?></td>
        '.chr(13).chr(10);
      }

      $strAtributos .= sprintf('public $%s;', $dado->Field).chr(13).chr(10);
      if ($dado->Type == 'date' || $dado->Type == 'timestamp') {
        $strConstrutor .= sprintf('$%s = date("Y-m-d");', $dado->Field).chr(13).chr(10);
        continue;
      }
      if (preg_match('/^varchar/m',$dado->Type)) {
        $strConstrutor .= sprintf('$%s = "";', $dado->Field).chr(13).chr(10);
        continue;
      }

      if (preg_match('/int|double|decimal|float/m',$dado->Type)) {
        $strConstrutor .= sprintf('$%s = 0;', $dado->Field).chr(13).chr(10);
        continue;
      }

      $strConstrutor .= sprintf('$%s = "NAO_IDENTIFICADO";', $dado->Field).chr(13).chr(10);
    }

    $arquivo_model = '<?php
        class '.$nome_tabela_objeto.' {
          '.$strAtributos.'
          public $nom_tabela = "'.$tabela.'";
          public function __construct() {
            '.$strConstrutor.'
          }

          public function listarTodos($pagina_atual = 0,$linha_inicial = 0,$coluna = \'\',$buscar = \'\') {
              $pdo = Conexao::getInstance();              
              $crud = bd::getInstance($pdo,$this->nom_tabela);
              $where = \'\';
              if ($coluna != \'\' && $buscar != \'\') {
                $where = sprintf(\' WHERE %s LIKE UPPER("%%%s%%") \',$coluna,strtoupper($buscar));
              }
              
              $paginacao = \' LIMIT \'.QTDE_REGISTROS;
              if ($pagina_atual > 0 && $linha_inicial > 0) {
                $paginacao = " LIMIT {$linha_inicial}, ".QTDE_REGISTROS;
              }		
              
              $sql = "SELECT * FROM ".$this->nom_tabela.$where.$paginacao;		
              $dados = $crud->getSQLGeneric($sql);
              
              return $dados;
          }

          public function listar($handle) {
            $pdo = Conexao::getInstance();
            $crud = bd::getInstance($pdo,$this->nom_tabela);
            $sql = "SELECT * FROM ".$this->nom_tabela." WHERE '.$primary_key.' = ?";
            $arrayParam = array($handle);
            $dados = $crud->getSQLGeneric($sql,$arrayParam, FALSE);
            return $dados;
          }	          

          public function listarTodosTotal() {
            $pdo = Conexao::getInstance();
            $crud = bd::getInstance($pdo,$this->nom_tabela);
            $sql = "SELECT count(*) as total_registros FROM ".$this->nom_tabela;
            $dados = $crud->getSQLGeneric($sql,null,FALSE);
            return $dados->total_registros;
          }

          public function editar($post) {
            $pdo = Conexao::getInstance();
            $crud = bd::getInstance($pdo,$this->nom_tabela);
              
            $arrEditar = array();
            foreach ($post as $key => $value) {
              if ($key != "handle" && $key != "data_criacao") {
                $arrEditar[$key] =  $value;
              }
            }
            $arrayCond = array("'.$primary_key.'=" => $post["handle"]);
            $retorno   = $crud->update($arrEditar, $arrayCond);
            return $retorno;            
          }

          public function cadastrar($post) {
            $pdo = Conexao::getInstance();
            
            $arrInserir = array();
            foreach ($post as $key => $value) {
              if ($key != "handle") {
                $arrInserir[$key] =  $value;
              }
            }
            $crud = bd::getInstance($pdo,$this->nom_tabela);
            $retorno   = $crud->insert($arrInserir);
            return $retorno;
          }

          public function excluir($handle) {
            $pdo = Conexao::getInstance();
            $crud = bd::getInstance($pdo,$this->nom_tabela);
            $crud->delete(array("'.$primary_key.'" => $handle));
            return true;
          }	          
        }
    ';

    $arquivo_controller = '
      <?php
        require_once \'./src/Model/'.$nome_tabela_objeto.'.php\';
        require_once \'./src/Controller/CommonController.php\';
        class '.$nome_tabela_objeto.'Controller extends CommonController {
          private $modulos = array();
          private $estados = array();
          private $classe = "'.$nome_tabela_objeto.'";
          private $breadcrumb = array();
          private $titulo_principal = "";

          public function __construct() {
            $common = new CommonController();	
            $modulos = $common->getModulos();

            $this->modulos = $modulos;

            $modulo_posicao = array_search($this->classe,array_column($modulos,"modulo"));
            $this->titulo_principal = $modulos[$modulo_posicao];
            $this->breadcrumb = array("Maestria"=>URL."dashboard/index/",$this->titulo_principal["descricao"] => URL.$this->classe."/listar/");
          }

          public function listar() {
            $common = new CommonController();

            $obj'.$nome_tabela_objeto.' = new '.$nome_tabela_objeto.'();
            $arr'.$nome_tabela_objeto.' = $obj'.$nome_tabela_objeto.'->listarTodos();

            $titulo_principal = $this->titulo_principal;
            $breadcrumb = $this->breadcrumb;		
            $modulos = $this->modulos;
            $classe = $this->classe;
            $metodo = $acao = "editar";
            require "./src/View/'.$nome_tabela_objeto.'/'.$tabela.'_listar.php";
          }

          public function editar($handle) {
            $msg_sucesso = "";
            $metodo = "editar";
            $obj'.$nome_tabela_objeto.' = new '.$nome_tabela_objeto.'();

            if (isset($_POST) && !empty($_POST)) {
              $retorno = $obj'.$nome_tabela_objeto.'->editar($_POST);
              if ($retorno) {
                $msg_sucesso = "'.$nome_tabela_objeto.' alterada com sucesso.";
              }
            }

            $'.$nome_tabela_objeto.' = $obj'.$nome_tabela_objeto.'->listar($handle);

            $titulo_principal = $this->titulo_principal;
            $breadcrumb = $this->breadcrumb;		
            $modulos = $this->modulos;
            $classe = $this->classe;
            require "./src/View/'.$nome_tabela_objeto.'/'.$tabela.'_form.php";
          }

          public function cadastrar() {
            $msg_sucesso = "";	
            $moedas = "";
            $metodo = "cadastrar";	
            $obj'.$nome_tabela_objeto.' = new '.$nome_tabela_objeto.'();

            if (isset($_POST) && !empty($_POST)) {
              $retorno = $obj'.$nome_tabela_objeto.'->cadastrar($_POST);
              if ($retorno) {
                $msg_sucesso = "'.$nome_tabela_objeto.' cadastrada com sucesso.";
              }
              $'.$nome_tabela_objeto.' = $obj'.$nome_tabela_objeto.'->listar($retorno);
              $metodo = "editar";
            } else {
              $'.$nome_tabela_objeto.' = $obj'.$nome_tabela_objeto.';
            }
            $titulo_principal = $this->titulo_principal;
            $breadcrumb = $this->breadcrumb;		
            $modulos = $this->modulos;
            $classe = $this->classe;
            require "./src/View/'.$nome_tabela_objeto.'/'.$tabela.'_form.php";	
          }

          public function excluir($handle) {
            $msg_sucesso = "";
            $produtos = "";
            $metodo = "cadastrar";

            $obj'.$nome_tabela_objeto.' = new '.$nome_tabela_objeto.'();
            $obj'.$nome_tabela_objeto.'->excluir($handle);
            $_SESSION["tipoMensagem"] = "callout-success";
            $_SESSION["mensagem"] = "'.$nome_tabela_objeto.' excluído com sucesso.";
            Header("Location: ".URL."'.$nome_tabela_objeto.'/listar/");
            exit();
          }	          
        }
    ';

    $arquivo_form = '
      <?php include_once(DOCUMENT_ROOT."src/View/Common/cabecalho.php"); ?>
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <form method="POST" action="<?php echo URL.$classe."/".$metodo."/".($'.$nome_tabela_objeto.'->'.$primary_key.' ?: 0);?>">
                  <?php if (!empty($msg_sucesso)) { ?>
                    <div class="callout callout-success">
                      <h4>Sucesso!</h4>
                      <p><?php echo $msg_sucesso;?></p>
                    </div>
                  <?php } ?>
                  <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Cadastro da sua '.$nome_tabela_objeto.'</h3>
                    </div>
                  </div>
                  '.$strCampos.'
                  <div class="box-footer">
                    <input type="hidden" name="handle" value="<?php echo $'.$nome_tabela_objeto.'->'.$primary_key.';?>">
                    <button class="btn btn-primary" type="submit">Salvar</button>
                    <a class="btn btn-primary" href="<?php echo URL.$classe.\'/Listar/\';?>">Voltar</a>
                  </div>                
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include_once(DOCUMENT_ROOT.\'src/View/Common/rodape.php\'); ?>  
    ';

    $arquivo_listar = '
      <?php include_once(DOCUMENT_ROOT."src/View/Common/cabecalho.php"); ?>  
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">					
                  <h3 class="box-title"></h3>					
                </div>              
              <div class="box-body">
                <div id="'.$tabela.'-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-6">
                      <a class="btn btn-primary" href="<?php echo URL.$classe.\'/cadastrar/\';?>">Cadastrar Novo</a>
                    </div>
                    <div class="col-sm-6">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <table class="table table-bordered table-striped dataTable">
                        <thead>
                          <tr role="row">
                            '.$strColunasCabecalho.'
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $aux = 0;
                            foreach ($arr'.$nome_tabela_objeto.' as $'.$nome_tabela_objeto.') {
                            if ($aux & 1) {
                          ?>
                            <tr class="odd" role="row">
                          <?php } else { ?>	
                            <tr class="even" role="row">
                          <?php } ?>	
                            '.$strColunasListagem.'
                            <td><a class="" href="<?php echo URL.$classe.\'/\'.$acao.\'/\'. $'.$nome_tabela_objeto.'->'.$primary_key.'; ?>"><i class="fa fa-edit"></i>Editar</a></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>									
                    </div>								
                  </div>                
                </div>
              </div>
            </div>
          </div>
        </div>
        </section>
      </div>
      <?php include_once(DOCUMENT_ROOT."src/View/Common/rodape.php"); ?>  
    ';
    
    /*
     * Expressão regular para formatar os arquivos
     * Irá remover os espaços em brancos no inicio de cada linha
    */    
    $re = '/^\s+/m';
    $subst = "";

    $fp = fopen(dirname(__DIR__,1).'/Model/'.$nome_tabela_objeto.'.php','w');
    fwrite($fp, preg_replace($re, $subst, $arquivo_model));
    fclose($fp);

    $fp = fopen(dirname(__DIR__,1).'/Controller/'.$nome_tabela_objeto.'Controller.php','w');
    fwrite($fp, preg_replace($re, $subst, $arquivo_controller));
    fclose($fp);
    
    if (!is_dir(dirname(__DIR__,1).'/View/'.$nome_tabela_objeto))
      mkdir(dirname(__DIR__,1).'/View/'.$nome_tabela_objeto);

    $fp = fopen(dirname(__DIR__,1).'/View/'.$nome_tabela_objeto.'/'.strtolower($tabela).'_form.php','w');
    fwrite($fp, preg_replace($re, $subst, $arquivo_form));
    fclose($fp);    

    $fp = fopen(dirname(__DIR__,1).'/View/'.$nome_tabela_objeto.'/'.strtolower($tabela).'_listar.php','w');
    fwrite($fp, preg_replace($re, $subst, $arquivo_listar));
    fclose($fp);        
    
  }

  public function downloadBase() {
    try {
      
      $dir = DOCUMENT_ROOT . 'publico/'.DBNAME.'.sql';
      $comando = sprintf(
          'mysqldump --add-drop-table --user=%s --password="%s" --host=%s %s --result-file=%s 2>&1',
          USER,
          PASSWORD,
          HOST,
          DBNAME,
          $dir
      );
      
      exec($comando, $output);

      exec("sed -i '1{/999999.*sandbox/d}' $dir 2>&1");

      if (empty($output)) {

        header("Expires: 0");
        header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Content-type: application/file");
        header('Content-length: '.filesize($dir));
        header('Content-disposition: attachment; filename="'.basename($dir).'"');
        readfile($dir);

        sleep(3);

        unlink($dir);
      }

      throw new ErrorException(json_encode($output));

    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }
}