<?php
require_once './src/Model/Cliente.php';
require_once './src/Model/ClienteGrupo.php';
require_once './src/Model/Orcamento.php';
require_once './src/Model/UnidadeMedida.php';
require_once './src/Model/Grupo.php';
require_once './src/Model/Dominio.php';
require_once './src/Controller/CommonController.php';
require_once './src/Model/Logradouro.php';
require_once './src/Model/Municipio.php';
require_once './src/Model/Assets.php';
require_once './src/Model/Estado.php';
require_once './src/Model/Municipio.php';
class ClienteController extends CommonController
{

  private $modulos = array();
  private $estados = array();
  private $classe = 'Cliente';
  private $breadcrumb = array();
  private $titulo_principal = '';

  private $arrLogradouros = array();

  private $habilita_financeiro = false;

  public function __construct()
  {
    $cliente = new Cliente();
    $common = new CommonController();
    $logradouro = new Logradouro();
    $modulos = $common->getModulos();
    $estados = $common->getEstados();
    $configuracoes = new Configuracoes();

    $this->modulos = $modulos;
    $this->estados = $estados;
    $this->habilita_financeiro = filter_var($configuracoes->listarConfiguracao('habilita_financeiro')->valor, FILTER_VALIDATE_BOOLEAN);
    $this->arrLogradouros = $logradouro->listarTodos();

    $modulo_posicao = array_search($this->classe, array_column($modulos, 'modulo'));
    $this->titulo_principal = $modulos[$modulo_posicao];
    $this->breadcrumb = array('Maestria' => URL . 'dashboard/index/', $this->titulo_principal['descricao'] => URL . $this->classe . '/listar/');
  }

  public function listar()
  {

    if (isset($_SESSION['mensagem']) && !empty($_SESSION['mensagem'])) {
      $msg_sucesso = $_SESSION['mensagem'];

      if (isset($_SESSION['tipoMensagem']) && !empty($_SESSION['tipoMensagem'])) {
        $tipo_mensagem = $_SESSION['tipoMensagem'];
      }

      unset($_SESSION['mensagem']);
      unset($_SESSION['tipoMensagem']);
    }
    $modulos = $this->modulos;
    $classe = $this->classe;

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;

    require './src/View/Cliente/cliente_listar.php';
  }

  public function editar($handle)
  {
    $msg_sucesso = '';
    $metodo = 'editar';
    $cliente = new Cliente();
    $objAssets = new Assets();
    if (isset($_POST) && !empty($_POST)) {
      $retorno = $cliente->editarCliente($_POST);
      if ($retorno) {
        $msg_sucesso = $this->classe . ' alterado com sucesso.';
      }
    }

    $UnidadeMedida = new UnidadeMedida();
    $unidades = $UnidadeMedida->listarTodos();

    $grupo = new Grupo();
    $grupos = $grupo->listarTodos();

    $dominio = new Dominio();
    $dominios = $dominio->listarTodos('TIPO_CLIENTE');

    $clienteGrupo = new ClienteGrupo();
    $clienteGrupos = $clienteGrupo->listarTodos(0, 0, 'status', '1');

    $clientes = $cliente->listarCliente($handle);
    $arrAssets = $objAssets->listarTodos([
      'filtro' => [
        'Title' => $handle
      ]
    ]);
    $modulos = $this->modulos;
    $estados = $this->estados;
    $classe = $this->classe;

    $municipio = new Municipio();
    $listaMunicipios = $municipio->listarMunicipiosPorEstado($clientes[0]->Estado);

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;

    require './src/View/Cliente/cliente_form.php';
  }

  public function cadastrar()
  {
    $msg_sucesso = '';
    $clientes = '';
    $metodo = 'cadastrar';

    $cliente = new Cliente();

    if (isset($_POST) && !empty($_POST)) {
      $retorno = $cliente->cadastrarCliente($_POST);
      if ($retorno) {
        $msg_sucesso = 'Cliente cadastrado com sucesso.';
      }
      $clientes = $cliente->listarCliente($retorno);
      $metodo = 'editar';
    } else {
      $clientes = array($cliente);
    }

    $clienteGrupo = new ClienteGrupo();
    $clienteGrupos = $clienteGrupo->listarTodos(0, 0, 'status', '1');

    $UnidadeMedida = new UnidadeMedida();
    $unidades = $UnidadeMedida->listarTodos();

    $grupo = new Grupo();
    $grupos = $grupo->listarTodos();

    $dominio = new Dominio();
    $dominios = $dominio->listarTodos('TIPO_CLIENTE');

    $modulos = $this->modulos;
    $classe = $this->classe;

    $estados = $this->estados;

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;

    $arrAssets = [];

    require './src/View/Cliente/cliente_form.php';
  }

  public function carrega()
  {
    $cliente = new Cliente();
    $clientes = array();
    $arrClientes = array();

    if (!$_POST) {
      return json_encode(array());
    }

    $clientes = $cliente->listarTodos(1, 0, ['CGC', 'RazaoSocial', 'Nomefantasia'], $_POST['q']);


    foreach ($clientes as $key => $cliente) {
      $arrClientes['itens'][] = array('id' => $cliente->CodigoCliente, 'text' => sprintf('%s - %s',$cliente->RazaoSocial != '' ? $cliente->RazaoSocial : $cliente->Nomefantasia, $cliente->Telefone1));
    }

    $clientes['total_count'] = count($clientes);

    $arrClientes = json_encode($arrClientes);

    echo $arrClientes;
  }

  public function excluir($handle)
  {
    $msg_sucesso = '';
    $produtos = '';
    $metodo = 'cadastrar';

    $cliente = new Cliente();
    $orcamento = new Orcamento();
    $consultaCliente = $orcamento->pedidosPorCliente($handle);

    $_SESSION['mensagem'] = 'Erro ao excluir o cliente.';
    $_SESSION['tipoMensagem'] = 'callout-danger';
    if ($consultaCliente[0]->total == 0) {
      $cliente->excluir($handle);
      $_SESSION['tipoMensagem'] = 'callout-success';
      $_SESSION['mensagem'] = 'Cliente excluído com sucesso.';
    }
    Header('Location: ' . URL . 'Cliente/listar/');
    exit();
  }

  public function listaClientesDataTables()
  {
    $cliente = new Cliente();

    $draw = $this->validateGet('draw') ?: 1;
    $length = $this->validateGet('length') ?: 10;
    $start = $this->validateGet('start') ?: 1;
    $searchValue = isset($_GET['search']['value']) ? $_GET['search']['value'] : '';
    $order = '';
    if (isset($_GET['order']) && is_array($_GET['order'])) {
      $order = $_GET['columns'][$_GET['order'][0]['column']]['data'] . ' ' . strtoupper($_GET['order'][0]['dir']);
    }

    $clientes = $cliente->listarTodos(1, ($start - 1), ['RazaoSocial', 'CGC', 'InscricaoEstadual'], $searchValue, $length, $order);

    $num_registros = $cliente->listarTodosTotal('', $searchValue);

    $arrClientes = array();
    foreach ($clientes as $cliente) {

      $editar = '<a class="btn btn-app" href="' . URL . $this->classe . '/editar/' . $cliente->CodigoCliente . '"><i class="fa fa-edit"></i>Editar</a>';
      $excluir = '<a class="btn btn-app excluirCliente" clienteId="' . $cliente->CodigoCliente . '"  href="#"><i class="fa fa-trash"></i>Excluir</a>';
      if ($cliente->qtdUso > 0) {
        $excluir = '<a class="btn btn-app naoExcluirCliente" style="opacity: 0.4;"><i class="fa fa-trash"></i>Excluir</a>';
      }
      $arrClientes[] = array(
        'RazaoSocial' => $cliente->RazaoSocial,
        'Nomefantasia' => $cliente->TipoCliente == 'J' ? $cliente->Nomefantasia : '<i>Não se aplica</i>',
        'Endereco' => $cliente->Endereco,
        'Telefone1' => $cliente->Telefone1,
        'Telefone2' => $cliente->Telefone2,
        'Ramal' => $cliente->Ramal,
        'EMail' => $cliente->EMail,
        'Contato' => $cliente->Contato,
        'Editar' => $editar,
        'Excluir' => $excluir
      );
    }
    $arrRetorno = array(
      'draw' => $draw,
      'recordsTotal' => $num_registros,
      'recordsFiltered' => $num_registros,
      'data' => $arrClientes
    );

    echo json_encode($arrRetorno);
    return;
  }

  public function consultaClienteExiste()
  {
    $cliente = new Cliente();

    $telefone = $this->validatePost('telefone');
    $documento = $this->validatePost('documento');
    $tipoCliente = $this->validatePost('tipoCliente');

    $documento =  $tipoCliente == 'F'
                    ? preg_replace('/([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})/m',"$1.$2.$3/$4", $documento)
                    : preg_replace('/([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})/m',"$1.$2.$3/$4-$5", $documento);

    $clientes = array();

    if (!$_POST) {
      return json_encode(array());
    }

    $clientes = $cliente->listarTodos(1, 0, $telefone ? 'Telefone1' : 'CGC', $telefone ?: $documento, 1);

    echo json_encode(['quantidade' => count($clientes)]);
  }  

  public function importar() {
    try {
      $objCliente = new Cliente();
      $objEstado = new Estado();
      $objMunicipio = new Municipio();

      $pdo = Conexao::getInstance();

      $crud = bd::getInstance($pdo, 'Clientes');      

      $temp_arquivo = dirname(__DIR__, 2) . '/publico/Cliente.csv';

      $linha = 1;
      if (($handle = fopen($temp_arquivo, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
          if ($linha == 1) {
            $linha++;
            continue;
          }

          $nome_fantasia = $data[1];
          $razao_social = $data[0];
          $cnpj = $data[2];
          $cpf = $data[3];
          $inscricao_estadual = $data[9];
          $email = $data[10];
          $telefone = $data[11];
          $cep = $data[13];

          $consultaEstado = $objEstado->listarTodos('uf', $data[14]);          
          if (count($consultaEstado) == 0) {
            throw new ErrorException("Estado $data[15] não localizado para o cliente $razao_social na linha " . ($linha + 1));
          }

          $consultaMunicipio = $objMunicipio->listarTodos(0,0,'nome',strtoupper($data[15]));
          if (count($consultaMunicipio) == 0) {
            throw new ErrorException("Municipio $data[15] não localizado para o cliente $razao_social na linha " . ($linha + 1));
          }          

          $estado = $consultaEstado[0]->codigo_uf;
          $cidade =  $consultaMunicipio[0]->codigo_ibge;
          $endereco = $data[16];
          $numeroEndereco = $data[17];
          $bairro = $data[18];
          $complemento = $data[19];

          if ($cnpj != '' || $cpf != '') {

            $consultaClienteExiste = $crud->getSQLGeneric(
              " SELECT
                  CodigoCliente, 
                  replace(REPLACE(CGC,'.', ''),'/','') as cgcSomenteNumero
                FROM
                  Clientes
                HAVING cgcSomenteNumero = '".$this->onlyNumber($cnpj ?: $cpf)."'
              "
            );

            if (count($consultaClienteExiste) > 0) {
              // throw new ErrorException("Cliente $razao_social na linha " . ($linha + 1) . " já cadastrado com o código " . $consultaClienteExiste[0]->CodigoCliente);
              echo "Cliente $razao_social na linha " . ($linha + 1) . " já cadastrado com o código " . $consultaClienteExiste[0]->CodigoCliente . "<br>";
              continue;
            }
          }

          $arrNovoCliente = [
            'RazaoSocial' => $razao_social,
            'Nomefantasia' => $nome_fantasia,
            'Endereco' => $endereco,
            'Complemento' => $complemento,
            'Bairro' => $bairro,
            'CEP' => $cep,
            'Cidade' => $cidade,
            'Estado' => $estado,
            'CGC' => $cnpj ?: $cpf,
            'InscricaoEstadual' => $inscricao_estadual,
            'Telefone1' => $telefone,
            'numeroEndereco' => $numeroEndereco,
            'EMail' => $email,
            'idLogradouro' => 33
          ];

          $objCliente->cadastrarCliente($arrNovoCliente);

        }
      }
      fclose($handle);
    } catch (Exception $e) {
      echo json_encode([
        'success' => 0,
        'msg' => $e->getMessage(),
        'erros' => []
      ]);
    } catch (Throwable $e) {
      echo json_encode([
        'success' => 0,
        'msg' => $e->getMessage(),
        'erros' => []
      ]);
    }  
  }
}
?>
