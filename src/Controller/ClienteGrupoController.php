<?php
require_once './src/Model/ClienteGrupo.php';
require_once './src/Controller/CommonController.php';
class ClienteGrupoController extends CommonController {

	private $modulos = array();
	private $classe = 'ClienteGrupo';
	private $breadcrumb = array();
	private $titulo_principal = '';		
	
	public function __construct() {
		$clienteGrupo = new ClienteGrupo();
		$common = new CommonController();

    $this->modulos = $common->getModulos();
		
		$modulo_posicao = array_search($this->classe,array_column($this->modulos,'modulo'));
		$this->titulo_principal = $this->modulos[$modulo_posicao];		
		$this->breadcrumb = array('Maestria'=>URL.'dashboard/index/',$this->titulo_principal['descricao'] => URL.$this->classe.'/listar/');				
		
	}
	
	public function listar() {
		$clienteGrupo = new ClienteGrupo();
		$common = new CommonController();
		
		$coluna = '';
		$buscar = '';
		$pagina_atual = 1;		
		if ($this->validateGet('parametros')) {
			$re = "/^[a-z]+=/"; 
			preg_match($re, $this->validateGet('parametros'), $matches);
			$acao = str_replace('=','',$matches[0]);
			
			$re = "/=([a-zA-Z].*)\|([A-Za-z0-9].*)$/";
			preg_match($re, $this->validateGet('parametros'), $matches);
			
			switch ($acao) {
				case 'buscar':
					if (isset($matches[1])) {
						$coluna = str_replace('=','',$matches[1]);
					}
					if (isset($matches[2])) {
						$buscar = str_replace('=','',$matches[2]);
					}					
				break;
				
				case 'listar':
					$pagina_atual = str_replace('=','',$matches[2]);
			}
		}
		
		$linha_inicial = ($pagina_atual -1) * QTDE_REGISTROS;
		
		$num_registros = $clienteGrupo->listarTodosTotal();
		
		/* Idêntifica a primeira página */  
		$primeira_pagina = 1;   
		
		/* Cálcula qual será a última página */  
		$ultima_pagina  = ceil($num_registros / QTDE_REGISTROS);   
		
		/* Cálcula qual será a página anterior em relação a página atual em exibição */   
		$pagina_anterior = ($pagina_atual > 1) ? $pagina_atual -1 : 0 ;   
		
		/* Cálcula qual será a pŕoxima página em relação a página atual em exibição */   
		$proxima_pagina = ($pagina_atual < $ultima_pagina) ? $pagina_atual +1 : 0 ;  
		
		/* Cálcula qual será a página inicial do nosso range */    
		$range_inicial  = (($pagina_atual - RANGE_PAGINAS) >= 1) ? $pagina_atual - RANGE_PAGINAS : 1 ;   
		
		/* Cálcula qual será a página final do nosso range */    
		$range_final   = (($pagina_atual + RANGE_PAGINAS) <= $ultima_pagina ) ? $pagina_atual + RANGE_PAGINAS : $ultima_pagina ;   
		
		/* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */   
		$exibir_botao_inicio = ($range_inicial < $pagina_atual) ? 'mostrar' : 'esconder'; 
		
		/* Verifica se vai exibir o botão "Anterior" e "Último" */   
		$exibir_botao_final = ($range_final > $pagina_atual) ? 'mostrar' : 'esconder';  						
		
		$arrCamposBusca = array('nome'	=> 'Nome','status'		=> 'Status',);
		
		$clienteGrupos = $clienteGrupo->listarTodos($pagina_atual,$linha_inicial,$coluna,$buscar);
		
		$titulo_principal = $this->titulo_principal;
		$breadcrumb = $this->breadcrumb;		
		$modulos = $this->modulos;
		$classe = $this->classe;
		require './src/View/ClienteGrupo/cliente_grupo_listar.php';
	}
	
	public function editar($handle) {
		$msg_sucesso = '';
		$metodo = 'editar';
		$clienteGrupo = new ClienteGrupo();		
		if (isset($_POST) && !empty($_POST)) {
      $_POST['handle'] = $handle;
			$retorno = $clienteGrupo->editarClienteGrupo($_POST);
			if ($retorno) {
				$msg_sucesso = ' Grupo de cliente alterado com sucesso.';
			}
		}
		
		$clienteGrupos = new ClienteGrupo();
		$clienteGrupos = $clienteGrupo->listarClienteGrupo($handle);
		$titulo_principal = $this->titulo_principal;
		$breadcrumb = $this->breadcrumb;		
		$modulos = $this->modulos;
		$classe = $this->classe;
		require './src/View/ClienteGrupo/cliente_grupo_form.php';
	}
	
	public function cadastrar() {
		$msg_sucesso = '';	
		$clienteGrupos = '';
		$metodo = 'cadastrar';	
		
		$clienteGrupo = new ClienteGrupo();

		if (isset($_POST) && !empty($_POST)) {
			$retorno = $clienteGrupo->cadastrarClienteGrupo($_POST);
			if ($retorno) {
				$msg_sucesso = 'Grupo de cliente cadastrado com sucesso.';        
			}
		  $clienteGrupos = $clienteGrupo->listarClienteGrupo($retorno);

		} else {
			$clienteGrupos = array($clienteGrupo);
		}		
		
		$titulo_principal = $this->titulo_principal;
		$breadcrumb = $this->breadcrumb;		
		$modulos = $this->modulos;
		$classe = $this->classe;
		require './src/View/ClienteGrupo/cliente_grupo_form.php';	
	}
}
?>
