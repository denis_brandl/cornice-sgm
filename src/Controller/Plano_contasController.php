
      <?php
        require_once './src/Model/Plano_contas.php';
        require_once './src/Controller/CommonController.php';
        class Plano_contasController extends CommonController {
          private $modulos = array();
          private $estados = array();
          private $classe = "Plano_contas";
          private $breadcrumb = array();
          private $titulo_principal = "";

          public function __construct() {
            $common = new CommonController();	
            $modulos = $common->getModulos();

            $this->modulos = $modulos;

            $modulo_posicao = array_search($this->classe,array_column($modulos,"modulo"));
            $this->titulo_principal = $modulos[$modulo_posicao];
            $this->breadcrumb = array("Maestria"=>URL."dashboard/index/",$this->titulo_principal["descricao"] => URL.$this->classe."/listar/");
          }

          public function listar() {
            $common = new CommonController();

            $objPlano_contas = new Plano_contas();
            $arrPlano_contas = $objPlano_contas->listarTodos();

            $titulo_principal = $this->titulo_principal;
            $breadcrumb = $this->breadcrumb;		
            $modulos = $this->modulos;
            $classe = $this->classe;
            $metodo = $acao = "editar";
            require "./src/View/Plano_contas/plano_contas_listar.php";
          }

          public function editar($handle) {
            $msg_sucesso = "";
            $metodo = "editar";
            $objPlano_contas = new Plano_contas();

            if (isset($_POST) && !empty($_POST)) {
              $retorno = $objPlano_contas->editar($_POST);
              if ($retorno) {
                $msg_sucesso = "Plano_contas alterada com sucesso.";
              }
            }

            $Plano_contas = $objPlano_contas->listar($handle);

            $titulo_principal = $this->titulo_principal;
            $breadcrumb = $this->breadcrumb;		
            $modulos = $this->modulos;
            $classe = $this->classe;
            require "./src/View/Plano_contas/plano_contas_form.php";
          }

          public function cadastrar() {
            $msg_sucesso = "";	
            $moedas = "";
            $metodo = "cadastrar";	
            $objPlano_contas = new Plano_contas();

            if (isset($_POST) && !empty($_POST)) {
              $retorno = $objPlano_contas->cadastrar($_POST);
              if ($retorno) {
                $msg_sucesso = "Plano_contas cadastrada com sucesso.";
              }
              $Plano_contas = $objPlano_contas->listar($retorno);
              $metodo = "editar";
            } else {
              $Plano_contas = $objPlano_contas;
            }
            $titulo_principal = $this->titulo_principal;
            $breadcrumb = $this->breadcrumb;		
            $modulos = $this->modulos;
            $classe = $this->classe;
            require "./src/View/Plano_contas/plano_contas_form.php";	
          }

          public function excluir($handle) {
            $msg_sucesso = "";
            $produtos = "";
            $metodo = "cadastrar";

            $objPlano_contas = new Plano_contas();
            $objPlano_contas->excluir($handle);
            $_SESSION["tipoMensagem"] = "callout-success";
            $_SESSION["mensagem"] = "Plano_contas excluído com sucesso.";
            Header("Location: ".URL."Plano_contas/listar/");
            exit();
          }	          
        }
    