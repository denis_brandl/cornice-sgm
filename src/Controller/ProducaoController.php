<?php
require_once './src/Controller/CommonController.php';
require_once './src/Model/ItemOrcamento.php';
require_once './src/Model/Componente_Item_Orcamento.php';
require_once './src/Model/Orcamento.php';
require_once './src/Model/Situacao.php';
require_once './src/Model/Empresas.php';
require_once './src/Model/Usuario.php';
class ProducaoController extends CommonController {

	private $modulos = array();
	private $classe = 'Producao';
  private $empresas = array();
	private $breadcrumb = array();
	private $titulo_principal = '';		
  private $usuarios = array();
	
	public function __construct() {
		$common = new CommonController();
    $objUsuario = new Usuario();

    $this->modulos = $common->getModulos();
    $this->usuarios = $objUsuario->listarTodos(0,0, 'usuario_master', '0');
		
		$modulo_posicao = array_search($this->classe,array_column($this->modulos,'modulo'));
		$this->titulo_principal = $this->modulos[$modulo_posicao];		
		$this->breadcrumb = array('Maestria'=>URL.'dashboard/index/',$this->titulo_principal['descricao'] => URL.$this->classe.'/listar/');				
		
	}
	
	public function listar() {

		$common = new CommonController();
    $objOrcamento = new Orcamento();
    $objItemOrcamento = new ItemOrcamento();
    $objMolduraItemOrcamento = new Moldura_Item_Orcamento();
    $objComponenteItemOrcamento = new Componente_Item_Orcamento();          
    $objSituacao = new Situacao();
    $objEmpresas = new Empresas();
    
    $usuario_master = (isset($_SESSION['usuario_master']) && $_SESSION['usuario_master'] == 1);

		$periodo_inicial = '';
		$periodo_final = '';    

    $this->empresas = $objEmpresas->listarTodos(0, 0, 'status', 1);

    if ($_POST) {
      $consultaPedido = $objOrcamento->listarOrcamento($_POST['handle']);
      $objOrcamento->editarOrcamento(
        [
          'handle' => $_POST['handle'],
          'id_situacao_de' => $consultaPedido[0]->Id_Situacao,
          'Id_Situacao' => $_POST['Id_Nova_Situacao'],
          'observacaoHistorico' => $_POST['Ds_Observacao']
        ]
      );
    }

    $listaSituacoes = $objSituacao->listarSituacaoProducao();

    $filtro_empresa = $this->validateGet('empresa') ?: '';
    $filtro_vendedor = $this->validateGet('vendedor') ?: '';
    $periodo_inicial = $common->validateGet('data_inicio') ? date('Y-m-d 00:00:00',strtotime(str_replace('/','-',$common->validateGet('data_inicio')))) : '';
    $periodo_final 	 = $common->validateGet('data_final') ? date('Y-m-d 23:59:59',strtotime(str_replace('/','-',$common->validateGet('data_final')))) : '';
    $listaPedidos = $objOrcamento->listarOrcamentoProducao(['empresa' => $filtro_empresa, 'vendedor' => $filtro_vendedor, 'data_inicio' => $periodo_inicial, 'data_final' => $periodo_final]);


    $arrPedidos = [];

    foreach ($listaPedidos as $pedido) {
      $itemsOrcamento = $objItemOrcamento->listarItemOrcamento($pedido->Cd_Orcamento);
      $arrItemsOrcamento  = [];
      $arrMoldurasItemOrcamento = array();
      $arrComponentesItemOrcamento = array();      
      foreach ($itemsOrcamento as $item_orcamento) {
        $moldurasItemOrcamento = $objMolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $pedido->Cd_Orcamento);
        foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
          $arrMoldurasItemOrcamento[] = array('Cd_Produto' => $moldura_item_orcamento->CodigoProduto, 'DescricaoProduto' => $moldura_item_orcamento->DescricaoProduto, 'codigoComplexidade' => $moldura_item_orcamento->codigoComplexidade, 'nomeComplexidade' => $moldura_item_orcamento->nomeComplexidade, 'sarrafo_reforco' => $moldura_item_orcamento->sarrafo_reforco);
        }
        $componentesItemOrcamento = $objComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $pedido->Cd_Orcamento);
        foreach ($componentesItemOrcamento as $componente_item_orcamento) {
          $arrComponentesItemOrcamento[] = array('Id_Componente' => $componente_item_orcamento->CodigoProduto, 'Descricao' => $componente_item_orcamento->DescricaoProduto);
        }

        $arrItemsOrcamento[] = 
          [
            'itens' => $item_orcamento, 
            'molduras' => $arrMoldurasItemOrcamento,
            'componentes' => $arrComponentesItemOrcamento
          ];
      }
      $arrPedidos[$pedido->Id_Situacao][] = [
        'dados_pedido'=>$pedido,
        'itens_orcamento' => $arrItemsOrcamento
      ];
    }
    // print_r($arrPedidos);exit;

		$titulo_principal = $this->titulo_principal;
		$breadcrumb = $this->breadcrumb;		
		$modulos = $this->modulos;
		$classe = $this->classe;
		require './src/View/Orcamento/producao_listar.php';
	}
}
?>
