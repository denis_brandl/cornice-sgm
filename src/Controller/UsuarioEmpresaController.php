<?php
require_once './src/Model/UsuarioEmpresa.php';
require_once './src/Controller/CommonController.php';
class UsuarioEmpresaController extends CommonController
{
    private $modulos = array();
    private $estados = array();
    private $classe = "UsuarioEmpresa";
    private $breadcrumb = array();
    private $titulo_principal = "";
    public function __construct()
    {
        $common = new CommonController();
        $modulos = $common->getModulos();
        $this->modulos = $modulos;
        $modulo_posicao = array_search($this->classe, array_column($modulos, "modulo"));
        $this->titulo_principal = $modulos[$modulo_posicao];
        $this->breadcrumb = array("Maestria" => URL . "dashboard/index/", $this->titulo_principal["descricao"] => URL . $this->classe . "/listar/");
    }
    public function listar()
    {
        $common = new CommonController();
        $objUsuarioEmpresa = new UsuarioEmpresa();
        $arrUsuarioEmpresa = $objUsuarioEmpresa->listarTodos();
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        $metodo = $acao = "editar";
        require "./src/View/UsuarioEmpresa/usuario_empresa_listar.php";
    }
    public function editar($handle)
    {
        $msg_sucesso = "";
        $metodo = "editar";
        $objUsuarioEmpresa = new UsuarioEmpresa();
        if (isset($_POST) && !empty($_POST)) {
            $retorno = $objUsuarioEmpresa->editar($_POST);
            if ($retorno) {
                $msg_sucesso = "UsuarioEmpresa alterada com sucesso.";
            }
        }
        $UsuarioEmpresa = $objUsuarioEmpresa->listar($handle);
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        require "./src/View/UsuarioEmpresa/usuario_empresa_form.php";
    }
    public function cadastrar()
    {
        $msg_sucesso = "";
        $moedas = "";
        $metodo = "cadastrar";
        $objUsuarioEmpresa = new UsuarioEmpresa();
        if (isset($_POST) && !empty($_POST)) {
            $retorno = $objUsuarioEmpresa->cadastrar($_POST);
            if ($retorno) {
                $msg_sucesso = "UsuarioEmpresa cadastrada com sucesso.";
            }
            $UsuarioEmpresa = $objUsuarioEmpresa->listar($retorno);
            $metodo = "editar";
        } else {
            $UsuarioEmpresa = $objUsuarioEmpresa;
        }
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        require "./src/View/UsuarioEmpresa/usuario_empresa_form.php";
    }
    public function excluir($handle)
    {
        $msg_sucesso = "";
        $produtos = "";
        $metodo = "cadastrar";
        $objUsuarioEmpresa = new UsuarioEmpresa();
        $objUsuarioEmpresa->excluir($handle);
        $_SESSION["tipoMensagem"] = "callout-success";
        $_SESSION["mensagem"] = "UsuarioEmpresa excluído com sucesso.";
        Header("Location: " . URL . "UsuarioEmpresa/listar/");
        exit();
    }
}
