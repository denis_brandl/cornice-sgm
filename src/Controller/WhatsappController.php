<?php

include '~/../src/Controller/CommonController.php';
include '~/../src/Controller/CurlController.php';
include '~/../src/Model/Orcamento.php';
include '~/../src/Model/Empresas.php';
include '~/../src/Model/Situacao.php';
include '~/../src/Model/ItemOrcamento.php';
// include '~/../src/Model/Moldura_Item_Orcamento.php';
include '~/../src/Model/Componente_Item_Orcamento.php';

class WhatsappController {


  private $arrVariaveisPedido = [
		  '[NUMERO_ORCAMENTO]',
		  '[NOME_CLIENTE]',
		  '[DATA_ORCAMENTO]',
		  '[ITENS_ORCAMENTO]',
		  '[OBSERVACAO_CLIENTE]',
		  '[VALOR_BRUTO_PEDIDO]',
		  '[SUBTOTAL]',
		  '[VALOR_ENTRADA]',
		  '[VALOR_DESCONTO]',
		  '[VALOR_SALDO]',
		  '[LABEL_ORCAMENTO_PEDIDO]',
		  '[NOME_EMPRESA]',
		  '[HORARIO_FUNCIONAMENTO]',
		  '[OBSERVACAO_EMPRESA]',
      '[NOME_VENDEDOR]'
  ];
  private $regex = '/\[(.*?)\]/x';
  
  
  public function __construct() {
  }

  public function enviarMensagem($id_orcamento = 0, $retorno = 'json') {

    try {
    $objOrcamento = new Orcamento();
    $objItemOrcamento = new ItemOrcamento();
    $objMolduraItemOrcamento = new Moldura_Item_Orcamento();
    $objComponenteItemOrcamento = new Componente_Item_Orcamento();
    $objConfiguracoes = new Configuracoes();
    $objEmpresa = new Empresas();

    $consultaOrcamento = $objOrcamento->listarOrcamento($id_orcamento);
    $itemsOrcamento = $objItemOrcamento->listarItemOrcamento($id_orcamento);    
    $consultaLayoutMensagemWhatsapp = $objConfiguracoes->listarConfiguracao('layout_mensagem_whatsapp');
    $ocultar_componentes_impressao_pedido = filter_var($objConfiguracoes->listarConfiguracao('ocultar_componentes_impressao_pedido')->valor, FILTER_VALIDATE_BOOLEAN);
    $layout_mensagem_whatsapp = trim($consultaLayoutMensagemWhatsapp->valor);
    $modelo_empresa = $objConfiguracoes->listarConfiguracao('modelo_empresa')->valor;
    
    $id_empresa = 1; //$consultaOrcamento[0]->id_empresa ?: 1;
    $consultaEmpresa = $objEmpresa->ListarEmpresa($id_empresa);
    
    if ($layout_mensagem_whatsapp === '') {
		throw new ErrorException('Não foi cadastrado nenhuma mensagem para whatsapp. Verifique em Administração -> Configurações');
	}

    if (sizeof($consultaOrcamento) == 0) {
      throw new ErrorException('Pedido ' . $id_orcamento . ' não encontrado');
    }
    
    $numeroDestionatario = preg_replace('/\D/m', '', $consultaOrcamento[0]->Telefone1);

    if ($numeroDestionatario == "") {
      throw new ErrorException('Número de destinatario inválido para o cliente ' . $consultaOrcamento[0]->RazaoSocial. 'Verifique o número cadastrado como Principal no cadastro do cliente');
    }

    // print_r($itemsOrcamento);exit;

    $strItemsOrcamento = '';
    foreach ($itemsOrcamento as $item_orcamento) {

      $moldurasItemOrcamento = $objMolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $id_orcamento);
      $arrMoldurasItemOrcamento = [];
      foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
        $arrMoldurasItemOrcamento[] = $moldura_item_orcamento->DescricaoProduto;
      }

      $arrComponentesOrcamento = [];
      if ($ocultar_componentes_impressao_pedido== false) {
        $componentesItemOrcamento = $objComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $id_orcamento);
        foreach ($componentesItemOrcamento as $componente_item_orcamento) {
          $arrComponentesOrcamento[] = $componente_item_orcamento->DescricaoProduto;
        }
      }

      if ($modelo_empresa != 'venda-avulsa') {
        $strItemsOrcamento .= sprintf(
          '
            *Item: %s*
            Objeto: %s
            Qtd: %s
            Largura: %s
            Altura: %s
            Valor Unitário: R$ %s
            Valor Total: R$ %s
            Produtos:
            %s
            %s
            %s
          ',
          $item_orcamento->Cd_Item_Orcamento,
          $item_orcamento->descricao,
          $item_orcamento->Qt_Item,
          $item_orcamento->Md_Largura,
          $item_orcamento->Md_Altura,
          number_format($item_orcamento->Vl_Unitario, 2, ",", ""),
          number_format($item_orcamento->Vl_Bruto, 2, ",", ""),
          implode(',', $arrMoldurasItemOrcamento) .  ',' . implode(',',$arrComponentesOrcamento),
          $item_orcamento->Ds_Observacao !== '' ? 'Observação: ' : '',
          $item_orcamento->Ds_Observacao

        );        
      } else {
          $exibir_valores = $item_orcamento->Vl_Unitario != 0 ?
            sprintf(
              'Valor Unitário: R$ %s
            Valor Total: R$ %s',
              number_format($item_orcamento->Vl_Unitario, 2, ",", ""),
              number_format($item_orcamento->Vl_Bruto, 2, ",", ""),
            )
            : '';
        $strItemsOrcamento .= sprintf(
          '
            *Item: %s*
            Qtd: %s %s
            %s
          ',
          $item_orcamento->descricao_produto_servico,
          $item_orcamento->Qt_Item,
          $exibir_valores,
          $item_orcamento->Ds_Observacao
        );
      }
        
    }


    $strDesconto = '';
    if (($consultaOrcamento[0]->valorDescontoFinal > 0)) {
      $strDesconto = sprintf('Desconto: R$ %s', number_format($consultaOrcamento[0]->valorDescontoFinal, 2, ",", ""));
    }

    $strEntrada = 0.00;
    if (($consultaOrcamento[0]->Vl_Entrada > 0)) {
      $strEntrada = sprintf('%s', number_format($consultaOrcamento[0]->Vl_Entrada, 2, ",", ""));
    }

    $strValorBruto = number_format($consultaOrcamento[0]->Vl_Bruto, 2, ",", "");
    $strSubTotal = number_format($consultaOrcamento[0]->vl_liquido, 2, ",", "");
    $strSaldo = number_format($consultaOrcamento[0]->vl_liquido - $consultaOrcamento[0]->Vl_Entrada, 2, ",", "");

    $labelPedido = $consultaOrcamento[0]->Id_Situacao <= 1 ? 'orçamento' : 'pedido';
    
    $arrVariaveisPedido = [
		  '[NUMERO_ORCAMENTO]' => $id_orcamento,
		  '[NOME_CLIENTE]' => $consultaOrcamento[0]->RazaoSocial,
		  '[DATA_ORCAMENTO]' => date('d/m/Y', strtotime($consultaOrcamento[0]->Dt_Orcamento)),
		  '[ITENS_ORCAMENTO]' => $strItemsOrcamento,
		  '[OBSERVACAO_CLIENTE]' => $consultaOrcamento[0]->Ds_Observacao_Pedido != '' ? 'Observação: ' . $consultaOrcamento[0]->Ds_Observacao_Pedido : '',
		  '[VALOR_BRUTO_PEDIDO]' => $strValorBruto,
		  '[SUBTOTAL]' => $strSubTotal,
		  '[VALOR_ENTRADA]' => $strEntrada,
		  '[VALOR_DESCONTO]' => $strDesconto,
		  '[VALOR_SALDO]' => $strSaldo,
		  '[LABEL_ORCAMENTO_PEDIDO]' => $labelPedido,
		  '[NOME_EMPRESA]' => $consultaEmpresa->NomeFantasia,
		  '[HORARIO_FUNCIONAMENTO]' => $consultaEmpresa->horario_funcionamento,
		  '[OBSERVACAO_EMPRESA]' => $consultaEmpresa->observacao_pedido,
      '[NOME_VENDEDOR]' => $consultaOrcamento[0]->NomeUsuarioCriado,
      '[DATA_PREVISTA_ENTREGA]' => $consultaOrcamento[0]->Dt_Prevista_Entrega != '' ? date('d/m/Y', strtotime($consultaOrcamento[0]->Dt_Prevista_Entrega)) : 'Não informado',
      '[PAGO]' => $consultaOrcamento[0]->Pago == '1' ? 'SIM' : 'NÃO'
	];
	
	preg_match_all($this->regex, $layout_mensagem_whatsapp, $ocorrenciasVariaveis, PREG_SET_ORDER, 0);
	
	foreach ($ocorrenciasVariaveis as $variavel) {
		$layout_mensagem_whatsapp = str_replace($variavel[0], $arrVariaveisPedido[$variavel[0]], $layout_mensagem_whatsapp);
	}    

    /**
     * Remove os espaço em branco no inicio de cada string
     * que é criado automaticamente por causa da formatação
     * usando o sprintf
     */
    $layout_mensagem_whatsapp = preg_replace('/^ */m', '', $layout_mensagem_whatsapp);
    
    // echo $layout_mensagem_whatsapp;exit;

    $arrDados = [
      'success' => '1',
      'msg' => urlencode($layout_mensagem_whatsapp),
      'destinatario' => '+55' . $numeroDestionatario,
      'id' => 0 //$message->sid
    ];

    if ($retorno == 'array') {
      return $arrDados;
    }

    echo json_encode($arrDados);

  } catch (Exception $e) {
    echo json_encode([
      'success' => '0',
      'msg' => $e->getMessage(),
      'id' => ''
    ]);    
  }

  }

  public function enviarMensagemIphone($id_orcamento = 0) {

    try {
      $foo = $this->enviarMensagem($id_orcamento, $retorno = 'array');

      $url = sprintf(
        'https://api.whatsapp.com/send?text=%s&phone=%s',
        $foo['msg'],
        str_replace('+','', $foo['destinatario'])
      );

      Header('Location: ' . $url);
      
      
    } catch (Exception $e) {
      echo json_encode([
        'success' => '0',
        'msg' => $e->getMessage(),
        'id' => ''
      ]);
    }

  }  
  
  public function enviarSituacao($id_orcamento = 0) {

    try {
    $objOrcamento = new Orcamento();
    $objEmpresa = new Empresas();
    $objSituacao = new Situacao();
    
    $consultaOrcamento = $objOrcamento->listarOrcamento($id_orcamento);
    
    $id_empresa = 1; //$consultaOrcamento[0]->id_empresa ?: 1;
    
    $consultaEmpresa = $objEmpresa->ListarEmpresa($id_empresa);
    $consultaSituacao = $objSituacao->listarSituacao($consultaOrcamento[0]->idSituacao);
    
    $layout_mensagem_whatsapp = $consultaSituacao[0]->notificacao ?: 'Olá, tudo bem? Aqui é da [NOME_EMPRESA], gostaria de avisar que seu pedido [NUMERO_ORCAMENTO] está na situação **[DESCRICAO_SITUACAO]**' ;
    
    if (sizeof($consultaOrcamento) == 0) {
      throw new ErrorException('Pedido ' . $id_orcamento . ' não encontrado');
    }
    
    $numeroDestionatario = preg_replace('/\D/m', '', $consultaOrcamento[0]->Telefone1);

    if ($numeroDestionatario == "") {
      throw new ErrorException('Número de destinatario inválido para o cliente ' . $consultaOrcamento[0]->RazaoSocial. 'Verifique o número cadastrado como Principal no cadastro do cliente');
    }
    
    $arrVariaveisPedido = [
		  '[NUMERO_ORCAMENTO]' => $id_orcamento,
		  '[NOME_CLIENTE]' => '',
		  '[DATA_ORCAMENTO]' => '',
		  '[ITENS_ORCAMENTO]' => '',
		  '[OBSERVACAO_CLIENTE]' => '',
		  '[VALOR_BRUTO_PEDIDO]' => '',
		  '[SUBTOTAL]' => '',
		  '[VALOR_ENTRADA]' => '',
		  '[VALOR_DESCONTO]' => '',
		  '[VALOR_SALDO]' => '',
		  '[LABEL_ORCAMENTO_PEDIDO]' => '',
		  '[NOME_EMPRESA]' => $consultaEmpresa->NomeFantasia,
		  '[DESCRICAO_SITUACAO]' => $consultaSituacao[0]->descricao
	];
	
	preg_match_all($this->regex, $layout_mensagem_whatsapp, $ocorrenciasVariaveis, PREG_SET_ORDER, 0);
	
	foreach ($ocorrenciasVariaveis as $variavel) {
		$layout_mensagem_whatsapp = str_replace($variavel[0], $arrVariaveisPedido[$variavel[0]], $layout_mensagem_whatsapp);
	}
    
    /**
     * Remove os espaço em branco no inicio de cada string
     * que é criado automaticamente por causa da formatação
     * usando o sprintf
     */
    $mensagem = preg_replace('/^ */m', '', $layout_mensagem_whatsapp);

    if ($_GET['iphone'] == 'true') {
      $url = sprintf(
        'https://api.whatsapp.com/send?text=%s&phone=%s',
        $mensagem,
        '+55' . $numeroDestionatario
      );
  
      Header('Location: ' . $url);
  
      return;
    }

    echo json_encode([
      'success' => '1',
      'msg' => urlencode($mensagem),
      'destinatario' => '+55' . $numeroDestionatario,
      'id' => 0 //$message->sid
    ]);

  } catch (Exception $e) {
    echo json_encode([
      'success' => '0',
      'msg' => $e->getMessage(),
      'id' => ''
    ]);    
  }

  }




}
