<?php
require_once './src/Model/Vendedor.php';
require_once './src/Controller/CommonController.php';
class VendedorController extends CommonController
{
    private $modulos = array();
    private $estados = array();
    private $classe = "Vendedor";
    private $breadcrumb = array();
    private $titulo_principal = "";
    public function __construct()
    {
        $common = new CommonController();
        $modulos = $common->getModulos();
        $this->modulos = $modulos;
        $modulo_posicao = array_search($this->classe, array_column($modulos, "modulo"));
        $this->titulo_principal = $modulos[$modulo_posicao];
        $this->breadcrumb = array("Maestria" => URL . "dashboard/index/", $this->titulo_principal["descricao"] => URL . $this->classe . "/listar/");
    }
    public function listar()
    {
        $common = new CommonController();
        $objVendedor = new Vendedor();
        $arrVendedor = $objVendedor->listarTodos();
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        $metodo = $acao = "editar";
        require "./src/View/Vendedor/vendedor_listar.php";
    }
    public function editar($handle)
    {
        $msg_sucesso = "";
        $metodo = "editar";
        $objVendedor = new Vendedor();
        if (isset($_POST) && !empty($_POST)) {
            $retorno = $objVendedor->editar($_POST);
            if ($retorno) {
                $msg_sucesso = "Vendedor alterada com sucesso.";
            }
        }
        $Vendedor = $objVendedor->listar($handle);
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        require "./src/View/Vendedor/vendedor_form.php";
    }
    public function cadastrar()
    {
        $msg_sucesso = "";
        $moedas = "";
        $metodo = "cadastrar";
        $objVendedor = new Vendedor();
        if (isset($_POST) && !empty($_POST)) {
            $retorno = $objVendedor->cadastrar($_POST);
            if ($retorno) {
                $msg_sucesso = "Vendedor cadastrada com sucesso.";
            }
            $Vendedor = $objVendedor->listar($retorno);
            $metodo = "editar";
        } else {
            $Vendedor = $objVendedor;
        }
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        require "./src/View/Vendedor/vendedor_form.php";
    }
    public function excluir($handle)
    {
        $msg_sucesso = "";
        $produtos = "";
        $metodo = "cadastrar";
        $objVendedor = new Vendedor();
        $objVendedor->excluir($handle);
        $_SESSION["tipoMensagem"] = "callout-success";
        $_SESSION["mensagem"] = "Vendedor excluído com sucesso.";
        Header("Location: " . URL . "Vendedor/listar/");
        exit();
    }
}
