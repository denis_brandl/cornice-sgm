<?php
require_once './src/Model/Account.php';
require_once './src/Controller/CommonController.php';
class AccountController extends CommonController {
private $modulos = array();
private $estados = array();
private $classe = "Account";
private $breadcrumb = array();
private $titulo_principal = "";
public function __construct() {
$common = new CommonController();	
$modulos = $common->getModulos();
$this->modulos = $modulos;
$modulo_posicao = array_search($this->classe,array_column($modulos,"modulo"));
$this->titulo_principal = $modulos[$modulo_posicao];
$this->breadcrumb = array("Maestria"=>URL."dashboard/index/",$this->titulo_principal["descricao"] => URL.$this->classe."/listar/");
}
public function listar() {
$common = new CommonController();
$objAccount = new Account();
$arrAccount = $objAccount->listarTodos();
$titulo_principal = $this->titulo_principal;
$breadcrumb = $this->breadcrumb;		
$modulos = $this->modulos;
$classe = $this->classe;
$metodo = $acao = "editar";
require "./src/View/Account/account_listar.php";
}
public function editar($handle) {
$msg_sucesso = "";
$metodo = "editar";
$objAccount = new Account();
if (isset($_POST) && !empty($_POST)) {
$retorno = $objAccount->editar($_POST);
if ($retorno) {
$msg_sucesso = "Account alterada com sucesso.";
}
}
$Account = $objAccount->listar($handle);
$titulo_principal = $this->titulo_principal;
$breadcrumb = $this->breadcrumb;		
$modulos = $this->modulos;
$classe = $this->classe;
require "./src/View/Account/account_form.php";
}
public function cadastrar() {
$msg_sucesso = "";	
$moedas = "";
$metodo = "cadastrar";	
$objAccount = new Account();
if (isset($_POST) && !empty($_POST)) {
$retorno = $objAccount->cadastrar($_POST);
if ($retorno) {
$msg_sucesso = "Account cadastrada com sucesso.";
}
$Account = $objAccount->listar($retorno);
$metodo = "editar";
} else {
$Account = $objAccount;
}
$titulo_principal = $this->titulo_principal;
$breadcrumb = $this->breadcrumb;		
$modulos = $this->modulos;
$classe = $this->classe;
require "./src/View/Account/account_form.php";	
}
public function excluir($handle) {
$msg_sucesso = "";
$produtos = "";
$metodo = "cadastrar";
$objAccount = new Account();
$objAccount->excluir($handle);
$_SESSION["tipoMensagem"] = "callout-success";
$_SESSION["mensagem"] = "Account excluído com sucesso.";
Header("Location: ".URL."Account/listar/");
exit();
}	          
}
