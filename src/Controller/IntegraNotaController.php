<?php

include_once '~/../src/Model/Cliente.php';
include_once '~/../src/Controller/CommonController.php';
include_once '~/../src/Controller/CurlController.php';
include_once '~/../src/Model/Empresas.php';
include_once '~/../src/Model/Orcamento.php';
include_once '~/../src/Model/ItemOrcamento.php';
include_once '~/../src/Model/FilaDocumentoFiscal.php';
include_once '~/../src/Model/Componente_Item_Orcamento.php';
include_once '~/../src/Model/Tributacao.php';

use CloudDfe\SdkPHP\Nfe;

class IntegraNotaController extends CommonController
{

  private $objCurl;
  private $objIntegraNota;
  private $objCommon;
  private $objFilaDocumentoFiscal;
  public function __construct()
  {
    $this->objCurl =  new CurlController();
    $this->objCommon = new CommonController();
    $this->objFilaDocumentoFiscal = new FilaDocumentoFiscal();
    $configuracoes = new Configuracoes();


  }

  public function processarFilaNotas()
  {
    $arrDocumentos = $this->objFilaDocumentoFiscal->consultaDocumentos(['tipo_documento' => 1]);
    if (count($arrDocumentos) == 0) {
      echo json_encode([
        'success' => '1',
        'msg' => 'Sem notas para processar'
      ]);
      exit;
    }

    foreach ($arrDocumentos as $documento) {
      $consultaNota = json_decode($this->consultarNota($documento->id_documento, 'consultaEmLote'));
      $status = 'PROCESSANDO';
      switch (true) {
        case in_array($consultaNota->codigo, range(201, 562)):
          $status = 'REJEITADO';
          break;

        case $consultaNota->codigo == '999':
          $status = 'REJEITADO';
          break;
        
        case $consultaNota->status == 'Autorizado':
          $status = 'Autorizado';
          break;
      }

      $this->objFilaDocumentoFiscal->editarFilaDocumentoFiscal(
        [
          'handle' => $documento->id_fila,
          'situacao' => $status,
          'retorno' => $consultaNota->msg
        ]
      );
    }

    echo json_encode([
      'success' => '1',
      'msg' => 'Nota pendente processada novamente!',
      'documento' => $arrDocumentos[0]->id_documento
    ]);
    
  }

  public function emitirDocumento($id_pedido = 0, $id_empresa = 1)
  {

    try {
      $objCliente = new Cliente();
      $objCommon = new CommonController();
      $objEmpresas = new Empresas();
      $objOrcamento = new Orcamento();
      $objItemOrcamento = new ItemOrcamento();
      $objMolduraItemOrcamento = new Moldura_Item_Orcamento();
      $objComponenteItemOrcamento = new Componente_Item_Orcamento();
      $objConfiguracoes = new Configuracoes;

      $ocultar_componentes_impressao_documento_fiscal = filter_var($objConfiguracoes->listarConfiguracao('ocultar_componentes_impressao_documento_fiscal')->valor, FILTER_VALIDATE_BOOLEAN);
      $observacao_fixa_nota = $objConfiguracoes->listarConfiguracao('observacao_fixa_nota')->valor;

      $parametros = $objCommon->validateGet('parametros');
      if ($parametros) {
        preg_match_all('/^(.*)=(\d)$/m', $parametros, $matches, PREG_SET_ORDER, 0);
        if (isset($matches[0][1]) && $matches[0][1] == 'IdEmpresa') {
          $id_empresa = $matches[0][2];
        }
      }

      $consultaEmpresa = $objEmpresas->ListarEmpresa($id_empresa);
      $consultaOrcamento = $objOrcamento->listarOrcamento($id_pedido);

      $this->alteraObjetoIntegraNota($id_empresa);

      $numero_sequencia_nf = $consultaEmpresa->ambiente_gerar_nota != '2' ? $consultaEmpresa->numero_sequencia_nf : date('zHmi');
      $serie_nf = $consultaEmpresa->ambiente_gerar_nota != '2' ? $consultaEmpresa->serie_nf : 889;

      $dadosNota = [];

      if (sizeof($consultaOrcamento) == 0) {
        throw new ErrorException('Pedido ' . $id_pedido . ' não encontrado');
      }

      $consultaDocumentoPedido = $this->objFilaDocumentoFiscal->listarFilaDocumentoFiscal(['id_pedido' => $id_pedido]);
      if (isset($consultaDocumentoPedido->id_fila) && in_array($consultaDocumentoPedido->situacao, ['CONCLUIDO', 'PROCESSANDO'])) {
        throw new ErrorException('Este pedido tem uma nota fiscal emitida. Status: ' . $consultaDocumentoPedido->situacao);
      }


      $itemsOrcamento = $objItemOrcamento->listarItemOrcamento($id_pedido);

      array_multisort(array_column($itemsOrcamento, 'Vl_Bruto'), SORT_DESC, $itemsOrcamento);

      $consultaCliente = $objCliente->listarCliente($consultaOrcamento[0]->Cd_Cliente);
      
      $arrItens = [];
      if (count($itemsOrcamento) == 0) {
        throw new ErrorException('Pedido sem produtos vinculados');
      }

      $vl_desconto = number_format($consultaOrcamento[0]->valorDescontoFinal, 2, ".", "") ?: 0;

      $quantidadeTotalProdutos = 0;
      foreach ($itemsOrcamento as $item) {
        $quantidadeTotalProdutos += $item->Qt_Item;
      }
      $quantidadeTotalProdutos = count($itemsOrcamento);
      
      $vl_desconto_por_item = 0;
      if ($quantidadeTotalProdutos > 0) {
        $vl_desconto_por_item =  floor(($vl_desconto / $quantidadeTotalProdutos)* 100)/100;
      }

      $fooBar = array_filter($itemsOrcamento, function($abc) use($vl_desconto_por_item) {
        if (($abc->Vl_Unitario * $abc->Qt_Item) > $vl_desconto_por_item) {
          return $abc;
        }
      });
      $total_itens_com_desconto = count($fooBar);

      if ($total_itens_com_desconto > 0) {
        $vl_desconto_por_item =  floor(($vl_desconto / $total_itens_com_desconto)* 100)/100;
      }

      $diferenca_desconto = 0;      
      if ($vl_desconto_por_item * count($fooBar) != $vl_desconto) {
        $diferenca_desconto = abs(round(($vl_desconto_por_item * $total_itens_com_desconto) - ($vl_desconto), 2));
      }

      $codigo_uf_nota = $consultaCliente[0]->codigo_uf > 0 ? $consultaCliente[0]->codigo_uf : 0;
      if ($codigo_uf_nota == 0) {
        throw new ErrorException('O endereço do cliente incorreto. Verifique no cadastro do cliente.');
      }

      if ($codigo_uf_nota != $consultaEmpresa->codigo_uf) {
        $codigo_uf_nota = 9999;
      }

      $soma_produtos = 0;
      $auxItem = 1;
      $soma_total_desconto = 0;
      $soma_valor_bruto = 0;
      $auxItensAplicadoDesconto = 0;
      foreach ($itemsOrcamento as $item_orcamento) {

        $vl_adicionais = (float) $item_orcamento->VL_ADICIONAIS;
        $vl_moldura = (float) $item_orcamento->Vl_Moldura;
        $valor_extras = 0; // $ocultar_componentes_impressao_documento_fiscal == false ? $vl_adicionais + $vl_moldura : 0;
        $qt_item = $item_orcamento->Qt_Item;

        $moldurasItemOrcamento = $objMolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $id_pedido, $codigo_uf_nota, 'Vl_Unitario DESC');
        
        $componentesItemOrcamento = [];
        if ($ocultar_componentes_impressao_documento_fiscal == false) {
          $componentesItemOrcamento = $objComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $id_pedido, $codigo_uf_nota);
        }

        $valor_extra_unitario = $valor_extras;

        $arrayProdutos = array_merge($moldurasItemOrcamento, $componentesItemOrcamento);

        // array_multisort(array_column($arrayProdutos, 'Vl_Unitario'), SORT_DESC, $arrayProdutos);

        if (!isset($arrayProdutos[0])) {
          throw new ErrorException('Nenhum produto foi vinculado ao item ' . $item_orcamento->Cd_Item_Orcamento . ' - ' . $item_orcamento->descricao);
        }

        $produto = $arrayProdutos[0];

        $vl_unitario = $item_orcamento->Vl_Unitario;
        $arrIcms["situacao_tributaria"] = '400';
        if ($produto->cst > 0) {
          $arrIcms["situacao_tributaria"] = $produto->cst;
        }

        if ($produto->aliquotaIcms > 0) {
          $arrIcms["aliquota"] = (string) $produto->aliquotaIcms;
        }

        $arrPis = [];
        $arrPis["situacao_tributaria"] = (string) $produto->cstpis ?: '99';
        $arrPis["aliquota"] = (float) $produto->aliquotaPis;
        $arrPis["valor_base_calculo"] = '0.00';
        $arrPis["valor"] = '0.00';

        $arrConfins = [];
        $arrConfins["situacao_tributaria"] = (string) $produto->cstconfins > 0 ? $produto->cstconfins : '99';
        $arrConfins["aliquota"] = (float) $produto->aliquotaConfins;
        $arrConfins["valor_base_calculo"] = '0.00';
        $arrConfins["valor"] = '0.00';

        $cfop = (int) $produto->cfop > 0 ? (string) $produto->cfop : '5102';
        $codigo_ncm = (int) $produto->codigo_ncm > 0 ? (string) $produto->codigo_ncm : '73239300';
        $soma_produtos += ($vl_unitario * $qt_item);

        $desconto_item = $auxItensAplicadoDesconto < $total_itens_com_desconto ? floor(($vl_desconto_por_item + $diferenca_desconto)* 100)/100 : 0;
        $auxItensAplicadoDesconto++;

        $soma_total_desconto += $desconto_item;

        $valor_bruto = ($vl_unitario + $valor_extra_unitario) * $qt_item;
        $soma_valor_bruto += $valor_bruto;

        $arrItens[] = [
          'numero_item' => (string) $auxItem,
          'codigo_produto' => (string) $auxItem . ($produto->NovoCodigo !=  '' ? $produto->NovoCodigo : $produto->CodigoProduto),
          'descricao' => substr(trim($produto->DescricaoProduto), 0, 120),
          'codigo_ncm' => $codigo_ncm,
          'cfop' => $codigo_ncm != '87032310' ? $cfop : '5551',
          'unidade_comercial' => 'UN',
          'quantidade_comercial' => $qt_item,
          'valor_unitario_comercial' => $item_orcamento->Vl_Unitario,
          'valor_bruto' => $valor_bruto,
          'valor_desconto' => $desconto_item,
          'inclui_no_total' => '1',
          'origem' => '0',
          'imposto' => [
            'icms' => $arrIcms,
            'pis' => $arrPis,
            'cofins' => $arrConfins
          ]
        ];

        $vl_desconto = 0;
        $diferenca_desconto = 0;
        $valor_extra_unitario = 0;
        $auxItem++;
      }

      if ($objCommon->onlyNumber($consultaCliente[0]->CGC) == '') {
        throw new ErrorException('Documento (CNPJ ou CPF) inválido. Verifique no cadastro do cliente.');
      }

      $dadosNota = [
        'numero' => $numero_sequencia_nf !== '' ? (string) $numero_sequencia_nf : '1',
        'serie' => $serie_nf !== '' ? (string) $serie_nf : '1',
        'data_emissao' => date('Y-m-d\TH:i:s-03:00'),
        'presenca_comprador' => '1',
        'consumidor_final'  => '1',
        'natureza_operacao' => 'VENDA DE PRODUTOS',
        'tipo_operacao' => '1',
        'finalidade_emissao' => '1',
        'destinatario' => [
          'cpf' => $consultaCliente[0]->TipoCliente == 'F' ? $objCommon->onlyNumber($consultaCliente[0]->CGC) : '',
          'cnpj' => $consultaCliente[0]->TipoCliente == 'J' ? $objCommon->onlyNumber($consultaCliente[0]->CGC) : '',
          'nome' => $consultaCliente[0]->RazaoSocial,
          'email' => $consultaCliente[0]->EMail !== NULL ? $consultaCliente[0]->EMail : '',
          'indicador_inscricao_estadual' => $consultaCliente[0]->TipoCliente == 'J' && $consultaCliente[0]->InscricaoEstadual != '' && strtoupper($consultaCliente[0]->InscricaoEstadual) != 'ISENTO' ? '1' : ( $consultaCliente[0]->TipoCliente == 'J' && strtoupper($consultaCliente[0]->InscricaoEstadual) != 'ISENTO' ? '2' : '9' ),
          'inscricao_estadual' => $consultaCliente[0]->InscricaoEstadual != '' && strtoupper($consultaCliente[0]->InscricaoEstadual) != 'ISENTO' ? $objCommon->onlyNumber($consultaCliente[0]->InscricaoEstadual) : '',
          'endereco' => [
            'logradouro' => sprintf('%s', $consultaCliente[0]->Endereco),
            'numero' => $consultaCliente[0]->numeroEndereco,
            'bairro' => $consultaCliente[0]->Bairro,
            'codigo_municipio' => $consultaCliente[0]->Cidade,
            'nome_municipio' => $consultaCliente[0]->nomeMunicipio,
            'uf' => $consultaCliente[0]->Estado,
            'cep' => $objCommon->onlyNumber($consultaCliente[0]->CEP),
            'telefone' => $objCommon->onlyNumber($consultaCliente[0]->Telefone1)
          ],
        ],
        'itens' => $arrItens,
        'pagamento' => [
          'formas_pagamento' =>
          [
            [
              'meio_pagamento' => '01',
              'valor' => (float) round(($consultaOrcamento[0]->Vl_Bruto - $consultaOrcamento[0]->valorDescontoFinal), 2)
            ]
          ]
        ],
        'frete' => [
          'modalidade_frete' => '0'
        ],
        'informacoes_adicionais_contribuinte' => $this->tirarAcentos($consultaOrcamento[0]->Ds_Observacao_Pedido != 'Venda de veiculo usado' ? 'Pedido: ' . $id_pedido . ' * ' . $consultaOrcamento[0]->Ds_Observacao_Pedido . $observacao_fixa_nota : $consultaOrcamento[0]->Ds_Observacao_Pedido),
        'debug' => [
          'soma_total_desconto' => $soma_total_desconto,
          'soma_valor_bruto' => $soma_valor_bruto
        ]
      ];


      if ($this->validateGet('debug') == true) {
        echo "<pre>".json_encode($dadosNota, JSON_PRETTY_PRINT)."</pre>";
        exit;
      }

      $retorno = $this->objIntegraNota->cria($dadosNota);

      $chave = isset($retorno->chave) ? $retorno->chave : '';
      $codigo_retorno = $retorno->codigo;

      if (array_key_exists($codigo_retorno, $this->objCommon->arrRejeicoesIntegrNotas)) {
        if (isset($retorno->erros)) {
          throw new ErrorException(json_encode($retorno->erros));
        }

        $chave = isset($retorno->chave) ? $retorno->chave : '';
        throw new ErrorException(str_replace('%CHAVE%',$chave, $this->objCommon->arrRejeicoesIntegrNotas[$codigo_retorno]));
      }

      if (array_key_exists($codigo_retorno, $this->objCommon->arrRejeicoesSefaz)) {
        throw new ErrorException(json_encode($retorno->erros));
      }      

      if (in_array($retorno->codigo, [5005])) {
        throw new ErrorException('Emitente sem o cadastro da Inscrição Estadual');
      }

      if (in_array($retorno->codigo, [5001, 5002])) {
        throw new ErrorException(json_encode($retorno->erros));
      }

      if (in_array($retorno->codigo, [5014])) {
        throw new ErrorException('Não encontrado nenhum certificado para este emitente');
      }      

      /**
       * Verifica se já foi gerado uma nota para esse pedido
       */
      if ($codigo_retorno === '5008') {
      }

      if ($retorno->sucesso) {
        $this->objFilaDocumentoFiscal->cadastrarFilaDocumentoFiscal(
          [
            'id_documento' => $chave,
            'id_pedido' => $id_pedido,
            'payload' =>  json_encode($dadosNota),
            'retorno' => json_encode($retorno->mensagem, JSON_NUMERIC_CHECK),
            'id_empresa' => $id_empresa,
            'id_usuario_criacao' => $_SESSION['handle'],
            'data_criacao' => $this->objFilaDocumentoFiscal->data_criacao,
            'situacao' => 'PROCESSANDO',
            'id_tipo_documento' => 1
          ]
        );

        $objEmpresas->editarEmpresa(
          [
            'handle' => $consultaEmpresa->CodigoEmpresa,
            'numero_sequencia_nf' => $consultaEmpresa->ambiente_gerar_nota != '2' ? ((int) $numero_sequencia_nf + 1) : $consultaEmpresa->numero_sequencia_nf
          ]
        );
      }

      echo json_encode(
        [
          'success' => '1',
          'msg' => $retorno->mensagem,
          'id_documento' => $chave,
          'codigo_retorno' => $codigo_retorno
        ]
      );
    } catch (Exception $e) {

      $arrExcecao = json_decode($e->getMessage(), true);

      if (!is_null($arrExcecao)) {
        $arrRespostaErro = [];
        foreach ($arrExcecao as $erro) {
          $arrRespostaErro[] = sprintf('%s - %s - %s', $erro['campo'], $erro['erro'], $erro['descricao']);
        }

        echo json_encode(
          [
            'success' => '0',
            'msg' => $arrRespostaErro,
            'codigo_retorno' => $codigo_retorno
          ]
        );
        return;
      }

      echo json_encode(
        [
          'success' => '0',
          'msg' => $e->getMessage()
        ]
      );
    }
  }

  public function emitirDevolucao($id_nota = 0)
  {

    try {

      $objCliente = new Cliente();
      $objCommon = new CommonController();
      $objEmpresas = new Empresas();
      $objOrcamento = new Orcamento();
      $objItemOrcamento = new ItemOrcamento();
      $objMolduraItemOrcamento = new Moldura_Item_Orcamento();
      $objComponenteItemOrcamento = new Componente_Item_Orcamento();
      $objConfiguracoes = new Configuracoes;

      $ocultar_componentes_impressao_documento_fiscal = filter_var($objConfiguracoes->listarConfiguracao('ocultar_componentes_impressao_documento_fiscal')->valor, FILTER_VALIDATE_BOOLEAN);
      $observacao_fixa_nota = $objConfiguracoes->listarConfiguracao('observacao_fixa_nota')->valor;

      $consultaDocumentoPedido = $this->objFilaDocumentoFiscal->listarFilaDocumentoFiscal(['id_documento' => $id_nota]);

      $id_pedido = $consultaDocumentoPedido->id_pedido;

      $parametros = $objCommon->validateGet('parametros');
      if ($parametros) {
        echo 111;exit;
        preg_match_all('/^(.*)=(\d)$/m', $parametros, $matches, PREG_SET_ORDER, 0);
        if (isset($matches[0][1]) && $matches[0][1] == 'IdEmpresa') {
          $id_empresa = $matches[0][2];
        }
      }

      $consultaOrcamento = $objOrcamento->listarOrcamento($id_pedido);
      $id_empresa = $consultaOrcamento[0]->id_empresa;
      $consultaEmpresa = $objEmpresas->ListarEmpresa($id_empresa);

      $this->alteraObjetoIntegraNota($id_empresa);

      $numero_sequencia_nf = $consultaEmpresa->ambiente_gerar_nota != '2' ? $consultaEmpresa->numero_sequencia_nf : date('zHmi');
      $serie_nf = $consultaEmpresa->ambiente_gerar_nota != '2' ? $consultaEmpresa->serie_nf : 889;

      $dadosNota = [];

      if (sizeof($consultaOrcamento) == 0) {
        throw new ErrorException('Pedido ' . $id_pedido . ' não encontrado');
      }

      // $consultaDocumentoPedido = $this->objFilaDocumentoFiscal->listarFilaDocumentoFiscal(['id_pedido' => $id_pedido]);
      // if (isset($consultaDocumentoPedido->id_fila) && in_array($consultaDocumentoPedido->situacao, ['CONCLUIDO', 'PROCESSANDO'])) {
      //   throw new ErrorException('Este pedido tem uma nota fiscal emitida. Status: ' . $consultaDocumentoPedido->situacao);
      // }


      $itemsOrcamento = $objItemOrcamento->listarItemOrcamento($id_pedido);

      array_multisort(array_column($itemsOrcamento, 'Vl_Bruto'), SORT_DESC, $itemsOrcamento);

      $consultaCliente = $objCliente->listarCliente($consultaOrcamento[0]->Cd_Cliente);
      
      $arrItens = [];
      if (count($itemsOrcamento) == 0) {
        throw new ErrorException('Pedido sem produtos vinculados');
      }

      $vl_desconto = number_format($consultaOrcamento[0]->valorDescontoFinal, 2, ".", "") ?: 0;

      $quantidadeTotalProdutos = 0;
      foreach ($itemsOrcamento as $item) {
        $quantidadeTotalProdutos += $item->Qt_Item;
      }
      $quantidadeTotalProdutos = count($itemsOrcamento);
      
      $vl_desconto_por_item = 0;
      if ($quantidadeTotalProdutos > 0) {
        $vl_desconto_por_item =  floor(($vl_desconto / $quantidadeTotalProdutos)* 100)/100;
      }

      $fooBar = array_filter($itemsOrcamento, function($abc) use($vl_desconto_por_item) {
        if (($abc->Vl_Unitario * $abc->Qt_Item) > $vl_desconto_por_item) {
          return $abc;
        }
      });
      $total_itens_com_desconto = count($fooBar);

      if ($total_itens_com_desconto > 0) {
        $vl_desconto_por_item =  floor(($vl_desconto / $total_itens_com_desconto)* 100)/100;
      }

      $diferenca_desconto = 0;      
      if ($vl_desconto_por_item * count($fooBar) != $vl_desconto) {
        $diferenca_desconto = abs(round(($vl_desconto_por_item * $total_itens_com_desconto) - ($vl_desconto), 2));
      }

      $codigo_uf_nota = $consultaCliente[0]->codigo_uf > 0 ? $consultaCliente[0]->codigo_uf : 0;
      if ($codigo_uf_nota == 0) {
        throw new ErrorException('O endereço do cliente incorreto. Verifique no cadastro do cliente.');
      }

      if ($codigo_uf_nota != $consultaEmpresa->codigo_uf) {
        $codigo_uf_nota = 9999;
      }

      $soma_produtos = 0;
      $auxItem = 1;
      $soma_total_desconto = 0;
      $soma_valor_bruto = 0;
      $auxItensAplicadoDesconto = 0;
      foreach ($itemsOrcamento as $item_orcamento) {

        $vl_adicionais = (float) $item_orcamento->VL_ADICIONAIS;
        $vl_moldura = (float) $item_orcamento->Vl_Moldura;
        $valor_extras = 0; // $ocultar_componentes_impressao_documento_fiscal == false ? $vl_adicionais + $vl_moldura : 0;
        $qt_item = $item_orcamento->Qt_Item;

        $moldurasItemOrcamento = $objMolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $id_pedido, $codigo_uf_nota, 'Vl_Unitario DESC');
        
        $componentesItemOrcamento = [];
        if ($ocultar_componentes_impressao_documento_fiscal == false) {
          $componentesItemOrcamento = $objComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $id_pedido, $codigo_uf_nota);
        }

        $valor_extra_unitario = $valor_extras;

        $arrayProdutos = array_merge($moldurasItemOrcamento, $componentesItemOrcamento);

        // array_multisort(array_column($arrayProdutos, 'Vl_Unitario'), SORT_DESC, $arrayProdutos);

        if (!isset($arrayProdutos[0])) {
          throw new ErrorException('Nenhum produto foi vinculado ao item ' . $item_orcamento->Cd_Item_Orcamento . ' - ' . $item_orcamento->descricao);
        }

        $produto = $arrayProdutos[0];

        $vl_unitario = $item_orcamento->Vl_Unitario;
        $arrIcms["situacao_tributaria"] = '400';
        if ($produto->cst > 0) {
          $arrIcms["situacao_tributaria"] = $produto->cst;
        }

        if ($produto->aliquotaIcms > 0) {
          $arrIcms["aliquota"] = (string) $produto->aliquotaIcms;
        }

        $arrPis = [];
        $arrPis["situacao_tributaria"] = (string) $produto->cstpis ?: '99';
        $arrPis["aliquota"] = (float) $produto->aliquotaPis;
        $arrPis["valor_base_calculo"] = '0.00';
        $arrPis["valor"] = '0.00';

        $arrConfins = [];
        $arrConfins["situacao_tributaria"] = (string) $produto->cstconfins > 0 ? $produto->cstconfins : '99';
        $arrConfins["aliquota"] = (float) $produto->aliquotaConfins;
        $arrConfins["valor_base_calculo"] = '0.00';
        $arrConfins["valor"] = '0.00';

        $cfop = (int) $produto->cfop > 0 ? (string) $produto->cfop : '5102';
        $codigo_ncm = (int) $produto->codigo_ncm > 0 ? (string) $produto->codigo_ncm : '73239300';
        $soma_produtos += ($vl_unitario * $qt_item);

        $desconto_item = $auxItensAplicadoDesconto < $total_itens_com_desconto ? floor(($vl_desconto_por_item + $diferenca_desconto)* 100)/100 : 0;
        $auxItensAplicadoDesconto++;

        $soma_total_desconto += $desconto_item;

        $valor_bruto = ($vl_unitario + $valor_extra_unitario) * $qt_item;
        $soma_valor_bruto += $valor_bruto;

        $arrItens[] = [
          'numero_item' => (string) $auxItem,
          'codigo_produto' => (string) $auxItem . ($produto->NovoCodigo !=  '' ? $produto->NovoCodigo : $produto->CodigoProduto),
          'descricao' => substr(trim($produto->DescricaoProduto), 0, 120),
          'codigo_ncm' => $codigo_ncm,
          'cfop' => '1202',
          'unidade_comercial' => 'UN',
          'quantidade_comercial' => $qt_item,
          'valor_unitario_comercial' => $item_orcamento->Vl_Unitario,
          'valor_bruto' => $valor_bruto,
          'valor_desconto' => $desconto_item,
          'inclui_no_total' => '1',
          'origem' => '0',
          'imposto' => [
            'icms' => $arrIcms,
            'pis' => $arrPis,
            'cofins' => $arrConfins
          ]
        ];

        $vl_desconto = 0;
        $diferenca_desconto = 0;
        $valor_extra_unitario = 0;
        $auxItem++;
      }

      if ($objCommon->onlyNumber($consultaCliente[0]->CGC) == '') {
        throw new ErrorException('Documento (CNPJ ou CPF) inválido. Verifique no cadastro do cliente.');
      }

      $dadosNota = [
        'numero' => $numero_sequencia_nf !== '' ? (string) $numero_sequencia_nf : '1',
        'serie' => $serie_nf !== '' ? (string) $serie_nf : '1',
        'data_emissao' => date('Y-m-d\TH:i:s-03:00'),
        'presenca_comprador' => '1',
        'consumidor_final'  => '1',
        'natureza_operacao' => 'VENDA DE PRODUTOS',
        'tipo_operacao' => '0',
        'finalidade_emissao' => '4',
        'destinatario' => [
          'cpf' => $consultaCliente[0]->TipoCliente == 'F' ? $objCommon->onlyNumber($consultaCliente[0]->CGC) : '',
          'cnpj' => $consultaCliente[0]->TipoCliente == 'J' ? $objCommon->onlyNumber($consultaCliente[0]->CGC) : '',
          'nome' => $consultaCliente[0]->RazaoSocial,
          'email' => $consultaCliente[0]->EMail !== NULL ? $consultaCliente[0]->EMail : '',
          'indicador_inscricao_estadual' => $consultaCliente[0]->TipoCliente == 'J' && $consultaCliente[0]->InscricaoEstadual != '' && strtoupper($consultaCliente[0]->InscricaoEstadual) != 'ISENTO' ? '1' : ( $consultaCliente[0]->TipoCliente == 'J' && strtoupper($consultaCliente[0]->InscricaoEstadual) != 'ISENTO' ? '2' : '9' ),
          'inscricao_estadual' => $consultaCliente[0]->InscricaoEstadual != '' && strtoupper($consultaCliente[0]->InscricaoEstadual) != 'ISENTO' ? $objCommon->onlyNumber($consultaCliente[0]->InscricaoEstadual) : '',
          'endereco' => [
            'logradouro' => sprintf('%s', $consultaCliente[0]->Endereco),
            'numero' => $consultaCliente[0]->numeroEndereco,
            'bairro' => $consultaCliente[0]->Bairro,
            'codigo_municipio' => $consultaCliente[0]->Cidade,
            'nome_municipio' => $consultaCliente[0]->nomeMunicipio,
            'uf' => $consultaCliente[0]->Estado,
            'cep' => $objCommon->onlyNumber($consultaCliente[0]->CEP),
            'telefone' => $objCommon->onlyNumber($consultaCliente[0]->Telefone1)
          ],
        ],
        'itens' => $arrItens,
        'pagamento' => [
          'formas_pagamento' =>
          [
            [
              'meio_pagamento' => '01',
              'valor' => (float) round(($consultaOrcamento[0]->Vl_Bruto - $consultaOrcamento[0]->valorDescontoFinal), 2)
            ]
          ]
        ],
        'frete' => [
          'modalidade_frete' => '0'
        ],
        'informacoes_adicionais_contribuinte' => $this->tirarAcentos('Pedido: ' . $id_pedido . ' * ' . $consultaOrcamento[0]->Ds_Observacao_Pedido . $observacao_fixa_nota),
        'notas_referenciadas' => [
          ['nfe' => ['chave' => $id_nota]]
        ],
        'debug' => [
          'soma_total_desconto' => $soma_total_desconto,
          'soma_valor_bruto' => $soma_valor_bruto
        ]
      ];


      if ($this->validateGet('debug') == true) {
        echo "<pre>".json_encode($dadosNota, JSON_PRETTY_PRINT)."</pre>";
        exit;
      }

      $retorno = $this->objIntegraNota->cria($dadosNota);

      $chave = isset($retorno->chave) ? $retorno->chave : '';
      $codigo_retorno = $retorno->codigo;

      if (array_key_exists($codigo_retorno, $this->objCommon->arrRejeicoesIntegrNotas)) {
        if (isset($retorno->erros)) {
          throw new ErrorException(json_encode($retorno->erros));
        }

        $chave = isset($retorno->chave) ? $retorno->chave : '';
        throw new ErrorException(str_replace('%CHAVE%',$chave, $this->objCommon->arrRejeicoesIntegrNotas[$codigo_retorno]));
      }

      if (array_key_exists($codigo_retorno, $this->objCommon->arrRejeicoesSefaz)) {
        throw new ErrorException(json_encode($retorno->erros));
      }      

      if (in_array($retorno->codigo, [5005])) {
        throw new ErrorException('Emitente sem o cadastro da Inscrição Estadual');
      }

      if (in_array($retorno->codigo, [5001, 5002])) {
        throw new ErrorException(json_encode($retorno->erros));
      }

      if (in_array($retorno->codigo, [5014])) {
        throw new ErrorException('Não encontrado nenhum certificado para este emitente');
      }      

      /**
       * Verifica se já foi gerado uma nota para esse pedido
       */
      if ($codigo_retorno === '5008') {
      }

      if ($retorno->sucesso) {

        $this->objFilaDocumentoFiscal->editarFilaDocumentoFiscal(
          [
            'id_fila' => $consultaDocumentoPedido->id_fila,
            'devolucao' => 2
          ]
        );

        $this->objFilaDocumentoFiscal->cadastrarFilaDocumentoFiscal(
          [
            'id_documento' => $chave,
            'id_pedido' => $id_pedido,
            'payload' =>  json_encode($dadosNota),
            'retorno' => json_encode($retorno->mensagem, JSON_NUMERIC_CHECK),
            'id_empresa' => $id_empresa,
            'id_usuario_criacao' => $_SESSION['handle'],
            'data_criacao' => $this->objFilaDocumentoFiscal->data_criacao,
            'situacao' => 'PROCESSANDO',
            'id_tipo_documento' => 1,
            'devolucao' => 1
          ]
        );

        $objEmpresas->editarEmpresa(
          [
            'handle' => $consultaEmpresa->CodigoEmpresa,
            'numero_sequencia_nf' => $consultaEmpresa->ambiente_gerar_nota != '2' ? ((int) $numero_sequencia_nf + 1) : $consultaEmpresa->numero_sequencia_nf
          ]
        );
      }

      echo json_encode(
        [
          'success' => '1',
          'msg' => $retorno->mensagem,
          'id_documento' => $chave,
          'codigo_retorno' => $codigo_retorno
        ]
      );
    } catch (Exception $e) {

      $arrExcecao = json_decode($e->getMessage(), true);

      if (!is_null($arrExcecao)) {
        $arrRespostaErro = [];
        foreach ($arrExcecao as $erro) {
          $arrRespostaErro[] = sprintf('%s - %s - %s', $erro['campo'], $erro['erro'], $erro['descricao']);
        }

        echo json_encode(
          [
            'success' => '0',
            'msg' => $arrRespostaErro,
            'codigo_retorno' => $codigo_retorno
          ]
        );
        return;
      }

      echo json_encode(
        [
          'success' => '0',
          'msg' => $e->getMessage()
        ]
      );
    }
  }

  public function consultarNota($idNota = 0, $origemRequisicao = '')
  {

    $consultaDocumento = $this->objFilaDocumentoFiscal->listarPorDocumento($idNota);

    // print_r($consultaDocumento);exit;

    $this->alteraObjetoIntegraNota(null, $idNota);

    $payload = [
      'chave' => $idNota
    ];

    $consultaNota = $this->objIntegraNota->consulta($payload);

    $codigo_retorno = $consultaNota->codigo;

    $status = 'PROCESSANDO';
    $mensagem_retorno = $consultaNota->mensagem;
    if (array_key_exists($codigo_retorno, $this->objCommon->arrRejeicoesSefaz)) {
      if ($codigo_retorno == 778) {
        $item_ncm_errado = (int) preg_replace('/Rejeicao: Informado NCM inexistente \[NCM:.*\], \[nItem:(\d)\]\./m', '$1', $consultaNota->mensagem);
        $arrItens = json_decode($consultaDocumento->payload, true);
        $mensagem_retorno = sprintf('Rejeicao: Informado NCM inexistente: %s no item %s', $arrItens['itens'][$item_ncm_errado]['codigo_ncm'] , $arrItens['itens'][$item_ncm_errado]['descricao']);
      }
      
      $status = 'REJEITADO';
    }

    if ((int) $consultaNota->sucesso === 1 && $consultaNota->codigo == '100') {
      $status  = 'Autorizado';
    }

    $this->objFilaDocumentoFiscal->editarFilaDocumentoFiscal(
      [
        'handle' => $consultaDocumento->id_fila,
        'situacao' => $status,
        'retorno' => $mensagem_retorno,
        'link_xml' => isset($consultaNota->xml) ? $consultaNota->xml : '',
        'link_pdf' => isset($consultaNota->pdf) ? $consultaNota->pdf : '',
        'numero_nota' => isset($consultaNota->numero) ? $consultaNota->numero : 0
      ]
    );

    $retorno = json_encode([
      'success' => (int) $consultaNota->sucesso,
      'msg' => $mensagem_retorno,
      'codigo' => $consultaNota->codigo,
      'status' => $status
    ]);



    if ($origemRequisicao === '') {
      echo $retorno;
      return;
    }


    return $retorno;
  }

  public function downloadNota($idNota = 0)
  {

    $this->alteraObjetoIntegraNota(null, $idNota);
    $payload = [
      'chave' => $idNota
    ];
    $retorno = $this->objIntegraNota->pdf($payload);

    $mensagem = $retorno->codigo == 5004 ? 'O emitente não contratou acesso a NF-e' : $retorno->mensagem;

    $status = $retorno->sucesso;

    if ((bool) !$status) {
      echo sprintf('<p>Erro: <br> %s </p>', $mensagem);
      exit;
    }

    header('Content-type: application/pdf');
    echo base64_decode($retorno->pdf);
    exit;
  }

  public function cancelarNota()
  {
    
    $idNota = $_POST['idNota'];
    $motivo = $_POST['motivo'];

    $this->alteraObjetoIntegraNota(null, $idNota);
    
    $consultaDocumento = $this->objFilaDocumentoFiscal->listarPorDocumento($idNota);

    $payload = [
      'chave' => $idNota,
      'justificativa' => $motivo
    ];

    $retorno = $this->objIntegraNota->cancela($payload);

    $status = (int) $retorno->sucesso ? 'Cancelado' : 'ERRO';

    $this->objFilaDocumentoFiscal->editarFilaDocumentoFiscal(
      [
        'handle' => $consultaDocumento->id_fila,
        'situacao' => $status,
        'retorno' => $retorno->mensagem
      ]
    );


    echo json_encode([
      'success' => (int) $retorno->sucesso,
      'msg' => $retorno->mensagem,
      'codigo' => $retorno->codigo,
      'status' => $status
    ]);    


  }

  public function downloadXML($idNota = 0)
  {

    $payload = [
      'chave' => $idNota
    ];

    $this->alteraObjetoIntegraNota(null, $idNota);

    $retorno = $this->objIntegraNota->consulta($payload);

    $status = $retorno->sucesso;

    if ($status != '1') {
      echo '<h1> Erro ao exibir o XML </h1>';
      $mensagem_erros = '';
      if (!empty($retorno->erros)) {
        $mensagem_erros = json_encode($retorno->erros);
      }
      echo sprintf('<p>%s <br> %s</p>', $retorno->mensagem, $mensagem_erros);
      exit;
    }

    header('Content-type: text/xml');
    header('Content-Disposition: attachment; filename="' . $retorno->numero . '.xml"');
    echo base64_decode($retorno->xml);
    // echo sprintf('<textarea style="height:100%%;width:100%%">%s</textarea>', $dom->saveXML());
    exit;
  }

  public function alteraObjetoIntegraNota($id_empresa = null, $id_documento = null) {


    if (is_null($id_empresa)) {
      $consultaDocumento = $this->objFilaDocumentoFiscal->listarPorDocumento($id_documento);

      $id_empresa = $consultaDocumento->id_empresa;
    }

    $objEmpresas = new Empresas();
    $empresa = $objEmpresas->ListarEmpresa($id_empresa);

    $token_integra_nota = $empresa->token_integra_nota;
    $ambiente_integra_nota = $empresa->ambiente_gerar_nota;

    $params = [
      'token' => $token_integra_nota,
      'ambiente' => $ambiente_integra_nota,
      'options' => [
        'debug' => false,
        'timeout' => 60,
        'port' => 443,
        'http_version' => CURL_HTTP_VERSION_NONE
      ]
    ];

    $this->objIntegraNota = new Nfe($params);
    
  }

  public function recuperarIdEmpresa() {
    $parametros = $this->objCommon->validateGet('parametros');
    if ($parametros) {
      preg_match_all('/^(.*)=(\d)$/m', $parametros, $matches, PREG_SET_ORDER, 0);
      if (isset($matches[0][1]) && $matches[0][1] == 'IdEmpresa') {
        return $matches[0][2];
      }
    }
    return null;
  }

}
