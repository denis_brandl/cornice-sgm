<?php
require_once './src/Model/Empresas.php';
require_once './src/Controller/CommonController.php';
class EmpresaController extends CommonController {

	private $modulos = array();
	private $estados = array();
	private $classe = 'Empresa';
	private $breadcrumb = array();
	private $titulo_principal = '';
	public $usuario_master = false;
	
	public function __construct() {
		$common = new CommonController();	
		$modulos = $common->getModulos();
		$estados = $common->getEstados();
		
		$this->modulos = $modulos;		
		$this->estados = $estados;
		
		$modulo_posicao = array_search($this->classe,array_column($modulos,'modulo'));
		$this->titulo_principal = $modulos[$modulo_posicao];		
		$this->breadcrumb = array('Maestria'=>URL.'dashboard/index/',$this->titulo_principal['descricao'] => URL.$this->classe.'/listar/');				
		$this->usuario_master = $common->usuario_master;
	}
	
	public function listar() {
		$common = new CommonController();

		$objEmpresa = new Empresas();
		$arrEmpresas = $objEmpresa->listarTodos();

		$estados = $this->estados;

		$titulo_principal = $this->titulo_principal;
		$breadcrumb = $this->breadcrumb;		
		$modulos = $this->modulos;
		$classe = $this->classe;
		$metodo = $acao = 'editar';
		require './src/View/Empresa/empresa_listar.php';
	}
	
	public function editar($handle) {
		$msg_sucesso = '';
		$metodo = 'editar';
		$objEmpresa = new Empresas();

		if (isset($_POST) && !empty($_POST)) {
			$retorno = $objEmpresa->editarEmpresa($_POST);
			if ($retorno) {
				$msg_sucesso = 'Empresa alterada com sucesso.';
			}
		}

    $empresa = $objEmpresa->ListarEmpresa($handle);

    $arrDemaisEmpresas = $objEmpresa->listarTodos();

		$titulo_principal = $this->titulo_principal;
		$breadcrumb = $this->breadcrumb;		
		$modulos = $this->modulos;
		$classe = $this->classe;
		require './src/View/Empresa/empresa_form.php';	    
		
	}
	
	public function cadastrar() {
		$msg_sucesso = '';	
		$moedas = '';
		$metodo = 'cadastrar';	
    	$objEmpresa = new Empresas();

		if (!$this->usuario_master) {
			Header('Location:' . URL . 'Empresa/listar/');
			exit;
		}

		if (isset($_POST) && !empty($_POST)) {
			$retorno = $objEmpresa->cadastrarEmpresa($_POST);
			if ($retorno) {
				$msg_sucesso = 'Empresa cadastrada com sucesso.';
			}
			$empresa = $objEmpresa->ListarEmpresa($retorno);
      $metodo = 'editar';
		} else {
			$empresa = $objEmpresa;
		}		

    $arrDemaisEmpresas = $objEmpresa->listarTodos();
		
		$titulo_principal = $this->titulo_principal;
		$breadcrumb = $this->breadcrumb;		
		$modulos = $this->modulos;
		$classe = $this->classe;
		require './src/View/Empresa/empresa_form.php';	
	}
}
?>
