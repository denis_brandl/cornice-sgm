
      <?php
        require_once './src/Model/Contas.php';
        require_once './src/Controller/CommonController.php';
        class ContasController extends CommonController {
          private $modulos = array();
          private $estados = array();
          private $classe = "Contas";
          private $breadcrumb = array();
          private $titulo_principal = "";

          public function __construct() {
            $common = new CommonController();	
            $modulos = $common->getModulos();

            $this->modulos = $modulos;

            $modulo_posicao = array_search($this->classe,array_column($modulos,"modulo"));
            $this->titulo_principal = $modulos[$modulo_posicao];
            $this->breadcrumb = array("Maestria"=>URL."dashboard/index/",$this->titulo_principal["descricao"] => URL.$this->classe."/listar/");
          }

          public function listar() {
            $common = new CommonController();

            $objContas = new Contas();
            $arrContas = $objContas->listarTodos();

            $titulo_principal = $this->titulo_principal;
            $breadcrumb = $this->breadcrumb;		
            $modulos = $this->modulos;
            $classe = $this->classe;
            $metodo = $acao = "editar";
            require "./src/View/Contas/contas_listar.php";
          }

          public function editar($handle) {
            $msg_sucesso = "";
            $metodo = "editar";
            $objContas = new Contas();

            if (isset($_POST) && !empty($_POST)) {
              $retorno = $objContas->editar($_POST);
              if ($retorno) {
                $msg_sucesso = "Contas alterada com sucesso.";
              }
            }

            $Contas = $objContas->listar($handle);

            $titulo_principal = $this->titulo_principal;
            $breadcrumb = $this->breadcrumb;		
            $modulos = $this->modulos;
            $classe = $this->classe;
            require "./src/View/Contas/contas_form.php";
          }

          public function cadastrar() {
            $msg_sucesso = "";	
            $moedas = "";
            $metodo = "cadastrar";	
            $objContas = new Contas();

            if (isset($_POST) && !empty($_POST)) {
              $retorno = $objContas->cadastrar($_POST);
              if ($retorno) {
                $msg_sucesso = "Contas cadastrada com sucesso.";
              }
              $Contas = $objContas->listar($retorno);
              $metodo = "editar";
            } else {
              $Contas = $objContas;
            }
            $titulo_principal = $this->titulo_principal;
            $breadcrumb = $this->breadcrumb;		
            $modulos = $this->modulos;
            $classe = $this->classe;
            require "./src/View/Contas/contas_form.php";	
          }

          public function excluir($handle) {
            $msg_sucesso = "";
            $produtos = "";
            $metodo = "cadastrar";

            $objContas = new Contas();
            $objContas->excluir($handle);
            $_SESSION["tipoMensagem"] = "callout-success";
            $_SESSION["mensagem"] = "Contas excluído com sucesso.";
            Header("Location: ".URL."Contas/listar/");
            exit();
          }	          
        }
    