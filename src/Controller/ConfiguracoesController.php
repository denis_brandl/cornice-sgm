<?php
require_once './src/Model/Configuracoes.php';
require_once './src/Model/Empresas.php';
require_once './src/Controller/CommonController.php';
require_once './src/Model/Situacao.php';
class ConfiguracoesController extends CommonController {

	private $modulos = array();
	private $estados = array();
	private $classe = 'Configuracoes';
	private $breadcrumb = array();
	private $titulo_principal = '';		
	public $usuario_master = false;
	
	public function __construct() {
		$configuracoes = new Configuracoes();
		$common = new CommonController();	
		$modulos = $common->getModulos();
		$estados = $common->getEstados();
		
		$this->modulos = $modulos;		
		$this->estados = $estados;
		
		$modulo_posicao = array_search($this->classe,array_column($modulos,'modulo'));
		$this->titulo_principal = $modulos[$modulo_posicao];		
		$this->breadcrumb = array('Maestria'=>URL.'dashboard/index/',$this->titulo_principal['descricao'] => URL.$this->classe.'/listar/');				
		$this->usuario_master = $common->usuario_master;
		
	}
	
	public function listar() {
		$configuracoes = new Configuracoes();
		$common = new CommonController();

		$empresas = new Empresas();
		$empresa = $empresas->listarEmpresa(1);
		$objSituacao = new Situacao();
		
		$consultaConfiguracoes = $configuracoes->listarTodos(0, 0, '', '', 1000, '');
		
		$arrConfiguracoes = [];
		
		foreach ($consultaConfiguracoes as $key) {
			switch ($key->nome) {
				case 'habilita_financeiro':
				case 'habilita_tributacao':
				case 'parceria_ruberti':
				case 'sistema_bloqueado':
				case 'desabilitar_controle_estoque':
				case 'considera_soma_total_perdas':
				case 'arredondar_medida_total_quadro':
				case 'relatorio_comissao_considerar_somente_pago':
				case 'habilitar_emissao_nf':
				case 'habilitar_emissao_nfs':
				case 'habilitar_emissao_nfc':
				case 'ocultar_componentes_impressao_documento_fiscal':
				case 'ocultar_componentes_impressao_pedido':
				case 'arredondar_valor_item':
				case 'exibir_medida_final_do_quadro':
				case 'exibir_total_custo_item':
				case 'permitir_informar_medidas_em_decimal':
				case 'exibir_valores_individuais':
					$arrConfiguracoes[] = array_merge((array) $key, ['tipo_campo' => 'select']);
					break;
				case 'configuracoes_email':
				case 'token_integra_nota':
					$arrConfiguracoes[] = array_merge((array) $key, ['tipo_campo' => 'textarea']);
					break;					
				default:
					$arrConfiguracoes[] = array_merge((array) $key, ['tipo_campo' => 'input']);
			}
		}
		
		
		
		$consultaLayoutMensagemWhatsapp = $configuracoes->listarConfiguracao('layout_mensagem_whatsapp');
		$layout_mensagem_whatsapp = $consultaLayoutMensagemWhatsapp->valor;
		
		$listaSituacoes = $objSituacao->listarTodos(0,0,'','',['idSituacao' => [3,4,5]]);

		$estados = $this->estados;

		$titulo_principal = $this->titulo_principal;
		$breadcrumb = $this->breadcrumb;		
		$modulos = $this->modulos;
		$classe = $this->classe;
		$metodo = $acao = 'editar';
		require './src/View/Configuracoes/configuracoes_form.php';
	}
	
	public function editar($handle) {
		$msg_sucesso = '';
		$metodo = 'editar';
		$configuracoes = new Configuracoes();
		$empresas = new Empresas();

		if (isset($_POST) && !empty($_POST)) {
			if (isset($_POST['configuracoesAvancadas'])) {
				$configuracoesAvancadas = $_POST['configuracoesAvancadas'];
				unset($_POST['configuracoesAvancadas']);
				$configuracoes->editarConfiguracao($configuracoesAvancadas);
			}
			$retorno = $empresas->editarEmpresa($_POST);
			if ($retorno) {
				$msg_sucesso = 'Configurações alteradas com sucesso.';
			}
		}
		
		Header('Location: '.URL.'Configuracoes/listar/');
		exit();
	}
	
	public function cadastrar() {
		$msg_sucesso = '';	
		$moedas = '';
		$metodo = 'cadastrar';	
		
		$configuracoes = new Configuracoes();

		if (isset($_POST) && !empty($_POST)) {
			$retorno = $configuracoes->cadastrarMoeda($_POST);
			if ($retorno) {
				$msg_sucesso = 'Moeda cadastrado com sucesso.';
			}
			$moedas = $configuracoes->listarMoeda($retorno);
		} else {
			$moedas = array($moeda);
		}		
				
		$configuracoes = new Configuracoes();	
		$unidades = $configuracoes->listarTodos();
		
		$grupo = new Grupo();	
		$grupos = $grupo->listarTodos();			
		
		
		$titulo_principal = $this->titulo_principal;
		$breadcrumb = $this->breadcrumb;		
		$modulos = $this->modulos;
		$classe = $this->classe;
		require './src/View/Moeda/moeda_form.php';	
	}
}
?>
