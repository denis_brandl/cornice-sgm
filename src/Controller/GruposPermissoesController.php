<?php
require_once './src/Model/GruposPermissoes.php';
require_once './src/Controller/CommonController.php';
class GruposPermissoesController extends CommonController
{
    private $modulos = array();
    private $estados = array();
    private $classe = "GruposPermissoes";
    private $breadcrumb = array();
    private $titulo_principal = "";
    public function __construct()
    {
        $common = new CommonController();
        $modulos = $common->getModulos();
        $this->modulos = $modulos;
        $modulo_posicao = array_search($this->classe, array_column($modulos, "modulo"));
        $this->titulo_principal = $modulos[$modulo_posicao];
        $this->breadcrumb = array("Maestria" => URL . "dashboard/index/", $this->titulo_principal["descricao"] => URL . $this->classe . "/listar/");
    }
    public function listar()
    {
        $common = new CommonController();
        $objGruposPermissoes = new GruposPermissoes();
        $arrGruposPermissoes = $objGruposPermissoes->listarTodos();
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        $metodo = $acao = "editar";
        require "./src/View/GruposPermissoes/grupos_permissoes_listar.php";
    }
    public function editar($handle)
    {
        $msg_sucesso = "";
        $metodo = "editar";
        $objGruposPermissoes = new GruposPermissoes();
        if (isset($_POST) && !empty($_POST)) {
            $retorno = $objGruposPermissoes->editar($_POST);
            if ($retorno) {
                $msg_sucesso = "GruposPermissoes alterada com sucesso.";
            }
        }
        $GruposPermissoes = $objGruposPermissoes->listar($handle);
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        require "./src/View/GruposPermissoes/grupos_permissoes_form.php";
    }
    public function cadastrar()
    {
        $msg_sucesso = "";
        $moedas = "";
        $metodo = "cadastrar";
        $objGruposPermissoes = new GruposPermissoes();
        if (isset($_POST) && !empty($_POST)) {
            $retorno = $objGruposPermissoes->cadastrar($_POST);
            if ($retorno) {
                $msg_sucesso = "GruposPermissoes cadastrada com sucesso.";
            }
            $GruposPermissoes = $objGruposPermissoes->listar($retorno);
            $metodo = "editar";
        } else {
            $GruposPermissoes = $objGruposPermissoes;
        }
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        require "./src/View/GruposPermissoes/grupos_permissoes_form.php";
    }
    public function excluir($handle)
    {
        $msg_sucesso = "";
        $produtos = "";
        $metodo = "cadastrar";
        $objGruposPermissoes = new GruposPermissoes();
        $objGruposPermissoes->excluir($handle);
        $_SESSION["tipoMensagem"] = "callout-success";
        $_SESSION["mensagem"] = "GruposPermissoes excluído com sucesso.";
        Header("Location: " . URL . "GruposPermissoes/listar/");
        exit();
    }
}
