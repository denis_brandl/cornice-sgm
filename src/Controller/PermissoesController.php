<?php
require_once './src/Model/Permissoes.php';
require_once './src/Controller/CommonController.php';
require_once './src/Model/GruposUsuario.php';
require_once './src/Model/GruposPermissoes.php';
require_once './src/Model/Permissoes.php';
require_once './src/Model/Situacao.php';

class PermissoesController extends CommonController
{
    private $modulos = array();
    private $estados = array();
    private $classe = "Permissoes";
    private $breadcrumb = array();
    private $titulo_principal = "";
    private $situacoes = [];
    public function __construct()
    {
        $common = new CommonController();
        $objSituacao = new Situacao();
        $modulos = $common->getModulos();
        $this->modulos = $modulos;
        $modulo_posicao = array_search($this->classe, array_column($modulos, "modulo"));
        $this->titulo_principal = $modulos[$modulo_posicao];
        $this->situacoes = $objSituacao->listarTodos();
        $this->breadcrumb = array("Maestria" => URL . "dashboard/index/", $this->titulo_principal["descricao"] => URL . $this->classe . "/listar/");
    }
    public function listar()
    {
        $common = new CommonController();
        $objGruposUsuario = new GruposUsuario();
        $objPermissoes = new Permissoes();

        $consultaGruposUsuarios = $objGruposUsuario->listarTodos();

        $arrPermissoes = $objPermissoes->listarTodos();
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        $metodo = $acao = "editar";
        require "./src/View/Permissoes/permissoes_listar.php";
    }

    public function editar($handle)
    {
        $objGruposUsuario = new GruposUsuario();
        $objGruposPermissoes = new GruposPermissoes();
        $msg_sucesso = '';
        $metodo = 'editar';
        if (isset($_POST) && !empty($_POST)) {
            $_POST['handle'] = $handle;
            $retorno = $objGruposPermissoes->editar($_POST);
            if ($retorno) {
                $msg_sucesso = ' Grupo de usuário e permissões alterado com sucesso.';
            }
        }
        $arrPermissoes = $objGruposPermissoes->listarPorGrupo($handle);
        $arrGrupoUsuario = $objGruposUsuario->listarGrupoUsuario($handle);

        
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        require "./src/View/Permissoes/permissoes_form.php";
    }    

    public function cadastrar()
    {

        $objGruposUsuario = new GruposUsuario();
        $objGruposPermissoes = new GruposPermissoes();
        $objPermissoes = new Permissoes();

        $msg_sucesso = '';
        $metodo = 'cadastrar';
        if (isset($_POST) && !empty($_POST)) {
            $retorno = $objGruposPermissoes->cadastrar($_POST);
            if ($retorno) {
                $msg_sucesso = ' Grupo de usuário e permissões cadastrado com sucesso.';
                $metodo = 'editar';
            }            
        }

        $arrPermissoes = [];
        $arrGrupoUsuario = (object) [
            'descricao' => '',
            'situacao' => 1
        ];

        $consultaPermissoes = $objPermissoes->listarTodos();
        foreach ($consultaPermissoes as $permissao) {
            $arrPermissoes[] = (object) array_merge(
                (array) $permissao,
                [
                    'cadastrar' => 0,
                    'editar' => 0,
                    'excluir' => 0,
                    'visualizar' => 0
                ]
            );
        }
        
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        $handle = 0;
        require "./src/View/Permissoes/permissoes_form.php";
    }        
}
