<?php
require_once './src/Model/FormaPagamento.php';
require_once './src/Controller/CommonController.php';
class FormaPagamentoController extends CommonController
{
    private $modulos = array();
    private $estados = array();
    private $classe = "FormaPagamento";
    private $breadcrumb = array();
    private $titulo_principal = "";
    public function __construct()
    {
        $common = new CommonController();
        $modulos = $common->getModulos();
        $this->modulos = $modulos;
        $modulo_posicao = array_search($this->classe, array_column($modulos, "modulo"));
        $this->titulo_principal = $modulos[$modulo_posicao];
        $this->breadcrumb = array("Maestria" => URL . "dashboard/index/", $this->titulo_principal["descricao"] => URL . $this->classe . "/listar/");
    }
    public function listar()
    {
        $common = new CommonController();
        $objFormaPagamento = new FormaPagamento();
        $arrFormaPagamento = $objFormaPagamento->listarTodos();
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        $metodo = $acao = "editar";
        require "./src/View/FormaPagamento/forma_pagamento_listar.php";
    }
    public function editar($handle)
    {
        $msg_sucesso = "";
        $metodo = "editar";
        $objFormaPagamento = new FormaPagamento();
        if (isset($_POST) && !empty($_POST)) {
            $retorno = $objFormaPagamento->editar($_POST);
            if ($retorno) {
                $msg_sucesso = "Forma de Pagamento alterada com sucesso.";
            }
        }
        $FormaPagamento = $objFormaPagamento->listar($handle);
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        require "./src/View/FormaPagamento/forma_pagamento_form.php";
    }
    public function cadastrar()
    {
        $msg_sucesso = "";
        $moedas = "";
        $metodo = "cadastrar";
        $objFormaPagamento = new FormaPagamento();
        if (isset($_POST) && !empty($_POST)) {
            $retorno = $objFormaPagamento->cadastrar($_POST);
            if ($retorno) {
                $msg_sucesso = "Forma de Pagamento cadastrada com sucesso.";
            }
            $FormaPagamento = $objFormaPagamento->listar($retorno);
            $metodo = "editar";
        } else {
            $FormaPagamento = $objFormaPagamento;
        }
        $titulo_principal = $this->titulo_principal;
        $breadcrumb = $this->breadcrumb;
        $modulos = $this->modulos;
        $classe = $this->classe;
        require "./src/View/FormaPagamento/forma_pagamento_form.php";
    }
    public function excluir($handle)
    {
        $msg_sucesso = "";
        $produtos = "";
        $metodo = "cadastrar";
        $objFormaPagamento = new FormaPagamento();
        $objFormaPagamento->excluir($handle);
        $_SESSION["tipoMensagem"] = "callout-success";
        $_SESSION["mensagem"] = "Forma de Pagamento excluído com sucesso.";
        Header("Location: " . URL . "FormaPagamento/listar/");
        exit();
    }
}
