<?php

/**
* {
*	"documents": [
*		{
*			"idIntegracao": "121220231108",
*			"prestador": "49312933000103",
*			"id": "65280583d8e97f5c360cef9e"
*		}
*	],
*	"message": "Nota(as) em processamento",
*	"protocol": "dcfa42ad-fcdd-4e00-9c41-72bdd25fe2d4"
*}
 */

include '~/../src/Model/Cliente.php';
include '~/../src/Controller/CommonController.php';
include '~/../src/Controller/CurlController.php';
include '~/../src/Model/Empresas.php';
include '~/../src/Model/Orcamento.php';
include '~/../src/Model/ItemOrcamento.php';
include '~/../src/Model/FilaDocumentoFiscal.php';
include '~/../src/Model/Componente_Item_Orcamento.php';
include '~/../src/Model/Tributacao.php';

class PlugnotasController {

  private $objCurl;
  public function __construct() {
    $this->objCurl =  new CurlController();
  }

  public function processarFilaNotas() {
    $objFilaDocumentoFiscal = new FilaDocumentoFiscal();
    $arrDocumentos = $objFilaDocumentoFiscal->consultaDocumentos();
    if (count($arrDocumentos) == 0) {
      echo json_encode([
        'success' => '1',
        'msg' => 'Sem notas para processar'
      ]);      
    }
    foreach ($arrDocumentos as $documento) {
      $consultaNota = json_decode($this->consultarNota($documento->id_documento, 'consultaEmLote'));
      $resposta = json_decode($consultaNota->msg);
      if ($consultaNota->status != 200) {
        echo json_encode([
          'success' => '0',
          'msg' => $resposta->error->message,
          'documento' => $documento->id_documento
        ]);
        continue;
      }
      
      $objFilaDocumentoFiscal->editarFilaDocumentoFiscal(
        [
          'handle' => $documento->id_fila,
          'situacao' => $resposta[0]->status,
          'retorno' => $consultaNota->msg
        ]
      );

      echo json_encode([
        'success' => '1',
        'msg' => 'Nota pendente processada novamente!',
        'documento' => $documento->id_documento
      ]);      
    }
  }

  public function emitirNota($id_pedido = 0, $id_empresa = 1) {
    
    /**
     * "{\"documents\":[{\"idIntegracao\":\"05620230424233856\",\"Emitente\":\"05678029000140\",\"id\":\"64473d411344660156cb5a89\"}],\"message\":\"Nota(as) em processamento\",\"protocol\":\"ab1d7a2d-4d92-4d38-b125-691806587057\"}"
     * "{\"documents\":[{\"idIntegracao\":\"056720230421170304\",\"Emitente\":\"05678029000140\",\"id\":\"6442ebf9803e1849636863b9\"}],\"message\":\"Nota(as) em processamento\",\"protocol\":\"b6165907-399b-4880-96b5-aef66575a378\"}"
     */
    try {
      $objCliente = new Cliente();
      $objCommon = new CommonController();
      $objEmpresas = new Empresas();
      $objOrcamento = new Orcamento();
      $objItemOrcamento = new ItemOrcamento();
      $objFilaDocumentoFiscal = new FilaDocumentoFiscal();
      $objMolduraItemOrcamento = new Moldura_Item_Orcamento();
      $objComponenteItemOrcamento = new Componente_Item_Orcamento();

      $parametros = $objCommon->validateGet('parametros');
      if ($parametros) {
        preg_match_all('/^(.*)=(\d)$/m', $parametros, $matches, PREG_SET_ORDER, 0);
        if (isset($matches[0][1]) && $matches[0][1] == 'IdEmpresa') {
          $id_empresa = $matches[0][2];
        }
      }

      $consultaEmpresa = $objEmpresas->ListarEmpresa($id_empresa);
      $listaRegimesTributarios = $objCommon->getRegimesTributarios();
      $consultaOrcamento = $objOrcamento->listarOrcamento($id_pedido);

      $dadosNota = [];
      $resposta = '';

      if (sizeof($consultaOrcamento) == 0) {
        throw new ErrorException('Pedido ' . $id_pedido . ' não encontrado');
      }
      
      $consultaDocumentoPedido = $objFilaDocumentoFiscal->listarFilaDocumentoFiscal(['id_pedido' => $id_pedido]);
      if (isset($consultaDocumentoPedido->id_fila) && in_array($consultaDocumentoPedido->situacao, ['CONCLUIDO', 'PROCESSANDO']) ) {
        throw new ErrorException('Este pedido já tem uma nota fiscal emitida. Seu status é: ' . $consultaDocumentoPedido->situacao);
      }

      
      $itemsOrcamento = $objItemOrcamento->listarItemOrcamento($id_pedido);
      /**
       * Verificando quantos produtos (molduras, escoras, etc) e componentes foram
       * selecionados para este pedido
       */
      $consultaTotalItensMolduraComponentes = $objItemOrcamento->consultaTotalMolduraComponentes($id_pedido);
      $total_itens = $consultaTotalItensMolduraComponentes->total_itens;

      $consultaCliente = $objCliente->listarCliente($consultaOrcamento[0]->Cd_Cliente);
      $idIntegracao = sprintf('%s%s', substr($objCommon->onlyNumber($consultaEmpresa->CGC),0,3), date('YmdHis'));

      $arrItens = [];
      if (count($itemsOrcamento) == 0) {
        throw New ErrorException('O pedido não possui produtos vinculados');
      }


      $vl_desconto = $consultaOrcamento[0]->valorDescontoFinal ?: 0;
      $vl_desconto_por_item = 0;
      if ($total_itens > 0) {
        $vl_desconto_por_item = (float) round(number_format($vl_desconto / $total_itens, 4), 2);
        // echo "$vl_desconto / $total_itens: $vl_desconto_por_item";
      }
      
      $codigo_uf_nota = $consultaCliente[0]->codigo_uf > 0 ? $consultaCliente[0]->codigo_uf : 0;
      if ($codigo_uf_nota == 0) {
        throw New ErrorException('O endereço do cliente está incorreto. Verifique no cadastro do cliente.');
      }      
	  
	  $valor_extra_total = 0;
    $soma_produtos = 0;
      
      foreach ($itemsOrcamento as $item_orcamento) {
		  
		$vl_adicionais = (float) $item_orcamento->VL_ADICIONAIS;
		$vl_moldura = (float) $item_orcamento->Vl_Moldura;
		$valor_extras = $vl_adicionais + $vl_moldura;
		$qt_item = $item_orcamento->Qt_Item;
		$total_valor_extra = $valor_extras * $qt_item;
		$diferenca = 0;
		$valor_extra_total += $total_valor_extra;
		
		//print_r($item_orcamento);exit;
		  
        $moldurasItemOrcamento = $objMolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $id_pedido, $codigo_uf_nota);
        $componentesItemOrcamento = $objComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $id_pedido);
		
		$qt_molduras_componentes = count($moldurasItemOrcamento) + count($componentesItemOrcamento);
		$valor_extra_unitario = round(($valor_extras * $qt_item) / $qt_molduras_componentes, 2);
		
		if ($valor_extra_unitario * $qt_molduras_componentes !== ($valor_extras * $qt_item)) {
			$diferenca = round(($valor_extras * $qt_item) - ($valor_extra_unitario * $qt_molduras_componentes),2);
		}
		
        //print_r($moldurasItemOrcamento);exit;
        foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
          $arrIcms["origem"] = "0";
          $qt_unitario = (int) $moldura_item_orcamento->Qt_Item != 0 ? $moldura_item_orcamento->Qt_Item : $item_orcamento->Qt_Item;
          $vl_unitario = (float) $moldura_item_orcamento->Vl_Unitario != 0 ? $moldura_item_orcamento->Vl_Unitario : $item_orcamento->Vl_Unitario;
//          $vl_unitario = (float) round($vl_unitario, 2);
		  //echo $vl_unitario;exit;
          //$vl_unitario = (float) $moldura_item_orcamento->Vl_Unitario;
          $arrIcms["cst"] = '400';
          if ($moldura_item_orcamento->cst > 0) {
            $arrIcms["cst"] = $moldura_item_orcamento->cst;
          }
          /*
          if ($arrIcms["cst"] != '400') {
            $arrIcms['creditoSimplesNacional'] = [
              'percentual' => (float) 1.8600, // Configurar banco de dados
              'valor' => round(($vl_unitario) * (1.8600/100), 2)
            ];
          }
          * */
          if ($moldura_item_orcamento->aliquotaIcms > 0) {
            $arrIcms["aliquota"] = (string) $moldura_item_orcamento->aliquotaIcms;
          }

          $arrPis = [];
          if ($moldura_item_orcamento->cstpis > 0) {
            $arrPis["cst"] = (string) $moldura_item_orcamento->cstpis;
          }
          $arrPis["aliquota"] = (float) $moldura_item_orcamento->aliquotaPis;
          $arrPis["valor"] = 0;

          $arrConfins = [];
          $arrConfins["cst"] = (string) $moldura_item_orcamento->cstconfins > 0 ? $moldura_item_orcamento->cstconfins : '';
          $arrConfins["aliquota"] = (float) $moldura_item_orcamento->aliquotaConfins;
	  $arrConfins["valor"] = 0;

	  $cfop = (string) $moldura_item_orcamento->cfop;
	  $codigo_ncm = (string) $moldura_item_orcamento->codigo_ncm;
    $soma_produtos += $vl_unitario;
          $arrItens[] = [
            "codigo" => (string) $moldura_item_orcamento->NovoCodigo,
            "descricao" => $moldura_item_orcamento->DescricaoProduto,
            "ncm" => (string) $moldura_item_orcamento->codigo_ncm,
            // "cest" => (int) $moldura_item_orcamento->codigo_cest > 0 ? $moldura_item_orcamento->codigo_cest : '',
            "cfop" => (string) $moldura_item_orcamento->cfop,
            "valorUnitario" => [
              "comercial" => (float) $vl_unitario,
              "tributavel"=> (float) $vl_unitario
            ],
          //  "valor" => 0, //(float) $vl_unitario,
			"valorOutros" => (float) $total_valor_extra,
            "valorDesconto" => (float) $vl_desconto, // $vl_desconto_por_item,
            "quantidade" => [
              'comercial' => 1, //(int) $qt_unitario,
              'tributavel' =>1, // (int) $qt_unitario,
            ],
            "tributos" => [
              "icms" => $arrIcms,
              "pis" => $arrPis,
              "cofins" =>$arrConfins
            ]
          ];
		  $diferenca = 0;
		  $vl_desconto = 0;
		  $total_valor_extra = 0;
        }

        foreach ($componentesItemOrcamento as $componente_item_orcamento) {
          $soma_produtos += (float) $componente_item_orcamento->VALOR_UNITARIO;
          $arrItens[] = [
            // "codigo" => $componente_item_orcamento->IDCOMPONENTE,
            "descricao" => $componente_item_orcamento->DescricaoProduto,
            "ncm" => $codigo_ncm,
            //"cest" => "1008000",
            "cfop" => $cfop,
            "valorUnitario" => [
              "comercial" => (float) $componente_item_orcamento->VALOR_UNITARIO,
              "tributavel"=> (float) $componente_item_orcamento->VALOR_UNITARIO
            ],
			// "valor" => (float) $componente_item_orcamento->VALOR_UNITARIO,
			"valorOutros" => $total_valor_extra,
	    "valorDesconto" => $vl_desconto, //(float) $vl_desconto_por_item,
            "tributos" => [
              "icms" => $arrIcms,
              "pis" => $arrPis,
              "cofins" =>$arrConfins
            ]
          ];
		  $diferenca = 0;
		  $vl_desconto = 0;
		  $total_valor_extra = 0;
        }
      }
      
      if ($objCommon->onlyNumber($consultaCliente[0]->CGC) == '') {
        throw New ErrorException('Documento (CNPJ ou CPF) inválido. Verifique no cadastro do cliente.');
      }

	  $cpfCnpjEmitente = strpos( URL_PLUGNOTAS, 'sandbox' ) !== false ? '08187168000160' : $objCommon->onlyNumber($consultaEmpresa->CGC);

      $dadosNota = [[
          "idIntegracao" => (string) $idIntegracao,
          "presencial"=> true,
          "consumidorFinal"=> true,
          "natureza"=> "VENDA DE PRODUTOS",
          "emitente"=> [
            "cpfCnpj"=> $cpfCnpjEmitente
          ],
          "destinatario" => [
            "cpfCnpj" => $objCommon->onlyNumber($consultaCliente[0]->CGC),
            "razaoSocial" => $consultaCliente[0]->RazaoSocial,
            "email" => $consultaCliente[0]->EMail !== NULL ? $consultaCliente[0]->EMail : '',
            "inscricaoEstadual" => $consultaCliente[0]->InscricaoEstadual,
            "endereco" => [
              "tipoLogradouro" => $consultaCliente[0]->descricaoLogradouro,
              "logradouro" => $consultaCliente[0]->Endereco,
              "numero" => $consultaCliente[0]->numeroEndereco,
              "bairro" => $consultaCliente[0]->Bairro,
              "codigoCidade" => $consultaCliente[0]->Cidade,
              "descricaoCidade" => $consultaCliente[0]->nomeMunicipio,
              "estado" => $consultaCliente[0]->Estado,
              "cep" => $objCommon->onlyNumber($consultaCliente[0]->CEP)
            ],
            // "telefone" => $consultaCliente[0]->Telefone1
          ],
          "itens" => $arrItens,
          "pagamentos" => [
            [
              "aVista" => true,
              "meio" => "01",
              "valor" => (float) round( ($consultaOrcamento[0]->Vl_Bruto - $consultaOrcamento[0]->valorDescontoFinal), 2)
            ]
          ],
          "total" => [
            'valorDesconto' => (float) $consultaOrcamento[0]->valorDescontoFinal,
            'valorProdutosServicos' => $soma_produtos, //(float) $consultaOrcamento[0]->Vl_Bruto - $valor_extra_total,
            'valorNfe' =>  (float) round( ($consultaOrcamento[0]->Vl_Bruto - $consultaOrcamento[0]->valorDescontoFinal), 2),
			'valorOutros' => $valor_extra_total
          ],
          "transporte" => [
            "modalidadeFrete" => "0"
          ],
          "responsavelTecnico" => [
            "cpfCnpj" => "49312933000103",
            "nome" => "Denis Brandl",
            "email" => "contato@maestriasistema.com.br",
            "telefone" => [
              "ddd" => "47",
              "numero" => "991786533"
            ]
          ],
            "enviaremail" => true,
            "informacoesComplementares" => $consultaOrcamento[0]->Ds_Observacao_Pedido
        ]];


	    echo "<pre>".json_encode($dadosNota, JSON_PRETTY_PRINT)."</pre>";
      exit;
      $retorno = json_decode($this->objCurl->callCurl(URL_PLUGNOTAS . 'nfe', [], json_encode($dadosNota)), true);

      $status = $retorno['status'];
      $resposta = json_decode($retorno['response'], true);

      if ($retorno['status'] !== 200) {
        throw new ErrorException(json_encode($retorno));
      }

      $objFilaDocumentoFiscal->cadastrarFilaDocumentoFiscal(
        [
          'id_documento' => $resposta['documents'][0]['id'],
          'id_pedido' => $id_pedido,
          'payload' =>  json_encode($dadosNota),
          'retorno' => json_encode($resposta,JSON_NUMERIC_CHECK),
          'id_empresa' => $id_empresa,
          'id_usuario_criacao' => $_SESSION['handle'],
          'data_criacao' => $objFilaDocumentoFiscal->data_criacao,
          'situacao' => 'PROCESSANDO'
        ]
      );
      
      echo json_encode([
        'success' => '1',
        'msg' => $resposta['message'],
        'id_documento' => $resposta['documents'][0]['id'] 
      ]);
    } catch (Exception $e) {
      if (count($dadosNota) > 0) {
        $objFilaDocumentoFiscal->cadastrarFilaDocumentoFiscal(
          [
            'id_documento' => '',
            'id_pedido' => $id_pedido,
            'payload' =>  json_encode($dadosNota),
            'retorno' => json_encode($resposta),
            'id_empresa' => $id_empresa,
            'id_usuario_criacao' => $_SESSION['handle'],
            'data_criacao' => $objFilaDocumentoFiscal->data_criacao,
            'situacao' => 'ERRO'
          ]
        );
      }
      $arrMessage = json_decode($e->getMessage(), true);
	  //print_r($arrMessage);
      $arrRespostaErro = [];
      if ($arrMessage) {
        $arrResponse = json_decode($arrMessage['response'],true);
        $arrFields = isset($arrResponse['error']['data']['fields']) ? $arrResponse['error']['data']['fields'] : [];

       //  print_r($arrFields);
        // print_r($dadosNota);
        
        foreach ($arrFields as $key => $value) {
          $dados = explode('.', $key);
		  
		  //print_r($dados);
		  //exit;
		 // echo count($dados);exit;

          $item = preg_replace('/[^\d]/m',"", $dados[1]);
		  
		 // var_dump($item);

		  if (isset($dados[3]) && isset($dados[4])) {
			  $arrRespostaErro[] = sprintf('%s do imposto %s para o item %s - %s: %s',
			  isset($dados[4]) ? strtoupper($dados[4]) : '',
			  isset($dados[3]) ? strtoupper($dados[3]) : '' ,
			  isset($dadosNota[0]['itens'][$item]['codigo']) ? $dadosNota[0]['itens'][$item]['codigo'] : '',
			  $dadosNota[0]['itens'][$item]['descricao'],
			  $value
			  );
		  } else if (isset($dados[2]) && isset($dados[1])) {
			  $mensagem_erro = sprintf('%s do %s: %s',
			  strtoupper($dados[2]),
			  strtoupper($dados[1]),
			  $value
			  );
			
			  $arrRespostaErro[] = $mensagem_erro;
		  } else {
			  $mensagem_erro = sprintf('%s %s',
			  strtoupper($dados[1]),
			  $value
			  );
			
			  $arrRespostaErro[] = $mensagem_erro;
		  }			  
        }
		
		if (isset($arrResponse['error']['message'])) {
			$arrRespostaErro[] = $arrResponse['error']['message'];
		}
      } else {
        $arrRespostaErro[] = $e->getMessage();
      }
      echo json_encode([
        'success' => '0',
        'msg' => json_encode($arrRespostaErro)
      ]);
    }
  }

  public function consultarNota($idNota = 0, $origemRequisicao = '') {
    $retorno = json_decode($this->objCurl->callCurl(URL_PLUGNOTAS . 'nfe/' . $idNota . '/resumo', [], [], 'GET'), true);
    $status = $retorno['status'];

    $retorno = json_encode([
        'success' => '1',
        'msg' => $retorno['response'],
        'status' => $status
    ]);

    if ($origemRequisicao === '') {
      echo $retorno;
      return;
    }


    return $retorno;
    
  }

  public function downloadNota($idNota = 0) {
    $retorno = json_decode($this->objCurl->callCurl(URL_PLUGNOTAS . 'nfe/' . $idNota . '/pdf', [], [], 'GET', false, true), true);

    $status = $retorno['status'];

    $resposta = json_decode($retorno['response'], true);

    if ($retorno['status'] !== 200) {
      echo '<h1> Deu ruim :-( </h1>';
      echo sprintf('<p>Erro: <br> %s <textarea>%s</textarea></p>', $resposta['error']['message'],json_encode($resposta['error']['data']));
      exit;
    }

    header('Content-type: application/pdf');
    echo hex2bin($retorno['response']);
    exit;
  }  

public function downloadXML($idNota = 0) {
    $retorno = json_decode($this->objCurl->callCurl(URL_PLUGNOTAS . 'nfe/' . $idNota . '/xml', [], [], 'GET', false, true), true);

    $status = $retorno['status'];

    $resposta = json_decode($retorno['response'], true);

    if ($retorno['status'] !== 200) {
      echo '<h1> Deu ruim :-( </h1>';
      echo sprintf('<p>Erro: <br> %s <textarea>%s</textarea></p>', $resposta['error']['message'],json_encode($resposta['error']['data']));
      exit;
    }

    header('Content-type: text/xml');
    header('Content-Disposition: attachment; filename="'.$idNota.'.xml"');
    echo hex2bin($retorno['response']);
    // echo sprintf('<textarea style="height:100%%;width:100%%">%s</textarea>', $dom->saveXML());
    exit;
  }    

}
