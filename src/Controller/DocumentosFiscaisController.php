<?php
require_once './src/Controller/CommonController.php';
require_once './src/Model/FilaDocumentoFiscal.php';
class DocumentosFiscaisController extends CommonController
{

  private $modulos = array();
  private $classe = 'DocumentosFiscais';
  private $breadcrumb = array();
  private $titulo_principal = '';
  private $habilitar_emissao_nf = false;
  private $habilitar_emissao_nfs = false;
  private $habilitar_emissao_nfc = false;

  public function __construct()
  {
    $documento_fiscal = new FilaDocumentoFiscal();
    $common = new CommonController();
    $objConfiguracoes = new Configuracoes();

    $this->modulos = $common->getModulos();

    $modulo_posicao = array_search($this->classe, array_column($this->modulos, 'modulo'));
    $this->titulo_principal = ['descricao' => 'Documentos Fiscais', 'icone' => 'fa fa-exchange'];
    $this->breadcrumb = array('Maestria' => URL . 'dashboard/index/', $this->titulo_principal['descricao'] => URL . $this->classe . '/listar/');

    $this->habilitar_emissao_nf  = filter_var($objConfiguracoes->listarConfiguracao('habilitar_emissao_nf')->valor, FILTER_VALIDATE_BOOLEAN);
    $this->habilitar_emissao_nfs = filter_var($objConfiguracoes->listarConfiguracao('habilitar_emissao_nfs')->valor, FILTER_VALIDATE_BOOLEAN);
    $this->habilitar_emissao_nfc = filter_var($objConfiguracoes->listarConfiguracao('habilitar_emissao_nfc')->valor, FILTER_VALIDATE_BOOLEAN);        
  }

  public function listar()
  {
    $objDocumentosFiscais = new FilaDocumentoFiscal();

    $coluna = '';
    $buscar = '';
    $pagina_atual = 1;
    if ($this->validateGet('parametros')) {
      $re = "/^[a-z]+=/";
      preg_match($re, $this->validateGet('parametros'), $matches);
      $acao = str_replace('=', '', $matches[0]);

      $re = "/=([a-zA-Z].*)\|([A-Za-z0-9].*)$/";
      preg_match($re, $this->validateGet('parametros'), $matches);

      switch ($acao) {
        case 'buscar':
          if (isset($matches[1])) {
            $coluna = str_replace('=', '', $matches[1]);
          }
          if (isset($matches[2])) {
            $buscar = str_replace('=', '', $matches[2]);
          }
          break;

        case 'listar':
          $pagina_atual = str_replace('=', '', $matches[2]);
      }
    }

    $linha_inicial = ($pagina_atual - 1) * QTDE_REGISTROS;

    $num_registros = $objDocumentosFiscais->listarTodosTotal();

    /* Idêntifica a primeira página */
    $primeira_pagina = 1;

    /* Cálcula qual será a última página */
    $ultima_pagina  = ceil($num_registros / QTDE_REGISTROS);

    /* Cálcula qual será a página anterior em relação a página atual em exibição */
    $pagina_anterior = ($pagina_atual > 1) ? $pagina_atual - 1 : 0;

    /* Cálcula qual será a pŕoxima página em relação a página atual em exibição */
    $proxima_pagina = ($pagina_atual < $ultima_pagina) ? $pagina_atual + 1 : 0;

    /* Cálcula qual será a página inicial do nosso range */
    $range_inicial  = (($pagina_atual - RANGE_PAGINAS) >= 1) ? $pagina_atual - RANGE_PAGINAS : 1;

    /* Cálcula qual será a página final do nosso range */
    $range_final   = (($pagina_atual + RANGE_PAGINAS) <= $ultima_pagina) ? $pagina_atual + RANGE_PAGINAS : $ultima_pagina;

    /* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */
    $exibir_botao_inicio = ($range_inicial < $pagina_atual) ? 'mostrar' : 'esconder';

    /* Verifica se vai exibir o botão "Anterior" e "Último" */
    $exibir_botao_final = ($range_final > $pagina_atual) ? 'mostrar' : 'esconder';

    $arrCamposBusca = array('nome'  => 'Nome', 'status'    => 'Status',);

    $documento_fiscais = $objDocumentosFiscais->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar,100);

    $arrNF = [];
    $arrNFS = [];
    $arrNFC = [];
    foreach ($documento_fiscais as $documento) {
      $links = $this->botoesAcao($documento);
      $arrDocumento = (object) array_merge((array) $documento, $links);
      switch ($documento->id_tipo_documento) {
        case 1:           
          $arrNF[]  = $arrDocumento;
        break;

        case 2: 
          $arrNFS[]  = $arrDocumento;
        break;
        
        case 3: 
          $arrNFC[]  = $arrDocumento;
        break;
      }
    }

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;
    $modulos = $this->modulos;
    $classe = $this->classe;
    require './src/View/DocumentosFiscais/documentos_fiscais_listar.php';
  }

  public function downloadXml($id_documento = 0) {
    $objDocumentosFiscais = new FilaDocumentoFiscal();
    $consultaDocumento = $objDocumentosFiscais->listarPorDocumento($id_documento);
    
    header('Content-type: text/xml');
    header('Content-Disposition: attachment; filename="' . $id_documento . '.xml"');
    echo base64_decode($consultaDocumento->link_xml);
    exit;
  }

  private function botoesAcao($documento) {
    $mensagem = '';
    $link_pdf = '';
    $link_validar = '';
    $link_cancelar = '';
    $link_devolucao = '';
    $cor = '';
    $link_documento = $documento->id_tipo_documento == 1 ? 'IntegraNota' : ($documento->id_tipo_documento == 2 ? 'IntegraNotaServico' : 'IntegraNotaConsumidor');
    $link_pdf = $documento->link_pdf != '' ? $documento->link_pdf : '';

    $link_xml = sprintf('
      <a
        role="button"
        class="btn btn-primary"
        href="%s"
        target="_blank"
      >
        <i class="fa fa-file-pdf-o"></i> Visualizar XML
      </a>',
      URL . $link_documento . '/downloadXML/' . $documento->id_documento . '/IdEmpresa=' . $documento->id_empresa
    );
    
    if ($documento->situacao === 'ERRO') {
      $cor = "red";
      $mensagem = $documento->retorno;
      $detalhes = '';                        
      $mensagem = sprintf('<p>%s<br>%s</p>', $mensagem, $detalhes);
    }

    if ($documento->situacao === 'REJEITADO') {
      $cor = "orange";
      $mensagem = $documento->retorno;
      $detalhes = '';
      
      $mensagem = sprintf('<p>%s<br>%s</p>', $mensagem, $detalhes);
    }

    if ($documento->situacao === 'Autorizado' || $documento->situacao === 'Autorizada') {
      $cor = 'green';
      $mensagem = $documento->retorno;
      $link_pdf = sprintf('
        <a
          role="button"
          class="btn btn-primary"
          href="%s"
          target="_blank"
        >
          <i class="fa fa-file-pdf-o"></i> Baixar PDF
        </a>',
        ( 
          //$link_pdf !== '' ? $link_pdf : URL . $link_documento . '/downloadNota/' . $documento->id_documento
          URL . $link_documento . '/downloadNota/' . $documento->id_documento
          )
      );
      
      if ($documento->devolucao == 0) {
      $link_cancelar = sprintf('
        <a
          role="button"
          class="btn btn-danger btnCancelarNota"
          data-nota="%s"
          data-tipo-documento="%s"
          target="_blank"
        >
          <i class="fa fa-trash-o"></i> Cancelar Nota
        </a>',
        $documento->id_documento,
        $link_documento
      );
      
      
        $link_devolucao = sprintf(
          '
        <a
          role="button"
          class="btn btn-warning btnNotaDevolucao"
          data-nota="%s"
          data-tipo-documento="%s"
          target="_blank"
        >
          <i class="fa fa-trash-o"></i> Emitir Nota de devolução
        </a>',
          $documento->id_documento,
          $link_documento
        );
      }
    }
    
    if ($documento->situacao === 'PROCESSANDO') {
      $cor = "blue";
      $mensagem = $documento->retorno;
      $link_validar = sprintf('
        <button class="btn btn-primary btnRecarregarConsulta" data-tipo-documento="%s" role="button"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Verificar novamente</button>',
        $documento->id_tipo_documento
      );
    }
    
    if ($documento->situacao === 'Cancelado') {
      $cor = "gray";
      $mensagem = $documento->retorno;
      $link_pdf = sprintf('
        <a
          role="button"
          class="btn btn-primary"
          href="%s"
          target="_blank"
        >
          <i class="fa fa-file-pdf-o"></i> Baixar PDF
        </a>',
        ($link_pdf != '' ? $link_pdf : URL . $link_documento . '/downloadNota/' . $documento->id_documento)
      );
    }

    return [
      'link_pdf' => $link_pdf,
      'link_validar' => $link_validar,
      'link_xml' => $link_xml,
      'link_cancelar' => $link_cancelar,
      'link_devolucao' => $link_devolucao,
      'cor' => $cor
    ];
    
  }
}
