<?php
require_once './src/Model/ComplexidadeProduto.php';
require_once './src/Controller/CommonController.php';
class ComplexidadeProdutoController extends CommonController {
public function listarComplexidadePorProduto() {
		$complexidadeProduto = new ComplexidadeProduto();
    $common = new CommonController();

    if (!$common->validatePost('handle')) {
				echo json_encode(array('success' => 0));
				return false;
    }

    $handle = $common->validatePost('handle');

		$retorno = $complexidadeProduto->listarComplexidadePorProduto($handle);

    echo json_encode(
      ['success' => 1, 'data' => $retorno]
    );

    exit;

	}  

}
?>
