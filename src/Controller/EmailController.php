<?php

require_once './src/Controller/CommonController.php';
require_once './src/Model/Configuracoes.php';

use Leaf\Mail\Mailer;
use PHPMailer\PHPMailer\PHPMailer;


class EmailController {
		
  public $configuracoes_email;
	public function __construct() {
    $configuracoes = new Configuracoes();
    $this->configuracoes_email = json_decode($configuracoes->listarConfiguracao('configuracoes_email')->valor);
	}
	
	public function enviar($arrMensagem) {
		$validaFormulario = self::validaDados($arrMensagem);

		if ($validaFormulario['success'] == 'false') {
			echo json_encode(
					[
						'success' => 'false',
						'message' => implode('<br>', $validaFormulario['message'])
					]
				);
			return;			
		}
		
		if (!filter_var($arrMensagem['email'], FILTER_VALIDATE_EMAIL)) {			
			echo json_encode(
					[
						'success' => 'false',
						'message' => implode('<br>', array('Por favor informe um endereço de e-mail válido'))
					]
				);
			return;
		}
		
    Mailer::connect([
      'host' => $this->configuracoes_email->servidor,
      'port' => $this->configuracoes_email->porta,
      'security' => $this->configuracoes_email->protocolo == 'ssl' ? PHPMailer::ENCRYPTION_SMTPS : ($this->configuracoes_email->protocolo == 'tls' ? PHPMailer::ENCRYPTION_STARTTLS : ''),
      'auth' => [
        'username' => $this->configuracoes_email->usuario,
        'password' => $this->configuracoes_email->senha
      ]
    ]);

    Mailer::config([
      'keepAlive' => false,
      'debug' => false
    ]);

		try {
      $email = \Leaf\Mail::create(
        [
        'subject' => $arrMensagem['assunto'],
        'body' => $arrMensagem['mensagem'].'<hr>'.'Mensagem enviada automaticamente pelo sistema <strong>Maestria Sistema</strong>',
        'recipientEmail' => $arrMensagem['email'],
        'recipientName' => $arrMensagem['nome'],
        'senderName' => $arrMensagem['remetente'],
        'senderEmail' => $this->configuracoes_email->usuario,
		'replyToName' => $arrMensagem['remetente'],
        'replyToEmail' => isset($arrMensagem['remetente_email']) && !empty($arrMensagem['remetente_email']) ? $arrMensagem['remetente_email'] : $this->configuracoes_email->usuario
        ]
      )->send();
			
			echo json_encode(
					[
						'success' => 'true',
						'message' => sprintf('Mensagem enviada com sucesso para %s', $arrMensagem['email'])
					]
				);
			return;			
			
		} catch (Exception $e) {
			echo json_encode(
					[
						'success' => 'false',
						'message' => 'Houve uma falha ao tentar enviar a mensagem! Por favor, tente novamente mais tarde',
            'erro' => $e->getMessage()
					]
				);
			return;
		}		
	}
	
	private static function validaDados($frmDados) {
		$frm_valido = 'true';
		$arrRetorno = ['success' => true, 'message' => [] ];
		$arrDados = '{
						"nome": {
							"obrigatorio":"false",
							"mensagem":"Informe o seu nome"
						},
						"email": {
							"obrigatorio":"true",
							"mensagem":"Informe o seu e-mail"
						},
						"mensagem": {
							"obrigatorio":"false",
							"mensagem":"Preencha uma mensagem"
						}
					}';

		$arrCampos = json_decode($arrDados, true);

		foreach ($arrCampos as $key => $value) {
			if (!isset($frmDados[$key]) || empty($frmDados[$key])) {
				if ($value['obrigatorio'] == 'true') {
					$arrRetorno['message'][] = $value['mensagem'];
					$frm_valido = 'false';
				}
			}
		}
		
		$arrRetorno['success'] = $frm_valido;
		
		return $arrRetorno;
		
	}	
}
?>
