<?php

include_once '~/../src/Model/Cliente.php';
include_once '~/../src/Controller/CommonController.php';
include_once '~/../src/Controller/CurlController.php';
include_once '~/../src/Model/Empresas.php';
include_once '~/../src/Model/Orcamento.php';
include_once '~/../src/Model/Assets.php';
include_once '~/../src/Model/ItemOrcamento.php';
include_once '~/../src/Model/FilaDocumentoFiscal.php';
include_once '~/../src/Model/Componente_Item_Orcamento.php';
include_once '~/../src/Model/Tributacao.php';

use CloudDfe\SdkPHP\Nfse;

class IntegraNotaServicoController extends CommonController
{

  private $objCurl;
  private $objIntegraNota;
  private $objCommon;
  private $objFilaDocumentoFiscal;
  public function __construct()
  {
    $this->objCurl =  new CurlController();
    $this->objCommon = new CommonController();
    $this->objFilaDocumentoFiscal = new FilaDocumentoFiscal();
  }

  public function processarFilaNotas()
  {
    
    $arrDocumentos = $this->objFilaDocumentoFiscal->consultaDocumentos(['tipo_documento' => 2]);
    if (count($arrDocumentos) == 0) {
      echo json_encode([
        'success' => '1',
        'msg' => 'Sem notas para processar'
      ]);
    }

    foreach ($arrDocumentos as $documento) {
      $consultaNota = json_decode($this->consultarNota($documento->id_documento, 'consultaEmLote'));

      /*
      if ($consultaNota->success != 1 && $consultaNota->codigo != '539') {
        echo json_encode([
          'success' => '0',
          'msg' => $consultaNota->msg,
          'documento' => $documento->id_documento
        ]);
        continue;
      }
      */
/*
      $status = 'PROCESSANDO';
      switch (true) {
        case in_array($consultaNota->codigo, range(201, 562)):
          $status = 'REJEITADO';
          break;

        case $consultaNota->codigo == '999':
          $status = 'REJEITADO';
          break;

        case $consultaNota->success ==   
      }

      $this->objFilaDocumentoFiscal->editarFilaDocumentoFiscal(
        [
          'handle' => $documento->id_fila,
          'situacao' => $status,
          'retorno' => $consultaNota->msg
        ]
      );
*/
    }

    echo json_encode([
      'success' => '1',
      'msg' => 'Nota pendente processada novamente!',
      'documento' => 0
    ]);
  }

  public function emitirDocumentoContaReceber($id_pedido = 0, $id_empresa = 1)
  {

    try {

      $objCliente = new Cliente();
      $objCommon = new CommonController();
      $objEmpresas = new Empresas();
      $objOrcamento = new Orcamento();
      $objContaReceber = new Assets();
      $objItemOrcamento = new ItemOrcamento();

      $this->alteraObjetoIntegraNota($id_empresa);

      $parametros = $objCommon->validateGet('parametros');
      if ($parametros) {
        preg_match_all('/^(.*)=(\d)$/m', $parametros, $matches, PREG_SET_ORDER, 0);
        if (isset($matches[0][1]) && $matches[0][1] == 'IdEmpresa') {
          $id_empresa = $matches[0][2];
        }
      }

      $consultaEmpresa = $objEmpresas->ListarEmpresa($id_empresa);
      $consultaContaReceber = $objContaReceber->listar($id_pedido);
      
      $numero_sequencia_nf = $consultaEmpresa->numero_sequencia_nfs;
      $serie_nf = $consultaEmpresa->serie_nfs;

      $dadosNota = [];

      if (!$consultaContaReceber) {
        throw new ErrorException('Conta a receber ' . $id_pedido . ' não encontrada');
      }

      $consultaDocumentoPedido = $this->objFilaDocumentoFiscal->listarFilaDocumentoFiscal(['id_pedido' => $id_pedido]);
      if (isset($consultaDocumentoPedido->id_fila) && in_array($consultaDocumentoPedido->situacao, ['CONCLUIDO', 'PROCESSANDO'])) {
        throw new ErrorException('Este pedido tem uma nota fiscal emitida. Status: ' . $consultaDocumentoPedido->situacao);
      }


      $total_itens = 1;

      $consultaCliente = $objCliente->listarCliente($consultaContaReceber->Title);
      $idIntegracao = sprintf('%s%s', substr(preg_replace('/0/', '', $objCommon->onlyNumber($consultaEmpresa->CGC)), 0, 3), $id_pedido);

      $arrItens = [];

      $codigo_uf_nota = $consultaCliente[0]->codigo_uf > 0 ? $consultaCliente[0]->codigo_uf : 0;
      if ($codigo_uf_nota == 0) {
        throw new ErrorException('O endereço do cliente incorreto. Verifique no cadastro do cliente.');
      }

      if ($objCommon->onlyNumber($consultaCliente[0]->CGC) == '') {
        throw new ErrorException('Documento (CNPJ ou CPF) inválido. Verifique no cadastro do cliente.');
      }


      
      $valor_aliquota = 2;
      $valor_pis = 0;
      $valor_confins = 0;
      $valor_inss = 0;
      $valor_ir = 0;
      $valor_csll = 0;
      $valor_outras = 0;
      $valor_desconto_incondicionado = 0;

      $arrVariaveisPedido = [
        '[MES_VENCIMENTO]' => strtoupper($objCommon->getMes()[date('m')]),
        '[MES_ANTERIOR]' => strtoupper($objCommon->getMes()[date('m',strtotime('-1 month'))]),
      ];
      
      $descricao = $consultaContaReceber->Description;
      $regex = '/\[(.*?)\]/x';

      preg_match_all($regex, $descricao, $ocorrenciasVariaveis, PREG_SET_ORDER, 0);
	
      foreach ($ocorrenciasVariaveis as $variavel) {
        $descricao = str_replace($variavel[0], $arrVariaveisPedido[$variavel[0]], $descricao);
      }

      $arrItens[] = [
        "codigo" => "01.04",
        'codigo_cnae' => '6201501',
        "discriminacao" => $descricao,
        "valor_servicos" => (float) $consultaContaReceber->Amount,
        "valor_pis" => $valor_pis,
        "valor_cofins" => $valor_confins,
        "valor_inss" => $valor_inss,
        "valor_ir" => $valor_ir,
        "valor_csll" => $valor_csll,
        "valor_outras" => $valor_outras,
        "valor_aliquota" => $valor_aliquota,
        "valor_desconto_incondicionado" => $valor_desconto_incondicionado
      ];

      $dadosNota = [
        'natureza_operacao' => '1',
        'numero' => $numero_sequencia_nf !== '' ? (string) $numero_sequencia_nf : '1',
        'serie' => $serie_nf !== '' ? (string) $serie_nf : '1',
        'tipo' => 1,
        'status' => 1,
        'data_emissao' => date('Y-m-d\TH:i:s-03:00'),
        'tomador' => [
          'cpf' => $consultaCliente[0]->TipoCliente == 'F' ? $objCommon->onlyNumber($consultaCliente[0]->CGC) : '',
          'cnpj' => $consultaCliente[0]->TipoCliente == 'J' ? $objCommon->onlyNumber($consultaCliente[0]->CGC) : '',
          'im' => '',
          'razao_social' => $consultaCliente[0]->RazaoSocial,
          'email' => $consultaCliente[0]->EMail !== NULL ? $consultaCliente[0]->EMail : '',
          'endereco' => [
            'logradouro' => sprintf('%s %s', $consultaCliente[0]->descricaoLogradouro, $consultaCliente[0]->Endereco),
            'numero' => $consultaCliente[0]->numeroEndereco,
            'bairro' => $consultaCliente[0]->Bairro,
            'codigo_municipio' => $consultaCliente[0]->Cidade,
            'uf' => $consultaCliente[0]->Estado,
            'cep' => $objCommon->onlyNumber($consultaCliente[0]->CEP),
            'telefone' => $objCommon->onlyNumber($consultaCliente[0]->Telefone1)
          ],
        ],
        'servico' => ['itens' => $arrItens],
        'intermediario' => [
          "cnpj" => $objCommon->onlyNumber($consultaEmpresa->CGC),
          'codigo_municipio' => 4202404,
          "cpf" => null,
          "im" => null,
          "razao_social" => $consultaEmpresa->RazaoSocial
        ]
      ];

      $retorno = $this->objIntegraNota->cria($dadosNota);
      $chave = isset($retorno->chave) ? $retorno->chave : '';
      $codigo_retorno = $retorno->codigo;

      if (in_array($retorno->codigo, [5001, 5002])) {
        throw new ErrorException(json_encode($retorno->erros));
      }

      if (in_array($retorno->codigo, [5014])) {
        throw new ErrorException('Não encontrado nenhum certificado para este emitente');
      }

      if (in_array($retorno->codigo, [5019])) {
        throw new ErrorException($retorno->mensagem);
      }      

      if ($retorno->sucesso) {
        $this->objFilaDocumentoFiscal->cadastrarFilaDocumentoFiscal(
          [
            'id_documento' => $chave,
            'id_pedido' => $id_pedido,
            'payload' =>  json_encode($dadosNota),
            'retorno' => json_encode($retorno->mensagem, JSON_NUMERIC_CHECK),
            'id_empresa' => $id_empresa,
            'id_usuario_criacao' => $_SESSION['handle'],
            'data_criacao' => $this->objFilaDocumentoFiscal->data_criacao,
            'situacao' => 'PROCESSANDO',
            'id_tipo_documento' => 2
          ]
        );

        $objEmpresas->editarEmpresa(
          [
            'handle' => $consultaEmpresa->CodigoEmpresa,
            'numero_sequencia_nfs' => ((int) $numero_sequencia_nf + 1)
          ]
        );
      }

      echo json_encode(
        [
          'success' => '1',
          'msg' => $retorno->mensagem,
          'id_documento' => $chave,
          'codigo_retorno' => $codigo_retorno
        ]
      );
    } catch (Exception $e) {

      $arrExcecao = json_decode($e->getMessage(), true);

      if (!is_null($arrExcecao)) {
        $arrRespostaErro = [];
        foreach ($arrExcecao as $erro) {
          $arrRespostaErro[] = sprintf('%s - %s - %s', $erro['campo'], $erro['erro'], $erro['descricao']);
        }

        echo json_encode(
          [
            'success' => '0',
            'msg' => $arrRespostaErro,
            'codigo_retorno' => $codigo_retorno
          ]
        );
        return;
      }

      echo json_encode(
        [
          'success' => '0',
          'msg' => $e->getMessage()
        ]
      );
    }
  }

  public function emitirDocumento($id_pedido = 0, $id_empresa = 1)
  {

    try {
      $objCliente = new Cliente();
      $objCommon = new CommonController();
      $objEmpresas = new Empresas();
      $objOrcamento = new Orcamento();
      $objItemOrcamento = new ItemOrcamento();
      $objMolduraItemOrcamento = new Moldura_Item_Orcamento();
      $objComponenteItemOrcamento = new Componente_Item_Orcamento();
      $objConfiguracoes = new Configuracoes;

      $ocultar_componentes_impressao_documento_fiscal = filter_var($objConfiguracoes->listarConfiguracao('ocultar_componentes_impressao_documento_fiscal')->valor, FILTER_VALIDATE_BOOLEAN);
      $modelo_empresa = $objConfiguracoes->listarConfiguracao('modelo_empresa')->valor;

      $this->alteraObjetoIntegraNota($id_empresa);

      $parametros = $objCommon->validateGet('parametros');
      if ($parametros) {
        preg_match_all('/^(.*)=(\d)$/m', $parametros, $matches, PREG_SET_ORDER, 0);
        if (isset($matches[0][1]) && $matches[0][1] == 'IdEmpresa') {
          $id_empresa = $matches[0][2];
        }
      }

      $consultaEmpresa = $objEmpresas->ListarEmpresa($id_empresa);
      $consultaOrcamento = $objOrcamento->listarOrcamento($id_pedido);

      $numero_sequencia_nf = $consultaEmpresa->ambiente_gerar_nota != '2' ? $consultaEmpresa->numero_sequencia_nf : date('zHmi');
      $serie_nf = $consultaEmpresa->ambiente_gerar_nota != '2' ? $consultaEmpresa->serie_nf : 889;

      $dadosNota = [];

      if (sizeof($consultaOrcamento) == 0) {
        throw new ErrorException('Pedido ' . $id_pedido . ' não encontrado');
      }

      $consultaDocumentoPedido = $this->objFilaDocumentoFiscal->listarFilaDocumentoFiscal(['id_pedido' => $id_pedido]);
      if (isset($consultaDocumentoPedido->id_fila) && in_array($consultaDocumentoPedido->situacao, ['CONCLUIDO', 'PROCESSANDO'])) {
        throw new ErrorException('Este pedido tem uma nota fiscal emitida. Status: ' . $consultaDocumentoPedido->situacao);
      }


      $itemsOrcamento = $objItemOrcamento->listarItemOrcamento($id_pedido);
      /**
       * Verificando quantos produtos (molduras, escoras, etc) e componentes foram
       * selecionados para este pedido
       */
      $consultaTotalItensMolduraComponentes = $objItemOrcamento->consultaTotalMolduraComponentes($id_pedido, $ocultar_componentes_impressao_documento_fiscal);
      $total_itens = count($consultaTotalItensMolduraComponentes); //$consultaTotalItensMolduraComponentes->total_itens;

      $consultaCliente = $objCliente->listarCliente($consultaOrcamento[0]->Cd_Cliente);
      $idIntegracao = sprintf('%s%s', substr(preg_replace('/0/', '', $objCommon->onlyNumber($consultaEmpresa->CGC)), 0, 3), $id_pedido);

      $arrItens = [];
      if (count($itemsOrcamento) == 0) {
        throw new ErrorException('Pedido sem produtos vinculados');
      }


      $vl_desconto = $consultaOrcamento[0]->valorDescontoFinal ?: 0;
      $vl_desconto_por_item = 0;
      if ($total_itens > 0) {
        $vl_desconto_por_item = (float) round(number_format($vl_desconto / $total_itens, 4), 2);
        // echo "$vl_desconto / $total_itens: $vl_desconto_por_item";
      }

      $codigo_uf_nota = $consultaCliente[0]->codigo_uf > 0 ? $consultaCliente[0]->codigo_uf : 0;
      if ($codigo_uf_nota == 0) {
        throw new ErrorException('O endereço do cliente incorreto. Verifique no cadastro do cliente.');
      }

      if ($objCommon->onlyNumber($consultaCliente[0]->CGC) == '') {
        throw new ErrorException('Documento (CNPJ ou CPF) inválido. Verifique no cadastro do cliente.');
      }

      $valor_aliquota = "2.00";
      $valor_pis = 0;
      $valor_confins = 0;
      $valor_inss = 0;
      $valor_ir = 0;
      $valor_csll = 0;
      $valor_outras = 0;
      $valor_desconto_incondicionado = 0;

      $arrItens[] = [
        /**
         * 01.04 - Elaboração de programas de computadores, inclusive de jogos eletrônicos
         * 07.06 - Colocação e instalação de tapetes, carpetes, assoalhos, cortinas, revestimentos de parede, vidros, divisórias, placas de gesso e congêneres, com material fornecido pelo tomador do serviço.
         */
        "codigo" => $modelo_empresa == 'gestao-interna' ? '01.04' : "07.06",
        'codigo_cnae' => $modelo_empresa == 'gestao-interna' ? '6201501' : "4330402",
        "discriminacao" => $modelo_empresa == 'gestao-interna' ? "PAGAMENTO REFERENTE A UTILIZAÇÃO DO MAESTRIA SISTEMA." : (trim($consultaOrcamento[0]->Ds_Observacao_Pedido) !== '' ? $this->tirarAcentos($consultaOrcamento[0]->Ds_Observacao_Pedido) : 'REFERENTE A SERVICO DE EMOLDURAMENTO E SIMILARES'),
        "valor_servicos" => (float) round(($consultaOrcamento[0]->Vl_Bruto - $consultaOrcamento[0]->valorDescontoFinal), 2),
        "valor_liquido" => (float) round(($consultaOrcamento[0]->Vl_Bruto - $consultaOrcamento[0]->valorDescontoFinal), 2),
        "valor_pis" => $valor_pis,
        "valor_cofins" => $valor_confins,
        "valor_inss" => $valor_inss,
        "valor_ir" => $valor_ir,
        "valor_csll" => $valor_csll,
        "valor_outras" => $valor_outras,
        // "valor_aliquota" => $valor_aliquota,
        "valor_desconto_incondicionado" => $valor_desconto_incondicionado,
        "iss_retido" => false,
        "valor_base_calculo" => (float) round(($consultaOrcamento[0]->Vl_Bruto - $consultaOrcamento[0]->valorDescontoFinal), 2)
      ];

      $dadosNota = [
        'natureza_operacao' => '1',
        'numero' => $numero_sequencia_nf !== '' ? (string) $numero_sequencia_nf : '1',
        'serie' => $serie_nf !== '' ? (string) $serie_nf : '1',
        'tipo' => 1,
        'status' => 1,
        'data_emissao' => date('Y-m-d\TH:i:s-03:00'),
        'tomador' => [
          'cpf' => $consultaCliente[0]->TipoCliente == 'F' ? $objCommon->onlyNumber($consultaCliente[0]->CGC) : '',
          'cnpj' => $consultaCliente[0]->TipoCliente == 'J' ? $objCommon->onlyNumber($consultaCliente[0]->CGC) : '',
          'im' => '',
          'razao_social' => $consultaCliente[0]->RazaoSocial,
          'email' => $consultaCliente[0]->EMail !== NULL ? $consultaCliente[0]->EMail : '',
          'endereco' => [
            'logradouro' => sprintf('%s', $consultaCliente[0]->Endereco),
            'numero' => $consultaCliente[0]->numeroEndereco,
            'bairro' => $consultaCliente[0]->Bairro,
            'codigo_municipio' => $consultaCliente[0]->Cidade,
            'uf' => $consultaCliente[0]->Estado,
            'cep' => $objCommon->onlyNumber($consultaCliente[0]->CEP),
            'telefone' => $objCommon->onlyNumber($consultaCliente[0]->Telefone1)
          ],
        ],
        'servico' => ['itens' => $arrItens, 'codigo_municipio' => '4208203'],
        // 'intermediario' => [
        //   "cnpj" => $objCommon->onlyNumber($consultaEmpresa->CGC),
        //   'codigo_municipio' => 4202404,
        //   "cpf" => null,
        //   "im" => null,
        //   "razao_social" => $consultaEmpresa->RazaoSocial
        // ]
      ];


      if ($this->validateGet('debug') == true) {
        echo "<pre>".json_encode($dadosNota, JSON_PRETTY_PRINT)."</pre>";
        exit;
      }

      $retorno = $this->objIntegraNota->cria($dadosNota);
      $chave = isset($retorno->chave) ? $retorno->chave : '';
      $codigo_retorno = $retorno->codigo;

      if (array_key_exists($codigo_retorno, $this->objCommon->arrRejeicoesIntegrNotas)) {
        if (isset($retorno->erros)) {
          throw new ErrorException(json_encode($retorno->erros));
        }

        $chave = isset($retorno->chave) ? $retorno->chave : '';
        throw new ErrorException(str_replace('%CHAVE%',$chave, $this->objCommon->arrRejeicoesIntegrNotas[$codigo_retorno]));
      }

      if (array_key_exists($codigo_retorno, $this->objCommon->arrRejeicoesSefaz)) {
        throw new ErrorException(json_encode($retorno->erros));
      }      

      if (in_array($retorno->codigo, [5005])) {
        throw new ErrorException('Emitente sem o cadastro da Inscrição Estadual');
      }

      if (in_array($retorno->codigo, [5001, 5002])) {
        throw new ErrorException(json_encode($retorno->erros));
      }

      if (in_array($retorno->codigo, [5014])) {
        throw new ErrorException('Não encontrado nenhum certificado para este emitente');
      }      

      /**
       * Verifica se já foi gerado uma nota para esse pedido
       */
      if ($codigo_retorno === '5008') {
      }

      if ($retorno->sucesso) {
        $this->objFilaDocumentoFiscal->cadastrarFilaDocumentoFiscal(
          [
            'id_documento' => $chave,
            'id_pedido' => $id_pedido,
            'payload' =>  json_encode($dadosNota),
            'retorno' => json_encode($retorno->mensagem, JSON_NUMERIC_CHECK),
            'id_empresa' => $id_empresa,
            'id_usuario_criacao' => $_SESSION['handle'],
            'data_criacao' => $this->objFilaDocumentoFiscal->data_criacao,
            'situacao' => 'PROCESSANDO',
            'id_tipo_documento' => 3
          ]
        );

        $objEmpresas->editarEmpresa(
          [
            'handle' => $consultaEmpresa->CodigoEmpresa,
            'numero_sequencia_nf' => ((int) $numero_sequencia_nf + 1)
          ]
        );
      }

      echo json_encode(
        [
          'success' => '1',
          'msg' => $retorno->mensagem,
          'id_documento' => $chave,
          'codigo_retorno' => $codigo_retorno
        ]
      );
    } catch (Exception $e) {

      $arrExcecao = json_decode($e->getMessage(), true);

      if (!is_null($arrExcecao)) {
        $arrRespostaErro = [];
        foreach ($arrExcecao as $erro) {
          $arrRespostaErro[] = sprintf('%s - %s - %s', $erro['campo'], $erro['erro'], $erro['descricao']);
        }

        echo json_encode(
          [
            'success' => '0',
            'msg' => $arrRespostaErro,
            'codigo_retorno' => $codigo_retorno
          ]
        );
        return;
      }

      echo json_encode(
        [
          'success' => '0',
          'msg' => $e->getMessage()
        ]
      );
    }
  }

  public function consultarNota($idNota = 0, $origemRequisicao = '')
  {

    $consultaDocumento = $this->objFilaDocumentoFiscal->listarPorDocumento($idNota);

    $this->alteraObjetoIntegraNota(null, $idNota);
    $payload = [
      'chave' => $idNota
    ];

    $consultaNota = $this->objIntegraNota->consulta($payload);

    $codigo_retorno = $consultaNota->codigo;

    $status = 'PROCESSANDO';
    $mensagem_retorno = $consultaNota->mensagem;
    if (array_key_exists($codigo_retorno, $this->objCommon->arrRejeicoesSefaz)) {
      if ($codigo_retorno == 778) {
        $item_ncm_errado = (int) preg_replace('/Rejeicao: Informado NCM inexistente \[NCM:.*\], \[nItem:(\d)\]\./m', '$1', $consultaNota->mensagem);
        $arrItens = json_decode($consultaDocumento->payload, true);
        $mensagem_retorno = sprintf('Rejeicao: Informado NCM inexistente: %s no item %s', $arrItens['itens'][$item_ncm_errado]['codigo_ncm'] , $arrItens['itens'][$item_ncm_errado]['descricao']);
      }
      
      $status = 'REJEITADO';
    }

    if (array_key_exists($codigo_retorno, $this->objCommon->arrRejeicoesIntegrNotas)) {
      $status = 'REJEITADO';
    }    

    if ((int) $consultaNota->sucesso === 1 && $consultaNota->codigo == '100') {
      $status  = 'Autorizado';
    }

    $link_pdf = isset($consultaNota->link_pdf) ? $consultaNota->link_pdf : '';

    $this->objFilaDocumentoFiscal->editarFilaDocumentoFiscal(
      [
        'handle' => $consultaDocumento->id_fila,
        'situacao' => $status,
        'retorno' => $consultaNota->mensagem,
        'link_pdf' => $link_pdf,
        'numero_nota' => isset($consultaNota->numero) ? $consultaNota->numero : 0
      ]
    );


    $retorno = json_encode([
      'success' => (int) $consultaNota->sucesso,
      'msg' => $consultaNota->mensagem,
      'codigo' => $consultaNota->codigo,
      'status' => $status,
      'link_pdf' => $link_pdf
    ]);



    if ($origemRequisicao === '') {
      echo $retorno;
      return;
    }


    return $retorno;
  }

  public function downloadNota($idNota = 0)
  {

    $this->alteraObjetoIntegraNota(null, $idNota);
    $payload = [
      'chave' => $idNota
    ];
    $retorno = $this->objIntegraNota->pdf($payload);

    $mensagem = $retorno->codigo == 5004 ? 'O emitente não contratou acesso a NFS-e' : $retorno->mensagem;

    $status = $retorno->sucesso;

    if ((bool) !$status) {
      echo sprintf('<p>Erro: <br> %s </p>', $mensagem);
      exit;
    }

    header('Content-type: application/pdf');
    echo base64_decode($retorno->pdf);
    exit;
  }

  public function downloadXML($idNota = 0)
  {

    $payload = [
      'chave' => $idNota
    ];

    $this->alteraObjetoIntegraNota(null, $idNota);

    $retorno = $this->objIntegraNota->consulta($payload);

    $status = $retorno->sucesso;

    if ($status != '1') {
      echo '<h1> Erro ao exibir o XML </h1>';
      $mensagem_erros = '';
      if (!empty($retorno->erros)) {
        $mensagem_erros = json_encode($retorno->erros);
      }
      echo sprintf('<p>%s <br> %s</p>', $retorno->mensagem, $mensagem_erros);
      exit;
    }

    header('Content-type: text/xml');
    header('Content-Disposition: attachment; filename="' . $retorno->numero . '.xml"');
    echo base64_decode($retorno->xml);
    // echo sprintf('<textarea style="height:100%%;width:100%%">%s</textarea>', $dom->saveXML());
    exit;
  }

  public function alteraObjetoIntegraNota($id_empresa = null, $id_documento = null) {


    if (is_null($id_empresa)) {
      $consultaDocumento = $this->objFilaDocumentoFiscal->listarPorDocumento($id_documento);

      $id_empresa = $consultaDocumento->id_empresa;
    }

    $objEmpresas = new Empresas();
    $empresa = $objEmpresas->ListarEmpresa($id_empresa);

    $token_integra_nota = $empresa->token_integra_nota;
    $ambiente_integra_nota = $empresa->ambiente_gerar_nota;

    $params = [
      'token' => $token_integra_nota,
      'ambiente' => $ambiente_integra_nota,
      'options' => [
        'debug' => false,
        'timeout' => 60,
        'port' => 443,
        'http_version' => CURL_HTTP_VERSION_NONE
      ]
    ];

    $this->objIntegraNota = new Nfse($params);
    
  }

  public function cancelarNota($idNota = 0)
  {

    $idNota = $_POST['idNota'];

    $this->alteraObjetoIntegraNota(null, $idNota);
    
    $consultaDocumento = $this->objFilaDocumentoFiscal->listarPorDocumento($idNota);

    $this->alteraObjetoIntegraNota(null, $idNota);

    $payload = [
      'chave' => $idNota,
      'codigo_cancelamento' => 1
    ];

    $consultaNota = $this->objIntegraNota->cancela($payload);

    $status = $consultaNota->sucesso == 1 ? 'Cancelado' : 'Cancelando';

    if ($consultaNota->sucesso == 1) {
      $this->objFilaDocumentoFiscal->editarFilaDocumentoFiscal(
        [
          'handle' => $consultaDocumento->id_fila,
          'situacao' => 'Cancelado',
          'retorno' => $consultaNota->mensagem
        ]
      );

    }

    $retorno = json_encode([
      'success' => (int) $consultaNota->sucesso,
      'msg' => $consultaNota->mensagem,
      'codigo' => $consultaNota->codigo,
      'status' => $status
    ]);

    echo $retorno;
  }  

  public function recuperarIdEmpresa() {
    $parametros = $this->objCommon->validateGet('parametros');
    if ($parametros) {
      preg_match_all('/^(.*)=(\d)$/m', $parametros, $matches, PREG_SET_ORDER, 0);
      if (isset($matches[0][1]) && $matches[0][1] == 'IdEmpresa') {
        return $matches[0][2];
      }
    }
    return null;
  }

}
