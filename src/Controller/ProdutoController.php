<?php
require_once './src/Model/Produto.php';
require_once './src/Model/UnidadeMedida.php';
require_once './src/Model/UnidadeMedida.php';
require_once './src/Model/Grupo.php';
require_once './src/Model/Fornecedor.php';
require_once './src/Model/ClienteGrupo.php';
require_once './src/Model/PercentualCustoClienteGrupo.php';
require_once './src/Model/Complexidade.php';
require_once './src/Model/ComplexidadeProduto.php';
require_once './src/Controller/CommonController.php';
require_once './src/Controller/CurlController.php';
require_once './src/Model/Configuracoes.php';
require_once './src/Model/GrupoTributario.php';
class ProdutoController extends CommonController
{

  private $modulos = array();
  private $estados = array();
  private $classe = 'Produto';
  private $breadcrumb = array();
  private $titulo_principal = '';

  private $clientesGrupo = array();

  private $complexidades = array();

  private $habilita_tributacao = false;

  private $parceria_ruberti = false;

  private $desabilitar_controle_estoque = false;

  private $gruposTributarios = array();

  private $permissoes;

  public function __construct()
  {
    $produto = new Produto();
    $common = new CommonController();
    $modulos = $common->getModulos();
    $estados = $common->getEstados();
    $configuracoes = new Configuracoes();

    $objGruposPermissoes = new GruposPermissoes();
	  $handle_grupo = isset($_SESSION['handle_grupo']) ? $_SESSION['handle_grupo'] : 0;
	  $this->permissoes = $objGruposPermissoes->listarPorGrupo($handle_grupo, 'Produto')[0];

    $this->modulos = $modulos;
    $this->estados = $estados;

    $this->habilita_tributacao = filter_var($configuracoes->listarConfiguracao('habilita_tributacao')->valor, FILTER_VALIDATE_BOOLEAN);
    $this->parceria_ruberti = filter_var($configuracoes->listarConfiguracao('parceria_ruberti')->valor, FILTER_VALIDATE_BOOLEAN);
    $this->desabilitar_controle_estoque = filter_var($configuracoes->listarConfiguracao('desabilitar_controle_estoque')->valor, FILTER_VALIDATE_BOOLEAN);
    $modulo_posicao = array_search($this->classe, array_column($modulos, 'modulo'));
    $this->titulo_principal = $modulos[$modulo_posicao];
    $this->breadcrumb = array('Maestria' => URL . 'dashboard/index/', $this->titulo_principal['descricao'] => URL . $this->classe . '/listar/');

    $clienteGrupo = new ClienteGrupo();
    $this->clientesGrupo = $clienteGrupo->listarTodos(0, 0, 'status', '1');

    $complexidade = new Complexidade();
    $this->complexidades = $complexidade->listarTodos(0, 0, 'status', '1');

    $objGrupoTributario = new GrupoTributario();
    $this->gruposTributarios = $objGrupoTributario->listarTodos();
  }

  public function listar()
  {
    $produto = new Produto();
    $common = new CommonController();
    $UnidadeMedida = new UnidadeMedida();
    $fornecedor = new Fornecedor();

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;
    $modulos = $this->modulos;
    $classe = $this->classe;
    $msg_sucesso = '';
    $tipo_mensagem = 'callout-success';
    $unidades = $UnidadeMedida->listarTodos();
    $fornecedores = $fornecedor->listarTodos();

    $usuario_master = (isset($_SESSION['usuario_master']) && $_SESSION['usuario_master'] == 1);
    if (isset($_SESSION['mensagem']) && !empty($_SESSION['mensagem'])) {
      $msg_sucesso = $_SESSION['mensagem'];

      if (isset($_SESSION['tipoMensagem']) && !empty($_SESSION['tipoMensagem'])) {
        $tipo_mensagem = $_SESSION['tipoMensagem'];
      }

      unset($_SESSION['mensagem']);
      unset($_SESSION['tipoMensagem']);
    }



    require './src/View/Produto/produto_listar.php';
  }

  public function editar($handle)
  {
    $msg_sucesso = '';
    $metodo = $acao = 'editar';
    $produto = new Produto();
    $fornecedor = new Fornecedor();
    if (isset($_POST) && !empty($_POST)) {
      $retorno = $produto->editarProduto($_POST);
      if ($retorno) {
        $msg_sucesso = $this->classe . ' alterado com sucesso.';
      }
    }

    $UnidadeMedida = new UnidadeMedida();
    $unidades = $UnidadeMedida->listarTodos();

    $grupo = new Grupo();
    $grupos = $grupo->listarTodos();

    $produtos = $produto->listarProduto($handle);

    $consultaProdutoEmPedido = $produto->produtoEmPedido($handle);
    $podeExcluir = isset($consultaProdutoEmPedido[0]->total) ? false : true;

    $historicoCompra = $produto->listarHistoricoCompra($handle);

    $fornecedores = $fornecedor->listarTodos();

    $clientesGrupo = $this->clientesGrupo;

    $complexidades = $this->complexidades;

    $percentualCustoClienteGrupo = new PercentualCustoClienteGrupo();
    $arrPercentualGrupo = $percentualCustoClienteGrupo->listarPercentualPorProduto($handle);

    $complexidadeProduto = new ComplexidadeProduto();
    $arrComplexidadeProduto = $complexidadeProduto->listarComplexidadePorProduto($handle, ['status' => 1]);

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;
    $modulos = $this->modulos;
    $classe = $this->classe;
    $acao = "editar";
    $habilita_tributacao = $this->habilita_tributacao;

    $readonly = $this->permissoes->editar == 0 ? 'readonly' : '';
    require './src/View/Produto/produto_form.php';
  }

  public function excluir($handle)
  {
    $msg_sucesso = '';
    $produtos = '';
    $metodo = 'cadastrar';

    $produto = new Produto();
    $consultaProdutoEmPedido = $produto->produtoEmPedido($handle);

    $_SESSION['mensagem'] = 'Erro ao excluir o produto.';
    $_SESSION['tipoMensagem'] = 'callout-danger';
    if ($consultaProdutoEmPedido[0]->total == 0) {
      $produto->excluirProduto($handle);
      $_SESSION['tipoMensagem'] = 'callout-success';
      $_SESSION['mensagem'] = 'Produto excluído com sucesso.';
    }
    Header('Location: ' . URL . 'Produto/listar/');
    exit();
  }

  public function cadastrar()
  {
    $msg_sucesso = '';
    $produtos = '';
    $metodo = 'cadastrar';

    $produto = new Produto();
    $fornecedor = new Fornecedor();

    $UnidadeMedida = new UnidadeMedida();
    $unidades = $UnidadeMedida->listarTodos();

    $fornecedores = $fornecedor->listarTodos();

    $historicoCompra = $produto->listarHistoricoCompra(0);

    $grupo = new Grupo();
    $grupos = $grupo->listarTodos();
    $produtos = array($produto);

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;
    $modulos = $this->modulos;
    $classe = $this->classe;
    $acao = "cadastrar";
    $arrPercentualGrupo = [];
    if (isset($_POST) && !empty($_POST)) {
      $retorno = $produto->cadastrarProduto($_POST);
      if (is_numeric($retorno)) {
        $msg_sucesso = 'Produto cadastrado com sucesso.';
        $handle = $retorno;
        $acao = "editar";
        $produtos = $produto->listarProduto($handle);
        $historicoCompra = $produto->listarHistoricoCompra($handle);
        $percentualCustoClienteGrupo = new PercentualCustoClienteGrupo();
        $arrPercentualGrupo = $percentualCustoClienteGrupo->listarPercentualPorProduto($handle);
        $complexidadeProduto = new ComplexidadeProduto();
        $arrComplexidadeProduto = $complexidadeProduto->listarComplexidadePorProduto($handle, ['status' => 1]);
      } else {
        echo $retorno;
        exit;
      }
    }

    $clientesGrupo = $this->clientesGrupo;
    $complexidades = $this->complexidades;
    $habilita_tributacao = $this->habilita_tributacao;
    $readonly = $this->permissoes->editar == 0 ? 'readonly' : '';

    require './src/View/Produto/produto_form.php';
  }

  public function alterarPrecoEmMassa()
  {
    $produto = new Produto();
    if (!$_POST) {
      echo json_encode(array('success' => 0));
      return false;
    }

    $alteracao = $this->validatePost('alteracao');
    if (!$alteracao) {
      echo json_encode(array('success' => 0, 'message' => "Não foi informado o campo alteração"));
      return false;
    }

    $valor = $this->validatePost('valor');
    if (!$valor) {
      echo json_encode(array('success' => 0, 'message' => "Não foi informado o campo valor"));
      return false;
    }

    $tipo = $this->validatePost('tipo');
    if (!$tipo) {
      echo json_encode(array('success' => 0, 'message' => "Não foi informado o campo tipo"));
      return false;
    }

    $tipo_valor = $this->validatePost('tipo_valor');

    $fornecedor = $this->validatePost('fornecedor');

    $retorno = $produto->alterarPrecoEmMassa(
      [
        'alteracao' => $alteracao == 'acrescentar' ? '+' : '-',
        'valor' => $valor,
        'tipo' => $tipo,
        'tipo_valor' => $tipo_valor,
        'fornecedor' => $fornecedor
      ]
    );
    if ($retorno) {
      echo json_encode(array('success' => 1, 'message' => "Preços atualizados com sucesso. A tela será recarregada com os novos preços"));
      return true;
    }
  }

  public function listaProdutosDataTables()
  {
    $objProduto = new Produto();

    $draw = $this->validateGet('draw') ?: 1;
    $length = $this->validateGet('length') ?: 10;
    $start = $this->validateGet('start') ?: 1;
    $searchValue = isset($_GET['search']['value']) ? $_GET['search']['value'] : '';
    $arrFiltro = [];
    if (!empty($searchValue)) {
      $arrFiltro = [
        'NovoCodigo' => $searchValue,
        'DescricaoProduto' => $searchValue
      ];
    }
    $order = '';
    if (isset($_GET['order']) && is_array($_GET['order'])) {
      $order = $_GET['columns'][$_GET['order'][0]['column']]['data'] . ' ' . strtoupper($_GET['order'][0]['dir']);
    }

    if (isset($_GET['filtro_componente']) && !empty($_GET['filtro_componente'])) {
      $arrFiltro = array_merge(
        $arrFiltro,
        [
          'componente' => $_GET['filtro_componente']
        ]
      );
    }

    if (isset($_GET['filtro_unidade_medida']) && !empty($_GET['filtro_unidade_medida'])) {
      $arrFiltro = array_merge(
        $arrFiltro,
        [
          'unidade_medida' => $_GET['filtro_unidade_medida']
        ]
      );
    }

    if (isset($_GET['filtro_fornecedor']) && !empty($_GET['filtro_fornecedor'])) {
      $arrFiltro = array_merge(
        $arrFiltro,
        [
          'fornecedor' => $_GET['filtro_fornecedor']
        ]
      );
    }
    if (isset($_GET['filtro_situacao_produto']) && ($_GET['filtro_situacao_produto'] !== '')) {
      $arrFiltro = array_merge(
        $arrFiltro,
        [
          'situacao' => array($_GET['filtro_situacao_produto'])
        ]
      );
    }

    $arrRetornoProdutos = $objProduto->listarTodos(1, ($start - 1), '', '', '', $arrFiltro, $length);
    $num_registros = $objProduto->listarTodosTotal($arrFiltro);
    $arrProdutos = array();
    foreach ($arrRetornoProdutos as $produto) {

      $editar = '<a class="btn btn-app" href="' . URL . $this->classe . '/editar/' . $produto->CodigoProduto . '"><i class="fa fa-edit"></i>'.($this->permissoes->editar == 1 ? 'Editar':'Visualizar').'</a>';
      $excluir = $this->permissoes->excluir == 1 ? '<a class="btn btn-app excluirProduto" produtoId="' . $produto->CodigoProduto . '"  href="#"><i class="fa fa-trash"></i>Excluir</a>' : '';
      if ($produto->qtdUso > 0) {
        $excluir = '<a class="btn btn-app naoExcluirProduto" style="opacity: 0.4;"><i class="fa fa-trash"></i>Excluir</a>';
      }

      $unidade_medida = 'CM';
      switch ($produto->DescricaoUnidadeMedida) {
        case 'UN':
        case 'M2':
          $unidade_medida = $produto->DescricaoUnidadeMedida;
          break;
      }

      $quantidade_exibir = $produto->MetragemLinear == '1' ? $produto->QuantidadeCm . ' CM' : $produto->Quantidade . ' ' .  $unidade_medida;

      $arrProdutos[] = array(
        'NovoCodigo' => $produto->NovoCodigo,
        'DescricaoProduto' => $produto->DescricaoProduto,
        'Quantidade' => $quantidade_exibir,
        'Desenho' => $produto->DescricaoUnidadeMedida != 'UN' ? $produto->Desenho . ' CM' : '<i>Não se aplica</i>',
        'PrecoVendaMaoObra' => 'R$ ' . number_format($produto->PrecoVendaMaoObra, 2),
        'Comprar' => '<a class="btn btn-app" href="https://loja.rubertimolduras.com.br/" target="_blank"><i class="fa fa-shopping-cart"></i>Comprar online</a>',
        'Editar' => $editar,
        'Excluir' => $excluir
      );
    }
    $arrRetorno = array(
      'draw' => $draw,
      'recordsTotal' => $num_registros,
      'recordsFiltered' => $num_registros,
      'data' => $arrProdutos
    );

    echo json_encode($arrRetorno);
    return;
  }

  public function exportar()
  {


    $objProdutos = new Produto();

    $consultaProdutos = $objProdutos->listarTodos($pagina_atual = 0, $linha_inicial = 0, $coluna = '', $buscar = '', $order = "DescricaoProduto ASC ", $arrayFiltro = array(), $quantidade = 0);

    $arrListaProdutos[] = [
      'Codigo do produto',
      'Codigo do fabricante',
      'Codigo NCM',
      'CNPJ fornecedor',
      'Descrição',
      'Unidade de medida',
      'Medida Vara',
      'Quantidade em estoque (UN)',
      'Quantidade em estoque (CM)',
      'Preço de custo',
      'Preço de venda',
      'Fator de perda (cm)',
      'Situação - ATIVO/INATIVO/DESCONTINUADO'
    ];
    foreach ($consultaProdutos as $produto) {
      $arrListaProdutos[] = [
        'codigo' => $produto->CodigoProduto,
        'codigo_fabricante' => $produto->CodigoProdutoFabricante,
        'codigo_ncm' => $produto->codigo_ncm,
        'cnpj_fornecedor' => $produto->CGC,
        'descricao' => $produto->DescricaoProduto,
        'unidade_medida' => $produto->DescricaoUnidadeMedida,
        'medida_vara' => $produto->MedidaVara,
        'quantidade_estoque_unitario' => $produto->Quantidade,
        'quantidade_estoque_cm' => $produto->QuantidadeCm,
        'preco_custo' => $produto->PrecoCusto,
        'preco_venda' => $produto->PrecoVenda,
        'fator_perda' => $produto->Desenho,
        'situacao' => $produto->Situacao == '0' ? 'INATIVO' : ($produto->Situacao == '1' ? 'ATIVO' : 'DESCONTINUADO')
      ];
    }

    $xlsx = Shuchkin\SimpleXLSXGen::fromArray($arrListaProdutos);
    $xlsx->downloadAs('produtos.xlsx');
  }

  public function importar()
  {
    try {

      $objProduto = new Produto();
      $objCommon = new CommonController();
      $objFornecedor = new Fornecedor();
      $objUnidadeMedida = new UnidadeMedida();
      $objCurl = new CurlController();
      $arrArquivo = $_FILES['files'];
      $temp_arquivo = $arrArquivo['tmp_name'][0];
      $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
      $reader->setReadEmptyCells(false);
      $reader->setReadDataOnly(true);
      $planilha = $reader->load($temp_arquivo);

      if ($_POST['excluirProdutos'] == 'S') {
        $objProduto->excluirTodos();
      }

      $conteudoPlanilha = $planilha->getActiveSheet()->toArray();
      $consultaUnidadesMedida = $objUnidadeMedida->listarTodos();

      $arrUnidadeMedida = [];
      foreach ($consultaUnidadesMedida as $unidade_medida) {
        $arrUnidadeMedida[$unidade_medida->DescricaoUnidadeMedida] = $unidade_medida->CodigoUnidadeMedida;
      }
      $arrUnidadeMedida['PC'] = 3;

      $arrSituacaoProduto = ['ATIVO' => 1, 'INATIVO' => 0, 'DESCONTINUADO' => 2];

      $aux = 0;
      $retornoErros = [];
      foreach ($conteudoPlanilha as $linha => $conteudo) {


        if ($linha === 0) {
          continue;
        }

        $fornecedor = $conteudo[3];
        $descricao = !is_null($conteudo[4]) ? strtoupper(strtolower($conteudo[4])) : '';
        $moldura = str_starts_with($descricao, 'MOL');
        $unidade_medida = !empty($conteudo[5]) ? strtoupper($conteudo[5]) : 'ML';
        $unidadeMedida = $moldura ? 'ML' : strtoupper(strtolower($unidade_medida));
        $situacao = !empty($conteudo[12]) ? strtoupper(strtolower(trim($conteudo[12]))) : '';
        if (!isset($arrUnidadeMedida[$unidadeMedida])) {
          throw new ErrorException("Unidade de medida $unidadeMedida não encontrado para o produto $descricao na linha " . ($linha + 1));
        }

        $consultaProduto = [];
        if ($conteudo[0] != '') {
          $consultaProduto = $objProduto->listarProduto($conteudo[0], 'NovoCodigo');
        }

        $cod_fornecedor = 0;
        if ($fornecedor != '') {
          $cnpj_formatado = preg_replace('/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/m', "$1.$2.$3/$4.$5", $fornecedor);
          $consultarFornecedor = $objFornecedor->listarTodos(0, 0, 'CGC', $cnpj_formatado);

          if (count($consultarFornecedor) == 0) {
            $cod_fornecedor = $objFornecedor->cadastrarFornecedor(
              [
                'CGC' => $cnpj_formatado,
                'RazaoSocial' =>  'FORNECEDOR IMPORTADO',
              ]
            );
          } else {
            $cod_fornecedor = $consultarFornecedor[0]->CodigoFornecedor;
          }
        }

        $codigo_ncm = (int) $conteudo[2];

        if (strlen((string) $codigo_ncm) > 8) {
          throw new ErrorException("Codigo NCM $codigo_ncm inválido para o produto $descricao na linha " . ($linha + 1));
        }

        $preco_custo = null;
        if (!is_null($conteudo[9])) {
          $preco_custo = str_replace(".", "", $conteudo[9]);
          $preco_custo = str_replace(",", ".", $preco_custo);
        }

        $preco_venda_mao_obra = null;
        if (!is_null($conteudo[10])) {
          $preco_venda_mao_obra = str_replace(".", "", $conteudo[10]);
          $preco_venda_mao_obra = str_replace(",", ".", $preco_venda_mao_obra);
        }

        $arrProduto = [
          'NovoCodigo' => $conteudo[0],
          'CodigoProdutoFabricante' => $conteudo[1],
          'codigo_ncm' => $codigo_ncm,
          'DescricaoProduto' => $descricao,
          'UnidadeProduto' => $arrUnidadeMedida[$unidadeMedida],
          'MedidaVara' => !is_null($conteudo[6]) ? $conteudo[6] : 0,
          'Quantidade' => $conteudo[7] > 0 ? $conteudo[7] : 9999,
          'QuantidadeCm' => $conteudo[8] > 0 ? $conteudo[8] : 9999,
          'PrecoVenda' => 0,
          'PrecoCusto' => !is_null($preco_custo) ? number_format($preco_custo, 3, ",", "") : 0,
          'PrecoVendaMaoObra' => !is_null($preco_venda_mao_obra) ? number_format($preco_venda_mao_obra, 3, ",", "") : 0,
          'Desenho' => !is_null($conteudo[11]) ? number_format($conteudo[11], 3, ",", "") : 0,
          'Situacao' => isset($arrSituacaoProduto[$situacao]) ? $arrSituacaoProduto[$situacao] : 0,
          'CodigoFornecedor' => $cod_fornecedor,
        ];

        if (count($consultaProduto) == 0) {
          $cadastraProduto =  $objProduto->cadastrarProduto($arrProduto);
          if (!is_numeric($cadastraProduto)) {
            $retornoErros[] = sprintf('Erro ao importar o produto %s: %s', $conteudo[0], $cadastraProduto);
          }
        } else {
          $objProduto->editarProduto(array_merge($arrProduto, ['handle' => $consultaProduto[0]->CodigoProduto]));
        }

        $aux++;
      }

      $mensagem_erros_importacao = '';
      if (count($retornoErros) > 0) {
        $mensagem_erros_importacao = '\n ' . count($retornoErros) . ' deu erro na importação';
      }

      echo json_encode([
        'success' => 1,
        'msg' => $aux - count($retornoErros) . ' produtos importados com sucesso!' . $mensagem_erros_importacao,
        'erros' => $retornoErros
      ], JSON_PRETTY_PRINT);
    } catch (Exception $e) {
      echo json_encode([
        'success' => 0,
        'msg' => $e->getMessage(),
        'erros' => []
      ]);
    } catch (Throwable $e) {
      echo json_encode([
        'success' => 0,
        'msg' => $e->getMessage() . " LINHA " . $linha,
        'erros' => []
      ]);
    }
  }
}
