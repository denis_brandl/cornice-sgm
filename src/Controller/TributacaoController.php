<?php
require_once './src/Controller/CommonController.php';
require_once './src/Model/GrupoTributario.php';
require_once './src/Model/Estado.php';
require_once './src/Model/OperacaoFiscal.php';
require_once './src/Model/Tributacao.php';
class TributacaoController extends CommonController
{

  private $modulos = array();
  private $classe = 'Tributacao';
  private $breadcrumb = array();
  private $titulo_principal = '';

  private $arrConfiguracaoGlobal = [
    ['codigo' => 'cfop', 'sigla' => 'CFOP', 'descricao' => 'Código Fiscal de Operações e Prestações'],
  ];

  private $arrTributacaoICMS = [
    ['codigo' => 'csosn', 'sigla' => 'CSOSN', 'descricao' => 'Código de Situação da Operação do Simples Nacional'],
    [
      'codigo' => 'csticms',
      'sigla' => 'CST',
      'descricao' => 'Código de Situação Tributária',
      'valorPadrao' => '00',
      'opcoes' => [
        '101' => 'Tributação pelo Simples com Permissão de Crédito',
        '102' => 'Tributação pelo Simples sem Permissão de Crédito',
        '103' => 'Isenção do ICMS no Simples para receita bruta',
        '201' => 'Simples Nacional com Permissão de Crédito e ICMS por Substituição Tributária',
        '202' => 'Simples Nacional sem Permissão de crédito e com cobrança de ICMS por substituição tributária',
        '203' => 'Isenção do ICMS no Simples para faixa da Receita Bruta e com cobrança de ICMS por substituição tributária',
        '300' => 'Imunidade',
        '400' => 'Não tributado pelo Simples',
        '500' => 'ICMS cobrado anteriormente por substituição',
        '900' => 'Outros'
      ]
    ],
    ['codigo' => 'icmsst', 'sigla' => 'ICMS-ST', 'descricao' => 'Substituição Tributária do ICMS'],
    // ['codigo' => 'cstb', 'sigla' => 'CST_B', 'descricao' => ''],
    // ['codigo' => 'modalidade_bc_icms', 'sigla' => 'Modalidade BC ICMS', 'descricao' => ''],
    ['codigo' => 'icms', 'sigla' => 'Alíquota', 'descricao' => ''],
    // ['codigo' => 'valor_pauta', 'sigla' => 'Valor Pauta', 'descricao' => ''],
    // ['codigo' => 'percentual_bc', 'sigla' => 'Percentual BC', 'descricao' => ''],
    // ['codigo' => 'mva', 'sigla' => 'MVA', 'descricao' => ''],
    // ['codigo' => 'modalide_bc_st', 'sigla' => 'Modalidade BC ST', 'descricao' => ''],
    // ['codigo' => 'aliquota_icms_st', 'sigla' => 'Alíquota ICMS ST', 'descricao' => ''],
    // ['codigo' => 'aliquota_interna_st', 'sigla' => 'Alíquota Interna ST', 'descricao' => ''],
    // ['codigo' => 'aliquota_interestadual_st', 'sigla' => 'Alíquota Interestadual ST', 'descricao' => ''],
    // ['codigo' => 'percentual_bc_st', 'sigla' => 'Percentual BC ST ', 'descricao' => ''],
    // ['codigo' => 'percentual_st_simples_nacional', 'sigla' => 'Percentual ST Simples Nacional', 'descricao' => ''],
    // ['codigo' => 'aliquota_fundo_combate_pobreza', 'sigla' => 'Alíquota Fundo Combate a Pobreza', 'descricao' => '' ]
  ];

  private $arrTributacaoIPI = [
    [
      'codigo' => 'cstipi',
      'sigla' => 'CST - IPI',
      'descricao' => 'Código da situação tributária',
      'opcoes' => [
        '50' => 'Saída tributada',
        '51' => 'Saída tributada com alíquota zero',
        '52' => 'Saída isenta',
        '53' => 'Saída não-tributada',
        '54' => 'Saída imune',
        '55' => 'Saída com suspensão',
        '99' => 'Outras saídas'
      ]
    ],
    ['codigo' => 'ipi', 'sigla' => 'Alíquota', 'descricao' => 'Alíquota do IPI'],
  ];

  private $arrTributacaoPIS = [
    [
      'codigo' => 'cstpis',
      'sigla' => 'CST - PIS',
      'descricao' => 'Código da situação tributária',
    ],
    ['codigo' => 'pis', 'sigla' => 'Alíquota', 'descricao' => 'Alíquota do PIS'],
  ];  

  private $arrTributacaoConfins = [
    [
      'codigo' => 'cstconfins',
      'sigla' => 'CST - Confins',
      'descricao' => 'Código da situação tributária',
    ],
    ['codigo' => 'confins', 'sigla' => 'Alíquota', 'descricao' => 'Alíquota do CONFINS'],
  ];

  public function __construct()
  {
    $grupo_tributario = new GrupoTributario();
    $common = new CommonController();

    $this->modulos = $common->getModulos();

    $modulo_posicao = array_search($this->classe, array_column($this->modulos, 'modulo'));
    $this->titulo_principal = $this->modulos[$modulo_posicao];
    $this->breadcrumb = array('Maestria' => URL . 'dashboard/index/', $this->titulo_principal['descricao'] => URL . $this->classe . '/listar/');
  }

  public function listar()
  {
    $objGrupoTributario = new GrupoTributario();
    $common = new CommonController();
    $objTributacao = new Tributacao();

    $coluna = '';
    $buscar = '';
    $pagina_atual = 1;
    if ($this->validateGet('parametros')) {
      $re = "/^[a-z]+=/";
      preg_match($re, $this->validateGet('parametros'), $matches);
      $acao = str_replace('=', '', $matches[0]);

      $re = "/=([a-zA-Z].*)\|([A-Za-z0-9].*)$/";
      preg_match($re, $this->validateGet('parametros'), $matches);

      switch ($acao) {
        case 'buscar':
          if (isset($matches[1])) {
            $coluna = str_replace('=', '', $matches[1]);
          }
          if (isset($matches[2])) {
            $buscar = str_replace('=', '', $matches[2]);
          }
          break;

        case 'listar':
          $pagina_atual = str_replace('=', '', $matches[2]);
      }
    }

    $linha_inicial = ($pagina_atual - 1) * QTDE_REGISTROS;

    $num_registros = $objGrupoTributario->listarTodosTotal();

    /* Idêntifica a primeira página */
    $primeira_pagina = 1;

    /* Cálcula qual será a última página */
    $ultima_pagina = ceil($num_registros / QTDE_REGISTROS);

    /* Cálcula qual será a página anterior em relação a página atual em exibição */
    $pagina_anterior = ($pagina_atual > 1) ? $pagina_atual - 1 : 0;

    /* Cálcula qual será a pŕoxima página em relação a página atual em exibição */
    $proxima_pagina = ($pagina_atual < $ultima_pagina) ? $pagina_atual + 1 : 0;

    /* Cálcula qual será a página inicial do nosso range */
    $range_inicial = (($pagina_atual - RANGE_PAGINAS) >= 1) ? $pagina_atual - RANGE_PAGINAS : 1;

    /* Cálcula qual será a página final do nosso range */
    $range_final = (($pagina_atual + RANGE_PAGINAS) <= $ultima_pagina) ? $pagina_atual + RANGE_PAGINAS : $ultima_pagina;

    /* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */
    $exibir_botao_inicio = ($range_inicial < $pagina_atual) ? 'mostrar' : 'esconder';

    /* Verifica se vai exibir o botão "Anterior" e "Último" */
    $exibir_botao_final = ($range_final > $pagina_atual) ? 'mostrar' : 'esconder';

    $arrCamposBusca = array('nome' => 'Nome', 'status' => 'Status', );

    // $grupo_tributarios = $objGrupoTributario->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar);
    $tributacoes = $objTributacao->listarPorGrupoTributario();

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;
    $modulos = $this->modulos;
    $classe = $this->classe;
    require './src/View/Tributacao/tributacao_listar.php';
  }

  public function editar($handle)
  {
    $msg_sucesso = '';
    $metodo = 'editar';
    $objOperacaoFiscal = new OperacaoFiscal();
    $objGrupoTributario = new GrupoTributario();
    $objTributacao = new Tributacao();
    $objEstado = new Estado();

    $arrEstados = $objEstado->listarTodos();
    $arrOperacaoFiscal = $objOperacaoFiscal->listarTodos(0, 0, '', '', 10000);
    $arrGrupoTributario = $objGrupoTributario->listarTodos(0, 0, '', '', 10000);
    if (isset($_POST) && !empty($_POST)) {
      $_POST['handle'] = $handle;
      $retorno = $objTributacao->editarTributacao($_POST);
      if ($retorno) {
        $msg_sucesso = ' Tributação alterada com sucesso.';
      }
    }

    $arrHandle = explode('_', $handle);
    $tributacao = $objTributacao->listarTributacao(
      [
        'id_operacao_fiscal' => $arrHandle[0],
        'id_estado' => $arrHandle[1]
      ]
    );

    // print_r($tributacao);
    $arrTributacaoGrupoTributario = [];
    foreach ($tributacao as $value) {
      $arrTributacaoGrupoTributario[$value->id_grupo_tributario] = [
        'icms' => $value->icms,
        'cfop' => $value->cfop,
        'csosn' => $value->csosn,
        'csticms' => $value->csticms,
        'icmsst' => $value->icmsst,
        'pis' => $value->pis,
        'ipi' => $value->ipi,
        'confins' => $value->confins
      ];
    }

    $arrTributacao =
      [
        'id_tributacao' => $tributacao[0]->id_tributacao,
        'id_operacao_fiscal' => $tributacao[0]->id_operacao_fiscal,
        'id_estado' => $tributacao[0]->id_estado,
        'grupos_tributarios' => $arrTributacaoGrupoTributario,
        'handle' => sprintf('%s_%s', $tributacao[0]->id_operacao_fiscal, $tributacao[0]->id_estado)
      ];
    // print_r($arrTributacao);
    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;
    $modulos = $this->modulos;
    $classe = $this->classe;
    require './src/View/Tributacao/tributacao_form.php';
  }

  public function cadastrar()
  {
    $msg_sucesso = '';
    $grupo_tributarios = '';
    $metodo = 'cadastrar';

    $objOperacaoFiscal = new OperacaoFiscal();
    $objGrupoTributario = new GrupoTributario();
    $objTributacao = new Tributacao();
    $objEstado = new Estado();

    $arrEstados = $objEstado->listarTodos();
    $arrOperacaoFiscal = $objOperacaoFiscal->listarTodos(0, 0, '', '', 10000);
    $arrGrupoTributario = $objGrupoTributario->listarTodos(0, 0, '', '', 10000);

    $tributacao = [
      new $objTributacao()
    ];
    if (isset($_POST) && !empty($_POST)) {
      $retorno = $objTributacao->cadastrarTributacao($_POST);
      if ($retorno) {
        $msg_sucesso = 'Grupo Tributario cadastrado com sucesso.';
        $arrHandle = explode('_', $retorno);
        $tributacao = $objTributacao->listarTributacao(
          [
            'id_operacao_fiscal' => $arrHandle[0],
            'id_estado' => $arrHandle[1]
          ]
        );
      }
    }

    $arrTributacaoGrupoTributario = [];
    foreach ($tributacao as $value) {
      $arrTributacaoGrupoTributario[$value->id_grupo_tributario] = [
        'icms' => $value->icms,
        'cfop' => $value->cfop,
        'csosn' => $value->csosn,
        'csticms' => $value->csticms,
        'icmsst' => $value->icmsst
      ];
    }

    $arrTributacao =
      [
        'id_tributacao' => $tributacao[0]->id_tributacao,
        'id_operacao_fiscal' => $tributacao[0]->id_operacao_fiscal,
        'id_estado' => $tributacao[0]->id_estado,
        'grupos_tributarios' => $arrTributacaoGrupoTributario,
        'handle' => sprintf('%s_%s', $tributacao[0]->id_operacao_fiscal, $tributacao[0]->id_estado)
      ];

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;
    $modulos = $this->modulos;
    $classe = $this->classe;
    require './src/View/Tributacao/tributacao_form.php';
  }

  public function buscaPorEstado($siglaEstado)
  {
    $grupo_tributario = new GrupoTributario();

    $grupo_tributarios = $grupo_tributario->listarGrupoTributariosPorEstado($siglaEstado);

    $arrGrupoTributarios = [];
    foreach ($grupo_tributarios as $value) {
      $arrGrupoTributarios[] = ['codigo_uf' => $value->codigo_uf, 'nome' => $value->nome, 'codigo_ibge' => $value->codigo_ibge];
    }

    echo json_encode($arrGrupoTributarios);
  }
}