
      <?php
        require_once './src/Model/BalanceAmount.php';
        require_once './src/Controller/CommonController.php';
        class BalanceAmountController extends CommonController {
          private $modulos = array();
          private $estados = array();
          private $classe = "Saldo inicial ";
          private $breadcrumb = array();
          private $titulo_principal = "";

          public function __construct() {
            $common = new CommonController();	
            $modulos = $common->getModulos();

            $this->modulos = $modulos;

            $modulo_posicao = array_search($this->classe,array_column($modulos,"modulo"));
            $this->titulo_principal = $modulos[$modulo_posicao];
            $this->breadcrumb = array("Maestria"=>URL."dashboard/index/",$this->titulo_principal["descricao"] => URL.$this->classe."/listar/");
          }

          public function listar() {
            $common = new CommonController();

            $objBalanceAmount = new BalanceAmount();
            $arrBalanceAmount = $objBalanceAmount->listarTodos();

            $titulo_principal = $this->titulo_principal;
            $breadcrumb = $this->breadcrumb;		
            $modulos = $this->modulos;
            $classe = $this->classe;
            $metodo = $acao = "editar";
            require "./src/View/BalanceAmount/balance_amount_listar.php";
          }

          public function editar($handle) {
            $msg_sucesso = "";
            $metodo = "editar";
            $objBalanceAmount = new BalanceAmount();

            if (isset($_POST) && !empty($_POST)) {
              $retorno = $objBalanceAmount->editar($_POST);
              if ($retorno) {
                $msg_sucesso = "Saldo inicial  alterada com sucesso.";
              }
            }

            $BalanceAmount = $objBalanceAmount->listar($handle);

            $titulo_principal = $this->titulo_principal;
            $breadcrumb = $this->breadcrumb;		
            $modulos = $this->modulos;
            $classe = $this->classe;
            require "./src/View/BalanceAmount/balance_amount_form.php";
          }

          public function cadastrar() {
            $msg_sucesso = "";	
            $moedas = "";
            $metodo = "cadastrar";	
            $objBalanceAmount = new BalanceAmount();

            if (isset($_POST) && !empty($_POST)) {
              $retorno = $objBalanceAmount->cadastrar($_POST);
              if ($retorno) {
                $msg_sucesso = "Saldo inicial  cadastrada com sucesso.";
              }
              $BalanceAmount = $objBalanceAmount->listar($retorno);
              $metodo = "editar";
            } else {
              $BalanceAmount = $objBalanceAmount;
            }
            $titulo_principal = $this->titulo_principal;
            $breadcrumb = $this->breadcrumb;		
            $modulos = $this->modulos;
            $classe = $this->classe;
            require "./src/View/BalanceAmount/balance_amount_form.php";	
          }

          public function excluir($handle) {
            $msg_sucesso = "";
            $produtos = "";
            $metodo = "cadastrar";

            $objBalanceAmount = new BalanceAmount();
            $objBalanceAmount->excluir($handle);
            $_SESSION["tipoMensagem"] = "callout-success";
            $_SESSION["mensagem"] = "Saldo inicial  excluído com sucesso.";
            Header("Location: ".URL."Saldo inicial /listar/");
            exit();
          }	          
        }
    