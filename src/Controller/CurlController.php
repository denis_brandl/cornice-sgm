<?php

class CurlController {
  public function callCurl($url, $cabecalho = [], $post = [], $metodo = 'POST', $desabilitaCabecalho = FALSE, $downloadArquivo = false) {
		try {

			$curl = curl_init();
		
			if ($curl === false) {
				throw new Exception('failed to initialize');
			}
			
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
			curl_setopt($curl, CURLOPT_TIMEOUT, 5000);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $metodo);
			if ($metodo == 'POST' || $metodo == 'PUT') {
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
			}
						
			$cabecalho = array_merge( 
							[
								"Content-Type: application/json",
                "x-api-key: " . TOKEN_PLUGNOTAS
							], 
							$cabecalho
						);						
      
			if (!$desabilitaCabecalho) {
				curl_setopt(
					$curl,
					CURLOPT_HTTPHEADER,
					$cabecalho
				);
			}
			
			$response = curl_exec($curl);
			
			if ($response === false) {
				return json_encode([
					'status' => 500,
					'response' => sprintf('Falha ao conectar na API (%s): #%d: %s', $url, curl_error($curl), curl_errno($curl))
				]);
			}
			
			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			
			curl_close($curl);
			
			return json_encode([
				'status' => $status,
				'response' => $downloadArquivo ? bin2hex($response) : $response
			]);
			
		}  catch(Exception $e) {
			return json_encode([
				'status' => 500,
				'response' => sprintf('Falha ao conectar na API: #%d: %s',$e->getCode(), $e->getMessage())
			]);
		}
	}  
}