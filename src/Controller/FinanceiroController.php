<?php
require_once './src/Model/Financeiro.php';
require_once './src/Model/BalanceAmount.php';
require_once './src/Model/Empresas.php';
require_once './src/Controller/CommonController.php';

class FinanceiroController extends CommonController
{
	private $modulos = array();
	private $estados = array();
  private $empresas = array();
	private $classe = "Financeiro";
	private $breadcrumb = array();
	private $titulo_principal = "";

	public function __construct()
	{
		$common = new CommonController();
    $objEmpresa = new Empresas();
		$modulos = $common->getModulos();

		$this->modulos = $modulos;

		$modulo_posicao = array_search($this->classe, array_column($modulos, "modulo"));
		$this->titulo_principal = $modulos[$modulo_posicao];

    $this->empresas = $objEmpresa->listarTodos();
		$this->breadcrumb = array("Maestria" => URL . "dashboard/index/", $this->titulo_principal["descricao"] => URL . $this->classe . "/listar/");
	}

	public function listar()
	{
		$common = new CommonController();

		$objBalanceAmount = new BalanceAmount();
		$consultaSaldosIniciais = $objBalanceAmount->listarTodos();

    $arrConfiguracoesIniciais = [];
    foreach ($consultaSaldosIniciais as $saldo_inicial) {
      switch ($saldo_inicial->tipo) {
        case 0: {
          $arrConfiguracoesIniciais[$saldo_inicial->UserId]['caixa'] = $saldo_inicial->amount;
          break;
        }
        case 1: {
          $arrConfiguracoesIniciais[$saldo_inicial->UserId]['banco'] = $saldo_inicial->amount;
          break;
        }
        case 2: {
          $arrConfiguracoesIniciais[$saldo_inicial->UserId]['cartao'] = $saldo_inicial->amount;
          break;
        }
        case 3: {
          $arrConfiguracoesIniciais[$saldo_inicial->UserId]['aplicacao'] = $saldo_inicial->amount;
          break;
        }
        case 4: {
          $arrConfiguracoesIniciais[$saldo_inicial->UserId]['aberto'] = $saldo_inicial->amount;
          break;
        }
      }
    }

		$titulo_principal = $this->titulo_principal;
		$breadcrumb = $this->breadcrumb;
		$modulos = $this->modulos;
		$classe = $this->classe;
		$metodo = $acao = "editar";
		require "./src/View/Financeiro/financeiro_listar.php";
	}

	public function editar($handle)
	{
		$msg_sucesso = "";
		$metodo = "editar";
		$objBalanceAmount = new BalanceAmount();

    $arr = $objBalanceAmount->listar([
      'coluna' => 'UserId',
      'valor' => $handle
    ]);


		if (isset($_POST) && !empty($_POST)) {
			$retorno = $objBalanceAmount->editar($_POST);
			if ($retorno) {
				$msg_sucesso = "Financeiro alterada com sucesso.";
			}
		}

		$consultaSaldosIniciais = $objBalanceAmount->listar([
      'coluna' => 'UserId',
      'valor' => $handle
    ]);

    $arrConfiguracoesIniciais = [];
    foreach ($consultaSaldosIniciais as $saldo_inicial) {
      switch ($saldo_inicial->tipo) {
        case 0: {
          $arrConfiguracoesIniciais['caixa'] = $saldo_inicial->amount;
          break;
        }
        case 1: {
          $arrConfiguracoesIniciais['banco'] = $saldo_inicial->amount;
          break;
        }
        case 2: {
          $arrConfiguracoesIniciais['cartao'] = $saldo_inicial->amount;
          break;
        }
        case 3: {
          $arrConfiguracoesIniciais['aplicacao'] = $saldo_inicial->amount;
          break;
        }
        case 4: {
          $arrConfiguracoesIniciais['aberto'] = $saldo_inicial->amount;
          break;
        }
      }
    }

		$titulo_principal = $this->titulo_principal;
		$breadcrumb = $this->breadcrumb;
		$modulos = $this->modulos;
		$classe = $this->classe;
		require "./src/View/Financeiro/financeiro_form.php";
	}

	public function cadastrar()
	{
		$msg_sucesso = "";
		$moedas = "";
		$metodo = "cadastrar";
    $handle = 0;
		$objBalanceAmount = new BalanceAmount();

		if (isset($_POST) && !empty($_POST)) {
			$retorno = $objBalanceAmount->cadastrar($_POST);
			if ($retorno) {
				$msg_sucesso = "Financeiro cadastrada com sucesso.";
			}

      $consultaSaldosIniciais = $objBalanceAmount->listar([
        'coluna' => 'UserId',
        'valor' => $_POST['UserId']
      ]);
      $handle = $_POST['UserId'];
      foreach ($consultaSaldosIniciais as $saldo_inicial) {
      switch ($saldo_inicial->tipo) {
        case 0: {
          $arrConfiguracoesIniciais['caixa'] = $saldo_inicial->amount;
          break;
        }
        case 1: {
          $arrConfiguracoesIniciais['banco'] = $saldo_inicial->amount;
          break;
        }
        case 2: {
          $arrConfiguracoesIniciais['cartao'] = $saldo_inicial->amount;
          break;
        }
        case 3: {
          $arrConfiguracoesIniciais['aplicacao'] = $saldo_inicial->amount;
          break;
        }
        case 4: {
          $arrConfiguracoesIniciais['aberto'] = $saldo_inicial->amount;
          break;
        }
      }
    }  
		  $metodo = "editar";
		} else {
      $arrConfiguracoesIniciais = ['caixa' => 0, 'banco' => 0, 'cartao' => 0, 'aplicacao' => 0, 'aberto' => 0];
		}  
    
		$titulo_principal = $this->titulo_principal;
		$breadcrumb = $this->breadcrumb;
		$modulos = $this->modulos;
		$classe = $this->classe;
		require "./src/View/Financeiro/financeiro_form.php";
	}

	public function excluir($handle)
	{
		$msg_sucesso = "";
		$produtos = "";
		$metodo = "cadastrar";

		$objFinanceiro = new Financeiro();
		$objFinanceiro->excluir($handle);
		$_SESSION["tipoMensagem"] = "callout-success";
		$_SESSION["mensagem"] = "Financeiro excluído com sucesso.";
		Header("Location: " . URL . "Financeiro/listar/");
		exit();
	}
}
