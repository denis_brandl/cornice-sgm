<?php
require_once './src/Controller/CommonController.php';
require_once './src/Model/Orcamento.php';
require_once './src/Model/ItemOrcamento.php';
require_once './src/Model/Moldura_Item_Orcamento.php';
require_once './src/Model/Componente_Item_Orcamento.php';
require_once './src/Model/Produto.php';
require_once './src/Model/ProdutoAuxiliar.php';
require_once './src/Model/Componente.php';
require_once './src/Model/Cliente.php';
require_once './src/Model/Empresas.php';
require_once './src/Model/Vendedor.php';
require_once './src/Model/ComposicaoPreco.php';
require_once './src/Model/Situacao.php';
require_once './src/Model/PercentualCustoClienteGrupo.php';
require_once './src/Model/ComplexidadeProduto.php';
require_once './src/Model/ClienteGrupo.php';
require_once './src/Model/Configuracoes.php';
require_once './src/Model/Logradouro.php';
require_once './src/Controller/EmailController.php';
require_once './src/Model/Category.php';
require_once './src/Model/Account.php';
require_once './src/Model/Assets.php';
require_once './src/Model/FormaPagamento.php';
require_once './src/Model/Usuario.php';
class OrcamentoController extends CommonController
{

  private $modulos = array();
  private $estados = array();
  private $classe = 'Orcamento';
  private $breadcrumb = array();
  private $titulo_principal = '';
  private $modelo_empresa = 'molduraria';
  private $empresas = array();
  private $usuarios = array();
  private $habilita_tributacao = false;
  private $habilita_financeiro = false;
  private $ocultar_componentes_impressao_pedido = false;
  private $ocultar_componentes_impressao_documento_fiscal = false;
  private $habilitar_emissao_nf = false;
  private $habilitar_emissao_nfs = false;
  private $habilitar_emissao_nfc = false;
  private $exibir_valores_individuais = false;
  private $desabilitar_controle_estoque = false;
  private $cor_produto_descontinuado = '#0772c2';
  private $categoriasContasReceber = array();
  private $contas = [];
  private $situacoes = [];
  private $formas_pagamento = [];
  private $ambiente = '';

  private $arrLogradouros = array();

  public function __construct()
  {
    $orcamento = new Orcamento();
    $common = new CommonController();
    $modulos = $this->getModulos();
    $estados = $this->getEstados();
    $configuracoes = new Configuracoes();
    $logradouro = new Logradouro();
    $objEmpresas = new Empresas();
    $objUsuario = new Usuario();
    $objContas = new Account();
    $objCategoriasFinanceiro = new Category();
    $objFormaPagamento = new FormaPagamento();
    $objSituacao = new Situacao();
    $this->arrLogradouros = $logradouro->listarTodos();

    $this->modulos = $modulos;
    $this->estados = $estados;

    $this->empresas = $objEmpresas->listarTodos(0, 0, 'status', 1);

    $this->usuarios = $objUsuario->listarTodos(0,0, 'usuario_master', '0');

    $this->situacoes = $objSituacao->listarTodos();

    $modulo_posicao = 0; // $modulo_posicao = array_search($this->classe,array_column($modulos,'modulo'));

    $this->categoriasContasReceber = $objCategoriasFinanceiro->listarPorTipo(['tipo' => 0]);
    $this->contas = $objContas->listarTodos();

    $this->habilita_tributacao = filter_var($configuracoes->listarConfiguracao('habilita_tributacao')->valor, FILTER_VALIDATE_BOOLEAN);
    $this->habilita_financeiro = filter_var($configuracoes->listarConfiguracao('habilita_financeiro')->valor, FILTER_VALIDATE_BOOLEAN);

    $this->ocultar_componentes_impressao_pedido = filter_var($configuracoes->listarConfiguracao('ocultar_componentes_impressao_pedido')->valor, FILTER_VALIDATE_BOOLEAN);
    $this->ocultar_componentes_impressao_documento_fiscal = filter_var($configuracoes->listarConfiguracao('ocultar_componentes_impressao_documento_fiscal')->valor, FILTER_VALIDATE_BOOLEAN);

    $this->habilitar_emissao_nf  = filter_var($configuracoes->listarConfiguracao('habilitar_emissao_nf')->valor, FILTER_VALIDATE_BOOLEAN);
    $this->habilitar_emissao_nfs = filter_var($configuracoes->listarConfiguracao('habilitar_emissao_nfs')->valor, FILTER_VALIDATE_BOOLEAN);
    $this->habilitar_emissao_nfc = filter_var($configuracoes->listarConfiguracao('habilitar_emissao_nfc')->valor, FILTER_VALIDATE_BOOLEAN);

    $this->exibir_valores_individuais = filter_var($configuracoes->listarConfiguracao('exibir_valores_individuais')->valor, FILTER_VALIDATE_BOOLEAN);

    $this->desabilitar_controle_estoque = filter_var($configuracoes->listarConfiguracao('desabilitar_controle_estoque')->valor, FILTER_VALIDATE_BOOLEAN);
    $this->cor_produto_descontinuado = $configuracoes->listarConfiguracao('cor_produto_descontinuado')->valor;

    $this->modelo_empresa = $configuracoes->listarConfiguracao('modelo_empresa')->valor;

    $this->formas_pagamento = $objFormaPagamento->listarTodos(0, 0, 'situacao', 1);
    $this->ambiente = $this->consultaAmbiente();

    $this->titulo_principal = array('descricao' => 'Pedidos', 'icone' => '');
    $this->breadcrumb = array('Maestria' => URL . 'dashboard/index/', $this->titulo_principal['descricao'] => URL . $this->classe . '/listar/');
  }

  public function listar()
  {
    $orcamento = new Orcamento();
    $common = new CommonController();

    $parametros = $common->validateGet('parametros');
    $filtro_padrao_situacao = null;
    if ($parametros) {
      preg_match_all('/^(.*)=(\d)$/m', $parametros, $matches, PREG_SET_ORDER, 0);
      if (isset($matches[0][1]) && $matches[0][1] == 'Id_Situacao') {
        $filtro_padrao_situacao = $matches[0][2];
      }
    }

    $modulos = $this->modulos;
    $classe = $this->classe;

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;

    require './src/View/Orcamento/orcamento_listar.php';
  }

  public function listaOrcamentosDataTables()
  {
    $objOrcamento = new Orcamento();
    $draw = $this->validateGet('draw') ?: 1;
    $length = $this->validateGet('length') ?: 10;
    $start = $this->validateGet('start') ?: 1;
    $searchValue = isset($_GET['search']['value']) ? $_GET['search']['value'] : '';

    $arrColunaConsultar = [];
    if (empty($searchValue)) {
      foreach ($_GET['columns'] as $colunas) {
        if (!empty($colunas['search']['value'])) {
          if ($colunas['data'] == 'Id_Situacao') {
            $searchValue = $colunas['search']['value'];
            $arrColunaConsultar = ['Orcamento.Id_Situacao'];
          }
        }
      }
    }
    $order = 'Cd_Orcamento DESC';

    if (isset($_GET['order']) && is_array($_GET['order'])) {
      $direcao = strtoupper($_GET['order'][0]['dir']);
      switch ($_GET['columns'][$_GET['order'][0]['column']]['data']) {
        case 'Id_Situacao':
          $order = 'Situacao.descricao ' . $direcao;
          break;

        default:
          $order = $_GET['columns'][$_GET['order'][0]['column']]['data'] . ' ' . $direcao;
      }
    }

    $arrFiltro = [];
    if (!empty($searchValue)) {
      $arrFiltro['Clientes.RazaoSocial'] = $searchValue;
      $arrFiltro['Orcamento.Cd_Orcamento'] = $searchValue;
      $arrFiltro['Orcamento.Id_Situacao'] = $searchValue;
      if (!empty($arrColunaConsultar)) {
        $arrFiltro = [
          $arrColunaConsultar[0] => $searchValue
        ];
      }
    }

    if (isset($_GET['filtro_empresa']) && ($_GET['filtro_empresa'] != '')) {
      $arrFiltro['id_empresa'] = $_GET['filtro_empresa'];
    }

    if (isset($_GET['filtro_vendedor']) && ($_GET['filtro_vendedor'] != '')) {
      $arrFiltro['criado_por'] = $_GET['filtro_vendedor'];
    }   
    
    if (isset($_GET['filtro_numero_pedido']) && ($_GET['filtro_numero_pedido'] != '')) {
      $arrFiltro['Cd_Orcamento'] = $_GET['filtro_numero_pedido'];
    }

    if (isset($_GET['filtro_situacao']) && (count($_GET['filtro_situacao'])) ) {
      $arrFiltro['Id_Situacao'] = implode(',', $_GET['filtro_situacao']);
    }

    if (isset($_GET['filtro_cliente']) && ($_GET['filtro_cliente'] != '') ) {
      $arrFiltro['Cd_Cliente'] = $_GET['filtro_cliente'];
    }     

    $orcamentos = $objOrcamento->listarTodos(1, ($start - 1), $length, $order, "", "", $arrFiltro);
    $num_registros = $objOrcamento->listarTodosTotal($arrFiltro);
    $arrClientes = [];
    foreach ($orcamentos as $orcamento) {

      $dataOrcamento = '';
      if ($orcamento->Dt_Orcamento != '') {
        $DataOrcamento = new DateTime($orcamento->Dt_Orcamento);
        $dataOrcamento = $DataOrcamento->format('d/m/Y');
      }

      $dataPrevistaEntrega = '';
      if ($orcamento->Dt_Prevista_Entrega != '') {
        $DataPrevistaEntrega = new DateTime($orcamento->Dt_Prevista_Entrega);
        $dataPrevistaEntrega = $DataPrevistaEntrega->format('d/m/Y');
      }

      $url_editar = URL . $this->classe . '/salvar/' . $orcamento->Cd_Orcamento;
      $arrDados = array(
        'Cd_Orcamento' => '<a href="' . $url_editar . '">' . $orcamento->Cd_Orcamento . '</a>',
        'Dt_Orcamento' => $dataOrcamento,
        'Dt_Prevista_Entrega' => $dataPrevistaEntrega,
        'Cd_Cliente' => $orcamento->RazaoSocial ?: '<i>Não definido</i>',
        'nome_empresa' => $orcamento->nome_empresa,
        // 'Consumidor_Temp' => $orcamento->RazaoSocialConsumidor ?: '<i>Não definido</i>',
        'Id_Situacao' => $orcamento->Id_Situacao > 0 ? $orcamento->descricao : '<i>Não definido</i>',
        'Editar' => '<a class="btn btn-app" href="' . $url_editar . '"><i class="fa fa-edit"></i>Editar</a>'
      );
      if ($objOrcamento->permissao_visualizar_pedidos_outros_vendedores) {
        $arrClientes[] = array_merge($arrDados, ['nome_vendedor' => $orcamento->nome_vendedor]);
      } else {
        $arrClientes[]  = $arrDados;
      }
    }
    $arrRetorno = array(
      'draw' => $draw,
      'recordsTotal' => $num_registros,
      'recordsFiltered' => $num_registros,
      'data' => $arrClientes
    );

    echo json_encode($arrRetorno);
    return;
  }

  public function salvar($handle = 0)
  {
    $msg_sucesso = '';
    $metodo = 'salvar'; //$handle == 0 ? 'cadastrar' : 'editar';
    $modulos = $this->modulos;
    $classe = $this->classe;
    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;

    $orcamento = new Orcamento();
    $produto = new Produto();
    $componente = new Componente();
    $ProdutoAuxiliar = new ProdutoAuxiliar();
    $ItemOrcamento = new ItemOrcamento();
    $MolduraItemOrcamento = new Moldura_Item_Orcamento();
    $ComponenteItemOrcamento = new Componente_Item_Orcamento();
    $cliente = new Cliente();
    $vendedor = new Vendedor();
    $situacao = new Situacao();
    $configuracoes = new Configuracoes();
    $objAssets = new Assets();

    $orcamentos = $orcamento->listarOrcamento($handle);

    if (isset($_POST) && !empty($_POST)) {
      /**
       * Quando o pedido é colocado para ser produzido, muda a
       * data do cadastro do pedido, para ser considerado a data de aprovação.
       */
      if (isset($orcamentos[0]->Id_Situacao) && ($orcamentos[0]->Id_Situacao <= 1 && ($_POST['Id_Situacao'] == 2 || $_POST['Id_Situacao'] == 3) ) ) {
        $_POST['Dt_Orcamento'] = date('d/m/Y');
      }
      $arrFinanceiro = [];
      if (isset($_POST['financeiro'])) {
        $arrFinanceiro = json_decode($_POST['financeiro'], true);
        unset($_POST['financeiro']);
      }

      $handle = $orcamento->cadastrarOrcamento($_POST);
      
      if (count($arrFinanceiro)) {
        foreach ($arrFinanceiro as $tipo_pagamento => $value) {
          if (empty($value)) {
            continue;
          }

          if ((int) $value['Amount'] == 0) {
            continue;
          }

          $arrComplemento = [];
          if ($tipo_pagamento === 'entrada') {
            $arrComplemento = ['data_pagamento_loja' => $this->dateFormat($_POST['Dt_Orcamento'])];
          }
          if ($tipo_pagamento === 'saldo') {
            $arrComplemento = ['data_pagamento_loja' => $this->dateFormat($_POST['data_pagamento'])];
          }
          $arrDadosFinanceiro = array_merge($value, ['ndoc' => $handle, 'Title' => $_POST['Cd_Cliente']], $arrComplemento);
          $objAssets->cadastrar($arrDadosFinanceiro);  
        }
      }

      if ($handle) {
        $link_impressao = URL . $classe . "/imprimir/" . $handle;
        $msg_sucesso = $this->classe . ' salvo com sucesso.';
        $msg_sucesso .= '<br><a href="#acoes" style="text-decoration:none;">Realizar ações</a>';

        $_SESSION['success'] = 'true';
        $_SESSION['msg'] = $msg_sucesso;

        header("Location: " . URL . $classe . "/salvar/" . $handle, true, 303);
        exit;
      }
    }

    $orcamentos = $handle > 0 ? $orcamento->listarOrcamento($handle) : array($orcamento);

    if (empty($orcamentos) && $handle != 0) {
      Header('Location:' . URL . 'dashboard/index/');
      return;
    }

    $indiceFormaPagamentoSaldo = array_search($orcamentos[0]->id_forma_pagamento_saldo, array_column($this->formas_pagamento, "id_forma_pagamento"));
    $quantidadeParcelasMaximoSaldo = $this->formas_pagamento[$indiceFormaPagamentoSaldo]->parcelas;

    $indiceFormaPagamentoEntrada = array_search($orcamentos[0]->id_forma_pagamento_entrada, array_column($this->formas_pagamento, "id_forma_pagamento"));
    $quantidadeParcelasMaximoEntrada = $this->formas_pagamento[$indiceFormaPagamentoEntrada]->parcelas;

    $itemsOrcamento = $ItemOrcamento->listarItemOrcamento($handle);

    $arrMoldurasItemOrcamento = array();
    $arrComponentesItemOrcamento = array();
    $temProdutoComMedida = false;
    foreach ($itemsOrcamento as $item_orcamento) {
      $moldurasItemOrcamento = $MolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
      $componentesItemOrcamento = $ComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);

      $quantidadeProdutosComponentes = count(array_merge($moldurasItemOrcamento, $componentesItemOrcamento));
      if ($quantidadeProdutosComponentes == 0) {
        $quantidadeProdutosComponentes = 1;
      }

      $valor_adicional_por_produto = $item_orcamento->VL_ADICIONAIS / $quantidadeProdutosComponentes;

      foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
        if ($temProdutoComMedida == false && ($moldura_item_orcamento->DescricaoUnidadeMedida == 'M2' || $moldura_item_orcamento->DescricaoUnidadeMedida == 'ML') ) {
          $temProdutoComMedida = true;
        }

        $valor_extra_fator_calculo = ($moldura_item_orcamento->Vl_Unitario * ((float) $item_orcamento->Vl_Moldura / 100));
        $valor_produto_com_adicional = $moldura_item_orcamento->Vl_Unitario + $valor_adicional_por_produto + $valor_extra_fator_calculo;
        $arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento][] = array(
          'Cd_Produto' => $moldura_item_orcamento->CodigoProduto,
          'DescricaoProduto' => $moldura_item_orcamento->DescricaoProduto,
          'NovoCodigo' => $moldura_item_orcamento->NovoCodigo,
          'codigoComplexidade' => $moldura_item_orcamento->codigoComplexidade,
          'nomeComplexidade' => $moldura_item_orcamento->nomeComplexidade,
          'sarrafo_reforco' => $moldura_item_orcamento->sarrafo_reforco,
          'valor_produto' => $moldura_item_orcamento->Vl_Unitario,
          'valor_produto_com_adicional' => $valor_produto_com_adicional
        );
      }
      
      foreach ($componentesItemOrcamento as $componente_item_orcamento) {
        if ($temProdutoComMedida == false && ($componente_item_orcamento->DescricaoUnidadeMedida == 'M2' || $componente_item_orcamento->DescricaoUnidadeMedida == 'M2') ) {
          $temProdutoComMedida = true;
        }

        $valor_produto_com_adicional = $componente_item_orcamento->Vl_Unitario + $valor_adicional_por_produto;

        $arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento][] = array(
          'Id_Componente' => $componente_item_orcamento->CodigoProduto,
          'Descricao' => $componente_item_orcamento->DescricaoProduto,
          'quantidade' => $componente_item_orcamento->Qt_Item,
          'valor_produto' => $componente_item_orcamento->Vl_Unitario,
          'valor_produto_com_adicional' => $valor_produto_com_adicional
        );
      }
    }

    $coluna = '';
    $buscar = '';
    $pagina_atual = 0;
    $linha_inicial = 0;
    $produtos = $produto->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar, 'NovoCodigo ASC', array('situacao' => array(1, 2), 'componente' => 0, 'quantidade' => '0'), 2000);

    $produtosAuxiliar = $ProdutoAuxiliar->listarTodos($pagina_atual, $linha_inicial, 'status', '1');

    // $componentes = $componente->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar);
    $componentes = $produto->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar, 'NovoCodigo ASC', array('situacao' => array(1), 'componente' => 1), 2000);
    $vendedores = $vendedor->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar);

    $situacoes = $situacao->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar);

    $sucesso = isset($_SESSION['success']) ? $_SESSION['success'] : '';
    $msg_sucesso = isset($_SESSION['msg']) ? $_SESSION['msg'] : '';

    unset($_SESSION['success'], $_SESSION['msg']);

    $modelo_empresa = $configuracoes->listarConfiguracao('modelo_empresa');
    $exibir_total_custo_item = filter_var($configuracoes->listarConfiguracao('exibir_total_custo_item')->valor, FILTER_VALIDATE_BOOLEAN);
    $exibir_medida_final_do_quadro = filter_var($configuracoes->listarConfiguracao('exibir_medida_final_do_quadro')->valor, FILTER_VALIDATE_BOOLEAN);
    $permitir_informar_medidas_em_decimal = filter_var($configuracoes->listarConfiguracao('permitir_informar_medidas_em_decimal')->valor, FILTER_VALIDATE_BOOLEAN);


    if ($modelo_empresa->valor !== 'molduraria') {
      include "./src/View/Orcamento/orcamento_{$modelo_empresa->valor}_form.php";
    } else {
      include './src/View/Orcamento/orcamento_form.php';
    }
  }

  public function calculaTotal()
  {
    $common = new CommonController();
    $componente = new Componente();
    $ComposicaoPreco = new ComposicaoPreco();
    $produto = new Produto();
    $percentualCustoClienteGrupo = new PercentualCustoClienteGrupo();
    $objClienteGrupo = new ClienteGrupo();
    $complexidadeProduto = new ComplexidadeProduto();
    $cliente = new Cliente();
    $configuracoes = new Configuracoes();

    $composicoesPreco = $ComposicaoPreco->listarComposicaoPreco(1);
    $valor_perda = $composicoesPreco[0]->VL_PERDA_MEDIA;
    $valor_margem_bruta = $composicoesPreco[0]->VL_MARGEM_BRUTA;
    $numero_cortes_considerar_perda = $configuracoes->listarConfiguracao('numero_cortes_considerar_perda')->valor;
    $considera_soma_total_perdas = (bool) filter_var($configuracoes->listarConfiguracao('considera_soma_total_perdas')->valor, FILTER_VALIDATE_BOOLEAN);
    $arredondar_medida_total_quadro = (bool) filter_var($configuracoes->listarConfiguracao('arredondar_medida_total_quadro')->valor, FILTER_VALIDATE_BOOLEAN);
    $arredondar_valor_item = (bool) filter_var($configuracoes->listarConfiguracao('arredondar_valor_item')->valor, FILTER_VALIDATE_BOOLEAN);
    $quantidade_lados_considerar = (int) $configuracoes->listarConfiguracao('lados_considerar_no_calculo_moldura')->valor;

    if (!$_POST) {
      return json_encode(array());
    }

    $Vl_Unitario = 0;
    foreach ($_POST as $key => $value) {
      if ($key == 'Md_Largura' || $key == 'Md_Altura') {
        ${$key} = $this->monetaryValue($value);
        continue;
      }
      if ($key == 'molduras') {
        ${$key} = json_decode($value, true);
        continue;
      }
      if ($key == 'componentes') {
        ${$key} = json_decode($value, true);
        continue;
      }
      ${$key} = $value;
    }

    $total_bruto = 0;

    $Vl_Adicionais = str_replace(".", "", $Vl_Adicionais);
    $Vl_Adicionais = (float) str_replace(",", ".", $Vl_Adicionais);

    $Vl_Moldura = str_replace(".", "", $Vl_Moldura);
    $Vl_Moldura = str_replace(",", ".", $Vl_Moldura);
    $arrComponentes = !empty($componentes) ? $componentes : [];
    $arrMolduras = !empty($molduras) ? $molduras : [];
    $Md_Altura_Moldura = 0;
    $Md_Largura_Moldura = 0;
    $Md_Total = 0;
    $custo_moldura = 0;
    if (isset($_POST['Vl_Avulso']) && ($_POST['Vl_Avulso'] != "") ) {
      $Vl_Avulso = str_replace(".", "", $Vl_Avulso);
      $Vl_Avulso = str_replace(",", ".", $Vl_Avulso);
    } else {
      $Vl_Avulso = 0;
    }

    if ($this->modelo_empresa == 'venda-avulsa') {
      $Md_Altura = 1;
      $Md_Largura = 1;
    }

    $Qt_Item = floatval($Qt_Item);

    $metro_linear = ($Md_Altura / 100) * ($Md_Largura / 100);

    $metro_linear = $metro_linear + (($metro_linear * $valor_perda) / 100);

    if (!empty($handleCliente)) {
      $consultaCliente = $cliente->listarCliente($handleCliente);
    }

    $valor_unitario_molduras = 0;
    $total_varas_consumidas = 0;
    $consumo_total_cm = 0;
    $soma_total_perdas = 0;
    $valor_quebra_moldura_anterior = 0;
    $arrPrecoMoldura = [];
    $valor_total_custo = 0;
    $medida_altura_final = 0;
    $medida_largura_final = 0;
    $temProdutoComMedida = false;

    $auxMoldura = 0;
    $auxComponente = 0;


    /**
     * Mapeando todas as molduras que serão utilizadas;
     */
    $arrCodigoMolduras = array_map(function ($arr) {
      return $arr['valor'];
    }, $arrMolduras);

    /**
     * Consulta as molduras, para ter acesso ao valor de perda.
     */
    $consultaMolduras = count($arrCodigoMolduras) > 0 ? $produto->listarTodos(0, 0, '', '', '', ['CodigoProduto' => $arrCodigoMolduras]) : [];

    if (is_array($arrComponentes) && count($arrComponentes) > 0) {
      $arrMolduras = array_merge($arrComponentes, $arrMolduras);
    }

    /**
     * Obtém o total de perdas considerando todas as molduras.
     */
    $perda_total = 0;
    foreach ($consultaMolduras as $perda_moldura) {
      $perda_individual = ($perda_moldura->Desenho * ($numero_cortes_considerar_perda * 2));
      $perda_total += $perda_individual;
    }

    if (count($arrMolduras) > 0 && $this->modelo_empresa != 'venda-avulsa') {
      foreach ($arrMolduras as $moldura_value) {

        $molduras = $produto->listarProduto($moldura_value['valor']);
        $molduras = $molduras[0];

        if ($temProdutoComMedida == false && ($molduras->DescricaoUnidadeMedida == 'M2' || $molduras->DescricaoUnidadeMedida == 'ML') ) {
          $temProdutoComMedida = true;
        }

        $arrPercentualGrupo = $percentualCustoClienteGrupo->listarPercentualPorProduto($moldura_value['valor']);

        $arrComplexidade = $complexidadeProduto->listarComplexidadePorProduto($moldura_value['valor']);

        $custo_moldura = $molduras->definicao_preco == 1 ? $molduras->PrecoVendaMaoObra : ($molduras->PrecoCusto + ($molduras->PrecoCusto * ($molduras->margem_venda / 100)));
        $preco_custo_material = $molduras->PrecoCusto;
        $valor_grupo_cliente = 0;
        $aplicacao = 'A';
        $definicao_preco = 2;
        $percentual_complexidade = isset($arrComplexidade[$moldura_value['complexidade']]) ? $arrComplexidade[$moldura_value['complexidade']]['percentual'] : 0;
        $quantidade_item_unitario = isset($moldura_value['quantidade']) && $moldura_value['quantidade'] > 0 ? $moldura_value['quantidade'] : 1;
        $sarrafo_reforco = isset($moldura_value['sarrafoReforco']) ? $moldura_value['sarrafoReforco'] : 'N';

        if (!empty($handleCliente) && isset($consultaCliente[0]) && isset($arrPercentualGrupo[$consultaCliente[0]->codigoClienteGrupo])) {
          $valor_grupo_cliente = $arrPercentualGrupo[$consultaCliente[0]->codigoClienteGrupo]['percentual'];
        }

        if ($valor_grupo_cliente == 0 && isset($consultaCliente[0]) && !empty($handleCliente)) {
          $arrClienteGrupo = $objClienteGrupo->listarClienteGrupo($consultaCliente[0]->codigoClienteGrupo);
          if (!empty($arrClienteGrupo)) {
            $aplicacao = $arrClienteGrupo[0]->aplicacao;
            $definicao_preco = $arrClienteGrupo[0]->definicao_preco;
            $valor_grupo_cliente = $arrClienteGrupo[0]->valor;
          }
        }

        $preco_custo = $valor_grupo_cliente > 0 && $definicao_preco == '2' ? $molduras->PrecoCusto : $custo_moldura;
        $custo_moldura = $aplicacao == 'A' ? $preco_custo + ($preco_custo * ($valor_grupo_cliente / 100)) : $preco_custo - ($preco_custo * ($valor_grupo_cliente / 100)) ;

        if ($percentual_complexidade > 0) {
          $aplicacao_complexidade = $arrComplexidade[$moldura_value['complexidade']]['aplicacao'];
          $definicao_preco_complexidde = $arrComplexidade[$moldura_value['complexidade']]['definicao_preco'];

          if ($definicao_preco_complexidde == 1) {
            $custo_moldura = $aplicacao_complexidade == 'A' ? $custo_moldura + $percentual_complexidade : $custo_moldura - $percentual_complexidade;
          } else {
            $custo_moldura = $aplicacao_complexidade == 'A' ? $custo_moldura + ($custo_moldura * ($percentual_complexidade / 100)) : $custo_moldura - ($custo_moldura * ($percentual_complexidade / 100));
          }

        }

        $medida_vara = (float) $molduras->MedidaVara;

        /**
         * Tipo de Material:
         * Moldura - Multiplicar por 2
         * Espelho - Multiplicar por 1
         * Escora  - Multiplicar por 1
         */
        $Md_Altura_Moldura =  $sarrafo_reforco == 'H' ? 0 : $Md_Altura ; // ($Md_Altura + $valor_quebra_moldura_anterior) * 1;
        $Md_Largura_Moldura = $sarrafo_reforco == 'V' ? 0 : $Md_Largura; // ($Md_Largura + $valor_quebra_moldura_anterior) * 1;
        // echo "<p> # $Md_Altura_Moldura - $Md_Largura_Moldura </p>";
        if ($molduras->MetragemLinear == 1 & $sarrafo_reforco == 'N') {
          $Md_Altura_Moldura = ($Md_Altura + $valor_quebra_moldura_anterior) * $quantidade_lados_considerar;
          $Md_Largura_Moldura = ($Md_Largura + $valor_quebra_moldura_anterior) * $quantidade_lados_considerar;
        }
        // echo "<p> ## $Md_Altura_Moldura - $Md_Largura_Moldura </p>";

        if ($molduras->DescricaoUnidadeMedida === 'UN') {
          $Md_Altura_Moldura = 1;
          $Md_Largura_Moldura = 1;
        }

        $md_quebra =  !$considera_soma_total_perdas ? $molduras->Desenho : 0;        
        $valor_quebra_moldura_anterior = !$considera_soma_total_perdas ? $valor_quebra_moldura_anterior + ($md_quebra * 2) : 0;
        $soma_total_perdas = $soma_total_perdas + ($md_quebra * $numero_cortes_considerar_perda);

        // echo "<p>$molduras->DescricaoProduto - $valor_quebra_moldura_anterior - soma_total_perdas: $soma_total_perdas</p>";
        if ($quantidade_lados_considerar == 2) {
          $Md_Altura_Moldura = $molduras->MetragemLinear == 1 && $md_quebra > 0 && $sarrafo_reforco == 'N' ? $Md_Altura_Moldura + ($md_quebra * $numero_cortes_considerar_perda) : $Md_Altura_Moldura;
          $Md_Largura_Moldura = $molduras->MetragemLinear ==  1 && $md_quebra > 0 && $sarrafo_reforco == 'N' ? $Md_Largura_Moldura + ($md_quebra * $numero_cortes_considerar_perda) : $Md_Largura_Moldura;
          // echo "<p> ### $Md_Altura_Moldura - $Md_Largura_Moldura </p>";
          $Md_Total = $Md_Altura_Moldura + $Md_Largura_Moldura;
        } else {
          $Md_Total = ($Md_Altura_Moldura + $Md_Largura_Moldura) * $md_quebra;
        }

        // echo "Md_Altura_Moldura: $Md_Altura_Moldura - medida_altura_final: $medida_altura_final";
        // exit;
        // echo "<p> * $Md_Total</p>";

        /**
         * Moldura: Medida linear (Altura + Largura)
         * Espelho: Medida quadrado (Altura * Largura)
         * Escora: Sem medidas, cobrado por unidade
         */
        if ($molduras->MetragemLinear == 0 && $molduras->DescricaoUnidadeMedida !== 'UN') {
          // echo '<p>PASSEI</p>';
          $Md_Total = $Md_Altura_Moldura * $Md_Largura_Moldura;
          // $valor_perda = 0;
        } else if ($molduras->DescricaoUnidadeMedida !== 'UN') {
          if ($quantidade_lados_considerar !== 1) {
            $Md_Total =  !$considera_soma_total_perdas ? $Md_Altura_Moldura + $Md_Largura_Moldura : ($Md_Altura_Moldura + $Md_Largura_Moldura) + $perda_total;
          }
        } else {
          $Md_Total = 1 * $quantidade_item_unitario;
        }

        // echo "<p> ** $Md_Total</p>";

        // echo "<p>$molduras->MetragemLinear</p>";

        if ($molduras->MetragemLinear == 1) {
          // echo "<p> $Md_Total + (($Md_Total * $valor_perda) / 100) </p>";
          $Md_Total = $Md_Total + (($Md_Total * $valor_perda) / 100);
        }

        // echo "<p> *** $Md_Total</p>";

        /**
         * Se for espelho, dividi-se por 100 
         * porque o valor é m²
         */
        if ($molduras->MetragemLinear == 0 && $molduras->DescricaoUnidadeMedida !== 'UN') {
          $Md_Total = $arredondar_medida_total_quadro == 'true' ? ceil($Md_Total / 100) : $Md_Total / 100;
        } else if ($molduras->DescricaoUnidadeMedida !== 'UN') {
          $Md_Total = round($Md_Total, 2);
          if ($arredondar_medida_total_quadro == 'true') {
            $Md_Total = round($Md_Total, -1);
          }
        }

        // echo "<p>$Md_Total</p>";

        if ($molduras->DescricaoUnidadeMedida !== 'UN') {
          $valor_unitario_molduras += round(($Md_Total * $custo_moldura) / 100, 2);
          $valor_total_custo += round(($Md_Total * $preco_custo_material) / 100, 2);
          $arrPrecoMoldura[]  = ['item' => $moldura_value['valor'], 'valor_individual' => $custo_moldura, 'valor_total' => round(($Md_Total * $custo_moldura) / 100, 2), 'consumo' => $Md_Total, 'descricao' => $molduras->DescricaoProduto, 'componente' => $molduras->componente, 'item_posicao' => $molduras->componente == '1' ? $auxComponente : $auxMoldura ];
        } else {
          $valor_unitario_molduras += ($custo_moldura * $quantidade_item_unitario);
          $valor_total_custo += ($preco_custo_material * $quantidade_item_unitario);          
          $arrPrecoMoldura[]  = ['item' => $moldura_value['valor'], 'valor_individual' => $custo_moldura, 'valor_total' => ($custo_moldura * $quantidade_item_unitario), 'consumo' => $Md_Total, 'descricao' => $molduras->DescricaoProduto, 'componente' => $molduras->componente, 'item_posicao' => $molduras->componente == '1' ? $auxComponente : $auxMoldura];
        }

        if ($molduras->componente == '1') {
          $auxComponente++;
        } else {
          $auxMoldura++;
        }

        $consumo_total_cm += round($Md_Total * $Qt_Item);
        $total_varas_consumidas += $medida_vara > 0 ? ceil(($Md_Total * $Qt_Item) / $medida_vara) : 0;
      }
    }

    // $total_unitario = $valor_unitario_molduras;

    $valor_unitario = $this->modelo_empresa == 'venda-avulsa' ? ($Vl_Unitario) : 0;
    $valor_individual = $this->modelo_empresa == 'venda-avulsa' ? ($Vl_Avulso + $valor_unitario) : 0;


    $valor_extra_fator_calculo = ($valor_unitario_molduras * ((float) $Vl_Moldura / 100));
    $total_unitario = $valor_unitario_molduras + $valor_extra_fator_calculo + (float) $Vl_Adicionais + $valor_individual;

    // echo count($arrPrecoMoldura) . "<br>";

    // echo $Vl_Adicionais . "<br>";

    $quantidadeProdutosComponentes = count($arrPrecoMoldura) > 0 ? count($arrPrecoMoldura): 1;
    // echo "($Vl_Adicionais + $valor_extra_fator_calculo) / $quantidadeProdutosComponentes";
    // exit;
    $divisao_preco_adicional = ( (float) $Vl_Adicionais + $valor_extra_fator_calculo) / $quantidadeProdutosComponentes;

    foreach ($arrPrecoMoldura as $index => $preco_moldura) {
      $arrPrecoMoldura[$index] = array_merge(
        $preco_moldura,
        [
          'valor_total_com_adicional' => $preco_moldura['valor_total'] + $divisao_preco_adicional
        ]
      );
    }
    
    if ($arredondar_valor_item) {
      $total_unitario = ceil($total_unitario);
    }

    $total_bruto = $total_unitario * $Qt_Item;

    // echo "<br> Varas a serem consumidas: " . ($total_varas_consumidas * $Qt_Item);
    $arrRetorno = array(
      'total_unitario' => $total_unitario,
      'total_bruto' => $total_bruto,
      'valor_total_custo' => number_format($valor_total_custo, 2, ".", ""),
      'varas_consumidas' => $total_varas_consumidas,
      'itens' => $arrPrecoMoldura,
      'calculo' => [
        'Md_Altura_Moldura' => $Md_Altura_Moldura,
        'Md_Largura_Moldura' => $Md_Largura_Moldura,
        'Md_Total' => $Md_Total,
        // 'custo_moldura' => $custo_moldura,
        // 'valor_unitario_molduras' => $valor_unitario_molduras,
        'total_varas_consumidas' => $total_varas_consumidas,
        'consumo_total_cm' => $consumo_total_cm,
        'soma_total_perdas' => $soma_total_perdas,
        'medida_final' => ($Md_Largura_Moldura/2) . 'X' . ($Md_Altura_Moldura/2),
        'tem_produto_com_medida' => $temProdutoComMedida
      ]
    );

    echo json_encode($arrRetorno);
  }

  public function teste($id_pedido = 0) {
    $orcamento = new Orcamento();
    $MolduraItemOrcamento = new Moldura_Item_Orcamento();
    $ComponenteItemOrcamento = new Componente_Item_Orcamento();

    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, 'Orcamento');

    $where = '';
    if ($id_pedido > 0) {
      $where = 'WHERE o.Cd_Orcamento IN ('.$id_pedido.') ';
    }

    $sql = '
    select
    o.Cd_Orcamento,
    o.Cd_Cliente,
    io.Cd_Item_Orcamento,
    io.Qt_Item,
    io.Md_Altura,
    io.Md_Largura
  FROM
    Orcamento o
  INNER JOIN Item_Orcamento io ON
    (o.Cd_Orcamento = io.Cd_Orcamento) ' . $where . '
  ORDER BY io.Cd_Item_Orcamento  ASC
  
    ';

    $pedidos = $crud->getSQLGeneric($sql);
    // print_r($pedidos);exit;

    $arrPedido = [];
    foreach ($pedidos as $pedido) {

      $id_pedido = $pedido->Cd_Orcamento;

      $arrComponentesItemOrcamento = [];
      $arrMoldurasItemOrcamento = [];

      $moldurasItemOrcamento = $MolduraItemOrcamento->listarMolduraItemOrcamento($pedido->Cd_Item_Orcamento, $id_pedido);
      foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
        $arrMoldurasItemOrcamento[] = array(          
          'valor' => $moldura_item_orcamento->CodigoProduto,
          'complexidade' => (int) $moldura_item_orcamento->codigoComplexidade,
          'sarrafoReforco' => $moldura_item_orcamento->sarrafo_reforco,
          'quantidade' => 0
        );
      }


      $componentesItemOrcamento = $ComponenteItemOrcamento->listarComponenteItemOrcamento($pedido->Cd_Item_Orcamento, $id_pedido);
      foreach ($componentesItemOrcamento as $componente_item_orcamento) {
        $arrComponentesItemOrcamento[] = array(
          
          "valor" => $componente_item_orcamento->CodigoProduto,
          'complexidade' => 0,
          'quantidade' => $componente_item_orcamento->Qt_Item
        );
      }
      

      $_POST['Cd_Prod_Aux'] = 32;
      $_POST['Qt_Item'] = $pedido->Qt_Item;
      $_POST['Md_Largura'] =  $pedido->Md_Largura;
      $_POST['Md_Altura'] = $pedido->Md_Altura;
      $_POST['Vl_Adicionais'] = 0;
      $_POST['Vl_Moldura'] =  0;
      $_POST['molduras'] = json_encode($arrMoldurasItemOrcamento);
      $_POST['componentes'] = json_encode($arrComponentesItemOrcamento);
      $_POST['handleCliente'] = $pedido->Cd_Cliente;


      $calculaTotal = json_decode($this->calculaTotal(), true);

      // print_r($arrMoldurasItemOrcamento);

      // print_r($calculaTotal);

      foreach ($calculaTotal['itens'] as $calculo) {
        $indiceMoldura = array_search($calculo['item'],array_column($arrMoldurasItemOrcamento,'valor'));

        $indiceComponente = array_search($calculo['item'],array_column($arrComponentesItemOrcamento,'valor'));

        // var_dump($indiceComponente);

        if ($indiceMoldura !== false) {
          $sqlAtualizaValor = sprintf(
            '
              UPDATE
                Moldura_Item_Orcamento
              SET
                Vl_Unitario = %s
              WHERE
                Cd_Orcamento = %s AND
                Cd_Item_Orcamento = %s AND
                Cd_Produto = %s
            ',
            $calculo['valor_total'],
            $id_pedido,
            $pedido->Cd_Item_Orcamento,
            $calculo['item']
          );

          $crud->getSQLGeneric(
            sprintf($sqlAtualizaValor)
          );
        }

        if ($indiceComponente !== false) {
          $sqlAtualizaValor = sprintf(
            '
              UPDATE
                COMPONENTES_ITEM_ORCAMENTO
              SET
                VALOR_UNITARIO = %s
              WHERE
                CD_ORCAMENTO = %s AND
                ITEM_ORCAMENTO = %s AND
                CD_COMPONENTE = %s
            ',
            $calculo['valor_total'],
            $id_pedido,
            $pedido->Cd_Item_Orcamento,
            $calculo['item']
          );

          $crud->getSQLGeneric(
            sprintf($sqlAtualizaValor)
          );
        }
      }

    }

    exit;
    

    print_r(array_values($arrPedido));

    foreach (array_values($arrPedido) as $key => $pedido) {
      foreach ($pedido as $p) {
        $foo = $orcamento->calculaPreco($p);
        print_r($foo);

        /*
        $crud->getSQLGeneric(
          sprintf(
            '
            UPDATE Moldura_Item_Orcamento WHERE Cd_Orcamento = %s
          ',
            $p['']
          )
        );
        */
      }
    }



    /*
    $arrProdutos = [
      [
        'handle_produto' => 587,
        'handle_complexidade' => 0,
        'sarrafo_reforco' => 'N',
        'quantidade_item_individual' => 1
      ],

      [
        'handle_produto' => 88,
        'handle_complexidade' => 0,
        'sarrafo_reforco' => 'N',
        'quantidade_item_individual' => 1
      ],
      
      [
        'handle_produto' => 527,
        'handle_complexidade' => 0,
        'sarrafo_reforco' => 'N',
        'quantidade_item_individual' => 1
      ]
    ];
    */



    
    die('Fim');
  }

  public function consultaItensOrcamento()
  {
    $json = array();
    $orcamento = new Orcamento();
    $ItemOrcamento = new ItemOrcamento();
    $MolduraItemOrcamento = new Moldura_Item_Orcamento();
    $ComponenteItemOrcamento = new Componente_Item_Orcamento();

    if (!$_POST) {
      return json_encode(array());
    }
    foreach ($_POST as $key => $value) {
      ${$key} = $value;
    }
    $itemsOrcamento = $ItemOrcamento->listarItemOrcamento($Cd_Orcamento, $item_orcamento);
    $moldurasItemOrcamento = $MolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento, $Cd_Orcamento);
    foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
      $json['Molduras'][] = array('Cd_Produto' => $moldura_item_orcamento->CodigoProduto, 'DescricaoProduto' => $moldura_item_orcamento->NovoCodigo);
    }

    foreach ($itemsOrcamento[0] as $key => $value) {
      $json[$key] = $value;
    }


    echo json_encode($json);
  }

  public function imprimir($handle)
  {
    $orcamento = new Orcamento();
    $produto = new Produto();
    $componente = new Componente();
    $ProdutoAuxiliar = new ProdutoAuxiliar();
    $ItemOrcamento = new ItemOrcamento();
    $MolduraItemOrcamento = new Moldura_Item_Orcamento();
    $ComponenteItemOrcamento = new Componente_Item_Orcamento();
    $configuracoes = new Configuracoes();
    $cliente = new Cliente();
    $vendedor = new Vendedor();
    $empresas = new Empresas();

    $permitir_informar_medidas_em_decimal = filter_var($configuracoes->listarConfiguracao('permitir_informar_medidas_em_decimal')->valor, FILTER_VALIDATE_BOOLEAN);

    $orcamentos = $orcamento->listarOrcamento($handle);

    $itemsOrcamento = $ItemOrcamento->listarItemOrcamento($handle);

    $arrMoldurasItemOrcamento = array();
    $arrComponentesItemOrcamento = array();
    foreach ($itemsOrcamento as $item_orcamento) {
      $moldurasItemOrcamento = $MolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
      foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
        $arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento][] = array('Cd_Produto' => $moldura_item_orcamento->CodigoProduto, 'NovoCodigo' => $moldura_item_orcamento->NovoCodigo ,'DescricaoProduto' => $moldura_item_orcamento->DescricaoProduto, 'NovoCodigo' => $moldura_item_orcamento->NovoCodigo, 'codigoComplexidade' => $moldura_item_orcamento->codigoComplexidade, 'nomeComplexidade' => $moldura_item_orcamento->nomeComplexidade, 'sarrafo_reforco' => $moldura_item_orcamento->sarrafo_reforco);
      }

      if ($this->ocultar_componentes_impressao_pedido == false) {
        $componentesItemOrcamento = $ComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
        foreach ($componentesItemOrcamento as $componente_item_orcamento) {
          $arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento][] = array('Id_Componente' => $componente_item_orcamento->CodigoProduto, 'Descricao' => $componente_item_orcamento->DescricaoProduto, 'quantidade' =>  $componente_item_orcamento->Qt_Item);
        }
      }
    }

    $coluna = '';
    $buscar = '';
    $pagina_atual = 0;
    $linha_inicial = 0;
    $produtos = $produto->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar, 'NovoCodigo ASC');

    $produtosAuxiliar = $ProdutoAuxiliar->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar);

    $componentes = $componente->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar);

    $clientes = $cliente->listarTodos($pagina_atual, $linha_inicial, 'CodigoCliente', (int) $orcamentos[0]->Cd_Cliente);
    $consumidores = $cliente->listarTodos($pagina_atual, $linha_inicial, 'CodigoCliente', (int) $orcamentos[0]->Consumidor_Temp);

    $vendedores = $vendedor->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar);

    $empresa = $empresas->ListarEmpresa($orcamentos[0]->id_empresa);

    $modulos = $this->modulos;
    $classe = $this->classe;

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;

    $layout_impressao = $this->validateGet('parametros') == 'pedidoeos' ? './src/View/Orcamento/orcamento_pedido_os_imprimir.php' : ($this->modelo_empresa == 'venda-avulsa' ? './src/View/Orcamento/orcamento_pedido_imprimir_venda_avulsa.php' : './src/View/Orcamento/orcamento_pedido_imprimir.php');
    if ($this->validateGet('parametros') != 'pedidoeos' && file_exists('./src/View/Orcamento/orcamento_pedido_imprimir_'.$this->ambiente.'.php')) {
      $layout_impressao = './src/View/Orcamento/orcamento_pedido_imprimir_'.$this->ambiente.'.php';
    }    

    // require './src/View/Orcamento/orcamento_pedido_imprimir.php';
    require $layout_impressao;
  }

  public function imprimirOs($handle)
  {
    $orcamento = new Orcamento();
    $produto = new Produto();
    $componente = new Componente();
    $ProdutoAuxiliar = new ProdutoAuxiliar();
    $ItemOrcamento = new ItemOrcamento();
    $MolduraItemOrcamento = new Moldura_Item_Orcamento();
    $ComponenteItemOrcamento = new Componente_Item_Orcamento();
    $cliente = new Cliente();
    $vendedor = new Vendedor();
    $configuracoes = new Configuracoes();

    $permitir_informar_medidas_em_decimal = filter_var($configuracoes->listarConfiguracao('permitir_informar_medidas_em_decimal')->valor, FILTER_VALIDATE_BOOLEAN);

    $orcamentos = $orcamento->listarOrcamento($handle);

    $itemsOrcamento = $ItemOrcamento->listarItemOrcamento($handle);

    $arrMoldurasItemOrcamento = array();
    $arrComponentesItemOrcamento = array();
    foreach ($itemsOrcamento as $item_orcamento) {
      $moldurasItemOrcamento = $MolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
      foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
        $arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento][] = array('Cd_Produto' => $moldura_item_orcamento->CodigoProduto, 'NovoCodigo' => $moldura_item_orcamento->NovoCodigo, 'DescricaoProduto' => $moldura_item_orcamento->DescricaoProduto, 'codigoComplexidade' => $moldura_item_orcamento->codigoComplexidade, 'nomeComplexidade' => $moldura_item_orcamento->nomeComplexidade, 'sarrafo_reforco' => $moldura_item_orcamento->sarrafo_reforco);
      }

      if ($this->ocultar_componentes_impressao_pedido == false) {
        $componentesItemOrcamento = $ComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
        foreach ($componentesItemOrcamento as $componente_item_orcamento) {
          $arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento][] = array('Id_Componente' => $componente_item_orcamento->CodigoProduto, 'NovoCodigo' => $moldura_item_orcamento->NovoCodigo, 'Descricao' => $componente_item_orcamento->DescricaoProduto, 'quantidade' =>  $componente_item_orcamento->Qt_Item);
        }
      }
    }

    $coluna = '';
    $buscar = '';
    $pagina_atual = 0;
    $linha_inicial = 0;
    $produtos = $produto->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar, 'NovoCodigo ASC');

    $produtosAuxiliar = $ProdutoAuxiliar->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar);

    $componentes = $componente->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar);

    $clientes = $cliente->listarTodos($pagina_atual, $linha_inicial, 'CodigoCliente', $orcamentos[0]->Cd_Cliente);
    $consumidores = $cliente->listarTodos($pagina_atual, $linha_inicial, 'CodigoCliente', (int) $orcamentos[0]->Consumidor_Temp);

    $vendedores = $vendedor->listarTodos($pagina_atual, $linha_inicial, $coluna, $buscar);

    $modulos = $this->modulos;
    $classe = $this->classe;

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;

    require file_exists('./src/View/Orcamento/orcamento_os_imprimir_'.$this->ambiente.'.php') ? './src/View/Orcamento/orcamento_os_imprimir_'.$this->ambiente.'.php' : './src/View/Orcamento/orcamento_os_imprimir.php';
  }

  public function cadastrarCliente()
  {
    $cliente = new Cliente();
    if (!$_POST) {
      echo json_encode(array('success' => 0));
      return false;
    }

    $retorno = $cliente->cadastrarCliente($_POST);
    if ($retorno) {
      echo json_encode(array('handle' => $retorno, 'success' => 1, 'message' => "Cliente cadastrado com sucesso! <br> Você pode fechar esta tela e inserir o cliente no orçamento."));
      return true;
    }
  }

  public function enviarEmail($id_orcamento)
  {
    try {

      $objEmpresas = new Empresas();
      $objOrcamento = new Orcamento();
      $objItemOrcamento = new ItemOrcamento();
      $objMolduraItemOrcamento = new Moldura_Item_Orcamento();
      $objComponenteItemOrcamento = new Componente_Item_Orcamento();
      $objEmail = new EmailController();

      $consultaOrcamento = $objOrcamento->listarOrcamento($id_orcamento);
      $itemsOrcamento = $objItemOrcamento->listarItemOrcamento($id_orcamento);
      $consultaEmpresa = $objEmpresas->ListarEmpresa(1);

      if (sizeof($consultaOrcamento) == 0) {
        throw new ErrorException('Pedido ' . $id_orcamento . ' não encontrado');
      }

      $emailDestinatario = $consultaOrcamento[0]->EMail;

      if ($emailDestinatario == "") {
        throw new ErrorException('Endereço de email inválido para o cliente ' . $consultaOrcamento[0]->RazaoSocial . 'Verifique o número cadastrado como Principal no cadastro do cliente');
      }
      $strItemsOrcamento = '';
      foreach ($itemsOrcamento as $item_orcamento) {

        $moldurasItemOrcamento = $objMolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $id_orcamento);
        $arrMoldurasItemOrcamento = [];
        foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
          $arrMoldurasItemOrcamento[] = sprintf('<li>%s</li>', $moldura_item_orcamento->DescricaoProduto);
        }

        $componentesItemOrcamento = $objComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $id_orcamento);
        $arrComponentesOrcamento = [];
        if ($this->ocultar_componentes_impressao_pedido == false) {
          foreach ($componentesItemOrcamento as $componente_item_orcamento) {
            $arrComponentesOrcamento[] = sprintf('<li>%s</li>', $componente_item_orcamento->DescricaoProduto);
          }
        }

        if ($this->modelo_empresa != 'venda-avulsa') {
          $strItemsOrcamento .= sprintf(
            '
              <p><strong>Item: %s</strong></p>
              <strong>Objeto:</strong> %s<br>
              <strong>Qtd:</strong> %s<br>
              <strong>Largura:</strong> %s<br>
              <strong>Altura:</strong> %s<br>
              <strong>Valor Unitário:</strong> %s<br>
              <strong>Valor Total:</strong> %s<br>
              <strong>Produtos</strong>:
              %s
              <br><strong>Observação</strong>:
              <br>%s
            ',
            $item_orcamento->Cd_Item_Orcamento,
            $item_orcamento->descricao,
            $item_orcamento->Qt_Item,
            $item_orcamento->Md_Largura,
            $item_orcamento->Md_Altura,
            $item_orcamento->Vl_Unitario,
            $item_orcamento->Vl_Bruto,
            implode('', $arrMoldurasItemOrcamento) . 
              ($this->ocultar_componentes_impressao_pedido == false  ? '<strong>Componentes:</strong> <br>' . implode('', $arrComponentesOrcamento) : ''),
            $item_orcamento->Ds_Observacao
          );
        } else {
          $strItemsOrcamento .= sprintf(
            '
            <p>
              <strong>Item: %s</strong> <br>
              Qtd: %s <br>
              Valor Unitário: R$ %s <br>
              Valor Total: R$ %s <br>
              %s
            </p>
            ',
            $item_orcamento->descricao_produto_servico,
            $item_orcamento->Qt_Item,
            number_format($item_orcamento->Vl_Unitario, 2, ",", ""),
            number_format($item_orcamento->Vl_Bruto, 2, ",", ""),
            $item_orcamento->Ds_Observacao
  
          );                   
        }
      }


      $strDesconto = '';
      if (($consultaOrcamento[0]->valorDescontoFinal > 0)) {
        $strDesconto = sprintf('Desconto R$: %s', number_format($consultaOrcamento[0]->valorDescontoFinal, 2, ",", ""));
      }

      $strEntrada = 0.00;
      if (($consultaOrcamento[0]->Vl_Entrada > 0)) {
        $strEntrada = sprintf('%s', number_format($consultaOrcamento[0]->Vl_Entrada, 2, ",", ""));
      }

      $strSubTotal = number_format($consultaOrcamento[0]->vl_liquido, 2, ",", "");
      $strSaldo = number_format($consultaOrcamento[0]->vl_liquido - $consultaOrcamento[0]->Vl_Entrada, 2, ",", "");

      $labelPedido = $consultaOrcamento[0]->Id_Situacao <= 1 ? 'orçamento' : 'pedido';

      $mensagem = sprintf(
        '<p>Olá, obrigado por sua confiança na <strong>%s</strong></p>
      Segue %s número <strong>%s</strong> <br/>
       <strong>Cliente:</strong> %s <br/>
       <strong>Data do pedido:</strong> %s <br/>
       <strong>Situação:</strong> %s <br/><br/>
       <strong>Itens do %2$s</strong> <br/>
       %s

       <p>
       <strong>Total do %2$s:</strong> <br/>
       <strong>Subtotal</strong>: R$ %s <br/>
       <strong>Desconto</strong>: R$ %s <br/>
       <strong>Entrada</strong> R$: %s <br/>
       <strong>Saldo</strong>: %s <br/>
       </p>

       <p><strong>Observação:</strong><br>%s</p>
       <p>Você foi atendido por: %s </p>
      ',
        $consultaEmpresa->NomeFantasia,
        $labelPedido,
        $id_orcamento,
        $consultaOrcamento[0]->RazaoSocial,
        date('d/m/Y', strtotime($consultaOrcamento[0]->Dt_Orcamento)),
        $consultaOrcamento[0]->situacao_pedido,
        $strItemsOrcamento,
        number_format((float) $strSubTotal, 2, ',', '.'),
        number_format((float) $strDesconto, 2, ',', '.'),
        number_format((float) $strEntrada, 2, ',', '.'),
        number_format((float) $strSaldo, 2, ',', '.'),
        $consultaOrcamento[0]->Ds_Observacao_Pedido,
        $consultaOrcamento[0]->NomeUsuarioCriado
      );

      /**
       * Remove os espaço em branco no inicio de cada string
       * que é criado automaticamente por causa da formatação
       * usando o sprintf
       */
      $mensagem = preg_replace('/^ */m', '', $mensagem);

      // echo 'Destinatario: ' . $emailDestinatario .   '<textarea rows="50" cols="100">'.$mensagem.'</textarea>';exit;
        // echo 'Destinatario: ' . $emailDestinatario .   '<br>'.$mensagem;exit;

      return $objEmail->enviar([
        'email' => $emailDestinatario,
        'nome' => $consultaOrcamento[0]->RazaoSocial,
        'mensagem' => $mensagem,
        'remetente' => $consultaEmpresa->NomeFantasia,
        'remetente_email' => $consultaEmpresa->EMail,
        'assunto' => sprintf('Pedido %s - %s', $id_orcamento, $consultaEmpresa->NomeFantasia)
      ]);
    } catch (Exception $e) {
      echo json_encode([
        'success' => '0',
        'message' => $e->getMessage(),
        'id' => ''
      ]);
    }
  }

  public function consultaOrcamentosCliente($handle)
  {
    $objOrcamento = new Orcamento();
    $consultaOrcamento = $objOrcamento->listarTodos(0, 0, 2000, 'Dt_Orcamento DESC', '', '', ['Cd_Cliente' => $handle]);
    $arrRetorno = [];
    foreach ($consultaOrcamento as $orcamento) {
      $arrRetorno[] = ['handle' => $orcamento->Cd_Orcamento, 'valor' => $orcamento->vl_liquido];
    }

    echo json_encode($arrRetorno);

    return;
  }
}
