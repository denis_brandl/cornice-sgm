<?php
require_once './src/Model/Category.php';
require_once './src/Model/Contas.php';
require_once './src/Controller/CommonController.php';
class CategoryController extends CommonController {
  private $modulos = [];
  private $arrDespesas = [];
  private $arrReceitas = [];
  private $classe = "Category";
  private $breadcrumb = array();
  private $titulo_principal = "";
  private $contas = array();

  public function __construct() {
    $common = new CommonController();	
    $modulos = $common->getModulos();
    $objContas = new Contas();

    $this->modulos = $modulos;
    $this->contas = $objContas->listarTodos();

    foreach ($this->contas as $conta) {
      if ($conta->idtipo == 0) {
        $this->arrReceitas[] = $conta;
        continue;
      }
      if ($conta->idtipo == 1) {
        $this->arrDespesas[] = $conta;
        continue;
      }      
    }

    $modulo_posicao = array_search($this->classe,array_column($modulos,"modulo"));
    $this->titulo_principal = $modulos[$modulo_posicao];
    $this->breadcrumb = array("Maestria"=>URL."dashboard/index/",$this->titulo_principal["descricao"] => URL.$this->classe."/listar/");
  }

  public function listar() {
    $common = new CommonController();

    $objCategory = new Category();
    $arrCategory = $objCategory->listarTodos();

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;		
    $modulos = $this->modulos;
    $classe = $this->classe;
    $metodo = $acao = "editar";
    require "./src/View/Category/category_listar.php";
  }

  public function editar($handle) {
    $msg_sucesso = "";
    $metodo = "editar";
    $objCategory = new Category();

    if (isset($_POST) && !empty($_POST)) {
      $retorno = $objCategory->editar($_POST);
      if ($retorno) {
        $msg_sucesso = "Categoria alterada com sucesso.";
      }
    }

    $Category = $objCategory->listar($handle);

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;		
    $modulos = $this->modulos;
    $classe = $this->classe;
    require "./src/View/Category/category_form.php";
  }

  public function cadastrar() {
    $msg_sucesso = "";	
    $moedas = "";
    $metodo = "cadastrar";	
    $objCategory = new Category();

    if (isset($_POST) && !empty($_POST)) {
      $retorno = $objCategory->cadastrar($_POST);
      if ($retorno) {
        $msg_sucesso = "Categoria cadastrada com sucesso.";
      }
      $Category = $objCategory->listar($retorno);
      $metodo = "editar";
    } else {
      $Category = $objCategory;
    }
    
    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;		
    $modulos = $this->modulos;
    $classe = $this->classe;
    require "./src/View/Category/category_form.php";	
  }

  public function excluir($handle) {
    $msg_sucesso = "";
    $produtos = "";
    $metodo = "cadastrar";

    $objCategory = new Category();
    $objCategory->excluir($handle);
    $_SESSION["tipoMensagem"] = "callout-success";
    $_SESSION["mensagem"] = "Categoria excluída com sucesso.";
    Header("Location: ".URL."Category/listar/");
    exit();
  }	          
}
