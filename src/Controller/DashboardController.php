<?php
require_once './src/Model/Dashboard.php';
require_once './src/Controller/CommonController.php';
require_once './src/Model/Orcamento.php';
require_once './src/Model/BalanceAmount.php';
require_once './src/Model/Assets.php';
require_once './src/Model/Bills.php';
require_once './src/Model/Empresas.php';
require_once './src/Model/Configuracoes.php';
class DashboardController extends CommonController {

	private $modulos = array();
	private $estados = array();
  private $empresas = array();
	private $classe = 'Dashboard';

  private $habilita_financeiro = false;
	
	public function __construct() {
		$dashboard = new Dashboard();
    $objEmpresa = new Empresas();
		$common = new CommonController();	
		$modulos = $common->getModulos();
    $configuracoes = new Configuracoes();

		$this->modulos = $modulos;
    $this->empresas = $objEmpresa->listarTodos();
    $this->habilita_financeiro = filter_var($configuracoes->listarConfiguracao('habilita_financeiro')->valor, FILTER_VALIDATE_BOOLEAN);    
	}
	
	public function index() {
    $objOrcamento = new Orcamento();
		$modulos = $this->modulos;
		$classe = $this->classe;

    $filtro_empresa = $this->validateGet('filtro_empresa');
		
		$ultimosPedidos = $this->ultimosPedidos();  
		$pedidosPorSituacao = $this->pedidosPorSituacao();

    if ($this->habilita_financeiro) {
      $saldoAtualPorEmpresa = $this->consultaSaldoConta();
    }
		
		$titulo_principal = array('descricao' => 'Dashboard','icone'=> '');
		$breadcrumb = array('Maestria'=>URL.'dashboard/index/');
		
		require './src/View/Dashboard/dashboard_index.php';
	}

  public function listar() {
		$this->index();
	}  
	
	public function ultimosPedidos() {
		$orcamento = new Orcamento();
    $filtro_empresa = $this->validateGet('filtro_empresa');
		$orcamentos = $orcamento->listarTodos(1,0,10,'Cd_Orcamento DESC',"", "", $filtro_empresa ? ['id_empresa' => $filtro_empresa] : []);
		
		return $orcamentos;
	}
	
	private function pedidosPorSituacao() {
		$orcamento = new Orcamento();
    $filtro_empresa = $this->validateGet('filtro_empresa');
		$orcamentos = $orcamento->pedidosPorSituacao($filtro_empresa);
		$arrPedidosPorSituacao = array();
		
		foreach ($orcamentos as $orcamento) {
			$arrPedidosPorSituacao[$orcamento->idSituacao] = $orcamento->total;
		}
		return $arrPedidosPorSituacao;	
	}

  private function consultaSaldoConta() {
    $objBalanceAmount = new BalanceAmount();
    $objAssets = new Assets();
    $objBills = new Bills();
		$consultaSaldosIniciais = $objBalanceAmount->listarTodos();
    $arrSaldoAtualPorEmpresa = [];
    foreach ($consultaSaldosIniciais as $saldo_inicial) {
      $arrSaldoAtualPorEmpresa[$saldo_inicial->UserId][$saldo_inicial->tipo] = ['descricao' => $saldo_inicial->AccountName, 'saldo' => $saldo_inicial->amount];
    }

    foreach ($this->empresas as $empresa) {
      $arrSaldoAtualPorEmpresa[$empresa->CodigoEmpresa]['receber'] = ['descricao' => 'Á Receber', 'saldo' => 0];
      $arrSaldoAtualPorEmpresa[$empresa->CodigoEmpresa]['pagar'] = ['descricao' => 'Á Pagar', 'saldo' => 0];
    }

    $consultaRecebimentos = $objAssets->listarRecebimentosPorPeriodo([]);
    foreach ($consultaRecebimentos as $recebimento) {
      $saldo = isset($arrSaldoAtualPorEmpresa[$recebimento->UserId][$recebimento->tipo]['saldo']) ? $arrSaldoAtualPorEmpresa[$recebimento->UserId][$recebimento->tipo]['saldo'] : 0;
      $arrSaldoAtualPorEmpresa[$recebimento->UserId][$recebimento->tipo]['saldo'] =  $saldo + $recebimento->total;
    }

    $consultaSaidas = $objBills->listarSaidasPorPeriodo([]);
    foreach ($consultaSaidas as $saida) {
      $saldo = isset($arrSaldoAtualPorEmpresa[$saida->UserId][$saida->tipo]['saldo']) ? $arrSaldoAtualPorEmpresa[$saida->UserId][$saida->tipo]['saldo'] : 0;
      $arrSaldoAtualPorEmpresa[$saida->UserId][$saida->tipo]['saldo'] = $saldo - $saida->total;
    }

    $consultaRecebimentosFuturo = $objAssets->listarRecebimentosFuturo();
    foreach ($consultaRecebimentosFuturo as $recebimento_futuro) {
      $arrSaldoAtualPorEmpresa[$recebimento_futuro->UserId]['receber']['saldo'] = $recebimento_futuro->total;
    }
    
    $consultaPagamentosFuturo = $objBills->listarPagamentosFuturo([]);
    foreach ($consultaPagamentosFuturo as $pagamento_futuro) {
      $arrSaldoAtualPorEmpresa[$pagamento_futuro->UserId]['pagar']['saldo'] = $pagamento_futuro->total;
    }
    
    return $arrSaldoAtualPorEmpresa;

  }
}
?>
