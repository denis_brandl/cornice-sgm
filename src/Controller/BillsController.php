<?php
require_once './src/Model/Bills.php';
require_once './src/Model/Category.php';
require_once './src/Model/Fornecedor.php';
require_once './src/Model/Account.php';
require_once './src/Model/Empresas.php';
require_once './src/Controller/CommonController.php';
class BillsController extends CommonController {
  private $modulos = array();
  private $contas = [];
  private $categoriasContasPagar = array();
  private $fornecedores = array();
  private $empresas = [];
  private $estados = array();
  private $classe = "Bills";
  private $breadcrumb = array();
  private $titulo_principal = "";
  private $objCommon;

  public function __construct() {
    $this->objCommon = new CommonController();	
    $objCategoriasFinanceiro = new Category();
    $objFornecedor = new Fornecedor();
    $objContas = new Account();
    $objEmpresa = new Empresas();
    $modulos = $this->objCommon->getModulos();

    $this->categoriasContasPagar = $objCategoriasFinanceiro->listarPorTipo(['tipo' => 1]);

    $this->fornecedores = $objFornecedor->listarTodos();
    $this->contas = $objContas->listarTodos();
    $this->empresas = $objEmpresa->listarTodos();

    $this->modulos = $modulos;

    $modulo_posicao = array_search($this->classe,array_column($modulos,"modulo"));
    $this->titulo_principal = $modulos[$modulo_posicao];
    $this->breadcrumb = array("Maestria"=>URL."dashboard/index/",$this->titulo_principal["descricao"] => URL.$this->classe."/listar/");
  }

  public function listar() {
    $this->objCommon = new CommonController();
    $objBills = new Bills();

    $arrParametros = [];
    if (!empty($_POST)) {
      $arrParametros['filtro'] = array_filter($_POST);
    }
    $arrBills = $objBills->listarTodos($arrParametros);

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;		
    $modulos = $this->modulos;
    $classe = $this->classe;
    $metodo = $acao = "Listar";
    require "./src/View/Bills/bills_listar.php";
  }

  public function editar($handle) {
    $msg_sucesso = "";
    $metodo = "editar";
    $objBills = new Bills();

    if (isset($_POST) && !empty($_POST)) {
      $retorno = $objBills->editar($_POST);
      if ($retorno) {
        $msg_sucesso = "Conta a pagar alterada com sucesso.";
      }
    }

    $Bills = $objBills->listar($handle);

    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;		
    $modulos = $this->modulos;
    $classe = $this->classe;
    require "./src/View/Bills/bills_form.php";
  }

  public function cadastrar() {
    $msg_sucesso = "";	
    $moedas = "";
    $metodo = "cadastrar";	
    $objBills = new Bills();

    if (isset($_POST) && !empty($_POST)) {
      $retorno = $objBills->cadastrar($_POST);
      if ($retorno) {
        $msg_sucesso = "Conta a pagar cadastrada com sucesso.";
      }
      $Bills = $objBills->listar($retorno);
      $metodo = "editar";
    } else {
      $Bills = $objBills;
    }
    $titulo_principal = $this->titulo_principal;
    $breadcrumb = $this->breadcrumb;		
    $modulos = $this->modulos;
    $classe = $this->classe;
    require "./src/View/Bills/bills_form.php";	
  }

  public function excluir($handle) {
    $msg_sucesso = "";
    $produtos = "";
    $metodo = "cadastrar";

    $objBills = new Bills();
    $objBills->excluir($handle);
    $_SESSION["tipoMensagem"] = "callout-success";
    $_SESSION["mensagem"] = "Conta a pagar excluído com sucesso.";
    Header("Location: ".URL."Bills/listar/");
    exit();
  }	          
}
