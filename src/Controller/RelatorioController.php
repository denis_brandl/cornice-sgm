<?php
require_once './src/Controller/CommonController.php';
require_once './src/Model/Orcamento.php';
require_once './src/Model/ItemOrcamento.php';
require_once './src/Model/Moldura_Item_Orcamento.php';
require_once './src/Model/Componente_Item_Orcamento.php';
require_once './src/Model/Produto.php';
require_once './src/Model/FilaDocumentoFiscal.php';
require_once './src/Model/ProdutoAuxiliar.php';
require_once './src/Model/Componente.php';
require_once './src/Model/Cliente.php';
require_once './src/Model/Vendedor.php';
require_once './src/Model/ComposicaoPreco.php';
require_once './src/Model/Situacao.php';
require_once './src/Model/Usuario.php';
require_once './src/Model/Empresas.php';

use Dompdf\Dompdf;

class RelatorioController extends CommonController
{

	private $modulos = array();
	private $estados = array();
	private $classe = 'Relatorio';
	private $breadcrumb = array();
	private $titulo_principal = '';
	private $usuarios = array();
	private $empresas = array();

	public function __construct()
	{
		$modulos = $this->getModulos();
		$estados = $this->getEstados();

		$this->modulos = $modulos;
		$this->estados = $estados;

		$objUsuario = new Usuario();
		$this->usuarios = $objUsuario->listarTodos(0, 0, 'usuario_master', '0');

		$objEmpresas = new Empresas();
		$this->empresas = $objEmpresas->listarTodos(0, 0, 'status', 1);

		$modulo_posicao = 0; // $modulo_posicao = array_search($this->classe,array_column($modulos,'modulo'));

		$this->titulo_principal = array('descricao' => 'Relatório', 'icone' => '');
		$this->breadcrumb = array('Maestria' => URL . 'dashboard/index/', $this->titulo_principal['descricao'] => URL . $this->classe . '/listar/');
	}

	public function documentosFiscais($retorno = '')
	{

		$modulos = $this->modulos;
		$classe = $this->classe;
		$orcamento = new Orcamento();
		$situacao = new Situacao();
		$filaDocumentoFiscal = new FilaDocumentoFiscal();
		$arrTiposDocumentos = [
			[
				'id_tipo_documento' => 1,
				'descricao' => 'Nota fiscal de produto'
			],
			[
				'id_tipo_documento' => 2,
				'descricao' => 'Nota fiscal de serviço'
			],
			[
				'id_tipo_documento' => 3,
				'descricao' => 'Nota Fiscal de consumidor'
			]
		];
		$situacoes = $situacao->listarTodos();
		$arrFiltro = [];

		$titulo_principal = array('descricao' => 'Documentos Fiscais', 'icone' => '');
		$breadcrumb = $this->breadcrumb;

		$pedidosEntregues = array();
		$periodo_inicial = date('Y-m-01 00:00:00', strtotime('-30 days'));
		$periodo_final 	 =  date('Y-m-t 23:59:59', strtotime('-30 days'));

		$arrFiltro['somente_pedidos_validos'] = '';
		$arrFiltro['pedido'] = '';
		$consultaDocumentos = [];
		if ($this->validateGet('parametros')) {
			$re = "/^[a-z]+=/";
			preg_match($re, $this->validateGet('parametros'), $matches);
			$acao = str_replace('=', '', $matches[0]);

			$re = "/([0-9].*)\|([0-9].*)$/";
			preg_match($re, urldecode($this->validateGet('parametros')), $matches);

			switch ($acao) {
				case 'buscar':
					if (isset($matches[0])) {
						$filtros = explode("|", $matches[0]);

						if (isset($filtros[0])) {
							$arrFiltro['periodo_inicial'] = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $filtros[0])));
							$periodo_inicial = $arrFiltro['periodo_inicial'];
						}
						if (isset($filtros[1])) {
							$arrFiltro['periodo_final'] = date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $filtros[1])));
							$periodo_final 	 =  $arrFiltro['periodo_final'];
						}

						if (isset($filtros[2])) {
							$arrFiltro['filtro_tipo_documento'] = $filtros[2];
						}

						// if (isset($filtros[3])) {
						// 	$arrFiltro['filtro_pago'] = $filtros[3];
						// }

						if (isset($filtros[4])) {
							$arrFiltro['tipo_data'] = $filtros[4];
						}

						if (isset($filtros[5])) {
							$arrFiltro['criado_por'] = $filtros[5];
						}

						if (isset($filtros[6])) {
							$arrFiltro['empresa'] = $filtros[6];
						}

						if (isset($filtros[7])) {
							$arrFiltro['cliente'] = $filtros[7];
						}

						if (isset($filtros[8])) {
							$arrFiltro['pedido'] = $filtros[8];
						}

						if (isset($filtros[9]) && $filtros[9] == 1) {
							$arrFiltro['somente_pedidos_validos'] = $filtros[9];
						}
					}

					$consultaDocumentos = $filaDocumentoFiscal->relatorio($arrFiltro);

					break;
			}
		}

		if ($retorno == 'dados') {
			return $consultaDocumentos;
		}
		require './src/View/Relatorio/documentos_fiscais.php';
	}

	public function documentosFiscaisExcel()
	{


		$consultaDocumentos = $this->documentosFiscais('dados');

		$arrListaDocumentos[] = [
			'NFCRetornoId',
			'PedidoId',
			'NaturezaOperacao',
			'TipoDocumento',
			'FinalidadeEmissao',
			'MensagemSEFAZ',
			'ChaveNfe',
			'Numero',
			'Serie',
			'NumeroSerie',
			'Valor',
			'LoginEmissor',
			'DataEmissao',
			'DataHoraEmissao',
			'ClienteNome',
			'Email',
			'QrcodeUrl',
			'UrlConsultaNf',
			'CaminhoDanfe',
			'CaminhoXmlNotaFiscal',
			'CaminhoXmlCancelamento',
			'CnpjEmitente',
			'ref',
			'Status',
			'StatusSefaz',
			'Email1',
			'Ambiente',
			'Consultas',
			'DataCancelamento',
			'Justificativa'
		];
		foreach ($consultaDocumentos as $documento) {
			$arrListaDocumentos[] = [
				'NFCRetornoId' => $documento->NFCRetornoId,
				'PedidoId' => $documento->PedidoId,
				'NaturezaOperacao' => $documento->NaturezaOperacao,
				'TipoDocumento' => $documento->TipoDocumento,
				'FinalidadeEmissao' => $documento->FinalidadeEmissao,
				'MensagemSEFAZ' => $documento->MensagemSEFAZ,
				'ChaveNfe' => $documento->ChaveNfe,
				'Numero' => $documento->Numero,
				'Serie' => $documento->Serie,
				'NumeroSerie' => $documento->NumeroSerie,
				'Valor' => $documento->Valor,
				'LoginEmissor' => $documento->LoginEmissor,
				'DataEmissao' => $documento->DataEmissao,
				'DataHoraEmissao' => $documento->DataHoraEmissao,
				'ClienteNome' => $documento->ClienteNome,
				'Email' => $documento->Email,
				'QrcodeUrl' => $documento->QrcodeUrl,
				'UrlConsultaNf' => $documento->UrlConsultaNf,
				'CaminhoDanfe' => '', // $documento->CaminhoDanfe,
				'CaminhoXmlNotaFiscal' => $documento->ChaveNfe . '.xml',
				'CaminhoXmlCancelamento' => $documento->CaminhoXmlCancelamento,
				'CnpjEmitente' => $documento->CnpjEmitente,
				'ref' => $documento->ref,
				'Status' => $documento->Status,
				'StatusSefaz' => $documento->StatusSefaz,
				'Email1' => $documento->Email1,
				'Ambiente' => $documento->Ambiente,
				'Consultas' => $documento->Consultas,
				'DataCancelamento' => $documento->DataCancelamento,
				'Justificativa' => $documento->Justificativa,

			];
		}

		$xlsx = Shuchkin\SimpleXLSXGen::fromArray($arrListaDocumentos);
		$nome_arquivo = sprintf('QuadrosRio1_NFPlan_%s', '01-08-2024_31-08-2024.xlsx');
		$xlsx->downloadAs($nome_arquivo);
	}

	public function downloadDocumentosFiscais()
	{


		try {
			$consultaDocumentos = $this->documentosFiscais('dados');

			$diretorio_xml = dirname(__DIR__, 2) . '/publico/xml/';
			$local_zip = dirname(__DIR__, 2) . '/publico/notas.zip';

			if (file_exists($local_zip)) {
				unlink($local_zip);
			}

			$objZip = new ZipArchive;

			$arquivoZip = $objZip->open($local_zip, ZipArchive::CREATE);

			if ($arquivoZip !== TRUE) {
				throw new ErrorException('Erro ao criar o arquivo zip');
			}

			if (!file_exists($diretorio_xml)) {
				mkdir($diretorio_xml, 0777, true);
			} else {
				array_map('unlink', glob("$diretorio_xml/*.*"));
			}
			

			$arrDados = [];


			foreach ($consultaDocumentos as $documento) {
				$cnpj_emitente = $this->onlyNumber($documento->CnpjEmitente);
				$tipo_documento = strtolower($documento->TipoDocumentoSigla);
				if (!isset($arrDados[$cnpj_emitente])) {
					$arrDados[$cnpj_emitente] = [];
				}

				if (!isset($arrDados[$cnpj_emitente])) {
					$arrDados[$cnpj_emitente][$tipo_documento] = [];
				}

				$arrDados[$cnpj_emitente][$tipo_documento][]  = [
					'nome_arquivo' => $documento->ChaveNfe . '.xml',
					'conteudo' => base64_decode($documento->CaminhoXmlNotaFiscal)
				];
			}

			foreach ($arrDados as $cnpj => $documentos) {
				foreach ($documentos as $tipo => $listaDocumentos) {
					foreach ($listaDocumentos as $documento) {
						$objZip->addFromString(
							sprintf(
								'%s/%s/%s',
								$cnpj,
								$tipo.'-e',
								$documento['nome_arquivo']
							),
							$documento['conteudo']
						);
					}
				}
			}
			$objZip->close();


			ob_clean(); flush();

			header('Content-Type: application/zip');
			header("Content-Transfer-Encoding: Binary");
			header("Content-Length: ".filesize($local_zip));
			header('Content-Disposition: attachment; filename=notas.zip');
			readfile($local_zip);
			exit;
		} catch (Exception $e) {
			echo $e->getMessage();
			exit;
		}

	}

	public function pedidosEntregues()
	{

		$modulos = $this->modulos;
		$classe = $this->classe;
		$orcamento = new Orcamento();
		$situacao = new Situacao();
		$situacoes = $situacao->listarTodos();
		$arrFiltro = [];

		$titulo_principal = array('descricao' => 'Relatório - Pedidos Entregue', 'icone' => '');
		$breadcrumb = $this->breadcrumb;

		$pedidosEntregues = array();
		$periodo_inicial = date('Y-m-d 00:00:00', strtotime('-30 days'));
		$periodo_final 	 =  date('Y-m-d 23:59:59');
		if ($this->validateGet('parametros')) {
			$re = "/^[a-z]+=/";
			preg_match($re, $this->validateGet('parametros'), $matches);
			$acao = str_replace('=', '', $matches[0]);

			$re = "/([0-9].*)\|([0-9].*)$/";
			preg_match($re, urldecode($this->validateGet('parametros')), $matches);

			switch ($acao) {
				case 'buscar':
					if (isset($matches[0])) {
						$filtros = explode("|", $matches[0]);
						if (isset($filtros[0])) {
							$arrFiltro['periodo_inicial'] = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $filtros[0])));
							$periodo_inicial = $arrFiltro['periodo_inicial'];
						}
						if (isset($filtros[1])) {
							$arrFiltro['periodo_final'] = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $filtros[1])));
							$periodo_final 	 =  $arrFiltro['periodo_final'];
						}

						if (isset($filtros[2])) {
							$arrFiltro['filtro_situacao'] = $filtros[2];
						}

						if (isset($filtros[3])) {
							$arrFiltro['filtro_pago'] = $filtros[3];
						}

						if (isset($filtros[4])) {
							$arrFiltro['tipo_data'] = $filtros[4];
						}

						if (isset($filtros[5])) {
							$arrFiltro['criado_por'] = $filtros[5];
						}

						if (isset($filtros[6])) {
							$arrFiltro['empresa'] = $filtros[6];
						}
					}

					$pedidosEntregues = $orcamento->listarOrcamentoPorDataEntregue($arrFiltro);

					break;
			}
		}

		require './src/View/Relatorio/pedidos_entregue.php';
	}

	public function pedidosSimplificado()
	{

		$modulos = $this->modulos;
		$classe = $this->classe;
		$orcamento = new Orcamento();
		$situacao = new Situacao();
		$situacoes = $situacao->listarTodos();
		$arrFiltro = [];

		$titulo_principal = array('descricao' => 'Relatório - Sumário de pedidos', 'icone' => '');
		$breadcrumb = $this->breadcrumb;

		$pedidosEntregues = array();
		$periodo_inicial = date('Y-m-d 00:00:00', strtotime('-30 days'));
		$periodo_final 	 =  date('Y-m-d 23:59:59');

		$arrFiltro['somente_pedidos_validos'] = '';
		$arrFiltro['pedido'] = '';
		if ($this->validateGet('parametros')) {
			$re = "/^[a-z]+=/";
			preg_match($re, $this->validateGet('parametros'), $matches);
			$acao = str_replace('=', '', $matches[0]);

			$re = "/([0-9].*)\|([0-9].*)$/";
			preg_match($re, urldecode($this->validateGet('parametros')), $matches);

			switch ($acao) {
				case 'buscar':
					if (isset($matches[0])) {
						$filtros = explode("|", $matches[0]);

						if (isset($filtros[0])) {
							$arrFiltro['periodo_inicial'] = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $filtros[0])));
							$periodo_inicial = $arrFiltro['periodo_inicial'];
						}
						if (isset($filtros[1])) {
							$arrFiltro['periodo_final'] = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $filtros[1])));
							$periodo_final 	 =  $arrFiltro['periodo_final'];
						}

						if (isset($filtros[2])) {
							$arrFiltro['filtro_situacao'] = $filtros[2];
						}

						// if (isset($filtros[3])) {
						// 	$arrFiltro['filtro_pago'] = $filtros[3];
						// }

						if (isset($filtros[4])) {
							$arrFiltro['tipo_data'] = $filtros[4];
						}

						if (isset($filtros[5])) {
							$arrFiltro['criado_por'] = $filtros[5];
						}

						if (isset($filtros[6])) {
							$arrFiltro['empresa'] = $filtros[6];
						}

						if (isset($filtros[7])) {
							$arrFiltro['cliente'] = $filtros[7];
						}

						if (isset($filtros[8])) {
							$arrFiltro['pedido'] = $filtros[8];
						}

						if (isset($filtros[9]) && $filtros[9] == 1) {
							$arrFiltro['somente_pedidos_validos'] = $filtros[9];
						}
					}

					$pedidosEntregues = $orcamento->listarOrcamentoSimplificado($arrFiltro);

					break;
			}
		}

		require './src/View/Relatorio/pedidos_simplificado.php';
	}

	public function comissao()
	{

		$modulos = $this->modulos;
		$classe = $this->classe;
		$orcamento = new Orcamento();
		$situacao = new Situacao();
		$configuracoes = new Configuracoes();
		$habilita_financeiro = filter_var($configuracoes->listarConfiguracao('habilita_financeiro')->valor, FILTER_VALIDATE_BOOLEAN);
		$relatorio_comissao_considerar_somente_pago = filter_var($configuracoes->listarConfiguracao('relatorio_comissao_considerar_somente_pago')->valor, FILTER_VALIDATE_BOOLEAN);

		$situacoes = $situacao->listarTodos();
		$arrFiltro = [];

		$titulo_principal = array('descricao' => 'Relatório - Comissão', 'icone' => '');
		$breadcrumb = $this->breadcrumb;

		$pedidosEntregues = array();
		$periodo_inicial = date('Y-m-d 00:00:00', strtotime('-30 days'));
		$periodo_final 	 =  date('Y-m-d 23:59:59');
		$arrComissoes = [];
		if ($this->validateGet('parametros')) {
			$re = "/^[a-z]+=/";
			preg_match($re, $this->validateGet('parametros'), $matches);
			$acao = str_replace('=', '', $matches[0]);

			$re = "/([0-9].*)\|([0-9].*)$/";
			preg_match($re, urldecode($this->validateGet('parametros')), $matches);

			switch ($acao) {
				case 'buscar':
					if (isset($matches[0])) {
						$filtros = explode("|", $matches[0]);
						if (isset($filtros[0])) {
							$arrFiltro['periodo_inicial'] = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $filtros[0])));
							$periodo_inicial = $arrFiltro['periodo_inicial'];
						}
						if (isset($filtros[1])) {
							$arrFiltro['periodo_final'] = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $filtros[1])));
							$periodo_final 	 =  $arrFiltro['periodo_final'];
						}

						if (isset($filtros[2])) {
							$arrFiltro['filtro_situacao'] = $filtros[2];
						}

						if (isset($filtros[4])) {
							$arrFiltro['tipo_data'] = $filtros[4];
						}

						if (isset($filtros[5])) {
							$arrFiltro['vendedor'] = $filtros[5];
						}
					}
					break;
			}
			$arrComissoes = $orcamento->listarOrcamentoComComissao($arrFiltro);
		}

		require './src/View/Relatorio/comissao.php';
	}


	public function comissaoPdf()
	{

		set_time_limit(300);
		$modulos = $this->modulos;
		$classe = $this->classe;
		$orcamento = new Orcamento();
		$situacao = new Situacao();
		$empresas = new Empresas();
		$configuracoes = new Configuracoes();
		$situacoes = $situacao->listarTodos();
		$arrFiltro = [];

		$habilita_financeiro = filter_var($configuracoes->listarConfiguracao('habilita_financeiro')->valor, FILTER_VALIDATE_BOOLEAN);
		$relatorio_comissao_considerar_somente_pago = filter_var($configuracoes->listarConfiguracao('relatorio_comissao_considerar_somente_pago')->valor, FILTER_VALIDATE_BOOLEAN);

		$titulo_principal = array('descricao' => 'Relatório - Comissão', 'icone' => '');
		$breadcrumb = $this->breadcrumb;

		$empresa = $empresas->ListarEmpresa(1);

		$pedidosEntregues = array();
		$periodo_inicial = date('Y-m-d 00:00:00', strtotime('-30 days'));
		$periodo_final 	 =  date('Y-m-d 23:59:59');
		$arrComissoes = [];
		if ($this->validateGet('parametros')) {
			$re = "/^[a-z]+=/";
			preg_match($re, $this->validateGet('parametros'), $matches);
			$acao = str_replace('=', '', $matches[0]);

			$re = "/([0-9].*)\|([0-9].*)$/";
			preg_match($re, urldecode($this->validateGet('parametros')), $matches);

			switch ($acao) {
				case 'buscar':
					if (isset($matches[0])) {
						$filtros = explode("|", $matches[0]);
						if (isset($filtros[0])) {
							$arrFiltro['periodo_inicial'] = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $filtros[0])));
							$periodo_inicial = $arrFiltro['periodo_inicial'];
						}
						if (isset($filtros[1])) {
							$arrFiltro['periodo_final'] = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $filtros[1])));
							$periodo_final 	 =  $arrFiltro['periodo_final'];
						}

						if (isset($filtros[2])) {
							$arrFiltro['filtro_situacao'] = $filtros[2];
						}

						if (isset($filtros[4])) {
							$arrFiltro['tipo_data'] = $filtros[4];
						}

						if (isset($filtros[5])) {
							$arrFiltro['vendedor'] = $filtros[5];
						}
					}
					break;
			}
			$arrComissoes = $orcamento->listarOrcamentoComComissao($arrFiltro);

			$dados_empresa = sprintf(
				'<p style="text-align:justify;">
					<strong>%s</strong>
				</p>
				<p> %s </p>
				<p> %s - %s - %s CEP: %s </p>
				<p> TELEFONE: %s </p>
				',
				$empresa->RazaoSocial,
				$empresa->Endereco,
				$empresa->Bairro,
				$empresa->Cidade,
				$empresa->Estado,
				$empresa->CEP,
				$empresa->Telefone1,
			);

			$html_final = '
				<div style="position: relative;
				left: 10em;
				width: 100%;
				height: 10em;">' . $dados_empresa . '
				</div> <h1 style="text-align:center"> Relatório de Comissões por pagamento </h1>';

			$total_valor_liquido = 0;
			$total_valor_comissao = 0;
			foreach ($arrComissoes as $comissao) {

				$valor_comissao = $relatorio_comissao_considerar_somente_pago ? $comissao->valor_comissao_pedido_receber  : $comissao->valor_comissao;

				$data_pagamento = $comissao->data_pagamento_pedido != '' ? date('d/m/Y', strtotime($comissao->data_pagamento_pedido)) : '<i>Não informado</i>';
				if ($habilita_financeiro) {
					$data_pagamento = $comissao->data_pagamento_conta_receber != '' ? date('d/m/Y', strtotime($comissao->data_pagamento_conta_receber)) : '<i>Não informado</i>';
				}

				// $data_pagamento = $comissao->data_pagamento != '' ? date('d/m/Y', strtotime($comissao->data_pagamento)) : '';
				$arrFormaPagamentoEntrada = !is_null($comissao->forma_pagamento_entrada) ? explode('|', $comissao->forma_pagamento_entrada) : [];
				$arrFormaPagamentoSaldo = !is_null($comissao->forma_pagamento_saldo) ? explode('|', $comissao->forma_pagamento_saldo) : [];

				$descricao_entrada = isset($arrFormaPagamentoEntrada[0]) ? $arrFormaPagamentoEntrada[0] . ' ' . $arrFormaPagamentoEntrada[1] . 'X' : '';
				$descricao_saldo = isset($arrFormaPagamentoSaldo[0]) ? $arrFormaPagamentoSaldo[0] . ' ' . $arrFormaPagamentoSaldo[1] . 'X' : '';

				$descricao_forma_pagamento = sprintf(
					'
						%s
						%s
						%s
					',
					$descricao_entrada,
					($descricao_entrada != '' && $descricao_saldo != '') ? '<br>' : '',
					$descricao_saldo

				);

				$coluna_conta_receber = '';
				$coluna_valor_conta_receber = '';
				if ($habilita_financeiro) {
					$coluna_conta_receber = '<th style="background-color: #d6d6d6"> Número do <br> compromisso </th>';
					$coluna_valor_conta_receber = '<td style="text-align:center;"> ' . str_pad($comissao->handle_conta_receber, 4, '0', STR_PAD_LEFT) . '</td>';
				}

				$html_final .= '<table width="100%" border="1">
				<tbody> <tr>
					<td align="center" colspan="' . ($habilita_financeiro ? '6' : '5') . '">
						<p>
							<strong>Pedido: ' . str_pad($comissao->handle_orcamento, 4, '0', STR_PAD_LEFT) . '</strong>
							<strong>Cliente: ' . strtoupper($comissao->nome_cliente) . '</strong>
							<strong>Vendedor:' . $comissao->nome_vendedor . '</strong>
						</p>
						<p>
							Emissão: ' . date('d/m/Y', strtotime($comissao->data_orcamento)) . '
							Valor:  R$ ' . number_format($comissao->valor_liquido, 2, ",", ".") . '
							Comissão Prevista: R$ ' . number_format($valor_comissao, 2, ",", ".") . '
						</p>
					</td>
					</tr>
					<tr>
						<th style="min-width:200px;background-color: #d6d6d6"> Forma de <br> Pagamento </th>
						<th style="background-color: #d6d6d6"> Data <br> Pagamento </th>
						' . $coluna_conta_receber . '
						<th style="background-color: #d6d6d6"> Valor do <br> compromisso </th>
						<th style="background-color: #d6d6d6"> Valor a <br> considerar </th>
						<th style="background-color: #d6d6d6"> Comissão do <br> vendedor </th>
					</tr>
					<tr>
						<td style="text-align:center;"> ' . $descricao_forma_pagamento . ' </td>
						<td style="text-align:center;">' . ($data_pagamento ?: '<i>Não cadastrado</i>') . '</td>
						' . $coluna_valor_conta_receber . '
						<td style="text-align:center;">'  . number_format($comissao->valor_liquido, 2, ",", ".") . '</td>
						<td style="text-align:center;">'  . number_format($comissao->valor_conta_receber, 2, ",", ".") . '</td>
						<td style="text-align:center;">'  . number_format($valor_comissao, 2, ",", ".") . '</td>
					</tr>
					</tbody>
					</table>
					<br>';

				$total_valor_liquido += $comissao->valor_liquido;
				$total_valor_comissao += $comissao->valor_comissao;
			}

			$html_final .= '<p style="line-height:10px;">Total Valor a considerar: R$ ' . number_format($total_valor_liquido, 2, ",", ".") . '</p>';
			$html_final .= '<p>Total Comissão Vendedor: R$ ' . number_format($total_valor_comissao, 2, ",", ".") . '</p>';

			// echo $html_final;exit;

			$dompdf = new Dompdf();
			$dompdf->setPaper('A4', 'portrait');
			$dompdf->loadHtml($html_final);
			$dompdf->render();

			$dompdf->stream(
				'comissoes_pagamento.pdf',
				[
					'Attachment' => false,
					'compress' => true
				]
			);
		}
	}

	public function comissaoHtml()
	{

		$modulos = $this->modulos;
		$classe = $this->classe;
		$orcamento = new Orcamento();
		$situacao = new Situacao();
		$empresas = new Empresas();
		$configuracoes = new Configuracoes();
		$situacoes = $situacao->listarTodos();
		$arrFiltro = [];

		$habilita_financeiro = filter_var($configuracoes->listarConfiguracao('habilita_financeiro')->valor, FILTER_VALIDATE_BOOLEAN);
		$relatorio_comissao_considerar_somente_pago = filter_var($configuracoes->listarConfiguracao('relatorio_comissao_considerar_somente_pago')->valor, FILTER_VALIDATE_BOOLEAN);

		$titulo_principal = array('descricao' => 'Relatório - Comissão', 'icone' => '');
		$breadcrumb = $this->breadcrumb;

		$empresa = $empresas->ListarEmpresa(1);

		$pedidosEntregues = array();
		$periodo_inicial = date('Y-m-d 00:00:00', strtotime('-30 days'));
		$periodo_final 	 =  date('Y-m-d 23:59:59');
		$arrComissoes = [];
		if ($this->validateGet('parametros')) {
			$re = "/^[a-z]+=/";
			preg_match($re, $this->validateGet('parametros'), $matches);
			$acao = str_replace('=', '', $matches[0]);

			$re = "/([0-9].*)\|([0-9].*)$/";
			preg_match($re, urldecode($this->validateGet('parametros')), $matches);

			switch ($acao) {
				case 'buscar':
					if (isset($matches[0])) {
						$filtros = explode("|", $matches[0]);
						if (isset($filtros[0])) {
							$arrFiltro['periodo_inicial'] = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $filtros[0])));
							$periodo_inicial = $arrFiltro['periodo_inicial'];
						}
						if (isset($filtros[1])) {
							$arrFiltro['periodo_final'] = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $filtros[1])));
							$periodo_final 	 =  $arrFiltro['periodo_final'];
						}

						if (isset($filtros[2])) {
							$arrFiltro['filtro_situacao'] = $filtros[2];
						}

						if (isset($filtros[4])) {
							$arrFiltro['tipo_data'] = $filtros[4];
						}

						if (isset($filtros[5])) {
							$arrFiltro['vendedor'] = $filtros[5];
						}
					}
					break;
			}
			$arrComissoes = $orcamento->listarOrcamentoComComissao($arrFiltro);

			$html = '
			<html>
			<head>
				<style>
				#tabela {
				font-family: Arial, Helvetica, sans-serif;
				border-collapse: collapse;
				width: 100%;
				}

				#tabela td, #tabela th {
				border: 1px solid #ddd;
				padding: 8px;
				}

				#tabela tr:nth-child(even){background-color: #f2f2f2;}

				#tabela tr:hover {background-color: #ddd;}

				#tabela th {
				padding-top: 12px;
				padding-bottom: 12px;
				text-align: left;
				
				}

				@page {
					size: auto;   /* auto is the initial value */
    				margin: 0;  /* this affects the margin in the printer settings */
				}
				</style>
			</head>
			<body>
			<table id="tabela">
				<thead>
					<tr role="row">
						<th style="width: 162px;text-align:left;">Pedido</th>
						<th style="width: 207px;text-align:left;">Status</th>
						<th style="width: 207px;text-align:left;">Data Pedido</th>
						<th style="width: 207px;text-align:left;">Vendedor</th>
						<th style="width: 207px;text-align:left;">Cliente</th>
						<th style="width: 207px;text-align:left;">Data de pagamento</th>
						<th style="width: 207px;text-align:left;">Valor Bruto</th>
						<th style="width: 207px;text-align:left;">Valor Desconto</th>
						<th style="width: 207px;text-align:left;">Valor do compromisso</th>
						<th style="width: 207px;text-align:left;">Valor a considerar <br> <span class="small">(para comissão)</span></th>
						<th style="width: 207px;text-align:left;">Valor Comissão</th>
					</tr>
				</thead>
				<tbody>
			';
					$soma_total_comissao = 0;
					foreach ($arrComissoes as $comissao) {
						$valor_comissao = $relatorio_comissao_considerar_somente_pago ? $comissao->valor_comissao_pedido_receber  : $comissao->valor_comissao;
						$soma_total_comissao += $valor_comissao;

						$data_pagamento = $comissao->data_pagamento_pedido != '' ? date('d/m/Y', strtotime($comissao->data_pagamento_pedido)) : '<i>Não informado</i>';
						if ($habilita_financeiro) {
							$data_pagamento = $comissao->data_pagamento_conta_receber != '' ? date('d/m/Y', strtotime($comissao->data_pagamento_conta_receber)) : '<i>Não informado</i>';
						}

						$html .= '
							<tr>
								<td>' . $comissao->handle_orcamento.'</td>
								<td>' . $comissao->situacao.'</td>
								<td>' . ($comissao->data_orcamento != '' ? date('d/m/Y', strtotime($comissao->data_orcamento)) : '').'</td>
								<td>' . $comissao->nome_vendedor.'</td>
								<td>' . $comissao->nome_cliente.'</td>
								<td>' . $data_pagamento.'</td>
								<td>' . 'R$ ' . number_format($comissao->valor_bruto, 2, ",", ".").'</td>
								<td>' . 'R$ ' . number_format($comissao->valor_desconto, 2, ",", ".").'</td>
								<td>' . 'R$ ' . number_format($comissao->valor_liquido, 2, ",", ".").'</td>
								<td>' . 'R$ ' . number_format($comissao->valor_conta_receber, 2, ",", ".").'</td>
								<td>' . 'R$ ' . number_format($valor_comissao, 2, ",", ".").'</td>
							</tr>';
					}
					$html .= '</tbody>
								<tbody>
									<tr>
										<th colspan="10" style="text-align: right;">Total Comissão</th>
										<th>R$ ' . number_format($soma_total_comissao, 2, ",", ".") . '</th>
									</tr>
								</tbody>
							</table>
							</body>
							<script>
								document.title = "Relatório Comissão";
								window.print();
								// setTimeout("window.close()",5000)
							</script>
							</html>
					';

					echo $html;
		}
	}	

	public function impressaoFabrica()
	{

		$modulos = $this->modulos;
		$classe = $this->classe;
		$orcamento = new Orcamento();
		$ItemOrcamento = new ItemOrcamento();
		$MolduraItemOrcamento = new Moldura_Item_Orcamento();
		$ComponenteItemOrcamento = new Componente_Item_Orcamento();

		$titulo_principal = array('descricao' => 'Relatório - Pedidos Fábrica', 'icone' => '');
		$breadcrumb = $this->breadcrumb;

		$periodo_inicial = date('d/m/Y');
		$periodo_final = date('d/m/Y', strtotime('+6 days'));
		$empresa = '';
		$tipo_data = 'Dt_Orcamento';

		$pedidosEntregues = array();

		if ($this->validateGet('parametros')) {
			$matches = explode("|", urldecode($this->validateGet('parametros')));

			if (isset($matches[0])) {
				$periodo_inicial = $matches[0];
			}
			if (isset($matches[1])) {
				$periodo_final = $matches[1];
			}

			if (isset($matches[2]) && $matches[2] != '0') {
				$empresa = $matches[2];
			}

			if (isset($matches[3])) {
				$tipo_data = $matches[3];
			}

			$periodo_inicial = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $periodo_inicial)));
			$periodo_final 	 =  date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $periodo_final)));

			$pedidosEntregues = $orcamento->listarOrcamentoPorDataPrevista($periodo_inicial, $periodo_final, $empresa, $tipo_data);
			$arrMoldurasItemOrcamento = array();
			$arrComponentesItemOrcamento = array();
			foreach ($pedidosEntregues as $item_orcamento) {
				$handle = $item_orcamento->Cd_Orcamento;
				$moldurasItemOrcamento = $MolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
				foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
					$arrMoldurasItemOrcamento[$handle][$item_orcamento->Cd_Item_Orcamento][] = array('Cd_Produto' => $moldura_item_orcamento->CodigoProduto, 'DescricaoProduto' => $moldura_item_orcamento->NovoCodigo, 'codigoComplexidade' => $moldura_item_orcamento->codigoComplexidade, 'nomeComplexidade' => $moldura_item_orcamento->nomeComplexidade);
				}


				$componentesItemOrcamento = $ComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
				foreach ($componentesItemOrcamento as $componente_item_orcamento) {
					$arrComponentesItemOrcamento[$handle][$item_orcamento->Cd_Item_Orcamento][] = array('Id_Componente' => $componente_item_orcamento->CodigoProduto, 'Descricao' => $componente_item_orcamento->DescricaoProduto);
				}
			}

			$periodo_inicial = date('d/m/Y', strtotime($periodo_inicial));
			$periodo_final = date('d/m/Y', strtotime($periodo_final));
		}

		require './src/View/Relatorio/impressao_fabrica.php';
	}

	public function impressaoFabricaHtml()
	{

		$modulos = $this->modulos;
		$classe = $this->classe;
		$orcamento = new Orcamento();
		$ItemOrcamento = new ItemOrcamento();
		$MolduraItemOrcamento = new Moldura_Item_Orcamento();
		$ComponenteItemOrcamento = new Componente_Item_Orcamento();

		$titulo_principal = array('descricao' => 'Relatório - Pedidos Fábrica', 'icone' => '');
		$breadcrumb = $this->breadcrumb;

		$periodo_inicial = date('d/m/Y');
		$periodo_final = date('d/m/Y', strtotime('+6 days'));
		$empresa = '';
		$tipo_data = 'Dt_Orcamento';

		$pedidosEntregues = array();

		if ($this->validateGet('parametros')) {
			$matches = explode("|", urldecode($this->validateGet('parametros')));

			if (isset($matches[0])) {
				$periodo_inicial = $matches[0];
			}
			if (isset($matches[1])) {
				$periodo_final = $matches[1];
			}

			if (isset($matches[2]) && $matches[2] != '0') {
				$empresa = $matches[2];
			}

			if (isset($matches[3])) {
				$tipo_data = $matches[3];
			}

			$periodo_inicial = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $periodo_inicial)));
			$periodo_final 	 =  date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $periodo_final)));

			$pedidosEntregues = $orcamento->listarOrcamentoPorDataPrevista($periodo_inicial, $periodo_final, $empresa, $tipo_data);
			$arrMoldurasItemOrcamento = array();
			$arrComponentesItemOrcamento = array();
			foreach ($pedidosEntregues as $item_orcamento) {
				$handle = $item_orcamento->Cd_Orcamento;
				$moldurasItemOrcamento = $MolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
				foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
					$arrMoldurasItemOrcamento[$handle][$item_orcamento->Cd_Item_Orcamento][] = array('Cd_Produto' => $moldura_item_orcamento->CodigoProduto, 'DescricaoProduto' => $moldura_item_orcamento->NovoCodigo, 'codigoComplexidade' => $moldura_item_orcamento->codigoComplexidade, 'nomeComplexidade' => $moldura_item_orcamento->nomeComplexidade);
				}


				$componentesItemOrcamento = $ComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
				foreach ($componentesItemOrcamento as $componente_item_orcamento) {
					$arrComponentesItemOrcamento[$handle][$item_orcamento->Cd_Item_Orcamento][] = array('Id_Componente' => $componente_item_orcamento->CodigoProduto, 'Descricao' => $componente_item_orcamento->DescricaoProduto);
				}
			}

			$periodo_inicial = date('d/m/Y', strtotime($periodo_inicial));
			$periodo_final = date('d/m/Y', strtotime($periodo_final));

			$CodigoCliente = 0;
			$CodigoPedido = 0;
			$total = 0;
			$total_bruto_cliente = 0;
			$total_desconto_cliente = 0;
			$total_liquido_cliente = 0;

			$total_bruto_geral = 0;
			$total_desconto_geral = 0;
			$total_liquido_geral = 0;
			$aux = 1;
			$quantidade = count($pedidosEntregues);
			$html_relatorio = '<html>
			<head>
				<style>
				#tabela {
				font-family: Arial, Helvetica, sans-serif;
				border-collapse: collapse;
				width: 100%;
				}

				#tabela td, #tabela th {
				border: 1px solid #ddd;
				padding: 8px;
				}

				#tabela tr:nth-child(even){background-color: #f2f2f2;}

				#tabela tr:hover {background-color: #ddd;}

				#tabela th {
				padding-top: 12px;
				padding-bottom: 12px;
				text-align: left;
				
				}

				@page {
					size: auto;   /* auto is the initial value */
    				margin: 0;  /* this affects the margin in the printer settings */
				}
				</style>
			</head>
			<body>';
			foreach ($pedidosEntregues as $item_orcamento) {
				if ($CodigoPedido != $item_orcamento->Cd_Orcamento) {

					if ($aux > 1) {
						$html_relatorio .= '</tbody>
																</table></div>' . chr(13) . chr(10);
						$html_relatorio .= '<hr style="border-top:2pt dashed black !important; margin-top:5px;margin-bottom:5px;"><div style="page-break-inside: avoid"> </div>' . chr(13) . chr(10);
					}
				}

				if ($CodigoCliente == 0 || $CodigoCliente != $item_orcamento->CodigoCliente) {
					$nome_cliente = 'Cliente: ' . $item_orcamento->RazaoSocial;                                        
				}

				if ($CodigoPedido != $item_orcamento->Cd_Orcamento) {
					$numero_pedido = sprintf('%s', 'Pedido: #' . $item_orcamento->Cd_Orcamento);
				}

				if ($CodigoPedido != $item_orcamento->Cd_Orcamento) {

					$data_prevista_entrega = $item_orcamento->Dt_Prevista_Entrega != '' ? date('d/m/Y', strtotime($item_orcamento->Dt_Prevista_Entrega)) : '<i>Não informado</i>';

					$data_prevista = sprintf('%s', 'Data Prevista: ' . $data_prevista_entrega);

					$html_relatorio .= '
									<table id="tabela">
									<thead>
										<tr role="row">
											<td width="90%">' . $nome_cliente . ' - ' . $numero_pedido . ' - ' . $data_prevista . '</td>
										</tr>
										<tr>
											<td> <strong> Empresa: </strong> ' . $item_orcamento->nome_empresa . '
											<strong> Vendedor: </strong> ' . $item_orcamento->NomeUsuarioCriado . '</td> 
										</tr>
									</thead>
									</table>
									<table id="tabela">
										<thead>
										<tr role="row">
											<th align="center">Item</th>
											<th>Produto</th>
											<th>Alt</th>
											<th>Lg</th>
											<th>Qtd</th>
											<th>Molduras/Componentes</th>
											<th>Observações do item</th>
										</tr>
										</thead>
									
									<tbody>';
				}


				$html_relatorio .= '<tr>';

				$html_relatorio .= sprintf('<td>%s</td>', $item_orcamento->Cd_Item_Orcamento) . chr(13);
				$html_relatorio .= sprintf('<td>%s</td>', $item_orcamento->Descricao) . chr(13);
				$html_relatorio .= sprintf('<td>%s</td>', $item_orcamento->Md_Altura) . chr(13);
				$html_relatorio .= sprintf('<td>%s</td>', $item_orcamento->Md_Largura) . chr(13);
				$html_relatorio .= sprintf('<td>%s</td>', $item_orcamento->Qt_Item) . chr(13);

				$html_relatorio .= '<td width="350" style="width:450px;text-align:left;">';

				$handle = $item_orcamento->Cd_Orcamento;
				$moldurasItemOrcamento = $MolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
				// if (is_array($moldurasItemOrcamento) && sizeof($arrMoldurasItemOrcamento) > 0) {
				foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
					$exibeComplexidade = '';
					if ($moldura_item_orcamento->codigoComplexidade > 0) {
						$exibeComplexidade = sprintf(' (%s)', $moldura_item_orcamento->nomeComplexidade);
					}
					$codigo_produto = $moldura_item_orcamento->CodigoProdutoFabricante != null ? $moldura_item_orcamento->CodigoProdutoFabricante : $moldura_item_orcamento->NovoCodigo;
					$html_relatorio .= '<li>' . $codigo_produto . '-' . $moldura_item_orcamento->DescricaoProduto . $exibeComplexidade . '</li> ';
				}
				// }


				$componentesItemOrcamento = $ComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
				// if (is_array($componentesItemOrcamento) && sizeof($componentesItemOrcamento) > 0) {
				foreach ($componentesItemOrcamento as $componente_item_orcamento) {
					$html_relatorio .= $componente_item_orcamento->DescricaoProduto . ', ';
				}
				// }


				$html_relatorio .= sprintf('<td style="">%s</td>', $item_orcamento->Ds_ObservacaoProducao) . chr(13);

				$html_relatorio .= '</td>';

				$html_relatorio .= '</tr>';

				if ($item_orcamento->Ds_Observacao_Producao !== '' && $CodigoPedido != $item_orcamento->Cd_Orcamento) {
					$html_relatorio .= sprintf('<tr><td colspan="7"><strong>Observações do pedido:</strong>%s</td></tr>', $item_orcamento->Ds_Observacao_Producao) . chr(13);
				}


				// if ($item_orcamento->Ds_Observacao_Producao !== '' && $CodigoPedido != $item_orcamento->Cd_Orcamento) {
				//     $html_relatorio .= sprintf('<tr><td colspan="7"><strong>Observações do pedido:</strong>%s</td></tr>', $item_orcamento->Ds_Observacao_Producao).chr(13);
				// }

				$total_bruto_cliente += (float) $item_orcamento->Vl_Bruto;
				$total_desconto_cliente += $item_orcamento->Vl_Desconto;
				$total_liquido_cliente += $item_orcamento->Vl_Bruto - $item_orcamento->Vl_Desconto;

				$CodigoCliente = $item_orcamento->CodigoCliente;
				$CodigoPedido = $item_orcamento->Cd_Orcamento;

				if ($aux == $quantidade) {
					$total_bruto_geral += $total_bruto_cliente;
					$total_desconto_geral += $total_desconto_cliente;
					$total_liquido_geral += $total_liquido_cliente - $total_desconto_cliente;

					$total_bruto_cliente = 0;
					$total_desconto_cliente = 0;
					$total_liquido_cliente = 0;

					$html_relatorio .= '';

					$html_relatorio .= '<hr style="border-top:2pt dashed black !important;margin-top:5px;margin-bottom:5px;"><div style="page-break-inside: avoid;">&nbsp;</div>';
				}

				$aux++;
			}

			$html_relatorio .= '
							<script>
								document.title = "Impressão Fábrica";
								window.print();
								// setTimeout("window.close()",5000)
							</script>
							</html>';

			echo $html_relatorio;			
		}

	}	

	public function impressaoFabricaPdf()
	{

		$orcamento = new Orcamento();
		$MolduraItemOrcamento = new Moldura_Item_Orcamento();
		$ComponenteItemOrcamento = new Componente_Item_Orcamento();

		$periodo_inicial = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $_POST['periodo_inicial'])));
		$periodo_final 	 =  date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $_POST['periodo_final'])));

		$html_relatorio = '';

		$pedidosEntregues = $orcamento->listarOrcamentoPorDataPrevista($periodo_inicial, $periodo_final);
		$arrMoldurasItemOrcamento = array();
		$arrComponentesItemOrcamento = array();
		$CodigoCliente = 0;
		$CodigoPedido = 0;
		$total = 0;
		$total_bruto_cliente = 0;
		$total_desconto_cliente = 0;
		$total_liquido_cliente = 0;

		$total_bruto_geral = 0;
		$total_desconto_geral = 0;
		$total_liquido_geral = 0;
		$aux = 1;
		$quantidade = count($pedidosEntregues);

		foreach ($pedidosEntregues as $item_orcamento) {
			if ($CodigoPedido != $item_orcamento->Cd_Orcamento) {

				if ($aux > 1) {
					$html_relatorio .= '</tbody>
											</table></div>' . chr(13) . chr(10);
					$html_relatorio .= '<hr><br><div style="page-break-inside: avoid"> </div>' . chr(13) . chr(10);
				}
			}

			if ($CodigoCliente == 0 || $CodigoCliente != $item_orcamento->CodigoCliente) {
				$nome_cliente = 'Cliente: ' . $item_orcamento->RazaoSocial;
			}

			if ($CodigoCliente != $item_orcamento->CodigoCliente) {
				$numero_pedido = sprintf('%s', 'Pedido: #' . $item_orcamento->Cd_Orcamento);
			}

			if ($CodigoPedido != $item_orcamento->Cd_Orcamento) {

				$data_prevista = sprintf('%s', 'Data Prevista: ' . date('d/m/Y', strtotime($item_orcamento->Dt_Prevista_Entrega)));

				$html_relatorio .= '<div style="page-break-inside: avoid">
				<table>
				<thead>
					<tr role="row">
					<td width="90%">' . $nome_cliente . '<br>'
					. $numero_pedido . '<br>'
					. $data_prevista . '</td>
					</tr>
				</thead>
				</table>
				<table>
					<thead>
					<tr role="row">
						<th width="30" style="width:30px;text-align:center;" align="center">Item</th>
						<th width="100" style="width:100px;text-align:left;">Produto</th>
						<th width="30" style="width:30px;text-align:center;">Alt</th>
						<th width="30" style="width:30px;text-align:center;">Lg</th>
						<th width="30" style="width:30px;text-align:center;">Qtd</th>
						<th width="150" style="width:350px;text-align:left;">Molduras</th>
					</tr>
					</thead>
				</table>
				<table>
				<tbody>';
			}


			$html_relatorio .= '<tr>';

			$html_relatorio .= sprintf('<td width="30" style="width:30px;text-align:center;">%s</td>', $item_orcamento->Cd_Item_Orcamento) . chr(13);
			$html_relatorio .= sprintf('<td width="100" style="width:100px;text-align:left;">%s</td>', $item_orcamento->Cd_Prod_Aux) . chr(13);
			$html_relatorio .= sprintf('<td width="30" style="width:30px;text-align:center;">%s</td>', $item_orcamento->Md_Altura) . chr(13);
			$html_relatorio .= sprintf('<td width="30" style="width:30px;text-align:center;">%s</td>', $item_orcamento->Md_Largura) . chr(13);
			$html_relatorio .= sprintf('<td width="30" style="width:30px;text-align:center;">%s</td>', $item_orcamento->Qt_Item) . chr(13);

			$html_relatorio .= '<td width="150" style="width:350px;text-align:left;">';

			$handle = $item_orcamento->Cd_Orcamento;
			$moldurasItemOrcamento = $MolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
			// if (is_array($moldurasItemOrcamento) && sizeof($arrMoldurasItemOrcamento) > 0) {
			foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
				$html_relatorio .= $moldura_item_orcamento->NovoCodigo . ', ';
			}
			// }


			$componentesItemOrcamento = $ComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
			// if (is_array($componentesItemOrcamento) && sizeof($componentesItemOrcamento) > 0) {
			foreach ($componentesItemOrcamento as $componente_item_orcamento) {
				$html_relatorio .= $componente_item_orcamento->DescricaoProduto . ', ';
			}
			// }

			$html_relatorio .= '</td> </tr>';

			$total_bruto_cliente += (float) $item_orcamento->Vl_Bruto;
			$total_desconto_cliente += $item_orcamento->Vl_Desconto;
			$total_liquido_cliente += $item_orcamento->Vl_Bruto - $item_orcamento->Vl_Desconto;

			$CodigoCliente = $item_orcamento->CodigoCliente;
			$CodigoPedido = $item_orcamento->Cd_Orcamento;

			if ($aux == $quantidade) {
				$total_bruto_geral += $total_bruto_cliente;
				$total_desconto_geral += $total_desconto_cliente;
				$total_liquido_geral += $total_liquido_cliente - $total_desconto_cliente;

				$total_bruto_cliente = 0;
				$total_desconto_cliente = 0;
				$total_liquido_cliente = 0;

				$html_relatorio .= '</tbody>
					</table>';

				$html_relatorio .= '<hr><br><div style="page-break-inside: avoid;">&nbsp;</div>';
			}

			$aux++;
		}

		$html_relatorio = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
							<html>
								<head>
									<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">									
								</head>
							<body>
						  ' . $html_relatorio . '
							</body>
						  </html>';

		file_put_contents('relatorio.html', $html_relatorio);
		// echo $html_relatorio;exit;

		// instantiate and use the dompdf class
		$dompdf = new Dompdf();
		$dompdf->loadHtml($html_relatorio);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		$output = $dompdf->output();
		$nome_arquivo = 'relatorio-' . time() . '.pdf';
		file_put_contents('./pdfTemporario/' . $nome_arquivo, $output);
		echo json_encode(
			array(
				'success' => true,
				'arquivo' => $nome_arquivo,
				'msg' => ''
			)
		);
		return true;
	}
}
