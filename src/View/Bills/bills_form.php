<?php include_once(DOCUMENT_ROOT . "src/View/Common/cabecalho.php"); ?>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <form method="POST" action="<?php echo URL . $classe . "/" . $metodo . "/" . ($Bills->BillsId ?: 0); ?>">
          <?php if (!empty($msg_sucesso)) { ?>
            <div class="callout callout-success">
              <h4>Sucesso!</h4>
              <p><?php echo $msg_sucesso; ?></p>
            </div>
          <?php } ?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group col-xs-12 col-md-2">
              <label>Data Lançamento</label>
              <?php $dataLancamento = $Bills->Date != '' ? date('d/m/Y', strtotime($Bills->Date)) : ''; ?>
              <div class="input-group date">
                <input class="form-control data" type="text" name="Date" value="<?php echo $dataLancamento; ?>">
              </div>
            </div>
            <div class="form-group col-xs-12 col-md-2">
              <label>Data Vencimento</label>
              <?php $dataVencimento = $Bills->cdtvencimento != '' ? date('d/m/Y', strtotime($Bills->cdtvencimento)) : ''; ?>
              <div class="input-group date">
                <input class="form-control data" type="text" name="cdtvencimento" value="<?php echo $dataVencimento; ?>">
              </div>
            </div>
            <div class="form-group col-xs-12 col-md-2">
              <label>Data Pagamento</label>
              <?php $dataPagamento = $Bills->cdtpagamento != '' ? date('d/m/Y', strtotime($Bills->cdtpagamento)) : ''; ?>
              <div class="input-group date">
                <input class="form-control data" type="text" name="cdtpagamento" value="<?php echo $dataPagamento; ?>">
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group col-xs-12 col-md-2">
              <label>Categoria</label>
              <div class="input-group">
                <select class="form-control" name="CategoryId">
                  <?php
                  foreach ($this->categoriasContasPagar as $categoria) {
                    echo sprintf('<option %s value="%s">%s</option>', $categoria->CategoryId == $Bills->CategoryId ? 'selected' : '',  $categoria->CategoryId, $categoria->CategoryName);
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group col-xs-12 col-md-2">
              <label>Fornecedor</label>
              <div class="input-group">
                <select class="form-control" name="Title">
                  <?php
                  foreach ($this->fornecedores as $fornecedor) {
                    echo sprintf('<option %s value="%s">%s</option>', $fornecedor->CodigoFornecedor == $Bills->Title ? 'selected' : '', $fornecedor->CodigoFornecedor, $fornecedor->RazaoSocial);
                  }
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-2">
              <label>Empresa</label>
              <select class="form-control" name="UserId">
                <?php
                foreach ($this->empresas as $empresa) {
                  echo sprintf('<option %s value="%s">%s</option>', $empresa->CodigoEmpresa == $Bills->UserId ? 'selected' : '', $empresa->CodigoEmpresa, $empresa->RazaoSocial);
                }
                ?>
              </select>
            </div>
          </div>

          <div class="box-body">
            <div class="form-group col-xs-12 col-md-2">
              <label>Tipo de documento</label>
              <div class="input-group">
                <select class="form-control" name="doc">
                  <option value="bol">Boleto</option>
                  <option value="cf">Cupom Fiscal</option>
                  <option value="dar">Darf</option>
                  <option value="fat">Fatura</option>
                  <option value="gps">GPS</option>
                  <option value="nf">Nota Fiscal</option>
                  <option value="rec">Recibo</option>
                </select>
              </div>
            </div>
            <div class="form-group col-xs-12 col-md-2">
              <label>Nº do documento</label>
              <div class="input-group">
                <input class="form-control" type="text" name="ndoc" value="<?php echo $Bills->ndoc; ?>">
              </div>
            </div>
            <div class="form-group col-xs-12 col-md-2">
              <label>Valor</label>
              <div class="input-group">
                <span class="input-group-addon">R$</span>
                <input class="form-control campoMonetario" type="text" name="Amount" value="<?php echo number_format($Bills->Amount, 2, ',', '.'); ?>">
              </div>
            </div>
            <div class="form-group col-xs-12 col-md-2">
              <label>Conta</label>
              <select class="form-control" name="AccountId">
                <?php
                foreach ($this->contas as $conta) {
                  echo sprintf('<option %s value="%s">%s</option>', $conta->AccountId == $Bills->AccountId ? 'selected' : '', $conta->AccountId, $conta->AccountName);
                }
                ?>
              </select>
            </div>
          </div>

          <?php if ($Bills->BillsId == 0) { ?>
            <div class="box-body">
              <div class="form-group col-xs-12 col-md-2">
                <label>Ocorrência (Ùnica ou Recorrente)</label>
                <div class="input-group">
                  <select class="form-control" id="tipoRecebimento" name="tipoRecebimento">
                    <option value="U" selected="">Única</option>
                    <option value="W">Semanal</option>
                    <option value="M">Mensal</option>
                    <option value="T">Trimestral</option>
                    <option value="S">Semestral</option>
                    <option value="A">Anual</option>
                    <option value="P">Parcelada</option>
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-2" id="divDiaVencimentoSemana" style="display:none;">
                <label>Dia do vencimento na semana</label>
                <div class="input-group">
                  <select name="diaVencimentoSemana" id="diaVencimentoSemana" class="form-control">
                    <option value="">Selecione</option>
                    <option value="0">Domingo</option>
                    <option value="1">Segunda-feira</option>
                    <option value="2">Terça-feira</option>
                    <option value="3">Quarta-feira</option>
                    <option value="4">Quinta-feira</option>
                    <option value="5">Sexta-feira</option>
                    <option value="6">Sábado</option>
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-2" id="divDiaVencimento" style="display:none;">
                <label>Dia do vencimento</label>
                <div class="input-group">
                  <input class="form-control" type="text" id="diaVencimento" name="diaVencimento" value="">
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-2" id="divQuantidadeRepeticoes" style="display:none;">
                <label>Quantidade de repetições</label>
                <div class="input-group">
                  <input class="form-control" type="text" id="quantidadeRepeticao" name="quantidadeRepeticao" value="">
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-2" id="divQuantidadeParcelas" style="display:none;">
                <label>Parcelas</label>
                <div class="input-group">
                  <input class="form-control" type="text" id="quantidadeParcelas" name="quantidadeParcelas" value="">
                </div>
              </div>
            </div>
          <?php } else { ?>
            <input type="hidden" name="tipoRecebimento" value="U">
          <?php } ?>

          <div class="box-body">
            <div class="form-group col-xs-12 col-md-2">
              <label>Observação</label>
              <textarea class="form-control" name="description"><?php echo $Bills->Description; ?></textarea>
            </div>
          </div>
          <div class="box-footer">
            <input type="hidden" name="handle" value="<?php echo $Bills->BillsId; ?>">
            <button class="btn btn-primary" type="submit">Salvar</button>
            <a class="btn btn-primary" href="<?php echo URL . $classe . '/Listar/'; ?>">Voltar</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
</div>
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>