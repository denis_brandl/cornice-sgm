<?php include_once(DOCUMENT_ROOT."src/View/Common/cabecalho.php"); ?>  
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">					
						<h3 class="box-title">
							Filtros
						</h3>
					</div>          
					<?php if (isset($msg_sucesso) && !empty($msg_sucesso)) { ?>
							<div class="callout <?= $tipo_mensagem;?>">
								<p><?= $msg_sucesso; ?></p>
							</div>
					<?php } ?>
					<div class="box-body">					
            <form id="frmFiltroFinanceiroPagamento" method="POST" action="<?php echo URL.$classe."/".$metodo."/";?>">
            <div class="row">
                  <div class="form-group col-md-3 col-sm-12">
                    <label for="CategoryId">Categoria</label>
                      <select class="form-control" id="CategoryId" name="CategoryId">
                        <option value="">Todos</option>
                        <?php
                          foreach ($this->categoriasContasPagar as $categoria) {
                            echo sprintf('<option %s value="%s">%s</option>',$categoria->CategoryId == $this->objCommon->validatePost('CategoryId') ? 'selected' : '',  $categoria->CategoryId, $categoria->CategoryName);
                          }
                        ?>
                      </select>
                  </div>
                  <div class="form-group col-md-3 col-sm-12">
                    <label for="Title">Fornecedor</label>
                      <select class="form-control" id="Title" name="Title">
                        <option value="">Todos</option>
                        <?php
                          foreach ($this->fornecedores as $fornecedor) {
                            echo sprintf('<option %s value="%s">%s</option>', $fornecedor->CodigoFornecedor == $this->objCommon->validatePost('Title') ? 'selected' : '' ,$fornecedor->CodigoFornecedor, $fornecedor->RazaoSocial);
                          }
                        ?>
                      </select>
                  </div>
                  <div class="form-group col-md-3 col-sm-12">
                    <label for="UserId">Empresa</label>
                    <select class="form-control" name="UserId">
                        <option value="">Todos</option>
                        <?php 
                          foreach ($this->empresas as $empresa) {
                            echo sprintf('<option %s value="%s">%s</option>', $empresa->CodigoEmpresa == $this->objCommon->validatePost('UserId') ? 'selected' : '' ,$empresa->CodigoEmpresa, $empresa->RazaoSocial);
                          }
                        ?>
                      </select> 
                  </div>
                  <div class="form-group col-md-3 col-sm-12">
                    <label for="AccountId">Conta</label>
                    <select class="form-control" name="AccountId">
                      <option value="">Todos</option>
                      <?php 
                        foreach ($this->contas as $conta) {
                          echo sprintf('<option %s value="%s">%s</option>', $conta->AccountId == $this->objCommon->validatePost('AccountId') ? 'selected' : '' ,$conta->AccountId, $conta->AccountName);
                        }
                      ?>
                    </select> 
                  </div>                  
            </div>
            <div class="row">
              <div class="form-group col-md-3 col-sm-12">
                <label>Data Lançamento</label>
                  <div class="input-daterange" id="datepicker">
                    <div class="input-group">
                      <span class="input-group-addon">De</span>
                      <input type="text" class="data form-control" value="<?php echo $this->objCommon->validatePost('filtroDataLancamentoInicio');?>" name="filtroDataLancamentoInicio" />
                      <span class="input-group-addon">á</span>
                      <input type="text" class="data form-control" value="<?php echo $this->objCommon->validatePost('filtroDataLancamentoFim');?>" name="filtroDataLancamentoFim" />
                    </div>
                  </div>
              </div>              
              <div class="form-group col-md-3 col-sm-12">
                <label>Data de vencimento</label>
                  <div class="input-daterange" id="datepicker">
                    <div class="input-group">
                      <span class="input-group-addon">De</span>
                      <input type="text" class="data form-control" value="<?php echo $this->objCommon->validatePost('filtroDataVencimentoInicio');?>" name="filtroDataVencimentoInicio" />
                      <span class="input-group-addon">á</span>
                      <input type="text" class="data form-control" value="<?php echo $this->objCommon->validatePost('filtroDataVencimentoFim');?>" name="filtroDataVencimentoFim" />
                    </div>
                  </div>
              </div>
              <div class="form-group col-md-3 col-sm-12">
                <label>Data de pagamento</label>
                  <div class="input-daterange" id="datepicker">
                    <div class="input-group">
                      <span class="input-group-addon">De</span>
                      <input type="text" class="data form-control" value="<?php echo $this->objCommon->validatePost('filtroDataPagamentoInicio');?>" name="filtroDataPagamentoInicio" />
                      <span class="input-group-addon">á</span>
                      <input type="text" class="data form-control" value="<?php echo $this->objCommon->validatePost('filtroDataPagamentoFim');?>" name="filtroDataPagamentoFim" />
                    </div>
                  </div>
              </div>              
              <div class="form-group col-md-3 col-sm-12">
                <label for="AccountId">Nº do documento</label>
                <div class="input-group">
                  <input class="form-control" type="text" name="ndoc" value="<?php echo $this->objCommon->validatePost('ndoc');?>">
                </div>                
              </div>
              <div class="form-group col-md-6 col-sm-12">
                <button class="btn btn-primary" type="submit">Aplicar Filtro</button>
                <button class="btn btn-primary" id="limparFiltro" type="button">Limpar Filtros</button>
              </div>
              <div class="form-group col-md-6 col-sm-12">
                <a class="btn btn-primary pull-right" href="<?php echo URL.$classe.'/cadastrar/';?>">Cadastrar Novo</a>
              </div>

              
            </div>
          </form>
					</div>          
					<div class="box-body">					
						<div id="componentes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
							<div class="row">
								<div class="col-sm-12">
              <table class="table table-bordered table-striped dataTable">
                <thead>
                  <tr role="row">
                    <th>Data de lançamento</th>
                    <th>Tipo de documento</th>
                    <th>Nº do documento</th>
                    <th>Categoria</th>
                    <th>Fornecedor</th>
                    <th>Empresa</th>
                    <th>Valor</th>
                    <th>Conta</th>
                    <th>Observação</th>
                    <th>Data de vencimento</th>
                    <th>Data de pagamento</th>
                    <th>Situação</th>
                    <th>Ação</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $aux = 0;
                  foreach ($arrBills as $Bills) {
                    if ($aux & 1) {
                  ?>
                      <tr class="odd" role="row">
                  <?php } else { ?>
                      <tr class="even" role="row">
                  <?php } ?>
                  <td><?php echo $Bills->Date != '' ? date('d/m/Y',strtotime($Bills->Date)) : ''; ?></td>
                  <td>
                    <?php
                      $tipo_documento = '';
                      switch ($Bills->doc) {
                        case 'bol':
                        default:
                          $tipo_documento = 'Boleto';
                          break;
                        case 'cf':
                          $tipo_documento = 'Cupom Fiscal';
                          break;
                        case 'dar':
                          $tipo_documento = 'Darf';
                          break;
                        case 'fat':
                          $tipo_documento = 'Fatura';
                          break;
                        case 'gps':
                          $tipo_documento = 'GPS';
                          break;
                        case 'nf':
                          $tipo_documento = 'Nota Fiscal';
                          break;
                        case 'rec':
                          $tipo_documento = 'Recibo';
                          break;                                                                              
                      }
                      echo $tipo_documento;
                    ?>
                  </td>
                  <td><?php echo $Bills->ndoc; ?></td>
                  <td><?php echo $Bills->CategoryName; ?></td>
                  <td><?php echo $Bills->RazaoSocial; ?></td>
                  <td><?php echo $Bills->Empresa; ?></td>
                  <td>R$ <?php echo number_format($Bills->Amount, 2, ',', '.'); ?></td>
                  <td><?php echo $Bills->nomeConta;?></td>
                  <td><?php echo $Bills->Description; ?></td>
                  <td><?php echo $Bills->cdtvencimento ? date('d/m/Y',strtotime($Bills->cdtvencimento)) : ''; ?></td>
                  <td><?php echo $Bills->cdtpagamento ? date('d/m/Y',strtotime($Bills->cdtpagamento)) : ''; ?></td>
                  <td style="background-color: <?php echo $Bills->situacao == 'VENCIDO' ? 'rgb(247, 192, 192)' : '';?>;"><?php echo $Bills->situacao; ?></td>
                  <td><a class="" href="<?php echo URL.$classe.'/editar/'. $Bills->BillsId; ?>"><i class="fa fa-edit"></i>Editar</a></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
            								</div>								
							</div>
						</div>
					</div>			
				</div>				
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT."src/View/Common/rodape.php"); ?>  
