<?php include_once(DOCUMENT_ROOT . "src/View/Common/cabecalho.php"); ?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                </div>
                <div class="box-body">
                    <div id="forma_pagamento-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6">
                                <a class="btn btn-primary" href="<?php echo URL . $classe . '/cadastrar/'; ?>">Cadastrar Novo</a>
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-striped dataTable">
                                    <thead>
                                        <tr role="row">
                                            <th>Descrição</th>
                                            <th>Máximo de parcelas</th>
                                            <th>Situação</th>
                                            <th>Editar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $aux = 0;
                                        foreach ($arrFormaPagamento as $forma_pagamento) {
                                            if ($aux & 1) {
                                        ?>
                                                <tr class="odd" role="row">
                                                <?php } else { ?>
                                                <tr class="even" role="row">
                                                <?php } ?>
                                                <td><?php echo $forma_pagamento->descricao; ?></td>
                                                <td><?php echo $forma_pagamento->parcelas; ?></td>
                                                <td><?php echo $forma_pagamento->situacao == '1' ? 'Ativo' : 'Inativo'; ?></td>                                                
                                                <td><a class="" href="<?php echo URL . $classe . '/' . $acao . '/' . $forma_pagamento->id_forma_pagamento; ?>"><i class="fa fa-edit"></i>Editar</a></td>
                                                </tr>
                                            <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<?php include_once(DOCUMENT_ROOT . "src/View/Common/rodape.php"); ?>