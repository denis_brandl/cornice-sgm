<?php include_once(DOCUMENT_ROOT . "src/View/Common/cabecalho.php"); ?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form method="POST" action="<?php echo URL . $classe . "/" . $metodo . "/" . ($FormaPagamento->id_forma_pagamento ?: 0); ?>">
                    <?php if (!empty($msg_sucesso)) { ?>
                        <div class="callout callout-success">
                            <h4>Sucesso!</h4>
                            <p><?php echo $msg_sucesso; ?></p>
                        </div>
                    <?php } ?>
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Cadastro da sua FormaPagamento</h3>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group col-xs-12 col-md-2">
                            <label>Descrição</label>
                            <input class="form-control" type="text" name="descricao" value="<?php echo $FormaPagamento->descricao; ?>">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group col-xs-12 col-md-2">
                            <label>Máximo de parcelas</label>
                            <input class="form-control" type="text" name="parcelas" value="<?php echo $FormaPagamento->parcelas; ?>">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group col-xs-12 col-md-2">
                            <label>Situação</label>
                            <select name="Situacao" class="form-control">
                                <option value="0" <?php echo $FormaPagamento->situacao == 0 ? 'selected="selected"' : ''; ?>>Inativo</option>
                                <option value="1" <?php echo $FormaPagamento->situacao == 1 ? 'selected="selected"' : ''; ?>>Ativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="hidden" name="handle" value="<?php echo $FormaPagamento->id_forma_pagamento; ?>">
                        <button class="btn btn-primary" type="submit">Salvar</button>
                        <a class="btn btn-primary" href="<?php echo URL . $classe . '/Listar/'; ?>">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
</div>
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>