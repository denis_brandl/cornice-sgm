<?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  


    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">					
						<h3 class="box-title"></h3>					
					</div>
					<div class="box-body">					
						<div id="componentes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
						<?php if ($this->usuario_master) { ?>
							<div class="row">
								<div class="col-sm-6">
									<a class="btn btn-primary" href="<?php echo URL.$classe.'/cadastrar/';?>">Cadastrar Novo</a>
								</div>
								<div class="col-sm-6">
								</div>
							</div>
						<?php } ?>
							<div class="row">
								<div class="col-sm-12">
									<table class="table table-bordered table-striped dataTable">
										<thead>
											<tr role="row">
                        <th>CNPJ</th>
												<th>Razao Social</th>
                        <th>Nome Fantasia</th>
                        <th>Padrão</th>
                        <th>Matriz ou Filial</th>
												<th>Ações</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												$aux = 0;
												foreach ($arrEmpresas as $empresa) {
												if ($aux & 1) {
											?>
												<tr class="odd" role="row">
											<?php } else { ?>	
												<tr class="even" role="row">
											<?php } ?>	
                        <td><?php echo $empresa->CGC; ?></td>
												<td><?php echo $empresa->RazaoSocial; ?></td>
                        <td><?php echo $empresa->NomeFantasia; ?></td>
                        <td><?php echo $empresa->padrao == '1' ? 'Sim' : 'Não'; ?></td>
                        <td><?php echo $empresa->filial == '1' ? 'Matriz' : 'Filial'; ?></td>
												<td><a class="" href="<?php echo URL.$classe.'/'.$acao.'/'. $empresa->CodigoEmpresa; ?>"><i class="fa fa-edit"></i>Editar</a></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>									
								</div>								
							</div>
						</div>
					</div>					
				</div>				
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  