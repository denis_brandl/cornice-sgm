<?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<form autocomplete="off"  role="form" method="POST" action="<?php echo URL.$classe.'/'.$metodo.'/'.($empresa->CodigoEmpresa ?: 0);?>">
						<?php if (!empty($msg_sucesso)) { ?>
							<div class="callout callout-success">
								<h4>Sucesso!</h4>
								<p><?php echo $msg_sucesso;?></p>
							</div>
						<?php } ?>
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Cadastro da sua empresa</h3>
							</div>						
						</div>
						<div class="box-body">
							<div class="form-group col-xs-12 col-md-2">
                <label>CNPJ</label>
                <input class="form-control" type="text" name="CGC" value="<?php echo $empresa->CGC;?>">
							</div>

							<div class="form-group col-xs-12 col-md-3">
                <label>Razão Social</label>
                <input class="form-control" type="text" value="<?php echo isset($empresa->RazaoSocial) ? $empresa->RazaoSocial : '' ;?>" name="RazaoSocial" />
							</div>
							
							<div class="form-group col-xs-12 col-md-3">
                <label>Nome Fantasia</label>
                <input class="form-control" type="text" name="NomeFantasia" value="<?php echo $empresa->NomeFantasia;?>">
							</div>

							<div class="form-group col-xs-12 col-md-2">
                <label>Inscrição Estadual</label>
                <input class="form-control" type="text" name="InscricaoEstadual" value="<?php echo $empresa->InscricaoEstadual;?>">
							</div>

              <div class="form-group col-xs-12 col-md-2">
                <label>Código de Regime Tributário</label>
                <select name="codigo_crt" class="form-control">
                  <option value="0" <?php echo $empresa->codigo_crt == '0' ? 'selected': '';?>>Nenhum</option>
                  <option value="1" <?php echo $empresa->codigo_crt == '1' ? 'selected': '';?>>Simples Nacional</option>
                  <option value="2" <?php echo $empresa->codigo_crt == '2' ? 'selected': '';?>>Simples Nacional - Excesso</option>
                  <option value="3" <?php echo $empresa->codigo_crt == '3' ? 'selected': '';?>>Regime Normal - Lucro Presumido</option>
                  <option value="4" <?php echo $empresa->codigo_crt == '4' ? 'selected': '';?>>Normal - Lucro Real</option>
                </select>
							</div>

            </div>

            <div class="box-body">
							<div class="form-group col-xs-12 col-md-4">
                <label>Telefone Principal</label>
                <input class="form-control" type="text" name="Telefone1" value="<?php echo $empresa->Telefone1;?>">
							</div>
							
							<div class="form-group col-xs-12 col-md-4">
									<label>Telefone Alternativo</label>
									<input class="form-control" type="text" name="Telefone2" value="<?php echo $empresa->Telefone2;?>">
							</div>
							
							<div class="form-group col-xs-12 col-md-4">
                  <label>Telefone Alternativo 2</label>
                  <input class="form-control" type="text" name="Fax" value="<?php echo $empresa->Fax;?>">
							</div>

            </div>

            <div class="box-body">
							<div class="form-group col-xs-12 col-md-4">
                <label>CEP</label>
                <input class="form-control" type="text" name="CEP" value="<?php echo $empresa->CEP;?>">
							</div>

							<div class="form-group col-xs-12 col-md-4">
                <label>Endereco</label>
                <input class="form-control" type="text" name="Endereco" value="<?php echo $empresa->Endereco;?>">
							</div>
							
							
							<div class="form-group col-xs-12 col-md-4">
                <label>Complemento</label>
                <input class="form-control" type="text" name="Complemento" value="<?php echo $empresa->Complemento;?>">
							</div>
							
							<div class="form-group col-xs-12 col-md-4">
                <label>Bairro</label>
                <input class="form-control" type="text" name="Bairro" value="<?php echo $empresa->Bairro;?>">
							</div>
							
							<div class="form-group col-xs-12 col-md-4">
                <label>Cidade</label>
                <input class="form-control" type="text" name="Cidade" value="<?php echo $empresa->Cidade;?>">
							</div>
							
							<div class="form-group col-xs-12 col-md-4">
                <label>Estado</label>
                <select name="Estado" class="form-control">
                  <option value="">Selecione</option>
                  <?php foreach ($this->estados as $estado_sigla => $estado_descricao) { 
                    $selected = "";
                    if ($estado_sigla == $empresa->Estado) {
                      $selected = ' selected="selected"';
                    }
                  ?>
                    <option value="<?php echo $estado_sigla;?>" <?php echo $selected;?>><?php echo $estado_descricao;?></option>									
                  <?php } ?>	
                </select>
							</div>
            </div>
							

            <div class="box-body">
							
							<div class="form-group col-xs-12 col-md-4">
									<label>Nome do responsável</label>
									<input class="form-control" type="text" name="Contato" value="<?php echo $empresa->Contato;?>">
							</div>
							
							<div class="form-group col-xs-12 col-md-4">
									<label>E-mail</label>
									<input class="form-control" type="email" name="EMail" value="<?php echo $empresa->EMail;?>">
							</div>
            </div>
              <div class="box-body">

              <div class="form-group col-xs-12 col-md-3">
                <label>Status</label>
                <select name="status" class="form-control">
                  <option value="1" <?php echo $empresa->status == '1' ? 'selected': '';?>>Ativo</option>
                  <option value="0" <?php echo $empresa->status == '0' ? 'selected': '';?>>Inativo</option>
                </select>
              </div>

              <div class="form-group col-xs-12 col-md-3">
                <label>Empresa Padrão?</label>
                <select id="padrao" name="padrao" class="form-control">
                  <option value="1" <?php echo $empresa->padrao == '1' ? 'selected': '';?>>Sim</option>
                  <option value="0" <?php echo $empresa->padrao == '0' ? 'selected': '';?>>Não</option>
                </select>
              </div>              

              <div class="form-group col-xs-12 col-md-1">
                <div class="input-group">
                  <label for="filial">Empresa Filial?</label>
                  <select id="filial" name="filial" class="form-control">
                    <option value="1" <?php echo $empresa->filial == '1' ? 'selected': '';?>>Sim</option>
                    <option value="0" <?php echo $empresa->filial == '0' ? 'selected': '';?>>Não</option>
                  </select>
                </div>              
              </div>

              <div class="form-group col-xs-12 col-md-3 empresa-matriz <?php echo $empresa->filial == '0' ? 'hide': 'show';?>">
                <div class="input-group">  
                  <label> Qual empresa matriz? </label>
                  <select name="matriz" class="form-control">
                    <option value="0">Selecione</option>
                    <?php foreach ($arrDemaisEmpresas as $empresa_matriz) {
                        if ($empresa_matriz->CodigoEmpresa === $empresa->CodigoEmpresa)  {
                          continue;
                        }
                    ?>
                      <option <?php echo $empresa->matriz == $empresa_matriz->CodigoEmpresa ? 'selected' : '';?> value="<?php echo $empresa_matriz->CodigoEmpresa;?>"><?php echo $empresa_matriz->RazaoSocial;?> - <?php echo $empresa->NomeFantasia;?></option>
                    <?php } ?>
                    
                  </select>
                </div>
              </div>
							
            </div>

            <div class="box-body">
              <h4 class="box-title">Informações a serem exibidas nos pedidos: </h4>
							<div class="form-group col-xs-12 col-md-6">
                <label for="horario_funcionamento">Horário de funcionamento</label><br>
                <textarea class="form-control" placeholder="Segunda á sexta: 08:00 - 18:00 (sem fechar para o almoço). Sábado: 08:00 - 12:00" id="horario_funcionamento" name="horario_funcionamento" rows="5" cols="100" style="resize: none;"/><?php echo $empresa->horario_funcionamento;?></textarea>
							</div>

							<div class="form-group col-xs-12 col-md-6">
									<label for="observacao_pedido">Observações</label><br>
									<textarea id="observacao_pedido" placeholder="Exemplo: Pedidos não retirados em 60 dias poderão ser vendidos pela molduraria" class="form-control" name="observacao_pedido"  rows="5" cols="100" style="resize: none;"/><?php echo $empresa->observacao_pedido;?></textarea>
							</div>
            </div>

            <div class="box-body">
              
              <?php if($usuario_master) { ?>
                <div class="row">
                <div class="form-group col-xs-12 col-md-6">
                    <label for="token_integra_nota">Token Integra Nota</label><br>
                    <textarea id="token_integra_nota" placeholder="" class="form-control" name="token_integra_nota"  rows="5" cols="100" style="resize: none;"/><?php echo $empresa->token_integra_nota;?></textarea>
                </div>

                <div class="form-group col-xs-12 col-md-4">
                  <label>Ambiente para gerar nota</label>
                  <select id="ambiente_gerar_nota" name="ambiente_gerar_nota" class="form-control">
                    <option value="1" <?php echo $empresa->ambiente_gerar_nota == '1' ? 'selected': '';?>>Produção</option>
                    <option value="2" <?php echo $empresa->ambiente_gerar_nota == '2' ? 'selected': '';?>>Homologação</option>
                  </select>
                </div>
                </div>

                <div class="form-group col-xs-12 col-md-2">
									<label>Número sequência nota fiscal - NF-e</label>
									<input class="form-control" type="text" name="numero_sequencia_nf" value="<?php echo $empresa->numero_sequencia_nf;?>">
							  </div>

                <div class="form-group col-xs-12 col-md-2">
									<label>Série nota fiscal - NF-e</label>
									<input class="form-control" type="text" name="serie_nf" value="<?php echo $empresa->serie_nf;?>">
							  </div>

                <div class="form-group col-xs-12 col-md-2">
									<label>Número sequência nota serviço - NFS-e</label>
									<input class="form-control" type="text" name="numero_sequencia_nfs" value="<?php echo $empresa->numero_sequencia_nfs;?>">
							  </div>

                <div class="form-group col-xs-12 col-md-2">
									<label>Série nota serviço - NFS-e</label>
									<input class="form-control" type="text" name="serie_nfs" value="<?php echo $empresa->serie_nfs;?>">
							  </div>

                <div class="form-group col-xs-12 col-md-2">
									<label>Número sequência nota consumidor - NFC-e</label>
									<input class="form-control" type="text" name="numero_sequencia_nfc" value="<?php echo $empresa->numero_sequencia_nfc;?>">
							  </div>

                <div class="form-group col-xs-12 col-md-2">
									<label>Série nota - NFC-e</label>
									<input class="form-control" type="text" name="serie_nfc" value="<?php echo $empresa->serie_nfc;?>">
							  </div>                
                
              <?php } ?>

						</div>

						<div class="box-footer">
							<input type="hidden" name="handle" value="<?php echo $empresa->CodigoEmpresa;?>">
							<button class="btn btn-primary" type="submit">Salvar</button>
							<a class="btn btn-primary" href="<?php echo URL.$classe.'/Listar/';?>">Voltar</a>
						</div>
					</form>
				</div>
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  