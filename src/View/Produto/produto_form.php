<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">

      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="pill" href="#geral">Geral</a></li>
        <?php if ($habilita_tributacao) { ?>
          <li class=""><a data-toggle="pill" href="#informacoesTributarias">Informações Tributárias</a></li>
        <?php } ?>
        <li class=""><a data-toggle="pill" href="#complexidade">Complexidades</a></li>
        <li><a data-toggle="pill" href="#grupoCliente">Grupo de Cliente</a></lirole=>
        <li><a data-toggle="pill" href="#historicoCompras">Histórico de compras</a></li>
      </ul>

      <div class="tab-content">
        <div id="geral" class="tab-pane fade in active">
          <!-- INICIO CONTEUDO GERAL -->
          <div class="box">
            <form role="form" method="POST" action="<?php echo URL . 'Produto/' . $acao . '/' . ($produtos[0]->CodigoProduto ?: 0); ?>">
              <?php if (!empty($msg_sucesso)) { ?>
                <div class="callout callout-success">
                  <h4>Sucesso!</h4>
                  <p><?php echo $msg_sucesso; ?></p>
                </div>
              <?php } ?>
              <div class="box-body">
                <div class="form-group col-xs-12 col-md-2">
                  <div class="form-group">
                    <label>Código Interno</label>
                    <input class="form-control" <?php echo $readonly; ?> type="text" value="<?php echo $produtos[0]->NovoCodigo; ?>" name="NovoCodigo" />
                    <p class="small">Como o produto é identificado internamente</p>
                  </div>
                </div>

                <div class="form-group col-xs-12 col-md-2">
                  <label>Código do fornecedor
                  </label>
                  <input class="form-control" <?php echo $readonly; ?> type="text" name="CodigoProdutoFabricante" value="<?php echo $produtos[0]->CodigoProdutoFabricante; ?>">
                </div>

                <div class="form-group col-xs-12 col-md-2">
                  <label>Código NCM</label>
                  <input class="form-control col-md-1" <?php echo $readonly; ?> type="text" value="<?php echo $produtos[0]->codigo_ncm; ?>" name="codigo_ncm" />
                </div>

                <div class="form-group col-xs-12 col-md-3">
                  <label>Fornecedor</label>
                  <div class="input-group">
                    <select name="CodigoFornecedor" <?php echo $readonly; ?> class="form-control">
                      <option value="0">Selecione</option>
                      <?php foreach ($fornecedores as $fornecedor) {
                        $selected = "";
                        if ($fornecedor->CodigoFornecedor == $produtos[0]->CodigoFornecedor) {
                          $selected = ' selected="selected"';
                        }
                      ?>
                        <option value="<?php echo $fornecedor->CodigoFornecedor; ?>" <?php echo $selected; ?>><?php echo $fornecedor->RazaoSocial; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group col-xs-12 col-md-1">
                  <label>Situação</label>
                  <select <?php echo $readonly; ?> name="Situacao" class="form-control">
                    <option value="0" <?php echo $produtos[0]->Situacao == 0 ? 'selected="selected"' : ''; ?>>Inativo</option>
                    <option value="1" <?php echo $produtos[0]->Situacao == 1 ? 'selected="selected"' : ''; ?>>Ativo</option>
                    <option value="2" <?php echo $produtos[0]->Situacao == 2 ? 'selected="selected"' : ''; ?>>Descontinuado</option>
                  </select>
                </div>

                <div class="form-group col-xs-12 col-md-2">
                  <label>Componente/Insumo?</label>
                  <select <?php echo $readonly; ?> name="componente" class="form-control">
                    <option value="1" <?php echo $produtos[0]->componente == 1 ? 'selected="selected"' : ''; ?>>Sim</option>
                    <option value="0" <?php echo $produtos[0]->componente == 0 ? 'selected="selected"' : ''; ?>>Não</option>
                  </select>
                  <p class="small">Produtos marcados como componente/insumos são exibidos de forma separada ao criar um novo pedido</p>
                </div>

                <div class="form-group col-xs-12 col-md-12">
                  <label>Descrição</label>
                  <input <?php echo $readonly; ?> class="form-control" type="text" name="DescricaoProduto" value="<?php echo $produtos[0]->DescricaoProduto; ?>">
                </div>

              </div>

              <div class="box-body">
                <div class="form-group col-xs-2" style="display:none;">
                  <label>Grupo</label>
                  <select <?php echo $readonly; ?> name="CodigoGrupo" class="form-control">
                    <option value="0">Selecione</option>
                    <?php foreach ($grupos as $grupo) {
                      $selected = "";
                      if ($grupo->CodigoGrupo == $produtos[0]->CodigoGrupo) {
                        $selected = ' selected="selected"';
                      }
                    ?>
                      <option value="<?php echo $grupo->CodigoGrupo; ?>" <?php echo $selected; ?>><?php echo $grupo->DescricaoGrupo; ?></option>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group col-xs-2" style="display:none;">
                  <label>Situação Tributária</label>
                  <div class="input-group">
                    <input <?php echo $readonly; ?> class="form-control" type="text" name="SituacaoTributaria" value="<?php echo $produtos[0]->SituacaoTributaria; ?>">
                  </div>
                </div>

              </div>

              <div class="box-body">
                <div class="form-group col-xs-12 col-md-3">
                  <label class="labelUnidadeMedida">Unidade de medida</label>
                  <select <?php echo $readonly; ?> name="UnidadeProduto" id="UnidadeProduto" class="form-control">
                    <?php
                    if ($produtos[0]->UnidadeProduto === NULL) {
                      $produtos[0]->UnidadeProduto = 21;
                    }
                    foreach ($unidades as $unidade) {
                      if ($unidade->status == 0) {
                        continue;
                      }
                      $selected = "";
                      if ($unidade->CodigoUnidadeMedida == $produtos[0]->UnidadeProduto) {
                        $selected = 'selected="selected"';
                      }
                    ?>
                      <option data-sigla-unidade-medida="<?php echo $unidade->DescricaoUnidadeMedida; ?>" data-metragem-linear="<?php echo (int) $unidade->MetragemLinear; ?>" value="<?php echo $unidade->CodigoUnidadeMedida; ?>" <?php echo $selected; ?>><?php echo $unidade->NomeUnidadeMedida; ?></option>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group col-xs-12 col-md-3 collapse" id="displayMedidaVara">
                  <label>Medida da barra</label>
                  <br><span style="font-weight:bold;">Informe a medida da barra inteira</span>
                  <div class="input-group">
                    <input <?php echo $readonly; ?> class="form-control numberOnly campoMonetario" type="text" name="MedidaVara" value="<?php echo $produtos[0]->MedidaVara; ?>">
                    <span class="input-group-addon">CM</span>
                  </div>
                </div>

                <div class="form-group col-xs-12 col-md-3">
                  <label>Quantidade em estoque</label>
                  <div class="campoQuantidadeCm">
                    <div style="display: inline;font-weight: bold;">
                      Barras inteiras
                    </div>
                    <div style="display: inline;font-weight: bold;float:right;">
                      Total em CM
                    </div>
                  </div>
                  <div class="input-group col-md-12">
                    <input <?php echo $readonly; ?> class="form-control numberOnly" type="text" name="Quantidade" value="<?php echo $produtos[0]->Quantidade; ?>">
                    <span class="input-group-addon">UN</span>
                    <input <?php echo $readonly; ?> class="form-control numberOnly campoMonetario campoQuantidadeCm" type="text" name="QuantidadeCm" value="<?php echo number_format($produtos[0]->QuantidadeCm, 2, ",", ""); ?>">
                    <span class="input-group-addon campoQuantidadeCm">CM</span>
                  </div>
                  <?php if ($this->desabilitar_controle_estoque) { ?>
                    <p class="small alert alert-danger">Controle de estoque desabilitado!</p>
                  <?php } ?>
                </div>

                <div class="form-group col-xs-12 col-md-3">
                  <label>Quantidade Mínima</label>
                  <div class="campoQuantidadeCm">
                    <div style="display: inline;font-weight: bold;">
                      Barras inteiras
                    </div>
                    <div style="display: inline;font-weight: bold;float:right;">
                      Total em CM
                    </div>
                  </div>
                  <div class="input-group col-md-12">
                    <input <?php echo $readonly; ?> class="form-control numberOnly" type="text" name="QuantidadeMinima" value="<?php echo $produtos[0]->QuantidadeMinima; ?>">
                    <span class="input-group-addon">UN</span>
                    <input <?php echo $readonly; ?> class="form-control numberOnly campoMonetario campoQuantidadeCm" type="text" name="QuantidadeMinimaCm" value="<?php echo number_format((float) $produtos[0]->QuantidadeMinimaCm, 2, ",", ""); ?>">
                    <span class="input-group-addon campoQuantidadeCm">CM</span>
                  </div>
                  <?php if ($this->desabilitar_controle_estoque) { ?>
                    <p class="small alert alert-danger">Controle de estoque desabilitado!</p>
                  <?php } ?>
                </div>

              </div>

              <div class="box-body">
                <div class="form-group col-xs-12 col-md-3">
                  <label>Preço de Custo</label>
                  <div class="input-group">
                    <span class="input-group-addon">R$</span>
                    <input <?php echo $readonly; ?> class="form-control numberOnly campoMonetario" type="text" name="PrecoCusto" value="<?php echo number_format($produtos[0]->PrecoCusto, 2, ",", "."); ?>">
                  </div>
                </div>

                <div class="form-group col-xs-12 col-md-3">
                  <label>Fator de perda</label>
                  <div class="input-group">
                    <input <?php echo $readonly; ?> class="form-control numberOnly campoMonetario" type="text" name="Desenho" value="<?php echo number_format($produtos[0]->Desenho, 2, ",", "."); ?>">
                    <span class="input-group-addon">CM</span>
                  </div>
                </div>

                <div class="form-group col-xs-12 col-md-3" style="display:none">
                  <label>Preço de venda em Metro</label>
                  <div class="input-group">
                    <span class="input-group-addon">R$</span>
                    <input <?php echo $readonly; ?> class="form-control numberOnly campoMonetario" type="text" name="PrecoVenda" value="<?php echo number_format($produtos[0]->PrecoVenda, 2, ",", "."); ?>">
                  </div>
                </div>

                <div class="form-group col-xs-12 col-md-3">
                  <label>Formação do preço</label>
                  <select <?php echo $readonly; ?> name="definicao_preco" id="definicao_preco" class="form-control">
                    <option value="1" <?php echo $produtos[0]->definicao_preco == 1 ? 'selected="selected"' : ''; ?>>Preço Fixo</option>
                    <option value="2" <?php echo $produtos[0]->definicao_preco == 2 ? 'selected="selected"' : ''; ?>>Percentual sobre o preço de custo</option>
                  </select>
                </div>

                <div class="form-group col-xs-12 col-md-3 formacao-preco-fixo" style="display:<?php echo $produtos[0]->definicao_preco == 1 ? 'block' : 'none'; ?>">
                  <label>
                    Preço de Venda
                    <span data-toggle="tooltip" data-placement="top" data-html="true" title="Valor geral para todos os clientes. Use a aba Grupo de Cliente para definir um percentual em cima do valor de custo por grupo de cliente."><i class="fa fa-question-circle"></i></span>
                  </label>
                  <div class="input-group">
                    <span class="input-group-addon">R$</span>
                    <input <?php echo $readonly; ?> class="form-control numberOnly campoMonetario" type="text" name="PrecoVendaMaoObra" value="<?php echo number_format($produtos[0]->PrecoVendaMaoObra, 2, ",", "."); ?>">
                  </div>
                </div>

                <div class="form-group col-xs-12 col-md-3 formacao-preco-percentual" style="display:<?php echo $produtos[0]->definicao_preco == 2 ? 'block' : 'none'; ?>">
                  <label>
                    Margem bruta de venda
                    <span data-toggle="tooltip" data-placement="top" data-html="true" title="Valor geral para todos os clientes. Use a aba Grupo de Cliente para definir um percentual em cima do valor de custo por grupo de cliente."><i class="fa fa-question-circle"></i></span>
                  </label>
                  <div class="input-group">
                    <span class="input-group-addon">%</span>
                    <input <?php echo $readonly; ?> class="form-control numberOnly campoMonetario" type="text" name="margem_venda" value="<?php echo number_format((float) $produtos[0]->margem_venda, 2, ",", "."); ?>">
                  </div>
                </div>

              </div>
              <!-- FIM CONTEUDO GERAL -->
          </div>
        </div>

        <div id="informacoesTributarias" class="tab-pane fade">
          <!-- INICIO INFORMAÇÕES TRIBUTÁRIAS -->
          <div class="box">
            <div class="box-body">
              <div class="form-group col-xs-12 col-md-3">
                <div class="form-group">
                  <label>Grupo Tributário</label>
                  <select <?php echo $readonly; ?> id="id_grupo_tributario" name="id_grupo_tributario" class="form-control">
                    <?php foreach ($this->gruposTributarios as $grupo_tributario) { ?>
                      <option <?php echo $grupo_tributario->id_grupo_tributario == $produtos[0]->id_grupo_tributario ? 'selected' : ''; ?> value="<?php echo $grupo_tributario->id_grupo_tributario; ?>"><?php echo $grupo_tributario->codigo; ?> - <?php echo $grupo_tributario->nome; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-3">
                <div class="form-group">
                  <label>Código CEST (Código Especificador da Substituição Tributária)</label>
                  <input <?php echo $readonly; ?> class="form-control" type="text" value="<?php echo $produtos[0]->codigo_cest; ?>" name="codigo_cest" />
                </div>
              </div>
            </div>
          </div>
          <!-- FIM INFORMAÇÕES TRIBUTÁRIAS -->
        </div>

        <div id="complexidade" class="tab-pane fade">
          <!-- INICIO CONTEUDO COMPLEXIDADES -->
          <div class="box">
            <div class="box-body">
              <h3> Complexidades </h3>
              <p class=""> Aplique uma regra, se necessário, para cada complexidade  </p>
            </div>
            <div class="box-body">
              <?php
              if (count($complexidades) > 0) {
                foreach ($complexidades as $complexidade) { ?>
                  <div class="box-body">

                    <p style="font-weight: bold;"><?php echo $complexidade->nome; ?></p>
                    <div class="form-group col-xs-12 col-md-3">
                      <label>Aplicação</label>
                      <select name="complexidadeProduto[<?php echo $complexidade->codigoComplexidade; ?>][aplicacao]" class="form-control">
                        <option value="D" <?php echo $arrComplexidadeProduto[$complexidade->codigoComplexidade]['aplicacao'] == 'D' ? 'selected' : ''; ?>>Desconto</option>
                        <option value="A" <?php echo $arrComplexidadeProduto[$complexidade->codigoComplexidade]['aplicacao'] == 'A' ? 'selected' : ''; ?>>Acréscimo</option>
                      </select>
                    </div>

                    <div class="form-group col-xs-12 col-md-3">
                      <label>Formação do preço</label>
                      <select name="complexidadeProduto[<?php echo $complexidade->codigoComplexidade; ?>][definicao_preco]" class="form-control">
                        <option value="1" <?php echo $arrComplexidadeProduto[$complexidade->codigoComplexidade]['definicao_preco'] == '1' ? 'selected' : ''; ?>>Preço Fixo em R$ </option>
                        <option value="2" <?php echo $arrComplexidadeProduto[$complexidade->codigoComplexidade]['definicao_preco'] == '2' ? 'selected' : ''; ?>>Percentual sobre o preço de custo</option>
                      </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                      <label>Valor</label>
                      <div class="input-group">
                        <input <?php echo $readonly; ?> class="form-control numberOnly campoMonetario" type="text" name="complexidadeProduto[<?php echo $complexidade->codigoComplexidade; ?>][valor]" value="<?php echo isset($arrComplexidadeProduto[$complexidade->codigoComplexidade]) ? $arrComplexidadeProduto[$complexidade->codigoComplexidade]['valor'] : ''; ?>">
                      </div>
                    </div>
                  </div>
                <?php
                }
              } else {
                ?>
                <p class="text-danger"> Não foi encontrado nenhuma complexidade cadastrada! Cadastre uma nova em Administração > Complexidades </p>
              <?php
              }
              ?>

            </div>
          </div>
          <!-- FIM CONTEUDO COMPLEXIDADES -->
        </div>

        <div id="grupoCliente" class="tab-pane fade">
          <!-- INICIO CONTEUDO GRUPO CLIENTE -->
          <div class="box">
            <div class="box-body">
              <h3> Grupo de cliente </h3>
              <p class=""> Aplique, se necessário, uma regra para cada grupo de cliente <br> Se nenhuma regra específica for atribuido, será utilizado a regra global definida em Adminnistração -> Grupo de Cliente</p>
            </div>
            <?php foreach ($clientesGrupo as $grupo) { ?>
              <div class="box-body">

                <p style="font-weight: bold;"><?php echo $grupo->nome; ?></p>

                <div class="form-group col-xs-12 col-md-3">
                  <label>Aplicação</label>
                  <select name="clienteGrupo[<?php echo $grupo->codigoClienteGrupo; ?>][aplicacao]" class="form-control">
                    <option value="D" <?php echo isset($arrPercentualGrupo[$grupo->codigoClienteGrupo]) && $arrPercentualGrupo[$grupo->codigoClienteGrupo]['aplicacao'] == 'D' ? 'selected' : ''; ?>>Desconto</option>
                    <option value="A" <?php echo isset($arrPercentualGrupo[$grupo->codigoClienteGrupo]) && $arrPercentualGrupo[$grupo->codigoClienteGrupo]['aplicacao'] == 'A' ? 'selected' : ''; ?>>Acréscimo</option>
                  </select>
                </div>

                <div class="form-group col-xs-12 col-md-3">
                  <label>Formação do preço</label>
                  <select name="clienteGrupo[<?php echo $grupo->codigoClienteGrupo; ?>][definicao_preco]" class="form-control">
                    <option value="1" <?php echo isset($arrPercentualGrupo[$grupo->codigoClienteGrupo]) && $arrPercentualGrupo[$grupo->codigoClienteGrupo]['definicao_preco'] == '1' ? 'selected' : ''; ?>>Preço Fixo em R$ </option>
                    <option value="2" <?php echo isset($arrPercentualGrupo[$grupo->codigoClienteGrupo]) && $arrPercentualGrupo[$grupo->codigoClienteGrupo]['definicao_preco'] == '2' ? 'selected' : ''; ?>>Percentual sobre o preço de custo</option>
                  </select>
                </div>

                <div class="form-group col-xs-12 col-md-3">
                  <label>Valor</label>
                  <div class="input-group">
                    <input <?php echo $readonly; ?> class="form-control numberOnly campoMonetario" type="text" name="clienteGrupo[<?php echo $grupo->codigoClienteGrupo; ?>][percentual]" value="<?php echo isset($arrPercentualGrupo[$grupo->codigoClienteGrupo]) ? $arrPercentualGrupo[$grupo->codigoClienteGrupo]['percentual'] : ''; ?>">
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
          <!-- FIM CONTEUDO GRUPO CLIENTE -->
        </div>
        <div id="historicoCompras" class="tab-pane fade">
          <div class="box">
            <div class="box-body">
              <h3> Histórico de compras </h3>
            </div>
            <?php if ($this->permissoes->editar == 1) { ?>
              <div class="box-body">
                <h4> Registrar Compra </h4>
                <div class="form-group col-xs-12 col-md-3">
                  <label>Data da compra</label>
                  <div class="input-group date">
                    <input class="form-control data" type="text" value="<?php echo date('d/m/Y'); ?>" name="dataCompra" />
                  </div>
                </div>
                <div class="form-group col-xs-12 col-md-3">
                  <label>Valor Pago</label>
                  <div class="input-group">
                    <span class="input-group-addon">R$</span>
                    <input class="form-control numberOnly campoMonetario" type="text" name="valorPago" value="">
                  </div>
                </div>
                <div class="form-group col-xs-12 col-md-3">
                  <label>Fornecedor</label>
                  <select name="CodigoFornecedorCompra" class="form-control">
                    <option value="0">Selecione</option>
                    <?php foreach ($fornecedores as $fornecedor) {
                      $selected = "";
                      if ($fornecedor->CodigoFornecedor == $produtos[0]->CodigoFornecedor) {
                        $selected = ' selected="selected"';
                      }
                    ?>
                      <option value="<?php echo $fornecedor->CodigoFornecedor; ?>" <?php echo $selected; ?>><?php echo $fornecedor->RazaoSocial; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            <?php } ?>
            <div class="box-body">
              <table class="table table-bordered table-striped dataTable" role="grid">
                <thead>
                  <th>Data</th>
                  <th>Valor Pago</th>
                  <th>Fornecedor</th>
                  <th>Marcar para excluir</th>
                </thead>
                <tbody>
                  <?php
                  if (is_array($historicoCompra)) {
                    foreach ($historicoCompra as $historico_compra) {
                      $DataCompra = new DateTime($historico_compra->dataCompra);
                      $dataCompra = $DataCompra->format('d/m/Y');
                  ?>
                      <tr>
                        <td><?php echo $dataCompra; ?></td>
                        <td>R$ <?php echo $historico_compra->valorPago; ?></td>
                        <td><?php echo $historico_compra->RazaoSocial; ?></td>
                        <td style="text-align:left;"><input type="checkbox" name="excluirHistorico[]" value="<?php echo $historico_compra->historicoId; ?>"></td>
                      </tr>
                  <?php
                    }
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>



        <div class="box-footer">
          <?php if ($this->permissoes->editar == 1 || $this->permissoes->cadastrar == 1) { ?>
            <input type="hidden" name="handle" value="<?php echo (int) $produtos[0]->CodigoProduto; ?>">
            <button class="btn btn-primary" type="submit">Salvar</button>
          <?php } ?>
          <a class="btn btn-primary" href="<?php echo URL . $classe; ?>/listar/">Voltar</a>
        </div>
      </div>
      </form>
    </div>
  </div>
  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>