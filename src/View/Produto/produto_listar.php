<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						<?php

						if ($arrPermissoesCabecalho[$indiceProdutos]->cadastrar == 1) {
						?>
							<a class="btn btn-primary" href="<?php echo URL . $classe . '/cadastrar/'; ?>">Cadastrar Novo</a>
							<a class="btn btn-primary" id="exportarProdutos"><i class="fa fa-file-excel"></i>Exportar produtos para o excel</a>
							<a class="btn btn-primary" data-toggle="modal" data-target="#importarProdutos"><i class="fa fa-file-excel"></i>Importar produtos do excel</a>
						<?php } ?>
						<?php if ($arrPermissoesCabecalho[$indiceProdutos]->editar == 1) { ?>
							<a class="btn btn-primary" data-toggle="modal" data-target="#alterarPreco">Alterar preço em massa</a>
						<?php } ?>
						<a class="btn btn-default" id="exibirFiltroProdutos">Exibir Filtros</a>
					</h3>
					<div id="divFiltroProdutos" class="row hide" style="padding-top:30px;">
						<div class="form-group col-md-3 col-sm-12">
							<label for="filtro_componente">Componente</label>
							<select class="form-control" id="filtro_componente" name="filtro_componente">
								<option value="">Todos</option>
								<option value="1">Sim</option>
								<option value="0">Não</option>
							</select>
						</div>
						<div class="form-group col-md-3 col-sm-12">
							<label for="filtro_unidade_medida">Unidade de medida</label>
							<select class="form-control" id="filtro_unidade_medida" name="filtro_unidade_medida">
								<option value="">Todos</option>
								<?php foreach ($unidades as $unidade) { ?>
									<option value="<?php echo $unidade->CodigoUnidadeMedida; ?>"><?php echo $unidade->NomeUnidadeMedida; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group col-md-3 col-sm-12">
							<label for="filtro_fornecedor">Fornecedor</label>
							<select class="form-control" id="filtro_fornecedor" name="filtro_fornecedor">
								<option value="">Todos</option>
								<?php foreach ($fornecedores as $fornecedor) { ?>
									<option value="<?php echo $fornecedor->CodigoFornecedor; ?>"><?php echo $fornecedor->RazaoSocial; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group col-md-3 col-sm-12">
							<label for="filtro_situacao_produto">Situação</label>
							<select class="form-control" id="filtro_situacao_produto" name="filtro_situacao_produto">
								<option value="">Todos</option>
								<option value="0">Inativo</option>
								<option value="1">Ativo</option>
								<option value="2">Descontinuado</option>
							</select>
						</div>
						<div class="form-group col-md-6 col-sm-12">
							<button class="btn btn-primary" type="button" id="aplicarFiltroProdutos">Aplicar Filtro</button>
							<button class="btn btn-primary" id="limparFiltroProdutos" type="button">Limpar Filtros</button>
						</div>
					</div>
				</div>
				<?php if (isset($msg_sucesso) && !empty($msg_sucesso)) { ?>
					<div class="callout <?= $tipo_mensagem; ?>">
						<p><?= $msg_sucesso; ?></p>
					</div>
				<?php } ?>
				<div class="box-body">
					<div id="componentes-cadastrados">
						<div class="row">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table id="listaProdutos" class="dataTable table table-bordered table-striped">
										<thead>
											<tr>
												<th>Moldura</th>
												<th>Descrição</th>
												<th>Estoque</th>
												<th>Fator de perda</th>
												<th>Preço de Venda</th>
												<?php if ($this->parceria_ruberti) { ?>
													<th> Comprar </th>
												<?php } ?>
												<th>Editar</th>
												<?php if ($arrPermissoesCabecalho[$indiceProdutos]->excluir == 1) { ?>
													<th>Excluir</th>
												<?php } ?>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- Modal -->
<div class="modal fade" id="alterarPreco" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Alteração de preço em massa</h4>
				<div class="alert alert-dismissible alertaAlteracaoPreco" style="display:none">
					<div id="mensagemAlteracaoPreco"></div>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Alteração</label>
					<select class="form-control" id="alteracao" name="alteracao">
						<option value="acrescentar">Acrescentar</option>
						<option value="reduzir">Reduzir</option>
					</select>
				</div>

				<div class="form-group">
					<label>Tipo de valor</label>
					<select class="form-control" id="tipo_valor" name="tipo_valor">
						<option value="preco_venda">Preço de Venda/Margem Bruta de venda</option>
						<option value="preco_custo">Preço de custo</option>
						<option value="ambos">Ambos</option>
					</select>
				</div>

				<div class="form-group">
					<label>Valor</label>
					<input class="form-control" type="text" id="valor" name="valor" value="">
				</div>
				<div class="form-group">
					<label>Alterar por</label>
					<select class="form-control" id="tipo" name="tipo">
						<option value="P">Percentual</option>
						<option value="R">Valor em R$</option>
					</select>
				</div>
				<div class="form-group">
					<label>Fornecedor</label>
					<select class="form-control" id="fornecedor" name="fornecedor">
						<option value="">Todos</option>
						<?php foreach ($fornecedores as $fornecedor) { ?>
							<option value="<?php echo $fornecedor->CodigoFornecedor; ?>"><?php echo $fornecedor->RazaoSocial; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="alert alert-danger" role="alert"> Atenção: Todos os produtos terão o preço de venda modificado </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
				<button type="button" id="btnAlterarPreco" class="btn btn-primary">Salvar</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="importarProdutos" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Importar produtos</h4>
				<div class="alert alert-dismissible alertaAlteracaoPreco" style="display:none">
					<div id="mensagemAlteracaoPreco"></div>
				</div>
			</div>
			<div class="modal-body">
				<form enctype="multipart/form-data">
					<div class="form-group">
						<label for="arquivoImportarProdutos">Arquivo</label>
						<input type="file" class="form-control" id="arquivoImportarProdutos" name="arquivoImportarProdutos" accept=".csv,.xlsx,.xls">
					</div>
					<div class="form-group">
						<label for="excluirArquivos">Excluir todos os produtos já existentes?</label>
						<select name="excluirArquivos" id="excluirArquivos">
							<option value="N">Não</option>
							<option value="S">Sim</option>
						</select>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
				<button type="button" id="btnImportarProdutos" class="btn btn-primary">Importar</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>