<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>


<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">

      <ul class="nav nav-tabs">
        <?php if ($this->habilitar_emissao_nf == true) { ?>
        <li class="active"><a data-toggle="pill" href="#nfe">Nota fiscal de produto</a></li>
        <?php } ?>
        <?php if ($this->habilitar_emissao_nfs == true) { ?>
        <li class=""><a data-toggle="pill" href="#nfse">Nota fiscal de serviço</a></li>
        <?php } ?>
        <?php if ($this->habilitar_emissao_nfc == true) { ?>
        <li class=""><a data-toggle="pill" href="#nfce">Nota fiscal consumidor</a></li>
        <?php } ?>
      </ul>

      <div class="tab-content">
        <?php if ($this->habilitar_emissao_nf == true) { ?>
        <!-- ABA NOTA FISCAL PRODUTOS -->
        <div id="nfe" class="tab-pane fade in active">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
            </div>
            <div class="box-body">
              <div id="documentos-fiscais-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-12">
                    <div id="example1_filter" class="dataTables_filter">
                      <form autocomplete="off" name="frmCliente" id="frmBuscar">
                      </form>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                      <thead>
                        <tr role="row">
                          <th>Pedido</th>
                          <th>Cliente</th>
                          <th>Empresa</th>
                          <th>Situação</th>
                          <th>Mensagem</th>
                          <th>Ação</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $aux = 0;
                        foreach ($arrNF as $documento) {

                          if ($aux & 1) {
                        ?>
                            <tr class="odd" role="row">
                            <?php } else { ?>
                            <tr class="even" role="row">
                            <?php } ?>
                            <td style="text-align:center;"><?php echo $documento->id_pedido; ?></td>
                            <td style="text-align:left;"><?php echo $documento->Nomefantasia != '' ? $documento->Nomefantasia : $documento->RazaoSocial; ?></td>
                            <td style="text-align:left;"><?php echo $documento->nome_fantasia_empresa != '' ? $documento->nome_fantasia_empresa : $documento->razao_social_empresa; ?></td>
                            <td style="font-weight:bold;color:<?php echo $documento->cor; ?>;">
                                <?php echo $documento->situacao; ?>
                                <?php if ($documento->devolucao == 1) { ?>
                                  <br><strong style="color:red">NOTA DE DEVOLUÇÃO</strong>
                                <?php } ?>
                                <?php if ($documento->devolucao == 2) { ?>
                                  <br><strong style="color:red">NOTA DEVOLVIDA</strong>
                                <?php } ?>
                            </td>
                            <td>
                              <?php echo $documento->retorno; ?>
                              <br><a class="small" data-toggle="collapse" href="#collapsePayload<?php echo $documento->id_fila; ?>" role="button" aria-expanded="false" aria-controls="collapsePayload<?php echo $documento->id_fila; ?>">Dados enviados</a>
                              <?php /* | <a class="small" data-toggle="collapse" href="#collapseRetorno<?php echo $documento->id_fila;?>" role="button" aria-expanded="false" aria-controls="collapseRetorno<?php echo $documento->id_fila;?>">Dados recebidos</a> */ ?>
                              <div class="collapse" id="collapsePayload<?php echo $documento->id_fila; ?>">
                                <div class="card card-body">
                                  <pre><?php echo json_encode(json_decode($documento->payload), JSON_PRETTY_PRINT); ?></pre>
                                </div>
                              </div>
                              <div class="collapse" id="collapseRetorno<?php echo $documento->id_fila; ?>">
                                <div class="card card-body">
                                  <pre><?php echo json_encode(json_decode($documento->retorno), JSON_PRETTY_PRINT); ?></pre>
                                </div>
                              </div>
                            </td>
                            <td>
                              <?php echo $documento->link_pdf; ?>
                              <?php echo $documento->link_validar; ?>
                              <?php echo $documento->link_xml; ?>
                              <?php echo $documento->link_cancelar; ?>
                              <?php echo $documento->link_devolucao; ?>
                            </td>
                            </tr>
                          <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <!-- FIM ABA NOTA FISCAL PRODUTOS -->

        <!-- ABA NOTA FISCAL SERVIÇO -->
        <?php if ($this->habilitar_emissao_nfs == true) { ?>
        <div id="nfse" class="tab-pane fade">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
            </div>
            <div class="box-body">
              <div id="documentos-fiscais-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-12">
                    <div id="example1_filter" class="dataTables_filter">
                      <form autocomplete="off" name="frmCliente" id="frmBuscar">
                        <label>
                          Buscar:
                          <select name="coluna" class="form-control">
                            <?php foreach ($arrCamposBusca as $value => $descricao) {
                              $selected = '';
                              if ($value == $coluna) {
                                $selected = 'selected';
                              }
                            ?>
                              <option value="<?php echo $value; ?>" <?php echo $selected; ?>><?php echo $descricao; ?></option>
                            <?php } ?>
                          </select>
                          <input class="form-control input-sm" type="search" placeholder="" value="<?php echo $buscar; ?>" name="buscar" aria-controls="example1">
                          <input type="button" class="btn btn-primary" id="btnBuscar" value="Buscar" />
                        </label>
                      </form>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                      <thead>
                        <tr role="row">
                          <th>Pedido</th>
                          <th>Cliente</th>
                          <th>Empresa</th>
                          <th>Situação</th>
                          <th>Mensagem</th>
                          <th>Ação</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $aux = 0;
                        if (count($arrNFS) > 0) {
                          foreach ($arrNFS as $documento) {
                            if ($aux & 1) {
                        ?>
                              <tr class="odd" role="row">
                              <?php } else { ?>
                              <tr class="even" role="row">
                              <?php } ?>
                              <td style="text-align:center;"><?php echo $documento->id_pedido; ?></td>
                              <td style="text-align:left;"><?php echo $documento->Nomefantasia != '' ? $documento->Nomefantasia : $documento->RazaoSocial; ?></td>
                              <td style="text-align:left;"><?php echo $documento->nome_fantasia_empresa != '' ? $documento->nome_fantasia_empresa : $documento->razao_social_empresa; ?></td>                              
                              <td style="font-weight:bold;color:<?php echo $documento->cor; ?>;"><?php echo $documento->situacao; ?></td>
                              <td>
                                <?php echo $documento->retorno; ?>
                                <br><a class="small" data-toggle="collapse" href="#collapsePayload<?php echo $documento->id_fila; ?>" role="button" aria-expanded="false" aria-controls="collapsePayload<?php echo $documento->id_fila; ?>">Dados enviados</a>
                                <?php /* | <a class="small" data-toggle="collapse" href="#collapseRetorno<?php echo $documento->id_fila;?>" role="button" aria-expanded="false" aria-controls="collapseRetorno<?php echo $documento->id_fila;?>">Dados recebidos</a> */ ?>
                                <div class="collapse" id="collapsePayload<?php echo $documento->id_fila; ?>">
                                  <div class="card card-body">
                                    <pre><?php echo json_encode(json_decode($documento->payload), JSON_PRETTY_PRINT); ?></pre>
                                  </div>
                                </div>
                                <div class="collapse" id="collapseRetorno<?php echo $documento->id_fila; ?>">
                                  <div class="card card-body">
                                    <pre><?php echo json_encode(json_decode($documento->retorno), JSON_PRETTY_PRINT); ?></pre>
                                  </div>
                                </div>
                              </td>
                              <td>
                                <?php echo $documento->link_pdf; ?>
                                <?php echo $documento->link_validar; ?>
                                <?php echo $documento->link_xml; ?>
                                <?php echo $documento->link_cancelar; ?>
                                <?php echo $documento->link_devolucao; ?>
                              </td>
                              </tr>
                            <?php } ?>
                          <?php } else { ?>
                            <tr>
                              <td style="text-align:center;" colspan="4">Não foram encontrados documentos do tipo Nota Fiscal de Serviço</td>
                            </tr>
                          <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <!-- FIM ABA NOTA FISCA SERVIÇO -->

        <!-- ABA NOTA FISCAL CONSUMIDOR -->
        <?php if ($this->habilitar_emissao_nfc == true) { ?>
        <div id="nfce" class="tab-pane fade">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
            </div>
            <div class="box-body">
              <div id="documentos-fiscais-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-12">
                    <div id="example1_filter" class="dataTables_filter">
                      <form autocomplete="off" name="frmCliente" id="frmBuscar">
                        <label>
                          Buscar:
                          <select name="coluna" class="form-control">
                            <?php foreach ($arrCamposBusca as $value => $descricao) {
                              $selected = '';
                              if ($value == $coluna) {
                                $selected = 'selected';
                              }
                            ?>
                              <option value="<?php echo $value; ?>" <?php echo $selected; ?>><?php echo $descricao; ?></option>
                            <?php } ?>
                          </select>
                          <input class="form-control input-sm" type="search" placeholder="" value="<?php echo $buscar; ?>" name="buscar" aria-controls="example1">
                          <input type="button" class="btn btn-primary" id="btnBuscar" value="Buscar" />
                        </label>
                      </form>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                      <thead>
                        <tr role="row">
                          <th>Pedido</th>
                          <th>Cliente</th>
                          <th>Empresa</th>
                          <th>Situação</th>
                          <th>Mensagem</th>
                          <th>Ação</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $aux = 0;
                        if (count($arrNFC) > 0) {
                          foreach ($arrNFC as $documento) {
                            if ($aux & 1) {
                        ?>
                              <tr class="odd" role="row">
                              <?php } else { ?>
                              <tr class="even" role="row">
                              <?php } ?>
                              <td style="text-align:center;"><?php echo $documento->id_pedido; ?></td>
                              <td style="text-align:left;"><?php echo $documento->Nomefantasia != '' ? $documento->Nomefantasia : $documento->RazaoSocial; ?></td>
                              <td style="text-align:left;"><?php echo $documento->nome_fantasia_empresa != '' ? $documento->nome_fantasia_empresa : $documento->razao_social_empresa; ?></td>                              
                              <td style="font-weight:bold;color:<?php echo $documento->cor; ?>;"><?php echo $documento->situacao; ?></td>
                              <td>
                                <?php echo $documento->retorno; ?>
                                <br><a class="small" data-toggle="collapse" href="#collapsePayload<?php echo $documento->id_fila; ?>" role="button" aria-expanded="false" aria-controls="collapsePayload<?php echo $documento->id_fila; ?>">Dados enviados</a>
                                <?php /* | <a class="small" data-toggle="collapse" href="#collapseRetorno<?php echo $documento->id_fila;?>" role="button" aria-expanded="false" aria-controls="collapseRetorno<?php echo $documento->id_fila;?>">Dados recebidos</a> */ ?>
                                <div class="collapse" id="collapsePayload<?php echo $documento->id_fila; ?>">
                                  <div class="card card-body">
                                    <pre><?php echo json_encode(json_decode($documento->payload), JSON_PRETTY_PRINT); ?></pre>
                                  </div>
                                </div>
                                <div class="collapse" id="collapseRetorno<?php echo $documento->id_fila; ?>">
                                  <div class="card card-body">
                                    <pre><?php echo json_encode(json_decode($documento->retorno), JSON_PRETTY_PRINT); ?></pre>
                                  </div>
                                </div>
                              </td>
                              <td>
                                <?php echo $documento->link_pdf; ?>
                                <?php echo $documento->link_validar; ?>
                                <?php echo $documento->link_xml; ?>
                                <?php echo $documento->link_cancelar; ?>
                                <?php echo $documento->link_devolucao; ?>
                              </td>
                              </tr>
                            <?php } ?>
                          <?php } else { ?>
                            <tr>
                              <td style="text-align:center;" colspan="4">Não foram encontrados documentos do tipo Nota Fiscal do consumidor</td>
                            </tr>
                          <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <!-- FIM ABA NOTA FISCA CONSUMIDOR -->

      </div>
    </div>
  </div>

</section>
<!-- /.content -->
</div>


<div class="modal fade" id="modalCancelarNota" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Cancelar Nota</h4>
        <div class="alert alert-dismissible alertaAlteracaoPreco" style="display:none">
          <div id="mensagemAlteracaoPreco"></div>
        </div>
      </div>
      <div class="modal-body">
        <form enctype="multipart/form-data">
          <div class="form-group">
            <label for="motivoCancelamento">Motivo do cancelamento</label>
            <input type="text" class="form-control" required id="motivoCancelamento" value="" name="motivoCancelamento">
            <input type="hidden" class="form-control" id="notaCancelamento" value="" name="notaCancelamento">
            <input type="hidden" class="form-control" id="notaTipoDocumento" value="" name="notaTipoDocumento">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
        <button type="button" id="btnEnviarCancelamento" class="btn btn-primary">Solicitar cancelamento</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>