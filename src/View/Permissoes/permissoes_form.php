<?php include_once(DOCUMENT_ROOT . "src/View/Common/cabecalho.php"); ?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <?php if (!empty($msg_sucesso)) { ?>
                    <div class="callout callout-success">
                        <h4>Sucesso!</h4>
                        <p>
                            <?php echo $msg_sucesso; ?>
                        </p>
                    </div>
                <?php } ?>
                <div class="box-header">
                    <h3 class="box-title"></h3>
                </div>
                <div class="box-body">
                    <div id="permissoes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
                        <form role="form" action="<?php echo URL . $classe . "/$metodo/$handle"; ?>" method="POST">
                            <div class="row">
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label>Nome</label>
                                        <input class="form-control" type="text" name="descricao" value="<?php echo $arrGrupoUsuario->descricao; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <div class="form-group">
                                        <label>Situação</label>
                                        <select name="situacao">
                                            <option value="1" <?php echo $arrGrupoUsuario->situacao == 1 ? 'selected' : ''; ?>>Ativo</option>
                                            <option value="0" <?php echo $arrGrupoUsuario->situacao == 0 ? 'selected' : ''; ?>>Inativo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <tr role="row">
                                                <th>Descrição</th>
                                                <th style="text-align: center;">Visualizar</th>
                                                <th style="text-align: center;">Cadastrar</th>
                                                <th style="text-align: center;">Alterar</th>
                                                <th style="text-align: center;">Excluir</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <form action="" method="POST">
                                                <?php
                                                $aux = 0;
                                                foreach ($arrPermissoes as $permissao_entidade) {
                                                    if ($aux & 1) {
                                                ?>
                                                        <tr class="odd" role="row">
                                                        <?php } else { ?>
                                                        <tr class="even" role="row">
                                                        <?php } ?>
                                                        <td style="<?php echo $permissao_entidade->permissao_complementar == 1 ? 'padding-left: 2em;' : ''; ?>">
                                                            <?php echo $permissao_entidade->permissao_complementar == 1 ? '<small><i class="fa fa-circle"></i></small> ' . $permissao_entidade->descricao : $permissao_entidade->descricao; ?>
                                                        </td>
                                                        <?php if ($permissao_entidade->nome != 'SITUACOES_PARA_SEREM_ATRIBUIDAS') { ?>
                                                            <td style="text-align: center;">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" name="permissoes[<?php echo $permissao_entidade->id_permissao; ?>][visualizar]" value="1" <?php echo $permissao_entidade->visualizar == 1 ? 'checked' : ''; ?>>
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td style="text-align: center;">
                                                                <?php if ($permissao_entidade->entidade != 'Usuario' &&  $permissao_entidade->entidade != 'Empresa' && $permissao_entidade->nome != 'PERMITIR_RETORNAR_SITUACAO') { ?>
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="checkbox" name="permissoes[<?php echo $permissao_entidade->id_permissao; ?>][cadastrar]" value="1" <?php echo $permissao_entidade->cadastrar == '1' ? 'checked' : ''; ?>>
                                                                        </label>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <input type="hidden" name="permissoes[<?php echo $permissao_entidade->id_permissao; ?>][cadastrar]" value="1">
                                                                    <i>Não se aplica</i>
                                                                <?php } ?>
                                                            </td>
                                                            <td style="text-align: center;">
                                                                <?php if ($permissao_entidade->nome != 'PERMITIR_RETORNAR_SITUACAO') { ?>
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="checkbox" name="permissoes[<?php echo $permissao_entidade->id_permissao; ?>][editar]" value="1" <?php echo $permissao_entidade->editar == 1 ? 'checked' : ''; ?>>
                                                                        </label>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <input type="hidden" name="permissoes[<?php echo $permissao_entidade->id_permissao; ?>][editar]" value="1">
                                                                    <i>Não se aplica</i>
                                                                <?php } ?>
                                                            </td>
                                                            <td style="text-align: center;">
                                                                <?php if ($permissao_entidade->nome != 'PERMITIR_RETORNAR_SITUACAO') { ?>
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="checkbox" name="permissoes[<?php echo $permissao_entidade->id_permissao; ?>][excluir]" value="1" <?php echo $permissao_entidade->excluir == 1 ? 'checked' : ''; ?>>
                                                                        </label>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <input type="hidden" name="permissoes[<?php echo $permissao_entidade->id_permissao; ?>][excluir]" value="1">
                                                                    <i>Não se aplica</i>
                                                                <?php } ?>
                                                            </td>
                                                        <?php } else { ?>
                                                            <td colspan="4" style="text-align: center">
                                                                <?php
                                                                foreach ($this->situacoes as $situacao) {
                                                                ?>
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="checkbox" name="permissoes[<?php echo $permissao_entidade->id_permissao; ?>][situacoes_atribuir][]" value="<?php echo $situacao->idSituacao; ?>" <?php echo in_array($situacao->idSituacao, explode(',', $permissao_entidade->conteudo)) ? 'checked' : ''; ?>>
                                                                            <?php echo $situacao->descricao; ?>
                                                                        </label>
                                                                    </div>
                                                                    &nbsp;
                                                                <?php
                                                                }
                                                                ?>
                                                            </td>
                                                        <?php } ?>
                                                        </tr>

                                                    <?php } ?>
                                            </form>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button class="btn btn-primary" type="submit">Salvar alterações</button>
                                    <a class="btn btn-primary" href="<?php echo URL . $classe . "/Listar/"; ?>">Voltar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<?php include_once(DOCUMENT_ROOT . "src/View/Common/rodape.php"); ?>