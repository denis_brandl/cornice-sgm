<?php include_once(DOCUMENT_ROOT . "src/View/Common/cabecalho.php"); ?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                </div>
                <div class="box-body">
                    <div id="permissoes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
                        <form role="form" action="<?php echo URL . $classe . '/listar/'; ?>" method="POST">
                            <div class="row">
                                <div class="col-sm-5">
                                    <a class="btn btn-primary" href="<?php echo URL . $classe . '/cadastrar/'; ?>">Cadastrar Novo</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <tr role="row">
                                                <th>Grupo</th>
                                                <th>Status</th>
                                                <th style="text-align: center;"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <form action="" method="POST">
                                                <?php
                                                $aux = 0;
                                                foreach ($consultaGruposUsuarios as $grupo) {
                                                    if ($aux & 1) {
                                                ?>
                                                        <tr class="odd" role="row">
                                                        <?php } else { ?>
                                                        <tr class="even" role="row">
                                                        <?php } ?>
                                                        <td><?php echo $grupo->descricao; ?></td>
                                                        <td><?php echo $grupo->situacao == 1 ? 'Ativo' : 'Inativo'; ?></td>
                                                        <td><a class="" href="<?php echo URL . $classe . '/editar/' . ($grupo->id_grupo ?: 0); ?>"><i class="fa fa-edit"></i>Editar</a></td>
                                                        </tr>
                                                    <?php } ?>
                                            </form>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<?php include_once(DOCUMENT_ROOT . "src/View/Common/rodape.php"); ?>