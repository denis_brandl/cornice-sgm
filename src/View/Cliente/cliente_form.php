<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">

      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="pill" href="#geral">Geral</a></li>
        <?php if ($this->habilita_financeiro) { ?>
          <li class=""><a data-toggle="pill" href="#informacoesFinanceiras">Financeiro</a></li>
        <?php } ?>
      </ul>

      <div class="tab-content">
        <div id="geral" class="tab-pane fade in active">
          <div class="box">
            <form autocomplete="off" role="form" method="POST"
              action="<?php echo URL; ?><?php echo $classe; ?>/<?php echo $metodo; ?>/<?php echo (int) $clientes[0]->CodigoCliente; ?>">
              <?php if (!empty($msg_sucesso)) { ?>
                <div class="callout callout-success">
                  <h4>Sucesso!</h4>
                  <p>
                    <?php echo $msg_sucesso; ?>
                  </p>
                </div>
              <?php } ?>
              <div class="box-body">
                <div class="form-group col-xs-12 col-md-3">
                  <label>Tipo de cliente</label>
                  <select name="TipoCliente" class="form-control tipoCliente">
                    <?php foreach ($dominios as $dominio) {
                      $selected = "";
                      if ($dominio->Cd_Idenficacao == $clientes[0]->TipoCliente) {
                        $selected = ' selected="selected"';
                      }
                      ?>
                      <option value="<?php echo $dominio->Cd_Idenficacao; ?>" <?php echo $selected; ?>><?php echo $dominio->Ds_Referencia; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group col-xs-12 col-md-3">
                  <label class="nomeRazao">Nome</label>
                  <input class="form-control" type="text"
                    value="<?php echo isset($clientes[0]->RazaoSocial) ? $clientes[0]->RazaoSocial : ''; ?>"
                    name="RazaoSocial" />
                </div>

                <div class="form-group col-xs-12 col-md-3 nomeFantasia">
                  <label class="">Nome Fantasia</label>
                  <input class="form-control" type="text" name="NomeFantasia"
                    value="<?php echo $clientes[0]->Nomefantasia; ?>">
                </div>

                <div class="form-group col-xs-12 col-md-3">
                  <label>Grupo do cliente</label>
                  <select name="codigoClienteGrupo" class="form-control">
                    <?php foreach ($clienteGrupos as $grupo) {
                      $selected = "";
                      if ($grupo->codigoClienteGrupo == $clientes[0]->codigoClienteGrupo) {
                        $selected = ' selected="selected"';
                      }
                      ?>
                      <option value="<?php echo $grupo->codigoClienteGrupo; ?>" <?php echo $selected; ?>><?php echo $grupo->nome; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="box-body">
                <div class="form-group col-xs-12 col-md-4">
                  <label class="documentoCliente">
                    <?php echo $clientes[0]->TipoCliente === 'F' ? 'CPF' : 'CNPJ' ?>
                  </label>
                  <input class="form-control" id="cgc" type="text" name="CGC" value="<?php echo $clientes[0]->CGC; ?>">
                </div>

                <div class="form-group col-xs-12 col-md-4 inscricaoEstadual"
                  style="display:<?php echo $clientes[0]->TipoCliente === 'F' ? 'none' : 'block' ?>">
                  <label class="">Inscrição Estadual</label>
                  <input class="form-control" type="text" name="InscricaoEstadual"
                    value="<?php echo $clientes[0]->InscricaoEstadual; ?>">
                </div>

                <div class="form-group col-xs-12 col-md-4">
                  <label>E-mail</label>
                  <input class="form-control" type="email" name="EMail" value="<?php echo $clientes[0]->EMail; ?>">
                </div>
              </div>

              <div class="box-body">
                <div class="form-group col-xs-12 col-md-4">
                  <label>Telefone Principal</label>
                  <input class="form-control" type="text" name="Telefone1"
                    value="<?php echo $clientes[0]->Telefone1; ?>">
                  <p class="small">Também pode ser o número de whatsapp</p>
                </div>

                <div class="form-group col-xs-12 col-md-4">
                  <label>Telefone Alternativo</label>
                  <input class="form-control" type="text" name="Telefone2"
                    value="<?php echo $clientes[0]->Telefone2; ?>">
                </div>

                <div class="form-group col-xs-12 col-md-4">
                  <label>Telefone Alternativo 2</label>
                  <input class="form-control" type="text" name="Fax" value="<?php echo $clientes[0]->Fax; ?>">
                </div>
              </div>

              <div class="box-body">
                <div class="form-group col-xs-12 col-md-1">
                  <label>CEP</label>
                  <input class="form-control" type="text" name="CEP" id="CEP" value="<?php echo $clientes[0]->CEP; ?>">
                </div>

                <div class="form-group col-xs-12 col-md-1">
                  <label>Logradouro</label>
                  <select name="idLogradouro" class="form-control">
                    <?php foreach ($this->arrLogradouros as $key => $value) { ?>
                      <option value="<?php echo $value->idLogradouro; ?>" <?php echo $value->idLogradouro == $clientes[0]->idLogradouro ? 'selected' : ''; ?>><?php echo $value->descricao; ?>
                      </option>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group col-xs-12 col-md-6">
                  <label>Endereço</label>
                  <input class="form-control" type="text" name="Endereco" value="<?php echo $clientes[0]->Endereco; ?>">
                </div>

                <div class="form-group col-xs-12 col-md-2">
                  <label>Número</label>
                  <input class="form-control" type="text" name="numeroEndereco"
                    value="<?php echo $clientes[0]->numeroEndereco; ?>">
                </div>


                <div class="form-group col-xs-12 col-md-2">
                  <label>Complemento</label>
                  <input class="form-control" type="text" name="Complemento"
                    value="<?php echo $clientes[0]->Complemento; ?>">
                </div>

                <div class="form-group col-xs-12 col-md-4">
                  <label>Bairro</label>
                  <input class="form-control" type="text" name="Bairro" value="<?php echo $clientes[0]->Bairro; ?>">
                </div>

                <div class="form-group col-xs-12 col-md-4">
                  <label>Estado</label>
                  <select name="Estado" id="Estado" class="form-control">
                    <option value="">Selecione</option>
                    <?php foreach ($estados as $estado_sigla => $estado_descricao) {
                      $selected = "";
                      if ($estado_sigla == $clientes[0]->uf) {
                        $selected = ' selected="selected"';
                      }
                      ?>
                      <option value="<?php echo $estado_sigla; ?>" <?php echo $selected; ?>><?php echo $estado_descricao; ?>
                      </option>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group col-xs-12 col-md-4">
                  <label>Cidade</label>
                  <!-- <input class="form-control" type="text" name="Cidade" value="<?php echo $clientes[0]->Cidade; ?>"> -->
                  <select name="Cidade" id="Cidade" class="form-control">
                  </select>
                </div>
                <div class="form-group col-xs-12 col-md-4">
                  <label>Data de Cadastro</label>
                  <p class="small">Informação gerada automaticamente</p>
                  <?php
                  $dataCadastro = '';
                  if ($clientes[0]->DataCadastro !== null) {
                    $DataCadastro = new DateTime($clientes[0]->DataCadastro);
                    $dataCadastro = $DataCadastro->format('d/m/Y');
                  }
                  ?>
                  <input type="text" disabled data-mask="" name="DataCadastro" value="<?php echo $dataCadastro; ?>"
                    data-inputmask="'alias': 'dd/mm/yyyy'" class="form-control">
                </div>


              </div>

              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Observações</h3>
                </div>
              </div>

              <div class="box-body">
                <div class="form-group">
                  <label>Observações</label>
                  <textarea class="form-control" name="Observacoes" />
                  <?php echo $clientes[0]->Observacoes; ?></textarea>
                </div>
              </div>
              <div class="box-footer">
                <input type="hidden" name="handle" value="<?php echo $clientes[0]->CodigoCliente; ?>">
                <button class="btn btn-primary" type="submit">Salvar</button>
                <a class="btn btn-primary" href="<?php echo URL; ?><?php echo $classe; ?>/listar/">Voltar</a>
              </div>
            </form>
          </div>
        </div>
        <div id="informacoesFinanceiras" class="tab-pane fade">
          <div class="box-body">
            <table class="table table-bordered table-striped dataTable" role="grid">
              <thead>
                <th>Data de Lançamento</th>
                <th>Categoria</th>
                <th>Valor</th>
                <th>Conta</th>
                <th>Empresa</th>
                <th>Observação</th>
                <th>Data de vencimento</th>
                <th>Data de pagamento</th>
                <th>Alterar</th>
              </thead>
              <tbody>
              <?php
                $aux = 0;
                if (count($arrAssets) > 0 ) {
                foreach ($arrAssets as $Assets) {
                  if ($aux & 1) {
                    ?>
                    <tr class="odd" role="row">
                    <?php } else { ?>
                    <tr class="even" role="row">
                    <?php } ?>
                    <td>
                      <?php echo date('d/m/Y', strtotime($Assets->Date)); ?>
                    </td>
                    <td>
                      <?php echo $Assets->CategoryName; ?>
                    </td>
                    <td>R$
                      <?php echo number_format($Assets->Amount, 2, ',', '.'); ?>
                    </td>
                    <td>
                      <?php echo $Assets->nomeConta; ?>
                    </td>
                    <td>
                      <?php echo $Assets->Empresa; ?>
                    </td>
                    <td>
                      <?php echo $Assets->Description; ?>
                    </td>
                    <td>
                      <?php echo date('d/m/Y', strtotime($Assets->cdtvencimento)); ?>
                    </td>
                    <td>
                      <?php echo $Assets->cdtpagamento != '' ? date('d/m/Y', strtotime($Assets->cdtpagamento)) : '<i>Em aberto</i>'; ?>
                    </td>
                    <td>
                      <a class="" href="<?php echo URL . $classe . '/' . $acao . '/' . $Assets->AssetsId; ?>"><i
                          class="fa fa-edit"></i>Editar</a>
                    </td>
                  </tr>
                <?php }
              } else { ?>
                    <tr><td colspan="9">Não existem lançamentos para este cliente </td></tr>
                <?php } ?>
              </tbody>
            </table>
          </div>          
        </div>
      </div>
    </div>
  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>