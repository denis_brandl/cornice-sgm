<?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">					
						<h3 class="box-title">
							<a class="btn btn-primary" href="<?php echo URL.$classe.'/cadastrar/';?>">Cadastrar Novo</a>
						</h3>					
					</div>
					<?php if (isset($msg_sucesso) && !empty($msg_sucesso)) { ?>
							<div class="callout <?= $tipo_mensagem;?>">
								<p><?= $msg_sucesso; ?></p>
							</div>
					<?php } ?>					
					<div class="box-body">					
						<div id="componentes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
							<div class="row">
								<div class="col-sm-12">
									<table id="listaClientes" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th>Nome/Razão Social</th>
												<th>Nome Fantasia<br><span class="small">Se pessoa jurídica</span></th>
												<th>Endereço</th>
												<th>Telefone 1</th>
												<th>Telefone 2</th>
												<th>Ramal</th>
												<th>E-mail</th>
												<th>Contato</th>
												<th>Editar</th>
												<th>Excluir</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>									
								</div>								
							</div>
						</div>
					</div>			
				</div>				
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  
