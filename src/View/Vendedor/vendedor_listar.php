<?php include_once(DOCUMENT_ROOT."src/View/Common/cabecalho.php"); ?>  
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box">
<div class="box-header">					
<h3 class="box-title"></h3>					
</div>              
<div class="box-body">
<div id="vendedor-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
<div class="row">
<div class="col-sm-6">
<a class="btn btn-primary" href="<?php echo URL.$classe.'/cadastrar/';?>">Cadastrar Novo</a>
</div>
<div class="col-sm-6">
</div>
</div>
<div class="row">
<div class="col-sm-12">
<table class="table table-bordered table-striped dataTable">
<thead>
<tr role="row">
<th>id_vendedor</th>
<th>nome_vendedor</th>
<th>percentual_comissao</th>
<th>id_usuario</th>
<th>afiliado</th>
<th>situacao</th>
<th>criado_em</th>
<th>modificado_em</th>
<th>criado_por</th>
<th>modificado_por</th>
</tr>
</thead>
<tbody>
<?php 
$aux = 0;
foreach ($arrVendedor as $Vendedor) {
if ($aux & 1) {
?>
<tr class="odd" role="row">
<?php } else { ?>	
<tr class="even" role="row">
<?php } ?>	
<td><?php echo $Vendedor->id_vendedor; ?></td>
<td><?php echo $Vendedor->nome_vendedor; ?></td>
<td><?php echo $Vendedor->percentual_comissao; ?></td>
<td><?php echo $Vendedor->id_usuario; ?></td>
<td><?php echo $Vendedor->afiliado; ?></td>
<td><?php echo $Vendedor->situacao; ?></td>
<td><?php echo $Vendedor->criado_em; ?></td>
<td><?php echo $Vendedor->modificado_em; ?></td>
<td><?php echo $Vendedor->criado_por; ?></td>
<td><?php echo $Vendedor->modificado_por; ?></td>
<td><a class="" href="<?php echo URL.$classe.'/'.$acao.'/'. $Vendedor->id_vendedor; ?>"><i class="fa fa-edit"></i>Editar</a></td>
</tr>
<?php } ?>
</tbody>
</table>									
</div>								
</div>                
</div>
</div>
</div>
</div>
</div>
</section>
</div>
<?php include_once(DOCUMENT_ROOT."src/View/Common/rodape.php"); ?>  
