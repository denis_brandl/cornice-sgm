<?php include_once(DOCUMENT_ROOT."src/View/Common/cabecalho.php"); ?>
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box">
<form method="POST" action="<?php echo URL.$classe."/".$metodo."/".($Vendedor->id_vendedor ?: 0);?>">
<?php if (!empty($msg_sucesso)) { ?>
<div class="callout callout-success">
<h4>Sucesso!</h4>
<p><?php echo $msg_sucesso;?></p>
</div>
<?php } ?>
<div class="box box-primary">
<div class="box-header with-border">
<h3 class="box-title">Cadastro da sua Vendedor</h3>
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>id_vendedor</label>
<input class="form-control" type="text" name="id_vendedor" value="<?php echo $Vendedor->id_vendedor;?>">
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>nome_vendedor</label>
<input class="form-control" type="text" name="nome_vendedor" value="<?php echo $Vendedor->nome_vendedor;?>">
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>percentual_comissao</label>
<input class="form-control" type="text" name="percentual_comissao" value="<?php echo $Vendedor->percentual_comissao;?>">
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>id_usuario</label>
<input class="form-control" type="text" name="id_usuario" value="<?php echo $Vendedor->id_usuario;?>">
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>afiliado</label>
<input class="form-control" type="text" name="afiliado" value="<?php echo $Vendedor->afiliado;?>">
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>situacao</label>
<input class="form-control" type="text" name="situacao" value="<?php echo $Vendedor->situacao;?>">
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>criado_em</label>
<input class="form-control" type="text" name="criado_em" value="<?php echo $Vendedor->criado_em;?>">
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>modificado_em</label>
<input class="form-control" type="text" name="modificado_em" value="<?php echo $Vendedor->modificado_em;?>">
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>criado_por</label>
<input class="form-control" type="text" name="criado_por" value="<?php echo $Vendedor->criado_por;?>">
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>modificado_por</label>
<input class="form-control" type="text" name="modificado_por" value="<?php echo $Vendedor->modificado_por;?>">
</div>
</div>
<div class="box-footer">
<input type="hidden" name="handle" value="<?php echo $Vendedor->id_vendedor;?>">
<button class="btn btn-primary" type="submit">Salvar</button>
<a class="btn btn-primary" href="<?php echo URL.$classe.'/Listar/';?>">Voltar</a>
</div>                
</form>
</div>
</div>
</div>
</section>
</div>
<?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  
