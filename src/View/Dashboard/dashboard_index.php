<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<?php if (count($this->empresas) > 1 && (count($objOrcamento->arrPermissoesUsuarioEmpresa) < 1 || count($objOrcamento->arrPermissoesUsuarioEmpresa) == 0)) { ?>
			<div class="form-group col-md-3 col-sm-12">
				<label for="empresa">Empresa</label>
				<select class="form-control" id="filtro_empresa_dashboard" name="filtro_empresa_dashboard">
					<option value="">Todas</option>
					<?php
					foreach ($this->empresas as $empresa) {
						echo sprintf(
							'<option %s value="%s">%s</option>',
							$empresa->CodigoEmpresa == $filtro_empresa ? 'selected' : '',
							$empresa->CodigoEmpresa,
							$empresa->RazaoSocial
						);
					}
					?>
				</select>
			</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo URL . 'Orcamento/listar/0/Id_Situacao=1'; ?>" style="text-decoration:none;color: #000;">
				<div class="info-box">
					<span class="info-box-icon bg-aqua">
						<i class="ion ion-battery-empty"></i>
					</span>
					<div class="info-box-content">
						<span class="info-box-text">Aguardando<br>Aprovação</span>
						<span class="info-box-number">
							<?php echo $pedidosPorSituacao[1]; ?>
							<small>Pedidos</small>
						</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo URL . 'Orcamento/listar/0/Id_Situacao=3'; ?>" style="text-decoration:none;color: #000;">
				<div class="info-box">
					<span class="info-box-icon bg-yellow">
						<i class="ion ion-battery-low"></i>
					</span>
					<div class="info-box-content">
						<span class="info-box-text">Produzindo</span>
						<span class="info-box-number">
							<?php echo $pedidosPorSituacao[3]; ?>
							<small>Pedidos</small>
						</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo URL . 'Orcamento/listar/0/Id_Situacao=4'; ?>" style="text-decoration:none;color: #000;">
				<div class="info-box">
					<span class="info-box-icon bg-green">
						<i class="ion ion-battery-half"></i>
					</span>
					<div class="info-box-content">
						<span class="info-box-text">Pronto
							<small><br>para entrega</small>
						</span>
						<span class="info-box-number">
							<?php echo $pedidosPorSituacao[4]; ?>
							<small>Pedidos</small>
						</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo URL . 'Orcamento/listar/0/Id_Situacao=7'; ?>" style="text-decoration:none;color: #000;">
				<div class="info-box">
					<span class="info-box-icon bg-red">
						<i class="ion ion-alert"></i>
					</span>
					<div class="info-box-content">
						<span class="info-box-text">Atrasado
						</span>
						<span class="info-box-number">
							<?php echo $pedidosPorSituacao[-1]; ?>
							<small>Pedidos</small>
						</span>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Últimos Pedidos</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse" type="button">
							<i class="fa fa-minus"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="table-responsive">
						<table class="table no-margin">
							<thead>
								<tr>
									<th>Pedidos</th>
									<th>Cliente</th>
									<?php if (count($this->empresas) > 1 && (count($objOrcamento->arrPermissoesUsuarioEmpresa) < 1 || count($objOrcamento->arrPermissoesUsuarioEmpresa) == 0)) { ?>
										<th>Empresa</th>
									<?php } ?>
									<th>Vendedor</th>
									<th>Data do pedido</th>
									<th>Data Prevista de Entrega</th>
									<th>Situação</th>
								</tr>
							</thead>
							<?php if (count($ultimosPedidos) > 0) { ?>
								<tbody>
									<?php foreach ($ultimosPedidos as $pedido) { ?>
										<tr>
											<td><?php echo sprintf('<a href="%s">%s</a>', URL . 'Orcamento/salvar/' . $pedido->Cd_Orcamento, $pedido->Cd_Orcamento); ?></td>
											<td><?php echo $pedido->RazaoSocial; ?></td>
											<?php if (count($this->empresas) > 1 && (count($objOrcamento->arrPermissoesUsuarioEmpresa) < 1 || count($objOrcamento->arrPermissoesUsuarioEmpresa) == 0)) { ?>
												<td><?php echo $pedido->nome_empresa; ?></td>
											<?php } ?>
											<td><?php echo $pedido->nome_vendedor; ?></td>
											<td><?php echo date('d/m/Y', strtotime($pedido->Dt_Orcamento)); ?></td>
											<td><?php echo $pedido->Dt_Prevista_Entrega != "" ? date('d/m/Y', strtotime($pedido->Dt_Prevista_Entrega)) : '<i>Não cadastrado</i>'; ?></td>
											<td><?php echo $pedido->descricao; ?></td>
										</tr>
									<?php } ?>
								</tbody>
							<?php } ?>
						</table>
					</div>
				</div>
				<div class="box-footer clearfix">
					<?php if ($objOrcamento->permissoes[0]->cadastrar) { ?>
					<a class="btn btn-sm btn-info btn-flat pull-left" href="<?php echo URL; ?>Orcamento/salvar/">Novo Pedido</a>
					<?php } ?>
					<a class="btn btn-sm btn-default btn-flat pull-right" href="<?php echo URL; ?>/Orcamento/listar/">Ver todos os pedidos</a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php if ($arrPermissoesCabecalho[$indiceDocumentosFiscais]->visualizar == 1 && $this->habilita_financeiro) { ?>
	<!-- Dashboard Financeiro -->
	<section class="content-header">
		<h1>
			<small><i class="fa "></i></small>
			<a style="text-decoration:none;" href="http://127.0.0.1:8080/Dashboard/listar/">Financeiro</a>
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<?php foreach ($saldoAtualPorEmpresa as $empresa => $saldos) {
						$indice_empresa = array_search($empresa, array_column($this->empresas, 'CodigoEmpresa'));
					?>

						<div class="box-header">
							<h5 class="box-title">Empresa: <?php echo $this->empresas[$indice_empresa]->RazaoSocial; ?></h5>
						</div>
						<div class="box-body">
							<?php
							foreach ($saldos as $conta => $saldo) {
								$icon = 'ion-cash';
								$color = 'bg-green';
								switch ($conta) {
									case 0:
										$icon = 'ion-cash';
										$color = 'bg-green';
										break;
									case 2:
										$icon = 'ion-card';
										$color = 'bg-purple';
										break;
									case 1:
										$icon = 'ion-ios-home';
										$color = 'bg-blue';
										break;
									case 3:
										$icon = 'ion-briefcase';
										$color = 'bg-maroon';
										break;
									case 'receber':
										$icon = 'ion-arrow-up-a';
										$color = 'bg-orange';
										break;
									case 'pagar':
										$icon = 'ion-arrow-down-a';
										$color = 'bg-aqua';
										break;
								}
							?>
								<div class="col-md-2 col-sm-6 col-xs-12">
									<div class="info-box">
										<span class="info-box-icon <?php echo $color; ?>">
											<i class="ion <?php echo $icon; ?>"></i>
										</span>
										<div class="info-box-content">
											<span class="info-box-text"><?php echo $saldo['descricao'] ?></span>
											<span class="info-box-number"> R$ <?php echo number_format($saldo['saldo'], 2, ',', '.'); ?></span>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>

<?php } ?>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>