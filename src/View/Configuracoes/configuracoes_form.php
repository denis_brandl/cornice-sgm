<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">

			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="pill" href="#geral">Geral</a></li>
				<?php if ($this->usuario_master) { ?>
					<li class=""><a data-toggle="pill" href="#avancadas">Avançadas</a></li>
				<?php } ?>
			</ul>

			<div class="tab-content">
				<div id="geral" class="tab-pane fade in active">
					<div class="box">
						<form role="form" method="POST" action="<?php echo URL . 'Configuracoes/' . $acao . '/' . ($empresa->CodigoEmpresa ?: 0); ?>" enctype="multipart/form-data">
							<?php if (!empty($msg_sucesso)) { ?>
								<div class="callout callout-success">
									<h4>Sucesso!</h4>
									<p><?php echo $msg_sucesso; ?></p>
								</div>
							<?php } ?>
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Configurações</h3>
								</div>
							</div>

							<div class="box-body">
								<h4 class="box-title">Informações a serem exibidas nos pedidos: </h4>
								<div class="form-group col-xs-12 col-md-6">
									<label for="horario_funcionamento">Horário de funcionamento</label><br>
									<textarea class="form-control" placeholder="Segunda á sexta: 08:00 - 18:00 (sem fechar para o almoço). Sábado: 08:00 - 12:00" id="horario_funcionamento" name="horario_funcionamento" rows="5" cols="100" style="resize: none;" /><?php echo $empresa->horario_funcionamento; ?></textarea>
								</div>

								<div class="form-group col-xs-12 col-md-6">
									<label for="observacao_pedido">Observações</label><br>
									<textarea id="observacao_pedido" placeholder="Exemplo: Pedidos não retirados em 60 dias poderão ser vendidos pela molduraria" class="form-control" name="observacao_pedido" rows="5" cols="100" style="resize: none;" /><?php echo $empresa->observacao_pedido; ?></textarea>
								</div>

							</div>

							<div class="box-body">
								<h4 class="box-title">Whatsapp </h4>
								<div class="form-group col-xs-12 col-md-12">
									<label for="layout_mensagem_whatsapp">Layout da mensagem - Orçamento</label><br>
									<textarea id="layout_mensagem_whatsapp" name="layout_mensagem_whatsapp" class="form-control" placeholder="" rows="10" cols="100" /><?php echo $layout_mensagem_whatsapp; ?></textarea>
								</div>

								<!-- Mensagem ao mudar a situação -->
								<div class="form-group col-xs-12 col-md-12">
									<p style="font-weight:bold;">Mensagem ao mudar a situação</p>
									<?php foreach ($listaSituacoes as $situacao) { ?>
										<div class="col-md-12" style="margin-top:5px">
											<div class="col-md-2"><?php echo $situacao->descricao; ?></div>
											<div class="col-md-8"><input name="layout_alteracao_pedido_whatsapp[<?php echo $situacao->idSituacao; ?>]" type="text" value="<?php echo $situacao->notificacao; ?>" class="form-control"></div>
										</div>
									<?php } ?>
								</div>
							</div>

							<div class="box-body">
								<h4 class="box-title">Logotipo</h4>
								<p class="text-left">
									Tamanho máximo do arquivo: 100KB <br>
									Dimensão maxima da imagem sugerido: 180x180 <br>
									Tipo suportado: png <br>
								</p>
								<p class="alert alert-danger alertaArquivoInvalido" style="display:none;"></p>
								<div class="form-group col-xs-12 col-md-4">
									<input class="form-control" type="hidden" id="logotipoSelecionado" name="logotipoSelecionado" value="" />
									<div class="input-group-btn">
										<label for="logotipo" class="btn btn-default">Selecionar arquivo</label>
										<input id="logotipo" type="file" class="btn btn-default" style="visibility:hidden;" accept="image/x-png,image/gif,image/jpeg,image/png,image/jpg" />
									</div>

									<div class="form-group">
										<label style="font-weight: normal">
											Habilitar logo de impressão em escala de cinza
											<input type="checkbox" name="logo-impressao-cinza" class="pull-left" value="1" checked>
										</label>

									</div>
								</div>

								<div class="form-group col-xs-12 col-md-4">
									<div class="image-area mt-4">
										<img id="imageResult" src="#" alt="" class="img-fluid rounded shadow-sm mx-auto d-block">
									</div>
								</div>

							</div>
					</div>
				</div>

				<?php if ($this->usuario_master) { ?>
					<div id="avancadas" class="tab-pane fade">
						<div class="box">

							<?php
							$arrConfiguracoesOcultar = ['layout_mensagem_whatsapp'];
							foreach ($arrConfiguracoes as $configuracao) {
								if (in_array($configuracao['nome'], $arrConfiguracoesOcultar)) {
									continue;
								}

								echo '<div class="box-body">';

								if ($configuracao['tipo_campo'] == 'input') {
							?>
									<div class="form-group col-xs-12 col-md-3">
										<label><?php echo $configuracao['nome']; ?></label>
										<input name="configuracoesAvancadas[<?php echo $configuracao['nome']; ?>]" type="text" value="<?php echo $configuracao['valor']; ?>" class="form-control">
									</div>
								<?php 	}  ?>

								<?php if ($configuracao['tipo_campo'] == 'select') {  ?>

									<div class="form-group col-xs-12 col-md-3">
										<label for="observacao_pedido"><?php echo $configuracao['nome']; ?></label> <br>
										<select name="configuracoesAvancadas[<?php echo $configuracao['nome']; ?>]" class="form-control tipoCliente">
											<option value="true" <?php echo $configuracao['valor'] == 'true' ? 'selected' : ''; ?>>Sim</option>
											<option value="false" <?php echo $configuracao['valor'] == 'false' ? 'selected' : ''; ?>>Não</option>
										</select>
									</div>
								<?php
								}
								?>

								<?php if ($configuracao['tipo_campo'] == 'textarea') {  ?>

									<div class="form-group col-xs-12 col-md-3">
										<label for="observacao_pedido"><?php echo $configuracao['nome']; ?></label><br>
										<textarea id="<?php echo $configuracao['nome']; ?>" class="form-control" name="configuracoesAvancadas[<?php echo $configuracao['nome']; ?>]" rows="5" cols="100" style="resize: none;" /><?php echo $configuracao['valor']; ?></textarea>
									</div>
							<?php
								}
								echo '</div>';
							}

							?>


						</div>
					</div>

				<?php } ?>

				<div class="box-footer">
					<input type="hidden" name="handle" value="<?php echo $empresa->CodigoEmpresa; ?>">
					<button class="btn btn-primary" type="submit">Salvar</button>
					<a class="btn btn-primary" href="<?php URL; ?>?class=dashboard&acao=index">Voltar</a>
					<?php if ($this->usuario_master) { ?>
						<a class="btn btn-primary" href="<?php URL; ?>/Developer/downloadBase/">Download Base de dados</a>
					<?php } ?>
				</div>
				</form>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>