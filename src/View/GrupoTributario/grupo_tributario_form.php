<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>


<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <form autocomplete="off" role="form" method="POST"
          action="<?php echo URL; ?><?php echo $classe; ?>/<?php echo $metodo; ?>/<?php echo $grupo_tributarios[0]->id_grupo_tributario; ?>">
          <?php if (!empty($msg_sucesso)) { ?>
            <div class="callout callout-success">
              <h4>Sucesso!</h4>
              <p>
                <?php echo $msg_sucesso; ?>
              </p>
            </div>
          <?php } ?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Cadastro</h3>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group col-xs-12 col-md-9">
              <label>Descrição</label>
              <input class="form-control" type="text" value="<?php echo $grupo_tributarios[0]->nome; ?>" name="nome" />
            </div>

            <div class="form-group col-xs-12 col-md-3">
              <label>Origem</label>
              <select id="codigo_origem" name="codigo_origem" class="form-control">
                <?php foreach ($arrOrigens as $key => $value) {
                  $selected = $key === $grupo_tributarios[0]->codigo_origem ? 'selected' : '';
                  echo sprintf('
                        <option value="%s" %s>%1$s - %s </option>
                      ',
                    $key,
                    $selected,
                    $value
                  );
                } ?>
              </select>
            </div>

          </div>

          <div class="box-footer">
            <input type="hidden" name="handle" value="<?php echo $grupo_tributarios[0]->id_grupo_tributario; ?>">
            <button class="btn btn-primary" type="submit">Salvar</button>
            <a class="btn btn-primary" href="<?php echo URL . $classe; ?>/listar/">Voltar</a>
          </div>
        </form>
      </div>
    </div>
  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>