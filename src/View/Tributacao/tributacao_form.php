<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <form role="form" id="frmTributacao" method="POST"
          action="<?php echo URL; ?><?php echo $classe; ?>/<?php echo $metodo; ?>/<?php echo isset($arrTributacao['handle']) ? $arrTributacao['handle'] : 0; ?>">
          <?php if (!empty($msg_sucesso)) { ?>
            <div class="callout callout-success">
              <h4>Sucesso!</h4>
              <p>
                <?php echo $msg_sucesso; ?>
              </p>
            </div>
          <?php } ?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Cadastro</h3>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group col-xs-12 col-md-4">
              <label>Operação Fiscal</label>
              <select name="id_operacao_fiscal" id="id_operacao_fiscal" class="form-control">
                <option value="">Selecione</option>
                <?php foreach ($arrOperacaoFiscal as $operacao) {
                  $selected = "";
                  if ($operacao->id_operacao_fiscal == $arrTributacao['id_operacao_fiscal']) {
                    $selected = ' selected="selected"';
                  }
                  ?>
                  <option value="<?php echo $operacao->id_operacao_fiscal; ?>" <?php echo $selected; ?>><?php echo $operacao->descricao; ?></option>
                <?php } ?>
              </select>
            </div>

            <div class="form-group col-xs-12 col-md-4">
              <label>Estado</label>
              <select name="id_estado" id="id_estado" class="form-control">
                <option value="">Selecione</option>
                <?php foreach ($arrEstados as $estado) {
                  $selected = "";
                  if ($estado->codigo_uf == $arrTributacao['id_estado']) {
                    $selected = ' selected="selected"';
                  }
                  ?>
                  <option value="<?php echo $estado->codigo_uf; ?>" <?php echo $selected; ?>><?php echo $estado->nome; ?>
                  </option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="box-body">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="pill" href="#geral">Geral</a></li>
              <li><a data-toggle="pill" href="#icms">ICMS</a></li>
              <li><a data-toggle="pill" href="#pis">PIS</a></li>
              <li><a data-toggle="pill" href="#ipi">IPI</a></li>
              <li><a data-toggle="pill" href="#confins">CONFINS</a></li>
            </ul>

            <div class="tab-content">
              <!--- GERAL -->
              <div id="geral" class="tab-pane fade  in active">
                <div class="box">
                  <table class="table table-responsive">
                    <tr>
                      <th>Grupo Tributário</th>
                      <th colspan="8">Configuração da Tributação</th>
                    </tr>
                    <tr>
                      <td>Descrição do Grupo Tributário</td>
                      <?php
                      foreach ($this->arrConfiguracaoGlobal as $tributacao) {
                        echo sprintf('<td>%s<p class="small">%s</p></td>', $tributacao['sigla'], $tributacao['descricao']);
                      }
                      ?>
                    </tr>

                    <?php
                    foreach ($arrGrupoTributario as $grupo) {
                      ?>
                      <tr>
                        <td>
                          <?php echo sprintf('%s - %s ', $grupo->codigo, $grupo->nome); ?>
                        </td>
                        <?php
                        foreach ($this->arrConfiguracaoGlobal as $tributacao) {
                          if (isset($tributacao['opcoes'])) {
                            $dropdown_tributo = sprintf('<select name="%s" id="%1$s" class="form-control"><option value="">Selecione...</option>', $tributacao['codigo']);
                            foreach ($tributacao['opcoes'] as $key => $value) {
                              $dropdown_tributo .= sprintf('<option value="%s">%1$s - %s</option>', $key, $value);
                            }
                            $dropdown_tributo .= '</select>';
                            echo sprintf('<td>%s</td>', $dropdown_tributo);
                          } else {
                            $valor_tributo = isset($tributacao['valorPadrao']) ? $tributacao['valorPadrao'] : '';
                            if (isset($arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario][$tributacao['codigo']])) {
                              $valor_tributo = ($arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario][$tributacao['codigo']]);
                            }
                            echo sprintf('<td><input name="tributacaoGeral[%s][%s]" class="form-control" type="text" value="%s"></td>', $grupo->id_grupo_tributario, $tributacao['codigo'], $valor_tributo);
                          }
                        }
                        ?>
                      </tr>
                      <?php
                    }
                    ?>
                  </table>
                </div>
              </div>
              <!-- FIM GERAL -->              
              <!--- ICMS -->
              <div id="icms" class="tab-pane fade">
                <div class="box">
                  <table class="table table-responsive">
                    <tr>
                      <th>Grupo Tributário</th>
                      <th colspan="8">Configuração da Tributação</th>
                    </tr>
                    <tr>
                      <td>Descrição do Grupo Tributário</td>
                      <?php
                      foreach ($this->arrTributacaoICMS as $tributacao_icms) {
                        echo sprintf('<td>%s<p class="small">%s</p></td>', $tributacao_icms['sigla'], $tributacao_icms['descricao']);
                      }
                      ?>
                    </tr>

                    <?php
                    foreach ($arrGrupoTributario as $grupo) {
                      ?>
                      <tr>
                        <td>
                          <?php echo $grupo->nome; ?>
                        </td>
                        <?php
                        foreach ($this->arrTributacaoICMS as $tributacao_icms) {
                          $valor_inserido = isset($arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario][$tributacao_icms['codigo']]) ? $arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario][$tributacao_icms['codigo']] : 0;
                          if (isset($tributacao_icms['opcoes'])) {
                            $dropdown_tributo = sprintf('<select name="tributacaoIcms[%s][%s]" id="%1$s" class="form-control"><option value="">Selecione...</option>', $grupo->id_grupo_tributario, $tributacao_icms['codigo']);
                            foreach ($tributacao_icms['opcoes'] as $key => $value) {
                              $selected = '';
                              if ($valor_inserido == $key) {
                                $selected = 'selected';
                              }                              
                              $dropdown_tributo .= sprintf('<option value="%s" %s>%1$s - %s</option>', $key, $selected, $value);
                            }
                            $dropdown_tributo .= '</select>';
                            echo sprintf('<td>%s</td>', $dropdown_tributo);
                          } else {                          
                            $valor_tributo = isset($tributacao_icms['valorPadrao']) ? $tributacao_icms['valorPadrao'] : '';
                            if ($valor_inserido) {
                              $valor_tributo = ($arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario][$tributacao_icms['codigo']]);
                            }
                            echo sprintf('<td><input name="tributacaoIcms[%s][%s]" class="form-control" type="text" value="%s"></td>', $grupo->id_grupo_tributario, $tributacao_icms['codigo'], $valor_tributo);
                          }
                        }
                        ?>
                      </tr>
                      <?php
                    }
                    ?>
                  </table>
                </div>
              </div>
              <!-- FIM ICMS -->

              <!--- IPI -->
              <div id="ipi" class="tab-pane fade">
                <div class="box">
                  <table class="table table-responsive">
                    <tr>
                      <th>Grupo Tributário</th>
                      <th colspan="8">Configuração da Tributação</th>
                    </tr>
                    <tr>
                      <td>Descrição do Grupo Tributário</td>
                      <?php
                      foreach ($this->arrTributacaoIPI as $tributacao_ipi) {
                        echo sprintf('<td>%s<p class="small">%s</p></td>', $tributacao_ipi['sigla'], $tributacao_ipi['descricao']);
                      }
                      ?>
                    </tr>

                    <?php
                    foreach ($arrGrupoTributario as $grupo) {
                      ?>
                      <tr>
                        <td>
                          <?php echo $grupo->nome; ?>
                        </td>
                        <?php
                        foreach ($this->arrTributacaoIPI as $tributacao_ipi) {
                          if (isset($tributacao_ipi['opcoes'])) {
                            $dropdown_tributo = sprintf('<select name="tributacaoIpi[%s][%s]" id="%1$s" class="form-control"><option value="">Selecione...</option>', $grupo->id_grupo_tributario, $tributacao_ipi['codigo']);
                            foreach ($tributacao_ipi['opcoes'] as $key => $value) {
                              $selected = '';
                              if (isset($arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario]['ipi'][$tributacao_ipi['codigo']]) && $arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario]['ipi'][$tributacao_ipi['codigo']] == $key ) {
                                $selected = 'selected';
                              }
                              $dropdown_tributo .= sprintf('<option value="%s" %s>%1$s - %s</option>', $key, $selected, $value);
                            }
                            $dropdown_tributo .= '</select>';
                            echo sprintf('<td>%s</td>', $dropdown_tributo);
                          } else {
                            $valor_tributo = '0';
                            if (isset($arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario]['ipi'][$tributacao_ipi['codigo']])) {
                              $valor_tributo = ($arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario]['ipi'][$tributacao_ipi['codigo']]);
                            }
                            echo sprintf('<td><input name="tributacaoIpi[%s][%s]" class="form-control" type="text" value="%s"></td>', $grupo->id_grupo_tributario, $tributacao_ipi['codigo'], $valor_tributo);
                          }
                        }
                        ?>
                      </tr>
                      <?php
                    }
                    ?>
                  </table>
                </div>
              </div>
              <!-- FIM IPI -->

              <!--- PIS -->
              <div id="pis" class="tab-pane fade">
                <div class="box">
                  <table class="table table-responsive">
                    <tr>
                      <th>Grupo Tributário</th>
                      <th colspan="8">Configuração da Tributação</th>
                    </tr>
                    <tr>
                      <td>Descrição do Grupo Tributário</td>
                      <?php
                      foreach ($this->arrTributacaoPIS as $tributacao_pis) {
                        echo sprintf('<td>%s<p class="small">%s</p></td>', $tributacao_pis['sigla'], $tributacao_pis['descricao']);
                      }
                      ?>
                    </tr>

                    <?php
                    foreach ($arrGrupoTributario as $grupo) {
                      ?>
                      <tr>
                        <td>
                          <?php echo $grupo->nome; ?>
                        </td>
                        <?php
                        foreach ($this->arrTributacaoPIS as $tributacao_pis) {
                          if (isset($tributacao_pis['opcoes'])) {
                            $dropdown_tributo = sprintf('<select name="tributacaoPis[%s][%s]" id="%1$s" class="form-control"><option value="">Selecione...</option>', $grupo->id_grupo_tributario, $tributacao_pis['codigo']);
                            foreach ($tributacao_pis['opcoes'] as $key => $value) {
                              $dropdown_tributo .= sprintf('<option value="%s">%1$s - %s</option>', $key, $value);
                            }
                            $dropdown_tributo .= '</select>';
                            echo sprintf('<td>%s</td>', $dropdown_tributo);
                          } else {
                            $valor_tributo = '0';
                            if (isset($arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario]['pis'][$tributacao_pis['codigo']])) {
                              $valor_tributo = ($arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario]['pis'][$tributacao_pis['codigo']]);
                            }
                            echo sprintf('<td><input name="tributacaoPis[%s][%s]" class="form-control" type="text" value="%s"></td>', $grupo->id_grupo_tributario, $tributacao_pis['codigo'], $valor_tributo);
                          }
                        }
                        ?>
                      </tr>
                      <?php
                    }
                    ?>
                  </table>
                </div>
              </div>
              <!-- FIM PIS -->     
              
              <!--- CONFINS -->
              <div id="confins" class="tab-pane fade">
                <div class="box">
                  <table class="table table-responsive">
                    <tr>
                      <th>Grupo Tributário</th>
                      <th colspan="8">Configuração da Tributação</th>
                    </tr>
                    <tr>
                      <td>Descrição do Grupo Tributário</td>
                      <?php
                      foreach ($this->arrTributacaoConfins as $tributacao) {
                        echo sprintf('<td>%s<p class="small">%s</p></td>', $tributacao['sigla'], $tributacao['descricao']);
                      }
                      ?>
                    </tr>

                    <?php
                    foreach ($arrGrupoTributario as $grupo) {
                      ?>
                      <tr>
                        <td>
                          <?php echo $grupo->nome; ?>
                        </td>
                        <?php
                        foreach ($this->arrTributacaoConfins as $tributacao) {
                          if (isset($tributacao['opcoes'])) {
                            $dropdown_tributo = sprintf('<select name="%s" id="%1$s" class="form-control"><option value="">Selecione...</option>', $tributacao['codigo']);
                            foreach ($tributacao['opcoes'] as $key => $value) {
                              $dropdown_tributo .= sprintf('<option value="%s">%1$s - %s</option>', $key, $value);
                            }
                            $dropdown_tributo .= '</select>';
                            echo sprintf('<td>%s</td>', $dropdown_tributo);
                          } else {
                            $valor_tributo = isset($tributacao['valorPadrao']) ? $tributacao['valorPadrao'] : '0';
                            if (isset($arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario]['confins'][$tributacao['codigo']])) {
                              $valor_tributo = ($arrTributacao['grupos_tributarios'][$grupo->id_grupo_tributario]['confins'][$tributacao['codigo']]);
                            }
                            echo sprintf('<td><input name="tributacaoConfins[%s][%s]" class="form-control" type="text" value="%s"></td>', $grupo->id_grupo_tributario, $tributacao['codigo'], $valor_tributo);
                          }
                        }
                        ?>
                      </tr>
                      <?php
                    }
                    ?>
                  </table>
                </div>
              </div>
              <!-- FIM CONFINS -->              
            </div>
          </div>

          <div class="box-footer">
            <input type="hidden" name="handle"
              value="<?php echo isset($arrTributacao['handle']) ? $arrTributacao['handle'] : 0; ?>">
            <button class="btn btn-primary" id="salvarTributacao" type="submit">Salvar</button>
            <a class="btn btn-primary" href="<?php echo URL . $classe; ?>/listar/">Voltar</a>
          </div>
        </form>
      </div>
    </div>
  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>
