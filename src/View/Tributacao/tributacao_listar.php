<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>


<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h5 class="box-title">Relacionamento entre a operação fiscal e o grupo tributário.</h5>
        </div>
        <div class="box-body">
          <div id="componentes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
              <div class="col-sm-6">
                <a class="btn btn-primary" href="<?php echo URL . $classe . '/cadastrar/'; ?>">Cadastrar Novo</a>
              </div>
              <div class="col-sm-6">
                <div id="example1_filter" class="dataTables_filter">
                  <form autocomplete="off" name="frmCliente" id="frmBuscar">
                    <label>
                      Buscar:
                      <select name="coluna" class="form-control">
                        <?php foreach ($arrCamposBusca as $value => $descricao) {
                          $selected = '';
                          if ($value == $coluna) {
                            $selected = 'selected';
                          }
                        ?>
                          <option value="<?php echo $value; ?>" <?php echo $selected; ?>><?php echo $descricao; ?></option>
                        <?php } ?>
                      </select>
                      <input class="form-control input-sm" type="search" placeholder="" value="<?php echo $buscar; ?>" name="buscar" aria-controls="example1">
                      <input type="button" class="btn btn-primary" id="btnBuscar" value="Buscar" />
                    </label>
                  </form>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                  <thead>
                    <tr role="row">
                      <th>Operação Fiscal</th>
                      <th>Estado Destino</th>
                      <th>Editar</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $aux = 0;
                    foreach ($tributacoes as $tributacao) {
                      $handle_tributacao = sprintf('%s_%s', $tributacao->id_operacao_fiscal, $tributacao->id_estado);
                      if ($aux & 1) {
                    ?>
                        <tr class="odd" role="row">
                        <?php } else { ?>
                        <tr class="even" role="row">
                        <?php } ?>
                        <td>
                          <a class="" href="<?php echo URL . $classe . '/editar/' . $handle_tributacao; ?>">
                            <?php echo $tributacao->descricao; ?>
                          </a>
                        </td>
                        <td>
                          <a class="" href="<?php echo URL . $classe . '/editar/' . $handle_tributacao; ?>">
                            <?php echo $tributacao->nome; ?>
                          </a>
                        </td>
                        <td><a class="" href="<?php echo URL . $classe . '/editar/' . $handle_tributacao; ?>"><i class="fa fa-edit"></i>Editar</a></td>
                        </tr>
                      <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>