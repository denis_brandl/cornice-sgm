<?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"></h3>		
					</div>
					<?php if (isset($msg_sucesso) && !empty($msg_sucesso)) { ?>
							<div class="callout <?= $tipo_mensagem;?>">
								<p><?= $msg_sucesso; ?></p>
							</div>
					<?php } ?>					
					<div class="box-body">
						<div id="componentes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
							<div class="row">
								<div class="col-sm-6">
									<a class="btn btn-primary" href="<?php echo URL.$classe.'/cadastrar/';?>">Cadastrar Novo</a>
								</div>
								<div class="col-sm-6">
									<div id="example1_filter" class="dataTables_filter">
										<label>
											<form autocomplete="off"  name="frmCliente" id="frmBuscar">
											Buscar:												
												<select name="coluna" class="form-control">
													<?php foreach ($arrCamposBusca as $value => $descricao) { 
															$selected = '';
															if ($value == $coluna) {
																$selected = 'selected';
															}
													?>
														<option value="<?php echo $value;?>" <?php echo $selected;?>><?php echo $descricao;?></option>
													<?php } ?>
												</select>
												<input class="form-control input-sm" type="search" placeholder="" value="<?php echo $buscar;?>" name="buscar" aria-controls="example1">
												<input type="button" class="btn btn-primary" id="btnBuscar" value="Buscar" />
											</form>
										</label>
									</div>
								</div>								
							</div>
							<div class="row">
								<div class="col-sm-12">
									<table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Descrição</th>
												<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 207px;" aria-label="Browser: activate to sort column ascending">Custo</th>
												<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 207px;" aria-label="Browser: activate to sort column ascending">Editar</th>
												<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 207px;" aria-label="Browser: activate to sort column ascending">Excluir</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												$aux = 0;
												foreach ($componentes as $componente) {
												if ($aux & 1) {
											?>
												<tr class="odd" role="row">
											<?php } else { ?>	
												<tr class="even" role="row">
											<?php } ?>	
												<td class="sorting_1"><?php echo $componente->DESCRICAO; ?></td>
												<td class="sorting_1">R$ <?php echo number_format($componente->CUSTO,2); ?></td>
												<td><a class="btn btn-app" href="<?php echo URL.$classe.'/editar/'.$componente->IDCOMPONENTE;?>"><i class="fa fa-edit"></i>Editar</a></td>
												<?php
													if ($componente->qtdUso == 0) {
												?>
													<td><a class="btn btn-app excluirComponente" componenteId="<?= $componente->IDCOMPONENTE;?>"  href="#"><i class="fa fa-trash"></i>Excluir</a></td>
												<?php
													} else {
												?>
													<td><a class="btn btn-app naoExcluirComponente" style="opacity: 0.4;"><i class="fa fa-trash"></i>Excluir</a></td>
												<?php
													}
												?>												
											</tr>
											<?php } ?>
										</tbody>
										<tfoot>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	<?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  