<?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<form autocomplete="off"  role="form" method="POST" action="<?php echo URL.'Componente/'.$acao.'/'.($componente->IDCOMPONENTE ?: 0);?>">
						<?php if (!empty($msg_sucesso)) { ?>
							<div class="callout callout-success">
								<h4>Sucesso!</h4>
								<p><?php echo $msg_sucesso;?></p>
							</div>
						<?php } ?>
						<div class="box-body">
              <div class="box-body">
                <div class="form-group col-xs-12 col-md-3">
                  <label>Descrição</label>
                  <input class="form-control" type="text" value="<?php echo isset($componente->DESCRICAO) ? $componente->DESCRICAO : '' ;?>" name="descricao" />
                </div>
              </div>
              <div class="box-body">
                <div class="form-group col-xs-12 col-md-3">
                  <label>Custo</label>
                  <div class="input-group">
                    <span class="input-group-addon">R$</span>
                    <input class="form-control numberOnly campoMonetario" type="text" name="custo" value="<?php echo isset($componente->CUSTO) ? number_format($componente->CUSTO,2,',','.') : 0.00;?>">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group col-xs-12 col-md-3">
                  <label>Margem bruta de venda</label>
                  <div class="input-group">
                    <span class="input-group-addon">%</span>
                    <input class="form-control numberOnly campoMonetario" type="text" name="margem_venda" value="<?php echo isset($componente->MARGEM_VENDA) ? number_format($componente->MARGEM_VENDA,2,',','.') : 0.00;?>">
                  </div>
                </div>
              </div>
						</div>
						<div class="box-footer">
							<input type="hidden" name="handle" value="<?php echo isset($componente->IDCOMPONENTE) ? $componente->IDCOMPONENTE : '';?>">
							<button class="btn btn-primary" type="submit">Salvar</button>
							<a class="btn btn-primary" href="<?php echo URL;?>Componente/listar/">Voltar</a>
						</div>
					</form>
				</div>
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  