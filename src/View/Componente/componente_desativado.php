<?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<h2 class="box-title">O cadastro de componentes foi vinculado ao cadastro de Produtos</h2>		
					</div>
					
					<div class="box-body">
							<div class="row">
								<div class="col-sm-12">
									O cadastro de componentes deve ser feito diretamente em 
									<a href="<?php echo URL.'Produto/listar/';?>">Administração > Produtos</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	<?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  
