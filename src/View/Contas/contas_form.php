
      <?php include_once DOCUMENT_ROOT . "src/View/Common/cabecalho.php"; ?>
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <form method="POST" action="<?php echo URL . $classe . "/" . $metodo . "/" . ($Contas->id ?: 0); ?>">
                  <?php if (!empty($msg_sucesso)) { ?>
                    <div class="callout callout-success">
                      <h4>Sucesso!</h4>
                      <p><?php echo $msg_sucesso; ?></p>
                    </div>
                  <?php } ?>
                  <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Cadastro de conta</h3>
                    </div>
                  </div>
                  <div class="box-body">
                    
                  </div>
                  <div class="box-body">
                    <div class="form-group col-xs-12 col-md-2">
                      <label>Nome</label>
                      <input class="form-control" type="text" name="nome" value="<?php echo $Contas->nome; ?>">
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="form-group col-xs-12 col-md-2">
                      <label>Tipo</label>
                      <select name="idTipo" class="form-control">
                        <option <?php echo $Contas->idtipo == 0 ? 'selected':''; ?> value="0">Entrada</option>
                        <option <?php echo $Contas->idtipo == 1 ? 'selected':''; ?> value="1">Saída</option>
                      </select>
                    </div>
                  </div>                  
                  <div class="box-footer">
                    <input type="hidden" name="handle" value="<?php echo $Contas->id; ?>">
                    <button class="btn btn-primary" type="submit">Salvar</button>
                    <a class="btn btn-primary" href="<?php echo URL . $classe . '/Listar/'; ?>">Voltar</a>
                  </div>                
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include_once DOCUMENT_ROOT . 'src/View/Common/rodape.php'; ?>  
    