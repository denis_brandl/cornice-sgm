
      <?php include_once(DOCUMENT_ROOT."src/View/Common/cabecalho.php"); ?>
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <form method="POST" action="<?php echo URL.$classe."/".$metodo."/".($Category->CategoryId ?: 0);?>">
                  <?php if (!empty($msg_sucesso)) { ?>
                    <div class="callout callout-success">
                      <h4>Sucesso!</h4>
                      <p><?php echo $msg_sucesso;?></p>
                    </div>
                  <?php } ?>
                  <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Cadastro de categoria</h3>
                    </div>
                  </div>                  
                  <div class="box-body hide">
                    <div class="form-group col-xs-12 col-md-2">
                      <label>UserId</label>
                      <input class="form-control" type="text" name="userid" value="1">
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="form-group col-xs-12 col-md-2">
                      <label>Nome</label>
                      <input class="form-control" type="text" name="CategoryName" value="<?php echo $Category->CategoryName;?>">
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="form-group col-xs-12 col-md-2">
                      <label>Descrição</label>
                      <textarea class="form-control" name="complemento"><?php echo $Category->complemento;?></textarea>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="form-group col-xs-12 col-md-2">
                      <label>Conta</label>
                      <select name="IDCONTA">
                        <optgroup label="Receitas">
                        <?php
                          foreach ($this->arrReceitas as $conta) {
                            echo sprintf('<option %s value="%s">%s</option>', $conta->id == $Category->IDCONTA ? 'selected' : '', $conta->id, $conta->nome);
                          }
                        ?>
                        </optgroup>

                        <optgroup label="Despesas">
                        <?php
                          foreach ($this->arrDespesas as $conta) {
                            echo sprintf('<option %s value="%s">%s</option>', $conta->id == $Category->IDCONTA ? 'selected' : '', $conta->id, $conta->nome);
                          }
                        ?>
                        </optgroup>                        

                      </select>                      
                    </div>
                  </div>                  

                  <div class="box-footer">
                    <input type="hidden" name="handle" value="<?php echo $Category->CategoryId;?>">
                    <button class="btn btn-primary" type="submit">Salvar</button>
                    <a class="btn btn-primary" href="<?php echo URL.$classe.'/Listar/';?>">Voltar</a>
                  </div>                
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  
    