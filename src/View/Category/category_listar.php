<?php include_once(DOCUMENT_ROOT."src/View/Common/cabecalho.php"); ?>  
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
        <div class="box-body">
          <div id="category-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
              <div class="col-sm-6">
                <a class="btn btn-primary" href="<?php echo URL.$classe.'/cadastrar/';?>">Cadastrar Novo</a>
              </div>
              <div class="col-sm-6">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <table class="table table-bordered table-striped dataTable">
                  <thead>
                    <tr role="row"> 
                      <th>Nome</th>
                      <th>Descrição</th>
                      <th>Conta</th>
                      <th>Tipo</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      $aux = 0;
                      foreach ($arrCategory as $Category) {
                        $indice_conta = array_search($Category->IDCONTA, array_column($this->contas, 'id'));
                      if ($aux & 1) {
                    ?>
                      <tr class="odd" role="row">
                    <?php } else { ?>	
                      <tr class="even" role="row">
                    <?php } ?>
                      <td><?php echo $Category->CategoryName; ?></td>
                      <td><?php echo $Category->complemento; ?></td>
                      <td><?php echo $this->contas[$indice_conta]->nome; ?></td>
                      <td><?php echo $this->contas[$indice_conta]->idtipo == 0 ? 'Receita' : 'Despesa'; ?></td>
                      <td><a class="" href="<?php echo URL.$classe.'/'.$acao.'/'. $Category->CategoryId; ?>"><i class="fa fa-edit"></i>Editar</a></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>									
              </div>								
            </div>                
          </div>
        </div>
      </div>
      </div>
    </div>
  </section>
</div>
<?php include_once(DOCUMENT_ROOT."src/View/Common/rodape.php"); ?>  
