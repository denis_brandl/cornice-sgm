<?php include_once(DOCUMENT_ROOT . "src/View/Common/cabecalho.php"); ?>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <form method="POST" id="frmCadastroContaReceber" action="<?php echo URL . $classe . "/" . $metodo . "/" . ($Assets->AssetsId ?: 0); ?>">
          <?php if (!empty($msg_sucesso)) { ?>
            <div class="callout callout-success">
              <h4>Sucesso!</h4>
              <p>
                <?php echo $msg_sucesso; ?>
              </p>
            </div>
          <?php } ?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
          </div>

          <div class="box-body">
            <div class="form-group col-xs-12 col-md-2">
              <label>Data Lançamento</label>
              <?php $dataLancamento = $Assets->Date != '0000-00-00 00:00:00' ? date('d/m/Y', strtotime($Assets->Date)) : date('d/m/Y'); ?>
              <div class="input-group date">
                <input class="form-control data" type="text" name="date" value="<?php echo $dataLancamento; ?>">
              </div>
            </div>
            <?php if ($arrPermissoesCabecalho[$permiteSalvarDataVencimento]->cadastrar == '1') { ?>
              <div class="form-group col-xs-12 col-md-2">
                <label>Data de vencimento</label>
                <?php $dataVencimento = $Assets->cdtvencimento != '0000-00-00 00:00:00' ? date('d/m/Y', strtotime($Assets->cdtvencimento)) : ''; ?>
                <div class="input-group date">
                  <input class="form-control data" type="text" name="cdtvencimento" value="<?php echo $dataVencimento; ?>">
                </div>
              </div>
            <?php } ?>

            <div class="form-group col-xs-12 col-md-2">
              <label>Data de pagamento Loja</label>
              <?php $dataPagamentoLoja = $Assets->data_pagamento_loja != '' ? date('d/m/Y', strtotime($Assets->data_pagamento_loja)) : ''; ?>
              <div class="input-group date">
                <input class="form-control data" disabled value="<?php echo $dataPagamentoLoja; ?>">
              </div>
            </div>



            <?php if ($arrPermissoesCabecalho[$permiteSalvarDataPagamento]->cadastrar == '1') { ?>
              <div class="form-group col-xs-12 col-md-2">
                <label>Data de pagamento</label>
                <?php $dataPagamento = $Assets->cdtpagamento != '0000-00-00 00:00:00' ? date('d/m/Y', strtotime($Assets->cdtpagamento)) : ''; ?>
                <div class="input-group date">
                  <input id="cdtpagamento" class="form-control data" type="text" name="cdtpagamento" value="<?php echo $dataPagamento; ?>">
                </div>
              </div>
            <?php } ?>

          </div>

          <div class="box-body">
            <div class="form-group col-xs-12 col-md-2">
              <label>Categoria</label>
              <div class="input-group col-md-12">
                <select class="form-control" name="CategoryId" id="CategoryId">
                  <?php
                  foreach ($this->categoriasContasReceber as $categoria) {
                    echo sprintf('<option %s value="%s">%s</option>', $categoria->CategoryId == $Assets->CategoryId ? 'selected' : '',  $categoria->CategoryId, $categoria->CategoryName);
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group col-xs-12 col-md-2">
              <label>Cliente</label>
              <select class="form-control" name="Title" id="Title">
                <option value="">Selecione</option>
                <?php
                foreach ($this->clientes as $cliente) {
                  echo sprintf('<option %s value="%s">%s</option>', $cliente->CodigoCliente == $Assets->Title ? 'selected' : '', $cliente->CodigoCliente, $cliente->RazaoSocial);
                }
                ?>
              </select>
            </div>
            <div class="form-group col-xs-12 col-md-2">
              <label>Número do pedido</label>
              <select class="form-control" id="ndoc" name="ndoc">
                <?php if ((int) $Assets->ndoc > 0) {
                  echo sprintf('<option data-valor="%s" value="%1$s">%1$s</option>', $Assets->ndoc);
                } else {
                  echo '<option value="">Selecione um cliente</option>';
                }
                ?>
              </select>
            </div>
          </div>
          <div class="box-body">

            <div class="form-group col-xs-12 col-md-2">
              <label>Empresa</label>
              <select class="form-control" name="UserId">
                <?php
                foreach ($this->empresas as $empresa) {
                  echo sprintf('<option %s value="%s">%s</option>', $empresa->CodigoEmpresa == $Assets->UserId ? 'selected' : '', $empresa->CodigoEmpresa, $empresa->RazaoSocial);
                }
                ?>
              </select>
            </div>

            <div class="form-group col-xs-12 col-md-2">
              <label>Conta</label>
              <select class="form-control" name="AccountId" id="AccountId">
                <?php
                foreach ($this->contas as $conta) {
                  echo sprintf('<option %s value="%s">%s</option>', $conta->AccountId == $Assets->AccountId ? 'selected' : '', $conta->AccountId, $conta->AccountName);
                }
                ?>
              </select>
            </div>

            <div class="form-group col-xs-12 col-md-2">
              <label>Valor</label>
              <div class="input-group">
                <span class="input-group-addon">R$</span>
                <input class="form-control campoMonetario" type="text" id="Amount" name="Amount" value="<?php echo number_format($Assets->Amount, 2, ',', '.'); ?>">
              </div>
            </div>

          </div>

          <?php if ($Assets->AssetsId == 0) { ?>
            <div class="box-body">
              <div class="form-group col-xs-12 col-md-2">
                <label>Ocorrência (Ùnica ou Recorrente)</label>
                <div class="input-group">
                  <select class="form-control" id="tipoRecebimento" name="tipoRecebimento">
                    <option value="U" selected="">Única</option>
                    <option value="W">Semanal</option>
                    <option value="M">Mensal</option>
                    <option value="T">Trimestral</option>
                    <option value="S">Semestral</option>
                    <option value="A">Anual</option>
                    <option value="P">Parcelada</option>
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-2" id="divDiaVencimentoSemana" style="display:none;">
                <label>Dia do vencimento na semana</label>
                <div class="input-group">
                  <select name="diaVencimentoSemana" id="diaVencimentoSemana" class="form-control">
                    <option value="">Selecione</option>
                    <option value="0">Domingo</option>
                    <option value="1">Segunda-feira</option>
                    <option value="2">Terça-feira</option>
                    <option value="3">Quarta-feira</option>
                    <option value="4">Quinta-feira</option>
                    <option value="5">Sexta-feira</option>
                    <option value="6">Sábado</option>
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-2" id="divDiaVencimento" style="display:none;">
                <label>Dia do vencimento</label>
                <div class="input-group">
                  <input class="form-control" type="text" id="diaVencimento" name="diaVencimento" value="">
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-2" id="divQuantidadeRepeticoes" style="display:none;">
                <label>Quantidade de repetições</label>
                <div class="input-group">
                  <input class="form-control" type="text" id="quantidadeRepeticao" name="quantidadeRepeticao" value="">
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-2" id="divQuantidadeParcelas" style="display:none;">
                <label>Parcelas</label>
                <div class="input-group">
                  <input class="form-control" type="text" id="quantidadeParcelas" name="quantidadeParcelas" value="">
                </div>
              </div>
            </div>
          <?php } else { ?>
            <input type="hidden" name="tipoRecebimento" value="U">
          <?php } ?>

          <div class="box-body">
            <div class="form-group col-xs-12 col-md-6">
              <label>Observação</label>
              <textarea class="form-control" rows="10" name="description"><?php echo $Assets->Description; ?></textarea>
            </div>

            <div class="col-sm-12 col-md-1">
              <div class="form-group">
                <label>Situação</label>
                <select name="situacao" id="situacao" class="form-control">
                  <option value="0" <?php echo $Assets->situacao == 0 ? 'selected' : ''; ?>>Em aberto</option>
                  <option value="1" <?php echo $Assets->situacao == 1 ? 'selected' : ''; ?>>Pago</option>
                </select>
              </div>
            </div>
          </div>

          <div class="box-footer">
            <input type="hidden" name="handle" value="<?php echo $Assets->AssetsId; ?>">
            <button class="btn btn-primary" type="submit">Salvar</button>

            <?php if ($this->modelo_empresa == 'gestao-interna') { ?>
              <td>
                <?php if (!is_null($Assets->id_fila)) { ?>
                  <a class="btn btn-info" href="<?php echo URL . 'IntegraNotaServico/downloadNota/' . $Assets->id_documento ?>" target="_blank"><i class="fa fa-money"></i> Baixar NFS-e</a>
                <?php } else { ?>
                  <a class="btn btn-info emitirNotaServicoContaReceber" data-handle-conta-receber="<?php echo $Assets->AssetsId; ?>" href="#"><i class="fa fa-money"></i> Emitir NFS-e</a>
                <?php } ?>
              </td>
            <?php } ?>

            <a class="btn btn-primary float-right" href="<?php echo URL . $classe . '/Listar/'; ?>">Voltar</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
</div>
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>