<?php include_once(DOCUMENT_ROOT . "src/View/Common/cabecalho.php"); ?>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Filtros</h3>
        </div>

        <div class="box-body">
          <form id="frmFiltroFinanceiroRecebimentos" method="POST" action="<?php echo URL . $classe . "/listar/"; ?>">
            <div class="row">
              <div class="form-group col-md-1 col-sm-12">
                <label for="CategoryId">Categoria</label>
                <select class="form-control" id="CategoryId" name="CategoryId">
                  <option value="">Todos</option>
                  <?php
                  foreach ($this->categoriasContasReceber as $categoria) {
                    echo sprintf('<option %s value="%s">%s</option>', $categoria->CategoryId == $this->objCommon->validatePost('CategoryId') ? 'selected' : '',  $categoria->CategoryId, $categoria->CategoryName);
                  }
                  ?>
                </select>
              </div>
              <div class="form-group col-md-3 col-sm-12">
                <label for="Title">Cliente</label>
                <select class="form-control" id="Title" name="Title">
                  <option value="">Todos</option>
                  <?php
                  foreach ($this->clientes as $cliente) {
                    echo sprintf('<option %s value="%s">%s</option>', $cliente->CodigoCliente == $Assets->Title ? 'selected' : '', $cliente->CodigoCliente, $cliente->RazaoSocial);
                  }
                  ?>
                </select>
              </div>
              <div class="form-group col-md-2 col-sm-12">
                <label for="UserId">Empresa</label>
                <select class="form-control" name="UserId" id="UserId">
                  <option value="">Todos</option>
                  <?php
                  foreach ($this->empresas as $empresa) {
                    echo sprintf('<option %s value="%s">%s</option>', $empresa->CodigoEmpresa == $this->objCommon->validatePost('UserId') ? 'selected' : '', $empresa->CodigoEmpresa, $empresa->RazaoSocial);
                  }
                  ?>
                </select>
              </div>


              <div class="form-group col-md-3 col-sm-12">
                <label for="criado_por">Vendedor</label>
                <select class="form-control" id="vendedor" name="vendedor">
                  <option value="">Todos</option>
                  <?php
                  foreach ($this->usuarios as $usuario) {
                    echo sprintf(
                      '<option %s value="%s">%s</option>',
                      $usuario->CodigoUsuario == $this->objCommon->validatePost('vendedor') ? 'selected' : '',
                      $usuario->CodigoUsuario,
                      $usuario->NomeUsuario
                    );
                  }
                  ?>
                </select>
              </div>

              <div class="form-group col-md-1 col-sm-12">
                <label for="situacao">Situação</label>
                <select class="form-control" name="situacao" id="situacao">
                  <option value="">Todos</option>
                  <option value="0" <?php echo (isset($_POST['situacao']) && $_POST['situacao'] == 0) ? 'selected' : ''; ?>>Em aberto</option>
                  <option value="1" <?php echo (isset($_POST['situacao']) && $_POST['situacao'] == 1) ? 'selected' : ''; ?>>Pago</option>                  
                </select>
              </div>

              
            </div>
            <div class="row">
              <div class="form-group col-md-3 col-sm-12">
                <label>Data Lançamento</label>
                <div class="input-daterange" id="datepicker">
                  <div class="input-group">
                    <span class="input-group-addon">De</span>
                    <input type="text" class="data form-control" value="<?php echo $this->objCommon->validatePost('filtroDataLancamentoInicio'); ?>" id="filtroDataLancamentoInicio" name="filtroDataLancamentoInicio" />
                    <span class="input-group-addon">á</span>
                    <input type="text" class="data form-control" value="<?php echo $this->objCommon->validatePost('filtroDataLancamentoFim'); ?>" id="filtroDataLancamentoFim" name="filtroDataLancamentoFim" />
                  </div>
                </div>
              </div>
              <div class="form-group col-md-3 col-sm-12">
                <label>Data de vencimento</label>
                <div class="input-daterange" id="datepicker">
                  <div class="input-group">
                    <span class="input-group-addon">De</span>
                    <input type="text" class="data form-control" value="<?php echo $this->objCommon->validatePost('filtroDataVencimentoInicio'); ?>" name="filtroDataVencimentoInicio" id="filtroDataVencimentoInicio" />
                    <span class="input-group-addon">á</span>
                    <input type="text" class="data form-control" value="<?php echo $this->objCommon->validatePost('filtroDataVencimentoFim'); ?>" name="filtroDataVencimentoFim" id="filtroDataVencimentoFim" />
                  </div>
                </div>
              </div>
              <div class="form-group col-md-3 col-sm-12">
                <label>Data de pagamento</label>
                <div class="input-daterange" id="datepicker">
                  <div class="input-group">
                    <span class="input-group-addon">De</span>
                    <input type="text" class="data form-control" value="<?php echo $this->objCommon->validatePost('filtroDataPagamentoInicio'); ?>" id="filtroDataPagamentoInicio" name="filtroDataPagamentoInicio" />
                    <span class="input-group-addon">á</span>
                    <input type="text" class="data form-control" value="<?php echo $this->objCommon->validatePost('filtroDataPagamentoFim'); ?>" id="filtroDataPagamentoFim" name="filtroDataPagamentoFim" />
                  </div>
                </div>
              </div>
              <div class="form-group col-md-3 col-sm-12">
                <label for="AccountId">Nº do pedido</label>
                <div class="input-group">
                  <input class="form-control" type="text" name="ndoc" id="ndoc" value="<?php echo $this->objCommon->validatePost('ndoc'); ?>">
                </div>
              </div>
              <div class="form-group col-md-6 col-sm-12">
                <button class="btn btn-primary" id="aplicarFiltroContasReceber" type="button">Aplicar Filtro</button>
                <button class="btn btn-primary" id="limparFiltro" type="button">Limpar Filtros</button>
              </div>
              <div class="form-group col-md-6 col-sm-12">
                <a class="btn btn-primary pull-right" href="<?php echo URL . $classe . '/cadastrar/'; ?>">Cadastrar Novo</a>
              </div>
            </div>
          </form>
        </div>

        <div class="callout <?= $tipo_mensagem; ?> hide" id="mensagemContasReceber">
          <p><?= $msg_sucesso; ?></p>
        </div>

        <div id="assets-cadastrados">
          <div class="box-body">
            <div class="table-responsive">
              <table id="listaContasReceber" class="dataTable table table-bordered table-striped">
                <thead>
                  <tr role="row">
                    <th>Pedido</th>
                    <th>Data de Lançamento</th>
                    <th>Categoria</th>
                    <th>Cliente</th>
                    <th>Valor</th>
                    <th>Conta</th>
                    <?php if (count($this->empresas) > 1 && (count($this->objOrcamento->arrPermissoesUsuarioEmpresa) < 1 || count($this->objOrcamento->arrPermissoesUsuarioEmpresa) == 0)) { ?>
                      <th>Empresa</th>
                    <?php } ?>
                    <?php if ($this->modelo_empresa != 'gestao-interna') { ?>
                      <th>Vendedor</th>
                    <?php } ?>
                    <th>Observação</th>
                    <th>Data de vencimento</th>
                    <?php if ($this->modelo_empresa != 'gestao-interna') { ?>
                      <th>Data de pagamento <br> Loja</th>
                    <?php } ?>
                    <th>Data de pagamento</th>
                    <th>Situação</th>
                    <?php if ($arrPermissoesCabecalho[$indiceContasAReceber]->editar) { ?>
                      <th>Editar</th>
                    <?php } ?>
                    <?php if ($arrPermissoesCabecalho[$indiceContasAReceber]->excluir) { ?>
                      <th>Excluir</th>
                    <?php } ?>
                    <?php if ($this->modelo_empresa == 'gestao-interna') { ?>
                      <th>Ações</th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody></tbody>
                <tfoot>
                  <tr>
                    <th colspan="4" style="text-align:right">Total:</th>
                    <th></th>
                    <th colspan="8">&nbsp;</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
</div>
<?php include_once(DOCUMENT_ROOT . "src/View/Common/rodape.php"); ?>