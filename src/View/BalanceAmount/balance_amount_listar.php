
      <?php include_once(DOCUMENT_ROOT."src/View/Common/cabecalho.php"); ?>  
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">					
                  <h3 class="box-title"></h3>					
                </div>
              </div>
              <div class="box-body">
                <div id="balance_amount-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-6">
                      <a class="btn btn-primary" href="<?php echo URL.$classe.'/cadastrar/';?>">Cadastrar Novo</a>
                    </div>
                    <div class="col-sm-6">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <table class="table table-bordered table-striped dataTable">
                        <thead>
                          <tr role="row">
                            
          <th>id</th>
        

          <th>tipo</th>
        

          <th>amount</th>
        

          <th>UserId</th>
        

                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $aux = 0;
                            foreach ($arrBalanceAmount as $BalanceAmount) {
                            if ($aux & 1) {
                          ?>
                            <tr class="odd" role="row">
                          <?php } else { ?>	
                            <tr class="even" role="row">
                          <?php } ?>	
                            
          <td><?php echo $Balance_amount->id; ?></td>
        

          <td><?php echo $Balance_amount->tipo; ?></td>
        

          <td><?php echo $Balance_amount->amount; ?></td>
        

          <td><?php echo $Balance_amount->UserId; ?></td>
        

                            <td><a class="" href="<?php echo URL.$classe.'/'.$acao.'/'. $BalanceAmount->id; ?>"><i class="fa fa-edit"></i>Editar</a></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>									
                    </div>								
                  </div>                
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include_once(DOCUMENT_ROOT."src/View/Common/rodape.php"); ?>  
    