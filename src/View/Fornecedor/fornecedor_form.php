  <?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  


    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<form autocomplete="off"  role="form" method="POST" action="<?php echo URL;?><?php echo $classe;?>/<?php echo $metodo;?>/<?php echo $fornecedores[0]->CodigoFornecedor;?>">
						<?php if (!empty($msg_sucesso)) { ?>
							<div class="callout callout-success">
								<h4>Sucesso!</h4>
								<p><?php echo $msg_sucesso;?></p>
							</div>
						<?php } ?>
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Cadastro</h3>
							</div>						
						</div>
						<div class="box-body">
							<div class="form-group col-xs-12 col-md-4">
									<label>Razão Social</label>
									<input class="form-control" type="text" value="<?php echo $fornecedores[0]->RazaoSocial;?>" name="RazaoSocial" />
							</div>

							<div class="form-group col-xs-12 col-md-4">
								<label>CNPJ</label>
								<input class="form-control" type="text" name="CGC" id="CGCFornecedor" value="<?php echo $fornecedores[0]->CGC;?>" maxlength="18">
							</div>

							<div class="form-group col-xs-12 col-md-4">
								<label>Inscrição Estadual</label>
								<input data-inputmask-regex="\d{1,15}" class="form-control" type="text" name="InscricaoEstadual" id="InscricaoEstadualFornecedor" value="<?php echo $fornecedores[0]->InscricaoEstadual;?>" maxlength="15">
							</div>
						</div>

						<div class="box-body">
							<div class="form-group col-xs-12 col-md-3">
								<label>CEP</label>
								<input class="form-control" type="text" name="CEP" value="<?php echo $fornecedores[0]->CEP;?>">
							</div>							

							<div class="form-group col-xs-12 col-md-4">
								<label>Endereço</label>
								<input class="form-control" type="text" name="Endereco" value="<?php echo $fornecedores[0]->Endereco;?>">
							</div>
							
							<div class="form-group col-xs-12 col-md-3">
								<label>Complemento</label>
								<input class="form-control" type="text" name="Complemento" value="<?php echo $fornecedores[0]->Complemento;?>">
							</div>
						</div>

						<div class="box-body">
							<div class="form-group col-xs-12 col-md-3">
								<label>Bairro</label>
								<input class="form-control" type="text" name="Bairro" value="<?php echo $fornecedores[0]->Bairro;?>">
							</div>
							
							<div class="form-group col-xs-12 col-md-3">
								<label>Cidade</label>
								<input class="form-control" type="text" name="Cidade" value="<?php echo $fornecedores[0]->Cidade;?>">
							</div>
							
							<div class="form-group col-xs-12 col-md-3">
								<div class="form-group">
									<label>Estado</label>
									<select name="Estado" class="form-control">
										<option value="">Selecione</option>
										<?php 
											if (is_array($estados)) {
												foreach ($estados as $estado_sigla => $estado_descricao) { 
													$selected = "";
													if ($estado_sigla == $fornecedores[0]->Estado) {
														$selected = ' selected="selected"';
													}
										?>
													<option value="<?php echo $estado_sigla;?>" <?php echo $selected;?>><?php echo $estado_descricao;?></option>									
										<?php 
												}
											}	
										?>	
									</select>
								</div>
							</div>
						</div>

						<div class="box-body">
							<div class="form-group col-xs-12 col-md-3">
								<label>Pessoa de contato</label>
								<input class="form-control" type="text" name="Contato" value="<?php echo $fornecedores[0]->Contato;?>">
							</div>

							<div class="form-group col-xs-12 col-md-3">
								<label>Telefone</label>
								<input class="form-control" type="text" name="Telefone1" value="<?php echo $fornecedores[0]->Telefone1;?>">
							</div>
							

							<div class="form-group col-xs-12 col-md-3">
								<label>E-mail</label>
								<input class="form-control" type="email" name="EMail" value="<?php echo $fornecedores[0]->EMail;?>">
							</div>

							<div class="form-group col-xs-12 col-md-3">
								<label>Data de Cadastro
                <span class="small">(Informação gerada automaticamente)</span>
                </label>
									<?php 
                    $dataCadastro = '';
                    if ($fornecedores[0]->DataCadastro) {
                      $DataCadastro = new DateTime($fornecedores[0]->DataCadastro);
                      $dataCadastro = $DataCadastro->format('d/m/Y');
                    }
									?>
									<input disabled type="text" data-mask="" name="DataCadastro" value="<?php echo $dataCadastro;?>" data-inputmask="'alias': 'dd/mm/yyyy'" class="form-control">
							</div>
							
							<div class="box-body">
								<div class="form-group">
									<label>Observações</label>
									<textarea class="form-control" name="Observacoes" /><?php echo $fornecedores[0]->Observacoes;?></textarea>
								</div>
							</div>							
							
						</div>

						
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Produtos deste fornecedor</h3>
							</div>						
						</div>

							<div class="row">
								<div class="col-sm-12">
									<table id="produtosFornecedor" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th>Código</th>
												<th>Descrição</th>
												<th>Última Compra</th>
												<th>Valor Pago</th>
											</tr>
										</thead>
										<tbody>
										<?php 
												if (is_array($historicoCompra)) {
													foreach ($historicoCompra as $historico_compra) {
														$DataCompra = new DateTime($historico_compra->dataCompra);
														$dataCompra = $DataCompra->format('d/m/Y');															
														echo '<tr>';
														echo sprintf('<td>%s</td>',$historico_compra->NovoCodigo);
														echo sprintf('<td>%s</td>',$historico_compra->DescricaoProduto);
														echo sprintf('<td>%s</td>',$dataCompra);
														echo sprintf('<td>R$ %s</td>',$historico_compra->valorPago);
														echo '</tr>';
													}
												}
										?>
										</tbody>
										<tfoot>
											<tr>
												<td class="sorting_1">Código</td>
												<td class="sorting_1">Descrição</td>
												<td class="sorting_1">Última Compra</td>
												<td class="sorting_1">Valor Pago</td>
											</tr>
										</tfoot>
									</table>									
								</div>								
							</div>												
						
						<div class="box-footer">
							<input type="hidden" name="handle" value="<?php echo $fornecedores[0]->CodigoFornecedor;?>">
							<button class="btn btn-primary" type="submit">Salvar</button>
							<a class="btn btn-primary" href="<?php echo URL.$classe;?>/listar/">Voltar</a>
						</div>
					</form>
				</div>
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  