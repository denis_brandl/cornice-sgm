<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<form autocomplete="off" role="form" id="formPedido" method="POST" action="<?php echo URL . $classe . '/' . $metodo . '/' . $orcamentos[0]->Cd_Orcamento ?>">
					<?php if (!empty($msg_sucesso)) { ?>
						<div class="callout callout-success">
							<h4>Sucesso!</h4>
							<p><?php echo $msg_sucesso; ?></p>
						</div>
					<?php } ?>
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Dados</h3>
						</div>
					</div>
					<div class="box-body">
						<div class="form-group col-xs-12 col-md-3">
							<label>Código Orçamento</label>
							<p class="small">Será gerado automaticamente</p>
							<div class="input-group">
								<input class="form-control" readonly type="text" value="<?php echo $orcamentos[0]->Cd_Orcamento; ?>" name="Cd_Orcamento" id="Cd_Orcamento" />
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-3">
							<label>Data do Orçamento</label>
							<div class="input-group date">
								<?php
								$DataOrcamento = new DateTime($orcamentos[0]->Dt_Orcamento);
								$dataOrcamento = $DataOrcamento->format('d/m/Y');
								?>
								<input class="form-control data" type="text" value="<?php echo $dataOrcamento; ?>" name="Dt_Orcamento" />
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-3">
							<label>Data Prevista Entrega</label>
							<div class="input-group">
								<?php
								$dataOrcamento = "";
								if ($orcamentos[0]->Dt_Prevista_Entrega != "0000-00-00 00:00:00" && $orcamentos[0]->Dt_Prevista_Entrega != NULL) {
									$dataOrcamento = $orcamentos[0]->Dt_Prevista_Entrega;
									$DataOrcamento = new DateTime($dataOrcamento);
									$dataOrcamento = $DataOrcamento->format('d/m/Y');
								}
								?>
								<input class="form-control data" type="text" value="<?php echo $dataOrcamento; ?>" name="Dt_Prevista_Entrega" />
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-3 hide">
							<label>Data Efetiva da Entrega</label>
							<div class="input-group date">
								<?php
								$dataEfetiva_Entrega = "";

								if ($orcamentos[0]->Dt_Efetiva_Entrega !== null) {
									$dataEfetiva_Entrega = new DateTime($orcamentos[0]->Dt_Efetiva_Entrega);
									$dataEfetiva_Entrega = $dataEfetiva_Entrega->format('d/m/Y');
								}
								?>
								<input class="form-control data" type="text" value="<?php echo $dataEfetiva_Entrega; ?>" name="Dt_Efetiva_Entrega" />
							</div>
						</div>
					</div>

					<div class="box-body">

						<div class="form-group col-md-4">
							<label>Cliente</label>
							<div class="input-group" style="width:100%;">
								<select name="Cd_Cliente" class="form-control" id="Cd_Cliente">
									<option value="<?php echo $orcamentos[0]->Cd_Cliente; ?>" selected="selected"><?php echo isset($orcamentos[0]->RazaoSocial) ? $orcamentos[0]->RazaoSocial : ''; ?></option>
								</select>
								<label>
									<br><button type="button" class="btn btn-warning" id="btnCadastrarCliente" data-toggle="modal" data-target="#cadastrarCliente">Cadastrar cliente</button>
								</label>
							</div>
						</div>
						<div class="form-group col-md-4 hidden">
							<label>Afiliado <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Quem será bonificado por essa venda? Campo não obrigatório" aria-hidden="true"></i></label>
							<div class="input-group" style="width:100%;">
								<select name="Consumidor_Temp" class="form-control" id="Consumidor_Temp">
									<?php if ($orcamentos[0]->Consumidor_Temp > 0) { ?>
										<option value="<?php echo $orcamentos[0]->Consumidor_Temp; ?>" selected="selected"><?php echo $orcamentos[0]->RazaoSocialConsumidor; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group col-md-4">
							<label>Situação</label>
							<div class="input-group">
								<select name="Id_Situacao" class="form-control" id="Id_Situacao">
									<?php
									foreach ($situacoes as $situacao) {
										$selected = "";
										if ($situacao->idSituacao == $orcamentos[0]->Id_Situacao) {
											$selected = "selected";
										}
									?>
										<option <?php echo $selected; ?> value="<?php echo $situacao->idSituacao; ?>"><?php echo $situacao->descricao; ?></option>
									<?php
									}
									?>
								</select>
								<?php /*
									<label>
										<input type="checkbox"> Notificar cliente?
									</label>
									*/ ?>
							</div>
						</div>

						<?php
						$criado_por = $orcamentos[0]->criado_por != 0 ? $orcamentos[0]->criado_por : $_SESSION['handle'];
						if ($grupo_usuario == '1') {
						?>
							<div class="form-group col-md-4 col-sm-12">
								<label for="criado_por">Vendedor</label>
								<select class="form-control" id="criado_por" name="criado_por">
									<?php
									foreach ($this->usuarios as $usuario) {
										echo sprintf(
											'<option %s value="%s">%s</option>',
											($usuario->CodigoUsuario == $criado_por) ? 'selected' : '',
											$usuario->CodigoUsuario,
											$usuario->NomeUsuario
										);
									}
									?>
								</select>
							</div>
						<?php } else { ?>
							<input type="hidden" value="<?php echo $criado_por; ?>" name="criado_por">
						<?php } ?>
					</div>
					<?php if ($orcamentos[0]->Id_Situacao <= 4) { ?>
						<div class="box-body">
							<div class="form-group col-xs-6">
								<label>&nbsp;</label>
								<div class="input-group">
									<button type="button" class="btn btn-info" id="btnExibeAdicionarQuadro">Incluir Item</button>
								</div>
							</div>
						</div>
					<?php } ?>

					<div class="box-body adicionar-quadro">
						<div class="box-header">
							<h3 class="box-title">Itens do pedido</h3>
						</div>
						<div class="form-group col-xs-12 col-md-2 hide">
							<label>Objeto</label>
							<select class="form-control" name="Cd_Prod_Aux_Pedido" id="Cd_Prod_Aux_Pedido">
								<?php foreach ($produtosAuxiliar as $produtoAuxiliar) { ?>
									<option value="<?php echo $produtoAuxiliar->id; ?>"><?php echo $produtoAuxiliar->descricao; ?></option>
								<?php } ?>
							</select>
						</div>

						<div class="form-group col-xs-12 col-md-3">
							<label>Produto/Serviço:</label>
							<div class="input-group" style="width:100%;">
								<input type="hidden" value="2" class="moldura">
								<input type="text" class="form-control" id="descricao_produto_servico" value="" name="descricao_produto_servico">
								<!--
										<select class="form-control moldura" style="width:100%;">
										<option value=""></option>
										<?php foreach ($produtos as $produto) {
											$stockColor = sprintf(
												'color: %s',
												$produto->Quantidade <= $produto->QuantidadeMinima ? '#ff0000' : '#000000'
											);
											$stockAlert = $produto->Quantidade <= $produto->QuantidadeMinima ? ' - (Estoque baixo)' : '';
										?>
											<option data-style="<?php echo $stockColor; ?>" value="<?php echo $produto->CodigoProduto; ?>"><?php echo $produto->NovoCodigo; ?> - <?php echo $produto->DescricaoProduto; ?> - R$ <?php echo number_format($produto->PrecoVendaMaoObra, 2, ".", ""); ?> <?php echo $stockAlert; ?></option>
										<?php } ?>
										</select>
					-->
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-2">
							<label>Quantidade</label>
							<div class="input-group">
								<input class="form-control" type="number" min="1" step="1" value="1" id="Qt_Item" name="Qt_Item" />
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-2 hide">
							<label>Preço Avulso</label>
							<div class="input-group">
								<input class="form-control numberOnly campoMonetario" type="text" value="" id="Vl_Avulso" name="Vl_Avulso" />
							</div>
						</div>
						<div class="form-group col-xs-12 col-md-2 hide">
							<label>Largura</label>
							<div class="input-group">
								<input class="form-control" type="number" min="0" step="1" value="0" id="Md_Largura" name="Md_Largura" />
								<span class="input-group-addon">CM</span>
							</div>
						</div>
						<div class="form-group col-xs-12 col-md-2 hide">
							<label>Altura</label>
							<div class="input-group">
								<input class="form-control" type="number" min="0" step="0.1" value="0" id="Md_Altura" name="Md_Altura" />
								<span class="input-group-addon">CM</span>
							</div>
						</div>
						<input class="form-control" type="hidden" min="0" step="0.1" value="0" id="VL_ADICIONAIS" name="VL_ADICIONAIS" />
						<input class="form-control" type="hidden" min="0" step="0.1" value="0" id="Vl_Moldura" name="Vl_Moldura" />

						<div class="clearfix"></div>
						<div class="form-group col-xs-12 col-md-6">
							<label>Observações em relação a peça</label>
							<div class="input-group col-xs-10">
								<textarea class="form-control col-xs-12" name="Ds_Observacao" id="Ds_Observacao"></textarea>
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-6">
							<label>Observações para produção</label>
							<div class="input-group col-xs-10">
								<textarea class="form-control col-xs-12" name="Ds_ObservacaoProducao" id="Ds_ObservacaoProducao"></textarea>
							</div>
						</div>
						<div class="clearfix"></div>

						<div class="box-body">
							<div class="form-group col-xs-12 col-md-2">
								<label>&nbsp;</label>
								<button class="form-control btn btn-primary" type="button" id="btnIncluirMoldura">Salvar Produto</button>
							</div>

							<div class="form-group col-xs-12 col-md-3 hide">
								<label>Componentes:</label>
								<div class="input-group" style="width:100%;">
									<select class="form-control componente" style="width:100%;">
										<option value=""></option>
										<?php foreach ($componentes as $componente) { ?>
											<option value="<?php echo $componente->IDCOMPONENTE; ?>"><?php echo $componente->DESCRICAO; ?> - R$ <?php echo number_format($componente->CUSTO, 2, ".", ""); ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group col-xs-12 col-md-2 hide">
								<label>&nbsp;</label>
								<button class="form-control btn btn-primary" type="button" id="btnIncluirComponente">Adicionar Componente</button>
							</div>
						</div>

						<div class="box-body hide">
							<div class="col-sm-6  table-responsive">
								<table id="tabelaMoldura" class="table table-bordered table-striped dataTable" role="grid">
									<thead>
										<tr role="row">
											<th colspan="4">Produtos selecionados</th>
										</tr>
										<tr role="row">
											<th>Produto</th>
											<th>Complexidade</th>
											<th> Ordem de produção </th>
											<th>Remover Item</th>
										</tr>
									</thead>
									<tbody class="input_molduras">
									</tbody>
								</table>
							</div>

							<div class="col-sm-6  table-responsive">
								<table id="tabelaComponente" class="table table-bordered table-striped dataTable" role="grid">
									<thead>
										<tr role="row">
											<th>Componentes selecionados</th>
										</tr>
									</thead>
									<tbody class="input_componentes">
									</tbody>
								</table>
							</div>

						</div>
						<div class="form-group col-xs-12 hide">
							<button type="button" class="btn btn-info" id="btnIncluirQuadro">Salvar Item</button>
						</div>

					</div>

					<div class="box-body">
						<div class="col-sm-12  table-responsive">
							<table id="tabelaImagem" class="table table-bordered table-striped dataTable" role="grid">
								<thead>
									<tr role="row">
										<?php /* <th>Objeto</th> */ ?>
										<th>Produto</th>
										<th>Qtd</th>
										<?php /*
										<th>Extras (R$) </th>
										<th>Extras 2 (R$)</th>
										<th>Valor Unitário</th>
										<th>Valor Item</th>
										*/ ?>
										<th>OBS</th>
										<th>OBS - Produção</th>
										<?php if ($orcamentos[0]->Id_Situacao <= 4) { ?>
											<th>Editar</th>
											<th>Excluir</th>
										<?php } ?>
									</tr>
								</thead>
								<tbody>
									<?php
									$aux = 0;
									if (isset($itemsOrcamento) && count($itemsOrcamento) > 0) {
										foreach ($itemsOrcamento	as $item_orcamento) {
											echo sprintf('<tr id="imagem%s">', $item_orcamento->Cd_Item_Orcamento);

											echo sprintf('<td id="molduras-imagem%s"> %s', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->descricao_produto_servico);
											if (is_array($arrMoldurasItemOrcamento) && count($arrMoldurasItemOrcamento) > 0 && isset($arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento])) {
												echo '<ul>';
												foreach ($arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento] as $molduras_item_orcamento) {
													$codigoComplexidade = 0;
													if (isset($molduras_item_orcamento['codigoComplexidade'])) {
														$codigoComplexidade = $molduras_item_orcamento['codigoComplexidade'];
													}
													$nomeComplexidade = '';
													if (isset($molduras_item_orcamento['nomeComplexidade'])) {
														$nomeComplexidade = sprintf('(%s)', $molduras_item_orcamento['nomeComplexidade']);
													}
													echo '<li><input data-moldura="' . $molduras_item_orcamento['Cd_Produto'] . '" type="hidden" text="' . $molduras_item_orcamento['DescricaoProduto'] . '" id="moldura-' . $molduras_item_orcamento['Cd_Produto'] . '" class="molduras-' . $item_orcamento->Cd_Item_Orcamento . '" name="Cd_Produto[' . $item_orcamento->Cd_Item_Orcamento . '][]" value="' . $molduras_item_orcamento['Cd_Produto'] . '|' . $codigoComplexidade . '" complexidade="' . $codigoComplexidade . '">' . $molduras_item_orcamento['Cd_Produto'] . ' - ' . $molduras_item_orcamento['DescricaoProduto'] . ' ' . $nomeComplexidade . '</li>';
												}
												echo '</ul>';
											}

											if (isset($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento])) {
												echo '<td>Componentes:';
												echo '<ul>';
												foreach ($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento] as $componentes_item_orcamento) {
													echo '<li><input type="hidden" text="' . $componentes_item_orcamento['Descricao'] . '" id="componente-' . $componentes_item_orcamento['Id_Componente'] . '" class="componentes-' . $item_orcamento->Cd_Item_Orcamento . '" name="CD_Componente[' . $item_orcamento->Cd_Item_Orcamento . '][]" value="' . $componentes_item_orcamento['Id_Componente'] . '">' . $componentes_item_orcamento['Descricao'] . '</li>';
												}
												echo '</ul>';
												echo '</td>';
											}

											echo '</td>';



											echo sprintf('<input type="hidden" name="Cd_Prod_Aux[%s]" id="Cd_Prod_Aux-%1$s" value="%s">', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->Cd_Prod_Aux, $item_orcamento->descricao);
											echo sprintf('<input type="hidden" name="descricao_produto_servico[%s]" id="descricao_produto_servico-%1$s" value="%s">', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->descricao_produto_servico, $item_orcamento->descricao_produto_servico);
											echo sprintf('<td><input type="hidden" name="Qt_Item[%s]" id="Qt_Item-%1$s" value="%s">%2$s</td>', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->Qt_Item);
											/*
											echo sprintf('<td><input type="hidden" name="Md_Largura[%s]" id="Md_Largura-%1$s" value="%s">%2$s</td>', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->Md_Largura);
											echo sprintf('<td><input type="hidden" name="Md_Altura[%s]" id="Md_Altura-%1$s" value="%s">%2$s</td>', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->Md_Altura);
											
											echo sprintf('<td><input type="text" name="VL_ADICIONAIS[%s]" id="VL_ADICIONAIS-%1$s" value="%s" class="campoMonetario recalcula"></td>', $item_orcamento->Cd_Item_Orcamento, number_format($item_orcamento->VL_ADICIONAIS, 2, ",", "."));
											echo sprintf('<td><input type="text" name="Vl_Moldura[%s]" id="Vl_Moldura-%1$s" value="%s" class="campoMonetario recalcula"></td>', $item_orcamento->Cd_Item_Orcamento, number_format($item_orcamento->Vl_Moldura, 2, ",", "."));
											echo sprintf('<td><input type="hidden" name="Vl_Unitario[%s]" id="Vl_Unitario-%1$s" value="%s"><span id="Vl_Unitario-Text-%1$s">R$ %2$s</span></td>', $item_orcamento->Cd_Item_Orcamento, number_format($item_orcamento->Vl_Unitario, 2, '.', ''));
											echo sprintf('<td><input type="hidden" name="Vl_Total[%s]" id="Vl_Total-%1$s" value="%s"><span id="Vl_Total-Text-%1$s">R$ %2$s</span></td>', $item_orcamento->Cd_Item_Orcamento, number_format($item_orcamento->Vl_Bruto, 2, '.', ''));
											*/
											echo sprintf('<td><input type="hidden" name="Ds_Observacao[%s]" id="Ds_Observacao-%1$s" value="%s">%2$s</td>', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->Ds_Observacao);
											echo sprintf('<td><input type="hidden" name="Ds_ObservacaoProducao[%s]" id="Ds_ObservacaoProducao-%1$s" value="%s">%2$s</td>', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->Ds_ObservacaoProducao);
											if ($orcamentos[0]->Id_Situacao <= 4) {
												echo sprintf('<td><a class="carrega_item_orcamento" value="%s"><i class="fa fa-edit"></i>Editar</a></td>', $item_orcamento->Cd_Item_Orcamento);
												echo sprintf('<td><a class="delete_item_orcamento" value="%s"><i class="fa fa-trash" aria-hidden="true"></i>Excluir</a></td>', $item_orcamento->Cd_Item_Orcamento);
											}
											//echo '</tr>';
											//$aux++;
											$aux = $item_orcamento->Cd_Item_Orcamento;
										}
										$aux++;
										echo '<tr id="imagem' . $aux . '"></tr>';
										echo '<tr id="molduras-imagem' . $aux . '"></tr>';
									} else {
									?>
										<tr id='imagem<?php echo $aux; ?>'></tr>
										<tr id='molduras-imagem<?php echo $aux; ?>'></tr>
									<?php
									}
									?>
								</tbody>
							</table>
						</div>
						<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:bold;font-size:16px;">
							<div>Observações gerais do pedido:</span><br>
								<textarea class="form-control col-xs-12" name="Ds_Observacao_Pedido" id="Ds_Observacao_Pedido"><?php echo isset($orcamentos[0]->Ds_Observacao_Pedido) == true ? $orcamentos[0]->Ds_Observacao_Pedido : ''; ?></textarea>

							</div>
						</div>

						<div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:bold;font-size:16px;">
							<div>Observações para a produção:</span><br>
								<textarea class="form-control col-xs-12" name="Ds_Observacao_Producao" id="Ds_Observacao_Producao"><?php echo isset($orcamentos[0]->Ds_Observacao_Producao) == true ? $orcamentos[0]->Ds_Observacao_Producao : ''; ?></textarea>

							</div>
						</div>

						<div class="col-md-12" style="font-weight:bold;font-size:16px;">
							<div>SubTotal: 
								<input type="text" name="Vl_Bruto" id="Vl_Bruto" value="<?php echo isset($orcamentos[0]->Vl_Bruto) ? number_format($orcamentos[0]->Vl_Bruto, 2, ".", "") : ''; ?>">
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-3">
							<label>Desconto R$ </label>
							<div class="input-group">
								<input class="form-control numberOnly campoMonetario" type="text" value="<?php echo number_format($orcamentos[0]->VL_desconto, 2, ",", ""); ?>" id="VL_desconto" name="VL_desconto" />
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-3">
							<label>Total R$</label>
							<div> <input type="text" name="vl_liquido" id="vl_liquido" value="<?php echo (float) $orcamentos[0]->vl_liquido; ?>"></div>
						</div>

						<div class="form-group col-xs-12 col-md-3">
							<label>Entrada R$ </label>
							<div class="input-group">
								<input class="form-control numberOnly campoMonetario" type="text" min="0" step="1" value="<?php echo number_format($orcamentos[0]->Vl_Entrada, 2, ",", ""); ?>" id="Vl_Entrada" name="Vl_Entrada" />
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-1">
							<label>Saldo R$ </label>
							<div><span id="totalSaldo"><?php echo number_format(($orcamentos[0]->vl_liquido - $orcamentos[0]->Vl_Entrada), 2, ".", ""); ?></span>
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-1">

							<label for="Pago">Pago ?
								<input type="checkbox" id="Pago" name="Pago" <?php echo $orcamentos[0]->Pago == 1 ? 'checked="checked"' : ''; ?> value="1" />
							</label>
						</div>

						<div class="form-group col-xs-12 col-md-3">
							<label>Data de pagamento</label>
							<div class="input-group date">
								<?php
								$dataPagamento = '';
								if (!empty($orcamentos[0]->data_pagamento)) {
									$DataPagamento = new DateTime($orcamentos[0]->data_pagamento);
									$dataPagamento = $DataPagamento->format('d/m/Y');
								}
								?>
								<input class="form-control data" type="text" value="<?php echo $dataPagamento; ?>" id="data_pagamento" name="data_pagamento" />
							</div>
						</div>

					</div>

					<div class="box-footer">
						<input type="hidden" name="handle" value="">
						<button class="col-md-1 col-xs-12 btn btn-primary" id="btnSalvarPedido" type="button"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;Salvar</button>
						<?php if (isset($orcamentos[0]->Cd_Orcamento) && $orcamentos[0]->Cd_Orcamento > 0) { ?>
							<button class="col-md-1 col-xs-12 btn btn-primary pedidoImprimir" id="btnPedidoImprimir" data-alvo="pedido" type="button"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Imprimir</button>
							<button class="col-md-1 col-xs-12 btn btn-primary" style="display:none;" id="btnPedidoEnviarWhatsapp" type="button"><i class="fa fa-whatsapp" aria-hidden="true"></i>&nbsp;Enviar por whatsapp</button>
							<a class="btn btn-primary col-md-1 col-xs-12" style="display:none;" id="abreWhatsappIphone" href="<?php echo URL . 'Whatsapp/enviarMensagemIphone/' . $orcamentos[0]->Cd_Orcamento ?>" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i>&nbsp;Enviar por whatsapp</a>							
							<button class="btn btn-primary" id="btnPedidoEnviarEmail" type="button"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;Enviar por email</button>
							<button class="btn btn-primary" id="btnPedidoImprimirOs" type="button"><i class="fa fa-exchange" aria-hidden="true"></i>&nbsp;Ordem de Serviço</button>
						<?php } ?>
						<a class="btn btn-primary" href="<?php echo URL; ?><?php echo $classe; ?>/listar/"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Voltar</a>
					</div>
				</form>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="cadastrarCliente" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Cadastro de cliente</h4>
				<div class="alert alert-success alert-dismissible alertaCadastroCliente" style="display:none">
					<div id="mensagemCadastroCliente"></div>
				</div>
			</div>
			<div class="modal-body">
				<div class="box">
					<div class="box-body">
						<div class="form-group col-xs-12 col-md-6">
							<label>Nome da pessoa | Razão Social</label>
							<input class="form-control" type="text" value="" id="RazaoSocial" name="RazaoSocial" />
						</div>
						<div class="form-group col-xs-12 col-md-6">
							<label>Nome Fantasia</label>
							<input class="form-control" type="text" id="NomeFantasia" name="NomeFantasia" value="">
						</div>
					</div>
					<div class="box-body">
						<div class="form-group col-xs-12 col-md-6">
							<label>Telefone/Whatsapp</label>
							<input class="form-control" type="text" id="Telefone1" name="Telefone1" value="">
						</div>
						<div class="form-group col-xs-12 col-md-6">
							<label>Telefone 2</label>
							<input class="form-control" type="text" id="Telefone2" name="Telefone2" value="">
						</div>
					</div>
					<div class="box-body">
						<div class="form-group col-xs-12 col-md-6">
							<label>E-mail</label>
							<input class="form-control" type="text" id="EMail" name="EMail" value="">
						</div>
						<div class="form-group col-xs-12 col-md-6">
							<label>Grupo do cliente</label>
							<select name="codigoClienteGrupo" id="codigoClienteGrupo" class="form-control">
								<option value="">Selecione</option>
								<?php foreach ($clienteGrupos as $grupo) { ?>
									<option value="<?php echo $grupo->codigoClienteGrupo; ?>"><?php echo $grupo->nome; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
				<button type="button" id="btnEnviarCadastroCliente" class="btn btn-primary">Salvar</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>