<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Filtros
					</h3>
				</div>
				<div class="box-body">

					<div class="row">
						<?php if (count($this->empresas) > 1 && (count($orcamento->arrPermissoesUsuarioEmpresa) < 1 || count($orcamento->arrPermissoesUsuarioEmpresa) == 0)) { ?>
							<div class="form-group col-md-3 col-sm-12">
								<label for="empresa">Empresa</label>
								<select class="form-control" id="empresa" name="empresa">
									<option value="">Todos</option>
									<?php
									foreach ($this->empresas as $empresa) {
										echo sprintf(
											'<option %s value="%s">%s</option>',
											'', // $empresa->CodigoEmpresa == $this->objCommon->validatePost('UserId') ? 'selected' : '',
											$empresa->CodigoEmpresa,
											$empresa->RazaoSocial
										);
									}
									?>
								</select>
							</div>
						<?php } ?>

						<?php if ($orcamento->permissao_visualizar_pedidos_outros_vendedores) { ?>
						<div class="form-group col-md-3 col-sm-12">
							<label for="criado_por">Vendedor</label>
							<select class="form-control" id="vendedor" name="vendedor">
								<option value="">Todos</option>
								<?php
								foreach ($this->usuarios as $usuario) {
									echo sprintf(
										'<option %s value="%s">%s</option>',
										'', //$conta->AccountId == $this->objCommon->validatePost('AccountId') ? 'selected' : '',
										$usuario->CodigoUsuario,
										$usuario->NomeUsuario
									);
								}
								?>
							</select>
						</div>
						<?php } else { ?>
							<input type="hidden" name="vendedor" id="vendedor" value="<?php echo $_SESSION['handle']; ?>" />
						<?php } ?>

						<?php if ($arrPermissoesCabecalho[$indiceClientes]->visualizar == 1) { ?>
							<div class="form-group col-md-3 col-sm-12">
								<label for="criado_por">Cliente</label>
								<select name="filtro_cliente" class="form-control" id="filtro_cliente"></select>
							</div>
						<?php } ?>

						<div class="form-group col-md-3 col-sm-12">
							<label label="situacao">Situação</label>
							<select name="situacao[]" class="form-control filtro_situacao" id="situacao" multiple="multiple">
								<?php
								foreach ($this->situacoes as $situacao) {
									$selected = "";

									if (isset($arrFiltro['filtro_situacao']) && in_array($situacao->idSituacao, explode(',', $arrFiltro['filtro_situacao']))) {
										$selected = "selected";
									}
								?>
									<option <?php echo $selected; ?> value="<?php echo $situacao->idSituacao; ?>"><?php echo $situacao->descricao; ?></option>
								<?php
								}
								?>
							</select>
						</div>

						<div class="form-group col-md-3 col-sm-12">
							<label for="pedido">Nº do pedido</label>
							<div class="input-group">
								<input class="form-control" type="text" id="pedido" name="pedido" value="">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6 col-sm-12">
							<button class="btn btn-primary" type="button" id="filtroPedidos">Aplicar Filtro</button>
						</div>
					</div>
					<?php if ($orcamento->permissoes[0]->cadastrar) { ?>
					<div class="row">
						<div class="form-group col-md-6 col-sm-12">
							<a class="btn btn-primary" href="<?php echo URL; ?><?php echo $classe; ?>/salvar/">Cadastrar Novo</a>
						</div>

					</div>
					<?php } ?>
				</div>
				<div class="box-body">
					<div id="componentes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12  table-responsive">
								<table id="listaOrcamento" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
									<thead>
										<tr>
											<th>Número do Pedido</th>
											<th>Data Pedido</th>
											<th>Data Prevista Entrega</th>
											<th>Cliente</th>
											<?php if (count($this->empresas) > 1 && (count($orcamento->arrPermissoesUsuarioEmpresa) < 1 || count($orcamento->arrPermissoesUsuarioEmpresa) == 0)) { ?>
												<th>Empresa</th>
											<?php } ?>
											<?php if ($orcamento->permissao_visualizar_pedidos_outros_vendedores) { ?>
											<th>Vendedor</th>
											<?php } ?>
											<th class="filterhead">Situação</th>
											<th>Editar</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>