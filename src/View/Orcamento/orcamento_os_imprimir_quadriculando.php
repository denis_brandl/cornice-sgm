<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo TITULO; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/bootstrap/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/bootstrap/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/select2/select2.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
    body {
      font-size: 13px;
    }

    a {
      cursor: pointer;
    }

    .adicionar-quadro {
      display: none;
    }

    .lblValores {
      font-weight: bold;
      min-width: 100px;
    }

    tr.border_bottom td {
      border-bottom: 2pt dashed black !important;
    }

    tr.border_top td {
      border-top: 2pt dashed black !important;
    }

    @media print {
      @page {
        margin: 0;
      }

      body {
        margin: 1.6cm;
      }
    }
  </style>
</head>

<body onload="window.print();">
  <div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <?php
      if (count($itemsOrcamento) > 0) {
        foreach ($itemsOrcamento  as $item_orcamento) {
      ?>
          <div class="row">

            <div class="col-xs-3 col-sm-3 col-md-3">
              <?php
              echo '<h5 style="margin-top:0px;font-weight:bold;">Ordem de serviço: ' . $orcamentos[0]->Cd_Orcamento . '</h5>';
              ?>
            </div>

            <div class="col-xs-3 col-sm-3 col-md-3">
              <strong>Cliente: <?php echo $clientes[0]->RazaoSocial; ?></strong>
              Telefone: <?php echo $clientes[0]->Telefone1; ?> <br> <?php echo $clientes[0]->Telefone2; ?>
            </div>

            <div class="col-xs-5 col-sm-5 col-md-5">
              <?php
              $DataOrcamento = new DateTime($orcamentos[0]->Dt_Orcamento);
              $dataOrcamento = $DataOrcamento->format('d/m/Y');
              echo '<b>Data da Emissão: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $dataOrcamento;
              ?>
              <?php
              if ($orcamentos[0]->Dt_Prevista_Entrega) {
                $dataOrcamento = $orcamentos[0]->Dt_Prevista_Entrega;
                $DataOrcamento = new DateTime($dataOrcamento);
                $dataOrcamento = $DataOrcamento->format('d/m/Y');
                echo '<br>Data Prevista Entrega:' . $dataOrcamento . '</b>';
              }
              ?>
              <br> <b>Situação: </b><?php echo $orcamentos[0]->situacao_pedido; ?>
            </div>

            <div class="col-xs-3 col-sm-3 col-md-3">
              <strong>Vendedor: <?php echo $orcamentos[0]->NomeUsuarioCriado; ?></strong>
            </div>

            <!-- /.col -->
          </div>

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-10 col-sm-10 col-md-10 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Item</th>
                    <th>Produto</th>
                    <th>Qtd</th>
                    <th>Largura</th>
                    <th>Altura</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                  $descricao_produto = '';
                  if (isset($arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento]) || (sizeof($arrComponentesItemOrcamento) > 0 && isset($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento])  && is_array($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento]))) {
                    $totalProdutos = 0;
                    
                    if (isset($arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento])) {
                      $totalProdutos = count($arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento]);
                      $auxProduto = 0;
                      foreach ($arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento] as $molduras_item_orcamento) {
                        $exibeComplexidade = '';
                        if ($molduras_item_orcamento['codigoComplexidade'] > 0) {
                          $exibeComplexidade = sprintf(' (%s)', $molduras_item_orcamento['nomeComplexidade']);
                        }

                        $exibeSarrafoReforco = $molduras_item_orcamento['sarrafo_reforco'] != 'N' ? ' Reforço: ' . ($molduras_item_orcamento['sarrafo_reforco'] == 'H' ? 'Horizontal' : 'Vertical') : '';
                        $descricao_produto .= $molduras_item_orcamento['NovoCodigo'] . '-' . $molduras_item_orcamento['DescricaoProduto'] . $exibeComplexidade . $exibeSarrafoReforco;
                        $auxProduto++;
                        if ($auxProduto < $totalProdutos) {
                          $descricao_produto .= '<br>';
                        }
                      }
                    }

                    if (sizeof($arrComponentesItemOrcamento) > 0 && isset($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento])  && is_array($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento])) {
                      if ($totalProdutos > 0) {
                        $descricao_produto .=  '<br>';
                      }
                      $totalProdutos = count($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento]);
                      $auxProduto = 0;
                      foreach ($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento] as $componentes_item_orcamento) {
                        $descricao_produto .=  $componentes_item_orcamento['Descricao'];
                        if ($componentes_item_orcamento['quantidade'] > 0) {
                          $descricao_produto .=  ' (' . $componentes_item_orcamento['quantidade'] . ' UNDs)';
                        }
                        $auxProduto++;
                        if ($auxProduto < $totalProdutos) {
                          $descricao_produto .=   '<br>';
                        }
                      }
                    }
                    echo '</tr>';
                  }

                  echo '<tr>';
                  echo sprintf('<td>%s</td>', $item_orcamento->Cd_Item_Orcamento);
                  echo sprintf('<td>%s</td>', $descricao_produto != '' ? $descricao_produto : $item_orcamento->descricao);
                  echo sprintf('<td>%s</td>', $item_orcamento->Qt_Item);
                  echo sprintf('<td>%s</td>', $item_orcamento->Md_Largura);
                  echo sprintf('<td>%s</td>', $item_orcamento->Md_Altura);

                  echo '</tr>';
                  

                  // echo '<tr><td> MEDIDAS FINAL: ' . $item_orcamento->medida_final . '</td></tr>';

                  if (trim($item_orcamento->Ds_ObservacaoProducao) != '') {
                    echo sprintf('<tr class="border_bottom"><td colspan="7"><strong>OBS: </strong>%s</td></tr>', $item_orcamento->Ds_ObservacaoProducao);
                  }

                  // echo '<tr class="border_bottom"><td colspan="6">&nbsp;</td><tr>';
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
      <?php
        }

        if (trim($orcamentos[0]->Ds_Observacao_Producao) != '') {
          echo sprintf('<p><strong>OBS: </strong>%s</p>', $orcamentos[0]->Ds_Observacao_Producao);
        }
      }
      ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- ./wrapper -->
</body>

</html>