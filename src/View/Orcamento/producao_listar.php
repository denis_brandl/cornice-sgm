<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Produção
          </h3>
        </div>

        <div class="box-body">

          <div class="row">
            <?php if (count($this->empresas) > 1 && (count($objOrcamento->arrPermissoesUsuarioEmpresa) < 1 || count($objOrcamento->arrPermissoesUsuarioEmpresa) == 0)) { ?>
              <div class="form-group col-md-2 col-sm-12">
                <label for="filtro-empresa-producao">Empresa</label>
                <select class="form-control" id="filtro-empresa-producao" name="filtro-empresa-producao">
                  <option value="">Todos</option>
                  <?php
                  foreach ($this->empresas as $empresa) {
                    echo sprintf(
                      '<option %s value="%s">%s</option>',
                      $empresa->CodigoEmpresa == $filtro_empresa ? 'selected' : '',
                      $empresa->CodigoEmpresa,
                      $empresa->RazaoSocial
                    );
                  }
                  ?>
                </select>
              </div>
            <?php } ?>

            <div class="form-group col-md-2 col-sm-12">
              <label for="filtro-vendedor-producao">Vendedor</label>
              <select class="form-control" id="filtro-vendedor-producao" name="filtro-vendedor-producao">
                <option value="">Todos</option>
                <?php
                foreach ($this->usuarios as $usuario) {
                  echo sprintf(
                    '<option %s value="%s">%s</option>',
                    '', //$conta->AccountId == $this->objCommon->validatePost('AccountId') ? 'selected' : '',
                    $usuario->CodigoUsuario,
                    $usuario->NomeUsuario
                  );
                }
                ?>
              </select>
            </div>

            <div class="form-group col-md-2 col-sm-12">
              <label>Período Inicial:</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar i-periodo-inicial"></i>
                </div>
                <input type="text" value="<?php echo $periodo_inicial != '' ? date('d/m/Y', strtotime($periodo_inicial)) : ''; ?>" id="filtro-data_inicio-producao" name="filtro-data_inicio-producao" class="form-control pull-right data">
              </div>
            </div>
            <div class="form-group col-md-2 col-sm-12">
              <label>Período Final:</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar  i-periodo-final"></i>
                </div>
                <input type="text" value="<?php echo $periodo_final != '' ? date('d/m/Y', strtotime($periodo_final)) : ''; ?>" id="filtro-data_final-producao" name="filtro-data_final-producao" class="form-control pull-right data">
              </div>

            </div>
          </div>

          <div class="row">
            <div class="form-group col-md-1 col-sm-12">
              <button class="btn btn-primary" type="button" id="filtroProducao">Aplicar Filtro</button>
            </div>
          </div>
        </div>

        <div class="box-body">
          <?php foreach ($listaSituacoes as $situacao) { ?>
            <div id="listaProducao<?php echo $situacao->idSituacao; ?>" class="col-md-3 col-xs-12">
              <h3><?php echo $situacao->descricao; ?> (<?php echo isset($arrPedidos[$situacao->idSituacao]) ? count($arrPedidos[$situacao->idSituacao]) : '0'; ?>)</h3>
              <?php if (isset($arrPedidos[$situacao->idSituacao])) { ?>
                <?php foreach ($arrPedidos[$situacao->idSituacao] as $pedido) { ?>
                  <div class="panel panel-default" id="pedido<?php echo $pedido['dados_pedido']->Cd_Orcamento; ?>">
                    <div class="panel-heading">
                      <?php
                      $data_prevista = $pedido['dados_pedido']->Dt_Prevista_Entrega !== NULL ? date('d/m/Y', strtotime($pedido['dados_pedido']->Dt_Prevista_Entrega)) : '<i>Não informado</i>';
                      echo sprintf('<strong>Código: %s - Cliente: %s</strong>', $pedido['dados_pedido']->Cd_Orcamento, $pedido['dados_pedido']->RazaoSocial);
                      echo sprintf('<br><strong>Vendedor: %s</strong>', $pedido['dados_pedido']->NomeUsuarioCriado);
                      echo sprintf('<p>Data prevista: %s', $data_prevista);
                      if ($_SESSION['handle_grupo'] != 4) {
                        echo sprintf('<br>Valor do pedido: R$ %s', number_format($pedido['dados_pedido']->Vl_Liquido, 2, ',', '.'));
                        echo sprintf('<br>Valor de entrada: R$ %s', number_format($pedido['dados_pedido']->Vl_Entrada, 2, ',', '.'));
                        echo sprintf('<br>Saldo: R$ %s', number_format($pedido['dados_pedido']->Vl_Liquido - $pedido['dados_pedido']->Vl_Entrada, 2, ',', '.'));

                        echo sprintf('<br>Observações: <br> <i>%s</i>', $pedido['dados_pedido']->Ds_Observacao_Pedido);
                      }
                      echo '</p>';
                      ?>
                      <p>
                        <?php echo sprintf('Observações Produção: <br> <i>%s</i>', $pedido['dados_pedido']->Ds_Observacao_Producao); ?>
                      </p>
                      <div class="form-inline">
                        <a class="btn btn-danger btn-xs" role="button" data-toggle="collapse" href="#detalhes-pedido-<?php echo $pedido['dados_pedido']->Cd_Orcamento; ?>" aria-expanded="false" aria-controls="detalhes-pedido-<?php echo $pedido['dados_pedido']->Cd_Orcamento; ?>">Visualizar detalhes</a>
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" onClick="statusAtual(<?php echo $pedido['dados_pedido']->Id_Situacao; ?>, <?php echo $pedido['dados_pedido']->Cd_Orcamento; ?>)" data-target="#alterarSituacaoPedido">Alterar Situação</button>
                        <?php
                        if (
                          ($pedido['dados_pedido']->Pago == 0 && $situacao->idSituacao == 4)
                          && ($_SESSION['handle_grupo'] != 4)
                        ) {
                          echo sprintf('<span class="btn btn-warning btn-xs" style="color:#fff;background-color:#f0ad4e;border-color:#eea236">Pagamento pendente</span>');
                        }
                        ?>
                      </div>
                    </div>
                    <div class="panel-body collapse" id="detalhes-pedido-<?php echo $pedido['dados_pedido']->Cd_Orcamento; ?>">
                      <p>Itens:</p>
                      <?php foreach ($pedido['itens_orcamento'] as $itens_orcamento) {
                        $lista_molduras = [];
                        foreach ($itens_orcamento['molduras'] as $moldura) {
                          $exibeComplexidade = '';
                          if ($moldura['codigoComplexidade'] > 0) {
                            $exibeComplexidade = sprintf(' (%s)', $moldura['nomeComplexidade']);
                          }
                          $exibeSarrafoReforco = $moldura['sarrafo_reforco'] != 'N' ? ' Reforço: ' . ($moldura['sarrafo_reforco'] == 'H' ? 'Horizontal' : 'Vertical') : '';
                          $lista_molduras[] = $moldura['DescricaoProduto'] . $exibeComplexidade . $exibeSarrafoReforco;
                        }
                        $lista_componentes = [];
                        foreach ($itens_orcamento['componentes'] as $componente) {
                          $lista_componentes[] = $componente['Descricao'];
                        }
                        echo sprintf(
                          '
                            <p><strong>Item: %s - %s </strong></p>
                            <p>
                              Quantidade: %s <br/>
                              Altura: %s cm <br/>
                              Largura: %s cm
                            <p>
                            <p>Molduras: <br/>
                              %s
                            </p>
                            <p>Componentes: <br/>
                              %s
                            </p>
                            <p>Observações: <br/>
                            <i>%s</i>
                            </p>
                            <hr>
                            ',
                          $itens_orcamento['itens']->Cd_Item_Orcamento,
                          $itens_orcamento['itens']->descricao,
                          $itens_orcamento['itens']->Qt_Item,
                          $itens_orcamento['itens']->Md_Altura,
                          $itens_orcamento['itens']->Md_Largura,
                          implode(',', $lista_molduras),
                          implode(',', $lista_componentes),
                          $itens_orcamento['itens']->Ds_ObservacaoProducao
                        )
                      ?>
                      <?php } ?>
                    </div>
                  </div>
                <?php } ?>
              <?php } ?>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="alterarSituacaoPedido" style="display: none;" data-status="">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Alterar situação do pedido</h4>
        <div class="alert alert-dismissible alertaAlteracaoPreco" style="display:none">
          <div id="mensagemAlteracaoPreco"></div>
        </div>
      </div>
      <div class="modal-body">
        <form action="#" method="POST" id="frmAlterarSituacao">
          <div class="form-group">
            <label for="arquivoImportarProdutos">Nova situação</label>
            <div class="input-group">
              <select name="Id_Nova_Situacao" class="form-control" id="Id_Nova_Situacao">
                <option value="">Selecione uma opção</option>
                <?php foreach ($listaSituacoes as $situacao) { ?>
                  <option value="<?php echo $situacao->idSituacao; ?>"><?php echo $situacao->descricao; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="arquivoImportarProdutos">Informe alguma observação (se necessário)</label>
            <div class="input-group col-xs-10">
              <textarea class="form-control col-xs-12" placeholder="Informe algum imprevisto ou ajuste que foi necessário" name="Ds_Observacao" id="Ds_Observacao"></textarea>
            </div>
          </div>
          <input type="hidden" name="handle" id="handle" value="">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
        <button type="button" id="btnAlterarSituacao" class="btn btn-primary">Salvar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>