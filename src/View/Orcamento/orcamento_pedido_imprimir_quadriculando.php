<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo TITULO; ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/bootstrap/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/bootstrap/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
	<link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/select2/select2.css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
	<style type="text/css">
		a {
			cursor: pointer;
		}

		.adicionar-quadro {
			display: none;
		}

		.lblValores {
			font-weight: bold;
			padding-right: 40px;
			text-align: right;
		}

		.table>tbody>tr>td {
			line-height: 1
		}

		@media print {
			@page {
				margin: 0;
			}

			body {
				margin: 1.6cm;
			}
		}
	</style>
</head>

<body onload="window.print();">
	<div class="wrapper">
		<!-- Main content -->
		<section class="invoice">
			<!-- title row -->
			<div class="row">
				<div class="col-xs-2 col-sm-4 col-md-3">
					<h2 class="page-header" style="border:none;padding-bottom:0px;margin-top:0px;">
						<img src="<?php echo URL; ?>images/logo-cliente-cinza.png" />
					</h2>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-5">
					<address>
						<?php
						echo sprintf(
							"
                   %s <br> CNPJ: %s <br>
                    %s<br>
                    %s %s %s <br>
                    <strong>
                      %s
                      %s
                    </strong><br>
                    %s
                  ",
							$empresa->NomeFantasia,
							$empresa->CGC,
							$empresa->Endereco . (trim($empresa->Complemento) != '' ? ' ' . $empresa->Complemento : ''),
							$empresa->Bairro,
							$empresa->Cidade,
							$empresa->Estado,
							$empresa->Telefone1,
							!empty($empresa->Telefone2) ? ' | ' . $empresa->Telefone2 : '',
							$empresa->EMail
						);
						?>
					</address>
				</div>

				<div class="col-xs-2 col-sm-3 col-md-3">
					<h4 style="margin-top:0px;"><strong><?php echo $orcamentos[0]->Id_Situacao > 1 ? 'Pedido' : 'Orçamento' ?> #<?php echo $orcamentos[0]->Cd_Orcamento; ?></strong></h4>
				</div>

				<div class="col-xs-4 col-sm-2 col-md-3">
					<?php
					$DataOrcamento = new DateTime($orcamentos[0]->Dt_Orcamento);
					$dataOrcamento = $DataOrcamento->format('d/m/Y');
					echo '<strong>Data: </strong>' . $dataOrcamento;
					?>
					<?php
					if ($orcamentos[0]->Id_Situacao > 1) {
						$dataOrcamento = '';
						if ($orcamentos[0]->Dt_Prevista_Entrega) {
							$dataOrcamento = $orcamentos[0]->Dt_Prevista_Entrega;
							$DataOrcamento = new DateTime($dataOrcamento);
							$dataOrcamento = $DataOrcamento->format('d/m/Y');
						}
						echo '<br><b>Previsão de Entrega: </b>' . $dataOrcamento;

						$dataOrcamento = '';
						if ($orcamentos[0]->Dt_Efetiva_Entrega) {
							$dataOrcamento = $orcamentos[0]->Dt_Efetiva_Entrega;
							$DataOrcamento = new DateTime($dataOrcamento);
							$dataOrcamento = $DataOrcamento->format('d/m/Y');
						}
						echo '<br><b>Data de Entrega: </b>' . $dataOrcamento;
					}
					?>
					<br> <b>Situação: </b><?php echo $orcamentos[0]->situacao_pedido; ?>
				</div>

				<!-- /.col -->
			</div>

			<!-- Table row -->
			<div class="row">
				<div class="col-xs-12 col-md-6 col-lg-6">
					<p>
						<strong><?php echo $clientes[0]->RazaoSocial; ?></strong>
						<?php echo $clientes[0]->CGC; ?><br>

						<?php
						$endereco = !is_null($clientes[0]->Endereco) ? $clientes[0]->Endereco : '';
						$numero = $clientes[0]->numeroEndereco !== '' ? ',' . $clientes[0]->numeroEndereco : '';
						echo $endereco !== '' ? $endereco . $numero . '<br/>' : '';
						?>
						<?php echo !is_null($clientes[0]->Complemento) ? $clientes[0]->Complemento : ''; ?>
						<?php echo $clientes[0]->Bairro !== '' ? $clientes[0]->Bairro .  '<br>' : ''; ?>

						<?php

						if ($clientes[0]->Cidade !== "null" && !is_null($clientes[0]->Estado)) {
							echo $clientes[0]->Cidade . '-' . $clientes[0]->Estado . '<br>';
						}
						?>
						Telefone: <?php echo $clientes[0]->Telefone1; ?> &nbsp; <?php echo $clientes[0]->Telefone2; ?> <br>
						<?php if ($clientes[0]->Observacoes) {
							echo 'Observações:' . $clientes[0]->Observacoes;
						}
						?>
					</p>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-6">
					<strong>Vendedor: <?php echo $orcamentos[0]->NomeUsuarioCriado; ?></strong>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Item</th>
								<th>Produto</th>
								<th>Qtd</th>
								<th>Larg</th>
								<th>Alt</th>
								<!-- <th><?php echo $this->modelo_empresa === 'molduraria' ? 'Produtos/Componentes' : 'Produto'; ?></th> -->
								<th style="text-align:right;">R$ Unit.</th>
								<th style="text-align:right;">R$ Total</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if (count($itemsOrcamento) > 0) {
								foreach ($itemsOrcamento	as $item_orcamento) {

									$conteudo_produtos = '';
									if (isset($arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento]) || (sizeof($arrComponentesItemOrcamento) > 0 && isset($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento])  && is_array($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento]))) {
										$totalProdutos = 0;
										if (isset($arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento])) {
											$totalProdutos = count($arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento]);
											$auxProduto = 0;
											foreach ($arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento] as $molduras_item_orcamento) {
												$conteudo_produtos .= $molduras_item_orcamento['DescricaoProduto'];
												$auxProduto++;
												if ($auxProduto < $totalProdutos) {
													$conteudo_produtos .= '<br><br>';
												}
											}
										}

										if (sizeof($arrComponentesItemOrcamento) > 0 && isset($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento])  && is_array($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento])) {
											if ($totalProdutos > 0) {
												$conteudo_produtos .= '<br><br>';
											}
											$totalProdutos = count($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento]);
											$auxProduto = 0;
											foreach ($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento] as $componentes_item_orcamento) {
												$conteudo_produtos .= $componentes_item_orcamento['Descricao'];

												$auxProduto++;
												if ($auxProduto < $totalProdutos) {
													$conteudo_produtos .=  '<br><br>';
												}
											}
										}
									}


									echo '<tr>';
									echo sprintf('<td>%s</td>', $item_orcamento->Cd_Item_Orcamento);
									echo sprintf('<td>%s</td>', $conteudo_produtos != '' ? $conteudo_produtos : $item_orcamento->descricao);
									echo sprintf('<td>%s</td>', $item_orcamento->Qt_Item);
									echo sprintf('<td>%s</td>', $item_orcamento->Md_Largura);
									echo sprintf('<td>%s</td>', $item_orcamento->Md_Altura);

									echo sprintf('<td style="text-align:right;">%s</td>', number_format($item_orcamento->Vl_Unitario, 2, ",", "."));
									echo sprintf('<td style="text-align:right;">%s</td>', number_format($item_orcamento->Vl_Unitario * $item_orcamento->Qt_Item, 2, ",", "."));
									echo '</tr>';

									if ($item_orcamento->medida_final != '') {
									//  echo '<tr><td> MEDIDAS FINAL: ' . $item_orcamento->medida_final . '</td></tr>';
									}

									if (trim($item_orcamento->Ds_Observacao) != '') {
										echo sprintf('<tr><td style="line-height: 1.5;" colspan="8"><strong>Obs Item %s: </strong><br>%s</td></tr>', $item_orcamento->Cd_Item_Orcamento, nl2br($item_orcamento->Ds_Observacao) );
									}
								}
							}
							?>
							<?php if ($orcamentos[0]->Id_Situacao > 1) { ?>
								<?php if ($orcamentos[0]->valorDescontoFinal > 0) { ?>
									<tr style="font-weight:bold;">
										<td colspan="5" style="text-align:right;">
											Subtotal R$:
										</td>
										<td colspan="2" style="text-align:right;">
											<?php echo number_format($orcamentos[0]->Vl_Bruto, 2, ",", "."); ?>
										</td>
									</tr>
									<tr style="font-weight:bold;">
										<td colspan="5" style="text-align:right;">
											Desconto R$:
										</td>
										<td colspan="2" style="text-align:right;">
											<?php echo number_format($orcamentos[0]->valorDescontoFinal, 2, ",", "."); ?>
										</td>
									</tr>
								<?php } ?>

								<tr style="font-weight:bold;">
									<td colspan="3">
										<strong>Horário de atendimento: </strong>
									</td>
									<td colspan="3" style="text-align:right;">
										Total R$:
									</td>
									<td colspan="1" style="text-align:right;">
										<?php echo number_format($orcamentos[0]->vl_liquido, 2, ",", "."); ?>
									</td>
								</tr>
								<tr style="font-weight:bold;">
									<td colspan="3" style="line-height: 1.00 !important;">
										<?php echo $empresa->horario_funcionamento; ?>
									</td>
									<td colspan="3" style="text-align:right;">
										Entrada R$:
									</td>
									<td colspan="1" style="text-align:right;">
										<?php echo number_format($orcamentos[0]->Vl_Entrada, 2, ",", "."); ?>
									</td>
								</tr>
								<tr style="font-weight:bold;">

									<td colspan="5" style="text-align:right;font-weight:bold;">
										<?php
										if ($orcamentos[0]->Pago == '1') {
											echo '<span style="padding:10px;width:80%;color:green;font-size:18px">PAGO</span>';
										}
										?>
									</td>
									<td colspan="1" style="text-align:right;font-weight:bold;">
										Saldo R$:
									</td>
									<td colspan="3" style="text-align:right;font-weight:bold;">
										<?php echo number_format($orcamentos[0]->vl_liquido - $orcamentos[0]->Vl_Entrada, 2, ",", "."); ?>
									</td>
								</tr>
							<?php } else { ?>
								<?php if ($orcamentos[0]->valorDescontoFinal > 0) { ?>
									<tr style="font-weight:bold;">
										<td colspan="7" style="text-align:right;">
											Desconto R$:
										</td>
										<td colspan="2" style="text-align:right;">
											<?php echo number_format($orcamentos[0]->valorDescontoFinal, 2, ",", "."); ?>
										</td>
									</tr>
								<?php } ?>

								<tr style="font-weight:bold;">
									<td colspan="6" style="text-align:right;">Total R$: </td>
									<td colspan="2" style="text-align:right;"><strong><?php echo number_format($orcamentos[0]->vl_liquido, 2, ",", "."); ?></strong></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<div class="row">
				<!-- /.col -->

				<!-- accepted payments column -->
				<?php if ($orcamentos[0]->Id_Situacao > 0) { ?>
					<div class="col-xs-12 col-lg-12 col-md-12">
						<strong>Observações: </strong>
						<strong><?php echo nl2br($orcamentos[0]->Ds_Observacao_Pedido); ?></strong>
					</div>
					<div class="col-xs-12 col-lg-12 col-md-12">
						<p class="text-muted well well-sm no-shadow">
							<strong>Aviso:</strong><br />
							<?php echo nl2br($empresa->observacao_pedido); ?>
						</p>
					</div>
				<?php } ?>

				<?php /*if ($orcamentos[0]->Id_Situacao > 1) { ?>
				<?php } else { ?>
				      <div class="col-md-offset-10">
					<strong>Total: R$ <?php echo number_format($orcamentos[0]->Vl_Bruto,2,",",""); ?></strong>
					</div>
				<?php }*/ ?>
			</div>
			<!-- /.row -->
			<footer>
				Maestria - Sistema para Gestão - www.maestriasistema.com.br
			</footer>
		</section>
		<!-- /.content -->
	</div>
	<!-- ./wrapper -->
</body>

</html>