<?php

include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php');
$situacao_bloquear_edicao = $this->ambiente == 'quadrosrios' ? 1 : 4;
$desabilita_campos = ($orcamentos[0]->Id_Situacao > $situacao_bloquear_edicao && $arrPermissoesCabecalho[$indicePermitirModificarValoresPedidos]->visualizar == 0) ? 'readonly' : '';

?>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <form autocomplete="off" role="form" id="formPedido" method="POST" action="<?php echo URL . $classe . '/' . $metodo . '/' . $orcamentos[0]->Cd_Orcamento ?>">
          <?php if (!empty($msg_sucesso)) { ?>
            <div class="callout callout-success">
              <h4>Sucesso!</h4>
              <p>
                <?php echo $msg_sucesso; ?>
              </p>
            </div>
          <?php } ?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Dados</h3>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group col-xs-12 col-md-3">
              <label>Código</label>
              <p class="small">Será gerado automaticamente</p>
              <div class="input-group">
                <input class="form-control" readonly type="text" value="<?php echo $orcamentos[0]->Cd_Orcamento; ?>" name="Cd_Orcamento" id="Cd_Orcamento" />
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-3">
              <label>Data de cadastro</label>
              <div class="input-group date">
                <?php
                $DataOrcamento = new DateTime($orcamentos[0]->Dt_Orcamento);
                $dataOrcamento = $DataOrcamento->format('d/m/Y');
                ?>
                <input class="form-control data" type="text" value="<?php echo $dataOrcamento; ?>" name="Dt_Orcamento" />
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-3">
              <label>Data Prevista Entrega</label>
              <div class="input-group">
                <?php
                $dataOrcamento = "";
                if ($orcamentos[0]->Dt_Prevista_Entrega != "0000-00-00 00:00:00" && $orcamentos[0]->Dt_Prevista_Entrega != NULL) {
                  $dataOrcamento = $orcamentos[0]->Dt_Prevista_Entrega;
                  $DataOrcamento = new DateTime($dataOrcamento);
                  $dataOrcamento = $DataOrcamento->format('d/m/Y');
                }
                ?>
                <input class="form-control data" type="text" value="<?php echo $dataOrcamento; ?>" name="Dt_Prevista_Entrega" />
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-3">
              <label>Data Efetiva da Entrega</label>
              <div class="input-group date">
                <?php
                $dataEfetiva_Entrega = "";
                if (!is_null($orcamentos[0]->Dt_Efetiva_Entrega) && $orcamentos[0]->Dt_Efetiva_Entrega !== "") {
                  $dataEfetiva_Entrega = new DateTime($orcamentos[0]->Dt_Efetiva_Entrega);
                  $dataEfetiva_Entrega = $dataEfetiva_Entrega->format('d/m/Y');
                }
                ?>
                <input class="form-control data" type="text" value="<?php echo $dataEfetiva_Entrega; ?>" name="Dt_Efetiva_Entrega" />
              </div>
            </div>
          </div>
          <div class="box-body">

            <?php if (count($this->empresas) > 1 && (count($orcamento->arrPermissoesUsuarioEmpresa) < 1 || count($orcamento->arrPermissoesUsuarioEmpresa) == 0)) { ?>
              <div class="form-group col-md-6">
                <label>Empresa</label>
                <select class="form-control" name="id_empresa" id="id_empresa">
                  <?php
                  foreach ($this->empresas as $empresa) {
                    $selected = isset($orcamentos[0]->id_empresa) && $empresa->CodigoEmpresa == $orcamentos[0]->id_empresa ? 'selected' : '';
                    echo sprintf('<option %s value="%s">%s</option>', $selected, $empresa->CodigoEmpresa, $empresa->RazaoSocial);
                  }
                  ?>
                </select>
              </div>
            <?php } else {
              $handle_empresa_padrao = isset($orcamento->arrPermissoesUsuarioEmpresa[0]) ? $orcamento->arrPermissoesUsuarioEmpresa[0] : $this->empresas[0]->CodigoEmpresa;
            ?>
              <input type="hidden" name="id_empresa" value="<?php echo $handle_empresa_padrao; ?>" />
            <?php } ?>

            <?php
            $criado_por = $orcamentos[0]->criado_por != 0 ? $orcamentos[0]->criado_por : $_SESSION['handle'];
            if ($grupo_usuario == '1') {
            ?>
              <div class="form-group col-md-6 col-sm-12">
                <label for="criado_por">Vendedor</label>
                <select class="form-control" id="criado_por" name="criado_por">
                  <?php
                  if ($orcamentos[0]->SituacaoUsuarioCriado == 0) {
                    echo sprintf(
                      '<option %s readonly value="%s">%s</option>',
                      'selected',
                      $orcamentos[0]->CodigoUsuarioCriado,
                      $orcamentos[0]->NomeUsuarioCriado
                    );                    
                  }
                  foreach ($this->usuarios as $usuario) {
                    echo sprintf(
                      '<option %s value="%s">%s</option>',
                      ($usuario->CodigoUsuario == $criado_por) ? 'selected' : '',
                      $usuario->CodigoUsuario,
                      $usuario->NomeUsuario
                    );
                  }
                  ?>
                </select>
              </div>
            <?php } else { ?>
              <input type="hidden" value="<?php echo $criado_por; ?>" name="criado_por">
            <?php } ?>

            <?php if ($arrPermissoesCabecalho[$indiceClientes]->visualizar == 1) { ?>
            <div class="form-group col-md-4">
              <label>Cliente</label>
              <div class="input-group" style="width:100%;">
                <select name="Cd_Cliente" class="form-control" id="Cd_Cliente">
                  <option value="<?php echo $orcamentos[0]->Cd_Cliente; ?>" selected="selected"><?php echo isset($orcamentos[0]->RazaoSocial) ? $orcamentos[0]->RazaoSocial . ' - ' .  $orcamentos[0]->Telefone1  : ''; ?></option>
                </select>
                <?php if ($arrPermissoesCabecalho[$indiceClientes]->cadastrar == 1) { ?>
                <label>
                  <br><button type="button" class="btn btn-warning" id="btnCadastrarCliente" data-toggle="modal" data-target="#cadastrarCliente">Cadastrar cliente</button>
                </label>
                <?php } ?>
              </div>
            </div>
            <?php } else { ?>
                <input type="hidden" id="Cd_Cliente" name="Cd_Cliente" value="<?php echo $orcamentos[0]->Cd_Cliente; ?>"/>
            <?php } ?>
            <div class="form-group col-md-4 hidden">
              <label>Afiliado <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Quem será bonificado por essa venda? Campo não obrigatório" aria-hidden="true"></i></label>
              <div class="input-group" style="width:100%;">
                <select name="Consumidor_Temp" class="form-control" id="Consumidor_Temp">
                  <?php if ($orcamentos[0]->Consumidor_Temp > 0) { ?>
                    <option value="<?php echo $orcamentos[0]->Consumidor_Temp; ?>" selected="selected"><?php echo $orcamentos[0]->RazaoSocialConsumidor; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group col-md-2">
              <label>Situação</label>
              <div class="input-group" style="width:100%;">
                <select name="Id_Situacao" class="form-control" id="Id_Situacao">
                  <?php
                  $arrSituacoesPermitidas = $indiceSituacoesPedido != false ? explode(',', $arrPermissoesCabecalho[$indiceSituacoesPedido]->conteudo) : [];
                  $temRestricaoSituacoes = count($arrSituacoesPermitidas) > 0;

                  $permitir_retornar_situacao = $arrPermissoesCabecalho[$indicePermitirRetornarSituacao]->visualizar == 1;

                  foreach ($situacoes as $situacao) {
                    $selected = "";
                    if ($situacao->idSituacao == $orcamentos[0]->Id_Situacao) {
                      $selected = "selected";
                    }

                    if (( $temRestricaoSituacoes && !in_array($situacao->idSituacao, $arrSituacoesPermitidas))) {
                        continue;
                    }

                    if ($permitir_retornar_situacao == false && $situacao->idSituacao < $orcamentos[0]->Id_Situacao) {
                      continue;
                    }
                  ?>
                    <option <?php echo $selected; ?> value="<?php echo $situacao->idSituacao; ?>"><?php echo $situacao->descricao; ?></option>
                  <?php
                  }
                  ?>
                </select>
                <label>
                  <?php if (isset($orcamentos[0]->Cd_Orcamento) && $orcamentos[0]->Cd_Orcamento > 0) { ?>
                    <?php if (isset($orcamentos[0]->Id_Situacao) && $orcamentos[0]->Id_Situacao >= 3 && $orcamentos[0]->Id_Situacao < 6) { ?>
                      <br>
                      <button class="btn btn-primary" id="btnSituacaoEnviarWhatsapp" type="button">
                        <i class="fa fa-whatsapp" aria-hidden="true"></i>&nbsp;Notificar situação por whatsapp
                      </button>
                </label>
              <?php } ?>
            <?php } ?>
              </div>
            </div>
          </div>
          <?php if ($orcamentos[0]->Id_Situacao <= $situacao_bloquear_edicao || $arrPermissoesCabecalho[$indicePermitirModificarValoresPedidos]->visualizar == 1) { ?>
            <div class="box-body">
              <div class="form-group col-xs-6">
                <label>&nbsp;</label>
                <div class="input-group">
                  <button type="button" class="btn btn-info" id="btnExibeAdicionarQuadro">Incluir Item</button>
                </div>
              </div>
            </div>
          <?php } ?>

          <div class="box-body adicionar-quadro">
            <div class="form-group col-xs-12 col-md-2">
              <label>Objeto</label>
              <select class="form-control" name="Cd_Prod_Aux_Pedido" id="Cd_Prod_Aux_Pedido">
                <?php foreach ($produtosAuxiliar as $produtoAuxiliar) { ?>
                  <option data-metragem="<?php echo $produtoAuxiliar->metragem; ?>" value="<?php echo $produtoAuxiliar->id; ?>"><?php echo $produtoAuxiliar->descricao; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group col-xs-12 col-md-2">
              <label>Quantidade</label>
              <div class="input-group">
                <input class="form-control numberOnly" onClick="this.select();" type="number" min="1" step="1" value="1" id="Qt_Item" name="Qt_Item" />
              </div>
            </div>
            <div class="form-group col-xs-12 col-md-2 medidas">
              <label>Largura</label>
              <div class="input-group">
                <input class="form-control numberOnly <?php echo $permitir_informar_medidas_em_decimal == 'true' ? 'campoMonetario': ''; ?>" onClick="this.select();" type="text" value="0" id="Md_Largura" name="Md_Largura" />
                <span class="input-group-addon">CM</span>
              </div>
            </div>
            <div class="form-group col-xs-12 col-md-2 medidas">
              <label>Altura</label>
              <div class="input-group">
                <input class="form-control numberOnly <?php echo $permitir_informar_medidas_em_decimal == 'true' ? 'campoMonetario': ''; ?>" onClick="this.select();" type="text" value="0" id="Md_Altura" name="Md_Altura" />
                <span class="input-group-addon">CM</span>
              </div>
            </div>
            <input class="form-control" type="hidden" min="0" step="0.1" value="0" id="VL_ADICIONAIS" name="VL_ADICIONAIS" />
            <input class="form-control" type="hidden" min="0" step="0.1" value="0" id="Vl_Moldura" name="Vl_Moldura" />

            <div class="clearfix"></div>
            <div class="form-group col-xs-12 col-md-6">
              <label>Observações referente ao objeto -
                <span class="small">
                  <strong>ATENÇÃO: Estas informações serão exibidas para o cliente.</strong>
                </span>
              </label>
              <div class="input-group col-xs-10">
                <textarea placeholder="Insira informações referente ao objeto como rasuras, partes danificadas, manuseio, etc." class="form-control col-xs-12" name="Ds_Observacao" id="Ds_Observacao"></textarea>
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-6">
              <label>Observações para produção -
                <span class="small">
                  <strong>Estas informações NÃO são exibidas aos clientes</strong>
                </span>
              </label>
              <div class="input-group col-xs-10">
                <textarea placeholder="Insira informações técnicas ou mais detalhes para a produção." class="form-control col-xs-12" name="Ds_ObservacaoProducao" id="Ds_ObservacaoProducao"></textarea>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="box-body">
              <div class="form-group col-xs-12 col-md-4">
                <label>Produto:</label>
                <div class="input-group" style="width:100%;">
                  <select class="form-control moldura" style="width:100%;">
                    <option value=""></option>
                    <?php foreach ($produtos as $produto) {
                      $cor = '#000000';
                      $labelStock = '';

                      if ($this->desabilitar_controle_estoque == false) {
                        $quantidadeEstoque = $produto->DescricaoUnidadeMedida != 'UN' ? ($produto->DescricaoUnidadeMedida == 'ML' ? $produto->QuantidadeCm : $produto->Quantidade) : $produto->Quantidade;

                        if ($quantidadeEstoque <= $produto->QuantidadeMinimaCm) {
                          if ($quantidadeEstoque <= 0) {
                            $cor = '#ff0000';
                            $labelStock = ' (Sem estoque)';
                          } else {
                            $cor = '#ed7014';
                            $labelStock = ' (Estoque baixo)';
                          }
                        }
                      }

                      if ($produto->Situacao == '2') {
                        $cor = $this->cor_produto_descontinuado;
                        $labelStock = ' (Produto descontinuado)';
                      }
                      $stockColor = sprintf(
                        'color: %s;',
                        $cor
                      );
                    ?>
                      <option data-style="<?php echo $stockColor; ?>" value="<?php echo $produto->CodigoProduto; ?>"><?php echo $produto->NovoCodigo; ?> - <?php echo $produto->DescricaoProduto; ?><?php echo $labelStock; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-2">
                <label>&nbsp;</label>
                <button class="form-control btn btn-primary" type="button" id="btnIncluirMoldura">Adicionar
                  Produto</button>
              </div>

              <div class="form-group col-xs-12 col-md-4">
                <label>Componentes:</label>
                <div class="input-group" style="width:100%;">
                  <select id="selecionaComponentes" class="form-control componente" style="width:100%;">
                    <option value=""></option>
                    <?php foreach ($componentes as $componente) { ?>
                      <option value="<?php echo $componente->CodigoProduto; ?>" data-sigla-unidade-medida="<?php echo $componente->DescricaoUnidadeMedida; ?>" data-quantidade-estoque="<?php echo $componente->Quantidade; ?>"><?php echo $componente->NovoCodigo . ' - ' . $componente->DescricaoProduto; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-2">
                <label>&nbsp;</label>
                <button class="form-control btn btn-primary" type="button" id="btnIncluirComponente">Adicionar
                  Componente</button>
              </div>
            </div>

            <div class="box-body">
              <div class="col-sm-6  table-responsive">
                <table id="tabelaMoldura" class="table table-bordered table-striped dataTable" role="grid">
                  <thead>
                    <tr role="row">
                      <th colspan="5">Produtos selecionados</th>
                    </tr>
                    <tr role="row">
                      <th>Produto</th>
                      <th>Complexidade</th>
                      <th>Sarrafo de Reforço?</th>
                      <th> Ordem de emolduramento </th>
                      <th>Remover Item</th>
                    </tr>
                  </thead>
                  <tbody class="input_molduras">
                  </tbody>
                </table>
              </div>

              <div class="col-sm-6  table-responsive">
                <table id="tabelaComponente" class="table table-bordered table-striped dataTable" role="grid">
                  <thead>
                    <tr role="row">
                      <th colspan="3">Componentes selecionados</th>
                    </tr>
                    <tr role="row">
                      <th>Componente</th>
                      <th>Quantidade Unitária</th>
                      <th>Remover Item</th>
                    </tr>
                  </thead>
                  <tbody class="input_componentes">
                  </tbody>
                </table>
              </div>

            </div>
            <div class="form-group col-xs-12">
              <button type="button" class="btn btn-info" id="btnIncluirQuadro">Salvar Item</button>
            </div>

          </div>

          <div class="box-body">
            <div class="col-sm-12  table-responsive">
              <table id="tabelaImagem" class="table table-bordered table-striped dataTable" role="grid">
                <thead>
                  <tr role="row">
                    <th>Objeto</th>
                    <th>Qtd</th>
                    <th>Larg</th>
                    <th>Alt</th>
                    <th>Extras (R$) </th>
                    <th>Fator de Cálculo (%)</th>
                    <th>Valor Unitário</th>
                    <th>Valor Item</th>
                    <th>OBS</th>
                    <th>OBS - Produção - Ambiente</th>
                    <?php 
                      if ($orcamentos[0]->Id_Situacao <= $situacao_bloquear_edicao || $arrPermissoesCabecalho[$indicePermitirModificarValoresPedidos]->visualizar == 1) { ?>
                      <th>Editar</th>
                      <th>Excluir</th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $aux = 0;
                  if (isset($itemsOrcamento) && count($itemsOrcamento) > 0) {
                    foreach ($itemsOrcamento as $item_orcamento) {
                      echo sprintf('<tr id="imagem%s">', $item_orcamento->Cd_Item_Orcamento);
                      echo sprintf('<td><input type="hidden" name="Cd_Prod_Aux[%s]" id="Cd_Prod_Aux-%1$s" value="%s">%s</td>', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->Cd_Prod_Aux, $item_orcamento->descricao);
                      echo sprintf('<td><input type="hidden" name="Qt_Item[%s]" id="Qt_Item-%1$s" value="%s">%2$s</td>', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->Qt_Item);
                      echo sprintf('<td><input type="hidden" name="Md_Largura[%s]" id="Md_Largura-%1$s" value="%s">%2$s</td>', $item_orcamento->Cd_Item_Orcamento, number_format($item_orcamento->Md_Largura,$permitir_informar_medidas_em_decimal == 'true' ? 2 : 0,",","."));
                      echo sprintf('<td><input type="hidden" name="Md_Altura[%s]" id="Md_Altura-%1$s" value="%s">%2$s %s </td>', $item_orcamento->Cd_Item_Orcamento, number_format($item_orcamento->Md_Altura,$permitir_informar_medidas_em_decimal == 'true' ? 2 : 0,",","."), (sprintf('<input type="hidden" name="medida_final[%s]" id="medida_final-%1$s" value="%s">', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->medida_final)));
                      echo sprintf('<td><input ' .$desabilita_campos. ' type="text" name="VL_ADICIONAIS[%s]" id="VL_ADICIONAIS-%1$s" value="%s" class="form-control campoMonetario recalcula"></td>', $item_orcamento->Cd_Item_Orcamento, number_format($item_orcamento->VL_ADICIONAIS, 2, ",", "."));
                      echo sprintf('<td><input ' .$desabilita_campos. ' type="text" name="Vl_Moldura[%s]" id="Vl_Moldura-%1$s" value="%s" class="form-control campoMonetario recalcula"></td>', $item_orcamento->Cd_Item_Orcamento, number_format($item_orcamento->Vl_Moldura, 2, ",", "."));
                      echo sprintf('<td><input type="hidden" name="Vl_Unitario[%s]" id="Vl_Unitario-%1$s" value="%s"><span id="Vl_Unitario-Text-%1$s">R$ %2$s</span>%s<input type="hidden" name="valor_custo[%1$s]" id="valor_custo-%1$s" value="%s"></td>', $item_orcamento->Cd_Item_Orcamento, number_format($item_orcamento->Vl_Unitario, 2, '.', ''), ($exibir_total_custo_item && $item_orcamento->valor_custo > 0 ? '<br><span class="small" id="valor_total_custo-Text-'.$item_orcamento->Cd_Item_Orcamento.'">Preço de custo R$ ' . $item_orcamento->valor_custo. '</span>' : ''),$item_orcamento->valor_custo);
                      echo sprintf('<td><input type="hidden" name="Vl_Total[%s]" id="Vl_Total-%1$s" value="%s"><span id="Vl_Total-Text-%1$s">R$ %2$s</span></td>', $item_orcamento->Cd_Item_Orcamento, number_format($item_orcamento->Vl_Bruto, 2, '.', ''));
                      echo sprintf('<td><input type="hidden" name="Ds_Observacao[%s]" id="Ds_Observacao-%1$s" value="%s">%2$s</td>', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->Ds_Observacao);
                      echo sprintf('<td><input type="hidden" name="Ds_ObservacaoProducao[%s]" id="Ds_ObservacaoProducao-%1$s" value="%s">%2$s</td>', $item_orcamento->Cd_Item_Orcamento, $item_orcamento->Ds_ObservacaoProducao);
                      if ($orcamentos[0]->Id_Situacao <= $situacao_bloquear_edicao || $arrPermissoesCabecalho[$indicePermitirModificarValoresPedidos]->visualizar == 1) {
                        echo sprintf('<td><a class="carrega_item_orcamento" value="%s"><i class="fa fa-edit"></i>Editar</a></td>', $item_orcamento->Cd_Item_Orcamento);
                        echo sprintf('<td><a class="delete_item_orcamento" value="%s"><i class="fa fa-trash" aria-hidden="true"></i>Excluir</a></td>', $item_orcamento->Cd_Item_Orcamento);
                      }
                      echo '</tr>';
                      //$aux++;
                      echo sprintf('<tr id="molduras-imagem%s">', $item_orcamento->Cd_Item_Orcamento);
                      if (is_array($arrMoldurasItemOrcamento) && count($arrMoldurasItemOrcamento) > 0 && isset($arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento])) {
                        echo '<td colspan="3">Produtos:';
                        echo '<ul>';
                        foreach ($arrMoldurasItemOrcamento[$item_orcamento->Cd_Item_Orcamento] as $indice => $molduras_item_orcamento) {
                          $codigoComplexidade = 0;
                          if (isset($molduras_item_orcamento['codigoComplexidade'])) {
                            $codigoComplexidade = $molduras_item_orcamento['codigoComplexidade'];
                          }
                          $nomeComplexidade = '';
                          if (isset($molduras_item_orcamento['nomeComplexidade'])) {
                            $nomeComplexidade = $molduras_item_orcamento['nomeComplexidade'];
                          }

                          $sarrafoReforco = $molduras_item_orcamento['sarrafo_reforco'] != 'N' ? 'Reforço: ' . ($molduras_item_orcamento['sarrafo_reforco'] == 'H' ? 'Horizontal' : 'Vertical') : '';

                          $exibeValorIndividual = $this->exibir_valores_individuais ? sprintf('R$ %s', number_format($molduras_item_orcamento['valor_produto_com_adicional'],2,",", "")) : '';

                          echo '<li><input data-moldura="' . $molduras_item_orcamento['Cd_Produto'] . '" type="hidden" text="' . (trim($molduras_item_orcamento['DescricaoProduto']) != '' ? $molduras_item_orcamento['DescricaoProduto'] : $molduras_item_orcamento['NovoCodigo']) . '" id="moldura-' . $molduras_item_orcamento['Cd_Produto'] . '" class="molduras-' . $item_orcamento->Cd_Item_Orcamento . '" name="Cd_Produto[' . $item_orcamento->Cd_Item_Orcamento . '][]" value="' . $molduras_item_orcamento['Cd_Produto'] . '|' . $codigoComplexidade . '|' . $molduras_item_orcamento['sarrafo_reforco'] . '|' . $molduras_item_orcamento['valor_produto'] . '" sarrafoReforco="' . $molduras_item_orcamento['sarrafo_reforco'] . '" complexidade="' . $codigoComplexidade . '">' . $molduras_item_orcamento['NovoCodigo'] . '-' . $molduras_item_orcamento['DescricaoProduto'] . '(' . $nomeComplexidade . $sarrafoReforco . ') - <span id="preco_produto_'.$molduras_item_orcamento['Cd_Produto'].'_0_'.$indice.'">' . $exibeValorIndividual . '</span></li>';
                        }
                        echo '</ul>';
                        if ($temProdutoComMedida && $exibir_medida_final_do_quadro && $item_orcamento->medida_final != '') {
                          echo "<br> <strong>MEDIDA FINAL: $item_orcamento->medida_final </strong>";
                        }
                        echo '</td>';
                      }

                      if (isset($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento])) {
                        echo '<td colspan="3">Componentes:';
                        echo '<ul>';
                        foreach ($arrComponentesItemOrcamento[$item_orcamento->Cd_Item_Orcamento] as $indice => $componentes_item_orcamento) {
                          $exibeValorIndividual = $this->exibir_valores_individuais ? sprintf('R$ %s', number_format($componentes_item_orcamento['valor_produto_com_adicional'],2,",", "")) : '';
                          $quantidade = isset($componentes_item_orcamento['quantidade']) ? $componentes_item_orcamento['quantidade'] : 1;
                          echo '<li><input quantidade="' . $quantidade . '" type="hidden" text="' . $componentes_item_orcamento['Descricao'] . '" id="componente-' . $componentes_item_orcamento['Id_Componente'] . '" class="componentes-' . $item_orcamento->Cd_Item_Orcamento . '" name="CD_Componente[' . $item_orcamento->Cd_Item_Orcamento . '][]" value="' . $componentes_item_orcamento['Id_Componente'] . '|' . $quantidade . '|' . $componentes_item_orcamento['valor_produto'] . '">' . $componentes_item_orcamento['Descricao'] . ' - <span id="preco_produto_'.$componentes_item_orcamento['Id_Componente'].'_1_'.$indice.'">' . $exibeValorIndividual . '</span></li>';
                        }
                        echo '</ul>';
                        echo '</td>';
                      }

                      echo '</tr>';
                      $aux = $item_orcamento->Cd_Item_Orcamento;
                    }
                    $aux++;
                    echo '<tr id="imagem' . $aux . '"></tr>';
                    echo '<tr id="molduras-imagem' . $aux . '"></tr>';
                  } else {
                  ?>
                    <tr id='imagem<?php echo $aux; ?>'></tr>
                    <tr id='molduras-imagem<?php echo $aux; ?>'></tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:bold;font-size:16px;">
              <div>Observações gerais do pedido:</span><br>
                <textarea class="form-control col-xs-12" name="Ds_Observacao_Pedido" id="Ds_Observacao_Pedido"><?php echo isset($orcamentos[0]->Ds_Observacao_Pedido) == true ? $orcamentos[0]->Ds_Observacao_Pedido : ''; ?></textarea>

              </div>
            </div>

            <div class="col-md-6 col-xs-12 col-sm-12" style="font-weight:bold;font-size:16px;">
              <div>Observações para a produção:</span><br>
                <textarea class="form-control col-xs-12" name="Ds_Observacao_Producao" id="Ds_Observacao_Producao"><?php echo isset($orcamentos[0]->Ds_Observacao_Producao) == true ? $orcamentos[0]->Ds_Observacao_Producao : ''; ?></textarea>

              </div>
            </div>

            <div class="col-md-12" style="font-weight:bold;font-size:16px;">
              <div>SubTotal: <span id="totalPedido">
                  <?php echo isset($orcamentos[0]->Vl_Bruto) ? number_format($orcamentos[0]->Vl_Bruto, 2, ".", "") : ''; ?>
                </span>
                <input type="hidden" name="Vl_Bruto" id="Vl_Bruto" value="<?php echo isset($orcamentos[0]->Vl_Bruto) ? number_format($orcamentos[0]->Vl_Bruto, 2, ".", "") : ''; ?>">
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-4">
              <div class="input-group">
                <label>Tipo de desconto</label>
                <select <?php echo $desabilita_campos; ?> class="form-control" name="tipo_desconto" id="tipo_desconto">
                  <?php $tipo_desconto = isset($orcamentos[0]->tipo_desconto) ? $orcamentos[0]->tipo_desconto : ''; ?>
                  <option <?php echo $tipo_desconto == 'P' ? 'selected="selected"' : ''; ?> value="P">Percentual</option>
                  <option <?php echo $tipo_desconto == 'M' ? 'selected="selected"' : ''; ?> value="M">R$</option>
                </select>
              </div>
            </div>
            <div class="form-group col-xs-12 col-md-4">
              <div class="input-group">
                <label id="labelValorDesconto">Valor</label>
                <input <?php echo $desabilita_campos; ?> class="form-control numberOnly campoMonetario" type="text" value="<?php echo number_format($orcamentos[0]->VL_desconto, 2, ",", ""); ?>" id="VL_desconto" name="VL_desconto" />
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-4">
              <label>Total R$</label>
              <div class="input-group">
                <input id="totalLiquido" readonly class="form-control" type="text" value="<?php echo number_format($orcamentos[0]->vl_liquido, 2, ".", ""); ?>">
                <input type="hidden" name="vl_liquido" id="vl_liquido" value="<?php echo (float) $orcamentos[0]->vl_liquido; ?>">
              </div>
            </div>

          </div>

          <div class="box-body">

            <div class="form-group col-xs-12 col-md-4">
              <label>Entrada </label>
              <div class="input-group">
                <span class="input-group-addon">R$</span>
                <input <?php echo $desabilita_campos; ?> class="form-control numberOnly campoMonetario" type="text" min="0" step="1" value="<?php echo number_format($orcamentos[0]->Vl_Entrada, 2, ",", ""); ?>" id="Vl_Entrada" name="Vl_Entrada" />
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-2">
              <label>Forma Pagamento </label>
              <div class="input-group">
                <select <?php echo $desabilita_campos; ?> class="form-control" name="id_forma_pagamento_entrada" id="id_forma_pagamento_entrada">
                  <option value="0">Sem entrada</option>
                  <?php foreach ($this->formas_pagamento as $forma_pagamento) {
                    $selected = $forma_pagamento->id_forma_pagamento == $orcamentos[0]->id_forma_pagamento_entrada ? 'selected' : '';
                  ?>
                    <option <?php echo $selected; ?> data-quantidade-parcelas="<?php echo $forma_pagamento->parcelas; ?>" value="<?php echo $forma_pagamento->id_forma_pagamento; ?>"><?php echo $forma_pagamento->descricao; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-4">
              <label>Parcelas </label>
              <div class="input-group">
                <select <?php echo $desabilita_campos; ?> class="form-control" name="numero_parcelas_entrada" id="numero_parcelas_entrada">
                  <?php for ($i = 1; $i <= $quantidadeParcelasMaximoEntrada; $i++) {
                    echo sprintf('<option %s value="%s">%2$s</option>', ($i == $orcamentos[0]->numero_parcelas_saldo ? 'selected' : ''), $i);
                  } ?>
                </select>
              </div>
            </div>

          </div>
          <div class="box-body">
            <div class="form-group col-xs-12 col-md-4">
              <label>Saldo R$ </label>
              <div><span id="totalSaldo">
                  <?php echo number_format(($orcamentos[0]->saldo), 2, ",", "."); ?>
                </span>
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-2">
              <label>Forma Pagamento </label>
              <div class="input-group">
                <select class="form-control" name="id_forma_pagamento_saldo" id="id_forma_pagamento_saldo">
                  <option value="0">Em aberto</option>
                  <?php foreach ($this->formas_pagamento as $forma_pagamento) {
                    $selected = $forma_pagamento->id_forma_pagamento == $orcamentos[0]->id_forma_pagamento_saldo ? 'selected' : '';
                  ?>
                    <option <?php echo $selected; ?> data-quantidade-parcelas="<?php echo $forma_pagamento->parcelas; ?>" value="<?php echo $forma_pagamento->id_forma_pagamento; ?>"><?php echo $forma_pagamento->descricao; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-2">
              <label>Parcelas </label>
              <div class="input-group">
                <select class="form-control" name="numero_parcelas_saldo" id="numero_parcelas_saldo">
                  <?php for ($i = 1; $i <= $quantidadeParcelasMaximoSaldo; $i++) {
                    echo sprintf('<option %s value="%s">%2$s</option>', ($i == $orcamentos[0]->numero_parcelas_saldo ? 'selected' : ''), $i);
                  } ?>
                </select>
              </div>
            </div>

            <div class="form-group col-xs-12 col-md-1">

              <label for="Pago">Pago ?
                <input type="checkbox" id="Pago" name="Pago" <?php echo $orcamentos[0]->Pago == 1 ? 'checked="checked"' : ''; ?> value="1" />
              </label>
            </div>

            <div class="form-group col-xs-12 col-md-3">
              <label>Data de pagamento</label>
              <div class="input-group date">
                <?php
                $dataPagamento = '';
                if (!empty($orcamentos[0]->data_pagamento)) {
                  $DataPagamento = new DateTime($orcamentos[0]->data_pagamento);
                  $dataPagamento = $DataPagamento->format('d/m/Y');
                }
                ?>
                <input class="form-control data" type="text" value="<?php echo $dataPagamento; ?>" id="data_pagamento" name="data_pagamento" />
              </div>
            </div>

          </div>

          <div class="box-footer" id="acoes">
            <div class="row" style="margin-left:0px">
              <input type="hidden" name="handle" value="">
              <?php if ($arrPermissoesCabecalho[$indicePedidos]->editar == 1) { ?>
                <button class="col-md-1 col-xs-12 btn btn-primary" id="btnSalvarPedido" type="button"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;Salvar</button>
              <?php } ?>
              <a class="col-md-1 col-xs-12  btn btn-primary" href="<?php echo URL; ?><?php echo $classe; ?>/listar/"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Voltar</a>
              <?php if (isset($orcamentos[0]->Cd_Orcamento) && $orcamentos[0]->Cd_Orcamento > 0) { ?>
                <button class="col-md-2 col-xs-12 btn btn-primary" style="display:none;" id="btnPedidoEnviarWhatsapp" type="button"><i class="fa fa-whatsapp" aria-hidden="true"></i>&nbsp;Enviar por whatsapp</button>
                <a class="btn btn-primary col-md-1 col-xs-12" style="display:none;" id="abreWhatsappIphone" href="<?php echo URL . 'Whatsapp/enviarMensagemIphone/' . $orcamentos[0]->Cd_Orcamento ?>" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i>&nbsp;Enviar por whatsapp</a>
                <button class="col-md-2 col-xs-12 btn btn-primary" id="btnPedidoEnviarEmail" type="button"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;Enviar por email</button>
                <button class="col-md-1 col-xs-12 btn btn-primary pedidoImprimir" id="btnPedidoImprimir" data-alvo="pedido" type="button"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Imprimir</button>
                <?php if (isset($orcamentos[0]->Id_Situacao) && $orcamentos[0]->Id_Situacao >= 2) { ?>
                  <button class="col-md-2 col-xs-12 btn btn-primary" id="btnPedidoImprimirOs" type="button"><i class="fa fa-reorder" aria-hidden="true"></i>&nbsp;Ordem de Serviço</button>
                  <?php if ($this->ambiente == 'moldurascuritiba') {  ?>
                  <button class="col-md-2 col-xs-12 btn btn-primary pedidoImprimir" id="btnPedidoImprimirPedidoEOs" data-alvo="pedidoeos" type="button"><i class="fa fa-reorder" aria-hidden="true"></i>&nbsp;Imprimir Pedido e OS</button>
                  <?php } ?>

                  <?php if ($this->habilita_tributacao) { ?>
            </div>
            <div class="row" style="padding-top: 10px;margin-left:0px;">
              <!-- NOTA DE PRODUTO -->
              <?php if ($this->habilitar_emissao_nf == true) { ?>
                <?php if ($orcamentos[0]->nf_situacao === 'Autorizado' && $orcamentos[0]->nf_devolucao == 0) { ?>
                  <a role="button" class="col-md-2 col-xs-12 btn btn-success" href="<?php echo URL . 'IntegraNota/downloadNota/' . $orcamentos[0]->nf_id_documento ?>" target="_blank">
                    <i class="fa fa-file-pdf-o"></i> Baixar PDF Nota de Produto
                  </a>
                <?php } else { ?>
                  <button class="col-md-2 col-xs-12 btn btn-success gerarDocumentoFiscal" data-id-documento="" data-tipo-documento="NF" id="btnGerarNFe" data-quantidade-empresa="<?= count($this->empresas); ?>" type="button"><i class="fa fa-sticky-note-o" aria-hidden="true"></i>&nbsp;Gerar Nota de Produto </button>
                <?php } ?>
              <?php } ?>
              <!-- FIM NOTA DE PRODUTO -->
              <!-- NOTA SERVIÇO -->
              <?php if ($this->habilitar_emissao_nfs == true) { ?>
                <?php if ($orcamentos[0]->nfs_situacao === 'Autorizado') { ?>
                  <a role="button" class="col-md-2  col-xs-12 btn btn-success" href="<?php echo URL . 'IntegraNotaServico/downloadNota/' . $orcamentos[0]->nfs_id_documento ?>" target="_blank">
                    <i class="fa fa-file-pdf-o"></i> Baixar PDF Nota de Serviço
                  </a>
                <?php } else { ?>
                  <button class="col-md-2 col-xs-12 btn btn-success gerarDocumentoFiscal" data-id-documento="" data-tipo-documento="NFS" id="btnGerarNFSe" data-quantidade-empresa="<?= count($this->empresas); ?>" type="button"><i class="fa fa-sticky-note-o" aria-hidden="true"></i>&nbsp;Gerar Nota Serviço </button>
                <?php } ?>
              <?php } ?>
              <!-- FIM NOTA SERVIÇO -->

              <!-- NOTA CONSUMIDOR -->
              <?php if ($this->habilitar_emissao_nfc == true) { ?>
                <?php if ($orcamentos[0]->nfc_situacao === 'Autorizado') { ?>
                  <a role="button" class="col-md-2  col-xs-12 btn btn-success" href="<?php echo URL . 'IntegraNotaConsumidor/downloadNota/' . $orcamentos[0]->nfc_id_documento ?>" target="_blank">
                    <i class="fa fa-file-pdf-o"></i> Baixar PDF Nota Consumidor
                  </a>
                <?php } else { ?>
                  <button class="col-md-2 col-xs-12 btn btn-success gerarDocumentoFiscal" data-id-documento="" data-tipo-documento="NFC" id="btnGerarNFCe" data-quantidade-empresa="<?= count($this->empresas); ?>" type="button"><i class="fa fa-sticky-note-o" aria-hidden="true"></i>&nbsp;Gerar Nota Consumidor </button>
                <?php } ?>
              <?php } ?>
              <!-- FIM NOTA CONSUMIDOR -->
            <?php } ?>
            <?php
                  if ($this->habilita_financeiro) {
                    if ($orcamentos[0]->idContaReceber == 0) { ?>
                <button class="col-md-2 col-xs-12  btn btn-success" data-target="#gerarFinanceiro" data-toggle="modal" type="button"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;Gerar Financeiro</button>
              <?php } else { ?>
                <a role="button" class="col-md-2 col-xs-12 btn btn-success" href="<?php echo URL . 'Assets/editar/' . $orcamentos[0]->idContaReceber ?>" target="_blank"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;Visualizar Conta a receber</a>
              <?php } ?>
            <?php } ?>
            </div>

        <?php }
              }
        ?>
          </div>
      </div>
      </form>
    </div>
  </div>
  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="cadastrarCliente" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Cadastro de cliente</h4>
      </div>
      <div class="modal-body">
        <div class="box">
          <div class="box-body">
            <div class="form-group col-xs-12 col-md-1">
              <label>Tipo de cliente</label>
              <select name="TipoCliente" id="TipoCliente" class="form-control tipoCliente">
                <option value="F" selected="selected">Física</option>
                <option value="J">Jurídica</option>
              </select>
            </div>
            <div class="form-group col-xs-12 col-md-3">
              <label class="nomeRazao">Nome</label>
              <input class="form-control" id="RazaoSocial" type="text" value="" name="RazaoSocial" />
            </div>

            <div class="form-group col-xs-12 col-md-3 nomeFantasia">
              <label class="">Nome Fantasia</label>
              <input class="form-control" type="text" id="NomeFantasia" name="NomeFantasia" value="">
            </div>
            <div class="form-group col-xs-12 col-md-2">
              <label class="documentoCliente">
                CPF
              </label>
              <input class="form-control" id="CGC" type="text" name="CGC" value="">
            </div>

            <div class="form-group col-xs-12 col-md-2 inscricaoEstadual" style="display:none">
              <label class="">Inscrição Estadual</label>
              <input class="form-control" type="text" id="InscricaoEstadual" name="InscricaoEstadual" value="">
            </div>
          </div>

          <div class="box-body">

            <div class="form-group col-xs-12 col-md-3">
              <label>E-mail</label>
              <input class="form-control" type="email" id="EMail" name="EMail" value="">
            </div>

            <div class="form-group col-xs-12 col-md-3">
              <label>Telefone</label>
              <span class="small">Também pode ser o número de whatsapp</span>
              <input class="form-control" type="text" id="Telefone1" name="Telefone1" value="">
            </div>
            <div class="form-group col-xs-12 col-md-3">
              <label>Telefone Alternativo</label>
              <input class="form-control" type="text" id="Telefone2" name="Telefone2" value="">
            </div>
          </div>
          <div class="box-body">
            <div class="form-group col-xs-12 col-md-2">
              <label>CEP</label>
              <input class="form-control" type="text" name="CEP" id="CEP" value="">
            </div>

            <div class="form-group col-xs-12 col-md-2">
              <label>Logradouro</label>
              <select name="idLogradouro" id="idLogradouro" class="form-control">
                <?php foreach ($this->arrLogradouros as $key => $value) { ?>
                  <option value="<?php echo $value->idLogradouro; ?>" <?php echo $value->descricao == 'Rua' ? 'selected' : ''; ?>><?php echo $value->descricao; ?></option>
                <?php } ?>
              </select>
            </div>

            <div class="form-group col-xs-12 col-md-6">
              <label>Endereço</label>
              <input class="form-control" type="text" id="Endereco" name="Endereco" value="">
            </div>

            <div class="form-group col-xs-12 col-md-2">
              <label>Número</label>
              <input class="form-control" type="text" id="numeroEndereco" name="numeroEndereco" value="">
            </div>


            <div class="form-group col-xs-12 col-md-6">
              <label>Complemento</label>
              <input class="form-control" type="text" id="Complemento" name="Complemento" value="">
            </div>

            <div class="form-group col-xs-12 col-md-6">
              <label>Bairro</label>
              <input class="form-control" type="text" id="Bairro" name="Bairro" value="">
            </div>

          </div>
          <div class="box-body">
            <div class="form-group col-xs-12 col-md-4">
              <label>Estado</label>
              <select name="Estado" id="Estado" class="form-control">
                <option value="">Selecione</option>
                <?php foreach ($this->estados as $estado_sigla => $estado_descricao) { ?>
                  <option value="<?php echo $estado_sigla; ?>"><?php echo $estado_descricao; ?>
                  </option>
                <?php } ?>
              </select>
            </div>

            <div class="form-group col-xs-12 col-md-4">
              <label>Cidade</label>
              <select name="Cidade" id="Cidade" class="form-control">
              </select>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group col-xs-12 col-md-6">
              <label>Grupo do cliente</label>
              <select name="codigoClienteGrupo" id="codigoClienteGrupo" class="form-control">
                <?php foreach ($clienteGrupos as $grupo) { ?>
                  <option value="<?php echo $grupo->codigoClienteGrupo; ?>" <?php echo $grupo->nome === 'Padrão' ? 'selected' : ''; ?>><?php echo $grupo->nome; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
        <button type="button" id="btnEnviarCadastroCliente" class="btn btn-primary">Salvar</button>
        <p class="alertaCadastroCliente" style="color:green;font-weight:bold;display:none;"></p>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="gerarNotaFiscal" style="display: none;" data-status="">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Emissão de Nota Documento Fiscal</h4>
        <div class="alert alert-dismissible alertaAlteracaoPreco" style="display:none">
          <div id="mensagemAlteracaoPreco"></div>
        </div>
      </div>
      <div class="modal-body">
        <form action="#" method="POST" id="frmEmitirNFe">
          <div class="form-group">
            <label for="id_empresa_nfe">Empresa</label>
            <div class="input-group">
              <select name="id_empresa_nfe" class="form-control" id="id_empresa_nfe">
                <?php foreach ($this->empresas as $empresa) { ?>
                  <option <?php echo $empresa->padrao == '1' ? 'selected' : ''; ?> value="<?php echo $empresa->CodigoEmpresa; ?>"><?php echo $empresa->CGC . ' - ' . $empresa->RazaoSocial; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <?php /* ?>
        <div class="form-group">
          <label for="email_nfe">
            Informe os e-mails adicionais para envio da Nota Fiscal
            <p class="small">Separados por ';' (ponto e vírgula)</p>
          </label>
          <div class="input-group col-xs-10">
            <input class="form-control" type="text" value="" id="email_nfe" name="email_nfe" />
          </div>      
        </div>
        */ ?>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
        <button type="button" id="btnEmitirNfeModal" class="btn btn-primary">Gerar Documento</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="gerarFinanceiro" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Gerar Lançamento Financeiro</h4>
        <div class="alert alert-dismissible alertaAlteracaoPreco" style="display:none">
          <div id="mensagemAlteracaoPreco"></div>
        </div>
      </div>
      <div class="modal-body" id="modalBodyGerarFinanceiro">
        <form action="#" method="POST" id="frmEmitirFinanceiro">

          <!-- LANÇAMENTO ENTRADA -->

          <div class="lancamento-financeiro-entrada">
            <h4> Lançamento - Entrada </h4>
            <div class="box-body">
              <div class="form-group col-xs-12 col-md-4">
                <label>Data Lançamento</label>
                <div class="input-group data-modal">
                  <input class="form-control data dataModal entrada" type="text" id="date" name="date" value="<?php echo date('d/m/Y'); ?>">
                </div>
              </div>
              <?php if ($arrPermissoesCabecalho[$permiteSalvarDataVencimento]->cadastrar == '1') { ?>
                <div class="form-group col-xs-12 col-md-4">
                  <label>Data de vencimento</label>
                  <div id="dataVencimento" class="input-group">
                    <input class="form-control data dataModal entrada" id="cdtvencimento" type="text" name="cdtvencimento" value="<?php echo date('d/m/Y'); ?>">
                  </div>
                </div>
              <?php } else {
                echo '<input class="entrada" id="cdtvencimento" name="cdtvencimento" type="hidden" value="">';
              } ?>
              <?php if ($arrPermissoesCabecalho[$permiteSalvarDataPagamento]->cadastrar == '1') { ?>
                <div class="form-group col-xs-12 col-md-4">
                  <label>Data de pagamento</label>
                  <div class="input-group dataModal">
                    <input class="form-control data entrada" type="text" id="cdtpagamento" name="cdtpagamento" value="">
                  </div>
                </div>
              <?php } else {
                echo '<input class="entrada" id="cdtpagamento" name="cdtpagamento" type="hidden" value="">';
              } ?>

            </div>

            <div class="box-body">
              <div class="form-group col-xs-12 col-md-3">
                <label>Categoria</label>
                <div class="input-group col-md-12">
                  <select class="form-control entrada" name="CategoryId" id="CategoryId">
                    <?php
                    foreach ($this->categoriasContasReceber as $categoria) {
                      echo sprintf('<option value="%s">%s</option>', $categoria->CategoryId, $categoria->CategoryName);
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-3">
                <label>Valor</label>
                <div class="input-group">
                  <span class="input-group-addon">R$</span>
                  <input class="form-control campoMonetario entrada" type="text" id="valor_entrada" readonly name="Amount" value="<?php echo number_format(($orcamentos[0]->Vl_Entrada), 2, ",", "."); ?>">
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-3">
                <label>Conta</label>
                <div class="input-group col-md-12">
                  <select class="form-control entrada" id="AccountId" name="AccountId">
                    <?php
                    foreach ($this->contas as $conta) {
                      echo sprintf('<option %s value="%s">%s</option>', $conta->AccountId == '19' ? 'selected' : '', $conta->AccountId, $conta->AccountName);
                    }
                    ?>
                  </select>
                </div>
              </div>

              <?php if (count($this->empresas) > 1 && (count($orcamento->arrPermissoesUsuarioEmpresa) < 1 || count($orcamento->arrPermissoesUsuarioEmpresa) == 0)) { ?>
                <div class="form-group col-xs-12 col-md-3">
                  <label>Empresa</label>
                  <select class="form-control entrada" name="UserId" id="UserId">
                    <?php
                    foreach ($this->empresas as $empresa) {
                      $selected = isset($orcamentos[0]->id_empresa) && $empresa->CodigoEmpresa == $orcamentos[0]->id_empresa ? 'selected' : '';
                      echo sprintf('<option %s value="%s">%s</option>', $selected, $empresa->CodigoEmpresa, $empresa->RazaoSocial);
                    }
                    ?>
                  </select>
                </div>
              <?php } else {
                $handle_empresa_padrao = isset($orcamento->arrPermissoesUsuarioEmpresa[0]) ? $orcamento->arrPermissoesUsuarioEmpresa[0] : $this->empresas[0]->CodigoEmpresa;
              ?>
                <input type="hidden" class="entrada" name="UserId" value="<?php echo $handle_empresa_padrao; ?>" />
              <?php } ?>
            </div>

            <div class="box-body">
              <div class="form-group col-xs-12 col-md-12">
                <label>Observação</label>
                <div class="input-group col-md-12">
                  <textarea class="form-control entrada" rows="5" id="description" name="description"><?php echo ($orcamentos[0]->saldo == 0) ? 'REFERENTE AO PAGAMENTO DO VALOR TOTAL DO PEDIDO' : 'REFERENTE AO PAGAMENTO DA ENTRADA' ?></textarea>
                </div>
              </div>
            </div>
          </div>
          <!-- FIM LANÇAMENTO ENTRADA -->

          <hr>

          <!-- LANÇAMENTO SALDO -->

          <div class="lancamento-financeiro-saldo">
            <h4> Lançamento - Saldo </h4>
            <div class="box-body">
              <div class="form-group col-xs-12 col-md-4">
                <label>Data Lançamento</label>
                <div class="input-group data-modal">
                  <input class="form-control data dataModal saldo" type="text" id="date" name="date" value="<?php echo date('d/m/Y'); ?>">
                </div>
              </div>
              <?php if ($arrPermissoesCabecalho[$permiteSalvarDataVencimento]->cadastrar == '1') { ?>
                <div class="form-group col-xs-12 col-md-4">
                  <label>Data de vencimento</label>
                  <div id="dataVencimento" class="input-group">
                    <input class="form-control data dataModal saldo" id="cdtvencimento" type="text" name="cdtvencimento" value="<?php echo date('d/m/Y'); ?>">
                  </div>
                </div>
              <?php } else {
                echo '<input class="saldo" id="cdtvencimento" name="cdtvencimento" type="hidden" value="">';
              } ?>
              <?php if ($arrPermissoesCabecalho[$permiteSalvarDataPagamento]->cadastrar == '1') { ?>
                <div class="form-group col-xs-12 col-md-4">
                  <label>Data de pagamento</label>
                  <div class="input-group dataModal">
                    <input class="form-control data saldo" type="text" id="cdtpagamento" name="cdtpagamento" value="">
                  </div>
                </div>
              <?php } else {
                echo '<input class="saldo" id="cdtpagamento" name="cdtpagamento" type="hidden" value="">';
              }
              ?>

            </div>

            <div class="box-body">
              <div class="form-group col-xs-12 col-md-3">
                <label>Categoria</label>
                <div class="input-group col-md-12">
                  <select class="form-control saldo" name="CategoryId" id="CategoryId">
                    <?php
                    foreach ($this->categoriasContasReceber as $categoria) {
                      echo sprintf('<option value="%s">%s</option>', $categoria->CategoryId, $categoria->CategoryName);
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-3">
                <label>Valor</label>
                <div class="input-group">
                  <span class="input-group-addon">R$</span>
                  <input class="form-control campoMonetario saldo" type="text" id="valor_saldo" readonly name="Amount" value="<?php echo number_format(($orcamentos[0]->saldo), 2, ",", "."); ?>">
                </div>
              </div>
              <div class="form-group col-xs-12 col-md-3">
                <label>Conta</label>
                <div class="input-group col-md-12">
                  <select class="form-control saldo" id="AccountId" name="AccountId">
                    <?php
                    foreach ($this->contas as $conta) {
                      echo sprintf('<option %s value="%s">%s</option>', $conta->AccountId == '19' ? 'selected' : '', $conta->AccountId, $conta->AccountName);
                    }
                    ?>
                  </select>
                </div>
              </div>

              <?php if (count($this->empresas) > 1 && (count($orcamento->arrPermissoesUsuarioEmpresa) < 1 || count($orcamento->arrPermissoesUsuarioEmpresa) == 0)) { ?>
                <div class="form-group col-xs-12 col-md-3">
                  <label>Empresa</label>
                  <select class="form-control saldo" name="UserId" id="UserId">
                    <?php
                    foreach ($this->empresas as $empresa) {
                      $selected = isset($orcamentos[0]->id_empresa) && $empresa->CodigoEmpresa == $orcamentos[0]->id_empresa ? 'selected' : '';
                      echo sprintf('<option %s value="%s">%s</option>', $selected, $empresa->CodigoEmpresa, $empresa->RazaoSocial);
                    }
                    ?>
                  </select>
                </div>
              <?php } else {
                $handle_empresa_padrao = isset($orcamento->arrPermissoesUsuarioEmpresa[0]) ? $orcamento->arrPermissoesUsuarioEmpresa[0] : $this->empresas[0]->CodigoEmpresa;
              ?>
                <input type="hidden" class="saldo" name="UserId" value="<?php echo $handle_empresa_padrao; ?>" />
              <?php } ?>
            </div>

            <div class="box-body">
              <div class="form-group col-xs-12 col-md-12">
                <label>Observação</label>
                <div class="input-group col-md-12">
                  <textarea class="form-control saldo" rows="5" id="description" name="description">REFERENTE AO SALDO DEVEDOR</textarea>
                </div>
              </div>
            </div>
          </div>
          <!-- FIM LANÇAMENTO SALDO -->

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
        <button type="button" id="btnGerarFinanceiroSimplificado" class="btn btn-primary">Salvar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>