
      <?php include_once(DOCUMENT_ROOT."src/View/Common/cabecalho.php"); ?>
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <form method="POST" action="<?php echo URL.$classe."/".$metodo."/".($Plano_contas->id_plano_contas ?: 0);?>">
                  <?php if (!empty($msg_sucesso)) { ?>
                    <div class="callout callout-success">
                      <h4>Sucesso!</h4>
                      <p><?php echo $msg_sucesso;?></p>
                    </div>
                  <?php } ?>
                  <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Cadastro da sua Plano_contas</h3>
                    </div>
                  </div>
                  <div class="box-body">
          <div class="form-group col-xs-12 col-md-2">
            <label>id_plano_contas</label>
            <input class="form-control" type="text" name="id_plano_contas" value="<?php echo $Plano_contas->id_plano_contas;?>">
          </div>
        </div>
<div class="box-body">
          <div class="form-group col-xs-12 col-md-2">
            <label>classificacao</label>
            <input class="form-control" type="text" name="classificacao" value="<?php echo $Plano_contas->classificacao;?>">
          </div>
        </div>

                  <div class="box-footer">
                    <input type="hidden" name="handle" value="<?php echo $Plano_contas->id_plano_contas;?>">
                    <button class="btn btn-primary" type="submit">Salvar</button>
                    <a class="btn btn-primary" href="<?php echo URL.$classe.'/Listar/';?>">Voltar</a>
                  </div>                
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  
    