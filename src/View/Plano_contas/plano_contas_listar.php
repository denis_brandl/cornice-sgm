
      <?php include_once(DOCUMENT_ROOT."src/View/Common/cabecalho.php"); ?>  
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">					
                  <h3 class="box-title"></h3>					
                </div>
              </div>
              <div class="box-body">
                <div id="plano_contas-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-6">
                      <a class="btn btn-primary" href="<?php echo URL.$classe.'/cadastrar/';?>">Cadastrar Novo</a>
                    </div>
                    <div class="col-sm-6">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <table class="table table-bordered table-striped dataTable">
                        <thead>
                          <tr role="row">
                            
          <th>id_plano_contas</th>
        

          <th>classificacao</th>
        

                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $aux = 0;
                            foreach ($arrPlano_contas as $Plano_contas) {
                            if ($aux & 1) {
                          ?>
                            <tr class="odd" role="row">
                          <?php } else { ?>	
                            <tr class="even" role="row">
                          <?php } ?>	
                            
          <td><?php echo $Plano_contas->id_plano_contas; ?></td>
        

          <td><?php echo $Plano_contas->classificacao; ?></td>
        

                            <td><a class="" href="<?php echo URL.$classe.'/'.$acao.'/'. $Plano_contas->id_plano_contas; ?>"><i class="fa fa-edit"></i>Editar</a></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>									
                    </div>								
                  </div>                
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include_once(DOCUMENT_ROOT."src/View/Common/rodape.php"); ?>  
    