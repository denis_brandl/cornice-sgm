  <?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<form autocomplete="off"  role="form" method="POST" action="<?php echo URL;?>index.php?class=<?php echo $classe;?>&acao=<?php echo $metodo;?>">
						<?php if (!empty($msg_sucesso)) { ?>
							<div class="callout callout-success">
								<h4>Sucesso!</h4>
								<p><?php echo $msg_sucesso;?></p>
							</div>
						<?php } ?>
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Cadastro</h3>
							</div>						
						</div>
						<div class="box-body">
							<div class="form-group">
								<label>Nome</label>
								<input class="form-control" type="text" value="" name="NomeVendedor" />
							</div>

							<div class="form-group">
								<label>Telefone</label>
								<input class="form-control" type="text" value="" name="NomeVendedor" />
							</div>							
							
							<label>Valor Disponível</label>
							<div class="input-group col-lg-4">
								<input class="form-control" type="number" step="0.01" name="PercentualComissao" value="">
							</div>										
						</div>
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Histórico</h3>
							</div>						
						</div>

							<div class="row">
								<div class="col-sm-12">
									<table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Nome</th>
												<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 207px;" aria-label="Browser: activate to sort column ascending">Valor Disponível</th>
												<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 207px;" aria-label="Browser: activate to sort column ascending">&nbsp;</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="sorting_1">Pedido</td>
												<td class="sorting_1">Cliente</td>
												<td>Valor do serviço</td>
												<td>% Recebido</td>
											</tr>
										</tbody>
										<tfoot>
											<tr>
												<td class="sorting_1">Pedido</td>
												<td class="sorting_1">Cliente</td>
												<td>Valor do serviço</td>
												<td>% Recebido</td>
											</tr>
										</tfoot>
									</table>									
								</div>								
							</div>						
						
						<div class="box-footer">
							<input type="hidden" name="handle" value="<?php echo $afiliados[0]->CodigoVendedor;?>">
							<button class="btn btn-primary" type="submit">Salvar</button>
							<a class="btn btn-primary" href="<?php URL;?>?class=<?php echo $classe;?>&acao=listar">Voltar</a>
						</div>
					</form>
				</div>
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  