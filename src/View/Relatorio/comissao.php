<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>


<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
					</h3>
				</div>
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<form autocomplete="off" name="frmRelatorioPedidosEntregue" id="frmRelatorioComissao">
									<!-- 										<div class="form-group col-sm-6"> -->

									<label for="criado_por">Vendedor</label>
									<select class="form-control" id="vendedor" name="vendedor">
										<option value="">Todos</option>
										<?php
										foreach ($this->usuarios as $usuario) {
											$vendedor = (isset($arrFiltro['vendedor']) && !empty($arrFiltro['vendedor'])) ? $arrFiltro['vendedor'] : 0;
											echo sprintf(
												'<option %s value="%s">%s</option>',
												$usuario->CodigoUsuario == $vendedor ? 'selected' : '',
												$usuario->CodigoUsuario,
												$usuario->NomeUsuario
											);
										}
										?>
									</select>

									<label>Período Inicial:</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" value="<?php echo date('d/m/Y', strtotime($periodo_inicial)); ?>" id="periodo_inicial" name="periodo_inicial" class="form-control pull-right data">
									</div>
									<label>Período Final:</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" value="<?php echo date('d/m/Y', strtotime($periodo_final)); ?>" id="periodo_final" name="periodo_final" class="form-control pull-right data">
									</div>

									<div class="form-group">
										<label>Tipo de data</label>
										<select name="tipo_data[]" class="form-control" id="tipo_data">
											<option <?php echo isset($arrFiltro['tipo_data']) &&  $arrFiltro['tipo_data'] === 'Dt_Orcamento' ? 'selected' : ''; ?> value="Dt_Orcamento">Cadastro do orçamento</option>
										<option <?php echo isset($arrFiltro['tipo_data']) &&  $arrFiltro['tipo_data'] === 'cdtpagamento' ? 'selected' : ''; ?> value="cdtpagamento">Pagamento do pedido</option>
											<option <?php echo isset($arrFiltro['tipo_data']) &&  $arrFiltro['tipo_data'] === 'Dt_Efetiva_Entrega' ? 'selected' : ''; ?> value="Dt_Efetiva_Entrega">Efetiva de entrega</option>
											<option <?php echo isset($arrFiltro['tipo_data']) &&  $arrFiltro['tipo_data'] === 'Dt_Prevista_Entrega' ? 'selected' : ''; ?> value="Dt_Prevista_Entrega">Prevista para entrega</option>
										</select>
									</div>
									<div class="form-group">
										<label>Situações</label>
										<select name="filtro_situacao[]" class="form-control filtro_situacao" id="filtro_situacao" multiple="multiple">
											<?php
											foreach ($situacoes as $situacao) {
												$selected = "";

												if (isset($arrFiltro['filtro_situacao']) && in_array($situacao->idSituacao, explode(',', $arrFiltro['filtro_situacao']))) {
													$selected = "selected";
												}
											?>
												<option <?php echo $selected; ?> value="<?php echo $situacao->idSituacao; ?>"><?php echo $situacao->descricao; ?></option>
											<?php
											}
											?>
										</select>
									</div>
									<div style="clear:both;"></div>
									<!-- /.input group -->
									<input type="button" class="btn btn-primary" id="btnRelatorioComissao" value="Buscar" />
									<input type="button" class="btn btn-primary" id="imprimirRelatorioComissao" value="Imprimir" />
									<input type="button" class="btn btn-primary" id="exportarRelatorioComissaoPdf" value="Exportar PDF" />
									<!-- /.input group -->
									<!-- 										</div>												 -->

								</form>
							</div>
						</div>
						<div style="clear:both;"></div>
						<div class="row">
							<div class="col-sm-12" id="relatorioComissao">
								<table class="table table-bordered dataTable">
									<thead>
										<tr role="row">
											<th style="width: 162px;">Pedido</th>
											<th style="width: 207px;">Status</th>
											<th style="width: 207px;">Data Pedido</th>
											<th style="width: 207px;">Vendedor</th>
											<th style="width: 207px;">Cliente</th>
											<th style="width: 207px;">Data de pagamento</th>
											<th style="width: 207px;">Valor Bruto</th>
											<th style="width: 207px;">Valor Desconto</th>
											<th style="width: 207px;">Valor do compromisso</th>
											<th style="width: 207px;">Valor a considerar <br> <span class="small">(para comissão)</span></th>
											<th style="width: 207px;">Valor Comissão</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$soma_total_comissao = 0;
											foreach ($arrComissoes as $comissao) {
											$valor_comissao = $relatorio_comissao_considerar_somente_pago ? $comissao->valor_comissao_pedido_receber  : $comissao->valor_comissao;
											$soma_total_comissao += $valor_comissao;

											$data_pagamento = $comissao->data_pagamento_pedido != '' ? date('d/m/Y', strtotime($comissao->data_pagamento_pedido)) : '<i>Não informado</i>';
											if ($habilita_financeiro) {
												$data_pagamento = $comissao->data_pagamento_conta_receber != '' ? date('d/m/Y', strtotime($comissao->data_pagamento_conta_receber)) : '<i>Não informado</i>';
											}
										?>
											<tr>
												<td><?php echo $comissao->handle_orcamento;?></td>
												<td><?php echo $comissao->situacao;?></td>
												<td><?php echo $comissao->data_orcamento != '' ? date('d/m/Y', strtotime($comissao->data_orcamento)) : '';?></td>
												<td><?php echo $comissao->nome_vendedor;?></td>
												<td><?php echo $comissao->nome_cliente;?></td>
												<td><?php echo $data_pagamento;?></td>
												<td><?php echo 'R$ ' . number_format($comissao->valor_bruto, 2, ",", ".");?></td>
												<td><?php echo 'R$ ' . number_format($comissao->valor_desconto, 2, ",", "."); ?></td>
												<td><?php echo 'R$ ' . number_format($comissao->valor_liquido, 2, ",", "."); ?></td>
												<td><?php echo 'R$ ' . number_format($comissao->valor_conta_receber, 2, ",", "."); ?></td>
												<td><?php echo 'R$ ' . number_format($valor_comissao, 2, ",", "."); ?></td>
											</tr>
										<?php } ?>
									</tbody>
									<tbody>
										<tr>
											<th colspan="10" style="text-align: right;">Total Comissão</th>
											<th>R$ <?php echo number_format($soma_total_comissao, 2, ",", "."); ?></th>
										</tr>
									</tbody>
								</table>

								<div style="clear:both;"></div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>