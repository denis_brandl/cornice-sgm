<?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  


    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">					
						<h3 class="box-title">
						</h3>					
					</div>
					<div class="box-body">					
						<div id="componentes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
							<div class="row">
								<div class="col-sm-12">
									<form autocomplete="off"  name="frmRelatorioPedidosEntregar" id="frmBuscar">
<!-- 										<div class="form-group col-sm-6"> -->
											<label>Período Inicial:</label>
											<div class="input-group date">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" value="<?php echo date('d/m/Y');?>" id="periodo_inicial" name="periodo_inicial" class="form-control pull-right data">
											</div>
											<label>Período Final:</label>
											<div class="input-group date">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" value="<?php echo date('d/m/Y');?>"  id="periodo_final" name="periodo_final" class="form-control pull-right data">
											</div>
											<!-- /.input group -->
											<input type="button" class="btn btn-primary" id="btnBuscarPedidosEntregar" value="Buscar" />
											<input type="button" class="btn btn-primary imprimirRelatorio" relatorio="relatorioPedidosEntregues" value="Imprimir" />
											<!-- /.input group -->
<!-- 										</div>												 -->
										
									</form>
								</div>								
							</div>
							<div style="clear:both;"></div>
							<div class="row">
								<div class="col-sm-12" id="relatorioPedidosEntregues">
									<h5>Relatório - Pedidos a Entregar - Periodo: <?php echo date('d/m/Y');?> á <?php echo date('d/m/Y');?> </h5>
									<table class="table table-bordered dataTable" role="grid" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Item</th>
												<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 207px;" aria-label="Browser: activate to sort column ascending">Produto</th>
												<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 207px;" aria-label="Browser: activate to sort column ascending">Altura</th>
												<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 207px;" aria-label="Browser: activate to sort column ascending">Largura</th>
												<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 207px;" aria-label="Browser: activate to sort column ascending">Quantidade</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$CodigoCliente = 0;
												$CodigoPedido = 0;
												$total = 0;
												$total_bruto_cliente = 0;
												$total_desconto_cliente = 0;
												$total_liquido_cliente = 0;
												
												$total_bruto_geral = 0;
												$total_desconto_geral = 0;
												$total_liquido_geral = 0;
												$aux = 1;
												$quantidade = count($pedidosEntregues);
												foreach ($pedidosEntregues as $pedido) {
													
													$Dt_Efetiva_Entrega = new DateTime($pedido->Dt_Efetiva_Entrega);
													$Dt_Efetiva_Entrega = $Dt_Efetiva_Entrega->format('d/m/Y');													
													
													$dados_cliente = "Cliente: ".$pedido->RazaoSocial.'<br>';
// 													$dados_cliente .= 'Pedido: #'.$pedido->Cd_Orcamento.'<br>';																																			
													
													if ($CodigoCliente > 0 && $CodigoCliente != $pedido->CodigoCliente) {
// 														echo '<tr class="border_bottom">';
// 														echo '<td style="text-align:left;" colspan="2"><strong>Sub Total Bruto: </strong> ';														
// 														echo 'R$ '.number_format($total_bruto_cliente,'2',',','').'</td>';
// 														echo '<td style="text-align:left;" colspan="2"><strong>Sub Total Descontos</strong> ';														
// 														echo 'R$ '.number_format($total_desconto_cliente,'2',',','').'</td>';
// 														echo '<td style="text-align:left;" colspan="3"><strong>Sub Total Liquido</strong>';														
// 														echo ' R$ '.number_format($total_liquido_cliente,'2',',','').'</td>';
// 														echo '</tr>';
														
														$total_bruto_geral += $total_bruto_cliente;
														$total_desconto_geral += $total_desconto_cliente;
														$total_liquido_geral += $total_bruto_cliente - $total_desconto_cliente;
														
														$total_bruto_cliente = 0;
														$total_desconto_cliente = 0;
														$total_liquido_cliente = 0;
														
// 														echo '<tr><td colspan="7">&nbsp;</td></tr>';
													}														
													
													
													if ($CodigoCliente != $pedido->CodigoCliente) {
														if ($CodigoCliente > 0) {
															echo '<tr class="border_top">';
														} else {
															echo '<tr>';
														}
														echo sprintf('<td style="background-color:#F4F4F5;" colspan="7">%s</td>',$dados_cliente);
														echo '</tr>';
													}
													
													if ($CodigoPedido != $pedido->Cd_Orcamento) {
														echo '<tr>';
														echo sprintf('<td class="coluna" style="background-color:#F4F4F5;" colspan="7">%s</td>','Pedido: #'.$pedido->Cd_Orcamento);
														echo '</tr>';													
													}
													
													
													echo '<tr>';
													echo sprintf('<td>%s</td>',$pedido->Cd_Item_Orcamento);
													echo sprintf('<td>%s</td>',$pedido->Cd_Prod_Aux);
													echo sprintf('<td>%s</td>',$pedido->Md_Altura);
													echo sprintf('<td>%s</td>',$pedido->Md_Largura);
													echo sprintf('<td>%s</td>',$pedido->Qt_Item);
// 													echo sprintf('<td>%s</td>',$pedido->Vl_Unitario);
// 													echo sprintf('<td>%s</td>',$pedido->Vl_Unitario * $pedido->Qt_Item);
													echo '</tr>';
																												
													$total_bruto_cliente += (float) $pedido->Vl_Bruto;
													$total_desconto_cliente += $pedido->Vl_Desconto;
													$total_liquido_cliente += $pedido->Vl_Bruto - $pedido->Vl_Desconto;
													
													$CodigoCliente = $pedido->CodigoCliente;
													$CodigoPedido = $pedido->Cd_Orcamento;
													
													if ($aux == $quantidade) {
// 														echo '<tr>';
// 														echo '<td style="text-align:left;" colspan="2"><strong>Sub Total Bruto: </strong> ';														
// 														echo 'R$ '.number_format($total_bruto_cliente,'2',',','').'</td>';
// 														echo '<td style="text-align:left;" colspan="2"><strong>Sub Total Descontos</strong> ';														
// 														echo 'R$ '.number_format($total_desconto_cliente,'2',',','').'</td>';
// 														echo '<td style="text-align:left;" colspan="3"><strong>Sub Total Liquido</strong>';														
// 														echo ' R$ '.number_format($total_liquido_cliente,'2',',','').'</td>';
// 														echo '</tr>';
														
														$total_bruto_geral += $total_bruto_cliente;
														$total_desconto_geral += $total_desconto_cliente;
														$total_liquido_geral += $total_liquido_cliente - $total_desconto_cliente;
														
														$total_bruto_cliente = 0;
														$total_desconto_cliente = 0;
														$total_liquido_cliente = 0;
													}
													
													$aux++;
												}
											?>
											<tr style="display:none;" class="border_top">
												<td style="text-align:right;" colspan="8"><strong>Total Bruto:</strong> R$ <?php echo number_format($total_bruto_geral,'2',',','');?></td>
											</tr>
											<tr style="display:none;">
												<td style="text-align:right;" colspan="8"><strong>Descontos:</strong> R$ <?php echo number_format($total_desconto_geral,'2',',','');?></td>
											</tr>
											<tr style="display:none;">
												<td style="text-align:right;" colspan="8"><strong>Total Liquido:</strong> R$ <?php echo number_format($total_liquido_geral,'2',',','');?></td>
											</tr>											
										</tbody>
									</table>									
								</div>								
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  