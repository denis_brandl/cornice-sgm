<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>


<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
					</h3>
				</div>
				<div class="box-body">

					<div class="row">
						<?php if (count($this->empresas) > 1 && (count($orcamento->arrPermissoesUsuarioEmpresa) < 1 || count($orcamento->arrPermissoesUsuarioEmpresa) == 0)) { ?>
							<div class="form-group col-md-3 col-sm-12">
								<label for="empresa">Empresa</label>
								<select class="form-control" id="empresa" name="empresa">
									<option value="">Todos</option>
									<?php
									foreach ($this->empresas as $empresa) {
										echo sprintf(
											'<option %s value="%s">%s</option>',
											$empresa->CodigoEmpresa == $arrFiltro['empresa'] ? 'selected' : '',
											$empresa->CodigoEmpresa,
											$empresa->RazaoSocial
										);
									}
									?>
								</select>
							</div>
						<?php } ?>

						<div class="form-group col-md-3 col-sm-12">
							<label for="criado_por">Vendedor</label>
							<select class="form-control" id="vendedor" name="vendedor">
								<option value="">Todos</option>
								<?php
								foreach ($this->usuarios as $usuario) {
									echo sprintf(
										'<option %s value="%s">%s</option>',
										$usuario->CodigoUsuario == $arrFiltro['criado_por'] ? 'selected' : '',
										$usuario->CodigoUsuario,
										$usuario->NomeUsuario
									);
								}
								?>
							</select>
						</div>

						<div class="form-group col-md-3 col-sm-12">
							<label for="criado_por">Cliente</label>
							<select name="filtro_cliente" class="form-control" id="filtro_cliente"></select>
						</div>

						<div class="form-group col-md-3 col-sm-12">
							<label for="pedido">Nº do pedido</label>
							<div class="input-group">
								<input class="form-control" type="text" id="pedido" name="pedido" value="<?php echo $arrFiltro['pedido']; ?>">
							</div>
						</div>

						<div class="form-group col-md-1 col-sm-12">
							<div class="checkbox">
								<label>
									<input type="checkbox" <?php echo $arrFiltro['somente_pedidos_validos'] == '1' ? 'checked' : ''; ?> name="somente_pedidos_validos" id="somente_pedidos_validos" value="1">
									Somente pedidos
								</label>
							</div>
							<p class="small">Serão desconsiderados as situações <strong>Aguardando Aprovação</strong> e <strong>Cancelado</strong></p>
						</div>

						<div class="form-group col-md-2 col-sm-12">
							<label>Situações</label>
							<select name="filtro_situacao[]" class="form-control filtro_situacao" id="filtro_situacao" multiple="multiple">
								<?php
								foreach ($situacoes as $situacao) {
									$selected = "";

									if (isset($arrFiltro['filtro_situacao']) && in_array($situacao->idSituacao, explode(',', $arrFiltro['filtro_situacao']))) {
										$selected = "selected";
									}
								?>
									<option <?php echo $selected; ?> value="<?php echo $situacao->idSituacao; ?>"><?php echo $situacao->descricao; ?></option>
								<?php
								}
								?>
							</select>
						</div>

						<div class="form-group col-md-3 col-sm-12">
							<label>Período Inicial:</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" value="<?php echo date('d/m/Y', strtotime($periodo_inicial)); ?>" id="periodo_inicial" name="periodo_inicial" class="form-control pull-right data">
							</div>
						</div>

						<div class="form-group col-md-3 col-sm-12">
							<label>Período Final:</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" value="<?php echo date('d/m/Y', strtotime($periodo_final)); ?>" id="periodo_final" name="periodo_final" class="form-control pull-right data">
							</div>
						</div>

						<div class="form-group col-md-3 col-sm-12">
							<label>Tipo de data</label>
							<select name="tipo_data[]" class="form-control" id="tipo_data">
								<option <?php echo isset($arrFiltro['tipo_data']) &&  $arrFiltro['tipo_data'] === 'Dt_Orcamento' ? 'selected' : ''; ?> value="Dt_Orcamento">Cadastro do orçamento</option>
								<option <?php echo isset($arrFiltro['tipo_data']) &&  $arrFiltro['tipo_data'] === 'Dt_Efetiva_Entrega' ? 'selected' : ''; ?> value="Dt_Efetiva_Entrega">Efetiva de entrega</option>
								<option <?php echo isset($arrFiltro['tipo_data']) &&  $arrFiltro['tipo_data'] === 'Dt_Prevista_Entrega' ? 'selected' : ''; ?> value="Dt_Prevista_Entrega">Prevista para entrega</option>
							</select>
						</div>

					</div>
					<div class="row">
						<div class="form-group col-md-6 col-sm-12">
							<button class="btn btn-primary" type="button" id="btnFiltroRelatorioPedidosSimplificado">Aplicar Filtro</button>
							<input type="button" class="btn btn-primary imprimirRelatorio" relatorio="relatorioPedidosEntregues" value="Imprimir">
						</div>
					</div>
				</div>

				<div style="clear:both;"></div>
				<div class="row">
					<div class="col-sm-12" id="relatorioPedidosEntregues">
						<div class="table-responsive">
							<table id="listagemRelatorioSimplificado" class="table table-bordered dataTable table-striped">
								<thead>
									<tr role="row">
										<th style="width: 162px;">Pedido</th>
										<th style="width: 207px;">Situação</th>
										<th style="width: 207px;">Data de cadastro</th>
										<th style="width: 207px;">Data prevista</th>
										<th style="width: 207px;">Data Entregue</th>
										<th style="width: 207px;">Forma de pagamento <br>Entrada</th>
										<th style="width: 207px;">Forma de pagamento <br>Saldo</th>
										<th style="width: 207px;">Cliente</th>
										<th style="width: 207px;">Vendedor</th>
										<th style="width: 207px;">Valor Bruto</th>
										<th style="width: 207px;">Valor Desconto</th>
										<th style="width: 207px;">Valor Entrada</th>
										<th style="width: 207px;">Saldo</th>
										<th style="width: 207px;">Valor Final</th>
									</tr>
								</thead>


								<tbody>
									<?php
									$CodigoCliente = 0;
									$CodigoPedido = 0;
									$total = 0;

									$total_bruto_geral = 0;
									$total_desconto_geral = 0;
									$total_liquido_geral = 0;
									$total_entrada = 0;
									$total_saldo = 0;
									$aux = 1;
									$quantidade = count($pedidosEntregues);
									foreach ($pedidosEntregues as $pedido) {

										$Dt_Efetiva_Entrega = '<i>Não definido</i>';
										if (!is_null($pedido->Dt_Efetiva_Entrega)) {
											$Dt_Efetiva_Entrega = new DateTime($pedido->Dt_Efetiva_Entrega);
											$Dt_Efetiva_Entrega = $Dt_Efetiva_Entrega->format('d/m/Y');
										}

										$dados_cliente = "Cliente: " . $pedido->RazaoSocial . '<br>';
										$dados_cliente .= "Vendedor: " . $pedido->NomeUsuarioCriado;

										$total_bruto_geral += $pedido->Vl_Bruto;
										$total_desconto_geral += $pedido->Vl_Desconto;
										$total_liquido_geral += $pedido->Vl_Liquido;
										$total_entrada += $pedido->Vl_Entrada;
										$total_saldo += $pedido->saldo;
									?>
										<tr>
											<td><?php echo $pedido->Cd_Orcamento; ?></td>
											<td><?php echo $pedido->situacao_pedido; ?></td>
											<td data-order="<?php echo $pedido->Dt_Orcamento;?>"><?php echo date('d/m/Y', strtotime($pedido->Dt_Orcamento)); ?></td>
											<td data-order="<?php echo $pedido->Dt_Prevista_Entrega;?>"><?php echo $pedido->Dt_Prevista_Entrega !=  '' ? date('d/m/Y', strtotime($pedido->Dt_Prevista_Entrega)) : '<i>Não definido</i>'; ?></td>
											<td data-order="<?php echo $pedido->Dt_Efetiva_Entrega;?>"><?php echo $pedido->Dt_Efetiva_Entrega != '' ? date('d/m/Y', strtotime($pedido->Dt_Efetiva_Entrega)) : '<i>Não definido</i>'; ?></td>
											<td><?php echo $pedido->forma_pagamento_entrada != '' ? $pedido->forma_pagamento_entrada : '<i>Não definido</i>'; ?></td>
											<td><?php echo $pedido->forma_pagamento_saldo != '' ? $pedido->forma_pagamento_saldo : '<i>Não definido</i>'; ?></td>
											<td><?php echo $pedido->RazaoSocial; ?></td>
											<td><?php echo $pedido->NomeUsuarioCriado; ?></td>

											<td data-order="<?php echo $pedido->Vl_Bruto;?>"><?php echo 'R$ ' . number_format($pedido->Vl_Bruto, 2, ",", "."); ?></td>
											<td data-order="<?php echo $pedido->Vl_Desconto;?>"><?php echo 'R$ ' . number_format($pedido->Vl_Desconto, 2, ",", "."); ?></td>
											
											<td data-order="<?php echo $pedido->Vl_Entrada;?>"><?php echo 'R$ ' . number_format($pedido->Vl_Entrada, 2, ",", "."); ?></td>
											<td data-order="<?php echo $pedido->saldo;?>"><?php echo 'R$ ' . number_format($pedido->saldo, 2, ",", "."); ?></td>
											<td data-order="<?php echo $pedido->Vl_Liquido;?>"><?php echo 'R$ ' . number_format($pedido->Vl_Liquido, 2, ",", "."); ?></td>

										</tr>
									<?php

									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="9">&nbsp;</td>
										<td>R$ <?php echo number_format($total_bruto_geral, '2', ',', '.'); ?></td>
										<td>R$ <?php echo number_format($total_desconto_geral, '2', ',', '.'); ?></td>
										<td>R$ <?php echo number_format($total_entrada, '2', ',', '.'); ?></td>
										<td>R$ <?php echo number_format($total_saldo, '2', ',', '.'); ?></td>
										<td>R$ <?php echo number_format($total_liquido_geral, '2', ',', '.'); ?></td>
									</tr>
									<tr role="row">
										<th style="width: 162px;">Pedido</th>
										<th style="width: 207px;">Situação</th>
										<th style="width: 207px;">Data de cadastro</th>
										<th style="width: 207px;">Data prevista</th>
										<th style="width: 207px;">Data Entregue</th>
										<th style="width: 207px;">Forma de pagamento <br>Entrada</th>
										<th style="width: 207px;">Forma de pagamento <br>Saldo</th>
										<th style="width: 207px;">Cliente</th>
										<th style="width: 207px;">Vendedor</th>
										<th style="width: 207px;">Valor Bruto</th>
										<th style="width: 207px;">Valor Desconto</th>
										<th style="width: 207px;">Valor Entrada</th>
										<th style="width: 207px;">Saldo</th>
										<th style="width: 207px;">Valor Final</th>
									</tr>																		
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>