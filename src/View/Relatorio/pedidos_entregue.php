<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>


<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
					</h3>
				</div>
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<form autocomplete="off" name="frmRelatorioPedidosEntregue" id="frmBuscar">
									<!-- 										<div class="form-group col-sm-6"> -->

									<label for="empresa">Empresa</label>
									<select class="form-control" id="empresa" name="empresa">
										<option value="">Todos</option>
										<?php
										foreach ($this->empresas as $empresa) {
											echo sprintf(
												'<option %s value="%s">%s</option>',
												$empresa->CodigoEmpresa == $arrFiltro['empresa'] ? 'selected' : '',
												$empresa->CodigoEmpresa,
												$empresa->RazaoSocial
											);
										}
										?>
									</select>

									<label for="criado_por">Vendedor</label>
									<select class="form-control" id="vendedor" name="vendedor">
										<option value="">Todos</option>
										<?php
										foreach ($this->usuarios as $usuario) {
											echo sprintf(
												'<option %s value="%s">%s</option>',
												$usuario->CodigoUsuario == $arrFiltro['criado_por'] ? 'selected' : '',
												$usuario->CodigoUsuario,
												$usuario->NomeUsuario
											);
										}
										?>
									</select>

									<label>Período Inicial:</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" value="<?php echo date('d/m/Y', strtotime($periodo_inicial)); ?>" id="periodo_inicial" name="periodo_inicial" class="form-control pull-right data">
									</div>
									<label>Período Final:</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" value="<?php echo date('d/m/Y', strtotime($periodo_final)); ?>" id="periodo_final" name="periodo_final" class="form-control pull-right data">
									</div>

									<div class="form-group">
										<label>Tipo de data</label>
										<select name="tipo_data[]" class="form-control" id="tipo_data">
											<option <?php echo isset($arrFiltro['tipo_data']) &&  $arrFiltro['tipo_data'] === 'Dt_Efetiva_Entrega' ? 'selected' : ''; ?> value="Dt_Efetiva_Entrega">Efetiva de entrega</option>
											<option <?php echo isset($arrFiltro['tipo_data']) &&  $arrFiltro['tipo_data'] === 'Dt_Prevista_Entrega' ? 'selected' : ''; ?> value="Dt_Prevista_Entrega">Prevista para entrega</option>
											<option <?php echo isset($arrFiltro['tipo_data']) &&  $arrFiltro['tipo_data'] === 'Dt_Orcamento' ? 'selected' : ''; ?> value="Dt_Orcamento">Cadastro do orçamento</option>
										</select>
									</div>
									<div class="form-group">
										<label>Situações</label>
										<select name="filtro_situacao[]" class="form-control filtro_situacao" id="filtro_situacao" multiple="multiple">
											<?php
											foreach ($situacoes as $situacao) {
												$selected = "";

												if (isset($arrFiltro['filtro_situacao']) && in_array($situacao->idSituacao, explode(',', $arrFiltro['filtro_situacao']))) {
													$selected = "selected";
												}
											?>
												<option <?php echo $selected; ?> value="<?php echo $situacao->idSituacao; ?>"><?php echo $situacao->descricao; ?></option>
											<?php
											}
											?>
										</select>

										<label>Filtrar pedidos pagos</label>
										<select name="filtro_pago" class="form-control filtro_pago" id="filtro_pago">
											<option value="-1">Todos</option>
											<option value="1">Pago</option>
											<option value="0">Não Pago</option>
										</select>
									</div>
									<div style="clear:both;"></div>
									<!-- /.input group -->
									<input type="button" class="btn btn-primary" id="btnBuscarPedidosEntregues" value="Buscar" />
									<input type="button" class="btn btn-primary imprimirRelatorio" relatorio="relatorioPedidosEntregues" value="Imprimir" />
									<!-- /.input group -->
									<!-- 										</div>												 -->

								</form>
							</div>
						</div>
						<div style="clear:both;"></div>
						<div class="row">
							<div class="col-sm-12" id="relatorioPedidosEntregues">
								<h5>Relatório - Pedidos Entregues - Periodo: <?php echo date('d/m/Y', strtotime($periodo_inicial)); ?> á <?php echo date('d/m/Y', strtotime($periodo_final)); ?> </h5>
								<table class="table table-bordered dataTable">
									<tr role="row">
										<th style="width: 162px;">Item</th>
										<th style="width: 207px;">Produto</th>
										<th style="width: 207px;">Altura</th>
										<th style="width: 207px;">Largura</th>
										<th style="width: 207px;">Quantidade</th>
										<th style="width: 207px;">Vl. Unit</th>
										<th style="width: 207px;">Vl. Bruto</th>
									</tr>
								</table>

								<?php
								$CodigoCliente = 0;
								$CodigoPedido = 0;
								$total = 0;
								$total_bruto_cliente = 0;
								$total_desconto_cliente = 0;
								$total_liquido_cliente = 0;

								$total_bruto_geral = 0;
								$total_desconto_geral = 0;
								$total_liquido_geral = 0;
								$aux = 1;
								$quantidade = count($pedidosEntregues);
								foreach ($pedidosEntregues as $pedido) {

									$Dt_Efetiva_Entrega = '<i>Não definido</i>';
									if (!is_null($pedido->Dt_Efetiva_Entrega)) {
										$Dt_Efetiva_Entrega = new DateTime($pedido->Dt_Efetiva_Entrega);
										$Dt_Efetiva_Entrega = $Dt_Efetiva_Entrega->format('d/m/Y');
									}

									$dados_cliente = "Cliente: " . $pedido->RazaoSocial . '<br>';
									$dados_cliente .= "Vendedor: " . $pedido->NomeUsuarioCriado;

									if ($CodigoCliente > 0 && $CodigoCliente != $pedido->CodigoCliente) {
										echo '<tr class="border_bottom" style="border-bottom:2pt dashed black !important;">';
										echo '<td style="font-size:12px;text-align:left;border-bottom:2pt dashed black !important;" colspan="2"><strong>Sub Total Bruto: </strong> ';
										echo 'R$ ' . number_format($total_bruto_cliente, '2', ',', '') . '</td>';
										echo '<td style="font-size:12px;text-align:left;border-bottom:2pt dashed black !important;" colspan="2"><strong>Sub Total Descontos</strong> ';
										echo 'R$ ' . number_format($total_desconto_cliente, '2', ',', '') . '</td>';
										echo '<td style="font-size:12px;text-align:left;border-bottom:2pt dashed black !important;" colspan="3"><strong>Sub Total Liquido</strong>';
										echo ' R$ ' . number_format($total_liquido_cliente, '2', ',', '') . '</td>';
										echo '</tr>';

										if ($aux > 1) {
											echo '</tbody>
																	</table></div>' . chr(13) . chr(10);
											echo '<div style="page-break-inside: avoid"></div>' . chr(13) . chr(10);
										}

										$total_bruto_geral += $total_bruto_cliente;
										$total_desconto_geral += $total_desconto_cliente;
										$total_liquido_geral += $total_bruto_cliente - $total_desconto_cliente;

										$total_bruto_cliente = 0;
										$total_desconto_cliente = 0;
										$total_liquido_cliente = 0;
									}


									if ($CodigoCliente != $pedido->CodigoCliente) {
										echo '<div style="page-break-inside: avoid">
																<table class="table table-bordered dataTable">
																	<tbody>';
										echo '<tr>';
										echo sprintf('<td style="background-color:#F4F4F5;" colspan="7">%s</td>', $dados_cliente);
										echo '</tr>';
									}

									if ($CodigoPedido != $pedido->Cd_Orcamento) {
										echo '<tr>';
										echo sprintf('<td class="coluna" style="background-color:#F4F4F5;" colspan="7"><a href="%s" target="_blank">%s</td>', URL . 'orcamento/salvar/' . $pedido->Cd_Orcamento, 'Pedido: #' . $pedido->Cd_Orcamento);
										echo '</tr>';

										$total_desconto_cliente += $pedido->Vl_Desconto;
									}


									echo '<tr>';
									echo sprintf('<td style="width: 162px;">%s</td>', $pedido->Cd_Item_Orcamento);
									echo sprintf('<td style="width: 207px;">%s</td>', $pedido->Cd_Prod_Aux);
									echo sprintf('<td style="width: 207px;">%s</td>', $pedido->Md_Altura);
									echo sprintf('<td style="width: 207px;">%s</td>', $pedido->Md_Largura);
									echo sprintf('<td style="width: 207px;">%s</td>', $pedido->Qt_Item);
									echo sprintf('<td style="width: 207px;">%s</td>', $pedido->Vl_Unitario);
									echo sprintf('<td style="width: 207px;">%s</td>', $pedido->Vl_Unitario * $pedido->Qt_Item);
									echo '</tr>';

									$total_bruto_cliente += $pedido->Vl_Unitario * $pedido->Qt_Item;
									$total_liquido_cliente = $total_bruto_cliente - $pedido->Vl_Desconto;

									$CodigoCliente = $pedido->CodigoCliente;
									$CodigoPedido = $pedido->Cd_Orcamento;

									if ($aux == $quantidade) {
										echo '<tr>';
										echo '<td style="font-size:12px;text-align:left;" colspan="2"><strong>Sub Total Bruto: </strong> ';
										echo 'R$ ' . number_format($total_bruto_cliente, '2', ',', '') . '</td>';
										echo '<td style="font-size:12px;text-align:left;" colspan="2"><strong>Sub Total Descontos</strong> ';
										echo 'R$ ' . number_format($total_desconto_cliente, '2', ',', '') . '</td>';
										echo '<td style="font-size:12px;text-align:left;" colspan="3"><strong>Sub Total Liquido</strong>';
										echo ' R$ ' . number_format($total_liquido_cliente, '2', ',', '') . '</td>';
										echo '</tr>';
										echo '</tbody>
                                                            </table></div>';
										echo '<div style="page-break-inside: avoid;">&nbsp;</div>';

										$total_bruto_geral += $total_bruto_cliente;
										$total_desconto_geral += $total_desconto_cliente;
										$total_liquido_geral += $total_bruto_cliente	 - $total_desconto_cliente;

										$total_bruto_cliente = 0;
										$total_desconto_cliente = 0;
										$total_liquido_cliente = 0;
									}

									$aux++;
								}
								?>
								<div style="clear:both;"></div>
								<hr style="margin-top: 0px;margin-bottom: 0px;border: 2px dashed;">
								<table style="float:right;">
									<tbody>
										<tr>
											<td style="text-align:right;" colspan="1"><strong>Total Bruto:</strong> R$ <?php echo number_format($total_bruto_geral, '2', ',', ''); ?></td>
										</tr>
										<tr>
											<td style="text-align:right;" colspan="1"><strong>Descontos:</strong> R$ <?php echo number_format($total_desconto_geral, '2', ',', ''); ?></td>
										</tr>
										<tr>
											<td style="text-align:right;" colspan="1"><strong>Total Liquido:</strong> R$ <?php echo number_format($total_liquido_geral, '2', ',', ''); ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>