<?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>
    </style>
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">					
						<h3 class="box-title">
						</h3>					
					</div>
					<div class="box-body">					
						<div id="componentes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
							<div class="row">
								<div class="col-sm-12">
									<form autocomplete="off"  name="frmRelatorioPedidosFabrica" id="frmBuscar">
<!-- 										<div class="form-group col-sm-6"> -->
											<label>Período Inicial:</label>
											<div class="input-group date">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" value="<?php echo $periodo_inicial;?>" id="periodo_inicial" name="periodo_inicial" class="form-control pull-right data">
											</div>
											<label>Período Final:</label>
											<div class="input-group date">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" value="<?php echo $periodo_final;?>"  id="periodo_final" name="periodo_final" class="form-control pull-right data">
											</div>
											<!-- /.input group -->
											<input type="button" class="btn btn-primary" id="btnBuscarPedidosFabrica" value="Buscar" />
											<input type="button" class="btn btn-primary relatorioPedidosFabrica" relatorio="relatorioPedidosFabrica" value="Imprimir" />
											<!-- /.input group -->
<!-- 										</div>												 -->
										
									</form>
								</div>								
							</div>
							<div style="clear:both;"></div>
							<div class="row">
								<div class="col-sm-12" id="relatorioPedidosFabrica">
									<h5>Relatório - Fábrica - Periodo: <?php echo $periodo_inicial;?> á <?php echo $periodo_final;?> </h5>
									<table class="table table-bordered dataTable" role="grid" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th style="text-align:center;">Item</th>
												<th style="text-align:center;">Produto</th>
												<th style="text-align:center;">Alt</th>
												<th style="text-align:center;">Lg</th>
												<th style="text-align:center;">Qtd</th>
												<th>Molduras</th>
											</tr>
										</thead>
											<?php
												$CodigoCliente = 0;
												$CodigoPedido = 0;
												$total = 0;
												$total_bruto_cliente = 0;
												$total_desconto_cliente = 0;
												$total_liquido_cliente = 0;
												
												$total_bruto_geral = 0;
												$total_desconto_geral = 0;
												$total_liquido_geral = 0;
												$aux = 1;
												$quantidade = count($pedidosEntregues);
												foreach ($pedidosEntregues as $pedido) {
													
													$Dt_Efetiva_Entrega = new DateTime($pedido->Dt_Efetiva_Entrega);
													$Dt_Efetiva_Entrega = $Dt_Efetiva_Entrega->format('d/m/Y');													
													
													$dados_cliente = "Cliente: ".$pedido->RazaoSocial.'<br>';																																	
													echo '<tbody>';
													if ($CodigoCliente > 0 && $CodigoCliente != $pedido->CodigoCliente) {													
														echo '<tr><td colspan="7">&nbsp;</td></tr>';
													}														
													
													if ($CodigoCliente != $pedido->CodigoCliente) {
														echo '<tr>';
														echo sprintf('<td style="background-color:#F4F4F5;" colspan="7">%s</td>',$dados_cliente);
														echo '</tr>';
													}
													
													if ($CodigoPedido != $pedido->Cd_Orcamento) {
														echo '<tr>';
														echo sprintf('<td class="coluna" style="background-color:#F4F4F5;" colspan="7">%s</td>','Pedido: #'.$pedido->Cd_Orcamento);
														echo '</tr>';	
														echo '<tr>';
														
														$Dt_Prevista_Entrega = $pedido->Dt_Prevista_Entrega;										
														$Dt_Prevista_Entrega = new DateTime($Dt_Prevista_Entrega);
														$Dt_Prevista_Entrega = $Dt_Prevista_Entrega->format('d/m/Y');														
														echo sprintf('<td class="coluna" style="background-color:#F4F4F5;" colspan="7">%s</td>','Data Prevista: '.$Dt_Prevista_Entrega);
														echo '</tr>';														
													}
													
													
													echo '<tr>';
													echo sprintf('<td style="text-align:center;">%s</td>',$pedido->Cd_Item_Orcamento);
													echo sprintf('<td style="text-align:center;">%s</td>',$pedido->Cd_Prod_Aux);
													echo sprintf('<td style="text-align:center;">%s</td>',$pedido->Md_Altura);
													echo sprintf('<td style="text-align:center;">%s</td>',$pedido->Md_Largura);
													echo sprintf('<td style="text-align:center;">%s</td>',$pedido->Qt_Item);
													echo '<td>';

													if (isset($arrMoldurasItemOrcamento[$pedido->Cd_Orcamento][$pedido->Cd_Item_Orcamento])) {														
														foreach ($arrMoldurasItemOrcamento[$pedido->Cd_Orcamento][$pedido->Cd_Item_Orcamento] as $molduras_item_orcamento) {
															echo $molduras_item_orcamento['DescricaoProduto'].', ';
														}
													}
													
													if (isset($arrComponentesItemOrcamento[$pedido->Cd_Orcamento][$pedido->Cd_Item_Orcamento])) {
														foreach ($arrComponentesItemOrcamento[$pedido->Cd_Orcamento][$pedido->Cd_Item_Orcamento] as $componentes_item_orcamento) {
															echo $componentes_item_orcamento['Descricao'].', ';
														}
													}	
													echo '</td>';
													
													echo '</tr>';
																												
													$total_bruto_cliente += (float) $pedido->Vl_Bruto;
													$total_desconto_cliente += $pedido->Vl_Desconto;
													$total_liquido_cliente += $pedido->Vl_Bruto - $pedido->Vl_Desconto;
													
													$CodigoCliente = $pedido->CodigoCliente;
													$CodigoPedido = $pedido->Cd_Orcamento;
													
													if ($aux == $quantidade) {
														$total_bruto_geral += $total_bruto_cliente;
														$total_desconto_geral += $total_desconto_cliente;
														$total_liquido_geral += $total_liquido_cliente - $total_desconto_cliente;
														
														$total_bruto_cliente = 0;
														$total_desconto_cliente = 0;
														$total_liquido_cliente = 0;
													}
													
													$aux++;
													echo '</tbody>';
												}
											?>
									</table>									
								</div>								
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  
	<div class="modal" id="impressaoRelatorio">
		<embed src="" width="800" type="application/pdf" height="600" id="arquivoPDF">
		
	</div>
  
  <!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  