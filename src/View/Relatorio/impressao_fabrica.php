<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                    </h3>
                </div>
                <div class="box-body">
                    <div id="componentes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Filtro: </h4>
                                <form autocomplete="off" name="frmRelatorioPedidosFabrica" id="frmBuscar">

                                    <label for="empresa">Empresa</label>
                                    <select class="form-control" id="empresa" name="empresa">
                                        <option value="">Todos</option>
                                        <?php
                                        $empresa_selecionada = $empresa;
                                        foreach ($this->empresas as $empresa) {
                                            echo sprintf(
                                                '<option %s value="%s">%s</option>',
                                                $empresa->CodigoEmpresa == $empresa_selecionada ? 'selected' : '',
                                                $empresa->CodigoEmpresa,
                                                $empresa->RazaoSocial
                                            );
                                        }
                                        ?>
                                    </select>

                                    <label>Período Inicial:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar i-periodo-inicial"></i>
                                        </div>
                                        <input type="text" value="<?php echo $periodo_inicial; ?>" id="periodo_inicial" name="periodo_inicial" class="form-control pull-right data">
                                    </div>
                                    <label>Período Final:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar  i-periodo-final"></i>
                                        </div>
                                        <input type="text" value="<?php echo $periodo_final; ?>" id="periodo_final" name="periodo_final" class="form-control pull-right data">
                                    </div>

									<div class="form-group">
										<label>Tipo de data</label>
										<select name="tipo_data[]" class="form-control" id="tipo_data">
											<option <?php echo isset($tipo_data) &&  $tipo_data === 'Dt_Efetiva_Entrega' ? 'selected' : ''; ?> value="Dt_Efetiva_Entrega">Efetiva de entrega</option>
											<option <?php echo isset($tipo_data) &&  $tipo_data === 'Dt_Prevista_Entrega' ? 'selected' : ''; ?> value="Dt_Prevista_Entrega">Prevista para entrega</option>
											<option <?php echo isset($tipo_data) &&  $tipo_data === 'Dt_Orcamento' ? 'selected' : ''; ?> value="Dt_Orcamento">Cadastro do orçamento</option>
										</select>
									</div>
                                    
                                    <!-- /.input group -->
                                    <input type="button" class="btn btn-primary" id="btnBuscarPedidosFabrica" value="Buscar" />
                                    <input type="button" class="btn btn-primary" id="imprimirPedidosFabrica" value="Imprimir" />
                                    <!-- /.input group -->

                                </form>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                        <div class="row">
                            <div class="col-sm-12" id="relatorioPedidosFabrica">
                                <h5>Relatório - Fábrica - Periodo: <?php echo $periodo_inicial; ?> á <?php echo $periodo_final; ?> </h5>
                                <?php
                                $CodigoCliente = 0;
                                $CodigoPedido = 0;
                                $total = 0;
                                $total_bruto_cliente = 0;
                                $total_desconto_cliente = 0;
                                $total_liquido_cliente = 0;

                                $total_bruto_geral = 0;
                                $total_desconto_geral = 0;
                                $total_liquido_geral = 0;
                                $aux = 1;
                                $quantidade = count($pedidosEntregues);
                                $html_relatorio = '';
                                foreach ($pedidosEntregues as $item_orcamento) {
                                    if ($CodigoPedido != $item_orcamento->Cd_Orcamento) {

                                        if ($aux > 1) {
                                            $html_relatorio .= '</tbody>
                                                                                    </table></div>' . chr(13) . chr(10);
                                            $html_relatorio .= '<hr style="border-top:2pt dashed black !important; margin-top:5px;margin-bottom:5px;"><div style="page-break-inside: avoid"> </div>' . chr(13) . chr(10);
                                        }
                                    }

                                    if ($CodigoCliente == 0 || $CodigoCliente != $item_orcamento->CodigoCliente) {
                                        $nome_cliente = 'Cliente: ' . $item_orcamento->RazaoSocial;                                        
                                    }

                                    if ($CodigoPedido != $item_orcamento->Cd_Orcamento) {
                                        $numero_pedido = sprintf('%s', 'Pedido: #' . $item_orcamento->Cd_Orcamento);
                                    }

                                    if ($CodigoPedido != $item_orcamento->Cd_Orcamento) {

                                        $data_prevista_entrega = $item_orcamento->Dt_Prevista_Entrega != '' ? date('d/m/Y', strtotime($item_orcamento->Dt_Prevista_Entrega)) : '<i>Não informado</i>';

                                        $data_prevista = sprintf('%s', 'Data Prevista: ' . $data_prevista_entrega);

                                        $html_relatorio .= '<div style="page-break-inside: avoid">
                                                        <table>
                                                        <thead>
                                                            <tr role="row">
                                                                <td width="90%">' . $nome_cliente . ' - ' . $numero_pedido . ' - ' . $data_prevista . '</td>
                                                            </tr>
                                                            <tr>
                                                                <td> <strong> Empresa: </strong> ' . $item_orcamento->nome_empresa . '
                                                                <strong> Vendedor: </strong> ' . $item_orcamento->NomeUsuarioCriado . '</td> 
                                                            </tr>
                                                        </thead>
                                                        </table>
                                                        <table>
                                                            <thead>
                                                            <tr role="row">
                                                                <th width="30" style="width:50px;text-align:center;" align="center">Item</th>
                                                                <th width="100" style="width:150px;text-align:left;">Produto</th>
                                                                <th width="30" style="width:50px;text-align:center;">Alt</th>
                                                                <th width="30" style="width:50px;text-align:center;">Lg</th>
                                                                <th width="30" style="width:50px;text-align:center;">Qtd</th>
                                                                <th width="350" style="width:450px;text-align:left;">Molduras/Componentes</th>
                                                                <th width="350" style="width:450px;text-align:left;">Observações do item</th>
                                                            </tr>
                                                            </thead>
                                                        </table>
                                                        <table>
                                                        <tbody>';
                                    }


                                    $html_relatorio .= '<tr>';

                                    $html_relatorio .= sprintf('<td width="30" style="width:50px;text-align:center;">%s</td>', $item_orcamento->Cd_Item_Orcamento) . chr(13);
                                    $html_relatorio .= sprintf('<td width="100" style="width:150px;text-align:left;">%s</td>', $item_orcamento->Descricao) . chr(13);
                                    $html_relatorio .= sprintf('<td width="30" style="width:50px;text-align:center;">%s</td>', $item_orcamento->Md_Altura) . chr(13);
                                    $html_relatorio .= sprintf('<td width="30" style="width:50px;text-align:center;">%s</td>', $item_orcamento->Md_Largura) . chr(13);
                                    $html_relatorio .= sprintf('<td width="30" style="width:50px;text-align:center;">%s</td>', $item_orcamento->Qt_Item) . chr(13);

                                    $html_relatorio .= '<td width="350" style="width:450px;text-align:left;">';

                                    $handle = $item_orcamento->Cd_Orcamento;
                                    $moldurasItemOrcamento = $MolduraItemOrcamento->listarMolduraItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
                                    // if (is_array($moldurasItemOrcamento) && sizeof($arrMoldurasItemOrcamento) > 0) {
                                    foreach ($moldurasItemOrcamento as $moldura_item_orcamento) {
                                        $exibeComplexidade = '';
                                        if ($moldura_item_orcamento->codigoComplexidade > 0) {
                                            $exibeComplexidade = sprintf(' (%s)', $moldura_item_orcamento->nomeComplexidade);
                                        }
                                        $codigo_produto = $moldura_item_orcamento->CodigoProdutoFabricante != null ? $moldura_item_orcamento->CodigoProdutoFabricante : $moldura_item_orcamento->NovoCodigo;
                                        $html_relatorio .= '<li>' . $codigo_produto . '-' . $moldura_item_orcamento->DescricaoProduto . $exibeComplexidade . '</li> ';
                                    }
                                    // }


                                    $componentesItemOrcamento = $ComponenteItemOrcamento->listarComponenteItemOrcamento($item_orcamento->Cd_Item_Orcamento, $handle);
                                    // if (is_array($componentesItemOrcamento) && sizeof($componentesItemOrcamento) > 0) {
                                    foreach ($componentesItemOrcamento as $componente_item_orcamento) {
                                        $html_relatorio .= $componente_item_orcamento->DescricaoProduto . ', ';
                                    }
                                    // }


                                    $html_relatorio .= sprintf('<td style="">%s</td>', $item_orcamento->Ds_ObservacaoProducao) . chr(13);

                                    $html_relatorio .= '</td>';

                                    $html_relatorio .= '</tr>';

                                    if ($item_orcamento->Ds_Observacao_Producao !== '' && $CodigoPedido != $item_orcamento->Cd_Orcamento) {
                                        $html_relatorio .= sprintf('<tr><td colspan="7"><strong>Observações do pedido:</strong>%s</td></tr>', $item_orcamento->Ds_Observacao_Producao) . chr(13);
                                    }


                                    // if ($item_orcamento->Ds_Observacao_Producao !== '' && $CodigoPedido != $item_orcamento->Cd_Orcamento) {
                                    //     $html_relatorio .= sprintf('<tr><td colspan="7"><strong>Observações do pedido:</strong>%s</td></tr>', $item_orcamento->Ds_Observacao_Producao).chr(13);
                                    // }

                                    $total_bruto_cliente += (float) $item_orcamento->Vl_Bruto;
                                    $total_desconto_cliente += $item_orcamento->Vl_Desconto;
                                    $total_liquido_cliente += $item_orcamento->Vl_Bruto - $item_orcamento->Vl_Desconto;

                                    $CodigoCliente = $item_orcamento->CodigoCliente;
                                    $CodigoPedido = $item_orcamento->Cd_Orcamento;

                                    if ($aux == $quantidade) {
                                        $total_bruto_geral += $total_bruto_cliente;
                                        $total_desconto_geral += $total_desconto_cliente;
                                        $total_liquido_geral += $total_liquido_cliente - $total_desconto_cliente;

                                        $total_bruto_cliente = 0;
                                        $total_desconto_cliente = 0;
                                        $total_liquido_cliente = 0;

                                        $html_relatorio .= '</tbody>
                                                            </table></div>';

                                        $html_relatorio .= '<hr style="border-top:2pt dashed black !important;margin-top:5px;margin-bottom:5px;"><div style="page-break-inside: avoid;">&nbsp;</div>';
                                    }

                                    $aux++;
                                }

                                echo $html_relatorio;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
</div>

<div class="modal" id="impressaoRelatorio">
    <embed src="" width="800" type="application/pdf" height="600" id="arquivoPDF">

</div>

<!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>