<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>


<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
					</h3>
				</div>
				<div class="box-body">

					<div class="row">
						<?php if (count($this->empresas) > 1 && (count($orcamento->arrPermissoesUsuarioEmpresa) < 1 || count($orcamento->arrPermissoesUsuarioEmpresa) == 0)) { ?>
							<div class="form-group col-md-3 col-sm-12">
								<label for="empresa">Empresa</label>
								<select class="form-control" id="empresa" name="empresa">
									<option value="">Todos</option>
									<?php
									foreach ($this->empresas as $empresa) {
										echo sprintf(
											'<option %s value="%s">%s</option>',
											$empresa->CodigoEmpresa == $arrFiltro['empresa'] ? 'selected' : '',
											$empresa->CodigoEmpresa,
											$empresa->RazaoSocial
										);
									}
									?>
								</select>
							</div>
						<?php } ?>

						<div class="form-group col-md-3 col-sm-12">
							<label>Período Inicial:</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" value="<?php echo date('d/m/Y', strtotime($periodo_inicial)); ?>" id="periodo_inicial" name="periodo_inicial" class="form-control pull-right data">
							</div>
						</div>

						<div class="form-group col-md-3 col-sm-12">
							<label>Período Final:</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" value="<?php echo date('d/m/Y', strtotime($periodo_final)); ?>" id="periodo_final" name="periodo_final" class="form-control pull-right data">
							</div>
						</div>

						<div class="form-group col-md-3 col-sm-12">
							<label>Tipo de data</label>
							<select name="tipo_data[]" class="form-control" id="tipo_data">
								<option <?php echo isset($arrFiltro['tipo_data']) &&  $arrFiltro['tipo_data'] === 'data_criacao' ? 'selected' : ''; ?> value="data_criacao">Emissão do documento</option>
								<option <?php echo isset($arrFiltro['tipo_data']) &&  $arrFiltro['tipo_data'] === 'Dt_Orcamento' ? 'selected' : ''; ?> value="Dt_Orcamento">Cadastro do orçamento</option>
							</select>
						</div>

						<div class="form-group col-md-3 col-sm-12">
							<label>Tipo de documento</label>
							<select name="filtro_tipo_documento[]" class="form-control filtro_tipo_documento" id="filtro_tipo_documento" multiple="multiple">
								<?php
								foreach ($arrTiposDocumentos as $tipo_documento) {
									$selected = "";

									if (isset($arrFiltro['filtro_tipo_documento']) && in_array($tipo_documento['id_tipo_documento'], explode(',', $arrFiltro['filtro_tipo_documento']))) {
										$selected = "selected";
									}
								?>
									<option <?php echo $selected; ?> value="<?php echo $tipo_documento['id_tipo_documento']; ?>"><?php echo $tipo_documento['descricao']; ?></option>
								<?php
								}
								?>
							</select>
						</div>						

					</div>
					<div class="row">
						<div class="form-group col-md-6 col-sm-12">
							<button class="btn btn-primary" type="button" id="btnFiltroRelatorioDocumentosFiscais">Aplicar Filtro</button>
						</div>

						<?php if (count($consultaDocumentos) > 0 ) { ?>
							<div class="form-group col-md-6 col-sm-12">
							<button type="button" id="btnDownloadZipRelatorioDocumentosFiscais" class="downloadDocumentosFiscais col-md-2 col-xs-12 btn btn-warning pull-right">Download XML Notas</button>
							<button type="button" id="btnDownloadExcelRelatorioDocumentosFiscais" class="downloadDocumentosFiscais  col-md-2 col-xs-12  btn btn-info pull-right">Exportar Dados Planilha </button>

							</div>
							<?php } ?>
					</div>
				</div>

				<div style="clear:both;"></div>
				<div class="row">
					<div class="col-sm-12" id="relatorioPedidosEntregues">
						<div class="table-responsive">
							<table id="listagemRelatorioSimplificado" class="table table-bordered dataTable table-striped">
								<thead>
									<tr role="row">
										<th>Número da nota</th>
										<th>Pedido</th>
										<th>Operação</th>
										<th>Tipo de <br>Documento</th>
										<th>Finalidade <br>Emissão</th>
										<th>Retorno <br>SEFAZ</th>
										<th>Chave</th>
										<th>Numero</th>
										<th>Serie</th>
										<th>Numero e Serie</th>
										<th>Valor</th>
										<th>Emitido por</th>
										<th>Data de Emissão</th>
										<th>Hora de Emissão</th>
										<th>Cliente</th>
										<th>E-mail</th>
										<th>Qrcode</th>
										<th>Endereço<br>Consulta</th>
										<th>Caminho<br>Danfe</th>
										<!--
										<th>CaminhoXmlNotaFiscal</th>
										<th>CaminhoXmlCancelamento</th>
							-->
										<th>Emitente</th>
										<th>Ref</th>
										<th>Status</th>
										<th>Status Sefaz</th>
										<th>E-mail</th>
										<th>Ambiente</th>
										<th>Consultas</th>
										<th>Data <br>Cancelamento</th>
										<th>Justificativa</th>
										<th>Download XML</th>

									</tr>
								</thead>


								<tbody>
									<?php
									$CodigoCliente = 0;
									$CodigoPedido = 0;
									$total = 0;

									$total_nfe = 0;
									$total_nfse = 0;
									$total_nfce = 0;
									$aux = 1;
									$quantidade = count($consultaDocumentos);

									if ($quantidade > 0) {
									foreach ($consultaDocumentos as $documento) {
										if ($documento->TipoDocumentoSigla == 'NF') {
											$total_nfe += $documento->Valor;
										}

										if ($documento->TipoDocumentoSigla == 'NFS') {
											$total_nfse += $documento->Valor;
										}

										if ($documento->TipoDocumentoSigla == 'NFC') {
											$total_nfce += $documento->Valor;
										}
									?>
										<tr>
											<td><?php echo $documento->NFCRetornoId; ?></td>
											<td><?php echo $documento->PedidoId; ?></td>
											<td><?php echo $documento->NaturezaOperacao; ?></td>
											<td><?php echo $documento->TipoDocumento; ?></td>
											<td><?php echo $documento->FinalidadeEmissao == 1 ? 'NF-e normal' : '1'; ?></td>
											<td><?php echo $documento->MensagemSEFAZ; ?></td>
											<td><?php echo $documento->ChaveNfe; ?></td>
											<td><?php echo $documento->Numero; ?></td>
											<td><?php echo $documento->Serie; ?></td>
											<td><?php echo $documento->NumeroSerie; ?></td>
											<td><?php echo $documento->Valor; ?></td>
											<td><?php echo $documento->LoginEmissor; ?></td>
											<td><?php echo $documento->DataEmissao; ?></td>
											<td><?php echo $documento->DataHoraEmissao; ?></td>
											<td><?php echo $documento->ClienteNome; ?></td>
											<td><?php echo $documento->Email; ?></td>
											<td><?php echo $documento->QrcodeUrl; ?></td>
											<td><?php echo $documento->UrlConsultaNf; ?></td>
											<td><?php echo $documento->CaminhoDanfe; ?></td>
											<!--
											<td><?php echo $documento->CaminhoXmlNotaFiscal; ?></td>
											<td><?php echo $documento->CaminhoXmlCancelamento; ?></td>
									-->
											<td><?php echo $documento->CnpjEmitente; ?></td>
											<td><?php echo $documento->ref; ?></td>
											<td><?php echo $documento->Status; ?></td>
											<td><?php echo $documento->StatusSefaz; ?></td>
											<td><?php echo $documento->Email1; ?></td>
											<td><?php echo $documento->Ambiente; ?></td>
											<td><?php echo $documento->Consultas; ?></td>
											<td><?php echo $documento->DataCancelamento; ?></td>
											<td><?php echo $documento->Justificativa; ?></td>

											<td><a href="<?php echo URL . 'DocumentosFiscais/downloadXml/' . $documento->id_documento; ?>" target="_blank">Download</a></td>
										</tr>
									<?php

									}
									?>
										<tr>
											<th>Total NF-e</th>
											<th colspan="28">R$ <?php echo number_format($total_nfe, 2, ",","."); ?></th>
										</tr>

										<tr>
											<th>Total NFC-e</th>
											<th colspan="28">R$ <?php echo number_format($total_nfce, 2, ",","."); ?></th>
										</tr>
										
										<tr>
											<th>Total NFS-e</th>
											<th colspan="28">R$ <?php echo number_format($total_nfse, 2, ",","."); ?></th>
										</tr>
										
										<tr>
											<th>Total</th>
											<th colspan="28">R$ <?php echo number_format(($total_nfce + $total_nfe + $total_nfse), 2, ",","."); ?></th>
										</tr>										
									<?php
									} else {
									?>
										<tr><td style="text-align:center;" colspan="15">Nenhum dado para ser exibido</td></tr>
									<?php	
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>