<?php include_once DOCUMENT_ROOT . "src/View/Common/cabecalho.php"; ?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Configurações Iniciais</h3>
                </div>
            </div>
            <div class="box-body">
                <div id="financeiro-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-6">
                            <a class="btn btn-primary" href="<?php echo URL . $classe . "/cadastrar/"; ?>">Cadastrar Novo</a>
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr role="row">
                                        <th>Empresa</th>
                                        <th>Saldo Caixa</th>
                                        <th>Saldo Banco</th>
                                        <th>Saldo Cartão</th>
                                        <th>Saldo Aplicação</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                          $aux = 0;
                          foreach ($arrConfiguracoesIniciais as $key => $Financeiro) {
                              if ($aux & 1) { ?>
                                    <tr class="odd" role="row">
                                        <?php } else { ?>
                                    <tr class="even" role="row">
                                        <?php } ?>
                                        <td>
                                            <?php
                                              $indice_empresa = array_search($key, array_column($this->empresas, 'CodigoEmpresa'));
                                              echo $this->empresas[$indice_empresa]->RazaoSocial;
                                            ?>
                                        </td>                                        
                                        <td>
                                            R$ <?php echo number_format($Financeiro['caixa'], 2, ',','.'); ?>
                                        </td>
                                        <td>
                                            R$ <?php echo number_format($Financeiro['banco'], 2, ',','.'); ?>
                                        </td>
                                        <td>
                                            R$ <?php echo number_format($Financeiro['cartao'], 2, ',','.'); ?>
                                        </td>
                                        <td>
                                            R$ <?php echo number_format($Financeiro['aplicacao'], 2, ',','.'); ?>
                                        </td>
                                        <td><a class="" href="<?php echo URL .$classe . "/" . $acao . "/" . $this->empresas[$indice_empresa]->CodigoEmpresa;?>"><i class="fa fa-edit"></i>Editar</a></td>
                                    </tr>
                                    <?php
                          }
                          ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<?php include_once DOCUMENT_ROOT . "src/View/Common/rodape.php"; ?>