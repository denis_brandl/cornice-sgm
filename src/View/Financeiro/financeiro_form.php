<?php include_once DOCUMENT_ROOT . "src/View/Common/cabecalho.php"; ?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form method="POST" action="<?php echo URL . $classe . "/" . $metodo . "/" . ($handle ?: 0); ?>">
                    <?php if (!empty($msg_sucesso)) { ?>
                    <div class="callout callout-success">
                        <h4>Sucesso!</h4>
                        <p>
                            <?php echo $msg_sucesso; ?>
                        </p>
                    </div>
                    <?php } ?>
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Saldos Iniciais das contas</h3>
                        </div>
                    </div>                    
                    <div class="box-body">
                        <div class="form-group col-xs-12 col-md-3">
                          <div class="input-group">  
                            <label> Empresa </label>
                            <select name="UserId" class="form-control">
                              <option value="0">Selecione</option>
                              <?php foreach ($this->empresas as $empresa) { ?>
                                <option <?php echo $empresa->CodigoEmpresa == $handle ? 'selected'  : ''; ?> value="<?php echo $empresa->CodigoEmpresa;?>"><?php echo $empresa->RazaoSocial;?> - <?php echo $empresa->NomeFantasia;?></option>
                              <?php } ?>                              
                            </select>
                          </div>
                        </div>                      
                        <div class="form-group col-xs-12 col-md-2">
                            <label>Saldo Caixa</label>
                            <input class="form-control numberOnly campoMonetario" type="text" name="caixa" value="<?php echo number_format($arrConfiguracoesIniciais['caixa'], 2, ',', '.'); ?>">
                        </div>

                        <div class="form-group col-xs-12 col-md-2">
                            <label>Saldo Banco</label>
                            <input class="form-control numberOnly campoMonetario" type="text" name="banco" value="<?php echo number_format($arrConfiguracoesIniciais['banco'], 2, ',', '.'); ?>">
                        </div>

                        <div class="form-group col-xs-12 col-md-2">
                            <label>Saldo Cartão</label>
                            <input class="form-control numberOnly campoMonetario" type="text" name="cartao" value="<?php echo number_format($arrConfiguracoesIniciais['cartao'], 2, ',', '.'); ?>">
                        </div>

                        <div class="form-group col-xs-12 col-md-2">
                          <label>Saldo Aplicação</label>
                          <input class="form-control numberOnly campoMonetario" type="text" name="aplicacao" value="<?php echo number_format($arrConfiguracoesIniciais['aplicacao'], 2, ',', '.'); ?>">
                        </div>            
                    </div>

                    <div class="box-footer">
                        <input type="hidden" name="handle" value="<?php echo $handle || 0; ?>">
                        <button class="btn btn-primary" type="submit">Salvar</button>
                        <a class="btn btn-primary" href="<?php echo URL .$classe ."/Listar/"; ?>">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
</div>
<?php include_once DOCUMENT_ROOT . "src/View/Common/rodape.php"; ?>