  <?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<form autocomplete="off"  role="form" method="POST" action="<?php echo URL;?><?php echo $classe;?>/<?php echo $metodo;?>/<?php echo $composicoesPreco[0]->Código;?>">
						<?php if (!empty($msg_sucesso)) { ?>
							<div class="callout callout-success">
								<h4>Sucesso!</h4>
								<p><?php echo $msg_sucesso;?></p>
							</div>
						<?php } ?>
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Cadastro</h3>
							</div>						
						</div>
						<div class="box-body">
							<div class="form-group col-xs-6">
								<label>Margem Bruta de Lucro</label>
                <p class="small">Senão for definido no cadastro da moldura, esse será o % global<br>&nbsp;</p>
								<div class="input-group">
									<input class="form-control" type="number" step="0.1" value="<?php echo number_format($composicoesPreco[0]->VL_MARGEM_BRUTA,2);?>" name="VL_MARGEM_BRUTA" />																
									<span class="input-group-addon">%</span>
								</div>
							</div>							
							<div class="form-group col-xs-6">
								<label>Perda Média no Corte da Moldura</label>
								<p class="small">
									Percentual média de sobra de todas as barras de molduras.<br>
									Esse percentual será utilizado na soma da medida para fazer o orçamento.
								</p>
								<div class="input-group">
									<input class="form-control" type="number" step="0.1" value="<?php echo number_format($composicoesPreco[0]->VL_PERDA_MEDIA,2);?>" name="VL_PERDA_MEDIA" />																
									<span class="input-group-addon">%</span>
								</div>
							</div>							
						</div>
						
						<div class="box-body hide">						
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Lançar preços</h3>
                  <p class="small"> Demais itens que podem fazer parte do cálculo do pedido <br/> Recurso não habilitado no momento. </p>
								</div>
							</div>						
							<div class="form-group col-xs-6 hidden">
								<label>Vidro Anti-Reflexo</label>
								<div class="input-group">
									<span class="input-group-addon">R$ </span>
									<input class="form-control" type="number" step="0.1" value="<?php echo number_format($composicoesPreco[0]->VL_PRECO_VIDRO_ANTI,2);?>" name="VL_PRECO_VIDRO_ANTI" />
									<span class="input-group-addon">/m&sup2;</span>
								</div>
							</div>							
							<div class="form-group col-xs-6">
								<label>Rigi Pontas</label>
								<div class="input-group">
									<span class="input-group-addon">R$ </span>
									<input class="form-control" disabled type="number" step="0.1" value="<?php echo number_format($composicoesPreco[0]->VL_PRECO_RIGI_PONTAS,2);?>" name="VL_PRECO_RIGI_PONTAS" />
									<span class="input-group-addon">/mil</span>
								</div>
							</div>
							
							<div class="form-group col-xs-6 hidden">
								<label>Vidro Comum</label>
								<div class="input-group">
									<span class="input-group-addon">R$ </span>								
									<input class="form-control" type="number" step="0.1" value="<?php echo number_format($composicoesPreco[0]->VL_PRECO_VIDRO_COM,2);?>" name="VL_PRECO_VIDRO_COM" />
									<span class="input-group-addon">/m&sup2;</span>
								</div>
							</div>							
							<div class="form-group col-xs-6">
								<label>Prego</label>
								<div class="input-group">
									<span class="input-group-addon">R$ </span>								
									<input class="form-control" disabled type="number" step="0.1" value="<?php echo number_format($composicoesPreco[0]->VL_PRECO_PREGO,2);?>" name="VL_PRECO_PREGO" />
									<span class="input-group-addon">/mil</span>
								</div>
							</div>								
							
							<div class="form-group col-xs-6">
								<label>Suporte</label>
								<div class="input-group">
									<span class="input-group-addon">R$ </span>								
									<input class="form-control" disabled type="number" step="0.1" value="<?php echo number_format($composicoesPreco[0]->VL_PRECO_SUPORTE,2);?>" name="VL_PRECO_SUPORTE" />
									<span class="input-group-addon">/mil</span>
								</div>
							</div>							
							<div class="form-group col-xs-6">							
								<label>Grampo</label>
								<div class="input-group">
									<span class="input-group-addon">R$ </span>								
									<input class="form-control" disabled type="number" step="0.1" value="<?php echo number_format($composicoesPreco[0]->VL_PRECO_GRAMPO,2);?>" name="VL_PRECO_GRAMPO" />
									<span class="input-group-addon">/mil</span>
								</div>
							</div>															
							
							<div class="form-group col-xs-6">
								<label>Qtde de Voltas</label>
								<div class="input-group">
									<span class="input-group-addon">R$ </span>								
									<input class="form-control" disabled type="number" step="0.1" value="<?php echo number_format($composicoesPreco[0]->VL_QTD_VOLTAS,2);?>" name="VL_QTD_VOLTAS" />
									<span class="input-group-addon">/mil</span>
								</div>
							</div>							
							<div class="form-group col-xs-6">
								<label>Preço Fita Gomada</label>
								<div class="input-group">
									<span class="input-group-addon">R$ </span>								
									<input class="form-control" disabled type="number" step="0.1" value="<?php echo number_format($composicoesPreco[0]->VL_PRECO_FITA_GOMADA,2);?>" name="VL_PRECO_FITA_GOMADA" />
									<span class="input-group-addon">/mil</span>
								</div>
							</div>															
							
						</div>
						
						<div class="box-footer">
							<input type="hidden" name="handle" value="<?php echo $composicoesPreco[0]->Código;?>">
							<button class="btn btn-primary" type="submit">Salvar</button>
						</div>
					</form>
				</div>
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  