<?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  


    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">					
						<h3 class="box-title">
							<a class="btn btn-primary" href="<?php echo URL;?>index.php?class=<?php echo $classe; ?>&acao=cadastrar">Cadastrar Novo</a>
						</h3>					
					</div>
					<div class="box-body">					
						<div id="componentes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
							<div class="row">
								<div class="col-sm-6">
									<div id="example1_length" class="dataTables_length">
										<label>
											Mostrar
											<select class="form-control input-sm" name="example1_length" aria-controls="example1">
												<option value="10">10</option>
												<option value="25">25</option>
												<option value="50">50</option>
												<option value="100">100</option>
											</select>
											itens
										</label>
									</div>
								</div>
								<div class="col-sm-6">
									<div id="example1_filter" class="dataTables_filter">
										<label>
											<form autocomplete="off"  name="frmCliente" id="frmBuscar">
											Buscar:
												<input class="form-control input-sm" type="search" placeholder="" aria-controls="example1">
											</form>	
										</label>
									</div>
								</div>								
							</div>
							<div class="row">
								<div class="col-sm-12">
									<table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Descrição</th>
												<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Sigla</th>
												<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Cotação</th>
												<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 162px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Data Atualização</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												$aux = 0;
												foreach ($moedas as $moeda) {
												if ($aux & 1) {
											?>
												<tr class="odd" role="row">
											<?php } else { ?>	
												<tr class="even" role="row">
											<?php } ?>	
												<td class="sorting_1"><?php echo $moeda->DescricaoMoeda; ?></td>
												<td class="sorting_1"><?php echo $moeda->SiglaMoeda; ?></td>
												<td class="sorting_1"><?php echo $moeda->Cotacao; ?></td>
												<?php 
													$DataAtualizacao = new DateTime($moeda->DataAtualizacao);
													$dataAtualizacao = $DataAtualizacao->format('d/m/Y');
												?>												
												<td class="sorting_1"><?php echo $dataAtualizacao; ?></td>
												<td><a class="" href="<?php echo URL.'Banco/editar/'. $moeda->CodigoMoeda; ?>"><i class="fa fa-edit"></i>Editar</a></td>
											</tr>
											<?php } ?>
										</tbody>
										<tfoot>
											<tr>
												<th>Descrição</th>
												<th>Sigla</th>
												<th>Cotação</th>
												<th>Data Atualização</th>
											</tr>
										</tfoot>
									</table>									
								</div>								
							</div>
						</div>
					</div>
					<div class="box-footer clearfix">
						<ul class="pagination pagination-sm no-margin pull-right">
						
							<li><a class='box-navegacao <?=$exibir_botao_inicio?>' href="index.php?class=<?php echo $classe; ?>&acao=listar&page=<?=$primeira_pagina?>" title="Primeira Página">Primeira</a></li>
							<li><a class='box-navegacao <?=$exibir_botao_inicio?>' href="index.php?class=<?php echo $classe; ?>&acao=listar&page=<?=$pagina_anterior?>" title="Página Anterior">Anterior</a></li>
							
							<?php  
								/* Loop para montar a páginação central com os números */   
								for ($i=$range_inicial; $i <= $range_final; $i++) {   
								$destaque = ($i == $pagina_atual) ? 'active' : '' ;  
								?>   
								<li class='box-numero <?=$destaque?>'><a href="index.php?class=<?php echo $classe; ?>&acao=listar&page=<?=$i?>"><?=$i?></a></li>
								<?php } ?>    
								<li><a href="index.php?class=<?php echo $classe; ?>&acao=listar&page=<?=$proxima_pagina?>" title="Próxima Página">Próxima</a></li>
								<li><a href="index.php?class=<?php echo $classe; ?>&acao=listar&page=<?=$ultima_pagina?>" title="Última Página">Último</a></li>							
						</ul>
					</div>					
				</div>				
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  