<?php include_once(DOCUMENT_ROOT."src/View/Common/cabecalho.php"); ?>  
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box">
<div class="box-header">					
<h3 class="box-title"></h3>					
</div>              
<div class="box-body">
<div id="account-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
<div class="row">
<div class="col-sm-6">
<a class="btn btn-primary" href="<?php echo URL.$classe.'/cadastrar/';?>">Cadastrar Novo</a>
</div>
<div class="col-sm-6">
</div>
</div>
<div class="row">
<div class="col-sm-12">
<table class="table table-bordered table-striped dataTable">
<thead>
<tr role="row">
<th>AccountId</th>
<th>UserId</th>
<th>AccountName</th>
<th>tipo</th>
</tr>
</thead>
<tbody>
<?php 
$aux = 0;
foreach ($arrAccount as $Account) {
if ($aux & 1) {
?>
<tr class="odd" role="row">
<?php } else { ?>	
<tr class="even" role="row">
<?php } ?>	
<td><?php echo $Account->AccountId; ?></td>
<td><?php echo $Account->UserId; ?></td>
<td><?php echo $Account->AccountName; ?></td>
<td><?php echo $Account->tipo; ?></td>
<td><a class="" href="<?php echo URL.$classe.'/'.$acao.'/'. $Account->AccountId; ?>"><i class="fa fa-edit"></i>Editar</a></td>
</tr>
<?php } ?>
</tbody>
</table>									
</div>								
</div>                
</div>
</div>
</div>
</div>
</div>
</section>
</div>
<?php include_once(DOCUMENT_ROOT."src/View/Common/rodape.php"); ?>  
