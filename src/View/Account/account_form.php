<?php include_once(DOCUMENT_ROOT."src/View/Common/cabecalho.php"); ?>
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box">
<form method="POST" action="<?php echo URL.$classe."/".$metodo."/".($Account->AccountId ?: 0);?>">
<?php if (!empty($msg_sucesso)) { ?>
<div class="callout callout-success">
<h4>Sucesso!</h4>
<p><?php echo $msg_sucesso;?></p>
</div>
<?php } ?>
<div class="box box-primary">
<div class="box-header with-border">
<h3 class="box-title">Cadastro da sua Account</h3>
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>AccountId</label>
<input class="form-control" type="text" name="accountid" value="<?php echo $Account->accountid;?>">
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>UserId</label>
<input class="form-control" type="text" name="userid" value="<?php echo $Account->userid;?>">
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>AccountName</label>
<input class="form-control" type="text" name="accountname" value="<?php echo $Account->accountname;?>">
</div>
</div>
<div class="box-body">
<div class="form-group col-xs-12 col-md-2">
<label>tipo</label>
<input class="form-control" type="text" name="tipo" value="<?php echo $Account->tipo;?>">
</div>
</div>
<div class="box-footer">
<input type="hidden" name="handle" value="<?php echo $Account->AccountId;?>">
<button class="btn btn-primary" type="submit">Salvar</button>
<a class="btn btn-primary" href="<?php echo URL.$classe.'/Listar/';?>">Voltar</a>
</div>                
</form>
</div>
</div>
</div>
</section>
</div>
<?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  
