  <?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <form autocomplete="off" role="form" method="POST" action="<?php echo URL; ?><?php echo $classe; ?>/<?php echo $metodo; ?>/<?php echo (int) $complexidades[0]->codigoComplexidade; ?>">
            <?php if (!empty($msg_sucesso)) { ?>
              <div class="callout callout-success">
                <h4>Sucesso!</h4>
                <p><?php echo $msg_sucesso; ?></p>
              </div>
            <?php } ?>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Cadastro</h3>
              </div>
            </div>
            <div class="box-body">
              <div class="form-group col-xs-12 col-md-4">
                <label>Nome</label>
                <input class="form-control col-sm-6" type="text" value="<?php echo $complexidades[0]->nome; ?>" name="nome" />
              </div>
            </div>

            <div class="box-body">
              <div class="form-group col-xs-12 col-md-3">
                <label>Aplicação</label>
                <select name="aplicacao" class="form-control">
                  <option value="D" <?php echo $complexidades[0]->aplicacao == 'D' ? 'selected' : ''; ?>>Desconto</option>
                  <option value="A" <?php echo $complexidades[0]->aplicacao == 'A' ? 'selected' : ''; ?>>Acréscimo</option>
                </select>
              </div>

              <div class="form-group col-xs-12 col-md-3">
                <label>Formação do preço</label>
                <select name="definicao_preco" id="definicao_preco" class="form-control">
                  <option value="1" <?php echo $complexidades[0]->definicao_preco == '1' ? 'selected' : ''; ?>>Preço Fixo em R$ </option>
                  <option value="2" <?php echo $complexidades[0]->definicao_preco == '2' ? 'selected' : ''; ?>>Percentual sobre o preço de custo</option>
                </select>
              </div>

              <div class="form-group col-xs-12 col-md-3">
                <label>Valor</label>
                <input class="form-control col-sm-6 numberOnly campoMonetario" type="text" value="<?php echo number_format($complexidades[0]->valor, 2, ",", "."); ?>" name="valor" />
              </div>

            </div>

            <div class="box-body">
              <div class="form-group col-xs-12 col-md-4">
                <label>Status</label>
                <select name="status" class="form-control">
                  <option value="0" <?php echo $complexidades[0]->status == '0' ? 'selected' : ''; ?>>Inativo</option>
                  <option value="1" <?php echo $complexidades[0]->status == '1' ? 'selected' : ''; ?>>Ativo</option>
                </select>
              </div>
            </div>

            <div class="box-footer">
              <button class="btn btn-primary" type="submit">Salvar</button>
              <a class="btn btn-primary" href="<?php echo URL; ?><?php echo $classe; ?>/listar/">Voltar</a>
            </div>

          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content-wrapper -->
  </div>

  <?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>