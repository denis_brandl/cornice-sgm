<?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  


    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">					
						<h3 class="box-title"></h3>					
					</div>
					<div class="box-body">					
						<div id="componentes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
							<div class="row">
								<div class="col-sm-6">
									&nbsp;
								</div>
								<div class="col-sm-6">
									<div id="example1_filter" class="dataTables_filter">
										<form autocomplete="off"  name="frmCliente" id="frmBuscar">
										<label>
											Buscar:												
												<select name="coluna" class="form-control">
													<?php foreach ($arrCamposBusca as $value => $descricao) { 
															$selected = '';
															if ($value == $coluna) {
																$selected = 'selected';
															}
													?>
														<option value="<?php echo $value;?>" <?php echo $selected;?>><?php echo $descricao;?></option>
													<?php } ?>
												</select>
												<input class="form-control input-sm" type="search" placeholder="" value="<?php echo $buscar;?>" name="buscar" aria-controls="example1">
												<input type="button" class="btn btn-primary" id="btnBuscar" value="Buscar" />
										</label>
										</form>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Sigla</th>
                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Nome</th>
												<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">&nbsp;</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												$aux = 0;
												foreach ($estados as $estado) {
												if ($aux & 1) {
											?>
												<tr class="odd" role="row">
											<?php } else { ?>	
												<tr class="even" role="row">
											<?php } ?>	
												<td class="sorting_1"><?php echo $estado->uf; ?></td>
                        <td class="sorting_1"><?php echo $estado->nome; ?></td>
												<td><a class="" href="<?php echo URL.$classe.'/editar/'. $estado->codigo_uf; ?>"><i class="fa fa-edit"></i>Editar</a></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>									
								</div>								
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  