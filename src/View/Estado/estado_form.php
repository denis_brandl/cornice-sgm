  <?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <form autocomplete="off"  role="form" method="POST" action="<?php echo URL;?><?php echo $classe;?>/<?php echo $metodo;?>/<?php echo (int) $estados[0]->codigo_uf;?>">
                <?php if (!empty($msg_sucesso)) { ?>
                  <div class="callout callout-success">
                    <h4>Sucesso!</h4>
                    <p><?php echo $msg_sucesso;?></p>
                  </div>
                <?php } ?>
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Cadastro</h3>
                  </div>						
                </div>
                <div class="box-body">
                  <div class="form-group col-xs-12 col-md-1">
                    <label>UF</label>
                    <input class="form-control col-sm-1" maxlength="2" type="text" value="<?php echo $estados[0]->uf;?>" name="uf" />
                  </div>
                  <div class="form-group col-xs-12 col-md-4">
                    <label>Nome</label>
                    <input class="form-control col-sm-4" type="nome" value="<?php echo $estados[0]->nome;?>" name="nome" />
                  </div>
                </div>

                <div class="box-body">
                  <div class="form-group col-xs-12 col-md-3">
                    <label>Aliquota ICMS</label>
                    <div class="input-group">
                      <input class="form-control col-sm-3" step="0.1" type="number" value="<?php echo $estados[0]->aliquota_icms;?>" name="aliquota_icms" />
                      <span class="input-group-addon">%</span>
                    </div>                    
                  </div>
                  <div class="form-group col-xs-12 col-md-3">
                    <label>Aliquota FCP
                      <span class="small">Fundo de combate a pobreza</span>
                    </label>
                    <div class="input-group">
                      <input class="form-control col-sm-3" step="0.1" type="number" value="<?php echo $estados[0]->aliquota_fcp;?>" name="aliquota_fcp" />
                      <span class="input-group-addon">%</span>
                    </div>
								</div>                    

                  </div>
                </div>                

                <div class="box-footer">
                  <button class="btn btn-primary" type="submit">Salvar</button>
                  <a class="btn btn-primary" href="<?php echo URL;?><?php echo $classe;?>/listar/">Voltar</a>
                </div>              

            </form>
          </div>          
        </div>
      </div>
    </section>
  <!-- /.content-wrapper -->
</div>

 <?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  