  <?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>


  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <form autocomplete="off" role="form" method="POST" action="<?php echo URL; ?>index.php?class=<?php echo $classe; ?>&acao=<?php echo $metodo; ?>">
            <?php if (!empty($msg_sucesso)) { ?>
              <div class="callout callout-success">
                <h4>Sucesso!</h4>
                <p><?php echo $msg_sucesso; ?></p>
              </div>
            <?php } ?>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Cadastro</h3>
              </div>
            </div>
            <div class="box-body">
              <div class="form-group">
                <label>Descrição</label>
                <input class="form-control" type="text" value="<?php echo $grupos[0]->DescricaoGrupo; ?>" name="DescricaoGrupo" />
              </div>
            </div>

            <div class="box-footer">
              <input type="hidden" name="handle" value="<?php echo $grupos[0]->CodigoGrupo; ?>">
              <button class="btn btn-primary" type="submit">Salvar</button>
              <a class="btn btn-primary" href="<?php echo URL . $classe; ?>/listar/">Voltar</a>
            </div>
          </form>
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>