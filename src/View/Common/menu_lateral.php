<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">

      <a href="<?php echo URL . ($grupo_usuario == 4 ? 'Producao/listar/' : 'Dashboard/index/'); ?>" class="navbar-brand">
        <span class="visible-lg">
          <img class="img-responsive" src="<?php echo URL; ?>images/logo-maestria-topo-transparente.png" />
          <p style="font-size: 14px;font-weight: bold;color:#BE1522">Sistema para molduraria</p>
        </span>

        <span class="visible-xs visible-sm">
          <img class="hidden-md img-responsive" src="<?php echo URL; ?>images/logo-maestria-topo-transparente-mobile.png" />
        </span>
      </a>
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
        <i class="fa fa-bars" style="color:#000"></i>
      </button>

      <?php /*
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle Navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="<?php echo URL; ?>Dashboard/index/" class="navbar-brand">
        <!-- logo for regular state and mobile devices -->
        <span class="visible-lg">
          <img class="img-responsive" src="<?php echo URL; ?>images/logo-maestria-topo-transparente.png" />
        </span>

        <span class="visible-xs visible-sm">
          <img class="hidden-md img-responsive" src="<?php echo URL; ?>images/logo-maestria-topo-transparente-mobile.png" />
        </span>

      </a>
      */ ?>
    </div>

    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
      <ul class="nav navbar-nav">
        <?php // if (1==1 || in_array($_SESSION['handle_grupo'], [1, 2, 3])) { ?>
          <?php if ( ($arrPermissoesCabecalho[$indicePedidos]->visualizar == 1)) { ?>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-shopping-cart"></i> Pedidos <span class="caret"></span></a>

            <ul class="dropdown-menu">
              <li><a href="<?php echo URL; ?>Orcamento/listar/">Listar</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="<?php echo URL; ?>Orcamento/salvar/">Novo</a></li>
              <li role="separator" class="divider"></li>
              <?php if ($arrPermissoesCabecalho[$indiceDocumentosFiscais]->visualizar == 1 && $habilita_tributacao) { ?>
                <li><a href="<?php echo URL; ?>DocumentosFiscais/listar/">Documentos Fiscais</a></li>
                <li role="separator" class="divider"></li>
              <?php } ?>

              <?php if ($arrPermissoesCabecalho[$indiceProducao]->visualizar == 1) { ?>
                <li><a href="<?php echo URL; ?>Producao/listar/">Produção</a></li>
              <?php } ?>
            </ul>
          </li>
          <?php } ?>
        <?php // } ?>

        <?php if ( ($arrPermissoesCabecalho[$indiceContasAPagar]->visualizar == 1 || $arrPermissoesCabecalho[$indiceContasAReceber]->visualizar == 1) && $habilita_financeiro) { ?>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-money" aria-hidden="true"></i> Financeiro <span class="caret"></span></a>
            </a>
            <ul class="dropdown-menu">
              <?php if ($arrPermissoesCabecalho[$indiceContasAPagar]->visualizar == 1) { ?>
              <li><a href="<?php echo URL; ?>Bills/listar/">Pagamentos</a></li>
              <li role="separator" class="divider"></li>
              <?php } ?>
              <?php if ($arrPermissoesCabecalho[$indiceContasAReceber]->visualizar == 1) { ?>
              <li><a href="<?php echo URL; ?>Assets/listar/">Recebimentos</a></li>
              <?php } ?>
            </ul>
          </li>
        <?php } ?>

        <?php if (in_array($_SESSION['handle_grupo'], [1, 2, 3])) { ?>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-bar-chart" aria-hidden="true"></i> Relatórios <span class="caret"></span></a>
            </a>
            <ul class="dropdown-menu">
            <li><a href="<?php echo URL; ?>Relatorio/pedidosSimplificado/">Pedidos Simplificado</a></li>
              <li role="separator" class="divider"></li>              
              <li><a href="<?php echo URL; ?>Relatorio/pedidosEntregues/">Pedidos Detalhado</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="<?php echo URL; ?>Relatorio/impressaoFabrica/">Impressao Fábrica</a></li>
              <li role="separator" class="divider"></li>
              <?php if (in_array($_SESSION['handle_grupo'], [1])) { ?>
              <li><a href="<?php echo URL; ?>Relatorio/comissao/">Comissão</a></li>
              <?php } ?>
              <?php if ($arrPermissoesCabecalho[$indiceDocumentosFiscais]->visualizar == 1 && $habilita_tributacao) { ?>
                <li role="separator" class="divider"></li>
              <li><a href="<?php echo URL; ?>Relatorio/documentosFiscais/">Documentos Fiscais</a></li>
              <?php } ?>              
            </ul>
          </li>
        <?php } ?>

        <?php if (count($arrAcessosMenuAdministracao) > 0) { ?>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-cog"></i> Administração <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <?php foreach ($arrAcessosMenuAdministracao as $class) {
                if (($class['modulo'] == 'Bills' || $class['modulo'] == 'Assets' || $class['modulo'] == 'Componente')) {
                  continue;
                }
              ?>
                <li>
                  <?php
                  if ($class['modulo'] != 'ComposicaoPreco') { ?>
                    <a href="<?php echo URL . $class['modulo'] . '/listar/'; ?>">
                    <?php } else { ?>
                      <a href="<?php echo URL . $class['modulo'] . '/editar/1'; ?>">
                      <?php } ?>
                      <i class="fa <?php echo $class['icone']; ?>"></i>
                      <?php echo $class['descricao']; ?>
                      </a>
                </li>
                <li role="separator" class="divider"></li>
              <?php } ?>
            </ul>
          </li>
        <?php } ?>

        <!--
        <li class="hidden-xs" style="font-size: 18px;font-weight: bold;">
          <a href="#">
            SISTEMA PARA MOLDURAS
          </a>
        </li>
        -->

      </ul>

    </div>

    <div class="navbar-custom-menu pull-right">
      <ul class="nav navbar-nav">
        <!-- User Account Menu -->
        <li class="dropdown user user-menu hidden-xs">
          <!-- Menu Toggle Button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <!-- The user image in the navbar-->
            <img src="<?php echo URL; ?>images/maestria-sistema-topo.png" class="img-responsive" alt="User Image">
          </a>
        </li>
        <li class="messages-menu">
          <!-- Menu toggle button -->
          <a href="<?php echo URL; ?>Usuario/perfil/">
            <i class="fa fa-user"></i>
            Olá, <?php echo isset($_SESSION['NomeUsuario']) ? $_SESSION['NomeUsuario'] : ''; ?>
          </a>
        </li>
        <li class="messages-menu">
          <!-- Menu toggle button -->
          <a href="<?php echo URL . 'Usuario'; ?>/logout/">
            <i class="fa fa-sign-out"></i>
            Sair
          </a>
        </li>
      </ul>
    </div>
</nav>
