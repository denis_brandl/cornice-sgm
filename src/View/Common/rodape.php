<?php
require_once './src/Model/Configuracoes.php';
require_once './src/Model/Orcamento.php';
require_once './src/Model/Empresas.php';
$configuracoes = new Configuracoes();
$parceria_ruberti = filter_var($configuracoes->listarConfiguracao('parceria_ruberti')->valor, FILTER_VALIDATE_BOOLEAN);
$exibir_total_custo_item = filter_var($configuracoes->listarConfiguracao('exibir_total_custo_item')->valor, FILTER_VALIDATE_BOOLEAN);
$exibir_medida_final_do_quadro = filter_var($configuracoes->listarConfiguracao('exibir_medida_final_do_quadro')->valor, FILTER_VALIDATE_BOOLEAN);
$exibir_valores_individuais = filter_var($configuracoes->listarConfiguracao('exibir_valores_individuais')->valor, FILTER_VALIDATE_BOOLEAN);
$modelo_empresa = $configuracoes->listarConfiguracao('modelo_empresa')->valor;

$objOrcamento = new Orcamento();
$objEmpresasRodape = new Empresas();
$arrEmpresasRodape = $objEmpresasRodape->listarTodos(0, 0, 'status', 1);



$multiplasEmpresas = (count($arrEmpresasRodape) > 1 && (count($objOrcamento->arrPermissoesUsuarioEmpresa) < 1 || count($objOrcamento->arrPermissoesUsuarioEmpresa) == 0));
?>

<!-- Main Footer -->
<footer class="main-footer">
  <!-- To the right -->

  <div class="col-md-<?php echo $parceria_ruberti ? '4' : '8'; ?>">
    <strong>Copyright &copy;
      <?php echo date('Y'); ?> <a href="http://www.maestriasistema.com.br/" target="_blank">Maestria Sistema para Molduras</a>.
    </strong>
  </div>

  <?php if ($parceria_ruberti) { ?>
    <div class="col-md-4" style="text-align:center;">

      <a style="font-weight:bold; text-decoration:none;color: #070707;font-size:14px;" href="https://www.rubertimolduras.com.br/" target="_blank">Parceria exclusiva Ruberti Molduras </a>
    </div>
  <?php } ?>

  <div class="col-md-<?php echo $parceria_ruberti ? '4' : '4'; ?>" style="text-align:right;">
    Para dúvidas, consulte o <strong><a target="_blank" href="<?php echo URL; ?>manual">Manual</a></strong> ou entre em
    contato no whatsapp <a href="https://web.whatsapp.com/send?phone=5547991601607&text=<?php echo urlencode('Ajuda com o sistema Maestria'); ?>" target="_blank">(47) 99160-1607</a>
  </div>
  <!-- Default to the left -->


</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Create the tabs -->
  <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
    <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
  </ul>
  <!-- Tab panes -->
  <div class="tab-content">
    <!-- Home tab content -->
    <div class="tab-pane active" id="control-sidebar-home-tab">
      <h3 class="control-sidebar-heading">Recent Activity</h3>
      <ul class="control-sidebar-menu">
        <li>
          <a href="javascript::;">
            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

            <div class="menu-info">
              <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

              <p>Will be 23 on April 24th</p>
            </div>
          </a>
        </li>
      </ul>
      <!-- /.control-sidebar-menu -->

      <h3 class="control-sidebar-heading">Tasks Progress</h3>
      <ul class="control-sidebar-menu">
        <li>
          <a href="javascript::;">
            <h4 class="control-sidebar-subheading">
              Custom Template Design
              <span class="label label-danger pull-right">70%</span>
            </h4>

            <div class="progress progress-xxs">
              <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
            </div>
          </a>
        </li>
      </ul>
      <!-- /.control-sidebar-menu -->

    </div>
    <!-- /.tab-pane -->
    <!-- Stats tab content -->
    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
    <!-- /.tab-pane -->
    <!-- Settings tab content -->
    <div class="tab-pane" id="control-sidebar-settings-tab">
      <form autocomplete="off" method="post">
        <h3 class="control-sidebar-heading">General Settings</h3>

        <div class="form-group">
          <label class="control-sidebar-subheading">
            Report panel usage
            <input type="checkbox" class="pull-right" checked>
          </label>

          <p>
            Some information about this general settings option
          </p>
        </div>
        <!-- /.form-group -->
      </form>
    </div>
    <!-- /.tab-pane -->
  </div>
</aside>
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo URL; ?>vendor/almasaeed2010/adminlte/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/jQueryUI/jquery-ui.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo URL; ?>vendor/almasaeed2010/adminlte/dist/js/app.min.js"></script>
<script src="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.js"></script>

<script src="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/moment.min.js"></script>
<script src="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/datetime-moment.js"></script>


<script src="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/select2/select2.min.js"></script>
<script src="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/select2/i18n/pt-BR.js"></script>

<script src="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js"></script>

<script>
  const modelo_empresa = '<?php echo isset($modelo_empresa) ? $modelo_empresa : 'molduraria'; ?>'
  const habilita_financeiro = '<?php echo isset($this->habilita_financeiro) ? $this->habilita_financeiro : 'false'; ?>'
  const parceria_ruberti = !!'<?php echo $parceria_ruberti ?>'
  const exibir_total_custo_item = !!'<?php echo $exibir_total_custo_item ?>'
  const exibir_medida_final_do_quadro = !!'<?php echo $exibir_medida_final_do_quadro ?>'
  const exibir_valores_individuais = !!'<?php echo $exibir_valores_individuais ?>'
  const multiplasEmpresas = '<?php echo $multiplasEmpresas ?>'
  const boolMolduraria = (modelo_empresa === 'molduraria');
  var formDataUploadProduto = new FormData();
  const possuiFinanceiro = ('<?php echo (isset($orcamentos[0]->idContaReceber) && $orcamentos[0]->idContaReceber > 0) ? 'true' : 'false'; ?>' === 'true')

  const isIphone = [
    'iPad Simulator',
    'iPhone Simulator',
    'iPod Simulator',
    'iPad',
    'iPhone',
    'iPod'
  ].includes(navigator.platform) || (navigator.userAgent.includes("Mac") && "ontouchend" in document)

  $(document).ready(function() {

    $('input[name="Telefone1"]').inputmask({
      "mask": ['(99) 9999-9999', '(99) 99999-9999'],
      //'autoUnmask': true
    });

    if ($('.tipoCliente').length > 0) {
      if ($('.tipoCliente').val() == 'J') {
        $(".nomeRazao").html('Razão Social');
        $(".documentoCliente").html("CNPJ")
        $('input[name="CGC"]').inputmask({
          "mask": "99.999.999/9999-99",
          'autoUnmask': true
        });
        $(".nomeFantasia").show()
        $(".inscricaoEstadual").show()
      } else {
        $(".nomeRazao").html('Nome');
        $(".documentoCliente").html("CPF")
        $('input[name="CGC"]').inputmask({
          "mask": "999.999.999/99",
          'autoUnmask': true
        });
        $(".nomeFantasia").hide()
        $(".inscricaoEstadual").hide()
      }
    } else {
      if ($('input[name="CGC"]').length > 0) {
        $('input[name="CGC"]').inputmask({
          "mask": "99.999.999/9999-99",
          'autoUnmask': true
        });
      }
    }

    if (!boolMolduraria) {
      $(".adicionar-quadro").slideDown("slow");
    }

    $('#CGCFornecedor').inputmask({
      "mask": "99.999.999/9999-99",
      'autoUnmask': true
    });
    $('#InscricaoEstadualFornecedor').inputmask({
      regex: "^\d{1,15}$"
    });

    $('input[name="CEP"]').inputmask({
      "mask": "99999-999",
      'autoUnmask': true
    });


  });

  const optionsComplexidade = (type, complexidadeSelecionada) => {
    const selected = type.handle == complexidadeSelecionada ? 'selected' : ''
    const descricao_valor = type.definicao_preco == 1 ? `R$ ${type.valor}` : `${type.valor}%`
    return `<option
                value="${type.handle}"
                ${selected}
                descricao="${type.nome}"
              >
                ${type.nome} (${type.aplicacao == 'A' ? ' Adiciona ' : ' Diminui '} ${descricao_valor} )
              </option>`
  }

  const consultaComplexidadesMoldura = async (moldura, complexidade = 0) => {
    const complexidades = await new Promise(resolve => {
      $.ajax({
        url: '<?php echo URL; ?>ComplexidadeProduto/listarComplexidadePorProduto/',
        type: 'post',
        data: 'handle=' + moldura,
        dataType: 'json',
        success: function(json) {
          resolve(json)
        }
      });
    })

    if (complexidades.success == "1") {
      return Object.values(complexidades.data)
        .map(type => optionsComplexidade(type, complexidade))
        .join('')
    }


  }

  var zIndexDatePicker = 0;
  $('.dataModal').click(function() {
    zIndexDatePicker = $('#ui-datepicker-div').css('z-index')
    $('#ui-datepicker-div').css('z-index', 1060)
  })
  $(".data").datepicker({
    dateFormat: 'dd/mm/yy',
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    nextText: 'Próximo',
    prevText: 'Anterior',
    onClose: function(e) {
      if (zIndexDatePicker !== 0) {
        $('#ui-datepicker-div').css('z-index', zIndexDatePicker)
      }
    }
  });

  $(function() {

    $('#example1').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": false,
      "className": 'details-control',
      language: {
        emptyTable: "Nenhum registro encontrado!"
      },
      dom: 'Bfrtip',
      buttons: [{
        extend: 'print',
        text: 'Imprimir',
        autoPrint: false,
        title: 'Relatório de pedidos entregues'
      }]
    });
  });

  $(document).ready(function() {

    var refreshSn = function() {
      var time = 120000;
      setTimeout(
        function() {
          $.ajax({
            url: '<?php echo URL; ?>Usuario/renovaSessao/',
            cache: false,
            complete: function() {
              refreshSn();
            }
          });
        },
        time
      );
    };

    refreshSn();

    $('#btnBuscar').on('click', function() {
      var url = "<?php echo URL . $classe . '/listar/'; ?>0/buscar=" + $('select[name=coluna]').val() + "=" + $('input[name=buscar]').val()
      if ($('select[name=filtro_por_status]').val() > 0) {
        url = url + "|Id_Situacao=" + $('select[name=filtro_por_status]').val();
      }
      window.location = url;
    });

    $('#btnBuscarPedidosEntregues').on('click', function() {
      var filtro_situacao = '';
      if ($('#filtro_situacao').val() != null) {
        filtro_situacao = $('#filtro_situacao').val();
      }

      var vendedor = '';
      if ($('#vendedor').val() != null) {
        vendedor = $('#vendedor').val();
      }

      var empresa = '';
      if ($('#empresa').val() != null) {
        empresa = $('#empresa').val();
      }
      var url = "<?php echo URL . $classe . '/pedidosEntregues/'; ?>0/buscar=" + $('#periodo_inicial').val() + "|" + $('#periodo_final').val() + "|" + filtro_situacao + "|" + $('#filtro_pago').val() + '|' + $('#tipo_data').val() + '|' + vendedor + '|' + empresa;
      window.location = url;
    });

    $('#btnFiltroRelatorioPedidosSimplificado').on('click', function() {
      var filtro_situacao = '';
      if ($('#filtro_situacao').val() != null) {
        filtro_situacao = $('#filtro_situacao').val();
      }

      var vendedor = '';
      if ($('#vendedor').val() != null) {
        vendedor = $('#vendedor').val();
      }

      var empresa = '';
      if ($('#empresa').val() != null) {
        empresa = $('#empresa').val();
      }

      var cliente = '';
      if ($('#filtro_cliente').val() != null) {
        cliente = $('#filtro_cliente').val();
      }

      var pedido = '';
      if ($('#pedido').val() != null) {
        pedido = $('#pedido').val();
      }

      var somente_pedidos_validos = '';
      if ($('#somente_pedidos_validos').prop('checked')) {
        somente_pedidos_validos = '1';
      }

      var url = "<?php echo URL . $classe . '/pedidosSimplificado/'; ?>0/buscar=" + $('#periodo_inicial').val() + "|" + $('#periodo_final').val() + "|" + filtro_situacao + "|" + '' + '|' + $('#tipo_data').val() + '|' + vendedor + '|' + empresa + '|' + cliente + '|' + pedido + '|' + somente_pedidos_validos;
      window.location = url;
    });

    $('#btnFiltroRelatorioDocumentosFiscais').on('click', function() {
      var filtro_tipo_documento = '';
      if ($('#filtro_tipo_documento').val() != null) {
        filtro_tipo_documento = $('#filtro_tipo_documento').val();
      }

      var vendedor = '';
      // if ($('#vendedor').val() != null) {
      //   vendedor = $('#vendedor').val();
      // }

      var empresa = '';
      if ($('#empresa').val() != null) {
        empresa = $('#empresa').val();
      }

      var cliente = '';
      // if ($('#filtro_cliente').val() != null) {
      //   cliente = $('#filtro_cliente').val();
      // }

      var pedido = '';
      // if ($('#pedido').val() != null) {
      //   pedido = $('#pedido').val();
      // }

      var somente_pedidos_validos = '';
      // if ($('#somente_pedidos_validos').prop('checked')) {
      //   somente_pedidos_validos = '1';
      // }

      var url = "<?php echo URL . $classe . '/documentosFiscais/'; ?>0/buscar=" + $('#periodo_inicial').val() + "|" + $('#periodo_final').val() + "|" + filtro_tipo_documento + "|" + '' + '|' + $('#tipo_data').val() + '|' + vendedor + '|' + empresa + '|' + cliente + '|' + pedido + '|' + somente_pedidos_validos;
      window.location = url;
    });

    $('.downloadDocumentosFiscais').on('click', function() {
      var filtro_situacao = '';
      // if ($('#filtro_situacao').val() != null) {
      //   filtro_situacao = $('#filtro_situacao').val();
      // }

      var vendedor = '';
      // if ($('#vendedor').val() != null) {
      //   vendedor = $('#vendedor').val();
      // }

      var empresa = '';
      if ($('#empresa').val() != null) {
        empresa = $('#empresa').val();
      }

      var cliente = '';
      // if ($('#filtro_cliente').val() != null) {
      //   cliente = $('#filtro_cliente').val();
      // }

      var pedido = '';
      // if ($('#pedido').val() != null) {
      //   pedido = $('#pedido').val();
      // }

      var somente_pedidos_validos = '';
      // if ($('#somente_pedidos_validos').prop('checked')) {
      //   somente_pedidos_validos = '1';
      // }

      alvo = $(this).attr('id') == 'btnDownloadZipRelatorioDocumentosFiscais' ? 'Relatorio/downloadDocumentosFiscais/' : 'Relatorio/documentosFiscaisExcel/';

      var url = "<?php echo URL; ?>" + alvo + '0/buscar=' + $('#periodo_inicial').val() + "|" + $('#periodo_final').val() + "|" + filtro_situacao + "|" + '' + '|' + $('#tipo_data').val() + '|' + vendedor + '|' + empresa + '|' + cliente + '|' + pedido + '|' + somente_pedidos_validos;
      window.open(url);
    });


    function filtroProducao() {
      var empresa = '';
      if ($('#filtro-empresa-producao').val() != null) {
        empresa = $('#filtro-empresa-producao').val();
      }

      var vendedor = '';
      if ($('#filtro-vendedor-producao').val() != null) {
        vendedor = $('#filtro-vendedor-producao').val();
      }

      var data_inicio = '';
      if ($('#filtro-data_inicio-producao').val() != null) {
        data_inicio = $('#filtro-data_inicio-producao').val();
      }

      var data_final = '';
      if ($('#filtro-data_final-producao').val() != null) {
        data_final = $('#filtro-data_final-producao').val();
      }

      var url = "<?php echo URL . $classe . '/listar/'; ?>?empresa=" + empresa + "&vendedor=" + vendedor + "&data_inicio=" + data_inicio + "&data_final=" + data_final;
      window.location = url;
    }

    $("#filtroProducao").click(function() {
      filtroProducao()
    })

    $('#btnRelatorioComissao').on('click', function() {
      var filtro_situacao = '';
      if ($('#filtro_situacao').val() != null) {
        filtro_situacao = $('#filtro_situacao').val();
      }
      var url = "<?php echo URL . $classe . '/comissao/'; ?>0/buscar=" + $('#periodo_inicial').val() + "|" + $('#periodo_final').val() + "|" + filtro_situacao + "|0|" + $('#tipo_data').val() + '|' + $('#vendedor').val();
      window.location = url;
    });

    $('#exportarRelatorioComissaoPdf').on('click', function() {
      var filtro_situacao = '';
      if ($('#filtro_situacao').val() != null) {
        filtro_situacao = $('#filtro_situacao').val();
      }
      var url = "<?php echo URL . $classe . '/comissaoPdf/'; ?>0/buscar=" + $('#periodo_inicial').val() + "|" + $('#periodo_final').val() + "|" + filtro_situacao + "|0|" + $('#tipo_data').val() + '|' + $('#vendedor').val();
      window.open(url, '_blank');
    });

    $('#imprimirRelatorioComissao').on('click', function() {
      var filtro_situacao = '';
      if ($('#filtro_situacao').val() != null) {
        filtro_situacao = $('#filtro_situacao').val();
      }
      var url = "<?php echo URL . $classe . '/comissaoHtml/'; ?>0/buscar=" + $('#periodo_inicial').val() + "|" + $('#periodo_final').val() + "|" + filtro_situacao + "|0|" + $('#tipo_data').val() + '|' + $('#vendedor').val();
      window.open(url, '_blank');
    });

    $('#btnBuscarPedidosFabrica').on('click', function() {

      var filtro_empresa = $('#empresa').val() || '0';

      var filtro_tipo_data = $('#tipo_data').val();

      var url = "<?php echo URL . $classe . '/impressaoFabrica/'; ?>0/" + $('#periodo_inicial').val() + "|" + $('#periodo_final').val() + '|' + filtro_empresa + '|' + filtro_tipo_data;
      window.location = url;
    });

    $('#imprimirPedidosFabrica').on('click', function() {

      var filtro_empresa = $('#empresa').val() || '0';

      var filtro_tipo_data = $('#tipo_data').val();

      var url = "<?php echo URL . $classe . '/impressaoFabricaHtml/'; ?>0/" + $('#periodo_inicial').val() + "|" + $('#periodo_final').val() + '|' + filtro_empresa + '|' + filtro_tipo_data;
      window.open(url);
    });


    $('#frmBuscar').on('submit', function() {
      var url = "<?php echo URL . $classe . '/listar/'; ?>0/buscar=" + $('select[name=coluna]').val() + "|" + $('input[name=buscar]').val();
      window.location = url;
      return false;
    });

    $('#itensPagina').on('change', function() {
      var url = "<?php echo URL . $classe . '/listar/'; ?>/0/paginacao=page|" + $('select[name=itensPagina]').val();
      window.location = url;
    });

    var i_moldura = 0;
    $('#btnIncluirMoldura').click(async function() {
      var valMoldura = $('select.moldura').val() || $('input.moldura').val();
      if (valMoldura > 0) {

        var txtMoldura = $('select.moldura  option:selected').text();
        var wrapper = $(".input_molduras");
        if (boolMolduraria) {
          const conteudo_complexidades = await consultaComplexidadesMoldura(valMoldura)
          var linhaProdutoHtml = `
              <tr id="moldura${i_moldura}">
                <td>
                  ${txtMoldura}
                  <input
                    type="hidden"
                    class="form-control"
                    name="molduras"
                    value="${valMoldura}"
                    text=${txtMoldura}"
                  >
                </td>
                <td>
                  <select
                    name="complexidade"
                    data-moldura="${i_moldura}_${valMoldura}"
                  >
                    <option value=""></option>
                    ${conteudo_complexidades}
                  </select>
                </td>
                <td>
                  <select
                    name="reforco"
                    data-reforco="${i_moldura}_${valMoldura}"
                  >
                    <option value="N">Não</option>
                    <option value="H">Horizontal</option>
                    <option value="V">Vertical</option>
                  </select>
                </td>                
                <td>
                  ${controlesOrdemProducao}
                </td>
                <td>
                  <span class="input-group-btn">
                    <button
                      class="btn btn-default remove_field"
                      name="moldura${i_moldura}"
                      type="button"
                    >
                      Remover
                    </button>
                  </span>
                </td>
              </tr>"
          `

          $(wrapper).append(linhaProdutoHtml);
          i_moldura++;
        } else {
          if (modelo_empresa != 'venda-avulsa') {
            $(wrapper).html("<tr id=\"moldura" + i_moldura + "\"><td>" + txtMoldura + "<input type=\"hidden\" class=\"form-control\" name=\"molduras\" value=\"" + valMoldura + "\" text=\"" + txtMoldura + "\"></td><td>" + controlesOrdemProducao + "</td><td><span class=\"input-group-btn\"><button class=\"btn btn-default remove_field\" name=\"moldura" + i_moldura + "\" type=\"button\">Remover</button></span></td></tr>");
          } else {
            $(wrapper).html("<tr id=\"moldura" + i_moldura + "\"><td>" + txtMoldura + "<input type=\"hidden\" class=\"form-control\" name=\"molduras\" value=\"" + valMoldura + "\" text=\"" + txtMoldura + "\"></td><td>" + controlesOrdemProducao + "</td><td><span class=\"input-group-btn\"><button class=\"btn btn-default remove_field\" name=\"moldura" + i_moldura + "\" type=\"button\">Remover</button></span></td></tr>");
          }
        }

        if (!boolMolduraria) {
          $('#btnIncluirQuadro').trigger('click')
        }

        if (modelo_empresa != 'venda-avulsa') {
          $('.moldura, .componente').val(null).trigger('change')
        } else {
          $('#descricao_produto_servico').val('')
          document.getElementById("descricao_produto_servico").focus();
          $('#Ds_Observacao').val('')
          $('#Ds_ObservacaoProducao').val('')
          $('#Qt_Item').val(1)
          $('#Vl_Avulso').val('');
          $('.moldura, .componente').val(1)
        }

      }

    });

    $('.input_molduras').on("click", ".remove_field", function(e) {
      if (!confirm("Deseja realmente remover essa moldura do orçamento?")) {
        return
      }
      var tr = $(this).closest('tr');
      tr.css("background-color", "#FF3700");
      tr.fadeOut(400, function() {
        tr.remove();
      });
      i_moldura--;
      return false;
    });

    /* INCLUSÃO/REMOÇÃO COMPONENTES */
    var i_componente = 0;
    $('#btnIncluirComponente').click(function() {
      var valComponente = $('select.componente').val();
      if (valComponente > 0) {
        var txtComponente = $('select.componente  option:selected').text();
        var siglaUnidadeMedida = $('select.componente  option:selected').data('sigla-unidade-medida');
        var quantidadeEstoque = $('select.componente  option:selected').data('quantidade-estoque');
        var siglaUnidadeMedida = $('select.componente  option:selected').data('sigla-unidade-medida')
        var wrapper = $(".input_componentes");
        var optionQuantidadeEstoque = '';
        for (qtdEstoque = 1; qtdEstoque < quantidadeEstoque; qtdEstoque++) {
          optionQuantidadeEstoque = optionQuantidadeEstoque + `<option value="${qtdEstoque}">${qtdEstoque}</option>`
        }
        selecaoQuantidade = siglaUnidadeMedida == 'UN' ?
          `
          <select data-componente="${valComponente}"  name="quantidade-componentes">${optionQuantidadeEstoque}</select>
        ` :
          `
          <i> - </i>
        `

        $(wrapper).append("<tr id=\"componente" + i_componente + "\"><td>" + txtComponente + "<input type=\"hidden\" class=\"form-control\" name=\"componentes\" value=\"" + valComponente + "\" text=\"" + txtComponente + "\"></td><td>" + selecaoQuantidade + "</td><td><span class=\"input-group-btn\"><button class=\"btn btn-default remove_field\" name=\"moldura" + i_componente + "\" type=\"button\">Remover</button></span></td></tr>");
        i_componente++;
      }
    });

    $('.input_componentes').on("click", ".remove_field", function(e) {
      if (!confirm("Deseja realmente remover essa componente do orçamento?")) {
        return
      }
      var tr = $(this).closest('tr');
      tr.css("background-color", "#FF3700");
      tr.fadeOut(400, function() {
        tr.remove();
      });
      i_componente--;
      return false;
    });



    var i = <?php echo isset($aux) ? (int) $aux : 0; ?>;
    var item_orcamento = 0;
    var edicao = false;
    $('#btnIncluirQuadro').click(function() {
      var vars = {};
      var molduras = [];
      var componentes = [];
      var arrComponentes = [];
      var arrMolduras = [];
      $('input.form-control').each(function(index, obj) {
        var valueToPush = {};
        var valor = $(this).val();

        if (valor != '') {
          if ($(this).attr('name') == 'molduras') {
            const valueMoldura = $(this).val()

            // debugger
            var linhaAtual = $(this).closest('tr').attr('id')
            const itemAtual = boolMolduraria ? $(`#${linhaAtual} > td > select[name="reforco"]`).data('reforco').replace(/_.*/g, '') : 0;

            const complexidade = $(`*[data-moldura=${itemAtual}_${valueMoldura}]  option:selected`).val() || 0
            const complexidadeText = $(`*[data-moldura=${itemAtual}_${valueMoldura}]  option:selected`).attr('descricao') || ''

            const sarrafoReforco = $(`#${linhaAtual} > td > select[name="reforco"]`).val() || 'N' // $(`*[data-reforco=${itemAtual}_${valueMoldura}]  option:selected`).val() || 'N'
            const sarrafoReforcoText = $(`#${linhaAtual} > td > select[name="reforco"] option:selected`).text() || '' // $(`*[data-reforco=${itemAtual}_${valueMoldura}]  option:selected`).text() || ''

            console.log({linhaAtual, itemAtual, sarrafoReforco, sarrafoReforcoText})

            molduras.push({
              valor: $(this).val(),
              complexidade,
              quantidade: 0,
              sarrafoReforco
            });
            valueToPush["name"] = $(this).val();
            valueToPush["text"] = $(this).attr("text");
            valueToPush['complexidade'] = complexidade;
            valueToPush['complexidadeText'] = complexidadeText;

            valueToPush['sarrafoReforco'] = `${itemAtual}_${sarrafoReforco}`;
            valueToPush['sarrafoValue'] = `${sarrafoReforco}`;
            valueToPush['sarrafoReforcoText'] = sarrafoReforcoText;

            arrMolduras.push(valueToPush);
          } else if ($(this).attr('name') == 'componentes') {
            const valueComponente = $(this).val()
            const quantidadeValor = $(`*[data-componente=${valueComponente}]  option:selected`).val() || 0
            const quantidadeText = $(`*[data-componente=${valueComponente}]  option:selected`).attr('descricao') || ''

            componentes.push({
              'valor': valor,
              'complexidade': 0,
              'quantidade': quantidadeValor
            });
            valueToPush["name"] = $(this).val();
            valueToPush["text"] = $(this).attr("text");
            valueToPush["quantidade"] = quantidadeValor;
            valueToPush["quantidadeText"] = quantidadeText;
            arrComponentes.push(valueToPush);
            //} //else if ($(this).attr('name') == 'VL_ADICIONAIS') {
            //vars[$(this).attr('name')] = $(this).val();
          } else {
            vars[$(this).attr('name')] = $(this).val();
          }
        }
      });

      $('select.form-control').each(function(index, obj) {
        var valor = $(this).val();
        if (valor != '') {
          vars[$(this).attr('name')] = $(this).val();
        }
        if ($(this).attr('name') === 'Cd_Prod_Aux_Pedido') {
          vars['imagem_text'] = $(this).find(":selected").text()
        }
      });

      $('textarea.form-control').each(function(index, obj) {
        var valor = $(this).val();
        if (valor != '') {
          vars[$(this).attr('name')] = $(this).val();
        }
      });

      $('hidden.form-control').each(function(index, obj) {
        var valor = $(this).val();
        if (valor != '') {
          vars[$(this).attr('name')] = $(this).val();
        }
      });


      vars['molduras'] = molduras;
      vars['componentes'] = componentes;

      var Cd_Prod_Aux = vars['Cd_Prod_Aux_Pedido'];
      var ImagemDesc = vars['imagem_text'];
      var Qt_Item = vars['Qt_Item'];
      var Md_Largura = vars['Md_Largura'];
      var Md_Altura = vars['Md_Altura'];
      var Vl_Unitario = vars['Vl_Unitario'];
      var Vl_Bruto = vars['Vl_Bruto'];
      var VL_ADICIONAIS = vars['VL_ADICIONAIS'];
      var Vl_Moldura = vars['Vl_Moldura'];
      var Ds_Observacao = vars['Ds_Observacao'];
      var Ds_ObservacaoProducao = vars['Ds_ObservacaoProducao'];
      var descricao_produto_servico = vars['descricao_produto_servico'];

      var error_msg = "";
      if (Cd_Prod_Aux == "" || Cd_Prod_Aux == undefined) {
        error_msg += "* Informe a imagem;\n";
      }

      if (Qt_Item == 0) {
        error_msg += "* Informe a quantidade;\n";
      }

      if (Md_Largura == "") {
        error_msg += "* Informe a largura;\n";
      }


      if (Md_Altura == "") {
        error_msg += "* Informe a altura;\n";
      }

      if (error_msg != "") {
        error_msg = "Por favor: \n" + error_msg;
        alert(error_msg);
        return false;
      }

      if (Ds_Observacao == undefined) {
        Ds_Observacao = "";
      }

      if (Ds_ObservacaoProducao == undefined) {
        Ds_ObservacaoProducao = "";
      }

      var i_temporario = i;
      if (item_orcamento > 0) {
        i = item_orcamento;
      }
      if (item_orcamento == 0 && edicao == true) {
        i = 0;
      }


      var Vl_Unitario_Item = 0;

      var strValorAvulso = '';
      if (modelo_empresa == 'venda-avulsa') {
        strValorAvulso = '&Vl_Avulso=' + $("#Vl_Avulso").val()
      }

      $.ajax({
        url: '<?php echo URL; ?>Orcamento/calculaTotal/',
        type: 'post',
        data: 'Cd_Prod_Aux=' + Cd_Prod_Aux + '&Qt_Item=' + Qt_Item + '&Md_Largura=' + Md_Largura + '&Md_Altura=' + Md_Altura + '&Vl_Adicionais=' + VL_ADICIONAIS + '&Vl_Moldura=' + Vl_Moldura + '&molduras=' + JSON.stringify(molduras) + '&componentes=' + JSON.stringify(componentes) + '&handleCliente=' + $("#Cd_Cliente").val() + strValorAvulso,
        dataType: 'json',
        success: function(json) {
          // 				var valor_total = $('#Vl_Bruto').val();

          // 				valor_total = parseFloat(valor_total);
          Vl_Unitario_Item = json.total_unitario;
          Vl_Unitario_Item = Vl_Unitario_Item.toFixed(2);

          Vl_Total = json.total_bruto;
          Vl_Total = Vl_Total.toFixed(2);

          medida_final = exibir_medida_final_do_quadro && json.calculo.tem_produto_com_medida ? `<strong>MEDIDA FINAL: ${json.calculo.medida_final} </strong>` : ``
          valor_total_custo = json.valor_total_custo

          exibirPrecoCusto = exibir_total_custo_item ? `Preço custo: R$ ${valor_total_custo}` : "";

          conteudo_molduras = "";
          conteudo_molduras_item = "";

          for (a = 0; a < arrMolduras.length; a++) {
            var temReforco = arrMolduras[a]["sarrafoReforco"] != 'N' ? 'Reforço: ' + arrMolduras[a]["sarrafoReforcoText"] : '';

            indexProduto = json.itens.findIndex((item) => {
              return item.item == arrMolduras[a]["name"] && item.componente == '0' && item.item_posicao == a
            })

            exibeValorIndividual = '';
            valorProduto = indexProduto != -1 ? json.itens[indexProduto].valor_total : 0
            if (exibir_valores_individuais) {
              valorProduto_com_adicional = json.itens[indexProduto].valor_total_com_adicional

              valorProdutoFormatado = new Intl.NumberFormat('pt-BR', {
                style: 'currency',
                currency: 'BRL'
              }).format(
                valorProduto_com_adicional
              )

              exibeValorIndividual = ` - <span id="preco_produto_${arrMolduras[a]["name"]}_0_${a}">${valorProdutoFormatado}</span>`
            }

            conteudo_molduras += "<li>" + arrMolduras[a]["text"] + '(' + arrMolduras[a]["complexidadeText"] + temReforco + ')' + exibeValorIndividual;
            conteudo_molduras += "<input sarrafoReforco=\"" + arrMolduras[a]["sarrafoReforco"] + "\" complexidade=\"" + arrMolduras[a]["complexidade"] + "\" text=\"" + arrMolduras[a]["text"] + "\" id=\"moldura-" + arrMolduras[a]["name"] + "\" class=\"molduras-" + i + "\" name=\"Cd_Produto[" + i + "][]\" type=\"hidden\" value=\"" + arrMolduras[a]["name"] + "|" + arrMolduras[a]["complexidade"] + "|" + arrMolduras[a]["sarrafoValue"] + "|" + valorProduto + "\">";
            conteudo_molduras += "</li>";
          }
          if (conteudo_molduras != "") {
            conteudo_molduras = "<ul>" + conteudo_molduras + "</ul>";
          }

          conteudo_componentes = "";
          conteudo_componentes_item = "";
          valorProduto = 0;
          for (a = 0; a < arrComponentes.length; a++) {

            indexProduto = json.itens.findIndex((item) => {
              return item.item == arrComponentes[a]["name"] && item.componente == '1' && item.item_posicao == a
            })

            exibeValorIndividual = '';
            valorProduto = indexProduto != -1 ? json.itens[indexProduto].valor_total : 0
            if (exibir_valores_individuais) {
              valorProduto_com_adicional = json.itens[indexProduto].valor_total_com_adicional

              valorProdutoFormatado = new Intl.NumberFormat('pt-BR', {
                style: 'currency',
                currency: 'BRL'
              }).format(
                valorProduto_com_adicional
              )

              exibeValorIndividual = ` - <span id="preco_produto_${arrComponentes[a]["name"]}_1_${a}">${valorProdutoFormatado}</span>`
            }

            conteudo_componentes += "<li>" + arrComponentes[a]["text"] + exibeValorIndividual;
            conteudo_componentes += "<input quantidade=\"" + arrComponentes[a]['quantidade'] + "\"text=\"" + arrComponentes[a]["text"] + "\" id=\"componente-" + arrComponentes[a]["name"] + "\" class=\"componentes-" + i + "\" name=\"CD_Componente[" + i + "][]\" type=\"hidden\" value=\"" + arrComponentes[a]["name"] + "|" + arrComponentes[a]["quantidade"] + "|" + valorProduto + "\">";
            conteudo_componentes += "</li>";
          }
          if (conteudo_componentes != "") {
            conteudo_componentes = "<ul>" + conteudo_componentes + "</ul>";
          }

          var conteudo_imagem = '';
          var conteudo_editar = '<a class="carrega_item_orcamento" value="' + i + '"><i class="fa fa-edit"></i>Editar</a>';
          var conteudo_excluir = '<a class="delete_item_orcamento" value="' + i + '"><i class="fa fa-trash" aria-hidden="true"></i>Excluir</a>';
          if (boolMolduraria) {
            conteudo_imagem += "<td><input id=\"Cd_Prod_Aux-" + i + "\" type=\"hidden\" name=\"Cd_Prod_Aux[" + i + "]\" value=\"" + Cd_Prod_Aux + "\">" + ImagemDesc + "</td>";
          } else {
            if (modelo_empresa != 'venda-avulsa') {
              conteudo_imagem += "<td id=\"molduras-imagem" + i + "\">" + conteudo_molduras + "</td>" + "<td class=\"" + (!boolMolduraria ? 'hide' : '') + "\">Componentes:" + conteudo_componentes + "</td>";
              conteudo_imagem += "<input id=\"Cd_Prod_Aux-" + i + "\" type=\"hidden\" name=\"Cd_Prod_Aux[" + i + "]\" value=\"" + Cd_Prod_Aux + "\">";
            } else {
              conteudo_imagem += "<td id=\"molduras-imagem" + i + "\">" + descricao_produto_servico + "</td>" + "<td class=\"" + (!boolMolduraria ? 'hide' : '') + "\">Componentes:" + conteudo_componentes + "</td>";
              conteudo_imagem += "<input id=\"Cd_Prod_Aux-" + i + "\" type=\"hidden\" name=\"Cd_Prod_Aux[" + i + "]\" value=\"" + Cd_Prod_Aux + "\">";
              conteudo_imagem += "<input id=\"descricao_produto_servico-" + i + "\" type=\"hidden\" name=\"descricao_produto_servico[" + i + "]\" value=\"" + descricao_produto_servico + "\">";
            }
          }
          conteudo_imagem += "<td><input id=\"Qt_Item-" + i + "\" type=\"hidden\" name=\"Qt_Item[" + i + "]\" value=\"" + Qt_Item + "\">" + Qt_Item + "</td>";

          if (modelo_empresa != 'venda-avulsa') {
            conteudo_imagem += "<td><input id=\"Md_Largura-" + i + "\" type=\"hidden\" name=\"Md_Largura[" + i + "]\" value=\"" + Md_Largura + "\">" + Md_Largura + "</td>";
            conteudo_imagem += "<td><input id=\"Md_Altura-" + i + "\" type=\"hidden\" name=\"Md_Altura[" + i + "]\" value=\"" + Md_Altura + "\">" + Md_Altura + "</td>";
            conteudo_imagem += "<input id=\"medida_final-" + i + "\" type=\"hidden\" name=\"medida_final[" + i + "]\" value=\"" + json.calculo.medida_final + "\">";
          }
          if (modelo_empresa != 'venda-avulsa') {
            conteudo_imagem += "<td><input id=\"VL_ADICIONAIS-" + i + "\" type=\"text\" name=\"VL_ADICIONAIS[" + i + "]\" class=\"campoMonetario recalcula\" value=\"" + VL_ADICIONAIS + "\">&nbsp;&nbsp;&nbsp;<i class=\"fa fa-refresh\" aria-hidden=\"true\"></i></td>";
            conteudo_imagem += "<td><input id=\"Vl_Moldura-" + i + "\" type=\"text\" name=\"Vl_Moldura[" + i + "]\" class=\"campoMonetario recalcula\" value=\"" + Vl_Moldura + "\">&nbsp;&nbsp;&nbsp;<i class=\"fa fa-refresh\" aria-hidden=\"true\"></i></td>";
            conteudo_imagem += "<td>";
            conteudo_imagem += "<input id=\"Vl_Unitario-" + i + "\" type=\"hidden\" name=\"Vl_Unitario[" + i + "]\" value=\"" + Vl_Unitario_Item + "\"><span id=\"Vl_Unitario-Text-" + i + "\">R$ " + Vl_Unitario_Item + "</span>";
            conteudo_imagem += "<input id=\"valor_custo-" + i + "\" type=\"hidden\" name=\"valor_custo[" + i + "]\" value=\"" + valor_total_custo + "\"><br><span class=\"small\" id=\"valor_total_custo-Text-" + i + "\">" + exibirPrecoCusto + "</span>";
            conteudo_imagem += "</td>"
            conteudo_imagem += "<td><input id=\"Vl_Total-" + i + "\" type=\"hidden\" name=\"Vl_Total[" + i + "]\" value=\"" + Vl_Total + "\"><span id=\"Vl_Total-Text-" + i + "\">R$ " + Vl_Total + "</span></td>";
          }
          conteudo_imagem += "<td><input id=\"Ds_Observacao-" + i + "\" type=\"hidden\" name=\"Ds_Observacao[" + i + "]\" value=\"" + Ds_Observacao + "\">" + Ds_Observacao + "</td>"
          conteudo_imagem += "<td><input id=\"Ds_ObservacaoProducao-" + i + "\" type=\"hidden\" name=\"Ds_ObservacaoProducao[" + i + "]\" value=\"" + Ds_ObservacaoProducao + "\">" + Ds_ObservacaoProducao + "</td>"
          conteudo_imagem += "<td>" + conteudo_editar + "</td><td>" + conteudo_excluir + conteudo_molduras_item + conteudo_componentes_item + "&nbsp;</td>";
          $('#imagem' + i).html(conteudo_imagem);
          //i++;
          if (boolMolduraria) {
            $('#molduras-imagem' + i).append('<tr id="molduras-imagem' + i + '"></tr>');
            $('#molduras-imagem' + i).html("<td colspan=\"3\">Produto:" + conteudo_molduras + '<br>' + medida_final + "</td>" + "<td colspan=\"4\" class=\"" + (!boolMolduraria ? 'hide' : '') + "\">Componentes:" + conteudo_componentes + "</td>");
          }

          if (item_orcamento == 0 && edicao == false) {
            i++;
            $('#tabelaImagem').append('<tr id="imagem' + i + '"></tr>');
            $('#tabelaImagem').append('<tr id="molduras-imagem' + i + '"></tr>');
          } else {
            i = i_temporario;
          }

          if (boolMolduraria) {
            $(".adicionar-quadro").slideUp("slow");
          }

          item_orcamento = 0;

          recalculaValor();

          edicao = false;

        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
      // i++;
    });

    $('body').on('click', '.delete_item_orcamento', function(e) {
      e.preventDefault();
      var confirma = confirm('Deseja realmente excluir esse item?');
      if (confirma == false) {
        return false;
      }
      item_orcamento = $(this).attr('value');

      $('#imagem' + item_orcamento).remove();
      $('#molduras-imagem' + item_orcamento).remove();

      recalculaValor();

      calculaDesconto();
      calculaEntrada();

    });


    $('.input_molduras').on("click", ".moveup", function(e) {
      var elem = $(this).closest("tr");
      elem.prev().before(elem);
      return false;
    });

    $('.input_molduras').on("click", ".movedown", function(e) {
      var elem = $(this).closest("tr");
      elem.next().after(elem);
      return false;
    });

    const controlesOrdemProducao = `
        <button
          class="moveup btn btn-default"
        >
          <i
            class="fa fa-arrow-up"
            aria-hidden="true">
          </i>
        </button>
        <button
          class="movedown btn btn-default"
        >
          <i
            class="fa fa-arrow-down"
            aria-hidden="true">
          </i>
        </button>
      `
    $('body').on('click', '.carrega_item_orcamento', function() {
      if (boolMolduraria) {
        $(".adicionar-quadro").slideDown("slow");
      }
      item_orcamento = $(this).attr('value');
      edicao = true;
      var cod_orcamento = $('#Cd_Orcamento').val();
      var wrapper = $(".input_molduras");
      var wrapper_componentes = $(".input_componentes");
      $(wrapper).empty();
      $(wrapper_componentes).empty();

      $('#Cd_Prod_Aux_Pedido').val($('#Cd_Prod_Aux-' + item_orcamento).val());
      $('#Qt_Item').val($('#Qt_Item-' + item_orcamento).val());
      $('#Md_Altura').val($('#Md_Altura-' + item_orcamento).val());
      $('#Md_Largura').val($('#Md_Largura-' + item_orcamento).val());
      $('#Ds_Observacao').val($('#Ds_Observacao-' + item_orcamento).val());
      $('#Ds_ObservacaoProducao').val($('#Ds_ObservacaoProducao-' + item_orcamento).val());
      $('#VL_ADICIONAIS').val($('#VL_ADICIONAIS-' + item_orcamento).val());
      $('#Vl_Moldura').val($('#Vl_Moldura-' + item_orcamento).val());
      $('#descricao_produto_servico').val($('#descricao_produto_servico-' + item_orcamento).val());
      if (modelo_empresa != 'venda-avulsa') {
        $('#Vl_Avulso').val($('#Vl_Unitario-' + item_orcamento).val().replaceAll('.', ','));
      }

      $.each($('.molduras-' + item_orcamento), async function(index, obj) {
        arrValuesMoldura = obj.value.split("|")
        objValue = arrValuesMoldura[0]
        var valMoldura = objValue;

        const itemAtual = $(obj).attr('sarraforeforco').replaceAll('_V', '').replaceAll('_H', '');

        var txtMoldura = $('#moldura-' + objValue).attr('text');
        var complexidade = $('#moldura-' + objValue).attr('complexidade') || 0;
        var sarraforeforco = arrValuesMoldura[2] || 'N';
        const conteudo_complexidades = '' // await consultaComplexidadesMoldura(valMoldura, complexidade)

        selecaoSarrafoReforco = `
              <select name="reforco" data-reforco="${itemAtual}_${valMoldura}">
                <option ${sarraforeforco == 'N' ? 'selected' : ''} value="N">Não</option>
                <option ${sarraforeforco == 'H' ? 'selected' : ''} value="H">Horizontal</option>
                <option ${sarraforeforco == 'V' ? 'selected' : ''} value="V">Vertical</option>
            </select>
        `

        $(wrapper).append("<tr id=\"moldura" + i_moldura + "\"><td>" + txtMoldura + "<input type=\"hidden\" class=\"form-control\" name=\"molduras\" value=\"" + valMoldura + "\" text=\"" + txtMoldura + "\"></td><td><select name=\"complexidade\" data-moldura=\"" + valMoldura + "\"><option value=\"\"></option>" + conteudo_complexidades + "</select></td><td>" + selecaoSarrafoReforco + "</td><td>" + controlesOrdemProducao + "</td><td><span class=\"input-group-btn\"><button class=\"btn btn-default remove_field\" name=\"moldura" + i_moldura + "\" type=\"button\">Remover</button></span></td></tr>");
        i_moldura++;

        if (!boolMolduraria) {
          $('.moldura').val(valMoldura).trigger('change')
        }
      });

      $.each($('.componentes-' + item_orcamento), function(index, obj) {
        arrValuesComponente = obj.value.split("|")
        objValue = arrValuesComponente[0];
        var valComponente = objValue;
        var txtComponente = $('#componente-' + objValue).attr('text');
        var optionQuantidadeEstoque = '';
        var quantidade = $('#componente-' + objValue).attr('quantidade') || 0;
        var quantidadeEstoque = $(`select#selecionaComponentes [value="${valComponente}"]`).data('quantidade-estoque')
        var siglaUnidadeMedida = $(`select#selecionaComponentes [value="${valComponente}"]`).data('sigla-unidade-medida')
        for (qtdEstoque = 1; qtdEstoque < quantidadeEstoque; qtdEstoque++) {
          var selected = '';
          if (quantidade == qtdEstoque) {
            selected = 'selected';
          }
          optionQuantidadeEstoque = optionQuantidadeEstoque + `<option value="${qtdEstoque}" ${selected}>${qtdEstoque}</option>`
        }

        selecaoQuantidade = siglaUnidadeMedida == 'UN' ?
          `
          <select data-componente="${valComponente}" name="quantidade-componentes">${optionQuantidadeEstoque}</select>
        ` :
          `
          <i> -</i>
        `

        $(wrapper_componentes).append("<tr id=\"componente" + i_componente + "\"><td>" + txtComponente + "<input type=\"hidden\" class=\"form-control\" name=\"componentes\" value=\"" + valComponente + "\" text=\"" + txtComponente + "\"></td><td>" + selecaoQuantidade + "</td><td><span class=\"input-group-btn\"><button class=\"btn btn-default remove_field\" name=\"moldura" + i_componente + "\" type=\"button\">Remover</button></span></td></tr>");
        i_componente++;
      });
    });

    $('#btnExibeAdicionarQuadro').click(function() {
      if (boolMolduraria) {
        $(".adicionar-quadro").slideDown("slow");
      }
      $('#Qt_Item').val(0);
      $('#Md_Altura').val(0);
      $('#Md_Largura').val(0);
      $('#Ds_Observacao').val('');
      $('#Ds_ObservacaoProducao').val('');
      $('#VL_ADICIONAIS').val(0);
      $('#Vl_Moldura').val(0);

      var wrapper = $(".input_molduras");
      $(wrapper).empty();

      var wrapper = $(".input_componentes");
      $(wrapper).empty();

      item_orcamento = 0;

    })

    function dataAtualFormatada() {
      var data = new Date(),
        dia = data.getDate().toString(),
        diaF = (dia.length == 1) ? '0' + dia : dia,
        mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
        mesF = (mes.length == 1) ? '0' + mes : mes,
        anoF = data.getFullYear();
      return diaF + "/" + mesF + "/" + anoF;
    }

    $('#btnSalvarPedido').click(function() {
      recalculaValor();
      calculaDesconto();
      calculaEntrada();
      if ($('#Cd_Cliente').val().trim() === '') {
        alert('Informe o cliente');
        return
      }

      if ($('#Pago').prop('checked') && $('#data_pagamento').val() == '') {
        alert('Informe a data de pagamento');
        document.getElementById("data_pagamento").focus();
        return
      }

      const temValor = parseFloat($("#Vl_Entrada").val().replaceAll('.', '').replaceAll(',', '.')) || (parseFloat($("#totalSaldo").text()) > 0)
      if (habilita_financeiro == false || possuiFinanceiro || $('#Id_Situacao').val() <= 1 || ($('#Id_Situacao').val() >= 6) || temValor == false) {
        $('#btnSalvarPedido').attr('disabled', 'true')
        $('#formPedido').submit();
        return
      }

      $(".lancamento-financeiro-entrada").addClass('hide');
      if (parseFloat($("#Vl_Entrada").val().replaceAll('.', '').replaceAll(',', '.'))) {
        $(".lancamento-financeiro-entrada").removeClass('hide');
        $("#valor_entrada").val($("#Vl_Entrada").val())
      } else {
        $("#valor_entrada").val(0)
      }

      $(".lancamento-financeiro-saldo").addClass('hide');
      if (parseFloat($("#totalSaldo").text()) > 0) {
        $(".lancamento-financeiro-saldo").removeClass('hide');
        $("#valor_saldo").val($("#totalSaldo").text().replaceAll('.', ','))
      } else {
        $("#valor_saldo").val(0)
      }

      const estaPago = $('#Pago').is(":checked")
      if (estaPago) {
        $('#AccountId').val('15')
        $('#cdtpagamento').val(dataAtualFormatada)
      }
      $('#gerarFinanceiro').modal('show')
      return
    });

    $('.btnCancelarNota').click(function() {
      $('#notaCancelamento').val($(this).data('nota'));
      $('#notaTipoDocumento').val($(this).data('tipo-documento'));
      $('#modalCancelarNota').modal('show')
      return
    });

    $('.btnNotaDevolucao').click(function() {
      var formCancelamento = new FormData();
      formCancelamento.append('idNota', $(this).data('nota'))
      formCancelamento.append('idNota', $(this).data('nota'))
      formCancelamento.append('devolucao', $(this).data('true'))

      const tipoDocumento = $(this).data('tipo-documento');

      $.ajax({
        url: `<?php echo URL; ?>${tipoDocumento}/emitirDevolucao/${$(this).data('nota')}`,
        type: 'GET',
        processData: false,
        // data: `&idNota=`,
        // dataType: 'json',
        contentType: false,
        success: function(json) {
          $.ajax({
            url: '<?php echo URL; ?>' + tipoDocumento + '/processarFilaNotas/',
            type: 'get',
            dataType: 'json',
            success: function(json) {
              alert(json.msg);
              location.reload()
            }
          });
        }
      });
      return
    });

    $('#btnEnviarCancelamento').click(function() {

      if ($('#motivoCancelamento').val() == '') {
        alert('Informe o motivo do cancelamento')
        return
      }

      if ($('#motivoCancelamento').val().length < 15 || $('#motivoCancelamento').val().length > 65) {
        alert(`O motivo do cancelamento deve ter entre 15 e 65 caracteres\nQuantidade informada: ${$('#motivoCancelamento').val().length}`)
        return
      }

      var formCancelamento = new FormData();
      formCancelamento.append('motivo', $('#motivoCancelamento').val())
      formCancelamento.append('idNota', $('#notaCancelamento').val())

      const tipoDocumento = $('#notaTipoDocumento').val();

      $.ajax({
        url: `<?php echo URL; ?>${tipoDocumento}/cancelarNota/`,
        type: 'POST',
        processData: false,
        data: formCancelamento,
        dataType: 'json',
        contentType: false,
        success: function(json) {
          alert(json.msg)
          location.reload()
        }
      });
    });

    $('#btnGerarFinanceiroSimplificado').click(function() {
      var formFinanceiro = []
      var dadosLancamentoEntrada = [];
      var dadosLancamentoSaldo = [];
      $(
        "#frmEmitirFinanceiro input[type='text'], #frmEmitirFinanceiro input[type='hidden'], #frmEmitirFinanceiro select, #frmEmitirFinanceiro textarea"
      ).map(function(idx, elem) {
        const name = $(elem).attr('name')
        const value = $(elem).val()
        if ($(elem).hasClass('entrada')) {
          dadosLancamentoEntrada[$(elem).attr('name')] = $(elem).val()
        } else {
          dadosLancamentoSaldo[$(elem).attr('name')] = $(elem).val()
        }
      })

      formFinanceiro = {
        entrada: Object.assign({}, dadosLancamentoEntrada),
        saldo: Object.assign({}, dadosLancamentoSaldo)
      }

      $("<input />").attr("type", "hidden")
        .attr("name", 'financeiro')
        .attr("value", JSON.stringify(Object.assign({}, formFinanceiro)))
        .appendTo("#formPedido");

      $('#btnSalvarPedido').attr('disabled', 'true')
      $('#btnGerarFinanceiroSimplificado').attr('disabled', 'true')
      $('#formPedido').submit();
      return
    });


    const permiteExcluirClienteSelecionado = $("#filtro_cliente").length == 1 ? true : false;
    $("#Cd_Cliente, #Consumidor_Temp, #filtro_cliente").select2({
      ajax: {
        url: "<?php echo URL; ?>Cliente/carrega/",
        dataType: 'json',
        delay: 250,
        type: 'post',
        data: function(params) {
          return {
            q: params.term, // search term
            page: params.page
          };
        },
        processResults: function(data) {
          return {
            results: data.itens
          };
        }
      },
      placeholder: 'Informe o nome, CPF ou CNPJ',
      minimumInputLength: 3,
      language: "pt-BR",
      tags: true,
      allowClear: permiteExcluirClienteSelecionado
    });

    /**
     * Limpar o <select> quando for removido o cliente do filtro.
     */
    $('#filtro_cliente').on('select2:unselecting', function(e) {
      $('#filtro_cliente').find('option').remove()
    });

    $('#Title').on('change', function() {
      $('#ndoc').find('option').remove().end().append('<option value="">Carregando pedidos...</option>');
      $.ajax({
        url: '<?php echo URL; ?>Orcamento/consultaOrcamentosCliente/' + $('#Title').val(),
        type: 'get',
        dataType: 'json',
        success: function(json) {
          $('#ndoc').find('option').remove().end();
          $('#ndoc').append('<option data-valor="0" value="">Selecione um pedido</option>');
          $.each(json, function(key, value) {
            $('#ndoc').append('<option data-valor=' + value.valor + ' value=' + value.handle + '>' + value.handle + '</option>');
          })
        }
      });
    });

    $('#frmCadastroContaReceber').on('submit', function() {
      const situacao = $('#situacao').val()
      const conta = $("#AccountId").val()
      const data_pagamento = $("#cdtpagamento").val()

      if (situacao == 1 && conta == 19 || data_pagamento != '' && conta == 19) {
        alert('Informe para qual conta o recebimento foi enviado!')
        document.getElementById('AccountId').focus()
        return false
      }

      if (data_pagamento == '' && conta != 19 || data_pagamento == '' && situacao == 1) {
        alert('Informe a data de pagamento!')
        document.getElementById('cdtpagamento').focus()
        return false
      }

      if (data_pagamento != '' && situacao == 0) {
        $('#situacao').val(1)
      }

      return true
    });


    $('#ndoc').on('change', function() {
      const valor = (`${$('#ndoc option:selected').data('valor')}`).replace('.', ',')
      $('#Amount').val(valor)
    });

    $("#btnEnviarCadastroCliente").click(function() {
      if ($("#RazaoSocial").val().trim() == '') {
        if ($("#TipoCliente").val() == 'F') {
          alert("Por favor informe o nome!")
        } else {
          alert("Por favor informe a razão social!")
        }
        return
      }
      if ($("#Telefone1").val().trim() == '') {
        alert("Por favor informe um número de telefone!")
        return
      }

      $('#btnEnviarCadastroCliente').attr('disabled', 'true')

      let inputCliente =
        'TipoCliente=' + $("#TipoCliente").val() +
        '&idLogradouro=' + $("#idLogradouro").val() +
        '&Estado=' + $("#Estado").val() +
        '&Cidade=' + $("#Cidade").val() +
        '&codigoClienteGrupo=' + $("#codigoClienteGrupo").val() +
        '&RazaoSocial=' + $("#RazaoSocial").val() +
        '&NomeFantasia=' + $("#NomeFantasia").val() +
        '&CGC=' + $("#CGC").val() +
        '&InscricaoEstadual=' + $("#InscricaoEstadual").val() +
        '&EMail=' + $("#EMail").val() +
        '&Telefone1=' + $("#Telefone1").val() +
        '&Telefone2=' + $("#Telefone2").val() +
        '&CEP=' + $("#CEP").val() +
        '&Endereco=' + $("#Endereco").val() +
        '&numeroEndereco=' + $("#numeroEndereco").val() +
        '&Complemento=' + $("#Complemento").val() +
        '&Bairro=' + $("#Bairro").val();

      $.ajax({
        url: '<?php echo URL; ?>Orcamento/cadastrarCliente/',
        type: 'post',
        data: inputCliente,
        dataType: 'json',
        success: function(json) {
          if (json.success == "1") {
            $(".alertaCadastroCliente").css('display', 'block');
            $(".alertaCadastroCliente").html(json.message);

            const dadosNovoCliente = {
              id: json.handle,
              text: $("#RazaoSocial").val()
            };

            const novoClienteOpcao = new Option(dadosNovoCliente.text, dadosNovoCliente.id, true, true);
            $('#Cd_Cliente').append(novoClienteOpcao).trigger('change');
          }
        }
      });
    });

    $("#exportarProdutos").click(function() {
      window.open('<?php echo URL; ?>Produto/exportar/', '_blank');
    })

    $("#btnAlterarPreco").click(function() {
      $(".alertaAlteracaoPreco").css('display', 'none');
      $(".alertaAlteracaoPreco").removeClass('alert-success');
      $(".alertaAlteracaoPreco").removeClass('alert-warning');
      $.ajax({
        url: '<?php echo URL; ?>Produto/alterarPrecoEmMassa/',
        type: 'post',
        data: 'alteracao=' + $("#alteracao").val() + '&valor=' + $("#valor").val() + '&tipo=' + $("#tipo").val() + '&fornecedor=' + $("#fornecedor").val() + '&tipo_valor=' + $("#tipo_valor").val(),
        dataType: 'json',
        success: function(json) {
          if (json.success == "1") {
            $("#mensagemAlteracaoPreco").html(json.message);
            $(".alertaAlteracaoPreco").css('display', 'block');
            $(".alertaAlteracaoPreco").addClass('alert-success');

            setTimeout(
              function() {
                location.reload()
              },
              2000
            );
          }

          if (json.success == "0") {
            $("#mensagemAlteracaoPreco").html(json.message);
            $(".alertaAlteracaoPreco").css('display', 'block');
            $(".alertaAlteracaoPreco").addClass('alert-warning');
          }

        }
      });
    });


    $('#arquivoImportarProdutos').on('change', function() {
      const files = document.querySelector('[type=file]').files;
      const file = files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      formDataUploadProduto.append('files[0]', file);
    });

    $("#btnImportarProdutos").click(function() {

      if ($("#excluirArquivos").val() == 'S') {
        confirmarAcao = confirm('Atenção! Todos os produtos serão excluídos! Deseja realmente continuar?')
        if (!confirmarAcao) {
          return false
        }
      }

      formDataUploadProduto.append('excluirProdutos', $('#excluirArquivos').val())

      $('#btnImportarProdutos').attr('disabled', true)
      $('#btnImportarProdutos').text('Importando planilha...Aguarde')

      $.ajax({
        url: '<?php echo URL; ?>Produto/importar/',
        type: 'POST',
        mimeType: "multipart/form-data",
        data: formDataUploadProduto,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function(json) {
          const erros = json.erros.map(item => item).join('\n');
          alert(json.msg + '\n\n ' + erros)
          $('#btnImportarProdutos').attr('disabled', false)
          $('#btnImportarProdutos').text('Importar')
          $("#excluirArquivos").val('N')
          $('#listaProdutos').DataTable().ajax.reload()
        }
      });
    });


    $('#VL_desconto, #tipo_desconto').change(function() {
      calculaDesconto();
    });

    $('#tipo_desconto').change(function() {
      if ($(this).val() == 'P') {
        $('#labelValorDesconto').html('%')
        return
      }
      $('#labelValorDesconto').html('R$')
    });

    $('#Vl_Entrada').change(function() {
      calculaEntrada();
    });


    $('.numberOnly').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && event.which != 0 && event.which != 8) {
        event.preventDefault();
      }
    });

    <?php
    if (isset($orcamentos) && is_array($orcamentos) && sizeof($orcamentos)) {
    ?>
      $('.pedidoImprimir').click(function() {
        window.open('<?php echo URL . $classe . "/imprimir/" . $orcamentos[0]->Cd_Orcamento ?>/' + $(this).data('alvo'), '_blank');
        return false;
      });

      $('#Id_Situacao').select2()

      const url_whatsapp = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 'https://api.whatsapp.com/send?phone=' : 'https://web.whatsapp.com/send?phone='
      $('#btnPedidoEnviarWhatsapp').click(function() {
        $('#btnPedidoEnviarWhatsapp').attr('disabled', 'true')
        $.ajax({
          url: '<?php echo URL; ?>Whatsapp/enviarMensagem/<?php echo $orcamentos[0]->Cd_Orcamento; ?>',
          type: 'get',
          dataType: 'json',
          success: function(json) {
            if (json.success == 1) {
              if (!isIphone) {
                window.open(url_whatsapp + json.destinatario + '&text=' + json.msg, '_blank')
              } else {
                window.open('<?php echo URL; ?>Whatsapp/enviarMensagemIphone/<?php echo $orcamentos[0]->Cd_Orcamento; ?>')
              }
            } else {
              alert(json.msg)
            }
            $('#btnPedidoEnviarWhatsapp').removeAttr('disabled', '')
          }
        });
      });

      $('#btnSituacaoEnviarWhatsapp').click(function() {
        $('#btnSituacaoEnviarWhatsapp').attr('disabled', 'true')

        if (isIphone) {
          window.open('<?php echo URL; ?>Whatsapp/enviarSituacao/<?php echo $orcamentos[0]->Cd_Orcamento; ?>/' + '&iphone=' + isIphone)
        } else {
          $.ajax({
            url: '<?php echo URL; ?>Whatsapp/enviarSituacao/<?php echo $orcamentos[0]->Cd_Orcamento; ?>/' + '&iphone=' + isIphone,
            type: 'get',
            dataType: 'json',
            success: function(json) {
              if (json.success == 1) {
                window.open(url_whatsapp + json.destinatario + '&text=' + json.msg, '_blank')
              } else {
                alert(json.msg)
              }
            }
          });
        }
        $('#btnSituacaoEnviarWhatsapp').removeAttr('disabled', '')
      });

      $('#btnPedidoEnviarEmail').click(function() {
        $('#btnPedidoEnviarEmail').attr('disabled', 'true')
        $.ajax({
          url: '<?php echo URL; ?>Orcamento/enviarEmail/<?php echo $orcamentos[0]->Cd_Orcamento; ?>',
          type: 'get',
          dataType: 'json',
          success: function(json) {
            alert(json.message)
            $('#btnPedidoEnviarEmail').removeAttr('disabled', '')
          }
        });
      });


      var tipoDocumento = 'NF';
      $('.gerarDocumentoFiscal').click(function() {

        if ($(this).hasClass('downloadDocumentoFiscal')) {
          var urlDocumento = 'IntegraNota';
          var botaoAcao = 'btnGerarNFe';
          var labelBotaoAcao = 'Nota Fiscal'
          switch (tipoDocumento) {
            case 'NFS':
              urlDocumento = 'IntegraNotaServico';
              break;
            case 'NFC':
              urlDocumento = 'IntegraNotaConsumidor';
              break;
          }
          const urlConsultaNota = '<?php echo URL; ?>' + urlDocumento + '/downloadNota/' + $(this).data('id-documento')
          window.open(urlConsultaNota)
          return
        }


        const quantidadeEmpresa = $(this).data('quantidade-empresa');
        tipoDocumento = $(this).data('tipo-documento');
        $("#id_empresa_nfe").val($("#id_empresa").val())
        if (quantidadeEmpresa > 1) {
          $('#gerarNotaFiscal').modal('show')
          return
        }
        return emitirNfe(tipoDocumento)
      });

      /*
      $('#btnGerarNFe').click(function () {
        const quantidadeEmpresa = $('#btnGerarNFe').data('quantidade-empresa');
        if (quantidadeEmpresa > 1) {
          $('#gerarNotaFiscal').modal('show')
          return
        }
        return emitirNfe()
      });
      */

      $('#btnEmitirNfeModal').click(function() {
        $('#gerarNotaFiscal').modal('hide')
        return emitirNfe(tipoDocumento)
      })

      function emitirNfe(tipo_documento = '') {
        const idEmpresaSelecionada = $('#id_empresa_nfe').val()
        var interromperConsulta = false;
        var numeroTentativas = 0;
        var mensagem = '';
        var urlDocumento = 'IntegraNota';
        var botaoAcao = 'btnGerarNFe';
        var labelBotaoAcao = 'Nota Fiscal'
        switch (tipoDocumento) {
          case 'NFS':
            urlDocumento = 'IntegraNotaServico';
            botaoAcao = 'btnGerarNFSe';
            labelBotaoAcao = 'Nota Fiscal de serviço'
            break;
          case 'NFC':
            urlDocumento = 'IntegraNotaConsumidor';
            botaoAcao = 'btnGerarNFCe';
            labelBotaoAcao = 'Nota Fiscal do consumidor'
            break;
        }

        $(`#${botaoAcao}`).attr('disabled', 'true')
        $(`#${botaoAcao}`).text('Gerando o documento, aguarde...')



        $.ajax({
          url: '<?php echo URL; ?>' + urlDocumento + '/emitirDocumento/<?php echo $orcamentos[0]->Cd_Orcamento; ?>/IdEmpresa=' + idEmpresaSelecionada,
          type: 'get',
          dataType: 'json',
          success: function(json) {
            if (json.success == '1') {

              mensagem = json.msg

              if (json.codigo_retorno == '5008') {
                alert(mensagem)
                $(`#${botaoAcao}`).removeAttr('disabled', '')
                $(`#${botaoAcao}`).text(`Gerar ${labelBotaoAcao}`)
                return
              }

              $(`#${botaoAcao}`).removeAttr('disabled', '')
              $(`#${botaoAcao}`).text('Aguardando retorno do SEFAZ')
              var interv = setInterval(function() {
                const id_documento = json.id_documento
                const urlConsultaNota = '<?php echo URL; ?>' + urlDocumento + '/consultarNota/' + id_documento
                var status = 0;
                $.ajax({
                  url: urlConsultaNota,
                  type: 'get',
                  dataType: 'json',
                  success: function(json) {
                    mensagem = json.msg
                    codigo = json.codigo
                    status = json.status
                    console.log({
                      mensagem,
                      status,
                      codigo
                    })
                    //clearInterval(interv)
                    if (status !== 'PROCESSANDO') {
                      if (status === 'Autorizado') {
                        const urlDownloadDocumento = '<?php echo URL; ?>' + urlDocumento + '/downloadNota/' + id_documento
                        window.open(urlDownloadDocumento, '_blank')
                        interromperConsulta = true;
                        clearInterval(interv)
                        $(`#${botaoAcao}`).removeAttr('disabled', '')
                        $(`#${botaoAcao}`).text('Baixar PDF da Nota')
                        $(`#${botaoAcao}`).addClass('downloadDocumentoFiscal')
                        $(`#${botaoAcao}`).removeClass('gerarDocumentoFiscal')
                        $(`#${botaoAcao}`).attr('data-id-documento', id_documento)
                      }
                      if (status == 'REJEITADO' || status == 'ERRO') {
                        alert(`Erro ao processar o documento: ${mensagem}`)
                        interromperConsulta = true;
                        clearInterval(interv)
                        $(`#${botaoAcao}`).removeAttr('disabled', '')
                        $(`#${botaoAcao}`).text(`Gerar ${labelBotaoAcao}`)
                      }
                      return;
                    }
                    numeroTentativas++;
                    if (numeroTentativas >= 5) {
                      alert(`Ainda não houve retorno do SEFAZ.\n O status da nota é ${status} Você poderá consulta-la em Pedidos > Documentos Fiscais`)
                      clearInterval(interv)
                      $(`#${botaoAcao}`).removeAttr('disabled', '')
                      $(`#${botaoAcao}`).text(`Gerar ${labelBotaoAcao}`)
                    }
                  }
                })
              }, 2000);
            } else {
              strMsg = 'ERRO AO GERAR O DOCUMENTO\n'
              strMsg += json.msg
              alert(strMsg)
              $(`#${botaoAcao}`).removeAttr('disabled', '')
              $(`#${botaoAcao}`).text(`Gerar ${labelBotaoAcao}`)
            }
          }
        });
      }


      $('#btnPedidoImprimirOs').click(function() {
        window.open('<?php echo URL . $classe . "/imprimirOs/" . $orcamentos[0]->Cd_Orcamento ?>', '_blank');
        return false;
      });

    <?php } ?>
  });
  $('#periodo_inicial').datepicker({
    format: "dd/mm/YYYY",
    language: "pt-BR",
    todayHighlight: true,
    orientation: "bottom auto"
  });

  $(document).ready(function() {
    $("#periodo_inicial").datepicker();
    $('.i-periodo-inicial').click(function() {
      $("#periodo_inicial").focus();
    });

    $("#periodo_final").datepicker();
    $('.i-periodo-final').click(function() {
      $("#periodo_final").focus();
    });
  });

  function recalculaValor() {
    var valor_total = 0;
    $("#tabelaImagem > tbody > tr").each(function() {
      var imagemId = $(this).attr('id');
      imagemId = imagemId.replace("imagem", "");
      if ($('#Vl_Total-' + imagemId).val() > 0) {
        valor_total = Number($('#Vl_Total-' + imagemId).val()) + valor_total;
        // valor_total = $('#Vl_Total-'+imagemId).val() + valor_total;
      }
    });

    valor_total = valor_total.toFixed(2);
    // var valor_total_monetario = valor_total.replace(",", ".");
    $('#totalPedido').html(valor_total);
    $('#totalSaldo').html(valor_total);
    if (modelo_empresa != 'venda-avulsa') {
      $('#Vl_Bruto').val(valor_total);
    }

    $('#vl_liquido').val(valor_total);
    if ($('#totalLiquido').length > 0) {
      $('#totalLiquido').val(valor_total);
    }
    // $("#VL_desconto").val(('#VL_desconto').val());
    // $("#Vl_Entrada").val(('#Vl_Entrada').val());

    calculaDesconto();
    calculaEntrada();
  }

  $('#Vl_Bruto').change(function() {
    recalculaValor();
  })

  function calculaDesconto() {
    var tipoDesconto = modelo_empresa != 'venda-avulsa' ? $('#tipo_desconto').val() : 'M'
    var Vl_Bruto = $('#Vl_Bruto').val();
    var Vl_Desconto = ($('#VL_desconto').val()).replace(".", "").replace(",", ".");
    var vl_liquido = tipoDesconto == 'M' ? Vl_Bruto - Vl_Desconto : Vl_Bruto - (Vl_Bruto * (Vl_Desconto / 100));
    vl_liquido = (Math.round(vl_liquido * 100) / 100).toFixed(2);
    $('#vl_liquido').val(vl_liquido);
    if ($('#totalLiquido').length > 0) {
      $('#totalLiquido').val(vl_liquido);
    }
    $('#totalSaldo').html(vl_liquido);
  }

  function calculaEntrada() {
    var vl_liquido = $('#vl_liquido').val();
    vl_liquido = Number(vl_liquido);
    var Vl_Entrada = ($('#Vl_Entrada').val()).replace(".", "").replace(",", ".");
    Vl_Entrada = Number(Vl_Entrada);
    var saldo = vl_liquido - Vl_Entrada;
    saldo = saldo.toFixed(2);
    $('#totalSaldo').html(saldo);
  }

  function printData(relatorio) {
    var divToPrint = document.getElementById(relatorio);
    newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

  $(document).ready(function() {
    $(document).on('change', '.recalcula', function(ev) {
      var imagemId = $(this).attr('id');
      imagemId = imagemId.replace("VL_ADICIONAIS-", "").replace("Vl_Moldura-", "");
      // var Vl_Moldura = $('#Vl_Moldura-'+imagemId).attr('value');
      var vars = {};
      var molduras = [];
      var componentes = [];
      var arrComponentes = [];
      var arrMolduras = [];
      // debugger
      $('#imagem' + imagemId + ' :input').each(function(index, obj) {
        var valueToPush = {};
        var valor = $(this).val();
        var nome_campo = $(this).attr('name').replace("[" + imagemId + "]", "");
        if (valor != '') {
          if (nome_campo == 'VL_ADICIONAIS') {
            vars[nome_campo] = $(this).val();
          } else {
            vars[nome_campo] = $(this).val();
          }
        }
      });

      const regex = /\[[0-9]+\]\[\]/g;
      $('#molduras-imagem' + imagemId + ' :input').each(function(index, obj) {
        var valueToPush = {};
        var valor = $(this).val();
        var nome_campo = $(this).attr('name').replace(regex, "");
        if (nome_campo == 'Cd_Produto') {
          arrValuesMoldura = obj.value.split("|")
          objValue = arrValuesMoldura[0] || 0
          complexidade = arrValuesMoldura[1] || 0
          sarrafoReforco = arrValuesMoldura[2] || 'N'

          valueToPush["valor"] = objValue;
          valueToPush['complexidade'] = complexidade;
          valueToPush['sarrafoReforco'] = sarrafoReforco;


          molduras.push(valueToPush);
        } else if (nome_campo == 'CD_Componente') {
          arrValuesComponente = obj.value.split("|")
          objValue = arrValuesComponente[0] || 0
          quantidade = arrValuesComponente[1] || 0
          componentes.push({
            'valor': objValue,
            'complexidade': 0,
            'quantidade': quantidade,

          });
          valueToPush["name"] = $(this).val();
          valueToPush["text"] = $(this).attr("text");
          arrComponentes.push(valueToPush);
        }
      });

      var Cd_Prod_Aux = vars['Cd_Prod_Aux'];
      var Qt_Item = vars['Qt_Item'];
      var Md_Largura = vars['Md_Largura'];
      var Md_Altura = vars['Md_Altura'];
      var Vl_Unitario = vars['Vl_Unitario'];
      var Vl_Bruto = vars['Vl_Bruto'];
      var VL_ADICIONAIS = vars['VL_ADICIONAIS'];
      var Vl_Moldura = vars['Vl_Moldura'];
      var Ds_Observacao = vars['Ds_Observacao'];
      var Ds_ObservacaoProducao = vars['Ds_ObservacaoProducao'];

      vars['molduras'] = molduras;
      vars['componentes'] = componentes;

      $.ajax({
        url: '<?php echo URL; ?>Orcamento/calculaTotal/',
        type: 'post',
        data: 'Cd_Prod_Aux=' + Cd_Prod_Aux + '&Qt_Item=' + Qt_Item + '&Md_Largura=' + Md_Largura + '&Md_Altura=' + Md_Altura + '&Vl_Adicionais=' + VL_ADICIONAIS + '&Vl_Moldura=' + Vl_Moldura + '&molduras=' + JSON.stringify(molduras) + '&componentes=' + JSON.stringify(componentes) + '&Vl_Moldura=' + Vl_Moldura + '&Vl_Unitario=' + Vl_Unitario,
        dataType: 'json',
        success: function(json) {
          Vl_Unitario_Item = json.total_unitario;
          Vl_Unitario_Item = Vl_Unitario_Item.toFixed(2);

          Vl_Total = json.total_bruto;
          Vl_Total = Vl_Total.toFixed(2);

          $("#Vl_Unitario-" + imagemId).val(Vl_Unitario_Item);
          $("#valor_custo-" + imagemId).val(json.valor_total_custo);
          $("#Vl_Total-" + imagemId).val(Vl_Total);

          if (exibir_valores_individuais) {
            json.itens.forEach(item => {
              elemento = $(`#preco_produto_${item.item}_${item.componente}_${item.item_posicao}`)
              if (elemento.length > 0) {
                valorProdutoFormatado = new Intl.NumberFormat('pt-BR', {
                  style: 'currency',
                  currency: 'BRL'
                }).format(
                  item.valor_total_com_adicional
                )
                elemento.html(valorProdutoFormatado)
              }
            })
          }

          exibirPrecoCusto = exibir_total_custo_item ? `Preço de custo R$ ${json.valor_total_custo}` : "";
          $("#Vl_Unitario-Text-" + imagemId).html("R$ " + Vl_Unitario_Item);
          $("#valor_total_custo-Text-" + imagemId).html(exibirPrecoCusto)
          $("#Vl_Total-Text-" + imagemId).html("R$ " + Vl_Total);
          recalculaValor();
        }
      });
    });


  });

  $('body').on('keyup', '.campoMonetario', function(ev) {
    var result = "";
    if ($(this).val() != "") {
      var vr = filtraCampo($(this).val());

      var tam = vr.length;

      var validChars = "0123456789";
      for (var n = 0; n < tam; n++) {
        if (validChars.indexOf(vr.substring(n, n + 1)) != -1) {
          result += vr.substring(n, n + 1);
        }
      }
      $(this).val(result);

      vr = $(this).val();

      tam = vr.length;

      var cont = 0;
      var comzero = new String;

      for (var i = 0; i < tam; i++) {
        if (vr.substring(i, i + 1) == 0) {
          comzero = comzero;
          if (cont == 1) {
            comzero = comzero + vr.substring(i, i + 1);
          }
        } else {
          comzero = comzero + vr.substring(i, i + 1);
          cont = 1;
        }
      }

      vr = (comzero == "") ? "0" : comzero;
      tam = vr.length;

      // Formata o campo, adicionando '.' e ','
      if ((tam == 1) && (vr == 0)) {
        $(this).val("0,00");
      } else {
        $(this).val("0,0" + vr);
      }
      if (tam == 2) {
        $(this).val("0," + vr);
      }
      if ((tam > 2) && (tam <= 5)) {
        $(this).val(vr.substr(0, tam - 2) + ',' + vr.substr(tam - 2, tam));
      }
      if ((tam >= 6) && (tam <= 8)) {
        $(this).val(vr.substr(0, tam - 5) + '.' + vr.substr(tam - 5, 3) + ',' + vr.substr(tam - 2, tam));
      }
      if ((tam >= 9) && (tam <= 11)) {
        $(this).val(vr.substr(0, tam - 8) + '.' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 3) + ',' + vr.substr(tam - 2, tam));
      }
      if ((tam >= 12) && (tam <= 14)) {
        $(this).val(vr.substr(0, tam - 11) + '.' + vr.substr(tam - 11, 3) + '.' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 3) + ',' + vr.substr(tam - 2, tam));
      }
      if (tam > 14) {
        $(this).val(vr.substr(0, tam - 15) + '.' + vr.substr(tam - 15, 3) + '.' + vr.substr(tam - 13, 3) + '.' + vr.substr(tam - 10, 3) + ',' + vr.substr(tam - 6, 2));
      }
    }

  });

  function filtraCampo(campo) {
    var s = "";
    var cp = "";
    var valor = campo;

    var tamanho = valor.length;

    for (i = 0; i < tamanho; i++) {
      if (valor.substring(i, i + 1) != "/" && valor.substring(i, i + 1) != "-" && valor.substring(i, i + 1) != "." && valor.substring(i, i + 1) != ",") {
        s = s + valor.substring(i, i + 1);
      }
    }
    valor = s;

    return cp = valor;
  }

  $('.imprimirRelatorio').on('click', function() {
    printData($(this).attr('relatorio'));
  })

  $('.relatorioPedidosFabrica').on('click', function() {
    printData($(this).attr('relatorio'));
    /*
  $.ajax({
    url: '<?php echo URL; ?>Relatorio/impressaoFabricaPdf/0/',
    type: 'post',
    data: 'periodo_inicial=' + $('#periodo_inicial').val() + '&periodo_final=' + $('#periodo_final').val(),
    dataType: 'json',
    success: function(json) {
      $("#arquivoPDF").attr('src','<?php echo URL; ?>/pdfTemporario/'+ json.arquivo);
      $("#embed2").attr('src','<?php echo URL; ?>/pdfTemporario/'+ json.arquivo);
      $( "#impressaoRelatorio" ).dialog( "open" );
    }
  });
  */

  });

  $(function() {
    $("#impressaoRelatorio").dialog({
      autoOpen: false,
      modal: true,
      title: 'Arquivo para impressão',
      width: 800,
      height: 600
    });

    if ($('#UnidadeProduto').find(":selected").attr('data-metragem-linear') == '1') {
      $("#displayMedidaVara").show()
      $(".campoQuantidadeCm").show()
      return
    } else {
      $("#displayMedidaVara").hide()
      $(".campoQuantidadeCm").hide()
      return
    }

    <?php if (isset($unidade->MetragemLinear) && $unidade->MetragemLinear == 1) { ?>
      $("#displayMedidaVara").show()
      $(".campoQuantidadeCm").show()
    <?php } ?>
  });

  $("input[type='text']").on("click", function() {
    $(this).select();
  });

  function copiarConteudo() {
    // Get the text field
    var copyText = document.getElementById("myInput");

    // Select the text field
    copyText.select();
    copyText.setSelectionRange(0, 99999); // For mobile devices

    // Copy the text inside the text field
    navigator.clipboard.writeText(copyText.value);

    // Alert the copied text
    alert("Copied the text: " + copyText.value);
  }
</script>
<script type="text/javascript">
  $('.input_molduras').sortable({
    stop: function(event, ui) {
      recalculaValor()
    }
  });

  $(document).ready(function() {
    $('.filtro_situacao').select2({
      placeholder: 'Selecione uma opção',
      multiple: true
    });

    $('.filtro_tipo_documento').select2({
      placeholder: 'Selecione uma opção',
      multiple: true
    });

    function formatState(state) {
      if (!state.id) {
        return state.text;
      }

      return $(
        '<div style="' + $(state.element).data('style') + '"> ' + state.text + '</div>'
      );
    };

    if (modelo_empresa != 'venda-avulsa') {
      $('.moldura, .componente').select2({
        placeholder: 'Selecione uma opção',
        multiple: false,
        templateResult: formatState
      });
    }


    $.fn.dataTable.moment('DD/MM/YYYY');
    const listaAcessos = $('#listaAcessos').DataTable({
      language: {
        url: '<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/pt-Br.json'
      },
      searching: false,
      order: [
        [0, "desc"]
      ]
    });

    const colunasListaPedidos = multiplasEmpresas ? [{
        "data": "Cd_Orcamento"
      },
      {
        "data": "Dt_Orcamento"
      },
      {
        "data": "Dt_Prevista_Entrega"
      },
      {
        "data": "Cd_Cliente"
      },
      {
        "data": "nome_empresa"
      },
      {
        "data": "nome_vendedor"
      },
      // {"data" : "Consumidor_Temp"},
      {
        "data": "Id_Situacao"
      },
      {
        "data": "Editar"
      }
    ] : [{
        "data": "Cd_Orcamento"
      },
      {
        "data": "Dt_Orcamento"
      },
      {
        "data": "Dt_Prevista_Entrega"
      },
      {
        "data": "Cd_Cliente"
      },
      <?php if ($objOrcamento->permissao_visualizar_pedidos_outros_vendedores) {

        echo '{
            "data": "nome_vendedor"
          },';
      }  ?>



      // {"data" : "Consumidor_Temp"},
      {
        "data": "Id_Situacao"
      },
      {
        "data": "Editar"
      }
    ]

    var tableListaOrcamento = $('#listaOrcamento').DataTable({
      searchCols: [
        null,
        null,
        null,
        null,
        {
          "search": ""
        },
        null
      ],
      processing: true,
      serverSide: true,
      searching: false,
      ajax: {
        url: '<?php echo URL; ?>Orcamento/listaOrcamentosDataTables/1/',
        data: function(d) {
          d.filtro_empresa = $('#empresa').val() || ''
          d.filtro_vendedor = $('#vendedor').val() || ''
          d.filtro_numero_pedido = $('#pedido').val() || '',
            d.filtro_situacao = $('#situacao').val() || <?php echo isset($filtro_padrao_situacao) ? '[' . $filtro_padrao_situacao . ']' : '[]'; ?>,
            d.filtro_cliente = $('#filtro_cliente').val() || []
        }
      },
      columns: colunasListaPedidos,
      language: {
        url: '<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/pt-Br.json'
      },
      columnDefs: [{
        orderable: false,
        targets: 5
      }],
      order: [
        [0, "desc"]
      ]
    });

    $('#filtro_empresa_dashboard').change(function() {
      window.location = "<?php echo URL . '/Dashboard/index/?filtro_empresa='; ?>" + $(this).val()
    })

    $('#filtroPedidos').click(function() {
      $('#listaOrcamento').DataTable().ajax.reload()
    });

    $('#listaClientes').DataTable({
      "processing": true,
      "serverSide": true,
      searching: true,
      "ajax": {
        "url": '<?php echo URL; ?>Cliente/listaClientesDataTables/1/'
      },
      columns: [{
          "data": 'RazaoSocial'
        },
        {
          "data": 'Nomefantasia'
        },
        {
          "data": 'Endereco'
        },
        {
          "data": 'Telefone1'
        },
        {
          "data": 'Telefone2'
        },
        {
          "data": 'Ramal'
        },
        {
          "data": 'EMail'
        },
        {
          "data": 'Contato'
        },
        {
          "data": 'Editar'
        },
        {
          "data": 'Excluir'
        }
      ],
      language: {
        url: '<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/pt-Br.json'
      },
      "order": [
        [0, "asc"]
      ],
      "columnDefs": [{
        "targets": "8, 9",
        "orderable": false
      }]
    });

    var colunasListaProdutos = parceria_ruberti ? [{
        "data": 'NovoCodigo'
      },
      {
        "data": 'DescricaoProduto'
      },
      {
        "data": 'Quantidade'
      },
      {
        "data": 'Desenho'
      },
      {
        "data": 'PrecoVendaMaoObra'
      },
      {
        "data": 'Comprar'
      },
      {
        "data": 'Editar'
      }
    ] : [{
        "data": 'NovoCodigo'
      },
      {
        "data": 'DescricaoProduto'
      },
      {
        "data": 'Quantidade'
      },
      {
        "data": 'Desenho'
      },
      {
        "data": 'PrecoVendaMaoObra'
      },
      {
        "data": 'Editar'
      }
    ]

    <?php if ($arrPermissoesCabecalho[$indiceProdutos]->excluir == 1) { ?>
      colunasListaProdutos.push({
        "data": 'Excluir'
      })
    <?php } ?>

    const colunasOrdenaveisListaProdutos = parceria_ruberti === 'true' ? [5, 6, 7] : [5, 6]

    $('#aplicarFiltroProdutos').click(function() {
      $('#listaProdutos').DataTable().ajax.reload()
    });

    $('#exibirFiltroProdutos').click(function() {
      if ($('#divFiltroProdutos').hasClass('hide')) {
        $('#divFiltroProdutos').removeClass('hide');
        return
      }

      $('#divFiltroProdutos').addClass('hide');
    })

    $('#limparFiltroProdutos').on('click', function() {
      const formulario = $(this).closest('form').attr('id')
      $(`form#${formulario} input[type=text]`).each(
        function(index) {
          var input = $(this);
          $(this).val('')
        }
      )

      $(`form#${formulario} select`).each(
        function(index) {
          var input = $(this);
          $(this).val('')
        }
      )
    })

    const listaProdutos = $('#listaProdutos').DataTable({
      "processing": true,
      "serverSide": true,
      "searching": true,
      "responsive": true,
      "ajax": {
        "data": function(d) {
          d.filtro_componente = $('#filtro_componente').val() || '',
            d.filtro_unidade_medida = $('#filtro_unidade_medida').val() || '',
            d.filtro_fornecedor = $('#filtro_fornecedor').val() || '',
            d.filtro_situacao_produto = $('#filtro_situacao_produto').val() || ''

        },
        "url": '<?php echo URL; ?>Produto/listaProdutosDataTables/1/',
      },
      columns: colunasListaProdutos,
      language: {
        url: '<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/pt-Br.json'
      },
      "order": [
        [0, "asc"]
      ],
      "columnDefs": [{
          "targets": colunasOrdenaveisListaProdutos,
          "orderable": false
        },
        {
          visible: false,
          targets: 0
        }
      ]
    })

  });

  $('body').on('click', '.excluirProduto', function(e) {
    e.preventDefault();
    var confirma = confirm('Deseja realmente excluir esse item?');
    if (confirma == false) {
      return false;
    }

    let cdProduto = $(this).attr('produtoId');
    window.location = "<?php echo URL . $classe . '/excluir/'; ?>" + cdProduto;

  });

  $('body').on('click', '.excluirContaReceber', function(e) {
    e.preventDefault();
    var confirma = confirm('Deseja realmente excluir esse item?');
    if (confirma == false) {
      return false;
    }

    let cdContaReceber = $(this).data('handle-conta-receber');

    $.ajax({
      url: "<?php echo URL . $classe . '/excluir/'; ?>" + cdContaReceber,
      type: 'get',
      dataType: 'json',
      success: function(json) {
        $('#mensagemContasReceber').removeClass('hide');
        $('#mensagemContasReceber').addClass(json.tipoMensagem);
        $('#mensagemContasReceber > p').html(json.mensagem);
      }
    });

    $('#listaContasReceber').DataTable().ajax.reload()

    setTimeout(
      function() {
        $('#mensagemContasReceber').addClass('hide');
        $('#mensagemContasReceber').removeClass('callout-success');
        $('#mensagemContasReceber > p').html('');
      },
      3000
    );

  });

  $('body').on('click', '.emitirNotaServicoContaReceber', function(e) {
    e.preventDefault();
    var confirma = confirm('Deseja realmente emitir Nota de Serviço?');
    if (confirma == false) {
      return false;
    }

    urlDocumento = 'IntegraNotaServico';
    let cdContaReceber = $(this).data('handle-conta-receber');
    const url = '<?php echo URL; ?>' + urlDocumento + '/emitirDocumentoContaReceber/' + cdContaReceber
    $.ajax({
      url,
      type: 'get',
      dataType: 'json',
      success: function(json) {
        if (json.success == '1') {

          mensagem = json.msg

          if (json.codigo_retorno == '5008') {
            alert(mensagem)
            return
          }

          var interv = setInterval(function() {
            const id_documento = json.id_documento
            const urlConsultaNota = '<?php echo URL; ?>' + urlDocumento + '/consultarNota/' + id_documento
            var status = 0;
            $.ajax({
              url: urlConsultaNota,
              type: 'get',
              dataType: 'json',
              success: function(json) {
                mensagem = json.msg
                codigo = json.codigo
                status = json.status
                console.log({
                  mensagem,
                  status,
                  codigo
                })
                //clearInterval(interv)
                if (status !== 'PROCESSANDO') {
                  if (status === 'Autorizado') {
                    if (json.link_pdf != '') {
                      window.open(json.link_pdf, '_blank')
                    } else {
                      const urlDownloadDocumento = '<?php echo URL; ?>' + urlDocumento + '/downloadNota/' + id_documento
                      window.open(urlDownloadDocumento, '_blank')
                    }
                    interromperConsulta = true;
                    clearInterval(interv)
                  }
                  if (status == 'REJEITADO' || status == 'ERRO') {
                    alert(`Erro ao processar o documento: ${mensagem}`)
                    interromperConsulta = true;
                    clearInterval(interv)
                  }
                  return;
                }
                numeroTentativas++;
                if (numeroTentativas >= 5) {
                  alert(`Ainda não houve retorno do SEFAZ.\n O status da nota é ${status} Você poderá consulta-la em Pedidos > Documentos Fiscais`)
                  clearInterval(interv)
                }
              }
            })
          }, 2000);
        } else {
          strMsg = 'ERRO AO GERAR O DOCUMENTO\n'
          strMsg += json.msg
          alert(strMsg)
        }
      }
    });

  });

  $('body').on('click', '.naoExcluirProduto', function(e) {
    alert('Esse produto não pode ser excluído por estar sendo utilizado em um ou mais orçamentos');
  });

  $('body').on('click', '.excluirCliente', function(e) {
    e.preventDefault();
    var confirma = confirm('Deseja realmente excluir esse cliente?');
    if (confirma == false) {
      return false;
    }

    let cdCliente = $(this).attr('clienteId');
    window.location = "<?php echo URL . $classe . '/excluir/'; ?>" + cdCliente;

  });

  $('body').on('click', '.naoExcluirCliente', function(e) {
    alert('Esse cliente não pode ser excluído por estar sendo utilizado em um ou mais orçamentos');
  });

  $('body').on('click', '.excluirComponente', function(e) {
    e.preventDefault();
    var confirma = confirm('Deseja realmente excluir esse componente?');
    if (confirma == false) {
      return false;
    }

    let cdComponente = $(this).attr('ComponenteId');
    window.location = "<?php echo URL . $classe . '/excluir/'; ?>" + cdComponente;

  });

  $('body').on('click', '.naoExcluirComponente', function(e) {
    alert('Esse componente não pode ser excluído por estar sendo utilizado em um ou mais orçamentos');
  });

  $('body').on('change', '#UnidadeProduto', function(e) {
    if ($(this).find(":selected").attr('data-metragem-linear') == '1') {
      $("#displayMedidaVara").show()
      $(".campoQuantidadeCm").show()
      return
    }
    $("#displayMedidaVara").hide()
    $(".campoQuantidadeCm").hide()
  });

  $('body').on('change', '#definicao_preco', function(e) {
    if ($(this).val() == '1') {
      $(".formacao-preco-fixo").show()
      $(".formacao-preco-percentual").hide()
      return
    }
    $(".formacao-preco-fixo").hide()
    $(".formacao-preco-percentual").show()
  });

  $('.tipoCliente').on('change', function() {
    if ($(this).val() == 'J') {
      $(".nomeRazao").html('Razão Social');
      $(".documentoCliente").html("CNPJ")
      $('input[name="CGC"]').inputmask({
        "mask": "99.999.999/9999-99",
        'autoUnmask': true
      });
      $(".nomeFantasia").show()
      $(".inscricaoEstadual").show()
    } else {
      $(".nomeRazao").html('Nome');
      $(".documentoCliente").html("CPF")
      $(".nomeFantasia").hide()
      $(".inscricaoEstadual").hide()
    }
  });

  $('input[name="CGC"]').blur(function() {
    var cnpj = $.trim($('input[name="CGC"]').val().replace(/\D/gm, ``));

    $.getJSON("https://publica.cnpj.ws/cnpj/" + cnpj, function(dados) {
      console.log(dados)
      if (!("erro" in dados)) {
        $('input[name="RazaoSocial"]').val(dados.razao_social);
        $('input[name="NomeFantasia"]').val(dados.estabelecimento.nome_fantasia || dados.razao_social);
        $('input[name="InscricaoEstadual"]').val(dados.estabelecimento.inscricoes_estaduais[0].inscricao_estadual || '');
        $('input[name="Telefone1"]').val(dados.estabelecimento.telefone1);
        $('input[name="Telefone2"]').val(dados.estabelecimento.telefone2);
        $('input[name="CEP"]').val(dados.estabelecimento.cep);
        $('input[name="Endereco"]').val(`${dados.estabelecimento.tipo_logradouro} ${dados.estabelecimento.logradouro}, ${dados.estabelecimento.numero}`);
        $('input[name="Complemento"]').val(dados.estabelecimento.complemento);
        $('input[name="numeroEndereco"]').val(dados.estabelecimento.numero);

        $('input[name="Bairro"]').val(dados.estabelecimento.bairro);
        $('input[name="Cidade"]').val(dados.estabelecimento.cidade.nome);

        $('select[name="Estado"]').val(dados.estabelecimento.estado.sigla);
        $('input[name="EMail"]').val(dados.estabelecimento.email);

        console.log(`Buscando selecionando municipio: ${dados.estabelecimento.cidade.ibge_id}`)
        buscaMunicipioPorEstado(dados.estabelecimento.cidade.ibge_id);

        // $('input[name="NomeFantasia"]').val(dados.razao_social);
        // $('input[name="Endereco"]').val(dados.logradouro);
        // $('input[name="Bairro"]').val(dados.bairro);
        // // $('input[name="Cidade"]').val(unescape(dados.localidade));
        // $('select[name="Estado"]').find('option[value="' + dados.uf.toUpperCase() + '"]').attr('selected', true);
        // buscaMunicipioPorEstado(dados.ibge);
      }
    });

  })

  $('input[name="CEP"]').blur(function() {
    var cep = $.trim($('input[name="CEP"]').val().replace('-', ''));

    $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {
      console.log(dados)
      if (!("erro" in dados)) {
        $('input[name="Endereco"]').val(dados.logradouro);
        $('input[name="Bairro"]').val(dados.bairro);
        // $('input[name="Cidade"]').val(unescape(dados.localidade));
        $('select[name="Estado"]').find('option[value="' + dados.uf.toUpperCase() + '"]').attr('selected', true);
        buscaMunicipioPorEstado(dados.ibge);
      }
    });
  });

  $('input[name="Telefone1"]').blur(function() {
    var telefone = $.trim($('input[name="Telefone1"]').val());

    if (!telefone) {
      return
    }

    $.ajax({
      url: '<?php echo URL; ?>Cliente/consultaClienteExiste/',
      type: 'post',
      data: 'telefone=' + telefone,
      dataType: 'json',
      success: function(json) {
        if (json.quantidade == 1) {
          alert('Já existe um cliente cadastrado com este número de telefone')
        }
      }
    });
  });

  $('input[name="CGC"]').blur(function() {
    var documento = $.trim($('input[name="CGC"]').val());
    var tipoCliente = $('select[name="TipoCliente"]').val();

    if (!documento) {
      return
    }

    $.ajax({
      url: '<?php echo URL; ?>Cliente/consultaClienteExiste/',
      type: 'post',
      data: 'documento=' + documento + '&tipoCliente=' + tipoCliente,
      dataType: 'json',
      success: function(json) {
        if (json.quantidade == 1) {
          alert('Já existe um cliente cadastrado com este número de documento')
        }
      }
    });
  });

  $('select[name="Estado').change(function() {
    buscaMunicipioPorEstado();
  })

  $('#id_forma_pagamento_entrada').change(function() {

    var quantidadeParcelas = $('#id_forma_pagamento_entrada option:selected').data('quantidade-parcelas');

    var optionQuantidadeParcelas = '';
    for (qtdEstoque = 1; qtdEstoque <= quantidadeParcelas; qtdEstoque++) {
      optionQuantidadeParcelas = optionQuantidadeParcelas + `<option value="${qtdEstoque}">${qtdEstoque}</option>`
    }

    $("#numero_parcelas_entrada").html(optionQuantidadeParcelas)


  })

  $('#id_forma_pagamento_saldo').change(function() {

    var quantidadeParcelas = $('#id_forma_pagamento_saldo option:selected').data('quantidade-parcelas');

    var optionQuantidadeParcelas = '';
    for (qtdEstoque = 1; qtdEstoque <= quantidadeParcelas; qtdEstoque++) {
      optionQuantidadeParcelas = optionQuantidadeParcelas + `<option value="${qtdEstoque}">${qtdEstoque}</option>`
    }

    $("#numero_parcelas_saldo").html(optionQuantidadeParcelas)


  })

  function buscaMunicipioPorEstado(id_municipio = '') {
    $('#Cidade').find('option').remove().end().append('<option value="">Carregando municipios...</option>');
    $.getJSON("<?php echo URL; ?>Municipio/buscaPorEstado/" + $('#Estado').val(), function(dados) {
      $('#Cidade').find('option').remove().end();
      $.each(dados, function(key, value) {
        selected = '';
        if (value.codigo_ibge == id_municipio) {
          selected = 'selected';
        }
        $('#Cidade').append('<option ' + selected + ' value=' + value.codigo_ibge + '>' + value.nome + '</option>');
      });

    });
  }

  <?php if (isset($clientes[0]->Cidade)) { ?>
    buscaMunicipioPorEstado('<?php echo $clientes[0]->Cidade; ?>');
  <?php } ?>

  $('.btnRecarregarConsulta').click(function() {
    $('.btnRecarregarConsulta').attr('disabled', '');
    const urlDocumento = $(this).data('tipo-documento') == 1 ? 'IntegraNota' : 'IntegraNotaServico'
    $.ajax({
      url: '<?php echo URL; ?>' + urlDocumento + '/processarFilaNotas/',
      type: 'get',
      dataType: 'json',
      success: function(json) {
        alert(json.msg);
        location.reload()
      }
    });
  });

  $('#exibirConteudoXML').on('show.bs.modal', function(event) {

  })

  $("#salvarTributacao").click(function() {
    if ($('#id_operacao_fiscal').val().trim() == '') {
      alert('Por favor informe uma operação fiscal!')
      return false
    }
    if ($('#id_estado').val().trim() == '') {
      alert('Por favor informe um estado!')
      return false
    }

    $('#frmTributacao').submit()
  });

  $('input[name="Quantidade"]').change(function() {
    if ($("#UnidadeProduto").find(':selected').data('sigla-unidade-medida') === 'UN') {
      return
    }
    const atualizarEstoqueMetragem = confirm('Deseja atualizar o estoque em metragem?')
    if (atualizarEstoqueMetragem) {
      const quantidade = parseInt($('input[name="Quantidade"]').val())
      const medida_vara = parseFloat($('input[name="MedidaVara"]').val().replaceAll('.', '').replaceAll(',', '.'))
      console.log(typeof quantidade, quantidade)
      console.log(typeof medida_vara, medida_vara)
      $('input[name="QuantidadeCm"]').val(quantidade * medida_vara)
    }
  })

  $('input[name="QuantidadeMinima"]').change(function() {
    if ($("#UnidadeProduto").find(':selected').data('sigla-unidade-medida') === 'UN') {
      return
    }
    const atualizarEstoqueMetragem = confirm('Deseja atualizar o estoque mínimo em metragem?')
    if (atualizarEstoqueMetragem) {
      const quantidade = parseInt($('input[name="QuantidadeMinima"]').val())
      const medida_vara = parseFloat($('input[name="MedidaVara"]').val().replaceAll('.', '').replaceAll(',', '.'))
      console.log(typeof quantidade, quantidade)
      console.log(typeof medida_vara, medida_vara)
      $('input[name="QuantidadeMinimaCm"]').val(quantidade * medida_vara)
    }
  })

  function statusAtual(status, cd_pedido) {
    $('select#Id_Nova_Situacao').children('option').attr('disabled', false)
    $('select#Id_Nova_Situacao').children('option[value="' + status + '"]').attr('disabled', true)
    switch (status) {
      case 2:
        $('#Id_Nova_Situacao').val('3')
        break
      case 3:
        $('#Id_Nova_Situacao').val('4')
        break
    }

    $('#handle').val(cd_pedido)
  }

  $("#btnAlterarSituacao").on('click', function() {
    $('#btnAlterarSituacao').addClass('disabled')
    const nova_situacao = $('#Id_Nova_Situacao').val()
    const observacao = $('#Ds_Observacao').val()
    const id_pedido = $('#handle').val()

    if (!nova_situacao || !id_pedido) {
      alert('Não foi identificado a nova situação ou o código do pedido')
      $('#btnAlterarSituacao').removeClass('disabled')
      $('#btnAlterarSituacao').text('Salvar')
      return
    }

    $('#btnAlterarSituacao').text('Alterando a situação do pedido...Por favor aguarde')

    $("#frmAlterarSituacao").submit()


    // $(`#pedido${id_pedido}`).appendTo(`#listaProducao${nova_situacao}`)
  })

  $('#filial').click(function() {
    if ($('#filial').val() == '1') {
      $('.empresa-matriz').removeClass('hide')
    } else {
      $('.empresa-matriz').addClass('hide')
    }
  })

  var fileTypes = ['jpg', 'jpeg', 'png'];

  function readURLImagem(input) {
    nome_arquivo = '';
    tipo_arquivo = '';
    conteudo_arquivo = '';
    msg_erro = ''
    isSuccess = true;
    $('.alertaArquivoInvalido').hide()
    $('.alertaArquivoInvalido').empty()
    if (input.files && input.files[0]) {
      var extension = input.files[0].name.split('.').pop().toLowerCase();
      if (fileTypes.indexOf(extension) == -1) {
        msg_erro = '<p>Formato da imagem ' + input.files[0].name + ' invalido</p>'
        isSuccess = false;
      }

      var sizeInMB = (input.files[0].size / 1024).toFixed(2);
      if (sizeInMB > 200) {
        msg_erro = msg_erro + '<p>Tamanho da imagem ' + input.files[0].name + ' invalido</p>';
        isSuccess = false;
      }

      var reader = new FileReader();
      if (isSuccess) {

        reader.onload = function(e) {
          $('#imageResult')
            .attr('src', e.target.result);

          conteudo_arquivo = e.target.result;

          $('#logotipoSelecionado').val(conteudo_arquivo)
        };
        const a = reader.readAsDataURL(input.files[0]);


      } else {
        $('#imageResult').attr('src', '');
        $('#upload').attr('value', '');
        if (msg_erro != '') {
          $('.alertaArquivoInvalido').append(msg_erro)
          $('.alertaArquivoInvalido').show()
        }
      }
    }
  }

  $(function() {
    $('#logotipo').on('change', function() {
      readURLImagem(this);
    });

    $('#limparFiltro').on('click', function() {
      const formulario = $(this).closest('form').attr('id')
      $(`form#${formulario} input[type=text]`).each(
        function(index) {
          var input = $(this);
          $(this).val('')
        }
      )

      $(`form#${formulario} select`).each(
        function(index) {
          var input = $(this);
          $(this).val('')
        }
      )
    })
  })

  $('#btnGerarFinanceiro').on('click', function() {
    $('#btnGerarFinanceiro').attr('disabled', 'true')
    $('#btnGerarFinanceiro').text('Salvando lançamento financeiro, aguarde...')

    const inputGerarFinanceiro = 'date=' + $("#date").val() + '&cdtvencimento=' + $("#cdtvencimento").val() + '&cdtpagamento=' + $("#cdtpagamento").val() + '&CategoryId=' + $("#CategoryId").val() +
      '&Title=' + '<?php echo isset($orcamentos[0]->Cd_Cliente) ? $orcamentos[0]->Cd_Cliente : 0; ?>' +
      '&Amount=' + $("#Amount").val() +
      '&AccountId=' + $("#AccountId").val() +
      '&description=' + $("#description").val() +
      '&description=' + $("#description").val() +
      '&UserId=' + $('#UserId').val() +
      '&ndoc=' + '<?php echo isset($orcamentos[0]->Cd_Orcamento) ? $orcamentos[0]->Cd_Orcamento : 0; ?>';

    $.ajax({
      url: '<?php echo URL; ?>Assets/cadastrar/0/origem:orcamento',
      data: inputGerarFinanceiro,
      type: 'post',
      dataType: 'json',
      success: function(json) {
        alert(json.msg)
        if (json.success == 0) {
          $('#btnGerarFinanceiro').removeAttr('disabled', '')
        }
        $('#btnGerarFinanceiro').text('Lançamento Financeiro')
      }
    });
  })

  $('#listaContasReceber').DataTable({
    // responsive: true,
    processing: true,
    serverSide: true,
    lengthMenu: [10,25,50,100,200],
    pageLength: <?php echo isset($this->ambiente) && $this->ambiente == 'quadrosrios' ? '100' : '10'; ?>,    
    ajax: {
      url: '<?php echo URL; ?>Assets/listarDatatables/',
      data: function(d) {
        $('#frmFiltroFinanceiroRecebimentos .form-control').each(function(obj) {
          d.CategoryId = $("#CategoryId").val() || ''
          d.Title = $("#Title").val() || ''
          d.UserId = $("#UserId").val() || ''
          d.vendedor = $("#vendedor").val() || ''
          d.situacao = $("#situacao").val() || ''
          d.filtroDataLancamentoInicio = $("#filtroDataLancamentoInicio").val() || ''
          d.filtroDataLancamentoFim = $("#filtroDataLancamentoFim").val() || ''
          d.filtroDataVencimentoInicio = $("#filtroDataVencimentoInicio").val() || ''
          d.filtroDataVencimentoFim = $("#filtroDataVencimentoFim").val() || ''
          d.filtroDataPagamentoInicio = $("#filtroDataPagamentoInicio").val() || ''
          d.filtroDataPagamentoFim = $("#filtroDataPagamentoFim").val() || ''
          d.ndoc = $("#ndoc").val() || ''
        })
        // d.filtro_empresa = $('#empresa').val() || ''
        // d.filtro_vendedor = $('#vendedor').val() || ''
        // d.filtro_numero_pedido = $('#pedido').val() || '',
        //   d.filtro_situacao = $('#situacao').val() || <?php echo isset($filtro_padrao_situacao) ? '[' . $filtro_padrao_situacao . ']' : '[]'; ?>,
        //   d.filtro_cliente = $('#filtro_cliente').val() || []
      }
    },
    columns: [{
        "data": 'ndoc'
      },
      {
        "data": 'Date'
      },
      {
        "data": 'CategoryName'
      },
      {
        "data": 'RazaoSocial'
      },
      {
        "data": 'Amount'
      },
      {
        "data": 'nomeConta'
      },
      <?php echo $multiplasEmpresas ? '{"data": "Empresa"},' : '' ?>

      <?php echo ($modelo_empresa != 'gestao-interna') ? '{
        "data": "NomeUsuario"
      },' : ''; ?>

      {
        "data": 'Description'
      },
      {
        "data": 'cdtvencimento'
      },
      <?php echo ($modelo_empresa != 'gestao-interna') ? '{
        "data": "data_pagamento_loja"
      },' : ''; ?>

      {
        "data": 'cdtpagamento'
      },
      {
        "data": 'situacao_conta_receber'
      },
      <?php echo ($arrPermissoesCabecalho[$indiceContasAReceber]->editar)  ?
        '{
        "data": "editar",
        "orderable": "false"
      },' : '' ?>
      <?php echo ($arrPermissoesCabecalho[$indiceContasAReceber]->excluir)  ?
        '{
        "data": "excluir",
        "orderable": "false"
      },' : '' ?>
      <?php echo ($modelo_empresa == 'gestao-interna') ? '{
        "data": "emitir_nfse",
        orderable: false
      },' : ''; ?>
    ],
    'rowCallback': function(row, data, index) {
      $cor_situacao_conta_receber = data['situacao_conta_receber'] == 'PAGO' ? '#d1fcd1' : '#f7c0c0'
      $(row).find(`td:eq(${colunSituacaoContaReceber})`).css('background-color', $cor_situacao_conta_receber);
    },
    searching: false,
    paging: true,
    lengthChange: true,
    info: true,
    language: {
      url: '<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/pt-Br.json'
    },
    order: [
      [0, 'desc']
    ],
    footerCallback: function(row, data, start, end, display) {
      let api = this.api();

      /**
       * Formata a string para número
       * com os valores decimais corretos
       */
      let intVal = function(i) {
        return typeof i == 'string' ? i.replaceAll(".", "").replaceAll(",", ".") * 1 : i
      };

      // Total over all pages
      total = api
        .column(4)
        .data()
        .reduce((a, b) => intVal(a) + intVal(b), 0);

      // Total over this page
      pageTotal = api
        .column(4, {
          page: 'current'
        })
        .data()
        .reduce((a, b) => intVal(a) + intVal(b), 0);


      const totalFormatado = new Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'BRL'
      }).format(
        pageTotal,
      )

      // Update footer
      api.column(4).footer().innerHTML = totalFormatado
      // '$' + pageTotal + ' ( $' + total + ' total)';
    }
  });

  if ($('#listaContasReceber').length > 0) {
    var colunasListaContaReceber = $('#listaContasReceber').DataTable().settings().init().columns
    var colunSituacaoContaReceber = colunasListaContaReceber.findIndex(function(coluna) {
      return coluna.data == "situacao_conta_receber"
    });

    $('#aplicarFiltroContasReceber').click(function() {
      $('#listaContasReceber').DataTable().ajax.reload()
    });
  }

  // $('#listagemRelatorioSimplificado').DataTable({
  //   responsive: true,
  //   searching: false,
  //   paging: false,
  //   lengthChange: false,
  //   info: false
  // });

  $('#tipoRecebimento').on('change', function() {
    $('#divDiaVencimentoSemana').hide();
    $('#divDiaVencimento').hide();
    $('#divQuantidadeParcelas').hide();
    $('#divQuantidadeRepeticoes').hide();
    const tipoRecebimento = $(this).val();
    if (tipoRecebimento == 'U') {
      return
    }

    if (tipoRecebimento !== 'P') {
      $('#divQuantidadeRepeticoes').show();
    }

    if (tipoRecebimento == 'W') {
      $('#divDiaVencimentoSemana').show();
      return
    }

    $('#divDiaVencimento').show();

    if (tipoRecebimento == 'P') {
      $('#divQuantidadeRepeticoes').hide();
      $('#divQuantidadeParcelas').show();
    }
  })

  $('#Cd_Prod_Aux_Pedido').on('change', function() {
    const temMetragem = $('option:selected', this).data('metragem')
    if (temMetragem === 0) {
      $('.medidas').hide()
      return
    }

    $('.medidas').show()

  })


  $('#btnPedidoEnviarWhatsapp').show();
  $('#abreWhatsappIphone').hide();
  if (isIphone) {
    $('#btnPedidoEnviarWhatsapp').hide();
    $('#abreWhatsappIphone').show();
  }
</script>
</body>

</html>