<?php
require_once './src/Model/Configuracoes.php';
require_once './src/Model/GruposPermissoes.php';
require_once './src/Model/UsuarioEmpresa.php';
require_once './src/Model/Orcamento.php';
$configuracoes = new Configuracoes();
$gruposPermissoes = new GruposPermissoes();
$objOrcamentoCabecalho = new Orcamento();
$habilita_financeiro = filter_var($configuracoes->listarConfiguracao('habilita_financeiro')->valor, FILTER_VALIDATE_BOOLEAN);
$habilita_tributacao = filter_var($configuracoes->listarConfiguracao('habilita_tributacao')->valor, FILTER_VALIDATE_BOOLEAN);
$parceria_ruberti = filter_var($configuracoes->listarConfiguracao('parceria_ruberti')->valor, FILTER_VALIDATE_BOOLEAN);
$usuario_master = filter_var($_SESSION['usuario_master'], FILTER_VALIDATE_BOOLEAN);
$modelo_empresa = $configuracoes->listarConfiguracao('modelo_empresa')->valor;

$grupo_usuario = $_SESSION['handle_grupo'];

$arrPermissoesCabecalho = $gruposPermissoes->listarPorGrupo($grupo_usuario);

$indiceContasAPagar = array_search('Bills', array_column($arrPermissoesCabecalho, 'entidade'));
$indiceContasAReceber = array_search('Assets', array_column($arrPermissoesCabecalho, 'entidade'));

$permiteSalvarDataVencimento = array_search('PERMITE_SALVAR_DATA_DE_VENCIMENTO', array_column($arrPermissoesCabecalho, 'nome'));
$permiteSalvarDataPagamento = array_search('PERMITE_SALVAR_DATA_DE_PAGAMENTO', array_column($arrPermissoesCabecalho, 'nome'));


$indiceDocumentosFiscais = array_search('DocumentosFiscais', array_column($arrPermissoesCabecalho, 'entidade'));

$indicePedidos = array_search('Pedidos', array_column($arrPermissoesCabecalho, 'entidade'));

$indiceProducao = array_search('Producao', array_column($arrPermissoesCabecalho, 'entidade'));

$indiceProdutos = array_search('Produtos', array_column($arrPermissoesCabecalho, 'entidade'));

$indiceClientes = array_search('Cliente', array_column($arrPermissoesCabecalho, 'entidade'));

$indiceSituacoesPedido = array_search('SITUACOES_PARA_SEREM_ATRIBUIDAS', array_column($arrPermissoesCabecalho, 'nome'));

$indicePermitirRetornarSituacao = array_search('PERMITIR_RETORNAR_SITUACAO', array_column($arrPermissoesCabecalho, 'nome'));

$indicePermitirModificarValoresPedidos = array_search('MODIFICAR_VALORES_PEDIDO', array_column($arrPermissoesCabecalho, 'nome'));

$arrAcessosMenuAdministracao = [];
foreach ($modulos as $class) {
  // if ($class['modulo'] == 'Assets') {
  //   echo 111;
  //   exit;
  // }
  $indiceAcesso = array_search($class['modulo'], array_column($arrPermissoesCabecalho, 'entidade'));
  if ($arrPermissoesCabecalho[$indiceAcesso]->visualizar == 1) {
    $arrAcessosMenuAdministracao[]  = $class;
  }
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>

  <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/images/favicons/favicon-16x16.png">
  <link rel="manifest" href="/images/favicons/site.webmanifest">
  <link rel="mask-icon" href="/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">


  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>
    <?php echo TITULO; ?>
  </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/bootstrap/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/bootstrap/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css">
  <!-- <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/buttons.dataTables.min.css"> -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.css">
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css">
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/select2/select2.css">
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css">

  <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

  <meta name="title" content="Sistema online para gestão de lojas de Molduras ">
  <meta name="description" content="Nossa solução atende empresas nos segmentos de Molduras, Vidraçarias, Marcenarias, Marmorarias, Serralherias, Escoras de Madeiras e Metálicos e demais segmentos que trabalham com matéria prima de metragem linear.Sistema 100% online, para você cuidar do seu negócio em qualquer lugar. Seja um computador, um tablet ou seu smartphone, conectados a internet.">
  <meta name="keywords" content="sistema de gestão, molduras, lojas de molduras, vidraçaria, marcenaria, serralheria, escoras, sistema online, controle de estoque, molduraria, sistema para moldurarias, sistema para lojas de molduras">
  <meta name="robots" content="index, follow">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="language" content="Portuguese">
  <meta name="author" content="Maestria Sistema">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
    .navbar-default .navbar-toggle .icon-bar {
      background-color: #FFF;
    }

    a {
      cursor: pointer;
    }

    .adicionar-quadro {
      display: none;
    }

    tr.border_bottom td {
      border-bottom: 2pt dashed black !important;
    }

    tr.border_top td {
      border-top: 2pt dashed black !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
      color: #000;
    }

    @media only screen and (max-width : 480px) {

      .dropdown-menu,
      .dropdown {
        font-size: 18px !important;
      }
    }

    .navbar .navbar-brand {
      padding-top: 0px;
    }

    /*	
      .navbar .navbar-brand img {
        height: 120px;
      }
      */

    #cadastrarCliente>.modal-dialog {
      width: 100%;
      max-width: 100%;
    }

    #gerarFinanceiro>.modal-dialog {
      width: 50%;
      max-width: 50%;
    }
  </style>

  <style type="text/css" media="print">
    @media print {

      @page {
        size: landscape;
      }

      body * {
        visibility: hidden;
      }

      #relatorioPedidosEntregue,
      #relatorioPedidosEntregue * {
        visibility: visible;
        border: none;
      }

      table.dadosPedido {
        page-break-inside: avoid;
        break-inside: avoid;
      }
    }

    div.container {
      max-width: 1200px
    }
  </style>

  <style>
    .skin-red-light .main-header .navbar {
      background-color: #c1bfbf;
    }

    .skin-red-light .main-header .navbar .nav>li>a {
      color: #000;
    }

    /*
		.navbar-nav > .user-menu .user-image {
			float: right;
			width: 150px;
			height: 50px;
			border-radius: 0 !Important;
			margin-right: 10px;
			margin-top: -15px;
		}
    */
    /*
		.main-header > .navbar {
			min-height: 100px;
		}
    */

    /*
		@media (min-width: 768px) {
			.navbar-nav > li {
				float: left;
				padding-top: 2px !important;
			}
		}
		
		.navbar-nav {
			margin-top:2em;
		}
		
    */

    /*
    .main-header {
      position: relative;
      max-height: none;
      z-index: 1030;
    }
    */

    @media (max-width: 768px) {
      .col-xs-12.btn {
        margin-top: 0.5em;
      }
    }

    .col-md-1.btn, .col-md-2.btn  {
      margin-right: 0.5em;
    }

    .btn {
      font-size: 12px;
    }

    @media (min-width: 768px) {
      #navbar-collapse {
        padding-top: 2em;
      }

      .main-header {
        max-height: unset;
      }

      .navbar-brand {
      float: left;
      height: 125px;
      padding: 15px 15px;
      padding-top: 15px;
      font-size: 18px;
      line-height: 20px;
    }      
    }

    @media (max-width: 768px) {
      .messages-menu>a.dropdown-toggle {
        font-size: 0.6em;
      }
    }
  </style>


</head>

<body class="hold-transition skin-red-light layout-top-nav">
  <div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">
      <?php include_once(DOCUMENT_ROOT . 'src/View/Common/menu_lateral.php'); ?>

    </header>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <small><i class="fa <?php echo $titulo_principal['icone']; ?>"></i></small>
          <a style="text-decoration:none;" href="<?php echo URL . $classe; ?>/listar/"><?php echo $titulo_principal['descricao']; ?></a>
        </h1>
        <ol class="breadcrumb">
          <?php foreach ($breadcrumb as $titulo => $link) { ?>
            <li class="active"><a href="<?php echo $link; ?>"><?php echo $titulo; ?></a></li>
          <?php } ?>
        </ol>
      </section>