  <?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  

    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<form autocomplete="off"  role="form" method="POST" action="<?php echo URL.'ProdutoAuxiliar/'.$acao.'/'.$produtosAuxiliar[0]->id;?>">
						<?php if (!empty($msg_sucesso)) { ?>
							<div class="callout callout-success">
								<h4>Sucesso!</h4>
								<p><?php echo $msg_sucesso;?></p>
							</div>
						<?php } ?>
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Cadastro</h3>
							</div>						
						</div>
						<div class="box-body">
							<div class="form-group col-xs-12 col-md-3">
								<label>Descrição</label>
								<input class="form-control" type="text" value="<?php echo $produtosAuxiliar[0]->descricao;?>" name="descricao" />
							</div>									
						</div>

            <div class="box-body">
              <div class="form-group col-xs-12 col-md-3">
                <label>Status</label>
                <select name="status" class="form-control">
                  <option value="0" <?php echo $produtosAuxiliar[0]->status == '0' ? 'selected' : '';?>>Inativo</option>
                  <option value="1" <?php echo $produtosAuxiliar[0]->status == '1' ? 'selected' : '';?>>Ativo</option>
                </select>
              </div>
            </div>

            <div class="box-body">
              <div class="form-group col-xs-12 col-md-3">
                <label>Objeto usa metragem?</label>
                <select name="metragem" class="form-control">
                  <option value="0" <?php echo $produtosAuxiliar[0]->metragem == '0' ? 'selected' : '';?>>Não</option>
                  <option value="1" <?php echo $produtosAuxiliar[0]->metragem == '1' ? 'selected' : '';?>>Sim</option>
                </select>
              </div>
            </div>            

            
						<div class="box-footer">
							<button class="btn btn-primary" type="submit">Salvar</button>
							<a class="btn btn-primary" href="<?php echo URL.$classe;?>/listar/">Voltar</a>
						</div>
					</form>
				</div>
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  