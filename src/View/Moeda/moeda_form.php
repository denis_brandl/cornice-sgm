  <?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<form autocomplete="off"  role="form" method="POST" action="<?php echo URL;?>index.php?class=<?php echo $classe;?>&acao=<?php echo $metodo;?>">
						<?php if (!empty($msg_sucesso)) { ?>
							<div class="callout callout-success">
								<h4>Sucesso!</h4>
								<p><?php echo $msg_sucesso;?></p>
							</div>
						<?php } ?>
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Cadastro</h3>
							</div>						
						</div>
						<div class="box-body">
							<div class="form-group">
								<label>Descrição</label>
								<input class="form-control" type="text" value="<?php echo $moedas[0]->DescricaoMoeda;?>" name="DescricaoMoeda" />
							</div>							
							<div class="form-group">
								<label>Sigla</label>
								<input class="form-control" type="text" value="<?php echo $moedas[0]->SiglaMoeda;?>" name="SiglaMoeda" />
							</div>
							<div class="form-group">
								<label>Cotação</label>
								<input class="form-control" type="text" value="<?php echo $moedas[0]->Cotacao;?>" name="Cotacao" />
							</div>	
							
							<div class="form-group">
								<label>Data atualização</label>

								<div class="input-group col-lg-4">
									<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
									</div>
									<?php 
										$DataAtualizacao = new DateTime($moedas[0]->DataAtualizacao);
										$dataAtualizacao = $DataAtualizacao->format('d/m/Y');
									?>
									<input type="text" data-mask="" name="DataAtualizacao" value="<?php echo $dataAtualizacao;?>" data-inputmask="'alias': 'dd/mm/yyyy'" class="form-control">
								</div>
							</div>							
							
						</div>

						<div class="box-footer">
							<input type="hidden" name="handle" value="<?php echo $moedas[0]->CodigoMoeda;?>">
							<button class="btn btn-primary" type="submit">Salvar</button>
							<a class="btn btn-primary" href="<?php echo URL.$classe;?>/listar/">Voltar</a>
						</div>
					</form>
				</div>
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  