<!DOCTYPE html>
<html>
<head>
  <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/images/favicons/favicon-16x16.png">
  <link rel="manifest" href="/images/favicons/site.webmanifest">
  <link rel="mask-icon" href="/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">  
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo TITULO;?></title>

  <meta name="title" content="Sistema online para gestão de lojas de Molduras ">
  <meta name="description" content="Nossa solução atende empresas nos segmentos de Molduras, Vidraçarias, Marcenarias, Marmorarias, Serralherias, Escoras de Madeiras e Metálicos e demais segmentos que trabalham com matéria prima de metragem linear.Sistema 100% online, para você cuidar do seu negócio em qualquer lugar. Seja um computador, um tablet ou seu smartphone, conectados a internet.">
  <meta name="keywords" content="sistema de gestão, molduras, lojas de molduras, vidraçaria, marcenaria, serralheria, escoras, sistema online, controle de estoque, molduraria, sistema para moldurarias, sistema para lojas de molduras">
  <meta name="robots" content="index, follow">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="language" content="Portuguese">
  <meta name="author" content="Maestria Sistema">

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?php echo URL;?>vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/bootstrap/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo URL; ?>vendor/almasaeed2010/adminlte/bootstrap/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo URL;?>vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo URL;?>vendor/almasaeed2010/adminlte/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo URL;?>">
		<img src="<?php echo URL;?>images/maestria-sistema.png" /> <br>
    
		<h4>Sistema para molduras</h4>
		<?php if ($parceria_ruberti) { ?>
		<h4>Parceria exclusiva Ruberti Molduras</h4>
		<?php } ?>
		
    </a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Logue para iniciar sua sessão</p>

    <form autocomplete="off"  action="<?php echo URL;?>Usuario/logar/" method="post">
		<?php if (!empty($msg_error)) { ?>
			<div class="callout callout-danger">
				<p><?php echo $msg_error;?></p>
			</div>
		<?php } ?>    
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="usuario" autofocus placeholder="Usuário">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" id="senha" class="form-control" name="senha" placeholder="Senha">
        <i class="fa fa-lock form-control-feedback"></i>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Mostrar senha
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
			<?php /* <input type="hidden" id="token" name="token" value="<?php echo $token; ?>" /> */ ?>
          <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <!-- /.social-auth-links -->

    <?php /* <a href="#">Esqueci minha senha</a><br> */ ?>

  </div>
  <!-- /.login-box-body -->
</div>

<div class="login-box">
  <p>Dúvidas ou suporte, acesse <a target="_blanK" href="https://maestriasistema.com.br/">maestriasistema.com.br</a></p>
</div>
<!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="<?php echo URL;?>vendor/almasaeed2010/adminlte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo URL;?>vendor/almasaeed2010/adminlte/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo URL;?>vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
    
  });

  $('input').on('ifClicked', function(event){
      var x = document.getElementById("senha");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
  });   

</script>
</body>
</html>
