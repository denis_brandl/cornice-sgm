<?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>


<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"></h3>
				</div>
				<div class="box-body">
					<div id="componentes-cadastrados" class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-6">
								<?php if ($usuario_master) { ?>
									<a class="btn btn-primary" href="<?php echo URL . $classe . '/cadastrar/'; ?>">Cadastrar
									Novo</a>
								<?php } ?>
							</div>
							<div class="col-sm-6">
								<div id="example1_filter" class="dataTables_filter">
									<form autocomplete="off" name="frmCliente" id="frmBuscar">
										<label>
											Buscar:
											<select name="coluna" class="form-control">
												<?php foreach ($arrCamposBusca as $value => $descricao) {
													$selected = '';
													if ($value == $coluna) {
														$selected = 'selected';
													}
													?>
													<option value="<?php echo $value; ?>" <?php echo $selected; ?>><?php echo $descricao; ?></option>
												<?php } ?>
											</select>
											<input class="form-control input-sm" type="search" placeholder=""
												value="<?php echo $buscar; ?>" name="buscar" aria-controls="example1">
											<input type="button" class="btn btn-primary" id="btnBuscar"
												value="Buscar" />
										</label>
									</form>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<table id="example1" class="table table-bordered table-striped dataTable" role="grid"
									aria-describedby="example1_info">
									<thead>
										<tr role="row">
											<th>Nome</th>
											<th>Login</th>
											<th>Grupo</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$aux = 0;
										foreach ($usuarios as $usuario) {
											if ($aux & 1) {
												?>
												<tr class="odd" role="row">
												<?php } else { ?>
												<tr class="even" role="row">
												<?php } ?>
												<td class="sorting_1">
													<?php echo $usuario->NomeUsuario; ?>
												</td>
												<td class="sorting_1">
													<?php echo $usuario->login; ?>
												</td>
												<td class="sorting_1">
													<?php echo $usuario->grupo_usuario; ?>
												</td>
												<td><a class=""
														href="<?php echo URL . $classe . '/editar/' . $usuario->CodigoUsuario; ?>"><i
															class="fa fa-edit"></i>Editar</a></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer clearfix">
					<ul class="pagination pagination-sm no-margin pull-right">

						<li><a class='box-navegacao <?= $exibir_botao_inicio ?>'
								href="<?php echo URL;?><?php echo $classe; ?>/listar/0/pagina=<?= $primeira_pagina ?>"
								title="Primeira Página">Primeira</a></li>
						<li><a class='box-navegacao <?= $exibir_botao_inicio ?>'
								href="<?php echo URL;?><?php echo $classe; ?>/listar/0/pagina=<?= $pagina_anterior ?>"
								title="Página Anterior">Anterior</a></li>

						<?php
						/* Loop para montar a páginação central com os números */
						for ($i = $range_inicial; $i <= $range_final; $i++) {
							$destaque = ($i == $pagina_atual) ? 'active' : '';
							?>
							<li class='box-numero <?= $destaque ?>'><a
									href="<?php echo URL;?><?php echo $classe; ?>/listar/0/pagina=<?= $i ?>"><?= $i ?></a></li>
						<?php } ?>
						<li><a href="<?php echo URL;?><?php echo $classe; ?>/listar/0/pagina=<?= $proxima_pagina ?>"
								title="Próxima Página">Próxima</a></li>
						<li><a href="<?php echo URL;?><?php echo $classe; ?>/listar/0/pagina=<?= $ultima_pagina ?>"
								title="Última Página">Último</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>