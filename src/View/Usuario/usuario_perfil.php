  <?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <form autocomplete="off" role="form" method="POST" action="<?php echo URL; ?><?php echo $classe; ?>/<?php echo $metodo; ?>/">
            <?php if (!empty($msg_sucesso)) { ?>
              <div class="callout <?php echo $sucesso ? 'callout-success' : 'callout-danger';?>">
                <h4><?php echo $sucesso ? 'Sucesso!' : 'Erro!';?></h4>
                <p><?php echo $msg_sucesso; ?></p>
              </div>
            <?php } ?>
            <div class="box-body">
              <div class="form-group col-xs-12 col-md-6">
                <label>Nome Completo</label>
                <input class="form-control col-sm-6" type="text" value="<?php echo $usuarios[0]->NomeUsuario; ?>" name="NomeUsuario" />
              </div>
            </div>            
            <div class="box-body">
              <div class="form-group col-xs-12 col-md-3">
                <label>Senha</label>
                <input class="form-control col-sm-6" type="password" value="" name="SenhaUsuario" />
                <p>Para <strong>manter a senha atual</strong>, deixe as senhas em branco</p>
              </div>
              <div class="form-group col-xs-12 col-md-3">
                <label>Repita a senha</label>
                <input class="form-control col-sm-6" type="password" value="" name="SenhaUsuario2" />
              </div>
            </div>
            
            <div class="box-footer">
              <button class="btn btn-primary" type="submit">Salvar</button>
            </div>

          </form>
        </div>

      </div>
    </div>
  </section>
  <!-- /.content-wrapper -->
  </div>
  <?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>
