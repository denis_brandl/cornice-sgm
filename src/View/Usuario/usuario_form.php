  <?php include_once(DOCUMENT_ROOT . 'src/View/Common/cabecalho.php'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">


        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="pill" href="#geral">Geral</a></li>
          <?php if ((int) $usuarios[0]->CodigoUsuario != 0) {  ?>
            <li><a data-toggle="pill" href="#historicoAcessos">Histórico de acessos</a></li>
          <?php } ?>
        </ul>

        <div class="tab-content">
          <!-- ABA GERAL -->
          <div id="geral" class="tab-pane fade in active">
            <div class="box">
              <form autocomplete="off" role="form" method="POST" action="<?php echo URL; ?><?php echo $classe; ?>/<?php echo $metodo; ?>/<?php echo (int) $usuarios[0]->CodigoUsuario; ?>">
                <?php if (!empty($msg_sucesso)) { ?>
                  <div class="callout callout-success">
                    <h4>Sucesso!</h4>
                    <p><?php echo $msg_sucesso; ?></p>
                  </div>
                <?php } ?>
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Cadastro</h3>
                  </div>
                </div>
                <div class="box-body">
                  <div class="form-group col-xs-12 col-md-6">
                    <label>Usuário</label>
                    <p class="small">Usado para fazer o acesso ao sistema</p>
                    <input class="form-control col-sm-6" type="text" value="<?php echo $usuarios[0]->login; ?>" name="login" />
                  </div>
                </div>
                <div class="box-body">
                  <div class="form-group col-xs-12 col-md-6">
                    <label>Nome Completo</label>
                    <input class="form-control col-sm-6" type="text" value="<?php echo $usuarios[0]->NomeUsuario; ?>" name="NomeUsuario" />
                  </div>
                </div>
                <div class="box-body">
                  <div class="form-group col-xs-12 col-md-6">
                    <label>Senha</label>
                    <input class="form-control col-sm-6" type="password" value="" name="SenhaUsuario" />
                  </div>
                </div>

                <div class="box-body">
                  <div class="form-group col-xs-12 col-md-2">
                    <label>Usuário também é vendedor?</label>
                    <select name="usuario_vendedor" class="form-control tipoCliente">
                      <option value="1" <?php echo $usuarios[0]->usuario_vendedor == 1 ? 'selected' : ''; ?>>Sim</option>
                      <option value="0" <?php echo $usuarios[0]->usuario_vendedor == 0 ? 'selected' : ''; ?>>Não</option>
                    </select>
                  </div>
                  <div class="form-group col-xs-12 col-md-3">
                    <label>Comissão sobre vendas</label>
                    <div class="input-group">
                      <input class="form-control col-sm-6 numberOnly campoMonetario" type="text" value="<?php echo number_format($usuarios[0]->percentual_comissao, 2, ",", ""); ?>" name="comissao" />
                      <span class="input-group-addon">%</span>
                    </div>
                  </div>
                </div>

                <div class="box-body">
                  <div class="form-group col-xs-12 col-md-12">
                    <label>Grupo</label>
                    <div class="input-group">
                      <select class="form-control" name="id_grupo_usuario" id="id_grupo_usuario">
                        <option value="0">Selecione</option>
                        <?php foreach ($arrListaGruposUsuario as $grupo_usuario) { ?>
                          <option <?php echo $grupo_usuario->id_grupo == $usuarios[0]->id_grupo_usuario ? 'selected' : ''; ?> value="<?php echo $grupo_usuario->id_grupo; ?>"><?php echo $grupo_usuario->descricao; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>

                <?php if ($usuario_master) { ?>
                  <div class="box-body">
                    <div class="form-group col-xs-12 col-md-12">
                      <label>Situação</label>
                      <div class="input-group">
                        <select class="form-control" name="situacao" id="situacao">
                          <option value="0" <?php echo $usuarios[0]->situacao == '0' ? 'selected="selected"' : ''; ?>>Inativo</option>
                          <option value="1" <?php echo $usuarios[0]->situacao == '1' ? 'selected="selected"' : ''; ?>>Ativo</option>
                        </select>
                      </div>
                    </div>
                  </div>
                <?php } ?>

                <div class="box-body">
                  <div class="form-group col-xs-12 col-md-12">
                    <label>Empresas com acesso</label>
                    <p class="small">Se nenhuma empresa for selecionada, o usuário terá acesso a todas </p>
                    <?php foreach ($arrDemaisEmpresas as $empresa_matriz) {

                      $checked = in_array($empresa_matriz->CodigoEmpresa, $permissoesEmpresaUsuario) ? 'checked' : '';
                    ?>
                      <div class="checkbox ">
                        <label>
                          <input type="checkbox" <?php echo $checked; ?> value="<?php echo $empresa_matriz->CodigoEmpresa; ?>" name="permissaoEmpresa[]"><?php echo $empresa_matriz->RazaoSocial; ?>
                        </label>
                      </div>
                    <?php } ?>
                  </div>
                </div>

                <div class="box-footer">
                  <button class="btn btn-primary" type="submit">Salvar</button>
                  <a class="btn btn-primary" href="<?php echo URL; ?><?php echo $classe; ?>/listar/">Voltar</a>
                </div>

              </form>
            </div>
          </div>
          <!-- FIM ABA GERAL -->

          <!-- ABA HISTORICO ACESSOS -->
          <div id="historicoAcessos" class="tab-pane fade">
            <div class="box">
              <div class="box-body">
                <table id="listaAcessos" class="table table-bordered table-striped dataTable" role="grid">
                  <thead>
                    <th>Data</th>
                    <th>Acesso</th>
                  </thead>
                  <tbody>
                    <?php
                    if (is_array($arrAcessos)) {
                      foreach ($arrAcessos as $acesso) {
                        $DataAcesso = new DateTime($acesso->data);
                        $dataAcesso = $DataAcesso->format('d/m/Y H:i:s');
                        // $dataAcesso = $acesso->data
                    ?>
                        <tr>
                          <td data-order="<?php echo $acesso->data; ?>"><?php echo $dataAcesso; ?></td>
                          <td data-order="<?php echo $acesso->sucesso; ?>"><?php echo (int) $acesso->sucesso === 1 ? 'Sucesso' : 'Falha'; ?></td>
                        </tr>
                    <?php
                      }
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- FIM ABA HISTORICO ACESSOS -->
        </div>
      </div>
    </div>
  </section>
  <!-- /.content-wrapper -->
  </div>

  <?php include_once(DOCUMENT_ROOT . 'src/View/Common/rodape.php'); ?>