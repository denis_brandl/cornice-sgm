  <?php include_once(DOCUMENT_ROOT.'src/View/Common/cabecalho.php'); ?>  
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<form autocomplete="off"  role="form" method="POST" action="<?php echo URL.$classe.'/'.$metodo.'/'.$bancos[0]->CodigoBanco;?>">
						<?php if (!empty($msg_sucesso)) { ?>
							<div class="callout callout-success">
								<h4>Sucesso!</h4>
								<p><?php echo $msg_sucesso;?></p>
							</div>
						<?php } ?>
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Cadastro</h3>
							</div>						
						</div>
						<div class="box-body">
							<div class="form-group">
								<label>Razão Social</label>
								<input class="form-control" type="text" value="<?php echo $bancos[0]->RazaoSocial;?>" name="RazaoSocial" />
							</div>							
							<div class="form-group">
								<label>Agência</label>
								<input class="form-control" type="text" value="<?php echo $bancos[0]->Agencia;?>" name="Agencia" />
							</div>
							<div class="form-group">
								<label>Conta Corrente</label>
								<input class="form-control" type="text" value="<?php echo $bancos[0]->ContaCorrente;?>" name="ContaCorrente" />
							</div>							
						</div>

						<div class="box-footer">
							<input type="hidden" name="handle" value="<?php echo $bancos[0]->CodigoBanco;?>">
							<button class="btn btn-primary" type="submit">Salvar</button>
							<a class="btn btn-primary" href="<?php echo URL.$classe.'/listar/'?>">Voltar</a>
						</div>
					</form>
				</div>
			</div>
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <?php include_once(DOCUMENT_ROOT.'src/View/Common/rodape.php'); ?>  