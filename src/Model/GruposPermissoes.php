<?php
require_once('conexao.php');
require_once('bd.php');
class GruposPermissoes
{
    public $id_grupo_permissao;
    public $id_grupo;
    public $id_permissao;
    public $cadastrar;
    public $editar;
    public $excluir;
    public $visualizar;
    public $conteudo;
    public $nom_tabela = "grupos_permissoes";
    public function __construct()
    {
        $id_grupo_permissao = 0;
        $id_grupo = 0;
        $id_permissao = 0;
        $cadastrar = 0;
        $editar = 0;
        $excluir = 0;
        $visualizar = 0;
        $conteudo = '';
    }
    public function listarTodos($pagina_atual = 0, $linha_inicial = 0, $coluna = '', $buscar = '')
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $where = '';
        if ($coluna != '' && $buscar != '') {
            $where = sprintf(' WHERE %s LIKE UPPER("%%%s%%") ', $coluna, strtoupper($buscar));
        }
        $paginacao = ' LIMIT ' . QTDE_REGISTROS;
        if ($pagina_atual > 0 && $linha_inicial > 0) {
            $paginacao = " LIMIT {$linha_inicial}, " . QTDE_REGISTROS;
        }
        $sql = "SELECT * FROM " . $this->nom_tabela . $where . $paginacao;
        $dados = $crud->getSQLGeneric($sql);
        return $dados;
    }
    public function listar($handle)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $sql = "SELECT * FROM " . $this->nom_tabela . " WHERE id_grupo_permissao = ?";
        $arrayParam = array($handle);
        $dados = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
        return $dados;
    }


    public function listarPorGrupo($handle, $entidade = '')
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);

        $where_entidade = '';
        if ($entidade != '') {
            $where_entidade = ' AND p.entidade = "'.$entidade.'"';
        }
        $sql = "
            SELECT
                *,
                IF (grupos_permissoes.conteudo is not null, grupos_permissoes.conteudo, '') as conteudo
            FROM "
            . $this->nom_tabela .
            " INNER JOIN permissoes p ON (grupos_permissoes.id_permissao = p.id_permissao) 
            WHERE
                id_grupo = ? $where_entidade
            ORDER BY p.entidade ASC, p.permissao_complementar ASC, p.descricao ASC ";
        $arrayParam = array($handle);
        // echo "<pre>$sql</pre>";exit;
        $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

        // print_r($dados);exit;
        return $dados;
    }

    public function listarTodosTotal()
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela;
        $dados = $crud->getSQLGeneric($sql, null, FALSE);
        return $dados->total_registros;
    }
    public function editar($post)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $crudGrupoUsuario = bd::getInstance($pdo, 'grupos_usuario');

        $arrEditarGrupoUsuario = ['descricao' => $post['descricao'], 'situacao' => $post['situacao']];
        unset($post['descricao'], $post['situacao']);

        $crudGrupoUsuario->update($arrEditarGrupoUsuario, ["id_grupo=" => $post["handle"]]);


        $arrayCond = array("id_grupo=" => $post["handle"]);
        $crud->update([
            'cadastrar' => 0,
            'editar' => 0,
            'excluir' => 0,
            'visualizar' => 0,
            'conteudo' => ''
        ], $arrayCond);

        $arrEditar = array();
        foreach ($post['permissoes'] as $key => $value) {
            $arrayCond = ['id_permissao=' => $key, "id_grupo=" => $post["handle"]];
            if (isset($value['situacoes_atribuir'])) {
                $arrEditar = ['conteudo' => sprintf('%s', implode(',',$value['situacoes_atribuir']))];
            } else {
                $arrEditar = array_merge($value);
            }
            $crud->update($arrEditar, $arrayCond);
        }


        return true;
    }
    public function cadastrar($post)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $crudGrupoUsuario = bd::getInstance($pdo, 'grupos_usuario');

        $arrEditarGrupoUsuario = ['descricao' => $post['descricao'], 'situacao' => $post['situacao']];
        unset($post['descricao'], $post['situacao']);

        $handleGrupoUsuario = $crudGrupoUsuario->insert($arrEditarGrupoUsuario);


        $arrayCond = array("id_grupo=" => $handleGrupoUsuario);
        
        $crud->insert([
            'cadastrar' => 0,
            'editar' => 0,
            'excluir' => 0,
            'visualizar' => 0,
        ], $arrayCond);

        $sql = '
            INSERT INTO grupos_permissoes
                (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
            SELECT
                '.$handleGrupoUsuario.' as id_grupo,
                id_permissao,
                0 as cadastrar,
                0 as editar,
                0 as excluir,
                0 as visualizar
            FROM
                permissoes
        ';

        $crud->getSQLGeneric($sql);

        if (!isset($post['permissoes'])) {
            return $handleGrupoUsuario;
        }
        $arrEditar = array();
        foreach ($post['permissoes'] as $key => $value) {
            $arrayCond = ['id_permissao=' => $key, "id_grupo=" => $handleGrupoUsuario];
            $arrEditar = array_merge($value);
            $crud->update($arrEditar, $arrayCond);
        }
        
        return $handleGrupoUsuario;
    }
    public function excluir($handle)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $crud->delete(array("id_grupo_permissao" => $handle));
        return true;
    }
}
