<?php
class Permissoes
{
    public $id_permissao;
    public $entidade;
    public $descricao;
    public $cadastrar;
    public $editar;
    public $excluir;
    public $visualizar;
    public $nom_tabela = "permissoes";
    public function __construct()
    {
        $id_permissao = 0;
        $entidade = "";
        $descricao = "";
        $cadastrar = 0;
        $editar = 0;
        $excluir = 0;
        $visualizar = 0;
    }
    public function listarTodos($pagina_atual = 0, $linha_inicial = 0, $coluna = '', $buscar = '')
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $where = '';
        if ($coluna != '' && $buscar != '') {
            $where = sprintf(' WHERE %s LIKE UPPER("%%%s%%") ', $coluna, strtoupper($buscar));
        }
        
        $sql = "SELECT * FROM " . $this->nom_tabela . $where . " ORDER BY descricao ASC";
        $dados = $crud->getSQLGeneric($sql);
        return $dados;
    }
    public function listar($handle)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $sql = "SELECT * FROM " . $this->nom_tabela . " WHERE id_permissao = ?";
        $arrayParam = array($handle);
        $dados = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
        return $dados;
    }
    public function listarTodosTotal()
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela;
        $dados = $crud->getSQLGeneric($sql, null, FALSE);
        return $dados->total_registros;
    }
    public function editar($post)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $arrEditar = array();
        foreach ($post as $key => $value) {
            if ($key != "handle" && $key != "data_criacao") {
                $arrEditar[$key] =  $value;
            }
        }
        $arrayCond = array("id_permissao=" => $post["handle"]);
        $retorno   = $crud->update($arrEditar, $arrayCond);
        return $retorno;
    }
    public function cadastrar($post)
    {
        $pdo = Conexao::getInstance();
        $arrInserir = array();
        foreach ($post as $key => $value) {
            if ($key != "handle") {
                $arrInserir[$key] =  $value;
            }
        }
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $retorno   = $crud->insert($arrInserir);
        return $retorno;
    }
    public function excluir($handle)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $crud->delete(array("id_permissao" => $handle));
        return true;
    }
}
