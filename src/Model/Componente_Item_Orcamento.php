<?php
require_once('conexao.php');
require_once('bd.php');
class Componente_Item_Orcamento {

	public $cd_orcamento;
	public $item_orcamento;
	public $sequencia;
	public $cd_componente;
	public $valor_unitario;
	public $alterar_valor;
	public $id_componente_item_orcamento;
	private $nom_tabela = 'COMPONENTES_ITEM_ORCAMENTO';
	public $quantidade;
	private $order_by_default = 'Sequencia ASC, Cd_Item_Orcamento ASC';		
	
	public function __construct() {
		$cd_orcamento = "";
		$item_orcamento = "";
		$sequencia = "";
		$cd_componente = "";
		$valor_unitario = "";
		$alterar_valor = "";
		$id_componente_item_orcamento = 0;
		$quantidade = 0;
	}
	
	public function listarComponenteItemOrcamento($Cd_Item_Orcamento,$Cd_Orcamento, $codigo_uf = 0) {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$sql = "SELECT 
					p.CodigoProduto,
					p.DescricaoProduto,
					p.CodigoProdutoFabricante,
					p.NovoCodigo,
					p.codigo_ncm,
					p.codigo_cest,
					p.UnidadeProduto,
					UnidadesMedida.DescricaoUnidadeMedida,
					t.csosn AS csosn,
					t.cfop AS cfop,
					t.csticms AS cst,
					t.icms as aliquotaIcms,
					pis.cstpis,
					pis.pis as aliquotaPis,
					confins.cstconfins,
					confins.confins as aliquotaConfins,
					cio.VALOR_UNITARIO as Vl_Unitario,
					cio.quantidade as Qt_Item
				FROM 
					COMPONENTES_ITEM_ORCAMENTO cio
					INNER JOIN Produtos p ON (cio.CD_COMPONENTE = p.CodigoProduto AND p.componente = 1)
					LEFT JOIN tributacao t ON p.id_grupo_tributario = t.id_grupo_tributario AND t.id_estado = $codigo_uf
					LEFT JOIN grupo_tributario gt ON t.id_grupo_tributario = gt.id_grupo_tributario AND p.id_grupo_tributario = t.id_grupo_tributario
					LEFT JOIN operacao_fiscal of2 ON t.id_operacao_fiscal = of2.id_operacao_fiscal
					LEFT JOIN pis ON pis.id_tributacao = t.id_tributacao
					LEFT JOIN confins ON confins.id_tributacao = t.id_tributacao
					LEFT JOIN UnidadesMedida ON p.UnidadeProduto = UnidadesMedida.CodigoUnidadeMedida
				WHERE 
					cio.ITEM_ORCAMENTO = ? 
					AND cio.CD_ORCAMENTO = ?
				 ORDER BY Sequencia ASC";
					
		$arrayParam = array($Cd_Item_Orcamento,$Cd_Orcamento); 
		
		$dados = $crud->getSQLGeneric($sql,$arrayParam, TRUE);
		
		return $dados;
	}
}
?>

