<?php
require_once('conexao.php');
require_once('bd.php');
class Logradouro {
	public $idLogradouro = 0;
	public $descricao = '';
	public $status = 1;
	
	public $nom_tabela = 'logradouros';

	
	public function __construct() {
		$this->idLogradouro = 0;
		$this->descricao = '';
		$this->status = 1;
	}
	
	public function listarTodos($pagina_atual = 0,$linha_inicial = 0,$coluna = '',$buscar = '', $quantidade = '', $ordem = '') {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$where = '';
		
		if (!empty($coluna) && (!empty($buscar)) ) {
			$where = sprintf(' WHERE %s = "%s" ',$coluna,$buscar);
			if ($coluna !== 'idLogradouro') {
				$where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ',$coluna,strtoupper($buscar));
			}
		}
		
		
		if ($ordem == '') {
            $ordem = 'descricao ASC';
		}
		
		$sql = "SELECT *, IF (status = 1, 'Ativo', 'Inativo') as statusDescricao FROM ".$this->nom_tabela.$where." ORDER BY ".$ordem;
		
		// echo $sql;exit;
		
		$dados = $crud->getSQLGeneric($sql);
		return $dados;
		
		//
	}

	public function listarTodosTotal($coluna = '',$buscar = '') {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$where = '';
		
		if (!empty($coluna) && (!empty($buscar) || $buscar >= 0) ) {
			$where = sprintf(' WHERE %s = "%s" ',$coluna,$buscar);
			if ($coluna !== 'idLogradouro') {
				$where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ',$coluna,strtoupper($buscar));
			}
		}		
		
		$sql = "SELECT count(*) as total_registros FROM ".$this->nom_tabela.$where;		
		
		$dados = $crud->getSQLGeneric($sql,null,FALSE);		
		
		return $dados->total_registros;
		
		//
	}	
	
	public function listarClienteGrupo($handle) {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$sql = "SELECT *, IF (status = 1, 'Ativo', 'Inativo') as statusDescricao FROM ".$this->nom_tabela." WHERE idLogradouro = ?";
    
		$arrayParam = array($handle); 
		
		$dados = $crud->getSQLGeneric($sql,$arrayParam, TRUE);
		
		return $dados;
		
		//
	}	
	
	public function editarClienteGrupo($post) {
		$pdo = Conexao::getInstance();
		
		$arrayCliente = array();
		foreach ($post as $key => $value) {
			if ($key != 'handle' && $key != 'idLogradouro') {
				$arrayCliente[$key] =  $value;
			}
		}
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$arrayCond = array('idLogradouro=' => $post['handle']);  
		$retorno   = $crud->update($arrayCliente, $arrayCond);  		
		
		return $retorno;
	}
	
	public function cadastrarClienteGrupo($post) {
		$pdo = Conexao::getInstance();
		
		$arrayCliente = array();
		foreach ($post as $key => $value) {
			if ($key != 'handle' && $key != 'idLogradouro') {
				$arrayCliente[$key] =  $value;
			}
		}		
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);

		$retorno   = $crud->insert($arrayCliente);  		
		
		return $retorno;
	}

	public function excluir($handle) {
		$pdo = Conexao::getInstance();
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		$crud->delete(array('idLogradouro' => $handle));
	}	
}
