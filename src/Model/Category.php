<?php
class Category
{
    public $CategoryId;
    public $UserId;
    public $CategoryName;
    public $Level;
    public $IDCONTA;
    public $TIPO;
    public $idgeral;
    public $complemento;

    public $nom_tabela = "category";
    public function __construct()
    {
        $this->CategoryId = 0;
        $this->UserId = 0;
        $this->CategoryName = "";
        $this->Level = 0;
        $this->IDCONTA = 0;
        $this->TIPO = 0;
        $this->idgeral = 0;
        $this->complemento = "";
    }

    public function listarTodos(
        $pagina_atual = 0,
        $linha_inicial = 0,
        $coluna = "",
        $buscar = ""
    ) {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $where = "";
        if ($coluna != "" && $buscar != "") {
            $where = sprintf(
                ' WHERE %s LIKE UPPER("%%%s%%") ',
                $coluna,
                strtoupper($buscar)
            );
        }

        $paginacao = " LIMIT " . QTDE_REGISTROS;
        if ($pagina_atual > 0 && $linha_inicial > 0) {
            $paginacao = " LIMIT {$linha_inicial}, " . QTDE_REGISTROS;
        }

        $sql = "SELECT * FROM " . $this->nom_tabela . $where . $paginacao;
        $dados = $crud->getSQLGeneric($sql);

        return $dados;
    }

    public function listarPorTipo(
        $arrFiltro = []
    ) {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $where = "";
        
        $sql = "SELECT
          CategoryId,
          CategoryName
        FROM
          ".$this->nom_tabela."
          INNER JOIN contas ON category.IDCONTA = contas.id
        WHERE
          contas.idtipo = " . $arrFiltro['tipo'];
        $dados = $crud->getSQLGeneric($sql);

        return $dados;
    }    

    public function listar($handle)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $sql = "SELECT * FROM " . $this->nom_tabela . " WHERE CategoryId = ?";
        $arrayParam = [$handle];
        $dados = $crud->getSQLGeneric($sql, $arrayParam, false);
        return $dados;
    }

    public function listarTodosTotal()
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela;
        $dados = $crud->getSQLGeneric($sql, null, false);
        return $dados->total_registros;
    }

    public function editar($post)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);

        $arrEditar = [];
        foreach ($post as $key => $value) {
            if ($key != "handle" && $key != "data_criacao") {
                $arrEditar[$key] = $value;
            }
        }

        $arrayCond = ["CategoryId=" => $post["handle"]];
        $retorno = $crud->update($arrEditar, $arrayCond);
        return $retorno;
    }

    public function cadastrar($post)
    {
        $pdo = Conexao::getInstance();
        $arrInserir = [];
        foreach ($post as $key => $value) {
            if ($key != "handle") {
                $arrInserir[$key] = $value;
            }
        }
        $arrInserir = array_merge($arrInserir, [
            "Level" => 0,
            "Tipo" => 0,
            "idgeral" => 1,
        ]);
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $retorno = $crud->insert($arrInserir);
        return $retorno;
    }

    public function excluir($handle)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $crud->delete(["CategoryId" => $handle]);
        return true;
    }
}