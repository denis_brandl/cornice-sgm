<?php
require_once('conexao.php');
require_once('bd.php');
class GruposUsuario {

	public $id_grupo;
	public $descricao;
  public $situacao;
	public $nom_tabela = 'grupos_usuario';
	private $order_by_default = 'descricao asc';

	
	public function __construct() {
		$this->id_grupo = 0;
		$this->descricao = '';
    $this->situacao = 1;
	}
	
	public function listarTodos($pagina_atual = 0,$linha_inicial = 0,$coluna = '',$buscar = '') {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$where = '';
		if ($coluna != '' && $buscar != '') {
			$where = sprintf(' WHERE %s LIKE UPPER("%%%s%%") ',$coluna,strtoupper($buscar));
		}
		
		$paginacao = ' LIMIT '.QTDE_REGISTROS;
		if ($pagina_atual > 0 && $linha_inicial > 0) {
			$paginacao = " LIMIT {$linha_inicial}, ".QTDE_REGISTROS;
		}		
		
		$sql = "SELECT * FROM ".$this->nom_tabela.$where." ORDER BY ".$this->order_by_default.$paginacao;		
		$dados = $crud->getSQLGeneric($sql);
		
		return $dados;
		
		//
	}

	public function listarTodosTotal() {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$sql = "SELECT count(*) as total_registros FROM ".$this->nom_tabela;		
		
		$dados = $crud->getSQLGeneric($sql,null,FALSE);		
		
		return $dados->total_registros;
		
		//
	}	
	
	public function listarGrupoUsuario($handle) {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$sql = "SELECT * FROM ".$this->nom_tabela." WHERE id_grupo = ?";
		$arrayParam = array($handle); 
		
		$dados = $crud->getSQLGeneric($sql,$arrayParam, FALSE);
		
		return $dados;
		
		//
	}	
	
	public function editarGrupoUsuario($post) {
		$pdo = Conexao::getInstance();
		
		$arrayGrupoUsuario = array();
		foreach ($post as $key => $value) {
			if ($key != 'handle' && $key != 'id_grupo')
				$arrayGrupoUsuario[$key] =  $value;
		}
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$arrayCond = array('id_grupo=' => $post['handle']);  
		$retorno   = $crud->update($arrayGrupoUsuario, $arrayCond);  		
		
		return $retorno;
	}
	
	public function cadastrarGrupoUsuario($post) {
		$pdo = Conexao::getInstance();
		
		$arrayGrupoUsuario = array();
		foreach ($post as $key => $value) {
			if ($key != 'handle' && $key != 'id_grupo')
				$arrayGrupoUsuario[$key] =  $value;
		}		
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);

		$retorno   = $crud->insert($arrayGrupoUsuario);  		
		
		return $retorno;
		exit;
	}	
}
?>
