<?php
require_once('conexao.php');
require_once('bd.php');
include_once(__DIR__ . '/../Controller/CommonController.php');
class Empresas {

	public $CodigoEmpresa;
	public $RazaoSocial;
	public $NomeFantasia;
	public $Endereco;
	public $Complemento;
	public $Bairro;
	public $CEP;
	public $Cidade;
	public $Estado;
	public $CGC;
	public $InscricaoEstadual;
	public $Telefone1;
	public $Telefone2;
	public $Ramal;
	public $Fax;
	public $EMail;
	public $Contato;
	public $horario_funcionamento;
	public $observacao_pedido;
	public $DataCadastro;
	public $status;
	public $padrao;
	public $filial;
	public $matriz;
	public $codigo_crt;
	public $token_integra_nota;
	public $ambiente_gerar_nota;
	public $numero_sequencia_nf;
	public $numero_sequencia_nfs;
	public $numero_sequencia_nfc;

	public $serie_nf;
	public $serie_nfs;
	public $serie_nfc;

	public $nom_tabela = 'Empresas';
	private $order_by_default = 'Matriz DESC, padrao DESC';

	
	public function __construct() {
		$CodigoEmpresa = '';
		$RazaoSocial = '';
		$NomeFantasia = '';
		$Endereco = '';
		$Complemento = '';
		$Bairro = '';
		$CEP = '';
		$Cidade = '';
		$Estado = '';
		$CGC = '';
		$InscricaoEstadual = '';
		$Telefone1 = '';
		$Telefone2 = '';
		$Ramal = '';
		$Fax = '';
		$EMail = '';
		$Contato = '';
		$horario_funcionamento = '';
		$observacao_pedido = '';
		$DataCadastro = date('Y-m-d H:i:s');

		$this->status = 1;
		$this->padrao = 0;
		$this->filial = 0;
		$this->matriz = 0;
		$this->codigo_crt = 0;
		$this->token_integra_nota = '';
		$this->ambiente_gerar_nota = 2;

		$this->numero_sequencia_nf = 0;
		$this->numero_sequencia_nfs = 0;
		$this->numero_sequencia_nfc = 0;

		$this->serie_nf = '';
		$this->serie_nfs = '';
		$this->serie_nfc = '';
	}
	
	public function listarTodos($pagina_atual = 0,$linha_inicial = 0,$coluna = '',$buscar = '') {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$where = '';
		if ($coluna != '' && $buscar != '') {
			$where = sprintf(' WHERE %s LIKE UPPER("%%%s%%") ',$coluna,strtoupper($buscar));
		}
		
		$paginacao = ' LIMIT '.QTDE_REGISTROS;
		if ($pagina_atual > 0 && $linha_inicial > 0) {
			$paginacao = " LIMIT {$linha_inicial}, ".QTDE_REGISTROS;
		}		
		
		$sql = "SELECT * FROM ".$this->nom_tabela.$where." ORDER BY ".$this->order_by_default.$paginacao;		
		$dados = $crud->getSQLGeneric($sql);
		// echo $sql;exit;
		return $dados;
		
		//
	}

	public function listarTodosTotal() {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$sql = "SELECT count(*) as total_registros FROM ".$this->nom_tabela;		
		
		$dados = $crud->getSQLGeneric($sql,null,FALSE);		
		
		return $dados->total_registros;
		
		//
	}	
	
	public function ListarEmpresa($handle) {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$sql = "
				SELECT
					" . $this->nom_tabela . ".*,
					estados.codigo_uf
				FROM "
					. $this->nom_tabela .
				"
					LEFT JOIN estados ON (estados.uf = Estado)
				WHERE
					CodigoEmpresa = ?
		";
		$arrayParam = array($handle);
		
		$dados = $crud->getSQLGeneric($sql,$arrayParam, FALSE);
		
		return $dados;
		
		//
	}	
	
	public function editarEmpresa($post) {
		$pdo = Conexao::getInstance();
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		$crudSituacao = bd::getInstance($pdo,'Situacao');
		$objCommon = new CommonController();
		$arrayEmpresa = array();
		$layout_alteracao_pedido_whatsapp = $objCommon->validatePost('layout_alteracao_pedido_whatsapp');
		$logo_impressao_cinza = $objCommon->validatePost('logo-impressao-cinza');
		
		try {
			if ($layout_alteracao_pedido_whatsapp) {
				unset($post['layout_alteracao_pedido_whatsapp']);
			}

			if ($logo_impressao_cinza) {
				unset($post['logo-impressao-cinza']);
			}			
			
			foreach ($post as $key => $value) {
				if ($key === 'logotipoSelecionado' || $key == 'layout_mensagem_whatsapp') {
					continue;
				}
				
				if ($key != 'handle' && $key != 'CodigoEmpresa' && $key != 'DataCadastro') {
					$arrayEmpresa[$key] =  $value;
				}
			}
			
			if (isset($post['padrao']) && $post['padrao'] == '1') {
				$crud->update(['padrao' => 0], ['padrao=' => 1]);  		
			}

			$arrayCond = array('CodigoEmpresa=' => $post['handle']);
			$retorno   = $crud->update($arrayEmpresa, $arrayCond);
			$dir_imagens = dirname(__DIR__,2) . '/images/';
			
			if (isset($post['logotipoSelecionado']) && $post['logotipoSelecionado'] != '') {
				$data = $post['logotipoSelecionado'];
				if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
					$data = substr($data, strpos($data, ',') + 1);
					$type = strtolower($type[1]); // jpg, png, gif
					
					if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
						throw new \Exception('invalid image type');
					}
					$data = str_replace( ' ', '+', $data );
					$data = base64_decode($data);
					
					if ($data === false) {
						throw new \Exception('base64_decode failed');
					}
					
					$tamanhoImagem = getimagesizefromstring($data);
					$relacaoTamanhoImagem = $tamanhoImagem[0]/$tamanhoImagem[1];
					if( $relacaoTamanhoImagem > 1) {
						$width = 200;
						$height = 200/$relacaoTamanhoImagem;
					} else {
						$width = 200*$relacaoTamanhoImagem;
						$height = 200;
					}
					$src = imagecreatefromstring($data);
					$dst = imagecreatetruecolor($width,$height);
					imagecopyresampled($dst,$src,0,0,0,0,$width,$height,$tamanhoImagem[0],$tamanhoImagem[1]);
					imagedestroy($src);
					
					$a = imagepng($dst, $dir_imagens.'maestria-sistema.png');
					$im = imagecreatefrompng($dir_imagens.'maestria-sistema.png');
					
					if($logo_impressao_cinza != '1' && $im && imagefilter($im, IMG_FILTER_GRAYSCALE)) {
						imagepng($im, $dir_imagens.'logo-cliente-cinza.png');
					} else {
						imagepng($im, $dir_imagens.'logo-cliente-cinza.png');
					}
					imagedestroy($dst);
					imagedestroy($im);


					$alturaTopo = 90;
					$larguraToto = 144;
					if ($larguraToto/$alturaTopo > $relacaoTamanhoImagem) {
						$larguraToto = $alturaTopo*$relacaoTamanhoImagem;
					} else {
						$alturaTopo = $larguraToto/$relacaoTamanhoImagem;
					}

					$src = imagecreatefromstring($data);
					$dst = imagecreatetruecolor($larguraToto,$alturaTopo);
					imagecopyresampled($dst,$src,0,0,0,0,$larguraToto,$alturaTopo,$tamanhoImagem[0],$tamanhoImagem[1]);
					imagedestroy($src);
					imagepng($dst, $dir_imagens.'maestria-sistema-topo.png');
					/** Fim Imagem Topo */
				} else {
					echo 222;exit;
					throw new \Exception('did not match data URI with image data');
				}
		   }
		   
		   if (isset($post['layout_mensagem_whatsapp']) && $post['layout_mensagem_whatsapp'] != '') {
			   $layout_mensagem_whatsapp = $post['layout_mensagem_whatsapp'];
			   $sql = 'INSERT INTO
							Configuracoes (nome, valor)
					   VALUES
						("layout_mensagem_whatsapp","'.$layout_mensagem_whatsapp.'") 
						ON DUPLICATE KEY UPDATE    
							nome="layout_mensagem_whatsapp", valor="'.$layout_mensagem_whatsapp.'"';
			  $dados = $crud->getSQLGeneric($sql);
			}
			
			if ($layout_alteracao_pedido_whatsapp) {
				foreach ($layout_alteracao_pedido_whatsapp as $situacao => $mensagem) {
					$crudSituacao->update(['notificacao' => $mensagem], ["idSituacao=" => $situacao]);
				}
			}
			return $retorno;
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
	
	public function cadastrarEmpresa($post) {
		$pdo = Conexao::getInstance();
		
		$arrayEmpresa = array();
		foreach ($post as $key => $value) {
			if ($key != 'handle' && $key != 'CodigoEmpresa')
				$arrayEmpresa[$key] =  $value;
		}


		$crud = bd::getInstance($pdo,$this->nom_tabela);

		$retorno   = $crud->insert($arrayEmpresa);    
		
		return $retorno;
		exit;
	}	
}
?>
