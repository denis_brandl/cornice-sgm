<?php
require_once('conexao.php');
require_once('bd.php');
class Moldura_Item_Orcamento
{

  public $Cd_Orcamento;
  public $Cd_Item_Orcamento;
  public $Sequencia;
  public $Cd_Produto;
  public $Qt_Item;
  public $Vl_Unitario;
  public $Id_Situacao_Item;
  public $AlterarValor;

  public $Qtd_Varas_Consumidas;

  public $Qtd_Cm_Consumidos;

  public $codigoComplexidade;
  public $sarrafo_reforco;
  private $nom_tabela = 'Moldura_Item_Orcamento';
  private $order_by_default = 'Sequencia ASC, Cd_Item_Orcamento ASC';

  public function __construct()
  {
    $this->Cd_Orcamento = "";
    $this->Cd_Item_Orcamento = "";
    $this->Sequencia = "";
    $this->Cd_Produto = "";
    $this->Qt_Item = "";
    $this->Vl_Unitario = "";
    $this->Id_Situacao_Item = "";
    $this->AlterarValor = "";
    $this->codigoComplexidade = 0;
    $this->Qtd_Varas_Consumidas = 0;
    $this->Qtd_Cm_Consumidos = 0;
    $this->sarrafo_reforco = 'N';
  }

  public function listarMolduraItemOrcamento($Cd_Item_Orcamento, $Cd_Orcamento, $codigo_uf = 0, $order = '')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $order_by = ' Sequencia ASC';
    if ($order != '') {
      $order_by = $order;
    }

    $sql = "SELECT 
				p.CodigoProduto,
				p.DescricaoProduto,
				p.CodigoProdutoFabricante,
				p.NovoCodigo,
				p.codigo_ncm,
				p.codigo_cest,
        p.UnidadeProduto,
        UnidadesMedida.DescricaoUnidadeMedida,
				mio.Vl_Unitario,
				mio.Qt_Item,
				complexidade.codigoComplexidade,
				complexidade.nome AS nomeComplexidade,
        mio.sarrafo_reforco,
				t.csosn AS csosn,
				t.cfop AS cfop,
				t.csticms AS cst,
				t.icms as aliquotaIcms,
				pis.cstpis,
				pis.pis as aliquotaPis,
				confins.cstconfins,
				confins.confins as aliquotaConfins
			FROM Moldura_Item_Orcamento mio
				INNER JOIN Produtos p ON (mio.Cd_Produto = p.CodigoProduto)
				LEFT JOIN Complexidades complexidade ON (mio.codigoComplexidade = complexidade.codigoComplexidade)
				LEFT JOIN tributacao t ON p.id_grupo_tributario = t.id_grupo_tributario AND t.id_estado = $codigo_uf
				LEFT JOIN grupo_tributario gt ON t.id_grupo_tributario = gt.id_grupo_tributario AND p.id_grupo_tributario = t.id_grupo_tributario
				LEFT JOIN operacao_fiscal of2 ON t.id_operacao_fiscal = of2.id_operacao_fiscal
				LEFT JOIN pis ON pis.id_tributacao = t.id_tributacao
				LEFT JOIN confins ON confins.id_tributacao = t.id_tributacao
        LEFT JOIN UnidadesMedida ON p.UnidadeProduto = UnidadesMedida.CodigoUnidadeMedida
			WHERE
				mio.Cd_Item_Orcamento = ? AND mio.Cd_Orcamento = ? ORDER BY $order_by";

    $arrayParam = array($Cd_Item_Orcamento, $Cd_Orcamento);

    // print_r($arrayParam);
    //  echo "<pre>".$sql."</pre>";exit;

    $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    return $dados;

    //
  }

  public function listarMoldurasOrcamento($Cd_Orcamento)
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql = "SELECT 
					mio.*
				FROM
					Moldura_Item_Orcamento mio
				WHERE
					mio.Cd_Orcamento = ?
				ORDER BY
					Sequencia ASC";

    $arrayParam = array($Cd_Orcamento);

    $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    return $dados;
  }
}
