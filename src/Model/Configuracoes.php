<?php
require_once('conexao.php');
require_once('bd.php');
class Configuracoes {

  public $handle = 0;
	public $nome = '';
	public $valor = '';
  public $descricao = '';

	public $nom_tabela = 'Configuracoes';
	
	public function __construct() {
    $this->handle = 0;
    $this->nome = '';
    $this->valor = '';
    $this->descricao = '';
	}

	public function listarTodos($pagina_atual = 0,$linha_inicial = 0,$coluna = '',$buscar = '', $quantidade = '', $ordem = '') {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$where = '';
		
		if (!empty($coluna) && (!empty($buscar)) ) {
			$where = sprintf(' WHERE %s = "%s" ',$coluna,$buscar);
			if ($coluna !== 'handle') {
				$where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ',$coluna,strtoupper($buscar));
			}
		}
		
		if ($ordem == '') {
            $ordem = 'nome ASC';
		}
		
		$sql = "SELECT * FROM ".$this->nom_tabela.$where." ORDER BY ".$ordem;
		
		$dados = $crud->getSQLGeneric($sql);
		return $dados;
		
		//
	}

	public function listarTodosTotal($coluna = '',$buscar = '') {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$where = '';
		
		if (!empty($coluna) && (!empty($buscar) || $buscar >= 0) ) {
			$where = sprintf(' WHERE %s = "%s" ',$coluna,$buscar);
			if ($coluna !== 'handle') {
				$where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ',$coluna,strtoupper($buscar));
			}
		}		
		
		$sql = "SELECT count(*) as total_registros FROM ".$this->nom_tabela.$where;		
		
		$dados = $crud->getSQLGeneric($sql,null,FALSE);		
		
		return $dados->total_registros;
		
		//
	}

	public function listarConfiguracao($nome) {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$sql = "SELECT * FROM ".$this->nom_tabela." WHERE nome = ?";
    
		$arrayParam = array($nome); 
		
		$dados = $crud->getSQLGeneric($sql,$arrayParam, FALSE);
		
		if (!$dados) {
			return (object) [
				'id' => 0,
				'nome' => '',
				'valor' => '',
				'descricao' => ''
			];
		}
		return $dados;
		
		//
	}
	
  public function editarConfiguracao($post)
  {
    $pdo = Conexao::getInstance();
    $crud = bd::getInstance($pdo, $this->nom_tabela);
    foreach ($post as $key => $value) {
		$crud->update(['valor' => $value], ['nome=' => $key] );
    }
  }	
}
?>
