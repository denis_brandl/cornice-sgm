
      <?php
        class Plano_contas {
          public $id_plano_contas;
public $classificacao;
public $data_criacao;
public $data_atualizacao;
public $id_usuario_criacao;
public $id_usuario_edicao;

          public $nom_tabela = "plano_contas";
          public function __construct() {
            $id_plano_contas = 0;
$classificacao = "";
$data_criacao = date("Y-m-d");
$data_atualizacao = date("Y-m-d");
$id_usuario_criacao = 0;
$id_usuario_edicao = 0;

          }

          public function listarTodos($pagina_atual = 0,$linha_inicial = 0,$coluna = '',$buscar = '') {
              $pdo = Conexao::getInstance();              
              $crud = bd::getInstance($pdo,$this->nom_tabela);
              $where = '';
              if ($coluna != '' && $buscar != '') {
                $where = sprintf(' WHERE %s LIKE UPPER("%%%s%%") ',$coluna,strtoupper($buscar));
              }
              
              $paginacao = ' LIMIT '.QTDE_REGISTROS;
              if ($pagina_atual > 0 && $linha_inicial > 0) {
                $paginacao = " LIMIT {$linha_inicial}, ".QTDE_REGISTROS;
              }		
              
              $sql = "SELECT * FROM ".$this->nom_tabela.$where.$paginacao;		
              $dados = $crud->getSQLGeneric($sql);
              
              return $dados;
          }

          public function listar($handle) {
            $pdo = Conexao::getInstance();
            $crud = bd::getInstance($pdo,$this->nom_tabela);
            $sql = "SELECT * FROM ".$this->nom_tabela." WHERE id_plano_contas = ?";
            $arrayParam = array($handle);
            $dados = $crud->getSQLGeneric($sql,$arrayParam, FALSE);
            return $dados;
          }	          

          public function listarTodosTotal() {
            $pdo = Conexao::getInstance();
            $crud = bd::getInstance($pdo,$this->nom_tabela);
            $sql = "SELECT count(*) as total_registros FROM ".$this->nom_tabela;
            $dados = $crud->getSQLGeneric($sql,null,FALSE);
            return $dados->total_registros;
          }

          public function editar($post) {
            $pdo = Conexao::getInstance();
            $crud = bd::getInstance($pdo,$this->nom_tabela);
              
            $arrEditar = array();
            foreach ($post as $key => $value) {
              if ($key != "Plano_contas" && $key != "data_criacao") {
                $arrEditar[$key] =  $value;
              }

              $arrayCond = array("id_plano_contas=" => $post["handle"]);
              $retorno   = $crud->update($arrEditar, $arrayCond);
              return $retorno;
            }
          }

          public function cadastrar($post) {
            $pdo = Conexao::getInstance();
            
            $arrInserir = array();
            foreach ($post as $key => $value) {
              if ($key != "id_plano_contas") {
                $arrInserir[$key] =  $value;
              }
            }
            $crud = bd::getInstance($pdo,$this->nom_tabela);
            $retorno   = $crud->insert($arrInserir);
            return $retorno;
          }

          public function excluir($handle) {
            $pdo = Conexao::getInstance();
            $crud = bd::getInstance($pdo,$this->nom_tabela);
            $crud->delete(array("id_plano_contas" => $handle));
            return true;
          }	          
        }
      ?>
    