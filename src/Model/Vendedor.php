<?php
class Vendedor
{
    public $id_vendedor;
    public $nome_vendedor;
    public $percentual_comissao;
    public $id_usuario;
    public $afiliado;
    public $situacao;
    public $criado_em;
    public $modificado_em;
    public $criado_por;
    public $modificado_por;
    public $nom_tabela = "vendedor";
    public function __construct()
    {
        $id_vendedor = 0;
        $nome_vendedor = "";
        $percentual_comissao = 0;
        $id_usuario = 0;
        $afiliado = 0;
        $situacao = 0;
        $criado_em = date("Y-m-d");
        $modificado_em = "NAO_IDENTIFICADO";
        $criado_por = 0;
        $modificado_por = 0;
    }
    public function listarTodos($pagina_atual = 0, $linha_inicial = 0, $coluna = '', $buscar = '')
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $where = '';
        if ($coluna != '' && $buscar != '') {
            $where = sprintf(' WHERE %s LIKE UPPER("%%%s%%") ', $coluna, strtoupper($buscar));
        }
        $paginacao = ' LIMIT ' . QTDE_REGISTROS;
        if ($pagina_atual > 0 && $linha_inicial > 0) {
            $paginacao = " LIMIT {$linha_inicial}, " . QTDE_REGISTROS;
        }
        $sql = "SELECT * FROM " . $this->nom_tabela . $where . $paginacao;
        $dados = $crud->getSQLGeneric($sql);
        return $dados;
    }
    public function listar($handle)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $sql = "SELECT * FROM " . $this->nom_tabela . " WHERE id_vendedor = ?";
        $arrayParam = array($handle);
        $dados = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
        return $dados;
    }
    public function listarTodosTotal()
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela;
        $dados = $crud->getSQLGeneric($sql, null, FALSE);
        return $dados->total_registros;
    }
    public function editar($post)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $arrEditar = array();

        $sql = sprintf(
            '
                INSERT INTO %s (percentual_comissao, id_usuario, afiliado, situacao) 
                VALUES (%s, %s, %s, %s)
                ON DUPLICATE KEY UPDATE id_usuario = %3$s, percentual_comissao = %2$s, situacao = %5$s
            ',
            $this->nom_tabela,
            $post['comissao'],
            $post['id_usuario'],
            (int) $post['afiliado'],
            $post['usuario_vendedor']
        );
        
        $retorno   = $crud->getSQLGeneric($sql);
        return $retorno;
    }
    public function cadastrar($post)
    {
        $pdo = Conexao::getInstance();
        $arrInserir = array();
        foreach ($post as $key => $value) {
            if ($key != "handle") {
                $arrInserir[$key] =  $value;
            }
        }
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $retorno   = $crud->insert($arrInserir);
        return $retorno;
    }
    public function excluir($handle)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $crud->delete(array("id_vendedor" => $handle));
        return true;
    }
}
