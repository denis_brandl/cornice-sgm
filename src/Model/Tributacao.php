<?php
require_once('conexao.php');
require_once('bd.php');
class Tributacao
{

  public $id_tributacao;
  public $id_operacao_fiscal;
  public $id_grupo_tributario;
  public $icms;
  public $cfop;
  public $csosn;
  public $csticms;
  public $icmsst;
  public $id_estado;
  public $nom_tabela = 'tributacao';


  public function __construct()
  {
    $this->id_tributacao = 0;
    $this->id_operacao_fiscal = 0;
    $this->id_grupo_tributario = 0;
    $this->icms = '';
    $this->cfop = '';
    $this->csosn = '';
    $this->csticms = '';
    $this->icmsst = '';
    $this->id_estado = 0;
  }

  public function listarTodos($pagina_atual = 0, $linha_inicial = 0, $coluna = '', $buscar = '', $quantidade = '', $ordem = '')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';

    if (!empty($coluna) && (!empty($buscar))) {
      $where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
      if ($coluna !== 'id_grupo_tributario') {
        $where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
      }
    }

    $paginacao = " LIMIT " . QTDE_REGISTROS;
    $qtd_registros = QTDE_REGISTROS;
    if ($quantidade > 0) {
      $qtd_registros = $quantidade;
    }
    if ($pagina_atual > 0) {
      $paginacao = ' LIMIT ' . $qtd_registros;
      if ($pagina_atual > 0 && $linha_inicial > 0) {
        $paginacao = " LIMIT $qtd_registros OFFSET " . ($linha_inicial);
      }
    }

    if ($ordem == '') {
      $ordem = 'id_tributacao ASC';
    }

    $sql = "SELECT * FROM " . $this->nom_tabela . $where . " ORDER BY " . $ordem . $paginacao;

    // echo $sql;exit;

    $dados = $crud->getSQLGeneric($sql);
    return $dados;

    //
  }

  public function listarPorGrupoTributario()
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);


    $sql = sprintf("
      SELECT
        tributacao.id_tributacao,
        tributacao.id_operacao_fiscal,
        tributacao.id_estado,
        operacao_fiscal.descricao,
        estados.uf,
        estados.nome
      FROM
        %s as tributacao
      LEFT JOIN 
        operacao_fiscal ON tributacao.id_operacao_fiscal = operacao_fiscal.id_operacao_fiscal
      LEFT JOIN
        estados ON tributacao.id_estado = estados.codigo_uf        
      GROUP BY
        tributacao.id_operacao_fiscal, estados.uf", $this->nom_tabela
    );

     //echo $sql;exit;

    $dados = $crud->getSQLGeneric($sql);
    return $dados;

    //
  }

  public function listarTodosTotal($coluna = '', $buscar = '')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';

    if (!empty($coluna) && (!empty($buscar) || $buscar >= 0)) {
      $where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
      if ($coluna !== 'id_grupo_tributario') {
        $where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
      }
    }

    $sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela . $where;

    $dados = $crud->getSQLGeneric($sql, null, FALSE);

    return $dados->total_registros;

    //
  }

  public function listarTributacao($filtro = [])
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';
    $qtdFiltros = count($filtro);
    $aux = 1;
    $and = ' AND ';
    if ($qtdFiltros > 0) {
      $where = ' WHERE ';
      foreach ($filtro as $key => $value) {
        $where .= sprintf('%s = %s %s', $key, $value, $and);
        $aux++;
        if ($aux >= $qtdFiltros) {
          $and = '';
        }
      }
    }

    $sql = "SELECT * FROM " . $this->nom_tabela . $where;

    // echo $sql;exit;
    $dados = $crud->getSQLGeneric($sql, [], TRUE);

    foreach ($dados as $key => $tributacao) {
      $sqlPis = sprintf("SELECT * FROM `pis` WHERE id_operacao_fiscal = %s AND id_grupo_tributario = %s ", $tributacao->id_operacao_fiscal, $tributacao->id_grupo_tributario);
      $consultaPis = $crud->getSQLGeneric($sqlPis, [], FALSE);
      $dados[$key]->pis = (array) $consultaPis;

      $sqlIpi = sprintf("SELECT * FROM `ipi` WHERE id_operacao_fiscal = %s AND id_grupo_tributario = %s ", $tributacao->id_operacao_fiscal, $tributacao->id_grupo_tributario);
      $consultaIpi = $crud->getSQLGeneric($sqlIpi, [], FALSE);
      $dados[$key]->ipi = (array) $consultaIpi;

      $sqlConfins = sprintf("SELECT * FROM `confins` WHERE id_operacao_fiscal = %s AND id_grupo_tributario = %s ", $tributacao->id_operacao_fiscal, $tributacao->id_grupo_tributario);
      $consultaConfins = $crud->getSQLGeneric($sqlConfins, [], FALSE);
      $dados[$key]->confins = (array) $consultaConfins;      
    }
    
    return $dados;

    //
  }

  public function listarTributacaosPorEstado($siglaEstado)
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql = "
      SELECT
        municipios.*
      FROM
        municipios
      INNER JOIN
        estados ON municipios.codigo_uf = estados.codigo_uf
      WHERE
	      estados.uf = '" . $siglaEstado . "' 
    ";

    $dados = $crud->getSQLGeneric($sql, [], TRUE);

    return $dados;

    //
  }

  public function editarTributacao($post)
  {
    try {
      $pdo = Conexao::getInstance();
      $crud = bd::getInstance($pdo, $this->nom_tabela);
      $instancePis = bd::getInstance($pdo, 'pis');
      $instanceIpi = bd::getInstance($pdo, 'ipi');
      $instanceConfins = bd::getInstance($pdo, 'confins');      
      $arrayTributacao = array();

      $arrHandle = explode('_', $post['handle']);
      
	  $sql = sprintf(
		'select id_tributacao, id_grupo_tributario from tributacao where id_operacao_fiscal = %s and id_estado = %s',
		$arrHandle[0],
		$arrHandle[1]
	  );
	  $arrConsultaTributacao = $crud->getSQLGeneric($sql);
	  
            
      foreach ($post['tributacaoIcms'] as $grupo_tributario => $imposto) {
        $arrayTributacao[$grupo_tributario]['cfop'] = (float) $post['tributacaoGeral'][$grupo_tributario]['cfop'];
        $arrayTributacao[$grupo_tributario]['icms'] = (float) $imposto['icms'];
        $arrayTributacao[$grupo_tributario]['csticms'] = (float) $imposto['csticms'];
        $arrayTributacao[$grupo_tributario]['icmsst'] = (float) $imposto['icmsst'];
        $arrayTributacao[$grupo_tributario]['csosn'] = (float) $imposto['csosn'];
      }

/*
      foreach ($post['tributacaoPis'] as $grupo_tributario => $imposto) {
            $instancePis->update([
            'cstpis' => (string) $imposto['cstpis'],
            'pis' => (float) $imposto['pis']
            ],
            [
              'id_operacao_fiscal = ' => $arrHandle[0],
              'id_grupo_tributario = ' => $grupo_tributario,
            ]
          );
      }

      foreach ($post['tributacaoIpi'] as $grupo_tributario => $imposto) {
            $instanceIpi->update([
            'cstipi' => (string) $imposto['cstipi'],
            'ipi' => (float) $imposto['ipi']
            ],
            [
              'id_operacao_fiscal = ' => $arrHandle[0],
              'id_grupo_tributario = ' => $grupo_tributario,
            ]            
          );
      }

      foreach ($post['tributacaoConfins'] as $grupo_tributario => $imposto) {
            $instanceConfins->update([
            'cstconfins' => (string) $imposto['cstconfins'],
            'confins' => (float) $imposto['confins'],
            ],
            [
              'id_operacao_fiscal = ' => $arrHandle[0],
              'id_grupo_tributario = ' => $grupo_tributario,
            ]
          );
      }
      */

      foreach ($arrayTributacao as $grupo_tributario => $valores) {
        $crud->update($valores, [
          'id_operacao_fiscal = ' => $arrHandle[0],
          'id_estado = ' => $arrHandle[1],
          'id_grupo_tributario = ' => $grupo_tributario,
        ]);
        
        $chave = array_search($grupo_tributario, array_column($arrConsultaTributacao, 'id_grupo_tributario'));
        
		$instancePis->update([
            'cstpis' => (string) $post['tributacaoPis'][$grupo_tributario]['cstpis'],
            'pis' => (float) $post['tributacaoPis'][$grupo_tributario]['pis']
            ],
            [
              'id_tributacao = ' => $arrConsultaTributacao[$chave]->id_tributacao,
              'id_grupo_tributario = ' => $grupo_tributario,
            ]
          );
        
		$instanceIpi->update(
			[
				'cstipi' => (string) $post['tributacaoIpi'][$grupo_tributario]['cstipi'],
				'ipi' => (float) $post['tributacaoIpi'][$grupo_tributario]['ipi']
			],
			[
				'id_tributacao = ' => $arrConsultaTributacao[$chave]->id_tributacao,
				'id_grupo_tributario = ' => $grupo_tributario,
			]            
		);
		
		$instanceConfins->update(
			[
				'cstconfins' => (string) $post['tributacaoConfins'][$grupo_tributario]['cstconfins'],
				'confins' => (float) $post['tributacaoConfins'][$grupo_tributario]['confins'],
			],
			[
				'id_tributacao = ' => $arrConsultaTributacao[$chave]->id_tributacao,
				'id_grupo_tributario = ' => $grupo_tributario,
			]
		);
      }
      return true;
    } catch (Exception $e) {
      return false;
    }
  }

  public function cadastrarTributacao($post)
  {
    try {
      $pdo = Conexao::getInstance();
      $crud = bd::getInstance($pdo, $this->nom_tabela);
      $instancePis = bd::getInstance($pdo, 'pis');
      $instanceIpi = bd::getInstance($pdo, 'ipi');
      $instanceConfins = bd::getInstance($pdo, 'confins');
      $arrayTributacao = [];

      foreach ($post['tributacaoIcms'] as $grupo_tributario => $imposto) {
        $arrayTributacao[$grupo_tributario]['icms'] = (float) $imposto['icms'];
        $arrayTributacao[$grupo_tributario]['csticms'] = (float) $imposto['csticms'];
        $arrayTributacao[$grupo_tributario]['icmsst'] = (float) $imposto['icmsst'];
        $arrayTributacao[$grupo_tributario]['csosn'] = (float) $imposto['csosn'];
      }

      /*
      foreach ($post['tributacaoPis'] as $grupo_tributario => $imposto) {
        $instancePis->insert([
          'cstpis' => (string) $imposto['cstpis'],
          'pis' => (float) $imposto['pis'],
          'id_grupo_tributario' => $grupo_tributario,
          'id_operacao_fiscal' => $post['id_operacao_fiscal']
        ]);
      }


      foreach ($post['tributacaoIpi'] as $grupo_tributario => $imposto) {
        $instanceIpi->insert([
          'cstipi' => (string) $imposto['cstipi'],
          'ipi' => (float) $imposto['ipi'],
          'id_grupo_tributario' => $grupo_tributario,
          'id_operacao_fiscal' => $post['id_operacao_fiscal']          
        ]);
      }

      foreach ($post['tributacaoConfins'] as $grupo_tributario => $imposto) {
        $instanceConfins->insert([
          'cstconfins' => (string) $imposto['cstconfins'],
          'confins' => (float) $imposto['confins'],
          'id_grupo_tributario' => $grupo_tributario,
          'id_operacao_fiscal' => $post['id_operacao_fiscal']          
        ]);
      }

      */
      foreach ($arrayTributacao as $grupo_tributario => $valores) {
        $arrInsertIcms = array_merge(
          [
            'cfop' => (float) $post['tributacaoGeral'][$grupo_tributario]['cfop'],
            'id_grupo_tributario' => $grupo_tributario,
            'id_operacao_fiscal' => $post['id_operacao_fiscal'],
            'id_estado' => $post['id_estado']
          ],
          $valores
        );
		
        $id_tributacao = $crud->insert($arrInsertIcms);
        
        
		$instancePis->insert([
          'cstpis' => (string) $post['tributacaoPis'][$grupo_tributario]['cstpis'],
          'pis' => (float) $post['tributacaoPis'][$grupo_tributario]['pis'],
          'id_grupo_tributario' => $grupo_tributario,
          'id_operacao_fiscal' => $post['id_operacao_fiscal'],
          'id_tributacao' => $id_tributacao
        ]);
        
		$instanceIpi->insert([
          'cstipi' => (string) $post['tributacaoIpi'][$grupo_tributario]['cstipi'],
          'ipi' => (float) $post['tributacaoIpi'][$grupo_tributario]['ipi'],
          'id_grupo_tributario' => $grupo_tributario,
          'id_operacao_fiscal' => $post['id_operacao_fiscal'],
          'id_tributacao' => $id_tributacao
        ]); 
        
        $instanceConfins->insert([
          'cstconfins' => (string) $post['tributacaoConfins'][$grupo_tributario]['cstconfins'],
          'confins' => (float) $post['tributacaoConfins'][$grupo_tributario]['confins'],
          'id_grupo_tributario' => $grupo_tributario,
          'id_operacao_fiscal' => $post['id_operacao_fiscal'],
          'id_tributacao' => $id_tributacao
        ]);               
        
      }

      return sprintf('%s_%s', $post['id_operacao_fiscal'], $post['id_estado']);
    } catch (Exception $e) {
      return false;
    }
  }

  public function excluir($handle)
  {
    $pdo = Conexao::getInstance();
    $crud = bd::getInstance($pdo, $this->nom_tabela);
    $crud->delete(array('id_grupo_tributario' => $handle));
  }
}
