<?php
class Account {
public $AccountId;
public $UserId;
public $AccountName;
public $tipo;
public $nom_tabela = "account";
public function __construct() {
$AccountId = 0;
$UserId = 0;
$AccountName = "";
$tipo = 0;
}
public function listarTodos($pagina_atual = 0,$linha_inicial = 0,$coluna = '',$buscar = '') {
$pdo = Conexao::getInstance();              
$crud = bd::getInstance($pdo,$this->nom_tabela);
$where = '';
if ($coluna != '' && $buscar != '') {
$where = sprintf(' WHERE %s LIKE UPPER("%%%s%%") ',$coluna,strtoupper($buscar));
}
$paginacao = ' LIMIT '.QTDE_REGISTROS;
if ($pagina_atual > 0 && $linha_inicial > 0) {
$paginacao = " LIMIT {$linha_inicial}, ".QTDE_REGISTROS;
}		
$sql = "SELECT * FROM ".$this->nom_tabela.$where." ORDER BY tipo DESC" .$paginacao;		
$dados = $crud->getSQLGeneric($sql);
return $dados;
}
public function listar($handle) {
$pdo = Conexao::getInstance();
$crud = bd::getInstance($pdo,$this->nom_tabela);
$sql = "SELECT * FROM ".$this->nom_tabela." WHERE AccountId = ?";
$arrayParam = array($handle);
$dados = $crud->getSQLGeneric($sql,$arrayParam, FALSE);
return $dados;
}	          
public function listarTodosTotal() {
$pdo = Conexao::getInstance();
$crud = bd::getInstance($pdo,$this->nom_tabela);
$sql = "SELECT count(*) as total_registros FROM ".$this->nom_tabela;
$dados = $crud->getSQLGeneric($sql,null,FALSE);
return $dados->total_registros;
}
public function editar($post) {
$pdo = Conexao::getInstance();
$crud = bd::getInstance($pdo,$this->nom_tabela);
$arrEditar = array();
foreach ($post as $key => $value) {
if ($key != "handle" && $key != "data_criacao") {
$arrEditar[$key] =  $value;
}
}
$arrayCond = array("AccountId=" => $post["handle"]);
$retorno   = $crud->update($arrEditar, $arrayCond);
return $retorno;            
}
public function cadastrar($post) {
$pdo = Conexao::getInstance();
$arrInserir = array();
foreach ($post as $key => $value) {
if ($key != "handle") {
$arrInserir[$key] =  $value;
}
}
$crud = bd::getInstance($pdo,$this->nom_tabela);
$retorno   = $crud->insert($arrInserir);
return $retorno;
}
public function excluir($handle) {
$pdo = Conexao::getInstance();
$crud = bd::getInstance($pdo,$this->nom_tabela);
$crud->delete(array("AccountId" => $handle));
return true;
}	          
}
