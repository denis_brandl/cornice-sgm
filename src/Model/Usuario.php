<?php
require_once('conexao.php');
require_once('bd.php');
class Usuario
{

	public $hash = '@.Br@nd!_sDr1nk5B33r5';
	public $CodigoUsuario = '';
	public $NomeUsuario = '';
	public $SenhaUsuario = '';
	public $SessaoUsuario = '';
	public $login = '';
	public $id_grupo_usuario = '';
	public $situacao = '';
	public $usuario_master = '';
	private $nom_tabela = 'usuarios';
	private $logado_usuario_master = false;
	public $usuario_vendedor = 0;
	public $percentual_comissao = 0;

	public function __construct()
	{
		$this->CodigoUsuario = 0;
		$this->NomeUsuario = '';
		$this->SenhaUsuario = '';
		$this->SessaoUsuario = '';
		$this->login = '';
		$this->id_grupo_usuario = 0;
		$this->situacao = 1;
		$this->usuario_master = 0;
		$this->percentual_comissao = 0;
		$this->logado_usuario_master = isset($_SESSION['usuario_master']) ? filter_var($_SESSION['usuario_master'], FILTER_VALIDATE_BOOLEAN) : false;
	}

	public function logar($post)
	{
		$pdo = Conexao::getInstance();

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$sql = "SELECT * FROM " . $this->nom_tabela . " WHERE login = ? AND situacao = 1";

		$arrayCond = array($post['usuario']);

		$dados = $crud->getSQLGeneric($sql, $arrayCond, FALSE);

		return $dados;
	}

	public function registraSessao($CodigoUsuario, $sessao)
	{
		$pdo = Conexao::getInstance();

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$arrayCond = array('CodigoUsuario=' => $CodigoUsuario);
		$crud->update(array('SessaoUsuario' => $sessao), $arrayCond);
	}

	public function info($sessao)
	{
		$pdo = Conexao::getInstance();

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$sql = "SELECT * FROM " . $this->nom_tabela . " WHERE SessaoUsuario = ?";

		$arrayCond = array($sessao);

		$dados = $crud->getSQLGeneric($sql, $arrayCond, FALSE);

		return $dados;
	}

	public function listarTodos($pagina_atual = 0, $linha_inicial = 0, $coluna = '', $buscar = '', $quantidade = '', $ordem = '')
	{
		$pdo = Conexao::getInstance();

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$where = '';

		if (!empty($coluna) && $buscar != '') {
			$where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
			if ($coluna !== 'codigoClienteGrupoGrupo') {
				$where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
			}
		}

		
		$condicional_usuario_master = ($this->logado_usuario_master == false ? ' usuarios.usuario_master = 0 AND usuarios.situacao = 1' : ' usuarios.usuario_master IN (0,1) ' );

		if ($where !== '') {
			$where .= ' AND ' . $condicional_usuario_master;
		} else {
			$where .= ' WHERE ' . $condicional_usuario_master;
		}

		$paginacao = " LIMIT " . QTDE_REGISTROS;
		$qtd_registros = QTDE_REGISTROS;
		if ($quantidade > 0) {
			$qtd_registros = $quantidade;
		}
		if ($pagina_atual > 0) {
			$paginacao = ' LIMIT ' . $qtd_registros;
			if ($pagina_atual > 0 && $linha_inicial > 0) {
				$paginacao = " LIMIT $qtd_registros OFFSET " . ($linha_inicial);
			}
		}

		if ($ordem == '') {
			$ordem = 'NomeUsuario ASC';
		}

		$sql = "SELECT *, grupos_usuario.descricao as grupo_usuario FROM " . $this->nom_tabela . " LEFT JOIN grupos_usuario ON id_grupo = id_grupo_usuario " . $where . " ORDER BY " . $ordem . $paginacao;

		// echo $sql;exit;

		$dados = $crud->getSQLGeneric($sql);
		return $dados;

		//
	}

	public function listarTodosTotal($coluna = '', $buscar = '')
	{
		$pdo = Conexao::getInstance();

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$where = '';

		if (!empty($coluna) && (!empty($buscar) || $buscar >= 0)) {
			$where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
			if ($coluna !== 'codigoClienteGrupoGrupo') {
				$where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
			}
		}

		$sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela . $where;

		$dados = $crud->getSQLGeneric($sql, null, FALSE);

		return $dados->total_registros;

		//
	}

	public function editarUsuario($post)
	{
		$pdo = Conexao::getInstance();

		$arrayUsuario = array();
		foreach ($post as $key => $value) {
			if ($key != 'handle' && $key != 'codigoUsuario') {
				if ($key == 'SenhaUsuario') {
					if (!empty(trim($value))) {
						$arrayUsuario[$key] = password_hash($value, PASSWORD_DEFAULT);
					}
				} else {
					if ($key == 'situacao') {
						$value = (int) $value;
					}
					$arrayUsuario[$key] =  $value;
				}
			}
		}

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$arrayCond = array('codigoUsuario=' => $post['handle']);
		$retorno   = $crud->update($arrayUsuario, $arrayCond);

		return $retorno;
	}

	public function cadastrarUsuario($post)
	{
		$pdo = Conexao::getInstance();

		$arrayUsuario = array('CodigoUsuario' => 0);
		foreach ($post as $key => $value) {
			if ($key != 'handle' && $key != 'CodigoUsuario') {
				$arrayUsuario[$key] =  $value;
			}
			if ($key == 'SenhaUsuario') {
				$arrayUsuario[$key] = password_hash($value, PASSWORD_DEFAULT);
			}
		}

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$retorno   = $crud->insert($arrayUsuario);

		return $retorno;
	}

	public function listarUsuario($handle, $login = '')
	{
		$pdo = Conexao::getInstance();

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$coluna_alvo = $login !== '' ? 'login' : 'codigoUsuario';
		$filtro = $login !== '' ? $login : $handle;
		$sql = "
			SELECT
				usuarios.*,
				v.id_usuario,
				IF(ISNULL(v.percentual_comissao), 0, v.percentual_comissao) as percentual_comissao,
				v.situacao usuario_vendedor
			FROM "
				. $this->nom_tabela . "
				LEFT JOIN vendedor v ON (v.id_usuario = CodigoUsuario)
			WHERE " . $coluna_alvo . " = ?";

		// echo $sql;exit;

		$arrayParam = array($filtro);

		$dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

		return $dados;

		//
	}
}
