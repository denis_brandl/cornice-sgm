<?php
require_once('conexao.php');
require_once('bd.php');
class ComplexidadeProduto
{
	public $codigoComplexidadeProduto = 0;
	public $codigoComplexidade = 0;
	public $codigoProduto = 0;
	public $valor = 0;
	public $aplicacao = 'A';
	public $definicao_preco = 2;

	public $nom_tabela = 'ComplexidadeProduto';


	public function __construct()
	{
		$this->codigoComplexidadeProduto = '';
		$this->codigoComplexidade = 0;
		$this->codigoProduto = 0;
		$this->valor = 0;
	}

	public function listarComplexidadePorProduto($handleProduto, $arrFilters = [])
	{
		$pdo = Conexao::getInstance();

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$where = isset($arrFilters['status']) ? ' WHERE status = ' . $arrFilters['status'] : '';


		$sql = "SELECT
				codigoComplexidade,
				valor,
				nome,
				aplicacao,
				definicao_preco,
				status
			FROM 
				(
					SELECT
						complexidade_produto.codigoComplexidadeProduto,
						complexidade_produto.codigoComplexidade,
						complexidade_produto.valor,
						Complexidades.nome,
						complexidade_produto.aplicacao,
						complexidade_produto.definicao_preco,
						Complexidades.status
					FROM
						ComplexidadeProduto complexidade_produto
						INNER JOIN Complexidades ON complexidade_produto.codigoComplexidade = Complexidades.codigoComplexidade
					WHERE
						complexidade_produto.codigoProduto = ? AND complexidade_produto.valor > 0
		
					UNION
		
					SELECT
						0 as codigoComplexidadeProduto,
						Complexidades.codigoComplexidade,
						Complexidades.valor,
						Complexidades.nome,
						Complexidades.aplicacao,
						Complexidades.definicao_preco,
						Complexidades.status
					FROM
						Complexidades
				) as t1
			$where
			GROUP BY
				codigoComplexidade";

		$arrayParam = array($handleProduto);
		// echo "<pre>$sql</pre>";
		// exit;
		$dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

		$arrDados = [];

		foreach ($dados as $dado) {
			$arrDados[$dado->codigoComplexidade] = ['handle' => $dado->codigoComplexidade, 'valor' => $dado->valor, 'nome' => $dado->nome, 'aplicacao' => $dado->aplicacao, 'definicao_preco' => $dado->definicao_preco];
		}

		return $arrDados;

		//
	}
}
