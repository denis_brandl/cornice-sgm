<?php
require_once('conexao.php');
require_once('bd.php');
class Estado
{
  public $codigo_uf = 0;
  public $uf = '';
  public $nome = '';
  public $latitude = 0;
  public $longitude = 0;

  public $aliquota_icms = 0;

  public $aliquota_fcp = 0;

  public $nom_tabela = 'estados';


  public function __construct()
  {
    $this->nome = '';
    $this->uf = '';
    $this->codigo_uf = 0;
    $this->latitude = 0;
    $this->longitude = 0;
    $this->aliquota_icms = 0;
    $this->aliquota_fcp = 0;
  }

  public function listarTodos($coluna = '', $buscar = '')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';

    if (!empty($coluna) && (!empty($buscar))) {
      $where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
      if ($coluna !== 'codigo_uf') {
        $where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
      }
    }

    $sql = "SELECT * FROM " . $this->nom_tabela . $where . " ORDER BY nome ASC";

    // echo $sql;exit;

    $dados = $crud->getSQLGeneric($sql);
    return $dados;

    //
  }

  public function listarTodosTotal($coluna = '', $buscar = '')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';

    if (!empty($coluna) && (!empty($buscar) || $buscar >= 0)) {
      $where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
      if ($coluna !== 'codigo_uf') {
        $where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
      }
    }

    $sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela . $where;

    $dados = $crud->getSQLGeneric($sql, null, FALSE);

    return $dados->total_registros;

    //
  }

  public function listarEstado($handle)
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql = "SELECT * FROM " . $this->nom_tabela . " WHERE codigo_uf = ?";

    $arrayParam = array($handle);

    $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    return $dados;

    //
  }


  public function editarEstado($post)
  {
    $pdo = Conexao::getInstance();

    $arrayCliente = array();
    foreach ($post as $key => $value) {
      if ($key != 'handle' && $key != 'codigo_uf') {
        $arrayCliente[$key] =  $value;
      }
    }

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $arrayCond = array('codigo_uf=' => $post['handle']);
    $retorno   = $crud->update($arrayCliente, $arrayCond);

    return $retorno;
  }

  public function cadastrarEstado($post)
  {
    $pdo = Conexao::getInstance();

    $arrayCliente = array();
    foreach ($post as $key => $value) {
      if ($key != 'handle' && $key != 'codigo_uf') {
        $arrayCliente[$key] =  $value;
      }
    }

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $retorno   = $crud->insert($arrayCliente);

    return $retorno;
  }

  public function excluir($handle)
  {
    $pdo = Conexao::getInstance();
    $crud = bd::getInstance($pdo, $this->nom_tabela);
    $crud->delete(array('codigo_uf' => $handle));
  }
}
