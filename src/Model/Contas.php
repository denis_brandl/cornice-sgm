
      <?php
        class Contas {
          public $id;
public $nome;
public $idtipo;
public $idgeral;

          public $nom_tabela = "contas";
          public function __construct() {
            $id = 0;
$nome = "";
$idtipo = 0;
$idgeral = 0;

          }

          public function listarTodos($pagina_atual = 0,$linha_inicial = 0,$coluna = '',$buscar = '') {
              $pdo = Conexao::getInstance();              
              $crud = bd::getInstance($pdo,$this->nom_tabela);
              $where = '';
              if ($coluna != '' && $buscar != '') {
                $where = sprintf(' WHERE %s LIKE UPPER("%%%s%%") ',$coluna,strtoupper($buscar));
              }
              
              $paginacao = ' LIMIT '.QTDE_REGISTROS;
              if ($pagina_atual > 0 && $linha_inicial > 0) {
                $paginacao = " LIMIT {$linha_inicial}, ".QTDE_REGISTROS;
              }		
              
              $sql = "SELECT * FROM ".$this->nom_tabela.$where.$paginacao;		
              $dados = $crud->getSQLGeneric($sql);
              
              return $dados;
          }

          public function listar($handle) {
            $pdo = Conexao::getInstance();
            $crud = bd::getInstance($pdo,$this->nom_tabela);
            $sql = "SELECT * FROM ".$this->nom_tabela." WHERE id = ?";
            $arrayParam = array($handle);
            $dados = $crud->getSQLGeneric($sql,$arrayParam, FALSE);
            return $dados;
          }	          

          public function listarTodosTotal() {
            $pdo = Conexao::getInstance();
            $crud = bd::getInstance($pdo,$this->nom_tabela);
            $sql = "SELECT count(*) as total_registros FROM ".$this->nom_tabela;
            $dados = $crud->getSQLGeneric($sql,null,FALSE);
            return $dados->total_registros;
          }

          public function editar($post) {
            $pdo = Conexao::getInstance();
            $crud = bd::getInstance($pdo,$this->nom_tabela);
              
            $arrEditar = array();
            foreach ($post as $key => $value) {
              if ($key != "id" && $key != "data_criacao") {
                $arrEditar[$key] =  $value;
              }

              $arrayCond = array("id=" => $post["handle"]);
              $retorno   = $crud->update($arrEditar, $arrayCond);
              return $retorno;
            }
          }

          public function cadastrar($post) {
            $pdo = Conexao::getInstance();
            $arrInserir = array();
            foreach ($post as $key => $value) {
              if ($key != "handle") {
                $arrInserir[$key] =  $value;
              }
            }
            $crud = bd::getInstance($pdo,$this->nom_tabela);
            $retorno   = $crud->insert(array_merge($arrInserir, ['idgeral' => 0]));
            return $retorno;
          }

          public function excluir($handle) {
            $pdo = Conexao::getInstance();
            $crud = bd::getInstance($pdo,$this->nom_tabela);
            $crud->delete(array("id" => $handle));
            return true;
          }	          
        }
      ?>
    