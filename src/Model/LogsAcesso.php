<?php
require_once('conexao.php');
require_once('bd.php');
class LogsAcesso {

	public $id_log_acesso = 0;
	public $login = '';
	public $sucesso = '';
	public $data = '';
	public $ip = '';
  private $nom_tabela = 'logs_acesso';
	
  public function __construct() {
    $this->id_log_acesso = 0;
    $this->login = '';
    $this->sucesso = '';
    $this->data = '';
    $this->ip = '';
  }

	public function registraLog($parametros) {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);

		$crud->insert($parametros);
	}

  public function consultaAcessos($handle) {
		$pdo = Conexao::getInstance();
		$crud = bd::getInstance($pdo,$this->nom_tabela);    

		$sql = "SELECT * FROM ".$this->nom_tabela." WHERE login = ? ORDER BY data DESC";
		
		$arrayCond = array($handle);  		
		
		$dados = $crud->getSQLGeneric($sql,$arrayCond,TRUE);
		
		return $dados;    
  }
}
?>
