<?php
require_once('conexao.php');
require_once('bd.php');
class PercentualCustoClienteGrupo
{
	public $codigoPercentualCustoClienteGrupo = 0;
	public $codigoClienteGrupo = 0;
	public $codigoProduto = 0;
	public $percentual = 0;
	public $aplicacao = 'A';
	public $definicao_preco = 2;

	public $nom_tabela = 'PercentualCustoClienteGrupo';


	public function __construct()
	{
		$this->codigoPercentualCustoClienteGrupo = '';
		$this->codigoPercentualCustoClienteGrupo = 0;
		$this->codigoClienteGrupo = 0;
		$this->codigoProduto = 0;
		$this->percentual = 0;
	}

	public function listarPercentualPorProduto($handleProduto)
	{
		$pdo = Conexao::getInstance();

		$crud = bd::getInstance($pdo, $this->nom_tabela);


		$sql = "SELECT
              percentual_custo.codigoClienteGrupo,
              percentual,
			  aplicacao,
			  definicao_preco
            FROM
              {$this->nom_tabela} percentual_custo
            WHERE
              percentual_custo.codigoProduto = ?";

		$arrayParam = array($handleProduto);

		$dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

		$arrDados = [];

		foreach ($dados as $dado) {
			$arrDados[$dado->codigoClienteGrupo] = [
				'percentual' => $dado->percentual,
				'aplicacao' => $dado->aplicacao,
				'definicao_preco' => $dado->definicao_preco
			];
		}

		return $arrDados;

		//
	}
}
