<?php
include_once(__DIR__ . '/../Controller/CommonController.php');

class Assets
{
    public $AssetsId;
    public $UserId;
    public $Title;
    public $Date = "0000-00-00 00:00:00";
    public $CategoryId;
    public $AccountId;
    public $Amount = "0";
    public $Description;
    public $IDCONTA;
    public $cdtvencimento = "0000-00-00 00:00:00";
    public $cdtpagamento = "0000-00-00 00:00:00";
    public $doc;
    public $ndoc;
    public $idreceber;
    public $mes;
    public $dtinclusao;
    public $dtalteracao;
    public $nom_tabela = "assets";
    public $data_pagamento_loja = "";
    public $situacao = 0;
    private $common;
    public function __construct()
    {
        $this->common = new CommonController();
        // $this->AssetsId = 0;
        // $this->UserId = 0;
        // $this->Title = "";
        // $this->Date = date("Y-m-d");
        // $this->CategoryId = 0;
        // $this->AccountId = 0;
        // $this->Amount = 0;
        // $this->Description = "";
        // $this->IDCONTA = 0;
        // $this->cdtvencimento = "0000-00-00 00:00:00";
        // $this->cdtpagamento = "0000-00-00 00:00:00";
        // $this->doc = "";
        // $this->ndoc = "";
        // $this->idreceber = 0;
        // $this->mes = "";
        // $this->dtinclusao = "";
        // $this->dtalteracao = "";
        // $this->data_pagamento_loja = "";
        // $this->situacao = 0;        
    }
    public function listarTodos(
      $arrParametros = []
    ) {

      // $vendedor = 0;
      // if (isset($arrParametros['vendedor']) && !empty($arrParametros['vendedor'])) {
      //   $vendedor = $arrParametros['vendedor'];
      //   unset($arrParametros['vendedor']);
      // }

      $configuracoes = new Configuracoes();

      $modelo_empresa = $configuracoes->listarConfiguracao('modelo_empresa')->valor;

      $pagina_atual = 1;
      if (isset($arrParametros['pagina_atual'])) {
        $pagina_atual = $arrParametros['pagina_atual'];
      }

      $linha_inicial = 0;
      if (isset($arrParametros['linha_inicial'])) {
        $linha_inicial = $arrParametros['linha_inicial'];
      }

      $coluna = "";
      if (isset($arrParametros['coluna'])) {
        $coluna = $arrParametros['coluna'];
      }
      
      $buscar = "";
      if (isset($arrParametros['buscar'])) {
        $buscar = $arrParametros['buscar'];
      }    

        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $where = "";
        if ($coluna != "" && $buscar != "") {
            $where = sprintf(
                ' WHERE %s LIKE UPPER("%%%s%%") ',
                $coluna,
                strtoupper($buscar)
            );
        }
        
        $paginacao = '';
        $quantidade = isset($arrParametros['quantidade']) ? $arrParametros['quantidade'] : '';
        $linha_inicial = isset($arrParametros['inicio']) ? $arrParametros['inicio'] : 0;
        $ordem = isset($arrParametros['ordem']) ? $arrParametros['ordem'] : 'AssetsId Desc';

        if ($quantidade > 0) {
          $qtd_registros = $quantidade;
          $paginacao = ' LIMIT ' . $qtd_registros;
        }
        if ($pagina_atual > 0) {
          if ($pagina_atual > 0 && $linha_inicial > 0) {
            $paginacao = " LIMIT $qtd_registros OFFSET " . ($linha_inicial);
          }
        }

        if (isset($arrParametros['filtro']) && !empty($arrParametros['filtro'])) {
          $tamanhoFiltros = count($arrParametros['filtro']);
          $where = ' WHERE ';
          $and = ' ';
          $aux = 0;
          $condicao = ' = ';
          $arrDatas = [
            'DataVencimento' => 'cdtvencimento',
            'DataLancamento' => 'Date',
            'DataPagamento' => 'cdtpagamento'
          ];

          foreach ($arrParametros['filtro'] as $key => $value) {
            if (trim($value) && $value == '') {
              $tamanhoFiltros--;
              continue;
            }
            $coluna = $key;
            $valor = $value;
            switch ($key) {
              case 'UserId';
                $coluna = 'Orcamento.id_empresa';
                break;

              case 'CategoryId';
              $coluna = 'category.'.$key;
              break;      
              
              case 'AccountId';
              $coluna = 'account.'.$key;
              break;

              case 'vendedor';
              $coluna = 'Orcamento.criado_por';
              break;

              case 'situacao';
              $coluna = 'assets.situacao';
              break;              

              case 'filtroDataVencimentoInicio';
              case 'filtroDataLancamentoInicio';
              case 'filtroDataPagamentoInicio';
              $condicao = ' >= ';
              $coluna = str_replace(['filtro','Inicio'],'',$key);
              $coluna = $arrDatas[$coluna];
              $valor = $coluna != 'cdtvencimento' ? $this->common->dateFormat($value) . ' 00:00:00' : $this->common->dateFormat($value,'Y-m-d');
              break;

              case 'filtroDataVencimentoFim';
              case 'filtroDataLancamentoFim';
              case 'filtroDataPagamentoFim';
              $condicao = ' <= ';
              $coluna = str_replace(['filtro','Fim'],'',$key);
              $coluna = $arrDatas[$coluna];              
              $valor = $coluna != 'cdtvencimento' ? $this->common->dateFormat($value) . ' 00:00:00' : $this->common->dateFormat($value,'Y-m-d');
              break;              
            }
            $where .= sprintf('%s %s %s "%s" ',$and, $coluna, $condicao , $valor);
            $aux++;
            if ($aux <= $tamanhoFiltros) {
              $and = ' AND ';
            }
          }
        }

        $sql = "
                  SELECT 
                    assets.AssetsId,
                    assets.UserId,
                    assets.Title,
                    assets.Date,
                    assets.CategoryId,
                    assets.AccountId,
                    assets.Amount,
                    assets.Description,
                    assets.IDCONTA,
                    assets.cdtvencimento,
                    assets.doc,
                    assets.idreceber,
                    assets.mes,
                    assets.dtinclusao,
                    assets.dtalteracao,
                    CAST(assets.ndoc AS INT) as ndoc,
                    Clientes.RazaoSocial,
                    category.CategoryName,
                    Empresas.RazaoSocial as Empresa,
                    account.AccountName nomeConta,
                    IF (DAYNAME(assets.cdtpagamento) IS NOT NULL, assets.cdtpagamento,'') as cdtpagamento,
                    IF (DAYNAME(assets.data_pagamento_loja) IS NOT NULL, assets.data_pagamento_loja,'') as data_pagamento_loja,
                    assets.situacao as situacao_conta_receber,
                    CONCAT(usuarios.NomeUsuario, IF (usuarios.situacao = 0, ' (Vendedor Removido)', '') ) as NomeUsuario,
                    fila_documento_fiscal.id_fila,
                    fila_documento_fiscal.id_documento,
                    fila_documento_fiscal.situacao as situacao_nota_servico,
                    fila_documento_fiscal.link_pdf,
                    '' as editar,
                    '' as excluir
                  FROM {$this->nom_tabela}
                    LEFT JOIN Clientes ON Clientes.CodigoCliente = assets.Title 
                    LEFT JOIN category ON category.CategoryId = assets.CategoryId
                    LEFT JOIN Empresas ON assets.UserId = Empresas.CodigoEmpresa
                    LEFT JOIN Orcamento ON Orcamento.Cd_Orcamento = assets.ndoc
                    LEFT JOIN usuarios ON usuarios.CodigoUsuario = Orcamento.criado_por
                    LEFT JOIN fila_documento_fiscal ON " . ($modelo_empresa != 'gestao-interna' ? " assets.ndoc = ( SELECT fila_documento_fiscal.id_pedido FROM fila_documento_fiscal LIMIT 1 )" : " assets.AssetsId = fila_documento_fiscal.id_pedido " ) . "
                    LEFT JOIN account ON account.AccountId = assets.AccountId $where ORDER BY $ordem, fila_documento_fiscal.data_criacao DESC $paginacao
                ";
        // echo "<pre>$sql</pre>";
        // exit;
        $dados = $crud->getSQLGeneric($sql);
        return $dados;
    }
    public function listar($handle)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $sql = "
          SELECT ". $this->nom_tabela . ".*,
            IF (DAYNAME(assets.data_pagamento_loja) IS NOT NULL,
            assets.data_pagamento_loja,'') as data_pagamento_loja,
            fila_documento_fiscal.id_fila,
            fila_documento_fiscal.id_documento,
            fila_documento_fiscal.situacao as situacao_nota_servico,
            fila_documento_fiscal.link_pdf
          FROM " 
            . $this->nom_tabela . "
            LEFT JOIN fila_documento_fiscal ON fila_documento_fiscal.id_pedido = assets.AssetsId
          WHERE
            AssetsId = ?";
        $arrayParam = [$handle];
        $dados = $crud->getSQLGeneric($sql, $arrayParam, false);
        return $dados;
    }
    public function listarTodosTotal($arrParametros = [])
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);

        $where = '';
        if (isset($arrParametros['filtro']) && !empty($arrParametros['filtro'])) {
          $tamanhoFiltros = count($arrParametros['filtro']);
          $where = ' WHERE ';
          $and = ' ';
          $aux = 0;
          $condicao = ' = ';
          $arrDatas = [
            'DataVencimento' => 'cdtvencimento',
            'DataLancamento' => 'Date',
            'DataPagamento' => 'cdtpagamento'
          ];

          foreach ($arrParametros['filtro'] as $key => $value) {
            if (trim($value) && $value == '') {
              $tamanhoFiltros--;
              continue;
            }
            $coluna = $key;
            $valor = $value;
            switch ($key) {
              case 'UserId';
                $coluna = 'Orcamento.id_empresa';
                break;

              case 'CategoryId';
              $coluna = 'category.'.$key;
              break;      
              
              case 'AccountId';
              $coluna = 'account.'.$key;
              break;

              case 'vendedor';
              $coluna = 'Orcamento.criado_por';
              break;

              case 'situacao';
              $coluna = 'assets.situacao';
              break;              

              case 'filtroDataVencimentoInicio';
              case 'filtroDataLancamentoInicio';
              case 'filtroDataPagamentoInicio';
              $condicao = ' >= ';
              $coluna = str_replace(['filtro','Inicio'],'',$key);
              $coluna = $arrDatas[$coluna];
              $valor = $coluna != 'cdtvencimento' ? $this->common->dateFormat($value) . ' 00:00:00' : $this->common->dateFormat($value,'Y-m-d');
              break;

              case 'filtroDataVencimentoFim';
              case 'filtroDataLancamentoFim';
              case 'filtroDataPagamentoFim';
              $condicao = ' <= ';
              $coluna = str_replace(['filtro','Fim'],'',$key);
              $coluna = $arrDatas[$coluna];              
              $valor = $coluna != 'cdtvencimento' ? $this->common->dateFormat($value) . ' 00:00:00' : $this->common->dateFormat($value,'Y-m-d');
              break;              
            }
            $where .= sprintf('%s %s %s "%s" ',$and, $coluna, $condicao , $valor);
            $aux++;
            if ($aux <= $tamanhoFiltros) {
              $and = ' AND ';
            }
          }
        }

        $sql = "
        SELECT count(*) as total_registros
        FROM {$this->nom_tabela}
          LEFT JOIN Clientes ON Clientes.CodigoCliente = assets.Title 
          LEFT JOIN category ON category.CategoryId = assets.CategoryId
          LEFT JOIN Empresas ON assets.UserId = Empresas.CodigoEmpresa
          LEFT JOIN Orcamento ON Orcamento.Cd_Orcamento = assets.ndoc
          LEFT JOIN usuarios ON usuarios.CodigoUsuario = Orcamento.criado_por
          LEFT JOIN fila_documento_fiscal ON assets.ndoc = (SELECT fila_documento_fiscal.id_pedido FROM fila_documento_fiscal LIMIT 1)
          LEFT JOIN account ON account.AccountId = assets.AccountId $where
      ";        
        $dados = $crud->getSQLGeneric($sql, null, false);
        return $dados->total_registros;
    }
    public function editar($post)
    {

        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $arrEditar = [];
        unset(
          $post['tipoRecebimento'],
          $post['diaVencimentoSemana'],
          $post['diaVencimento'],
          $post['quantidadeRepeticao'],
          $post['quantidadeParcelas']
      );

        foreach ($post as $key => $value) {
          if ($key != "handle") {
            if ($key == 'Amount') {
              $value = $this->common->monetaryValue($value);
            }
            if ($key == 'cdtpagamento' || $key == 'date') {
              if ($value == '') {
                $value = '0000-00-00 00:00:00';
              } else {
                $value = $this->common->dateFormat($value);
              }
            }
            if ($key == 'cdtvencimento') {
              $value = $this->common->dateFormat($value,'Y-m-d');
            }            
            $arrEditar[$key] = $value;
          }
        }
        
        $arrayCond = ["AssetsId=" => $post["handle"]];
        $retorno = $crud->update($arrEditar, $arrayCond);
        return $retorno;
    }
    public function cadastrar($post)
    {
      try {
          $pdo = Conexao::getInstance();
          $arrInserir = [];

          $tipoRecebimento = isset($post['tipoRecebimento'])  ? $post['tipoRecebimento'] : 'U';
          $diaVencimentoSemana = isset($post['diaVencimentoSemana']) ? $post['diaVencimentoSemana'] : '';
          $diaVencimento = isset($post['diaVencimento']) ? $post['diaVencimento'] : '';
          $quantidadeParcelas = isset($post['quantidadeParcelas']) ? $post['quantidadeParcelas'] : '1';
          $quantidadeRepeticao = $quantidadeParcelas !== '' ? $quantidadeParcelas : $post['quantidadeRepeticao'];
          unset(
              $post['tipoRecebimento'],
              $post['diaVencimentoSemana'],
              $post['diaVencimento'],
              $post['quantidadeRepeticao'],
              $post['quantidadeParcelas']
          );

          foreach ($post as $key => $value) {
            if ($key != "handle") {
              if ($key == 'Amount') {
                $value = $this->common->monetaryValue($value);
              }
              if ($key == 'cdtpagamento' || $key == 'date' || $key == 'data_pagamento_loja') {
                if ($value == '') {
                  $value = '0000-00-00 00:00:00';
                } else {
                  $value = $this->common->dateFormat($value);
                }
              }
              if ($key == 'cdtvencimento') {
                $value = $this->common->dateFormat($value,'Y-m-d');
              }            
              $arrInserir[$key] = $value;
            }
          }
          
          $arrInserir = array_merge($arrInserir, [
            'AccountId' => $arrInserir['AccountId'] ,
            'IDCONTA' => 0,
            'doc' => 'ped',
            'ndoc' => isset($arrInserir['ndoc']) ? $arrInserir['ndoc'] : '',
            'idreceber' => 1,
            'mes' => date('m'),
            'dtinclusao' => date('Y-m-d H:i:s'),
            'dtalteracao' => date('Y-m-d H:i:s')
          ]);        
          //  print_r($arrInserir);
           

          $weekDays = array('sunday', 'monday', 'tuesday', 'wednesday','thursday','friday', 'saturday');
          $arrVencimentos = new DateTime($arrInserir['cdtvencimento']);
          if ($diaVencimentoSemana !== '') {
            $weekday = new DateTime($weekDays[$diaVencimentoSemana]);
            $endDate = clone $weekday;
            $endDate->modify('+'.($quantidadeRepeticao * 7).' days');
            $dateInterval = new DateInterval('P1W');
            $arrVencimentos = new DatePeriod($weekday, $dateInterval, $endDate);
          } else {
            if ($diaVencimento !== '') {
              switch ($tipoRecebimento) {
                case 'M':
                default:
                  $intervaloDias = 30;
                  $intervalo = 'P1M';
                  break; 
                case 'T':
                  $intervaloDias = 90;
                  $intervalo = 'P3M';
                  break;
                case 'S':
                  $intervaloDias = 180;
                  $intervalo = 'P6M';
                  break;
                case 'A':
                  $intervaloDias = 365;
                  $intervalo = 'P1Y';
                  break;                                                
              }
              $weekday = new DateTime($arrInserir['cdtvencimento']);
              $endDate = clone $weekday;
              $endDate->modify('+'.($quantidadeRepeticao * $intervaloDias).' days');
              $dateInterval = new DateInterval($intervalo);
              $arrVencimentos = new DatePeriod($weekday, $dateInterval, $endDate);
            } else {
              $weekday = new DateTime($arrInserir['cdtvencimento']);
              $endDate = clone $weekday;
              $endDate->modify('+1 days');
              $dateInterval = new DateInterval('P1D');
              $arrVencimentos = new DatePeriod($weekday, $dateInterval, $endDate);
              $diaVencimento = date('d', strtotime($arrInserir['cdtvencimento']));
            }
          }
          // print_r($arrVencimentos);exit;

          $crud = bd::getInstance($pdo, $this->nom_tabela);
          $handleAssets = 0;
          foreach ($arrVencimentos as $key => $vencimento) {
            $cadastro = $crud->insert(array_merge($arrInserir, ['cdtvencimento' => $vencimento->format(sprintf('Y-m-%s', $diaVencimento))]));
            if ($handleAssets == 0) {
              $handleAssets = $cadastro;
            }
          }

          return $cadastro;
        } catch (Exception $e) {
          echo 'Erro ao salvar uma conta a receber ' . $e->getMessage();
          return false;
        }
    }
    public function excluir($handle)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $crud->delete(["AssetsId" => $handle]);
        return true;
    }

    public function listarRecebimentosPorPeriodo($arrFiltros) {
      $pdo = Conexao::getInstance();
      $crud = bd::getInstance($pdo, $this->nom_tabela);

      $where = '';
      if (!empty($arrFiltros)) {
        if (isset($arrFiltros['periodo_inicial']) && isset($arrFiltros['periodo_final'])) {
           $where = sprintf(
            'AND assets.cdtpagamento BETWEEN "%s" AND "%s"',
            $arrFiltros['periodo_inicial'],
            $arrFiltros['periodo_final'],
           );
        }
      }

      $sql = sprintf(
        'SELECT
          SUM(Amount) as total,
          assets.UserId,
          account.tipo
        FROM
          assets
        LEFT JOIN category on assets.CategoryId = category.CategoryId 
        LEFT JOIN account on assets.AccountId = account.AccountId
        WHERE
          account.tipo <> 4
          %s
        GROUP BY assets.UserId, account.tipo
        ',
        $where
        )
        ;
      // echo "<pre>$sql</pre>";
      return $crud->getSQLGeneric($sql, [], true);
    }

    public function listarRecebimentosFuturo($arrFiltros = []) {
      $pdo = Conexao::getInstance();
      $crud = bd::getInstance($pdo, $this->nom_tabela);
      $where = ' WHERE (cdtpagamento IS NULL OR cdtpagamento = "")';
      if (!empty($arrFiltros)) {
        if (isset($arrFiltros['periodo_inicial'])) {
           $where .= sprintf(
            ' AND cdtvencimento >= "%s"',
            $arrFiltros['periodo_inicial']
           );
        }
      }

      $sql = sprintf('
        SELECT
          SUM(Amount) as total,
          UserId
        FROM
          assets
          %s 
        GROUP BY assets.UserId
      ',
      $where
    );
      //  echo "<pre>$sql</pre>";
      return $crud->getSQLGeneric($sql, [], true);
    }    
}