<?php
require_once('conexao.php');
require_once('bd.php');
class GrupoTributario
{
  public $id_grupo_tributario = 0;
  public $id_empresa = 0;
  public $nome = '';
  public $codigo_origem;
  public $codigo;

  public $nom_tabela = 'grupo_tributario';


  public function __construct()
  {
    $this->id_grupo_tributario = 0;
    $this->nome = '';
    $this->id_empresa = 0;
    $this->codigo_origem = 0;
    $this->codigo = '';
  }

  public function listarTodos($pagina_atual = 0, $linha_inicial = 0, $coluna = '', $buscar = '', $quantidade = '', $ordem = '')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';

    if (!empty($coluna) && (!empty($buscar))) {
      $where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
      if ($coluna !== 'id_grupo_tributario') {
        $where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
      }
    }

    $paginacao = " LIMIT " . QTDE_REGISTROS;
    $qtd_registros = QTDE_REGISTROS;
    if ($quantidade > 0) {
      $qtd_registros = $quantidade;
    }
    if ($pagina_atual > 0) {
      $paginacao = ' LIMIT ' . $qtd_registros;
      if ($pagina_atual > 0 && $linha_inicial > 0) {
        $paginacao = " LIMIT $qtd_registros OFFSET " . ($linha_inicial);
      }
    }

    if ($ordem == '') {
      $ordem = 'codigo ASC';
    }

    $sql = "SELECT * FROM " . $this->nom_tabela . $where . " ORDER BY " . $ordem . $paginacao;

    // echo $sql;exit;

    $dados = $crud->getSQLGeneric($sql);
    return $dados;

    //
  }

  public function listarTodosTotal($coluna = '', $buscar = '')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';

    if (!empty($coluna) && (!empty($buscar) || $buscar >= 0)) {
      $where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
      if ($coluna !== 'id_grupo_tributario') {
        $where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
      }
    }

    $sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela . $where;

    $dados = $crud->getSQLGeneric($sql, null, FALSE);

    return $dados->total_registros;

    //
  }

  public function listarGrupoTributario($handle)
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql = "SELECT * FROM " . $this->nom_tabela . " WHERE id_grupo_tributario = ?";

    $arrayParam = array($handle);

    $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    return $dados;

    //
  }

  public function listarGrupoTributariosPorEstado($siglaEstado)
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql = "
      SELECT
        municipios.*
      FROM
        municipios
      INNER JOIN
        estados ON municipios.codigo_uf = estados.codigo_uf
      WHERE
	      estados.uf = '" . $siglaEstado . "' 
    ";

    $dados = $crud->getSQLGeneric($sql, [], TRUE);

    return $dados;

    //
  }

  public function editarGrupoTributario($post)
  {
    $pdo = Conexao::getInstance();

    $arrayCliente = array();
    foreach ($post as $key => $value) {
      if ($key != 'handle' && $key != 'id_grupo_tributario') {
        $arrayCliente[$key] = $value;
      }
    }

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $arrayCond = array('id_grupo_tributario=' => $post['handle']);
    $retorno = $crud->update($arrayCliente, $arrayCond);

    return $retorno;
  }

  public function cadastrarGrupoTributario($post)
  {
    $pdo = Conexao::getInstance();

    $arrayCliente = array();
    foreach ($post as $key => $value) {
      if ($key != 'handle' && $key != 'id_grupo_tributario') {
        $arrayCliente[$key] = $value;
      }
    }

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $retorno = $crud->insert($arrayCliente);

    return $retorno;
  }

  public function excluir($handle)
  {
    $pdo = Conexao::getInstance();
    $crud = bd::getInstance($pdo, $this->nom_tabela);
    $crud->delete(array('id_grupo_tributario' => $handle));
  }

  public function getListaOrigens()
  {
    $arrOrigem[0] = 'Nacional, exceto as indicadas nos códigos 3 a 5';
    $arrOrigem[1] = 'Estrangeira - Importação direta, exceto a indicada no código 6';
    $arrOrigem[2] = 'Estrangeira - Adquirida no mercado interno, exceto a indicada no código 7';
    $arrOrigem[3] = 'Nacional, mercadoria ou bem com Conteúdo de Importação superior a 40% e inferior ou igual a 70%';
    $arrOrigem[4] = 'Nacional, cuja produção tenha sido feita em conformidade com os processos produtivos básicos de que tratam as legislações citadas nos Ajustes';
    $arrOrigem[5] = 'Nacional, mercadoria ou bem com Conteúdo de Importação inferior ou igual a 40%';
    $arrOrigem[6] = 'Estrangeira - Importação direta, sem similar nacional, constante em lista da CAMEX';
    $arrOrigem[7] = 'Estrangeira - Adquirida no mercado interno, sem similar nacional, constante em lista da CAMEX';
    $arrOrigem[8] = 'Nacional, mercadoria ou bem com Conteúdo de Importação superior a 70%';

    return $arrOrigem;
  }
}