<?php
require_once('conexao.php');
require_once('bd.php');
class Produto
{
  public $CodigoProduto;
  public $DescricaoProduto;
  public $DescricaoTraduzida;
  public $CodigoProdutoFabricante;
  public $CodigoProdutoFabrica;
  public $UnidadeProduto;
  public $MedidaVara;
  public $CodigoGrupo;
  public $PrecoCusto;
  public $PrecoVenda;
  public $CodigoFornecedor;
  public $Quantidade;
  public $QuantidadeMinima;

  public $QuantidadeMinimaCm;

  public $QuantidadeCm;
  public $DataUltimaCompra;
  public $ValorUltimaCompra;
  public $ClassificacaoFiscal;
  public $SituacaoTributaria;
  public $QuantidadeMaxima;
  public $PrecoMinimo;
  public $QtdadeSaidaMesAtual;
  public $QtdadeSaidaMesPassado;
  public $QtdadeSaidaMesRetrasado;
  public $Moeda;
  public $CodigoIPI;
  public $OrigemProduto;
  public $IndServico;
  public $CodigoGrupoEstoque;
  public $Detalhes;
  public $NovoCodigo;
  public $Desenho;
  public $PrecoVendaMaoObra;
  public $Situacao;

  public $codigo_ncm;
  public $codigo_cest;
  public $codigo_cfop;
  public $codigo_origem;
  public $codigo_cst;
  public $codigo_crt;
  public $aliquota_icms;

  public $aliquota_pis;
  public $aliquota_cofins;

  public $codigo_csosn;

  public $id_grupo_tributario;
  public $descontinuado;
  public $componente;
  public $definicao_preco;
  public $margem_venda;
  private $nom_tabela = 'Produtos';
  private $order_by_default = 'DescricaoProduto';

  public function __construct()
  {
    $this->CodigoProduto = '';
    $this->DescricaoProduto = '';
    $this->DescricaoTraduzida = '';
    $this->CodigoProdutoFabricante = '';
    $this->UnidadeProduto = '';
    $this->MedidaVara = 0;
    $this->CodigoGrupo = '';
    $this->PrecoCusto = 0;
    $this->PrecoVenda = 0;
    $this->CodigoFornecedor = '';
    $this->Quantidade = 0;
    $this->QuantidadeMinima = 0;
    $this->QuantidadeCm = 0;
    $this->QuantidadeMinimaCm = 0;
    $this->DataUltimaCompra = '';
    $this->ValorUltimaCompra = '';
    $this->ClassificacaoFiscal = '';
    $this->SituacaoTributaria = '';
    $this->QuantidadeMaxima = '';
    $this->PrecoMinimo = '';
    $this->QtdadeSaidaMesAtual = '';
    $this->QtdadeSaidaMesPassado = '';
    $this->QtdadeSaidaMesRetrasado = '';
    $this->Moeda = '';
    $this->CodigoIPI = '';
    $this->OrigemProduto = '';
    $this->IndServico = '';
    $this->CodigoGrupoEstoque = '';
    $this->Detalhes = '';
    $this->NovoCodigo = '';
    $this->Desenho = 0;
    $this->PrecoVendaMaoObra = 0;
    $this->Situacao = 1;
    $this->id_grupo_tributario = 0;
    $this->descontinuado = 0;

    $this->codigo_ncm = 0;
    $this->codigo_cest = 0;
    $this->codigo_cfop = 0;
    $this->codigo_origem = 0;
    $this->codigo_cst = 0;
    $this->codigo_crt = 0;
    $this->aliquota_icms = 0;
    $this->aliquota_pis = 0;
    $this->aliquota_cofins = 0;
    $this->codigo_csosn = 0;
    
    $this->componente = 0;
    $this->definicao_preco = 1;
    $this->margem_venda = 0;
  }


  public function listarTodos($pagina_atual = 0, $linha_inicial = 0, $coluna = '', $buscar = '', $order = "", $arrayFiltro = array(), $quantidade = '')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';
    $where_palavra_chave = '';
    if ($coluna != '' && $buscar != '') {
      $where = sprintf(' WHERE %s LIKE UPPER("%s%%") ', $coluna, strtoupper($buscar));
    }

    if (sizeof($arrayFiltro)) {
      $_and = ' AND ';
      if (empty($where)) {
        $where = ' WHERE ';
        $_and = '';
      }

      $arrayFiltroLength = count($arrayFiltro);      
      $aux = 1;
      foreach ($arrayFiltro as $coluna => $valor) {
        if ($coluna == 'situacao') {
          $where .= $_and . ' Situacao IN (' . implode(',', $valor) . ') ';
          $_and = ' AND ';
        }
        
        if ($coluna == 'componente') {
          $where .= $_and . ' componente = ' . $valor;
          $_and = ' AND ';
        }        

        if ($coluna == 'quantidade') {
          $where .= $_and . ' Quantidade >= ' . $valor;
          $_and = ' AND ';
        }
        
        if ($coluna == 'unidade_medida') {
          $where .= $_and . ' UnidadeProduto = ' . $valor;
          $_and = ' AND ';
        }        
        
		    if ($coluna == 'fornecedor') {
          $where .= $_and . $this->nom_tabela . '.CodigoFornecedor = ' . $valor;
          $_and = ' AND ';
        }                

        if ($coluna == 'CodigoProduto') {
          $where .= $_and . ' CodigoProduto IN (' . implode(',', $valor) . ') ';
          $_and = ' AND ';
        }

        if (in_array($coluna, ['NovoCodigo', 'DescricaoProduto'])) {
          $_and = $aux < $arrayFiltroLength ? ' OR ' : '';
          $where_palavra_chave .= sprintf(' UPPER(%s) LIKE "%%%s%%" %s ', $coluna, strtoupper($valor), $_and);
        }

        $aux++;

        // $where .= sprintf(' UPPER(%s) LIKE "%s%%" %s ',$coluna,strtoupper($valor), $_and);
      }
    }


    if ($where_palavra_chave != '') {
		$where_palavra_chave = sprintf(' %s ( %s ) ', $where != ' WHERE ' ? 'AND' : '', $where_palavra_chave);
	}

    $paginacao = " ";
    $qtd_registros = QTDE_REGISTROS;
    if ($quantidade > 0) {
      $qtd_registros = $quantidade;
      $paginacao = ' LIMIT ' . $qtd_registros;
    }
    if ($pagina_atual > 0) {
      if ($pagina_atual > 0 && $linha_inicial > 0) {
        $paginacao = " LIMIT $qtd_registros OFFSET " . ($linha_inicial);
      }
    }

    if (empty($order)) {
      $order = $this->order_by_default;
    }

    $sql = "
      SELECT
        *,
        PrecoCusto + (PrecoCusto * (margem_venda / 100)) as preco_venda,
        UnidadesMedida.DescricaoUnidadeMedida,
        UnidadesMedida.NomeUnidadeMedida,
        UnidadesMedida.MetragemLinear,
        Fornecedores.CGC,
        IF((
          SELECT
            Moldura_Item_Orcamento.Cd_Orcamento FROM `Moldura_Item_Orcamento`
          WHERE
            Moldura_Item_Orcamento.Cd_Produto = Produtos.CodigoProduto
            LIMIT 1
        ) IS NULL, 0, 1) as qtdUso
      FROM " . $this->nom_tabela . "
      LEFT JOIN UnidadesMedida ON Produtos.UnidadeProduto = UnidadesMedida.CodigoUnidadeMedida
      LEFT JOIN Fornecedores ON Produtos.CodigoFornecedor = Fornecedores.CodigoFornecedor " . $where  . $where_palavra_chave .  "
      ORDER BY " . $order . $paginacao;
       //echo "<pre>$sql</pre><hr>";
     // exit;
    $dados = $crud->getSQLGeneric($sql);
    return $dados;
  }

  public function listarProduto($handle, $coluna = 'CodigoProduto')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql = "SELECT *, MetragemLinear, DescricaoUnidadeMedida FROM " . $this->nom_tabela . " LEFT JOIN UnidadesMedida ON UnidadeProduto = CodigoUnidadeMedida WHERE $coluna = ?";
    $arrayParam = array($handle);

    $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    return $dados;

    //
  }

  public function listarTodosTotal($arrayFiltro = [])
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';
    $where_palavra_chave = '';
    if (sizeof($arrayFiltro)) {
      $_and = ' AND ';
      if (empty($where)) {
        $where = ' WHERE ';
        $_and = '';
      }

      $arrayFiltroLength = count($arrayFiltro);
      $aux = 1;
      foreach ($arrayFiltro as $coluna => $valor) {
        if ($coluna == 'situacao') {
          $where .= $_and . ' Situacao IN (' . implode(',', $valor) . ') ';
          $_and = ' AND ';
        }
        
        if ($coluna == 'componente') {
          $where .= $_and . ' componente = ' . $valor;
          $_and = ' AND ';
        }        

        if ($coluna == 'quantidade') {
          $where .= $_and . ' Quantidade >= ' . $valor;
          $_and = ' AND ';
        }
        
        if ($coluna == 'unidade_medida') {
          $where .= $_and . ' UnidadeProduto = ' . $valor;
          $_and = ' AND ';
        }        
        
		if ($coluna == 'fornecedor') {
          $where .= $_and . $this->nom_tabela . '.CodigoFornecedor = ' . $valor;
          $_and = ' AND ';
        }

        if (in_array($coluna, ['NovoCodigo', 'DescricaoProduto'])) {
          $_and = $aux < $arrayFiltroLength ? ' OR ' : '';
          $where_palavra_chave .= sprintf(' UPPER(%s) LIKE "%%%s%%" %s ', $coluna, strtoupper($valor), $_and);
        }

        $aux++;

        // $where .= sprintf(' UPPER(%s) LIKE "%s%%" %s ',$coluna,strtoupper($valor), $_and);
      }
    }
    
    if ($where_palavra_chave != '') {
		$where_palavra_chave = sprintf(' %s ( %s ) ', $where != ' WHERE ' ? 'AND' : '', $where_palavra_chave);
	}
    
    $sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela . $where . $where_palavra_chave;

    $dados = $crud->getSQLGeneric($sql, null, FALSE);

    return $dados->total_registros;

    //
  }

  public function listarHistoricoCompra($CodigoProduto = 0, $CodigoFornecedor = 0)
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);
    if ($CodigoProduto > 0) {
      $sql = "SELECT hcp.historicoId , hcp.CodigoFornecedor , hcp.CodigoProduto , hcp.valorPago , hcp.dataCompra, for.RazaoSocial, pro.NovoCodigo, pro.DescricaoProduto FROM `historicoComprasProdutos` hcp INNER JOIN Fornecedores `for` ON (for.CodigoFornecedor = hcp.CodigoFornecedor) INNER JOIN `Produtos` pro ON (hcp.CodigoProduto = pro.CodigoProduto)  WHERE hcp.CodigoProduto = " . $CodigoProduto . " ORDER BY dataCompra DESC";
    } else {
      $sql = "SELECT hcp.historicoId , hcp.CodigoFornecedor , hcp.CodigoProduto , hcp.valorPago , hcp.dataCompra, for.RazaoSocial, pro.NovoCodigo, pro.DescricaoProduto FROM `historicoComprasProdutos` hcp INNER JOIN Fornecedores `for` ON (for.CodigoFornecedor = hcp.CodigoFornecedor) INNER JOIN `Produtos` pro ON (hcp.CodigoProduto = pro.CodigoProduto)  WHERE hcp.CodigoFornecedor = " . $CodigoFornecedor . " ORDER BY dataCompra DESC";
    }

    $dados = $crud->getSQLGeneric($sql);
    return $dados;

    //
  }

  public function produtoEmPedido($CodigoProduto)
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql = "SELECT Cd_Orcamento as total FROM `Moldura_Item_Orcamento` WHERE Cd_Produto = " . $CodigoProduto . ' LIMIT 1';

    $dados = $crud->getSQLGeneric($sql);

    return $dados;
  }

  public function excluirProduto($handle)
  {
    $pdo = Conexao::getInstance();
    $crud = bd::getInstance($pdo, $this->nom_tabela);
    $retorno = $crud->delete(array('CodigoProduto' => $handle));
  }

  public function excluirTodos()
  {
    $pdo = Conexao::getInstance();
    $crud = bd::getInstance($pdo, $this->nom_tabela);
    $sql = "TRUNCATE " . $this->nom_tabela;
    $crud->getSQLGeneric($sql);
  }  

  public function editarProduto($post)
  {    
    $pdo = Conexao::getInstance();
    $crud_historico_compras_produtos = bd::getInstance($pdo, 'historicoComprasProdutos');
    $crud_percentual_custo_cliente_grupo = bd::getInstance($pdo, 'PercentualCustoClienteGrupo');
    $crud_complexidade_produto = bd::getInstance($pdo, 'ComplexidadeProduto');
    $dataCompra = "";

    if (isset($post['excluirHistorico']) && !empty($post['excluirHistorico'])) {
      foreach ($post['excluirHistorico'] as $item_historico_excluir) {

        $crud_historico_compras_produtos->delete(array('historicoId' => $item_historico_excluir));
      }
    }
    unset($post['excluirHistorico']);

    if (isset($post['clienteGrupo']) && !empty($post['clienteGrupo'])) {
      foreach ($post['clienteGrupo'] as $key => $value) {
        
        $valorPercentual = !empty($value['percentual']) ? $value['percentual'] : 0;
        $valorPercentual = str_replace(".", "", $valorPercentual);
        $valorPercentual = str_replace(",", ".", $valorPercentual);
        
        $crud_percentual_custo_cliente_grupo->delete(array('codigoClienteGrupo' => $key, 'codigoProduto' => $post['handle']));
        $crud_percentual_custo_cliente_grupo->insert(array('codigoClienteGrupo' => $key, 'codigoProduto' => $post['handle'], 'percentual' => $valorPercentual, 'aplicacao' => $value['aplicacao'], 'definicao_preco' => $value['definicao_preco']));
      }
    }
    unset($post['clienteGrupo']);
    if (isset($post['complexidadeProduto']) && !empty($post['complexidadeProduto'])) {
      foreach ($post['complexidadeProduto'] as $key => $value) {


        $valor = !empty($value['valor']) ? $value['valor'] : 0;
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", ".", $valor);

        $crud_complexidade_produto->delete(array('codigoComplexidade' => $key, 'codigoProduto' => $post['handle']));
        $crud_complexidade_produto->insert(array('codigoComplexidade' => $key, 'codigoProduto' => $post['handle'], 'valor' => $valor, 'aplicacao' => $value['aplicacao'], 'definicao_preco' => $value['definicao_preco']));
      }
    }
    unset($post['complexidadeProduto']);

    if (isset($post['dataCompra']) && !empty($post['dataCompra'])) {
      $dataCompra = $post['dataCompra'];
      $dataCompra = date("Y-m-d", strtotime(str_replace('/', '-', $dataCompra)));
    }
    unset($post['dataCompra']);

    $CodigoFornecedorCompra = "";
    if (isset($post['CodigoFornecedorCompra']) && !empty($post['CodigoFornecedorCompra'])) {
      $CodigoFornecedorCompra = $post['CodigoFornecedorCompra'];
    }
    unset($post['CodigoFornecedorCompra']);

    $valorPago = "";
    if (isset($post['valorPago']) && !empty($post['valorPago'])) {
      $valorPago = $post['valorPago'];
      $valorPago = str_replace(".", "", $valorPago);
      $valorPago = str_replace(",", ".", $valorPago);
    }
    unset($post['valorPago']);

    $arrayProduto = array();
    foreach ($post as $key => $value) {
      // if ($key != 'handle' && $key != 'NovoCodigo')
      if ($key != 'handle') {
        $arrayProduto[$key] = $value;
      }

      if (!is_null($value) && $key == 'PrecoCusto' || $key == 'PrecoVenda' || $key == 'PrecoVendaMaoObra' || $key == 'MedidaVara' || $key == 'Desenho' || $key == 'QuantidadeCm' || $key == 'QuantidadeMinimaCm' || $key == 'margem_venda') {
        $_valor = str_replace(".","", $value);
        $_valor = str_replace(",", ".", $_valor);        
        $arrayProduto[$key] = (float) str_replace(",", ".", $_valor);
      }
    }

    if (!empty($dataCompra) && !empty($CodigoFornecedorCompra) && !empty($valorPago)) {
      $crud = bd::getInstance($pdo, 'historicoComprasProdutos');

      $retorno = $crud->insert(array('CodigoFornecedor' => $CodigoFornecedorCompra, 'CodigoProduto' => $post['handle'], 'valorPago' => $valorPago, 'dataCompra' => $dataCompra));
    }

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $arrayCond = array('CodigoProduto=' => $post['handle']);
    $retorno = $crud->update($arrayProduto, $arrayCond);

    return $retorno;
  }

  public function cadastrarProduto($post)
  {
    $pdo = Conexao::getInstance();
    // print_r($_POST);exit;
    unset($post['excluirHistorico']);
    if (isset($post['dataCompra']) && !empty($post['dataCompra'])) {
      $dataCompra = $post['dataCompra'];
      $dataCompra = date("Y-m-d", strtotime(str_replace('/', '-', $dataCompra)));
    }
    unset($post['dataCompra']);

    $CodigoFornecedorCompra = "";
    if (isset($post['CodigoFornecedorCompra']) && !empty($post['CodigoFornecedorCompra'])) {
      $CodigoFornecedorCompra = $post['CodigoFornecedorCompra'];
    }
    unset($post['CodigoFornecedorCompra']);

    $valorPago = "";
    if (isset($post['valorPago']) && !empty($post['valorPago'])) {
      $valorPago = $post['valorPago'];
      $valorPago = str_replace(".", "", $valorPago);
      $valorPago = str_replace(",", ".", $valorPago);
    }
    unset($post['valorPago']);

    $arrPercentualCusto = [];
    if (isset($post['clienteGrupo']) && !empty($post['clienteGrupo'])) {
      foreach ($post['clienteGrupo'] as $key => $value) {
        $valorPercentual = !empty($value['percentual']) ? $value['percentual'] : 0;
        $valorPercentual = str_replace(".", "", $valorPercentual);
        $valorPercentual = str_replace(",", ".", $valorPercentual);

        $arrPercentualCusto[] = array('codigoClienteGrupo' => $key, 'codigoProduto' => $post['handle'], 'percentual' => $valorPercentual, 'aplicacao' => $value['aplicacao'], 'definicao_preco' => $value['definicao_preco']);
      }
    }
    unset($post['clienteGrupo']);

    $arrComplexidade = [];
    if (isset($post['complexidadeProduto']) && !empty($post['complexidadeProduto'])) {
      foreach ($post['complexidadeProduto'] as $key => $value) {

        $valor = !empty($value['valor']) ? $value['valor'] : 0;
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", ".", $valor);        

        $arrComplexidade[] = array('codigoComplexidade' => $key, 'codigoProduto' => $post['handle'], 'valor' => $valor, 'aplicacao' => $value['aplicacao'], 'definicao_preco' => $value['definicao_preco']);
      }
    }

    unset($post['complexidadeProduto']);

    $arrayProduto = array();

    foreach ($post as $key => $value) {
      if ($key != 'handle') {
        if (!empty($value)) {
          $valor_campo = $value;
          if (!is_null($value) && $key == 'PrecoCusto' || $key == 'PrecoVenda' || $key == 'PrecoVendaMaoObra' || $key == 'MedidaVara' || $key == 'Desenho' || $key == 'QuantidadeMinimaCm' || $key == 'margem_venda' || $key == 'QuantidadeCm') {
            $valor_campo = str_replace(",", ".", $valor_campo);
          }
          $arrayProduto[$key] = $valor_campo;
        }
      }
    }

    $crud = bd::getInstance($pdo, $this->nom_tabela);
    $retorno = $crud->insert($arrayProduto);

    if (!is_numeric($retorno)) {
      return $retorno;
    }

    if (!empty($dataCompra) && !empty($CodigoFornecedorCompra) && !empty($valorPago)) {
      $crud = bd::getInstance($pdo, 'historicoComprasProdutos');
      $crud->insert(array('CodigoFornecedor' => $CodigoFornecedorCompra, 'CodigoProduto' => $retorno, 'valorPago' => $valorPago, 'dataCompra' => $dataCompra));
    }

    if (count($arrPercentualCusto) > 0) {
      $crud = bd::getInstance($pdo, 'PercentualCustoClienteGrupo');
      foreach ($arrPercentualCusto as $value) {
        $crud->insert(array_merge($value, ['codigoProduto' => $retorno]));
      }
    }

    if (count($arrComplexidade) > 0) {
      $crud = bd::getInstance($pdo, 'ComplexidadeProduto');
      foreach ($arrComplexidade as $value) {
        $crud->insert(array_merge($value, ['codigoProduto' => $retorno]));
      }
    }

    return $retorno;
  }

  public function alterarPrecoEmMassa($arrFiltros)
  {
    $pdo = Conexao::getInstance();
    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql_alteracao_preco_custo = '';
    $sql_alteracao_preco_venda = '';

    if ($arrFiltros['tipo_valor'] == 'preco_venda' || $arrFiltros['tipo_valor'] == 'ambos') {
      $sql_alteracao_preco_venda = 'PrecoVendaMaoObra =  PrecoVendaMaoObra ' . $arrFiltros['alteracao'];
      if ($arrFiltros['tipo'] == 'P') {
        $sql_alteracao_preco_venda = $sql_alteracao_preco_venda . 
          sprintf(
            ' (PrecoVendaMaoObra * '.$arrFiltros['valor'].' / 100 ) ',
            
          );
      } else {
        $sql_alteracao_preco_venda = $sql_alteracao_preco_venda . sprintf(
          ' ( '.$arrFiltros['valor'].' ) ',
          
        );
      }
    }

    if ($arrFiltros['tipo_valor'] == 'preco_custo' || $arrFiltros['tipo_valor'] == 'ambos') {
       $sql_alteracao_preco_custo = 'PrecoCusto =  PrecoCusto ' . $arrFiltros['alteracao'];
      if ($arrFiltros['tipo'] == 'P') {
         $sql_alteracao_preco_custo =  $sql_alteracao_preco_custo . 
          sprintf(
            ' (PrecoCusto * '.$arrFiltros['valor'].' / 100 ) ',
            
          );
      } else {
         $sql_alteracao_preco_custo =  $sql_alteracao_preco_custo . sprintf(
          ' ( '.$arrFiltros['valor'].' ) ',
          
        );
        
        if ($sql_alteracao_preco_venda != '') {
          $sql_alteracao_preco_custo = ', ' . $sql_alteracao_preco_custo;
        }
      }    
    }

    $sql = 'UPDATE Produtos SET ' . $sql_alteracao_preco_venda . $sql_alteracao_preco_custo;

    if ($arrFiltros['fornecedor'] &&  $arrFiltros['fornecedor'] != '') {
		  $sql = $sql . ' WHERE CodigoFornecedor = ' . $arrFiltros['fornecedor'];
	  }

    $stm = $pdo->prepare($sql);

    $retorno = $stm->execute();

    return $retorno;
  }
}
