<?php
require_once('conexao.php');
require_once('bd.php');
class HistoricoPedido {
  public $id_historico_pedido;
  public $id_pedido;
  public $id_situacao_de;
  public $id_situacao_para;
  public $observacao;
  public $id_usuario;
  public $data;
	public $nom_tabela = 'historicoPedido';

	
	public function __construct() {
    $this->id_historico_pedido = 0;
    $this->id_pedido = 0;
    $this->id_situacao_de = 0;
    $this->id_situacao_para = 0;
    $this->observacao = '';
    $this->id_usuario = 0;
    $this->data = '';
	}
	
	public function listarTodos($pagina_atual = 0,$linha_inicial = 0,$coluna = '',$buscar = '', $quantidade = '', $ordem = '') {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$where = '';
		
		if (!empty($coluna) && (!empty($buscar)) ) {
			$where = sprintf(' WHERE %s = "%s" ',$coluna,$buscar);
			if ($coluna !== 'codigoHistoricoPedido') {
				$where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ',$coluna,strtoupper($buscar));
			}
		}
		
		$paginacao = " LIMIT " . QTDE_REGISTROS;
		$qtd_registros = QTDE_REGISTROS;
		if ($quantidade > 0) {
            $qtd_registros = $quantidade;
        }
		if ( $pagina_atual > 0) {
			$paginacao = ' LIMIT '.$qtd_registros;
			if ($pagina_atual > 0 && $linha_inicial > 0) {
				$paginacao = " LIMIT $qtd_registros OFFSET ".($linha_inicial);
			}
		}
		
		if ($ordem == '') {
            $ordem = 'nome ASC';
		}
		
		$sql = "SELECT *, IF (status = 1, 'Ativo', 'Inativo') as statusDescricao FROM ".$this->nom_tabela.$where." ORDER BY ".$ordem.$paginacao;
		
		// echo $sql;exit;
		
		$dados = $crud->getSQLGeneric($sql);
		return $dados;
		
		//
	}

	public function listarTodosTotal($coluna = '',$buscar = '') {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$where = '';
		
		if (!empty($coluna) && (!empty($buscar) || $buscar >= 0) ) {
			$where = sprintf(' WHERE %s = "%s" ',$coluna,$buscar);
			if ($coluna !== 'codigoHistoricoPedido') {
				$where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ',$coluna,strtoupper($buscar));
			}
		}		
		
		$sql = "SELECT count(*) as total_registros FROM ".$this->nom_tabela.$where;		
		
		$dados = $crud->getSQLGeneric($sql,null,FALSE);		
		
		return $dados->total_registros;
		
		//
	}	
	
	public function editarHistoricoPedido($post) {
		$pdo = Conexao::getInstance();
		
		$arrInsert = array();
		foreach ($post as $key => $value) {
			if ($key != 'handle' && $key != 'codigoHistoricoPedido') {
				$arrInsert[$key] =  $value;
			}
		}
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$arrayCond = array('codigoHistoricoPedido=' => $post['handle']);  
		$retorno   = $crud->update($arrInsert, $arrayCond);  		
		
		return $retorno;
	}
	
	public function cadastrarHistoricoPedido($post) {
		$pdo = Conexao::getInstance();
		
		$arrInsert = array();
		foreach ($post as $key => $value) {
				$arrInsert[$key] =  $value;
		}
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);

		$retorno   = $crud->insert($arrInsert);  		
		
		return $retorno;
	}

	public function excluir($handle) {
		$pdo = Conexao::getInstance();
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		$crud->delete(array('codigoHistoricoPedido' => $handle));
	}	
}
