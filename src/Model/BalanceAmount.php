<?php
include_once(__DIR__ . '/../Controller/CommonController.php');
class BalanceAmount
      {
          public $id;
          public $tipo;
          public $amount;
          public $UserId;

          public $nom_tabela = "balance_amount";
          public function __construct()
          {
              $this->id = 0;
              $this->tipo = 0;
              $this->amount = 0;
              $this->UserId = 0;
          }

          public function listarTodos(
              $pagina_atual = 0,
              $linha_inicial = 0,
              $coluna = "",
              $buscar = ""
          ) {
              $pdo = Conexao::getInstance();
              $crud = bd::getInstance($pdo, $this->nom_tabela);
              $where = "";
              if ($coluna != "" && $buscar != "") {
                  $where = sprintf(
                      ' WHERE %s LIKE UPPER("%%%s%%") ',
                      $coluna,
                      strtoupper($buscar)
                  );
              }

              $paginacao = " LIMIT " . QTDE_REGISTROS;
              if ($pagina_atual > 0 && $linha_inicial > 0) {
                  $paginacao = " LIMIT {$linha_inicial}, " . QTDE_REGISTROS;
              }

              $sql = "SELECT
                balance_amount.*,
                account.AccountName
                FROM
                " . $this->nom_tabela. "
                  INNER JOIN account ON
                  account.tipo = balance_amount.tipo" . $where . $paginacao;
              // echo $sql;exit;
              $dados = $crud->getSQLGeneric($sql);

              return $dados;
          }

          public function listar($arrParametros = [])
          {
              $pdo = Conexao::getInstance();
              $crud = bd::getInstance($pdo, $this->nom_tabela);
              $sql = sprintf("SELECT * FROM " . $this->nom_tabela . " WHERE %s = ?", $arrParametros['coluna']);
              $arrayParam = [$arrParametros['valor']];
              $dados = $crud->getSQLGeneric($sql, $arrayParam, true);
              return $dados;
          }

          public function listarTodosTotal()
          {
              $pdo = Conexao::getInstance();
              $crud = bd::getInstance($pdo, $this->nom_tabela);
              $sql =
                  "SELECT count(*) as total_registros FROM " .
                  $this->nom_tabela;
              $dados = $crud->getSQLGeneric($sql, null, false);
              return $dados->total_registros;
          }

          public function editar($post)
          {
            try {
              $objCommon = new CommonController();
              $pdo = Conexao::getInstance();
              $crud = bd::getInstance($pdo, $this->nom_tabela);
              $crud->update(['amount'=>$objCommon->monetaryValue($post['caixa'])], ["UserId=" => $post["UserId"], "tipo=" => 0]);
              $crud->update(['amount'=>$objCommon->monetaryValue($post['banco'])], ["UserId=" => $post["UserId"], "tipo=" => 1]);
              $crud->update(['amount'=>$objCommon->monetaryValue($post['cartao'])], ["UserId=" => $post["UserId"], "tipo=" => 2]);
              $crud->update(['amount'=>$objCommon->monetaryValue($post['aplicacao'])], ["UserId=" => $post["UserId"], "tipo=" =>3]);
              return true;
            } catch (Exception $e) {
              return false;
            }
          }

          public function cadastrar($post)
          {
            try {
              $pdo = Conexao::getInstance();
              $objCommon = new CommonController();
              $crud = bd::getInstance($pdo, $this->nom_tabela);

              $crud->insert(["UserId" => $post["UserId"], 'amount'=>$objCommon->monetaryValue($post['caixa']), "tipo" => 0]);
              $crud->insert(["UserId" => $post["UserId"], 'amount'=>$objCommon->monetaryValue($post['banco']), "tipo" => 1]);
              $crud->insert(["UserId" => $post["UserId"], 'amount'=>$objCommon->monetaryValue($post['cartao']), "tipo" => 2]);
              $crud->insert(["UserId" => $post["UserId"], 'amount'=>$objCommon->monetaryValue($post['aplicacao']), "tipo" => 3]);
              return true;
            } catch (Exception $e) {
              return false;
            }
              
          }

          public function excluir($handle)
          {
              $pdo = Conexao::getInstance();
              $crud = bd::getInstance($pdo, $this->nom_tabela);
              $crud->delete(["id" => $handle]);
              return true;
          }
      }
?>    