<?php
require_once('conexao.php');
require_once('bd.php');
require_once('Produto.php');
require_once('ComposicaoPreco.php');
require_once('Moldura_Item_Orcamento.php');
require_once('Cliente.php');
require_once('PercentualCustoClienteGrupo.php');
require_once('ComplexidadeProduto.php');
require_once('Componente.php');
require_once('ComposicaoPreco.php');
require_once('HistoricoPedido.php');
require_once('UsuarioEmpresa.php');
require_once('GruposPermissoes.php');
include_once(__DIR__ . '/../Controller/CommonController.php');
class Orcamento
{

  public $Cd_Orcamento;
  public $Dt_Orcamento;
  public $Dt_Prevista_Entrega;
  public $Dt_Efetiva_Entrega;
  public $Cd_Cliente;
  public $Cd_Forma_Pgto;
  public $Cd_Vendedor;
  public $Vl_Bruto;
  public $VL_desconto;
  public $vl_liquido;
  public $Id_Situacao;
  public $Vl_Moldura;
  public $Ds_Observacao_Pedido;
  public $Ds_Observacao_Producao;
  public $Consumidor_Temp;
  public $Producao_Finalizada;
  public $Vl_Entrada;
  public $Pago;
  private $nom_tabela = 'Orcamento';
  private $order_by_default = 'Cd_Orcamento DESC';
  private $objProduto;
  private $objComposicaoPreco;
  private $objMolduraItemOrcamento;
  public $criado_por;
  public $modificado_por;
  public $id_empresa;
  public $saldo;
  public $data_pagamento;

  public $tipo_desconto;

  public $id_forma_pagamento_entrada;
  public $id_forma_pagamento_saldo;

  public $numero_parcelas_entrada;
  public $numero_parcelas_saldo;

  public $nf_situacao;
  public $nf_id_documento;
  public $nfs_situacao;
  public $nfs_id_documento;
  public $nfc_situacao;
  public $nfc_id_documento;
  public $permissoes;
  public $permissao_visualizar_pedidos_outros_vendedores;

  public $arrPermissoesUsuarioEmpresa = [];

  private $modulo_financeiro;


  public function __construct()
  {
    $this->Cd_Orcamento = '';
    $this->Dt_Orcamento = '';
    $this->Dt_Prevista_Entrega = '';
    $this->Dt_Efetiva_Entrega = '';
    $this->Cd_Cliente = '';
    $this->Cd_Forma_Pgto = '';
    $this->Cd_Vendedor = '';
    $this->Vl_Bruto = 0;
    $this->VL_desconto = 0;
    $this->vl_liquido = 0;
    $this->Id_Situacao = '';
    $this->Ds_Observacao_Pedido = '';
    $this->Ds_Observacao_Producao = '';
    $this->Consumidor_Temp = '';
    $this->Producao_Finalizada = '';
    $this->Vl_Entrada = 0;
    $this->Pago = 0;
    $this->objProduto = new Produto();
    $this->objComposicaoPreco = new ComposicaoPreco();
    $this->objMolduraItemOrcamento = new Moldura_Item_Orcamento();
    $this->tipo_desconto = 0;
    $this->criado_por = 0;
    $this->modificado_por = 0;
    $this->id_empresa = 1;
    $this->saldo = 0;
    $this->data_pagamento = '';

    $this->id_forma_pagamento_entrada = 0;
    $this->id_forma_pagamento_saldo = 0;
  
    $this->numero_parcelas_entrada = 1;
    $this->numero_parcelas_saldo = 1;

    $this->nf_situacao = '';
    $this->nf_id_documento = 0;
    $this->nfs_situacao = '';
    $this->nfs_id_documento = 0;
    $this->nfc_situacao = '';
    $this->nfc_id_documento = 0;

    $objUsuarioEmpresa = new UsuarioEmpresa();
    $objGruposPermissoes = new GruposPermissoes();
    $configuracoes = new Configuracoes();

    $this->modulo_financeiro = filter_var($configuracoes->listarConfiguracao('habilita_financeiro')->valor, FILTER_VALIDATE_BOOLEAN);

    $consultaPermissoesUsuario = $objUsuarioEmpresa->listarEmpresasUsuario($_SESSION['handle']);

    $this->arrPermissoesUsuarioEmpresa = array_column($consultaPermissoesUsuario, 'id_empresa');

    $handle_grupo = isset($_SESSION['handle_grupo']) ? $_SESSION['handle_grupo'] : 0;
    $this->permissoes = $objGruposPermissoes->listarPorGrupo($handle_grupo, 'Pedidos');

    $indice = array_search('VISUALIZAR_PEDIDOS_OUTROS_VENDEDORES',array_column($this->permissoes,"nome"));

    $this->permissao_visualizar_pedidos_outros_vendedores = (bool) $this->permissoes[$indice]->visualizar;
    
  }

  public function listarTodos($pagina_atual, $linha_inicial, $limit = 0, $order = "", $coluna = "", $buscar = "", $filtro = array())
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';
    if ($coluna != '' && $buscar != '') {
      $where = sprintf(' WHERE %s LIKE UPPER("%s%%") ', $coluna, strtoupper($buscar));
    }

    $filtro_condicao = '';
    $tamanho_filtro = count($filtro);
    $auxFiltro = 0;
    // print_r($filtro);
    if (!empty($filtro) && is_array($filtro)) {
      foreach ($filtro as $f_k => $f_v) {
        if ($auxFiltro == $tamanho_filtro) {
          $filtro_condicao = '';
        }

        if ($f_v == 'Id_Situacao=7') {
          $f_v = sprintf(' %s Id_Situacao IN (2,3) and Dt_Prevista_Entrega < NOW()', $filtro_condicao);
          continue;
        }

        if ($f_k == 'Orcamento.Id_Situacao') {
          if ($f_v == '7') {
            $where .= sprintf(
              '%s IN (2,3) and Dt_Prevista_Entrega < NOW() %s',
              $f_k,
              $filtro_condicao
            );
          } else {
            $where .= sprintf(
              '( (%s = "%s") OR (Situacao.descricao LIKE UPPER("%%%s%%")) ) %s',
              $f_k,
              $f_v,
              strtoupper($f_v),
              $filtro_condicao
            );
          }
        } else {
          if ($f_k == 'id_empresa' || $f_k == 'criado_por' || $f_k == 'Cd_Orcamento' || $f_k == 'Cd_Cliente') {
            $where .= sprintf("%s $f_k = $f_v", $filtro_condicao);
          } else if ($f_k == 'Id_Situacao') {
            if ($f_v != '7') {
              $where .= sprintf("%s $f_k IN ($f_v)", $filtro_condicao);
            } else {
              $where .= sprintf(' %s Id_Situacao IN (2,3) and Dt_Prevista_Entrega < NOW()', $filtro_condicao);
            }
          } else {
            $where .= sprintf(
              '%s LIKE UPPER("%s%%") %s',
              $f_k,
              strtoupper($f_v),
              $filtro_condicao
            );
          }
        }

        $filtro_condicao = ' AND ';

        $auxFiltro++;
      }
    }

    if (!empty($where)) {
      $where = ' WHERE ' . $where;
    }

    if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
      $where .= sprintf(
        '%s id_empresa IN (%s)',
        ($where != '' ? ' AND ' : ' WHERE '),
        implode(',', $this->arrPermissoesUsuarioEmpresa)
      );
    }

    if (!$this->permissao_visualizar_pedidos_outros_vendedores) {
      $where .= sprintf(
        '%s criado_por  =  %s',
        ($where != '' ? ' AND ' : ' WHERE '),
        (int) $_SESSION['handle']
      );
    }    

    $paginacao = " LIMIT " . QTDE_REGISTROS;
    $qtd_registros = QTDE_REGISTROS;
    if ($limit > 0) {
      $qtd_registros = $limit;
    }
    if ($pagina_atual > 0) {
      $paginacao = ' LIMIT ' . $qtd_registros;
      if ($pagina_atual > 0 && $linha_inicial > 0) {
        $paginacao = " LIMIT $qtd_registros OFFSET " . ($linha_inicial);
      }
    }

    $ordenacao = " ORDER BY " . $this->order_by_default;
    if ($order != "") {
      $ordenacao = " ORDER BY " . $order;
    }

    $sql = "
			SELECT " . $this->nom_tabela . ".*, 
				Clientes.RazaoSocial,
				Clientes.Nomefantasia,
				Situacao.descricao,
				consumidor.RazaoSocial as RazaoSocialConsumidor,
        IF(Orcamento.tipo_desconto = 'M', Orcamento.VL_desconto, Orcamento.Vl_Bruto * Orcamento.VL_desconto / 100 ) as valorDescontoFinal,
        Empresas.NomeFantasia as nome_empresa,
        usuarios.NomeUsuario as nome_vendedor
			FROM " . $this->nom_tabela . ' 
				LEFT JOIN Clientes ON (Clientes.CodigoCliente = Orcamento.Cd_Cliente)
				LEFT JOIN Clientes consumidor ON (Orcamento.Consumidor_Temp = consumidor.CodigoCliente)
        INNER JOIN Empresas ON (Empresas.CodigoEmpresa = Orcamento.id_empresa) 
        INNER JOIN usuarios ON (usuarios.CodigoUsuario = Orcamento.criado_por) 
				INNER JOIN Situacao ON (Situacao.idSituacao = Orcamento.Id_Situacao) ' .
      $where .
      $ordenacao .
      $paginacao;
    //  echo "<hr><pre>$sql</pre><hr>";
    $dados = $crud->getSQLGeneric($sql);
    return $dados;

    //
  }

  public function listarTodosTotal($filtro)
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';
    $filtro_condicao = '';
    $tamanho_filtro = count($filtro);
    $auxFiltro = 0;
    if (!empty($filtro) && is_array($filtro)) {
      foreach ($filtro as $f_k => $f_v) {

        if ($auxFiltro == $tamanho_filtro) {
          $filtro_condicao = '';
        }

        if ($f_v == 'Id_Situacao=7') {
          $f_v = 'Id_Situacao IN (2,3) and Dt_Prevista_Entrega < NOW()';
          continue;
        }

        if ($f_k == 'Orcamento.Id_Situacao') {
          if ($f_v == '7') {
            $where .= sprintf(
              '%s IN (2,3) and Dt_Prevista_Entrega < NOW() %s',
              $f_k,
              $filtro_condicao
            );
          } else {
            $where .= sprintf(
              '%s = "%s" %s',
              $f_k,
              $f_v,
              $filtro_condicao
            );
          }
        } else {
          if ($f_k == 'id_empresa' || $f_k == 'criado_por' || $f_k == 'Cd_Orcamento' || $f_k == 'Cd_Cliente') {
            $where .= sprintf("%s $f_k = $f_v", $filtro_condicao);
          } else if ($f_k == 'Id_Situacao') {
            if ($f_v != '7') {
              $where .= sprintf("%s $f_k IN ($f_v)", $filtro_condicao);
            } else {
              $where .= sprintf(' %s Id_Situacao IN (2,3) and Dt_Prevista_Entrega < NOW()', $filtro_condicao);
            }
          } else {
            $where .= sprintf(
              '%s LIKE UPPER("%s%%") %s',
              $f_k,
              strtoupper($f_v),
              $filtro_condicao
            );
          }
        }

        $filtro_condicao = ' AND ';

        $auxFiltro++;
      }
    }

    if (!empty($where)) {
      $where = ' WHERE ' . $where;
    }

    if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
      $where .= sprintf(
        '%s id_empresa IN (%s)',
        ($where != '' ? ' AND ' : ' WHERE '),
        implode(',', $this->arrPermissoesUsuarioEmpresa)
      );
    }
    
    if (!$this->permissao_visualizar_pedidos_outros_vendedores) {
      $where .= sprintf(
        '%s criado_por  =  %s',
        ($where != '' ? ' AND ' : ' WHERE '),
        (int) $_SESSION['handle']
      );
    }     

    $sql = "
			SELECT 
				count(Cd_Orcamento) as total_registros FROM " .
      $this->nom_tabela .
      ' LEFT JOIN Clientes ON (Clientes.CodigoCliente = Orcamento.Cd_Cliente) ' . $where;

      // echo "<hr><pre>$sql</pre><hr>";

    $dados = $crud->getSQLGeneric($sql, null, FALSE);

    return $dados->total_registros;

    //
  }

  public function listarOrcamento($handle)
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    // $sql = "SELECT *, Clientes.*, consumidor.RazaoSocial as RazaoSocialConsumidor FROM " . $this->nom_tabela . " LEFT JOIN Clientes ON (Orcamento.Cd_Cliente = Clientes.CodigoCliente) LEFT JOIN Clientes consumidor ON (Orcamento.Consumidor_Temp = consumidor.CodigoCliente) WHERE Cd_Orcamento = ?";

    $condicional_restricao_empresa = '';
    if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
      $condicional_restricao_empresa = sprintf(
        'AND Orcamento.id_empresa IN (%s)',
        implode(',', $this->arrPermissoesUsuarioEmpresa)
      );
    }    

    $condicional_restricao_pedidos_vendedores = '';
    if (!$this->permissao_visualizar_pedidos_outros_vendedores) {
      $condicional_restricao_pedidos_vendedores = sprintf(
        ' AND criado_por  =  %s',
        (int) $_SESSION['handle']
      );
    }     

    $sql = sprintf("
                    SELECT
                       $this->nom_tabela.*,
                      Clientes.*,
                      sit.*,
                      consumidor.RazaoSocial as RazaoSocialConsumidor,

                      documento_fiscal_nf.situacao AS nf_situacao,
                      documento_fiscal_nf.id_documento AS nf_id_documento,
                      documento_fiscal_nf.devolucao AS nf_devolucao,

                      documento_fiscal_nfs.situacao AS nfs_situacao,
                      documento_fiscal_nfs.id_documento AS nfs_id_documento,
                      documento_fiscal_nfs.devolucao AS nfs_devolucao,

                      documento_fiscal_nfc.situacao AS nfc_situacao,
                      documento_fiscal_nfc.id_documento AS nfc_id_documento,                      
                      documento_fiscal_nfc.devolucao AS nfc_devolucao,

                      sit.descricao as situacao_pedido,
                      IF(Orcamento.tipo_desconto = 'M', Orcamento.VL_desconto, ROUND(Orcamento.Vl_Bruto * (Orcamento.VL_desconto / 100), 2) ) as valorDescontoFinal,
                      IF(assets.AssetsId > 0, assets.AssetsId, 0) as idContaReceber,
                      CONCAT(usuario_criado.NomeUsuario, IF(usuario_criado.situacao = 0, ' (Vendedor Removido)', '')) as NomeUsuarioCriado,
                      usuario_criado.CodigoUsuario as CodigoUsuarioCriado,
                      usuario_criado.situacao as SituacaoUsuarioCriado, 
                      usuario_modificado.NomeUsuario as NomeUsuarioModificado,
                      usuario_modificado.CodigoUsuario as CodigoUsuarioModificado,
                      IF(DAYNAME(Orcamento.Dt_Prevista_Entrega) IS NULL, '', Orcamento.Dt_Prevista_Entrega) as Dt_Prevista_Entrega,
                      (Orcamento.vl_liquido - Orcamento.Vl_Entrada) as saldo

                    FROM
                      Orcamento
                    LEFT JOIN Clientes ON
                      (Orcamento.Cd_Cliente = Clientes.CodigoCliente)
                    LEFT JOIN Clientes consumidor ON
                      (Orcamento.Consumidor_Temp = consumidor.CodigoCliente)
                    LEFT JOIN fila_documento_fiscal documento_fiscal_nf ON 
                      (Orcamento.Cd_Orcamento = documento_fiscal_nf.id_pedido AND documento_fiscal_nf.id_tipo_documento = 1)
                    LEFT JOIN fila_documento_fiscal documento_fiscal_nfs ON 
                      (Orcamento.Cd_Orcamento = documento_fiscal_nfs.id_pedido AND documento_fiscal_nfs.id_tipo_documento = 2)
                    LEFT JOIN fila_documento_fiscal documento_fiscal_nfc ON 
                      (Orcamento.Cd_Orcamento = documento_fiscal_nfc.id_pedido AND documento_fiscal_nfc.id_tipo_documento = 3)
                    LEFT JOIN Situacao sit ON 
                      (Orcamento.Id_Situacao = sit.idSituacao)                      
                      LEFT JOIN assets ON 
                      (Orcamento.Cd_Orcamento = assets.ndoc)
                    LEFT JOIN usuarios as usuario_criado ON
						(Orcamento.criado_por = usuario_criado.CodigoUsuario)
					LEFT JOIN usuarios as usuario_modificado ON
						(Orcamento.modificado_por = usuario_modificado.CodigoUsuario)	
                    WHERE
                      Cd_Orcamento = ? $condicional_restricao_empresa $condicional_restricao_pedidos_vendedores
                    ORDER BY
                      documento_fiscal_nf.id_fila desc
                    LIMIT 1    
    ");

    // echo "<pre>$sql</pre>";exit;

    $arrayParam = array($handle);

    $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    return $dados;

    //
  }

  public function listarOrcamentoPorDataEntregue($arrFiltro = array())
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $condicional_restricao_empresa = '';
    if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
      $condicional_restricao_empresa = sprintf(
        'AND Orcamento.id_empresa IN (%s)',
        implode(',', $this->arrPermissoesUsuarioEmpresa)
      );
    }     

    $where = '';
    $condicional = '';
    if (sizeof($arrFiltro)) {
      if (isset($arrFiltro['periodo_inicial']) && isset($arrFiltro['periodo_final'])) {
        $tipo_data = 'Orcamento.Dt_Efetiva_Entrega';
        if (isset($arrFiltro['tipo_data'])) {
          $tipo_data = 'Orcamento.' . $arrFiltro['tipo_data'];
        }

        $where .= sprintf(" %s %s BETWEEN '%s' AND '%s'", $condicional, $tipo_data, $arrFiltro['periodo_inicial'], $arrFiltro['periodo_final']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['filtro_situacao']) && !empty($arrFiltro['filtro_situacao'])) {
        $where .= sprintf(" %s Orcamento.Id_Situacao IN (%s)", $condicional, $arrFiltro['filtro_situacao']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['filtro_pago']) && $arrFiltro['filtro_pago'] != "-1") {
        $where .= sprintf(" %s Orcamento.Pago = %s", $condicional, $arrFiltro['filtro_pago']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['criado_por']) && !empty($arrFiltro['criado_por'])) {
        $where .= sprintf(" %s Orcamento.criado_por = %s", $condicional, $arrFiltro['criado_por']);
        $condicional = ' AND ';
      }  
      
      if (isset($arrFiltro['empresa']) && !empty($arrFiltro['empresa'])) {
        $where .= sprintf(" %s Orcamento.id_empresa = %s", $condicional, $arrFiltro['empresa']);
        $condicional = ' AND ';
      }        
    }

    if (!empty($where)) {
      $where = ' WHERE ' . $where;
    }

    if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
      $where .= sprintf(
        '%s id_empresa IN (%s)',
        ($where != '' ? ' AND ' : ' WHERE '),
        implode(',', $this->arrPermissoesUsuarioEmpresa)
      );
    }    

    $sql = "SELECT 
					Clientes.CodigoCliente,
					Clientes.RazaoSocial,
					Orcamento.Cd_Orcamento,
          IF(DAYNAME(Orcamento.Dt_Orcamento) IS NULL, '', Orcamento.Dt_Orcamento) as Dt_Orcamento,
					IF(DAYNAME(Orcamento.Dt_Efetiva_Entrega) IS NULL, '', Orcamento.Dt_Efetiva_Entrega) as Dt_Efetiva_Entrega,
					IF(DAYNAME(Orcamento.Dt_Prevista_Entrega) IS NULL, '', Orcamento.Dt_Prevista_Entrega) as Dt_Prevista_Entrega,
					Orcamento.Vl_Bruto,
					Orcamento.Vl_Desconto,
					Orcamento.Vl_Liquido,
					Item_Orcamento.Cd_Item_Orcamento,
					Produtos_Auxiliares.descricao,
					Item_Orcamento.Cd_Prod_Aux,
					Item_Orcamento.Md_Altura,
					Item_Orcamento.Md_Largura,
					Item_Orcamento.Qt_Item,
					Item_Orcamento.Vl_Unitario,
					Item_Orcamento.Ds_Observacao,			
					Item_Orcamento.Ds_ObservacaoProducao,
          CONCAT(usuario_criado.NomeUsuario, IF(usuario_criado.situacao = 0, ' (Vendedor Removido)', '')) as NomeUsuarioCriado,
          usuario_criado.CodigoUsuario as CodigoUsuarioCriado,
          usuario_criado.situacao as SituacaoUsuarioCriado, 
          usuario_modificado.NomeUsuario as NomeUsuarioModificado,
          usuario_modificado.CodigoUsuario as CodigoUsuarioModificado,
          sit.descricao as situacao_pedido,
          Orcamento.Pago,
          (SELECT CONCAT(descricao,' (',Orcamento.numero_parcelas_entrada,'X)') FROM forma_pagamento WHERE Orcamento.id_forma_pagamento_entrada = id_forma_pagamento LIMIT 1) as forma_pagamento_entrada,
          (SELECT CONCAT(descricao,' (', Orcamento.numero_parcelas_saldo,'X)') FROM forma_pagamento WHERE Orcamento.id_forma_pagamento_saldo = id_forma_pagamento LIMIT 1) as forma_pagamento_saldo
				FROM
					Orcamento
					LEFT JOIN Clientes ON (Orcamento.Cd_Cliente = Clientes.CodigoCliente)
					INNER JOIN Item_Orcamento ON (Orcamento.Cd_Orcamento = Item_Orcamento.Cd_Orcamento)
					INNER JOIN Produtos_Auxiliares ON (Item_Orcamento.Cd_Item_Orcamento = Produtos_Auxiliares.id)
          LEFT JOIN usuarios as usuario_criado ON (Orcamento.criado_por = usuario_criado.CodigoUsuario)
					LEFT JOIN usuarios as usuario_modificado ON (Orcamento.modificado_por = usuario_modificado.CodigoUsuario)						
          LEFT JOIN Situacao sit ON (Orcamento.Id_Situacao = sit.idSituacao)                      
				 $where 
				ORDER BY
					Orcamento.Cd_Orcamento,
					Item_Orcamento.Cd_Item_Orcamento					
					";
    $dados = $crud->getSQLGeneric($sql, array(), TRUE);

    return $dados;
  }

  public function listarOrcamentoSimplificado($arrFiltro = array())
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $condicional_restricao_empresa = '';
    if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
      $condicional_restricao_empresa = sprintf(
        'AND Orcamento.id_empresa IN (%s)',
        implode(',', $this->arrPermissoesUsuarioEmpresa)
      );
    }     

    $where = '';
    $condicional = '';
    if (sizeof($arrFiltro)) {
      if (isset($arrFiltro['periodo_inicial']) && isset($arrFiltro['periodo_final'])) {
        $tipo_data = 'Orcamento.Dt_Efetiva_Entrega';
        if (isset($arrFiltro['tipo_data'])) {
          $tipo_data = 'Orcamento.' . $arrFiltro['tipo_data'];
        }

        $where .= sprintf(" %s %s BETWEEN '%s' AND '%s'", $condicional, $tipo_data, $arrFiltro['periodo_inicial'], $arrFiltro['periodo_final']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['filtro_situacao']) && !empty($arrFiltro['filtro_situacao']) && $arrFiltro['somente_pedidos_validos'] != '1') {
        $where .= sprintf(" %s Orcamento.Id_Situacao IN (%s)", $condicional, $arrFiltro['filtro_situacao']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['somente_pedidos_validos']) && $arrFiltro['somente_pedidos_validos'] == "1") {
        $where .= sprintf(" %s Orcamento.Id_Situacao > 1 AND Orcamento.Id_Situacao < 6 ", $condicional);
        $condicional = ' AND ';
      }      

      if (isset($arrFiltro['filtro_pago']) && $arrFiltro['filtro_pago'] != "-1") {
        $where .= sprintf(" %s Orcamento.Pago = %s", $condicional, $arrFiltro['filtro_pago']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['criado_por']) && !empty($arrFiltro['criado_por'])) {
        $where .= sprintf(" %s Orcamento.criado_por = %s", $condicional, $arrFiltro['criado_por']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['cliente']) && !empty($arrFiltro['cliente'])) {
        $where .= sprintf(" %s Orcamento.Cd_Cliente = %s", $condicional, $arrFiltro['cliente']);
        $condicional = ' AND ';
      }      

      if (isset($arrFiltro['pedido']) && !empty($arrFiltro['pedido'])) {
        $where .= sprintf(" %s Orcamento.Cd_Orcamento = %s", $condicional, $arrFiltro['pedido']);
        $condicional = ' AND ';
      }            
      
      if (isset($arrFiltro['empresa']) && !empty($arrFiltro['empresa'])) {
        $where .= sprintf(" %s Orcamento.id_empresa = %s", $condicional, $arrFiltro['empresa']);
        $condicional = ' AND ';
      }        
    }

    if (!empty($where)) {
      $where = ' WHERE ' . $where;
    }

    if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
      $where .= sprintf(
        '%s id_empresa IN (%s)',
        ($where != '' ? ' AND ' : ' WHERE '),
        implode(',', $this->arrPermissoesUsuarioEmpresa)
      );
    }    

    $sql = "SELECT 
					Clientes.CodigoCliente,
					Clientes.RazaoSocial,
					Orcamento.Cd_Orcamento,
          IF(DAYNAME(Orcamento.Dt_Orcamento) IS NULL, '', Orcamento.Dt_Orcamento) as Dt_Orcamento,
					IF(DAYNAME(Orcamento.Dt_Efetiva_Entrega) IS NULL, '', Orcamento.Dt_Efetiva_Entrega) as Dt_Efetiva_Entrega,
					IF(DAYNAME(Orcamento.Dt_Prevista_Entrega) IS NULL, '', Orcamento.Dt_Prevista_Entrega) as Dt_Prevista_Entrega,
					Orcamento.Vl_Bruto,
					Orcamento.Vl_Desconto,
					Orcamento.Vl_Liquido,
          CONCAT(usuario_criado.NomeUsuario, IF(usuario_criado.situacao = 0, ' (Vendedor Removido)', '')) as NomeUsuarioCriado,
          usuario_criado.CodigoUsuario as CodigoUsuarioCriado,
          usuario_criado.situacao as SituacaoUsuarioCriado, 
          usuario_modificado.NomeUsuario as NomeUsuarioModificado,
          usuario_modificado.CodigoUsuario as CodigoUsuarioModificado,
          sit.descricao as situacao_pedido,
          Orcamento.Pago,
          Orcamento.Vl_Entrada,
          (Orcamento.vl_liquido - Orcamento.Vl_Entrada) as saldo,
          (SELECT CONCAT(descricao,' (',Orcamento.numero_parcelas_entrada,'X)') FROM forma_pagamento WHERE Orcamento.id_forma_pagamento_entrada = id_forma_pagamento LIMIT 1) as forma_pagamento_entrada,
          (SELECT CONCAT(descricao,' (', Orcamento.numero_parcelas_saldo,'X)') FROM forma_pagamento WHERE Orcamento.id_forma_pagamento_saldo = id_forma_pagamento LIMIT 1) as forma_pagamento_saldo
				FROM
					Orcamento
					LEFT JOIN Clientes ON (Orcamento.Cd_Cliente = Clientes.CodigoCliente)
          LEFT JOIN usuarios as usuario_criado ON (Orcamento.criado_por = usuario_criado.CodigoUsuario)
					LEFT JOIN usuarios as usuario_modificado ON (Orcamento.modificado_por = usuario_modificado.CodigoUsuario)						
          LEFT JOIN Situacao sit ON (Orcamento.Id_Situacao = sit.idSituacao)
          INNER JOIN Empresas e ON (Orcamento.id_empresa = e.CodigoEmpresa)
				 $where 
				ORDER BY
					Orcamento.Cd_Orcamento
					";
    // echo "<pre>$sql</pre>";
    $dados = $crud->getSQLGeneric($sql, array(), TRUE);

    return $dados;
  }  

  public function listarOrcamentoComComissao($arrFiltro = array())
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $condicional_restricao_empresa = '';
    if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
      $condicional_restricao_empresa = sprintf(
        'AND o.id_empresa IN (%s)',
        implode(',', $this->arrPermissoesUsuarioEmpresa)
      );
    }     

    $where = '';
    $condicional = '';
    if (sizeof($arrFiltro)) {
      if (isset($arrFiltro['periodo_inicial']) && isset($arrFiltro['periodo_final'])) {
        $tipo_data = 'o.Dt_Efetiva_Entrega';
        if (isset($arrFiltro['tipo_data'])) {
          if ($arrFiltro['tipo_data'] == 'cdtpagamento') {
            $tipo_data = $this->modulo_financeiro ? 'a.cdtpagamento' : 'o.data_pagamento';
          } else {
            $tipo_data = 'o.' . $arrFiltro['tipo_data'];
          }
        }

        $where .= sprintf(" %s %s BETWEEN '%s' AND '%s'", $condicional, $tipo_data, $arrFiltro['periodo_inicial'], $arrFiltro['periodo_final']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['filtro_situacao']) && !empty($arrFiltro['filtro_situacao'])) {
        $where .= sprintf(" %s o.Id_Situacao IN (%s)", $condicional, $arrFiltro['filtro_situacao']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['filtro_pago']) && $arrFiltro['filtro_pago'] != "-1") {
        $where .= sprintf(" %s o.Pago = %s", $condicional, $arrFiltro['filtro_pago']);
        $condicional = ' AND ';
      }
      
      if (isset($arrFiltro['vendedor']) && $arrFiltro['vendedor'] != "") {
        $where .= sprintf(" %s o.criado_por = %s", $condicional, $arrFiltro['vendedor']);
        $condicional = ' AND ';
      }      
    }

    if (!empty($where)) {
      $where = ' WHERE ' . $where;
    }

    if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
      $where .= sprintf(
        '%s id_empresa IN (%s)',
        ($where != '' ? ' AND ' : ' WHERE '),
        implode(',', $this->arrPermissoesUsuarioEmpresa)
      );
    }    

    $sql = "
            SELECT
              o.Cd_Orcamento as handle_orcamento,
              s.descricao as situacao,
              o.Dt_Orcamento as data_orcamento,
              CONCAT(u.NomeUsuario, IF(u.situacao = 0, ' (Vendedor Removido)', '')) as nome_vendedor,
              c.RazaoSocial as nome_cliente,
              IF(DAYNAME(a.cdtpagamento) IS NULL, '', a.cdtpagamento) as data_pagamento_conta_receber,
              IF(DAYNAME(o.data_pagamento) IS NULL, '', o.data_pagamento) as data_pagamento_pedido,
              o.Vl_Bruto as valor_bruto,
              o.VL_desconto as valor_desconto,
              o.vl_liquido as valor_liquido,
              IF (a.Amount is null, o.vl_liquido, a.Amount) as valor_conta_receber,
              IFNULL((a.amount * (v.percentual_comissao / 100) ), 0) as valor_comissao_pedido_receber,
              IFNULL((o.vl_liquido * (v.percentual_comissao / 100) ), 0) as valor_comissao,
              IF(a.AssetsId is not null, a.AssetsId , 0) as handle_conta_receber,
              (SELECT CONCAT(descricao,'|',o.numero_parcelas_entrada) FROM forma_pagamento WHERE o.id_forma_pagamento_entrada = id_forma_pagamento LIMIT 1) as forma_pagamento_entrada,
              (SELECT CONCAT(descricao,'|', o.numero_parcelas_saldo) FROM forma_pagamento WHERE o.id_forma_pagamento_saldo = id_forma_pagamento LIMIT 1) as forma_pagamento_saldo
            FROM
              Orcamento o
              INNER JOIN Situacao s ON (o.Id_Situacao = s.idSituacao)
              INNER JOIN usuarios u ON (o.criado_por = u.CodigoUsuario)
              INNER JOIN Clientes c ON (o.Cd_Cliente = c.CodigoCliente)
              INNER JOIN Empresas e ON (o.id_empresa = e.CodigoEmpresa)
              LEFT JOIN assets a ON (o.Cd_Orcamento = a.ndoc and a.situacao = 1)
              LEFT JOIN vendedor v ON (u.CodigoUsuario = v.id_usuario)
            $where
            ORDER BY o.Cd_Orcamento
    ";
      // echo "<pre>$sql</pre>";
    // exit;
    $dados = $crud->getSQLGeneric($sql, array(), TRUE);

    return $dados;
  }

  public function listarOrcamentoProducao($filtro = [])
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $condicional_restricao_empresa = '';
    if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
      $condicional_restricao_empresa = sprintf(
        'AND Orcamento.id_empresa IN (%s)',
        implode(',', $this->arrPermissoesUsuarioEmpresa)
      );
    }

    $filtro_empresa = isset($filtro['empresa']) && !empty($filtro['empresa']) ? ' AND Orcamento.id_empresa = ' . $filtro['empresa'] : '';
    $filtro_vendedor = isset($filtro['vendedor']) && !empty($filtro['vendedor']) ? ' AND Orcamento.criado_por = ' . $filtro['vendedor'] : '';

    $data_inicio = isset($filtro['data_inicio']) && !empty($filtro['data_inicio']) ?  $filtro['data_inicio'] : '';
    $data_final = isset($filtro['data_final']) && !empty($filtro['data_final']) ? $filtro['data_final'] : '';

    $filtro_data = '';
    if ($data_inicio != '' && $data_final != '') {
      $filtro_data = sprintf(' AND Orcamento.Dt_Prevista_Entrega BETWEEN "%s" AND "%s"', $data_inicio, $data_final);
    }

    $sql = "SELECT 
					Clientes.CodigoCliente,
					Clientes.RazaoSocial,
					Orcamento.Cd_Orcamento,
					Orcamento.Dt_Efetiva_Entrega,
					Orcamento.Dt_Prevista_Entrega,
					Orcamento.Vl_Bruto,
					Orcamento.Vl_Desconto,
					Orcamento.Vl_Liquido,
          Orcamento.Vl_Entrada,
					Orcamento.Ds_Observacao_Producao,
          Orcamento.Ds_Observacao_Pedido,
          Orcamento.Id_Situacao,
          Orcamento.Pago,
          usuario_criado.NomeUsuario as NomeUsuarioCriado
				FROM
					Orcamento
					LEFT JOIN Clientes ON (Orcamento.Cd_Cliente = Clientes.CodigoCliente)
          LEFT JOIN usuarios as usuario_criado ON (Orcamento.modificado_por = usuario_criado.CodigoUsuario)						
				WHERE
					Orcamento.Id_Situacao > 1 AND Orcamento.Id_Situacao < 5 $condicional_restricao_empresa $filtro_empresa $filtro_vendedor $filtro_data
				ORDER BY
          Orcamento.Id_Situacao ASC,
					Orcamento.Dt_Prevista_Entrega ASC,
					Orcamento.Cd_Orcamento ASC
					";
    // echo "<pre> $sql </pre>";
    $dados = $crud->getSQLGeneric($sql, array(), TRUE);

    return $dados;
  }  

  public function listarOrcamentoPorDataPrevista($periodo_inicial, $periodo_final, $empresa = '', $tipo_data = 'Dt_Orcamento')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $condicional_restricao_empresa = '';
    if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
      $condicional_restricao_empresa = sprintf(
        'AND Orcamento.id_empresa IN (%s)',
        implode(',', $this->arrPermissoesUsuarioEmpresa)
      );
    }

    $where_empresa = '';
    if ($empresa != '') {
      $where_empresa = sprintf(' AND Orcamento.id_empresa = %s ', $empresa);
    }

    $sql = "SELECT 
					Clientes.CodigoCliente,
					Clientes.RazaoSocial,
					Orcamento.Cd_Orcamento,
          IF(DAYNAME(Orcamento.Dt_Orcamento) IS NULL, '', Orcamento.Dt_Orcamento) as Dt_Orcamento,
					IF(DAYNAME(Orcamento.Dt_Efetiva_Entrega) IS NULL, '', Orcamento.Dt_Efetiva_Entrega) as Dt_Efetiva_Entrega,
					IF(DAYNAME(Orcamento.Dt_Prevista_Entrega) IS NULL, '', Orcamento.Dt_Prevista_Entrega) as Dt_Prevista_Entrega,
					Orcamento.Vl_Bruto,
					Orcamento.Vl_Desconto,
					Orcamento.Vl_Liquido,
					Orcamento.Ds_Observacao_Producao,
					Item_Orcamento.Cd_Item_Orcamento,
					Produtos_Auxiliares.Descricao,
					Item_Orcamento.Cd_Prod_Aux,
					Item_Orcamento.Md_Altura,
					Item_Orcamento.Md_Largura,
					Item_Orcamento.Qt_Item,
					Item_Orcamento.Vl_Unitario,
					Item_Orcamento.Ds_Observacao,
					Item_Orcamento.Ds_ObservacaoProducao,
          CONCAT(usuario_criado.NomeUsuario, IF(usuario_criado.situacao = 0, ' (Vendedor Removido)', '')) as NomeUsuarioCriado,
          usuario_criado.CodigoUsuario as CodigoUsuarioCriado,
          usuario_criado.situacao as SituacaoUsuarioCriado, 
          usuario_modificado.NomeUsuario as NomeUsuarioModificado,
          usuario_modificado.CodigoUsuario as CodigoUsuarioModificado,
          Empresas.NomeFantasia as nome_empresa
				FROM
					Orcamento
					LEFT JOIN Clientes ON (Orcamento.Cd_Cliente = Clientes.CodigoCliente)
					INNER JOIN Item_Orcamento ON (Orcamento.Cd_Orcamento = Item_Orcamento.Cd_Orcamento)
					INNER JOIN Produtos_Auxiliares ON (Item_Orcamento.Cd_Prod_Aux = Produtos_Auxiliares.id)
                    LEFT JOIN usuarios as usuario_criado ON
						(Orcamento.criado_por = usuario_criado.CodigoUsuario)
					LEFT JOIN usuarios as usuario_modificado ON
						(Orcamento.modificado_por = usuario_modificado.CodigoUsuario)						
          INNER JOIN Empresas ON (Empresas.CodigoEmpresa = Orcamento.id_empresa)             
				WHERE
					Orcamento.{$tipo_data} BETWEEN '$periodo_inicial' AND '$periodo_final' $condicional_restricao_empresa $where_empresa
          AND Orcamento.Id_Situacao IN (2,3)
				ORDER BY
					Orcamento.{$tipo_data} ASC,
					Orcamento.Cd_Orcamento ASC,
					Item_Orcamento.Cd_Item_Orcamento					
					";

    // echo "<pre>$sql</pre>";
    $dados = $crud->getSQLGeneric($sql, array(), TRUE);

    return $dados;
  }

  public function cadastrarOrcamento($post)
  {    
    // print_r($post);
    //  exit;
    try {
      $pdo = Conexao::getInstance();
      $Cd_Orcamento = 0;
      $arrayOrcamento = array('Dt_Orcamento' => 0, 'Cd_Cliente' => 0, 'Vl_Bruto' => 0, 'VL_desconto' => 0, 'vl_liquido' => 0, 'Vl_Entrada' => 0, 'Id_Situacao' => 0, 'Consumidor_Temp' => 0, 'Dt_Prevista_Entrega' => 0, 'Dt_Efetiva_Entrega' => 0, 'Pago' => 0, 'Ds_Observacao_Pedido' => '', 'Ds_Observacao_Producao' => '', 'tipo_desconto' => 'M', 'id_empresa' => 1, 'id_forma_pagamento_entrada' => 0, 'id_forma_pagamento_saldo' => 0, 'numero_parcelas_entrada' => 1, 'numero_parcelas_saldo' => 1, 'data_pagamento' => 'NULL', 'criado_por' => 0);
      $arrItemOrcamento = array('Cd_Prod_Aux' => 0, 'Qt_Item' => 0, 'Md_Altura' => 0, 'Md_Largura' => 0, 'Vl_Unitario' => 0, 'Vl_Total' => 0, 'Ds_Observacao' => '', 'Ds_ObservacaoProducao' => '', 'VL_ADICIONAIS' => 0, 'Vl_Moldura' => 0, 'descricao_produto_servico' => '', 'medida_final' => '', 'valor_custo' => 0);
      $arrComponentesOrcamento = array('CD_Componente' => 0);
      $arrMoldurasOrcamento = array('Cd_Produto' => 0);
      $composicoesPreco = $this->objComposicaoPreco->listarComposicaoPreco(1);
      $valor_perda = $composicoesPreco[0]->VL_PERDA_MEDIA;
      $percentualCustoClienteGrupo = new PercentualCustoClienteGrupo();
      $objCliente  = new Cliente();
      $objComplexidadePorProduto = new ComplexidadeProduto();
      $objComponente = new Componente();
      $objComposicaoPreco = new ComposicaoPreco();
      $configuracoes = new Configuracoes();
      $objCommon = new CommonController();

      if (isset($post['Md_Altura']) && is_array($post['Md_Altura'])) {
        $post['Md_Altura']  = array_map(function($num ) use ($objCommon) {return $objCommon->monetaryValue($num);}, $post['Md_Altura']);
      } else {
        $post['Md_Altura'] = [];
      }

      if (isset($post['Md_Largura']) && is_array($post['Md_Largura'])) {
        $post['Md_Largura'] = array_map(function($num ) use ($objCommon) {return $objCommon->monetaryValue($num);}, $post['Md_Largura']);
      } else {
        $post['Md_Largura'] = [];
      }

      //print_r($arrAlturas);exit;

      $composicoesPreco = $objComposicaoPreco->listarComposicaoPreco(1);
      $valor_perda = $composicoesPreco[0]->VL_PERDA_MEDIA;
      $valor_margem_bruta = $composicoesPreco[0]->VL_MARGEM_BRUTA;
      $numero_cortes_considerar_perda = $configuracoes->listarConfiguracao('numero_cortes_considerar_perda')->valor;
      $desabilitar_controle_estoque = filter_var($configuracoes->listarConfiguracao('desabilitar_controle_estoque')->valor, FILTER_VALIDATE_BOOLEAN);

      foreach ($post as $key => $value) {
        if ($key != 'handle' && $key != 'Cd_Orcamento' && $key != 'Cd_Prod_Aux_Pedido') {
          if (array_key_exists($key, $arrayOrcamento)) {
            if (($key == 'Dt_Orcamento' || $key == 'Dt_Prevista_Entrega' || $key == 'Dt_Efetiva_Entrega' || $key == 'data_pagamento') && !empty(trim($value))) {
              if (preg_match('/^[0-9]+\/[0-9]+\/[0-9]{4}$/m', $value) == 1) {
                $value = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $value)));
              }
            } else if (($key == 'Dt_Orcamento' || $key == 'Dt_Prevista_Entrega' || $key == 'Dt_Efetiva_Entrega' || $key == 'data_pagamento') && $value == '') {
              $value = NULL;
            }

            switch ($key) {
              case 'VL_desconto':
                // case 'vl_liquido':
              case 'Vl_Entrada':
                if ($value == "") {
                  $value = 0;
                  break;
                }
                $value = str_replace(".", "", $value);
                $value = str_replace(",", ".", $value);
                $value = number_format($value, 2, ".", "");
                break;
            }

            $arrayOrcamento[$key] =  $value;
          }
        }
        if ($key == 'Cd_Cliente' && empty($value)) {
          $arrayOrcamento['Cd_Cliente'] = 0;
        }
        if ($key == 'Cd_Orcamento' && (int) $value > 0) {
          $Cd_Orcamento = $value;
        }
      }
      $arrItemsOrcamento = array();
      $arrItemsMoldurasOrcamento = array();
      $arrItemsComponentesOrcamento = array();
      foreach ($post as $key => $value) {
        if ($value  != "" && array_key_exists($key, $arrItemOrcamento)) {
          if (is_array($post[$key])) {
            foreach ($post[$key] as $key_ => $value_) {
              if ($key == 'Qt_Item') {
                foreach ($post[$key] as $itens_orcamento => $item_orcamento) {
                  if (isset($post['Cd_Produto'][$itens_orcamento]) && !empty($post['Cd_Produto'][$itens_orcamento]) && empty($arrItemsMoldurasOrcamento[$itens_orcamento])) {
                    foreach ($post['Cd_Produto'][$itens_orcamento] as $moldura_key) {
                      $arrItemsMoldurasOrcamento[$itens_orcamento][] = $moldura_key;
                    }
                  }

                  if (isset($post['CD_Componente'][$itens_orcamento]) && !empty($post['CD_Componente'][$itens_orcamento]) && empty($arrItemsComponentesOrcamento[$itens_orcamento])) {
                    foreach ($post['CD_Componente'][$itens_orcamento] as $componente_key) {
                      $arrItemsComponentesOrcamento[$itens_orcamento][] = $componente_key;
                    }
                  }
                }
                $arrItemsOrcamento[$key_][$key] = $value_;
              } elseif ($key == "Vl_Total") {
                // $value_ = floatval(str_replace(',', '.', str_replace('.', '', $value_)));
                $arrItemsOrcamento[$key_]["Vl_Bruto"] = $value_;
              } elseif ($key == 'descricao_produto_servico') {
                $arrItemsOrcamento[$key_]["descricao_produto_servico"] = $value_;
              } else {
                switch ($key) {
                  case 'VL_ADICIONAIS':
                  case 'Vl_Moldura':
                  case 'VL_desconto':
                  case 'vl_liquido':
                  case 'Vl_Entrada':
                    $value_ = floatval(str_replace(',', '.', str_replace('.', '', $value_)));
                    break;
                }
                $arrItemsOrcamento[$key_][$key] = $value_;
              }
            }
          }
        }
      }

      $crud = bd::getInstance($pdo, $this->nom_tabela);
      if ($Cd_Orcamento > 0) {
        $arrayOrcamento['modificado_por'] = $_SESSION['handle'];
        $arrayCond = array('Cd_Orcamento=' => $Cd_Orcamento);
        $crud->update($arrayOrcamento, $arrayCond);
      } else {
        $arrayOrcamento['criado_por'] = $post['criado_por'];
        
        // echo json_encode($arrayOrcamento,JSON_PRETTY_PRINT);

        // echo "<hr>";

        $sqlConsultaPedidoExistente = "
          SELECT
            COUNT(Cd_Orcamento) as qtdOrcamento,
            Cd_Orcamento
          FROM
            Orcamento
          WHERE
            Cd_Cliente = '".$arrayOrcamento['Cd_Cliente']."'
            AND Vl_Bruto = '".$arrayOrcamento['Vl_Bruto']."'
            AND Dt_Orcamento = '".$arrayOrcamento['Dt_Orcamento']."'
        ";

        // echo "<hr><pre>$sqlConsultaPedidoExistente</pre><hr>";
        
        $consultaPedidoExistente = $crud->getSQLGeneric($sqlConsultaPedidoExistente, [], FALSE);

        // print_r($consultaPedidoExistente);

        if ($consultaPedidoExistente->qtdOrcamento > 0) {
          $msg_pedido_existente = 'Possível pedido existente: <a href="'.URL.'/Orcamento/salvar/'.$consultaPedidoExistente->Cd_Orcamento.'" target="_blank">' . $consultaPedidoExistente->Cd_Orcamento.'</a><br>Clique no número do pedido para acessar ele <strong>OU</strong> entre em contato com o suporte do Maestria Sistema';
          throw new ErrorException($msg_pedido_existente);
        }
        
        $Cd_Orcamento  = $crud->insert($arrayOrcamento);
      }

      $consultaMoldurasOrcamento = $this->objMolduraItemOrcamento->listarMoldurasOrcamento($Cd_Orcamento);
      $arrMoldurasOrcamento = [];
      if (count($consultaMoldurasOrcamento) > 0) {
        foreach ($consultaMoldurasOrcamento as $moldura_orcamento) {
          $arrMoldurasOrcamento[$moldura_orcamento->Cd_Produto] = ['Qtd_Varas_Consumidas' => $moldura_orcamento->Qtd_Varas_Consumidas, 'Qtd_Cm_Consumidos' => $moldura_orcamento->Qtd_Cm_Consumidos];
        }
      }
      $crud_item_orcamento = bd::getInstance($pdo, 'Item_Orcamento');
      $crud_moldura_item_orcamento = bd::getInstance($pdo, 'Moldura_Item_Orcamento');
      $crud_componentes_item_orcamento = bd::getInstance($pdo, 'COMPONENTES_ITEM_ORCAMENTO');

      $crud_item_orcamento->delete(array('Cd_Orcamento' => $Cd_Orcamento));
      $crud_moldura_item_orcamento->delete(array('CD_ORCAMENTO' => $Cd_Orcamento));
      $crud_componentes_item_orcamento->delete(array('CD_ORCAMENTO' => $Cd_Orcamento));

      $consultaCliente = $objCliente->listarCliente($arrayOrcamento['Cd_Cliente']);

      foreach ($arrItemsOrcamento as $item_orcamento_key => $item_orcamento_value) {
        $arrTemp = array();
        foreach ($item_orcamento_value as $key => $value) {
          $arrTemp[$key] = $value;
        }
        
        $arrTemp['Cd_Orcamento'] = $Cd_Orcamento;
        $Cd_Item_Orcamento  = $crud_item_orcamento->insert($arrTemp);

        if (isset($arrItemsMoldurasOrcamento[$item_orcamento_key])) {
          $seqMoldura = 1;
          $valor_quebra_moldura_anterior = 0;
          $vl_unitario_moldura = 0;
          $soma_total_perdas = 0;
          $percentual_grupo_cliente = 0;
          foreach ($arrItemsMoldurasOrcamento[$item_orcamento_key] as $item_moldura_orcamento_key => $item_moldura_orcamento_value) {
            $arrTemp = array();

            $arrMolduraComplexidade = explode("|", $item_moldura_orcamento_value);

            $moldura_value = $arrMolduraComplexidade[0];
            $moldura_complexidade = (int) $arrMolduraComplexidade[1];
            $moldura_sarrafo_reforco = $arrMolduraComplexidade[2];
            $valor_produto = $arrMolduraComplexidade[3];

            $molduras = $this->objProduto->listarProduto($moldura_value);
            $molduras = $molduras[0];

            // $arrCalculaPreco = $this->calculaPreco(
            //   [
            //     'produtos' => [[
            //       'handle_produto' => $molduras->CodigoProduto,
            //       'handle_complexidade' => $moldura_complexidade,
            //       'sarrafo_reforco' => $moldura_sarrafo_reforco,
            //       'quantidade_item_individual' => 1
            //     ]],
            //     'handle_cliente' => $arrayOrcamento['Cd_Cliente'],
            //     'altura' => $post['Md_Altura'][$item_orcamento_key],
            //     'largura' => $post['Md_Largura'][$item_orcamento_key],
            //     'quantidade' => $post['Qt_Item'][$item_orcamento_key],
            //     'soma_total_perdas' => $soma_total_perdas,
            //     'valor_quebra_moldura_anterior' => $valor_quebra_moldura_anterior
            //   ]
            // );

            // print_r($arrCalculaPreco);

            // echo "<br><br>";

            $vl_unitario_moldura = $valor_produto; // !empty($arrCalculaPreco['precoProdutos']) ? $arrCalculaPreco['precoProdutos'][0]['valor_total'] : $arrCalculaPreco['valor_total_produtos'];
            // $valor_quebra_moldura_anterior = $arrCalculaPreco['valor_quebra_moldura_anterior'];
            // $soma_total_perdas = $arrCalculaPreco['soma_total_perdas'];

            if ($post['Id_Situacao'] >= 2) {
              $arrPercentualGrupo = $percentualCustoClienteGrupo->listarPercentualPorProduto($moldura_value);
              if (!empty($arrayOrcamento['Cd_Cliente']) && isset($arrPercentualGrupo[$consultaCliente[0]->codigoClienteGrupo])) {
                $percentual_grupo_cliente = $arrPercentualGrupo[$consultaCliente[0]->codigoClienteGrupo];
              }

              $arrComplexidade = $objComplexidadePorProduto->listarComplexidadePorProduto($moldura_value);

              $medida_vara = (float) $molduras->MedidaVara;
              $Md_Altura_Moldura = ($post['Md_Altura'][$item_orcamento_key] + $valor_quebra_moldura_anterior) * 2;
              $Md_Largura_Moldura = ($post['Md_Largura'][$item_orcamento_key] + $valor_quebra_moldura_anterior) * 2;

              $md_quebra = (float) $molduras->Desenho;

              // $valor_quebra_moldura_anterior = $valor_quebra_moldura_anterior + $md_quebra;

              $Md_Altura_Moldura = $Md_Altura_Moldura + ($md_quebra * $numero_cortes_considerar_perda);
              $Md_Largura_Moldura = $Md_Largura_Moldura + ($md_quebra * $numero_cortes_considerar_perda);
              $Md_Total = $Md_Altura_Moldura + $Md_Largura_Moldura;
              $Md_Total = $Md_Total + (($Md_Total * $valor_perda) / 100);
              $Md_Total = round($Md_Total, 2);
              $total_varas_consumidas = 0;
              $consumo_total_cm = 0;
              $atualiza_estoque = false;
              $atualiza_estoque_cm = false;
              $quantidade_atualizar_estoque = $molduras->Quantidade;
              $quantidade_atualizar_estoque_cm = isset($molduras->QuantidadeCm) ? $molduras->QuantidadeCm : 0;


              $consumo_total_cm = round(($Md_Total * $post['Qt_Item'][$item_orcamento_key]), 2);

              /*
              $preco_moldura = $percentual_grupo_cliente > 0 ? $molduras->PrecoCusto : $molduras->PrecoVendaMaoObra;
              $percentual_complexidade = isset($arrComplexidade[$moldura_complexidade]) ? $arrComplexidade[$moldura_complexidade]['percentual'] : 0;
              // echo "($consumo_total_cm / 100) * $preco_moldura <br>";
              $vl_unitario_moldura = ($consumo_total_cm / 100) * $preco_moldura;
              if ($molduras->DescricaoUnidadeMedida == 'UN') {
                $vl_unitario_moldura = $preco_moldura * $post['Qt_Item'][$item_orcamento_key];
              }
              // echo "<p>$molduras->DescricaoUnidadeMedida (($consumo_total_cm / 100) * $preco_moldura) -> VL_UNITARIO: $vl_unitario_moldura</p>";
              if ($percentual_grupo_cliente > 0) {
                $vl_unitario_moldura = $vl_unitario_moldura + ($vl_unitario_moldura * ($percentual_grupo_cliente / 100));
              }

              if ($percentual_complexidade > 0) {
                $vl_unitario_moldura = $vl_unitario_moldura + ($vl_unitario_moldura * ($percentual_complexidade / 100));
              }

              echo "<p>$vl_unitario_moldura</p>";
              */

              if ($medida_vara > 0) {
                $total_varas_consumidas = ceil(($Md_Total * $post['Qt_Item'][$item_orcamento_key]) / $medida_vara);
                $atualiza_estoque = true;
                $atualiza_estoque_cm = true;
                $quantidade_atualizar_estoque = ($molduras->Quantidade - $total_varas_consumidas);
                $quantidade_atualizar_estoque_cm = ($molduras->QuantidadeCm - $consumo_total_cm);
              }

              if (isset($arrMoldurasOrcamento[$moldura_value]['Qtd_Varas_Consumidas'])) {
                if ($total_varas_consumidas < $arrMoldurasOrcamento[$moldura_value]['Qtd_Varas_Consumidas']) {
                  $quantidade_atualizar_estoque = ($molduras->Quantidade + ($arrMoldurasOrcamento[$moldura_value]['Qtd_Varas_Consumidas'] - $total_varas_consumidas));
                } else if ($total_varas_consumidas > $arrMoldurasOrcamento[$moldura_value]['Qtd_Varas_Consumidas']) {
                  $quantidade_atualizar_estoque = ($molduras->Quantidade - ($total_varas_consumidas - $arrMoldurasOrcamento[$moldura_value]['Qtd_Varas_Consumidas']));
                } else {
                  $atualiza_estoque = false;
                }
              }

              if (isset($arrMoldurasOrcamento[$moldura_value]['Qtd_Cm_Consumidos'])) {
                if ($consumo_total_cm < $arrMoldurasOrcamento[$moldura_value]['Qtd_Cm_Consumidos']) {
                  $quantidade_atualizar_estoque_cm = ($molduras->QuantidadeCm + ($arrMoldurasOrcamento[$moldura_value]['Qtd_Cm_Consumidos'] - $total_varas_consumidas));
                } else if ($consumo_total_cm > $arrMoldurasOrcamento[$moldura_value]['Qtd_Cm_Consumidos']) {
                  $quantidade_atualizar_estoque_cm = ($molduras->QuantidadeCm - ($consumo_total_cm - $arrMoldurasOrcamento[$moldura_value]['Qtd_Cm_Consumidos']));
                } else {
                  $atualiza_estoque_cm = false;
                }
              }

              $debugStock = json_encode([
                'moldura' => $moldura_value,
                'complexidade' => $moldura_complexidade,
                'atualiza_estoque' => (int) $atualiza_estoque,
                'total_varas_consumidas' => $total_varas_consumidas,
                'consumo_total_cm' => $consumo_total_cm,
                'quantidade_atual' => $molduras->Quantidade,
                'quantidade_atual_cm' => isset($molduras->QuantidadeCm) ? $molduras->QuantidadeCm : 0,
                'novo estoque' => $quantidade_atualizar_estoque,
                'novo estoque cm' => $quantidade_atualizar_estoque_cm
              ]);

              // echo "<p> $debugStock </p>";

              if ($atualiza_estoque && $desabilitar_controle_estoque != 'true') {
                $this->objProduto->editarProduto(
                  [
                    'handle' => $moldura_value,
                    'Quantidade' => max($quantidade_atualizar_estoque, 0)
                  ]
                );
              }
              $arrTemp['Qtd_Varas_Consumidas'] = $total_varas_consumidas;

              if ($atualiza_estoque_cm && $desabilitar_controle_estoque != 'true') {
                $this->objProduto->editarProduto(
                  [
                    'handle' => $moldura_value,
                    'QuantidadeCm' => max($quantidade_atualizar_estoque_cm, 0)
                  ]
                );
              }
              $arrTemp['Qtd_Cm_Consumidos'] = $consumo_total_cm;
            }

            $arrTemp['Cd_Produto'] = $moldura_value;
            $arrTemp['Cd_Orcamento'] = $Cd_Orcamento;
            $arrTemp['Cd_Item_Orcamento'] = $Cd_Item_Orcamento;
            $arrTemp['Sequencia'] = $seqMoldura;
            $arrTemp['codigoComplexidade'] = $moldura_complexidade;
            $arrTemp['sarrafo_reforco']  = $moldura_sarrafo_reforco;
            $arrTemp['Vl_Unitario'] = $vl_unitario_moldura;
            $a = $crud_moldura_item_orcamento->insert($arrTemp);

            $seqMoldura++;
          }
        }
        if (isset($arrItemsComponentesOrcamento[$item_orcamento_key])) {
          $seqComponente = 1;

          $Md_Altura = $post['Md_Altura'][$item_orcamento_key];
          $Md_Largura = $post['Md_Largura'][$item_orcamento_key];

          $metro_linear = ($Md_Altura / 100) * ($Md_Largura / 100);
          $metro_linear = $metro_linear + (($metro_linear * $valor_perda) / 100);

          $valor_unitario_componentes = 0;
          foreach ($arrItemsComponentesOrcamento[$item_orcamento_key] as $item_componentes_orcamento_key => $item_componentes_orcamento_value) {

            $arrComponenteQuantidade = explode("|", $item_componentes_orcamento_value);
            $handle_componente = $arrComponenteQuantidade[0];
            $quantidade_componente = isset($arrComponenteQuantidade[1]) ? $arrComponenteQuantidade[1] : 0;
            $valor_produto = $arrComponenteQuantidade[2];

            $arrComponentes = $this->objProduto->listarProduto($handle_componente);

            // $arrCalculaPreco = $this->calculaPreco(
            //   [
            //     'produtos' => [[
            //       'handle_produto' => $handle_componente,
            //       'handle_complexidade' => '',
            //       'sarrafo_reforco' => 'N',
            //       'quantidade_item_individual' => $quantidade_componente
            //     ]],
            //     'handle_cliente' => $arrayOrcamento['Cd_Cliente'],
            //     'altura' => $post['Md_Altura'][$item_orcamento_key],
            //     'largura' => $post['Md_Largura'][$item_orcamento_key],
            //     'quantidade' => $post['Qt_Item'][$item_orcamento_key],
            //     'soma_total_perdas' => $soma_total_perdas,
            //     'valor_quebra_moldura_anterior' => $valor_quebra_moldura_anterior
            //   ]
            // );

            // print_r($arrCalculaPreco);

            // echo "<br><br>";

            $arrTemp = array();
            //$arrComponentes = $objComponente->listarComponente($item_componentes_orcamento_value);
            
            // $custo_componente = $arrComponentes[0]->PrecoCusto;
            // $margem_venda_componente = $arrComponentes[0]->margem_venda !== null ? $arrComponentes[0]->margem_venda : $valor_margem_bruta;
            // $valor_unitario_componente = $metro_linear * ($custo_componente + (($custo_componente * $margem_venda_componente) / 100));
            // $valor_unitario_componente = $valor_unitario_componente * $post['Qt_Item'][$item_orcamento_key];

            // echo "$metro_linear * ($custo_componente + (($custo_componente * $margem_venda_componente) / 100)) <br>";
            // echo "valor_unitario_componente: $valor_unitario_componente <br><br>#####<br><br>"; 

            $arrTemp['id_componente_item_orcamento'] = 0;
            $arrTemp['CD_COMPONENTE'] = $handle_componente;
            $arrTemp['CD_ORCAMENTO'] = $Cd_Orcamento;
            $arrTemp['ITEM_ORCAMENTO'] = $Cd_Item_Orcamento;
            $arrTemp['SEQUENCIA'] = $item_componentes_orcamento_key;
            $arrTemp['VALOR_UNITARIO'] = $valor_produto; //$arrCalculaPreco['valor_total_produtos'];
            $arrTemp['quantidade'] = $quantidade_componente;
            $crud_componentes_item_orcamento->insert($arrTemp);
            $seqComponente++;
          }
        }
      }
      return $Cd_Orcamento;
  } catch(Exception $e) {
    echo "<h1> Erro ao cadastrar o pedido </h1>";
    echo "<p> {$e->getMessage()} </p>";
    echo '<textarea cols="100" rows="10">'.json_encode($post).'</textarea>';
    exit;
  }
  }

  function calculaPreco($parametros) {

    $objComplexidadeProduto = new ComplexidadeProduto();
    $objPercentualCustoClienteGrupo = new PercentualCustoClienteGrupo();
    $configuracoes = new Configuracoes();
    $objCliente = new Cliente();
    $objProduto = new Produto();
    $ComposicaoPreco = new ComposicaoPreco();

    $numero_cortes_considerar_perda = $configuracoes->listarConfiguracao('numero_cortes_considerar_perda')->valor;
    $considera_soma_total_perdas = (bool) filter_var($configuracoes->listarConfiguracao('considera_soma_total_perdas')->valor, FILTER_VALIDATE_BOOLEAN);
    $arredondar_medida_total_quadro = (bool) filter_var($configuracoes->listarConfiguracao('arredondar_medida_total_quadro')->valor, FILTER_VALIDATE_BOOLEAN);
    $quantidade_lados_considerar = (int) $configuracoes->listarConfiguracao('lados_considerar_no_calculo_moldura')->valor;    

    $composicoesPreco = $ComposicaoPreco->listarComposicaoPreco(1);
    $valor_perda = $composicoesPreco[0]->VL_PERDA_MEDIA;
    
    $produtos = $parametros['produtos'];
    $valor_quebra_moldura_anterior = isset($parametros['valor_quebra_moldura_anterior']) ? $parametros['valor_quebra_moldura_anterior'] : 0;
    $soma_total_perdas = isset($parametros['soma_total_perdas']) ? $parametros['soma_total_perdas'] : 0;
    $valor_unitario_molduras = 0;
    $consumo_total_cm = 0;
    $total_varas_consumidas = 0;
    $perda_total = 0;
    $arrPrecoMoldura = [];
    foreach ($produtos as $produto) {
      $consultaProduto = $objProduto->listarProduto($produto['handle_produto'])[0];
      
      $complexidadeProdutoPedido = $produto['handle_complexidade'];
      $sarrafo_reforco = $produto['sarrafo_reforco'];
      $handle_cliente = $parametros['handle_cliente'];
      $Md_Altura = $parametros['altura'];
      $Md_Largura = $parametros['largura'];
      $Qt_Item = $parametros['quantidade'];

      $arrComplexidade = $objComplexidadeProduto->listarComplexidadePorProduto($consultaProduto->CodigoProduto);
      $arrPercentualGrupo = $objPercentualCustoClienteGrupo->listarPercentualPorProduto($consultaProduto->CodigoProduto);
      $arrComplexidade = $objComplexidadeProduto->listarComplexidadePorProduto($consultaProduto->CodigoProduto);
      $consultaCliente = $objCliente->listarCliente($handle_cliente);

      $custo_moldura = $consultaProduto->definicao_preco == 1 ? $consultaProduto->PrecoVendaMaoObra : ($consultaProduto->PrecoCusto + ($consultaProduto->PrecoCusto * ($consultaProduto->margem_venda / 100)));
      $percentual_grupo_cliente = 0;
      $percentual_complexidade = isset($arrComplexidade[$complexidadeProdutoPedido]) ? $arrComplexidade[$complexidadeProdutoPedido]['percentual'] : 0;
      $quantidade_item_unitario = isset($produto['quantidade']) && $produto['quantidade'] > 0 ? $produto['quantidade'] : 1;

      if (!empty($handle_cliente) && isset($arrPercentualGrupo[$consultaCliente[0]->codigoClienteGrupo])) {
        $percentual_grupo_cliente = $arrPercentualGrupo[$consultaCliente[0]->codigoClienteGrupo];
      }
      $preco_custo = $percentual_grupo_cliente > 0 ? $consultaProduto->PrecoCusto : $custo_moldura;
      $custo_moldura = $preco_custo + ($preco_custo * ($percentual_grupo_cliente / 100));

      if ($percentual_complexidade > 0) {
        $custo_moldura = $custo_moldura + ($custo_moldura * ($percentual_complexidade / 100));
      }

      $medida_vara = (float) $consultaProduto->MedidaVara;

      /**
       * Tipo de Material:
       * Moldura - Multiplicar por 2
       * Espelho - Multiplicar por 1
       * Escora  - Multiplicar por 1
       */
      $Md_Altura_Moldura =  $sarrafo_reforco == 'H' ? 0 : $Md_Altura ; // ($Md_Altura + $valor_quebra_moldura_anterior) * 1;
      $Md_Largura_Moldura = $sarrafo_reforco == 'V' ? 0 : $Md_Largura; // ($Md_Largura + $valor_quebra_moldura_anterior) * 1;
      // echo "<p> # $Md_Altura_Moldura - $Md_Largura_Moldura </p>";
      if ($consultaProduto->MetragemLinear == 1 & $sarrafo_reforco == 'N') {
        $Md_Altura_Moldura = ($Md_Altura + $valor_quebra_moldura_anterior) * $quantidade_lados_considerar;
        $Md_Largura_Moldura = ($Md_Largura + $valor_quebra_moldura_anterior) * $quantidade_lados_considerar;
      }
      // echo "<p> ## $Md_Altura_Moldura - $Md_Largura_Moldura </p>";

      if ($consultaProduto->DescricaoUnidadeMedida === 'UN') {
        $Md_Altura_Moldura = 1;
        $Md_Largura_Moldura = 1;
      }

      $md_quebra =  !$considera_soma_total_perdas ? $consultaProduto->Desenho : 0;
      $valor_quebra_moldura_anterior = !$considera_soma_total_perdas ? $valor_quebra_moldura_anterior + ($md_quebra * 2) : 0;
      $soma_total_perdas = $soma_total_perdas + ($md_quebra * $numero_cortes_considerar_perda);

      // echo "<p>$consultaProduto->DescricaoProduto - $valor_quebra_moldura_anterior - soma_total_perdas: $soma_total_perdas</p>";
      if ($quantidade_lados_considerar == 2) {
        $Md_Altura_Moldura = $consultaProduto->MetragemLinear == 1 && $md_quebra > 0 && $sarrafo_reforco == 'N' ? $Md_Altura_Moldura + ($md_quebra * $numero_cortes_considerar_perda) : $Md_Altura_Moldura;
        $Md_Largura_Moldura = $consultaProduto->MetragemLinear == 1 && $md_quebra > 0 && $sarrafo_reforco == 'N' ? $Md_Largura_Moldura + ($md_quebra * $numero_cortes_considerar_perda) : $Md_Largura_Moldura;
        // echo "<p> ### $Md_Altura_Moldura - $Md_Largura_Moldura </p>";
        $Md_Total = $Md_Altura_Moldura + $Md_Largura_Moldura;
      } else {
        $Md_Total = ($Md_Altura_Moldura + $Md_Largura_Moldura) * $md_quebra;
      }
      // echo "<p> * $Md_Total</p>";

      /**
       * Moldura: Medida linear (Altura + Largura)
       * Espelho: Medida quadrado (Altura * Largura)
       * Escora: Sem medidas, cobrado por unidade
       */
      if ($consultaProduto->MetragemLinear == 0 && $consultaProduto->DescricaoUnidadeMedida !== 'UN') {
        $Md_Total = $Md_Altura_Moldura * $Md_Largura_Moldura;
        // $valor_perda = 0;
      } else if ($consultaProduto->DescricaoUnidadeMedida !== 'UN') {
        if ($quantidade_lados_considerar !== 1) {
          $Md_Total =  !$considera_soma_total_perdas ? $Md_Altura_Moldura + $Md_Largura_Moldura : ($Md_Altura_Moldura + $Md_Largura_Moldura) + $perda_total;
        }
      } else {
        $Md_Total = 1 * $quantidade_item_unitario;
      }
      // echo "<p> ** $Md_Total</p>";

      // echo "<p>$consultaProduto->MetragemLinear</p>";

      if ($consultaProduto->MetragemLinear == 1) {
        // echo "<p> $Md_Total + (($Md_Total * $valor_perda) / 100) </p>";
        $Md_Total = $Md_Total + (($Md_Total * $valor_perda) / 100);
      }

      // echo "<p> *** $Md_Total</p>";

      /**
       * Se for espelho, dividi-se por 100 
       * porque o valor é m²
       */
      if ($consultaProduto->MetragemLinear == 0 && $consultaProduto->DescricaoUnidadeMedida !== 'UN') {
        $Md_Total = $arredondar_medida_total_quadro == 'true' ? ceil($Md_Total / 100) : $Md_Total / 100;
      } else if ($consultaProduto->DescricaoUnidadeMedida !== 'UN') {
        $Md_Total = round($Md_Total, 2);
        if ($arredondar_medida_total_quadro == 'true') {
          $Md_Total = round($Md_Total, -1);
        }
      }

      // echo "<p>$Md_Total</p>";

      if ($consultaProduto->DescricaoUnidadeMedida !== 'UN') {
        $valor_unitario_molduras += round(($Md_Total * $custo_moldura) / 100, 2);
        $arrPrecoMoldura[]  = ['item' => $consultaProduto->CodigoProduto, 'valor_individual' => $custo_moldura, 'valor_total' => round(($Md_Total * $custo_moldura) / 100, 2), 'consumo' => $Md_Total, 'descricao' => $consultaProduto->DescricaoProduto];
      } else {
        $valor_unitario_molduras += ($custo_moldura * $quantidade_item_unitario);
      }

      $consumo_total_cm += round($Md_Total * $Qt_Item);
      $total_varas_consumidas += $medida_vara > 0 ? ceil(($Md_Total * $Qt_Item) / $medida_vara) : 0;

    }

    return [
      'precoProdutos' => $arrPrecoMoldura,
      'valor_total_produtos' => $valor_unitario_molduras,
      'valor_quebra_moldura_anterior' => $valor_quebra_moldura_anterior,
      'soma_total_perdas' => $soma_total_perdas
    ];

  }

  function pedidosPorSituacao($filtro_empresa = null)
  {
    $pdo = Conexao::getInstance();
    $arrayParam = array();

    $where_restricao = '';
    if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
      $where_restricao .= sprintf(
        ' AND id_empresa IN (%s)',
        implode(',', $this->arrPermissoesUsuarioEmpresa)
      );
    }

    if (!$this->permissao_visualizar_pedidos_outros_vendedores) {
      $where_restricao .= sprintf(
        ' AND criado_por  =  %s',
        (int) $_SESSION['handle']
      );
    }

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $filtro_por_empresa = '';
    if ($filtro_empresa) {
      $filtro_empresa = ' AND o.id_empresa = ' . $filtro_empresa;
    }

    $sql = "select s.idSituacao, s.descricao, (select count(o.Cd_Orcamento) from " . $this->nom_tabela . " o INNER JOIN Empresas e ON (o.id_empresa = e.CodigoEmpresa) WHERE o.Id_Situacao = s.idSituacao $where_restricao $filtro_empresa ) as total from Situacao s ";
    $sql .= ' UNION 
					SELECT "-1", "Atrasado", COUNT(o.Cd_Orcamento) from Orcamento o INNER JOIN Empresas e ON (o.id_empresa = e.CodigoEmpresa) WHERE o.Id_Situacao IN (2,3) and o.Dt_Prevista_Entrega < NOW() ' . $where_restricao .  $filtro_empresa;

    // echo "<pre>$sql</pre>";exit;
    $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    return $dados;
  }

  function pedidosPorCliente($handle_cliente)
  {
    $pdo = Conexao::getInstance();
    $arrayParam = array();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql = "select count(o.Cd_Orcamento) as total from Orcamento o WHERE Cd_Cliente = " . $handle_cliente;

    $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    return $dados;
  }

  public function editarOrcamento($post) {
		$pdo = Conexao::getInstance();
    $objHistoricoPedido = new HistoricoPedido();
		
		$arraySituacao = array();
		foreach ($post as $key => $value) {
			if ($key != 'handle')
				$arraySituacao[$key] =  $value;
		}
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$arrayCond = array('Cd_Orcamento=' => $post['handle']);  
		$retorno   = $crud->update(['Id_Situacao' => $post['Id_Situacao']], $arrayCond);

    $objHistoricoPedido->cadastrarHistoricoPedido(
      [
        'id_pedido' => $post['handle'],
        'id_situacao_de' => $post['id_situacao_de'],
        'id_situacao_para' => $post['Id_Situacao'],
        'observacao' => $post['observacaoHistorico'],
        'id_usuario' => $_SESSION['handle'],
        'data' => date('Y-m-d h:i:s')
      ]
    );

		return $retorno;
	}
}
