<?php
include_once(__DIR__ . '/../Controller/CommonController.php');
class Bills
{
    public $BillsId;
    public $UserId;
    public $Title;
    public $Date;
    public $CategoryId;
    public $AccountId;
    public $Amount;
    public $Description;
    public $IDCONTA;
    public $cdtvencimento;
    public $cdtpagamento;
    public $doc;
    public $ndoc;
    public $dtinclusao;
    public $dtalteracao;

    public $nom_tabela = "bills";
    private $common;
    public function __construct()
    {
        $this->common = new CommonController();
        $this->BillsId = 0;
        $this->UserId = 0;
        $this->Title = "";
        $this->Date = date("Y-m-d");
        $this->CategoryId = 0;
        $this->AccountId = 0;
        $this->Amount = 0;
        $this->Description = "";
        $this->IDCONTA = 0;
        $this->cdtvencimento = "";
        $this->cdtpagamento = "";
        $this->doc = "";
        $this->ndoc = "";
        $this->dtinclusao = "";
        $this->dtalteracao = "";
    }

    public function listarTodos(
        $arrParametros = []
    ) {
        $pagina_atual = 0;
        if (isset($arrParametros['pagina_atual'])) {
          $pagina_atual = $arrParametros['pagina_atual'];
        }

        $linha_inicial = 0;
        if (isset($arrParametros['linha_inicial'])) {
          $linha_inicial = $arrParametros['linha_inicial'];
        }

        $coluna = "";
        if (isset($arrParametros['coluna'])) {
          $coluna = $arrParametros['coluna'];
        }
        
        $buscar = "";
        if (isset($arrParametros['buscar'])) {
          $buscar = $arrParametros['buscar'];
        }        

        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $where = "";
        if ($coluna != "" && $buscar != "") {
            $where = sprintf(
                ' WHERE %s LIKE UPPER("%%%s%%") ',
                $coluna,
                strtoupper($buscar)
            );
        }
        
        if (isset($arrParametros['filtro']) && !empty($arrParametros['filtro'])) {
          $tamanhoFiltros = count($arrParametros['filtro']);
          $where = ' WHERE ';
          $and = ' ';
          $aux = 0;
          $condicao = ' = ';
          $arrDatas = [
            'DataVencimento' => 'cdtvencimento',
            'DataLancamento' => 'Date',
            'DataPagamento' => 'cdtpagamento'
          ];
          foreach ($arrParametros['filtro'] as $key => $value) {
            if (empty(trim($value))) {
              $tamanhoFiltros--;
              continue;
            }
            $coluna = $key;
            $valor = $value;
            switch ($key) {
              case 'UserId';
                $coluna = 'account.'.$key;
                break;

              case 'CategoryId';
              $coluna = 'category.'.$key;
              break;      
              
              case 'AccountId';
              $coluna = 'account.'.$key;
              break;               

              case 'filtroDataVencimentoInicio';
              case 'filtroDataLancamentoInicio';
              case 'filtroDataPagamentoInicio';
              $condicao = ' >= ';
              $coluna = str_replace(['filtro','Inicio'],'',$key);
              $coluna = $arrDatas[$coluna];
              $valor = $coluna != 'cdtvencimento' ? $this->common->dateFormat($value) . ' 00:00:00' : $this->common->dateFormat($value,'Y-m-d');
              break;

              case 'filtroDataVencimentoFim';
              case 'filtroDataLancamentoFim';
              case 'filtroDataPagamentoFim';
              $condicao = ' <= ';
              $coluna = str_replace(['filtro','Fim'],'',$key);
              $coluna = $arrDatas[$coluna];              
              $valor = $coluna != 'cdtvencimento' ? $this->common->dateFormat($value) . ' 00:00:00' : $this->common->dateFormat($value,'Y-m-d');
              break;              
            }
            $where .= sprintf('%s %s %s "%s" ',$and, $coluna, $condicao , $valor);
            $aux++;
            if ($aux <= $tamanhoFiltros) {
              $and = ' AND ';
            }
          }
        }
        $paginacao = "";
        // $paginacao = " LIMIT " . QTDE_REGISTROS;
        // if ($pagina_atual > 0 && $linha_inicial > 0) {
        //     $paginacao = " LIMIT {$linha_inicial}, " . QTDE_REGISTROS;
        // }

        $sql = "
                  SELECT 
                    bills.BillsId,
                    bills.UserId,
                    bills.Title,
                    bills.Date,
                    bills.CategoryId,
                    bills.AccountId,
                    bills.Amount,
                    bills.Description,
                    bills.IDCONTA,
                    bills.cdtvencimento,
                    IF (DAYNAME(bills.cdtvencimento) IS NOT NULL, bills.cdtvencimento,'') as cdtvencimento,
                    IF (DAYNAME(bills.cdtpagamento) IS NOT NULL, bills.cdtpagamento,'') as cdtpagamento,
                    bills.doc,
                    bills.ndoc,
                    bills.dtinclusao,
                    bills.dtalteracao,
                    Fornecedores.RazaoSocial,
                    category.CategoryName,
                    Empresas.RazaoSocial as Empresa,
                    account.AccountName nomeConta,
                    IF(bills.cdtpagamento = '' and bills.cdtvencimento < current_date(), 'VENCIDO', IF(bills.cdtpagamento <> '','PAGO', 'A VENCER')) as situacao
                  FROM " . $this->nom_tabela . '
                  LEFT JOIN Fornecedores ON Fornecedores.CodigoFornecedor = bills.Title 
                  LEFT JOIN category ON category.CategoryId = bills.CategoryId 
                  LEFT JOIN Empresas ON bills.UserId = Empresas.CodigoEmpresa 
                  LEFT JOIN account ON account.AccountId = bills.AccountId ' 
                  . $where . 
                  ' 
                    ORDER BY cdtvencimento ASC' . $paginacao;

        // echo $sql;
        $dados = $crud->getSQLGeneric($sql);

        return $dados;
    }

    public function listar($handle)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $sql = "SELECT                     
                    bills.BillsId,
                    bills.UserId,
                    bills.Title,
                    bills.Date,
                    bills.CategoryId,
                    bills.AccountId,
                    bills.Amount,
                    bills.Description,
                    bills.IDCONTA,
                    bills.cdtvencimento,
                    IF (DAYNAME(bills.cdtvencimento) IS NOT NULL, bills.cdtvencimento,'') as cdtvencimento,
                    IF (DAYNAME(bills.cdtpagamento) IS NOT NULL, bills.cdtpagamento,'') as cdtpagamento,
                    bills.doc,
                    bills.ndoc,
                    bills.dtinclusao,
                    bills.dtalteracao
                FROM " 
                    . $this->nom_tabela . "
                WHERE
                  BillsId = ?";
        $arrayParam = [$handle];
        $dados = $crud->getSQLGeneric($sql, $arrayParam, false);
        return $dados;
    }

    public function listarTodosTotal()
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela;
        $dados = $crud->getSQLGeneric($sql, null, false);
        return $dados->total_registros;
    }

    public function editar($post)
    {
        
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);

        $arrEditar = [];
        unset(
            $post['tipoRecebimento'],
            $post['diaVencimentoSemana'],
            $post['diaVencimento'],
            $post['quantidadeRepeticao'],
            $post['quantidadeParcelas']
        );        
        foreach ($post as $key => $value) {
          if ($key != "handle") {
            if ($key == 'Amount') {
              $value = $this->common->monetaryValue($value);
            }
            if ($key == 'cdtpagamento' || $key == 'Date') {
              if ($value == '') {
                $value = '0000-00-00 00:00:00';
              } else {
                $value = $this->common->dateFormat($value);
              }
            }
            if ($key == 'cdtvencimento') {
              $value = $this->common->dateFormat($value,'Y-m-d');
            }            
            $arrEditar[$key] = $value;
          }
        }

        $arrayCond = ["BillsId=" => $post["handle"]];
        $crud->update($arrEditar, $arrayCond);
        

        return true;        
    }

    public function cadastrar($post)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $arrInserir = [];

        $tipoRecebimento = $post['tipoRecebimento'];
        $diaVencimentoSemana = $post['diaVencimentoSemana'];
        $diaVencimento = $post['diaVencimento'];
        $quantidadeParcelas = $post['quantidadeParcelas'];
        $quantidadeRepeticao = $quantidadeParcelas !== '' ? $quantidadeParcelas : $post['quantidadeRepeticao'];
        unset(
            $post['tipoRecebimento'],
            $post['diaVencimentoSemana'],
            $post['diaVencimento'],
            $post['quantidadeRepeticao'],
            $post['quantidadeParcelas']
        );

        foreach ($post as $key => $value) {
          if ($key != "handle") {
            if ($key == 'Amount') {
              $value = $this->common->monetaryValue($value);
              if ($quantidadeParcelas !== '') {
                $value = $value / $quantidadeParcelas;
              }
            }
            if ($key == 'cdtpagamento' || $key == 'Date') {
              if ($value == '') {
                $value = '0000-00-00 00:00:00';
              } else {
                $value = $this->common->dateFormat($value);
              }
            }
            if ($key == 'cdtvencimento') {
              $value = $this->common->dateFormat($value,'Y-m-d');
            }            
            $arrInserir[$key] = $value;
          }
        }

        $arrInserir = array_merge($arrInserir, [
          'IDCONTA' => 0,
          'dtinclusao' => date('Y-m-d H:i:s'),
          'dtalteracao' => date('Y-m-d H:i:s')
        ]);

        $weekDays = array('sunday', 'monday', 'tuesday', 'wednesday','thursday','friday', 'saturday');
        $arrVencimentos = new DateTime($arrInserir['cdtvencimento']);
        if ($diaVencimentoSemana !== '') {
          $weekday = new DateTime($weekDays[$diaVencimentoSemana]);
          $endDate = clone $weekday;
          $endDate->modify('+'.($quantidadeRepeticao * 7).' days');
          $dateInterval = new DateInterval('P1W');
          $arrVencimentos = new DatePeriod($weekday, $dateInterval, $endDate);
        } else {
          if ($diaVencimento !== '') {
            switch ($tipoRecebimento) {
              case 'M':
              default:
                $intervaloDias = 30;
                $intervalo = 'P1M';
                break; 
              case 'T':
                $intervaloDias = 90;
                $intervalo = 'P3M';
                break;
              case 'S':
                $intervaloDias = 180;
                $intervalo = 'P6M';
                break;
              case 'A':
                $intervaloDias = 365;
                $intervalo = 'P1Y';
                break;                                                
            }
            $weekday = new DateTime($arrInserir['cdtvencimento']);
            $endDate = clone $weekday;
            $endDate->modify('+'.($quantidadeRepeticao * $intervaloDias).' days');
            $dateInterval = new DateInterval($intervalo);
            $arrVencimentos = new DatePeriod($weekday, $dateInterval, $endDate);
          } else {
            $weekday = new DateTime($arrInserir['cdtvencimento']);
            $endDate = clone $weekday;
            $endDate->modify('+1 days');
            $dateInterval = new DateInterval('P1D');
            $arrVencimentos = new DatePeriod($weekday, $dateInterval, $endDate);
            $diaVencimento = date('d', strtotime($arrInserir['cdtvencimento']));
          }
        }

        foreach ($arrVencimentos as $key => $vencimento) {
          $crud->insert(array_merge($arrInserir, ['cdtvencimento' => $vencimento->format(sprintf('Y-m-%s', $diaVencimento))]));
        }
        return true;
    }

    public function excluir($handle)
    {
        $pdo = Conexao::getInstance();
        $crud = bd::getInstance($pdo, $this->nom_tabela);
        $crud->delete(["BillsId" => $handle]);
        return true;
    }

    public function listarSaidasPorPeriodo($arrFiltros) {
      $pdo = Conexao::getInstance();
      $crud = bd::getInstance($pdo, $this->nom_tabela);
      $sql = sprintf(
        'SELECT
          SUM(Amount) as total,
          bills.UserId,
          account.tipo,
          account.AccountName nomeConta
        FROM
          bills
        LEFT JOIN category on bills.CategoryId = category.CategoryId 
        LEFT JOIN account on bills.AccountId = account.AccountId
        WHERE
          account.tipo = 0
          AND bills.cdtpagamento BETWEEN "%s" AND "%s"
          AND account.tipo <> 4
        GROUP BY bills.UserId, account.tipo
        ',
        '2000-01-01 00:00:00',
        date('Y-m-d H:i:s')
        )
        ;
      // echo "<pre>$sql</pre>";
      return $crud->getSQLGeneric($sql, [], true);
    }        
    
    public function listarPagamentosFuturo($arrFiltros) {
      $pdo = Conexao::getInstance();
      $crud = bd::getInstance($pdo, $this->nom_tabela);
      $sql = sprintf(
        'SELECT
          SUM(Amount) as total,
          bills.UserId,
          account.tipo
        FROM
          bills
        LEFT JOIN category on bills.CategoryId = category.CategoryId 
        LEFT JOIN account on bills.AccountId = account.AccountId
        WHERE
          cdtpagamento = "[NULL]"
          AND bills.cdtvencimento >= "%s"
        GROUP BY bills.UserId, account.tipo
        ',
        date('Y-m-d H:i:s')
        )
        ;
      // echo "<pre>$sql</pre>";
      return $crud->getSQLGeneric($sql, [], true);
    }     
}