<?php
require_once('conexao.php');
require_once('bd.php');
class ProdutoAuxiliar {

	public $id = 0;

  public $descricao = '';

  public $status;

  public $metragem;
	public $nom_tabela = 'Produtos_Auxiliares';
	public $order_by_default = 'descricao';	
	
	public function __construct() {
		$this->id = '';
    $this->status = 1;
    $this->metragem = 1;
    $this->descricao = '';
	}
	
	public function listarTodos($pagina_atual = 0,$linha_inicial = 0,$coluna = '',$buscar = '') {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$where = '';
		if ($coluna != '' && $buscar != '') {
			$where = sprintf(' WHERE %s LIKE UPPER("%%%s%%") ',$coluna,strtoupper($buscar));
		}
		
// 		$paginacao = ' LIMIT '.QTDE_REGISTROS;
		$paginacao = '';
		if ($pagina_atual > 0 && $linha_inicial > 0) {
			$paginacao = " LIMIT {$linha_inicial}, ".QTDE_REGISTROS;
		}		
		
		$sql = "SELECT * FROM ".$this->nom_tabela.$where." ORDER BY ".$this->order_by_default.$paginacao;		
		$dados = $crud->getSQLGeneric($sql);
		return $dados;
	}

	public function listarTodosTotal() {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$sql = "SELECT count(*) as total_registros FROM ".$this->nom_tabela;		
		
		$dados = $crud->getSQLGeneric($sql,null,FALSE);		
		
		return $dados->total_registros;
		
		//
	}	
	
	public function listarProdutoAuxiliar($handle) {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$sql = "SELECT * FROM ".$this->nom_tabela." WHERE id = ?";
		$arrayParam = array($handle); 
		$dados = $crud->getSQLGeneric($sql,$arrayParam, TRUE);
		
		return $dados;
		
		//
	}	
	
	public function editarProdutoAuxiliar($post) {
		$pdo = Conexao::getInstance();
		$arrayProdutoAuxiliar = array();
		foreach ($post as $key => $value) {
			if ($key != 'id')
				$arrayProdutoAuxiliar[$key] =  $value;
		}
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$arrayCond = array('id=' => $post['id']);  
		$retorno   = $crud->update($arrayProdutoAuxiliar, $arrayCond);  		
		
		return $arrayProdutoAuxiliar['descricao'];
	}
	
	public function cadastrarProdutoAuxiliar($post) {
		$pdo = Conexao::getInstance();
		
		$arrayProdutoAuxiliar = array();
		foreach ($post as $key => $value) {
			if ($key != 'id')
				$arrayProdutoAuxiliar[$key] =  $value;
		}		
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);

		$retorno   = $crud->insert($arrayProdutoAuxiliar);
		
		return $retorno;
	}	
}
?>
