<?php
require_once('conexao.php');
require_once('bd.php');
class ClienteGrupo {
	public $codigoClienteGrupo = 0;
	public $nome = '';
	public $status = 1;
	public $aplicacao = 'A';
	public $definicao_preco = 2;
	public $valor = '0';
	
	public $nom_tabela = 'ClienteGrupo';

	
	public function __construct() {
	}
	
	public function listarTodos($pagina_atual = 0,$linha_inicial = 0,$coluna = '',$buscar = '', $quantidade = '', $ordem = '') {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$where = '';
		
		if (!empty($coluna) && (!empty($buscar)) ) {
			$where = sprintf(' WHERE %s = "%s" ',$coluna,$buscar);
			if ($coluna !== 'codigoClienteGrupoGrupo') {
				$where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ',$coluna,strtoupper($buscar));
			}
		}
		
		$paginacao = " LIMIT " . QTDE_REGISTROS;
		$qtd_registros = QTDE_REGISTROS;
		if ($quantidade > 0) {
            $qtd_registros = $quantidade;
        }
		if ( $pagina_atual > 0) {
			$paginacao = ' LIMIT '.$qtd_registros;
			if ($pagina_atual > 0 && $linha_inicial > 0) {
				$paginacao = " LIMIT $qtd_registros OFFSET ".($linha_inicial);
			}
		}
		
		if ($ordem == '') {
            $ordem = 'nome ASC';
		}
		
		$sql = "SELECT *, IF (status = 1, 'Ativo', 'Inativo') as statusDescricao FROM ".$this->nom_tabela.$where." ORDER BY ".$ordem.$paginacao;
		
		// echo $sql;exit;
		
		$dados = $crud->getSQLGeneric($sql);
		return $dados;
		
		//
	}

	public function listarTodosTotal($coluna = '',$buscar = '') {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$where = '';
		
		if (!empty($coluna) && (!empty($buscar) || $buscar >= 0) ) {
			$where = sprintf(' WHERE %s = "%s" ',$coluna,$buscar);
			if ($coluna !== 'codigoClienteGrupoGrupo') {
				$where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ',$coluna,strtoupper($buscar));
			}
		}		
		
		$sql = "SELECT count(*) as total_registros FROM ".$this->nom_tabela.$where;		
		
		$dados = $crud->getSQLGeneric($sql,null,FALSE);		
		
		return $dados->total_registros;
		
		//
	}	
	
	public function listarClienteGrupo($handle) {
		$pdo = Conexao::getInstance();
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$sql = "SELECT *, IF (status = 1, 'Ativo', 'Inativo') as statusDescricao FROM ".$this->nom_tabela." WHERE codigoClienteGrupo = ?";
    
		$arrayParam = array($handle); 
		
		$dados = $crud->getSQLGeneric($sql,$arrayParam, TRUE);
		
		return $dados;
		
		//
	}	
	
	public function editarClienteGrupo($post) {
		$pdo = Conexao::getInstance();
		
		$arrayCliente = array();
		foreach ($post as $key => $value) {
			if ($key != 'handle' && $key != 'codigoClienteGrupo') {
				if (!is_null($value) && $key == 'valor') {
					$_valor = str_replace(".","", $value);
					$_valor = str_replace(",", ".", $_valor);        
					$arrayCliente[$key] = (float) str_replace(",", ".", $_valor);
				} else {
					$arrayCliente[$key] =  $value;
				}
			}
		}
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		
		$arrayCond = array('codigoClienteGrupo=' => $post['handle']);  
		$retorno   = $crud->update($arrayCliente, $arrayCond);  		
		
		return $retorno;
	}
	
	public function cadastrarClienteGrupo($post) {
		$pdo = Conexao::getInstance();
		
		$arrayCliente = array();
		foreach ($post as $key => $value) {
			if ($key != 'handle' && $key != 'codigoClienteGrupo') {
				if (!is_null($value) && $key == 'valor') {
					$_valor = str_replace(".","", $value);
					$_valor = str_replace(",", ".", $_valor);        
					$arrayCliente[$key] = (float) str_replace(",", ".", $_valor);
				} else {
					$arrayCliente[$key] =  $value;
				}
			}
		}		
		
		$crud = bd::getInstance($pdo,$this->nom_tabela);

		$retorno   = $crud->insert($arrayCliente);  		
		
		return $retorno;
	}

	public function excluir($handle) {
		$pdo = Conexao::getInstance();
		$crud = bd::getInstance($pdo,$this->nom_tabela);
		$crud->delete(array('codigoClienteGrupo' => $handle));
	}	
}
