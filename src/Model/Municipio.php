<?php
require_once('conexao.php');
require_once('bd.php');
class Municipio
{
	public $codigo_ibge = 0;
	public $nome = '';
	public $capital = 0;
	public $codigo_uf = 0;
	public $latitude = 0;
	public $longitude = 0;

	public $nom_tabela = 'municipios';


	public function __construct()
	{
		$this->codigo_ibge = 0;
		$this->nome = '';
		$this->capital = 0;
		$this->codigo_uf = 0;
		$this->latitude = 0;
		$this->longitude = 0;
	}

	public function listarTodos($pagina_atual = 0, $linha_inicial = 0, $coluna = '', $buscar = '', $quantidade = '', $ordem = '')
	{
		$pdo = Conexao::getInstance();

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$where = '';

		if (!empty($coluna) && (!empty($buscar))) {
			$where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
			if ($coluna !== 'codigo_ibge') {
				$where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
			}
		}

		$paginacao = " LIMIT " . QTDE_REGISTROS;
		$qtd_registros = QTDE_REGISTROS;
		if ($quantidade > 0) {
			$qtd_registros = $quantidade;
		}
		if ($pagina_atual > 0) {
			$paginacao = ' LIMIT ' . $qtd_registros;
			if ($pagina_atual > 0 && $linha_inicial > 0) {
				$paginacao = " LIMIT $qtd_registros OFFSET " . ($linha_inicial);
			}
		}

		if ($ordem == '') {
			$ordem = 'nome ASC';
		}

		$sql = "SELECT * FROM " . $this->nom_tabela . $where . " ORDER BY " . $ordem . $paginacao;

		// echo $sql;exit;

		$dados = $crud->getSQLGeneric($sql);
		return $dados;

		//
	}

	public function listarTodosTotal($coluna = '', $buscar = '')
	{
		$pdo = Conexao::getInstance();

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$where = '';

		if (!empty($coluna) && (!empty($buscar) || $buscar >= 0)) {
			$where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
			if ($coluna !== 'codigo_ibge') {
				$where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
			}
		}

		$sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela . $where;

		$dados = $crud->getSQLGeneric($sql, null, FALSE);

		return $dados->total_registros;

		//
	}

	public function listarMunicipio($handle)
	{
		$pdo = Conexao::getInstance();

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$sql = "SELECT * FROM " . $this->nom_tabela . " WHERE codigo_ibge = ?";

		$arrayParam = array($handle);

		$dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

		return $dados;

		//
	}

	public function listarMunicipiosPorEstado($siglaEstado)
	{
		$pdo = Conexao::getInstance();

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$sql = "
      SELECT
        municipios.*
      FROM
        municipios
      INNER JOIN
        estados ON municipios.codigo_uf = estados.codigo_uf
      WHERE
	      estados.uf = '" . $siglaEstado . "' 
    ";

		$dados = $crud->getSQLGeneric($sql, [], TRUE);

		return $dados;

		//
	}

	public function editarMunicipio($post)
	{
		$pdo = Conexao::getInstance();

		$arrayCliente = array();
		foreach ($post as $key => $value) {
			if ($key != 'handle' && $key != 'codigo_ibge') {
				$arrayCliente[$key] =  $value;
			}
		}

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$arrayCond = array('codigo_ibge=' => $post['handle']);
		$retorno   = $crud->update($arrayCliente, $arrayCond);

		return $retorno;
	}

	public function cadastrarMunicipio($post)
	{
		$pdo = Conexao::getInstance();

		$arrayCliente = array();
		foreach ($post as $key => $value) {
			if ($key != 'handle' && $key != 'codigo_ibge') {
				$arrayCliente[$key] =  $value;
			}
		}

		$crud = bd::getInstance($pdo, $this->nom_tabela);

		$retorno   = $crud->insert($arrayCliente);

		return $retorno;
	}

	public function excluir($handle)
	{
		$pdo = Conexao::getInstance();
		$crud = bd::getInstance($pdo, $this->nom_tabela);
		$crud->delete(array('codigo_ibge' => $handle));
	}
}
