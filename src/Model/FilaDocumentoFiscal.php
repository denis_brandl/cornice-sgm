<?php
require_once('conexao.php');
require_once('bd.php');
require_once('Orcamento.php');
class FilaDocumentoFiscal
{

  public $id_fila;
  public $id_documento;
  public $id_pedido;
  public $payload;
  public $retorno;
  public $situacao;
  public $id_empresa;
  public $id_usuario_criacao;
  public $id_usuario_edicao;
  public $data_criacao;
  public $data_atualizacao;
  public $numero_nota;
  public $devolucao;
  public $nom_tabela = 'fila_documento_fiscal';


  public function __construct()
  {
    $this->id_fila = 0;
    $this->id_documento = '';
    $this->id_pedido = 0;
    $this->payload = '';
    $this->retorno = '';
    $this->situacao = '';
    $this->id_empresa = 0;
    $this->id_usuario_criacao = 0;
    $this->id_usuario_edicao = 0;
    $this->data_criacao = date('Y-m-d h:i:s', time());
    $this->data_atualizacao = date('Y-m-d h:i:s', time());
    $this->numero_nota = 0;
    $this->devolucao = 1;
  }

  public function consultaDocumentos($arrFiltro = [])
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $filtros = '';
    if (isset($arrFiltro['tipo_documento'])) {
      $filtros = ' AND id_tipo_documento = ' . $arrFiltro['tipo_documento'];
    }

    $sql = "SELECT * FROM " . $this->nom_tabela . ' WHERE (situacao = "PROCESSANDO" OR situacao = "PENDENTE") ' . $filtros;

    $dados = $crud->getSQLGeneric($sql);

    return $dados;

    //
  }

  public function listarPorDocumento($documento)
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql = "SELECT * FROM " . $this->nom_tabela . " WHERE id_documento =  '$documento'";

    $dados = $crud->getSQLGeneric($sql, null, false);

    return $dados;

    //
  }  

  public function listarTodos($pagina_atual = 0, $linha_inicial = 0, $coluna = '', $buscar = '', $quantidade = '', $ordem = '')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';

    if (!empty($coluna) && (!empty($buscar))) {
      $where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
      if ($coluna !== 'id_fila') {
        $where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
      }
    }

    $paginacao = " LIMIT " . QTDE_REGISTROS;
    $qtd_registros = QTDE_REGISTROS;
    if ($quantidade > 0) {
      $qtd_registros = $quantidade;
    }
    if ($pagina_atual > 0) {
      $paginacao = ' LIMIT ' . $qtd_registros;
      if ($pagina_atual > 0 && $linha_inicial > 0) {
        $paginacao = " LIMIT $qtd_registros OFFSET " . ($linha_inicial);
      }
    }

    if ($ordem == '') {
      $ordem = 'id_fila DESC';
    }

    $sql = "
      SELECT 
        *,
        Empresas.NomeFantasia nome_fantasia_empresa,
        Empresas.RazaoSocial razao_social_empresa
      FROM " 
        . $this->nom_tabela 
        . " LEFT JOIN Orcamento ON Orcamento.Cd_Orcamento = fila_documento_fiscal.id_pedido "
        . " INNER JOIN Clientes ON Orcamento.Cd_Cliente = Clientes.CodigoCliente "
        . " INNER JOIN Empresas ON Orcamento.id_empresa = Empresas.CodigoEmpresa "
      . $where
      . " ORDER BY "
        . $ordem
      . $paginacao;

    // echo $sql;

    $dados = $crud->getSQLGeneric($sql);
    return $dados;

    //
  }

  public function listarTodosTotal($coluna = '', $buscar = '')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';

    if (!empty($coluna) && (!empty($buscar) || $buscar >= 0)) {
      $where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
      if ($coluna !== 'id_fila') {
        $where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
      }
    }

    $sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela . $where;

    $dados = $crud->getSQLGeneric($sql, null, FALSE);

    return $dados->total_registros;

    //
  }

  public function listarFilaDocumentoFiscal($filtro = [])
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql = sprintf("SELECT * FROM %s WHERE %s = ? ORDER BY id_fila DESC LIMIT 1", $this->nom_tabela, key($filtro));

    $arrayParam = array($filtro[key($filtro)]);

    $dados = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

    return $dados;

    //
  }

  public function editarFilaDocumentoFiscal($post)
  {
    $pdo = Conexao::getInstance();

    $arrayFilaDocumentoFiscal = array();
    foreach ($post as $key => $value) {
      if ($key != 'handle' && $key != 'id_fila') {
        $arrayFilaDocumentoFiscal[$key] =  $value;
      }
    }

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $arrayCond = array('id_fila=' => $post['handle']);
    $retorno   = $crud->update($arrayFilaDocumentoFiscal, $arrayCond);

    return $retorno;
  }

  public function cadastrarFilaDocumentoFiscal($post)
  {
    // print_r($post);exit;
    $pdo = Conexao::getInstance();

    $arrayFilaDocumentoFiscal = array();

    foreach ($post as $key => $value) {
      $arrayFilaDocumentoFiscal[$key] = $value;
    }

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $retorno   = $crud->insert($arrayFilaDocumentoFiscal);

    return $retorno;
  }

  public function excluir($handle)
  {
    $pdo = Conexao::getInstance();
    $crud = bd::getInstance($pdo, $this->nom_tabela);
    $crud->delete(array('id_fila' => $handle));
  }

  public function relatorio($arrFiltro = [])
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $objOrcamento = new Orcamento();

    $condicional_restricao_empresa = '';
    // if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
    //   $condicional_restricao_empresa = sprintf(
    //     'AND Orcamento.id_empresa IN (%s)',
    //     implode(',', $this->arrPermissoesUsuarioEmpresa)
    //   );
    // }     

    $where = '';
    $condicional = '';
    if (sizeof($arrFiltro)) {
      if (isset($arrFiltro['periodo_inicial']) && isset($arrFiltro['periodo_final'])) {
        $tipo_data = 'fila_documento_fiscal.data_criacao';
        if (isset($arrFiltro['tipo_data']) && $arrFiltro['tipo_data'] != 'data_criacao') {
          $tipo_data = 'Orcamento.' . $arrFiltro['tipo_data'];
        }

        $where .= sprintf(" %s %s BETWEEN '%s' AND '%s'", $condicional, $tipo_data, $arrFiltro['periodo_inicial'], $arrFiltro['periodo_final']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['filtro_tipo_documento']) && !empty($arrFiltro['filtro_tipo_documento'])) {
        $where .= sprintf(" %s fila_documento_fiscal.id_tipo_documento IN (%s)", $condicional, $arrFiltro['filtro_tipo_documento']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['somente_pedidos_validos']) && $arrFiltro['somente_pedidos_validos'] == "1") {
        $where .= sprintf(" %s Orcamento.Id_Situacao > 1 AND Orcamento.Id_Situacao < 6 ", $condicional);
        $condicional = ' AND ';
      }      

      if (isset($arrFiltro['filtro_pago']) && $arrFiltro['filtro_pago'] != "-1") {
        $where .= sprintf(" %s Orcamento.Pago = %s", $condicional, $arrFiltro['filtro_pago']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['criado_por']) && !empty($arrFiltro['criado_por'])) {
        $where .= sprintf(" %s Orcamento.criado_por = %s", $condicional, $arrFiltro['criado_por']);
        $condicional = ' AND ';
      }

      if (isset($arrFiltro['cliente']) && !empty($arrFiltro['cliente'])) {
        $where .= sprintf(" %s Orcamento.Cd_Cliente = %s", $condicional, $arrFiltro['cliente']);
        $condicional = ' AND ';
      }      

      if (isset($arrFiltro['pedido']) && !empty($arrFiltro['pedido'])) {
        $where .= sprintf(" %s Orcamento.Cd_Orcamento = %s", $condicional, $arrFiltro['pedido']);
        $condicional = ' AND ';
      }            
      
      if (isset($arrFiltro['empresa']) && !empty($arrFiltro['empresa'])) {
        $where .= sprintf(" %s Orcamento.id_empresa = %s", $condicional, $arrFiltro['empresa']);
        $condicional = ' AND ';
      } elseif (count($objOrcamento->arrPermissoesUsuarioEmpresa) > 0) {
        $where .= sprintf(
          '%s Orcamento.id_empresa IN (%s)',
          $condicional,
          implode(',', $objOrcamento->arrPermissoesUsuarioEmpresa)
        );
        $condicional = ' AND ';
      }
    }

    if (!empty($where)) {
      $where = ' WHERE fila_documento_fiscal.situacao IN ("Autorizado", "Cancelado") AND ' . $where;
    }

    // if (count($this->arrPermissoesUsuarioEmpresa) > 0) {
    //   $where .= sprintf(
    //     '%s id_empresa IN (%s)',
    //     ($where != '' ? ' AND ' : ' WHERE '),
    //     implode(',', $this->arrPermissoesUsuarioEmpresa)
    //   );
    // }    

    $sql = sprintf('
              SELECT
                fila_documento_fiscal.id_documento,
                fila_documento_fiscal.numero_nota AS NFCRetornoId,
                fila_documento_fiscal.id_pedido AS PedidoId,
                "VENDA AO CONSUMIDOR" AS NaturezaOperacao,
                tipos_documento.sigla as TipoDocumentoSigla,
                tipos_documento.descricao as TipoDocumento,
                1 AS FinalidadeEmissao,
                fila_documento_fiscal.retorno AS MensagemSEFAZ,
                fila_documento_fiscal.id_documento AS ChaveNfe,
                "" AS Numero,
                "" AS Serie,
                "" AS NumeroSerie,
                Orcamento.vl_liquido AS Valor,
                usuarios.login AS LoginEmissor,
                fila_documento_fiscal.data_atualizacao AS DataEmissao,
                fila_documento_fiscal.data_atualizacao AS DataHoraEmissao,
                Clientes.RazaoSocial AS ClienteNome,
                Clientes.EMail AS Email,
                "" AS QrcodeUrl,
                "" AS UrlConsultaNf,
                fila_documento_fiscal.link_pdf AS CaminhoDanfe,
                fila_documento_fiscal.link_xml AS CaminhoXmlNotaFiscal,
                "" AS CaminhoXmlCancelamento,
                Empresas.CGC AS CnpjEmitente,
                fila_documento_fiscal.id_pedido AS ref,
                fila_documento_fiscal.situacao AS Status,
                100 AS StatusSefaz,
                "" AS Email1,
                "PRODUÇÃO" AS Ambiente,
                "" AS Consultas,
                "" AS DataCancelamento,
                "" AS Justificativa
              FROM
                fila_documento_fiscal
                INNER JOIN tipos_documento ON
                  (fila_documento_fiscal.id_tipo_documento = tipos_documento.id_tipo_documento)
                INNER JOIN Orcamento ON
                  (Orcamento.Cd_Orcamento = fila_documento_fiscal.id_pedido)
                INNER JOIN usuarios ON
                  (fila_documento_fiscal.id_usuario_criacao = usuarios.CodigoUsuario)
                INNER JOIN Clientes ON
                  (Orcamento.Cd_Cliente = Clientes.CodigoCliente)
                INNER JOIN Empresas ON
                  (Orcamento.id_empresa = Empresas.CodigoEmpresa)
                %s
              ORDER BY
                fila_documento_fiscal.data_criacao ASC
            ', $where
    );

    // echo "<pre>$sql</pre>";


    $dados = $crud->getSQLGeneric($sql, [], TRUE);

    return $dados;

    //
  }  
}
