<?php
require_once('conexao.php');
require_once('bd.php');
class OperacaoFiscal
{
  public $id_operacao_fiscal = 0;
  public $descricao = '';
  public $descricao_cfop = '';

  public $nom_tabela = 'operacao_fiscal';


  public function __construct()
  {
    $this->id_operacao_fiscal = 0;
    $this->descricao_cfop = '';
    $this->descricao = '';
  }

  public function listarTodos($pagina_atual = 0, $linha_inicial = 0, $coluna = '', $buscar = '', $quantidade = '', $ordem = '')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';

    if (!empty($coluna) && (!empty($buscar))) {
      $where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
      if ($coluna !== 'id_operacao_fiscal') {
        $where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
      }
    }

    $paginacao = " LIMIT " . QTDE_REGISTROS;
    $qtd_registros = QTDE_REGISTROS;
    if ($quantidade > 0) {
      $qtd_registros = $quantidade;
    }
    if ($pagina_atual > 0) {
      $paginacao = ' LIMIT ' . $qtd_registros;
      if ($pagina_atual > 0 && $linha_inicial > 0) {
        $paginacao = " LIMIT $qtd_registros OFFSET " . ($linha_inicial);
      }
    }

    if ($ordem == '') {
      $ordem = 'descricao_cfop ASC';
    }

    $sql = "SELECT * FROM " . $this->nom_tabela . $where . " ORDER BY " . $ordem . $paginacao;

    // echo $sql;exit;

    $dados = $crud->getSQLGeneric($sql);
    return $dados;

    //
  }

  public function listarTodosTotal($coluna = '', $buscar = '')
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $where = '';

    if (!empty($coluna) && (!empty($buscar) || $buscar >= 0)) {
      $where = sprintf(' WHERE %s = "%s" ', $coluna, $buscar);
      if ($coluna !== 'id_operacao_fiscal') {
        $where = sprintf(' WHERE UPPER(%s) LIKE "%s%%" ', $coluna, strtoupper($buscar));
      }
    }

    $sql = "SELECT count(*) as total_registros FROM " . $this->nom_tabela . $where;

    $dados = $crud->getSQLGeneric($sql, null, FALSE);

    return $dados->total_registros;

    //
  }

  public function listarOperacaoFiscal($handle)
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql = "SELECT * FROM " . $this->nom_tabela . " WHERE id_operacao_fiscal = ?";

    $arrayParam = array($handle);

    $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    return $dados;

    //
  }

  public function listarOperacaoFiscalsPorEstado($siglaEstado)
  {
    $pdo = Conexao::getInstance();

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $sql = "
      SELECT
        municipios.*
      FROM
        municipios
      INNER JOIN
        estados ON municipios.codigo_uf = estados.codigo_uf
      WHERE
	      estados.uf = '" . $siglaEstado . "' 
    ";

    $dados = $crud->getSQLGeneric($sql, [], TRUE);

    return $dados;

    //
  }

  public function editarOperacaoFiscal($post)
  {
    $pdo = Conexao::getInstance();

    $arrayCliente = array();
    foreach ($post as $key => $value) {
      if ($key != 'handle' && $key != 'id_operacao_fiscal') {
        $arrayCliente[$key] =  $value;
      }
    }

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $arrayCond = array('id_operacao_fiscal=' => $post['handle']);
    $retorno   = $crud->update($arrayCliente, $arrayCond);

    return $retorno;
  }

  public function cadastrarOperacaoFiscal($post)
  {
    $pdo = Conexao::getInstance();

    $arrayCliente = array();
    foreach ($post as $key => $value) {
      if ($key != 'handle' && $key != 'id_operacao_fiscal') {
        $arrayCliente[$key] =  $value;
      }
    }

    $crud = bd::getInstance($pdo, $this->nom_tabela);

    $retorno   = $crud->insert($arrayCliente);

    return $retorno;
  }

  public function excluir($handle)
  {
    $pdo = Conexao::getInstance();
    $crud = bd::getInstance($pdo, $this->nom_tabela);
    $crud->delete(array('id_operacao_fiscal' => $handle));
  }
}
