#!/bin/bash

clear

read -p "Nome do ambiente: " ambiente

if [ "$ambiente" = "" ]
then
    echo "Ambiente invalido"
    exit
fi

read -p "Tem parceria Ruberti (s/N): " parceria_ruberti
read -p "Base do banco de dados: " db_database
read -p "Usuário do banco de dados: " db_login
read -s -p "Senha do banco de dados: " db_senha

printf "\n\nVamos iniciar a configuração do ambiente $ambiente \n\n"

echo -e "[client]\nuser=$db_login\npassword=$db_senha" >> ~/.my.cnf

cd $ambiente

rm -rf default.php .gitignore .git/ .htaccess .DS_Store

printf "Clonando o projeto do repositório de código \n\n"

git clone -q  git@bitbucket.org:denis_brandl/cornice-sgm.git .

printf "Criando a configuração de banco de dados e ambiente \n\n"

cp config-sample.php config.php
sed -i "s/https:\/\/localhost:83\/cornize\//https:\/\/$ambiente\.maestriasistema\.com\.br\//g" config.php
sed -i "s/cornize//g" config.php

cd src/Model/

cp conexao-sample.php conexao.php
 
sed -i "s/'HOST', ''/'HOST', 'localhost'/g" conexao.php
sed -i "s/'DBNAME', ''/'DBNAME', '$db_database'/g" conexao.php
sed -i "s/'USER', ''/'USER', '$db_login'/g" conexao.php
sed -i "s/'PASSWORD', ''/'PASSWORD', '$db_senha'/g" conexao.php
  
cd ../..

printf "Importando o banco de dados \n\n"

mysql -u $db_login -D $db_login < banco/banco.sql
composer migrations

if [ "$parceria_ruberti" = "s" ]
then
	printf "Importando produtos Ruberti \n\n"
	mysql -u $db_login -D $db_database < banco/produtos_ruberti.sql

	mysql -u $db_login -D $db_database -Bse "update Configuracoes set valor = 'true' where nome = 'parceria_ruberti';"
fi


echo "Ambiente liberado para uso, acesse http://$ambiente.maestriasistema.com.br/"
