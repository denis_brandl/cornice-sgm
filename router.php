<?php

	$request_uri = isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] ? $_SERVER['REQUEST_URI'] : '';
	if ( $request_uri === '/') {
		include 'index.php';		
		return true;
	}
	
	$a = preg_match('/pdfTemporario/', $request_uri);
	if ($a) {
		return false;
	}	
	
	if (preg_match('/(?:png|jpg|jpeg|gif|css|min\.js|min\.css|js|ico|json|woff.*)$/', $request_uri)) {
		return false;
	}
	
	$a = preg_match('/^login\/?$/', $request_uri);
	if ($a) {
		die('achou algo');
	}
	
	$a = preg_match('/^fornecedor\/?$/', $request_uri, $matches);
	if ($a) {
		die('O que eu faço?');
	}
		
	$a = preg_match('/^\/([a-zA-Z]+)?\/([a-zA-Z]+)?\/?([0-9A-Za-z_-]+)?\/?(.*)$/m', $request_uri, $matches);
	if ($a) {
		$_GET['class'] = $matches[1];
		$_GET['acao'] = $matches[2];
		$_GET['handle'] = isset($matches[3]) ? $matches[3] : '';
		$_GET['parametros'] = isset($matches[4]) ? $matches[4]: '';
		include 'index.php';
		return true;
	}

  if ($request_uri == '/server') {
    include 'server.php';
    return true;
  }
	
	die('Não deu: ' . 	$request_uri);
?>	