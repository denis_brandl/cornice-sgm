<?php

  error_reporting(E_ALL);
  ini_set('display_errors', '1');
  
	/**
	 * Define o tempo de sessão para 5 horas
	 */
	ini_set('session.gc_maxlifetime', 3600);
	session_set_cookie_params(['httponly' => true]);
	session_set_cookie_params(3600);
	session_start();

  date_default_timezone_set('America/Sao_Paulo');

	require_once 'config.php';	
	require 'vendor/autoload.php';
	require_once './src/Model/GruposPermissoes.php';	
	if (!isset($_GET['class'])) 
	{
		require_once './src/Controller/UsuarioController.php';	
		$obj = new UsuarioController();
		$obj->login();
		die();
	}

	if ( ($_GET['class'] != 'Cliente' || $_GET['acao'] != 'logar') && $_GET['class'] != 'Plugnotas' ) {
		if ( ($_GET['acao'] !== 'logar') && (!isset($_SESSION['token']) || empty($_SESSION['token'])) ) {		
			require_once './src/Controller/UsuarioController.php';	
			$obj = new UsuarioController();
			$obj->login();
			die();
		}	
	}

	$classe = ucwords($_GET['class']);

	$objGruposPermissoes = new GruposPermissoes();
	$handle_grupo = isset($_SESSION['handle_grupo']) ? $_SESSION['handle_grupo'] : 0;
	$arrConsultaPermissao = $objGruposPermissoes->listarPorGrupo($handle_grupo);

	$classe_entidade = $classe == 'Orcamento' ? 'Pedidos' : $classe;
	$indicePermissao = array_search($classe_entidade, array_column($arrConsultaPermissao, 'entidade'));

	if (!$indicePermissao) {
		// die('Não foi identificado a permissão.');
	}
	
	$metodo = 'listar';
	if (isset($_GET['acao'])) {
		$metodo = $acao = $_GET['acao'];		
	}

	$handle = '';
	if (isset($_GET['handle'])) {
		$handle = $_GET['handle'];
	} elseif (isset($_POST['handle'])) {
		$handle = $_POST['handle'];	
	}


	$arrEntidadesIgnorar = ['Dashboard', 'Usuario', 'Producao', 'ComplexidadeProduto'];
	if (!in_array($classe, $arrEntidadesIgnorar) && $arrConsultaPermissao[$indicePermissao]->visualizar != 1) {

		if ($classe == 'Cliente' && $metodo != 'carrega') {
			Header('Location:' . URL . 'dashboard/listar/');
			exit;
		}
		
		$arrPermissoesAcesso = (array) $arrConsultaPermissao[$indicePermissao];
		if (isset($arrPermissoesAcesso[$metodo == 'listar' ? 'visualizar' : $metodo]) && ($arrPermissoesAcesso[$metodo == 'listar' ? 'visualizar' : $metodo] == 0) && ($classe == 'Cliente' && $metodo != 'carrega') && ($classe == 'Whatsapp' && $metodo != 'enviarMensagem')) {
			Header('Location:' . URL . 'dashboard/listar/');
			exit;
		}
	}
	 
	$classe .= 'Controller';
	
	require_once './src/Controller/'.$classe.'.php';
	
	$obj = new $classe();
	if (!empty($handle)) {
		$obj->$metodo($handle);
	} else {
		$obj->$metodo();
	}
