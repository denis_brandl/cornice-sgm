<?php
define('URL','https://localhost:83/cornize/');

define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT'].'/cornize');

define('TITULO','Maestria - Sistema de Gestão');

define('QTDE_REGISTROS', 10);   
define('RANGE_PAGINAS', 10); 

define('TOKEN_PLUGNOTAS', '');
define('TOKEN_SANDBOX_PLUGNOTAS', '2da392a6-79d2-4304-a8b7-959572c7e44d');
define('URL_PLUGNOTAS', '');
define('URL_SANDBOX_PLUGNOTAS', 'https://api.sandbox.plugnotas.com.br/');