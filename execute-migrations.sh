CURRENT_DB=$(cat src/Model/conexao.php | grep " define('DBNAME', '" | sed "s/ define('DBNAME', '//" | sed "s/');.*//g")
CURRENT_USER=$(cat src/Model/conexao.php | grep " define('USER', '" | sed "s/ define('USER', '//" | sed "s/');.*//g")
CURRENT_PASS=$(cat src/Model/conexao.php | grep " define('PASSWORD', '" | sed "s/ define('PASSWORD', '//" | sed "s/');.*//g")

mysqldump -u $CURRENT_USER --password="$CURRENT_PASS" $CURRENT_DB > ~/backups-clientes/$CURRENT_DB.sql

PHINX_DBPASS=$CURRENT_PASS PHINX_DBUSER=$CURRENT_USER PHINX_DBNAME=$CURRENT_DB php vendor/bin/phinx migrate
