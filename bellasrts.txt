### MYSQL - SISTEMA JR
use bellasarts_jr;

truncate maestria_bellasarts.Clientes;
ALTER TABLE maestria_bellasarts.Clientes AUTO_INCREMENT = 1;

insert
	into
	maestria_bellasarts.Clientes
(
	RazaoSocial,
	Nomefantasia,
	idLogradouro,
	Endereco,
	numeroEndereco,
	Complemento,
	Bairro,
	Cidade,
	Estado,
	CEP,
	Telefone1,
	Telefone2,
	Observacoes,
	CGC,
	Contato,
	TipoCliente,
	InscricaoEstadual
)
select
	nome,
	fantasia,
	33,
	endereco,
	numender,
	Complemento,
	bairro,
	CodCidade,
	uf,
	cep,
	IF(celular <> '', celular , fone),
	IF(celular <> '', fone , ''),
	'',
	cnpj_cpf,
	'',
	IF(TipoCadastro = 'PESSOA FÍSICA', 'F', 'J'),
	IF(TipoCadastro <> 'PESSOA FÍSICA', insc_rg , '')
from
	bellasarts_jr.clientes
where
	cli_forn = 'CLIENTE'
;

### POSTGRES - SISTEMA FAST

select
	nome as "RazaoSocial",
	nome_fantasia "NomeFantasia",
	33 "idLogradouro",
	rua "Endereco",
	clientes.numero "numeroEndereco",
	complemento "Complemento",
	bairro "Bairro",
	clientes.cod_cidade "Cidade",
	e.uf as "Estado",
	cep "CEP",
	(select MAX(CONCAT(tc.ddd,' ',tc.numero))  from telefone_cliente tc where tc.cod_cliente = clientes.cod_cliente and tc.cod_tipo_telefone = 2) as "Telefone1",
	(select MAX(CONCAT(tc2.ddd,' ',tc2.numero)) from telefone_cliente tc2 where tc2.cod_cliente = clientes.cod_cliente and tc2.cod_tipo_telefone = 6) as "Telefone2",
	(select string_agg('Demais telefones importados: '||tc3.ddd||tc3.numero,' | ')  from telefone_cliente tc3 where tc3.cod_cliente = clientes.cod_cliente and tc3.cod_tipo_telefone not in (2,6)) "Observacoes", 
	cnpj "CGC",
	'' "Contato",
	case when inscricao_estadual <> '' THEN 'J' ELSE 'F' end "TipoCliente",
	inscricao_estadual "InscricaoEstadual"
from
	bellasarts_fast.public.clientes clientes
left join cidade c on clientes.cod_cidade = c.cod_cidade 
left join estados e on c.cod_estado  = e.cod_estado 

;


select 
	p.cod_produto as "NovoCodigo",
	p.referencia_fornecedor  as "CodigoProdutoFabricante",
	p.nome_nota as "DescricaoProduto",
	p.cod_unidade_venda as "UnidadeProduto",
	ien.valor_unitario as PrecoCusto,
	pv.preco_venda as PrecoVendaMaoObra,
	g.estoque_atual as "Quantidade",
	(g.estoque_atual - 10 * 100) as "QuantidadeMinima",
	p.ncm as codigo_ncm
from produto p 
left join precos_venda pv on (p.cod_produto = pv.cod_produto)
left join itens_entrada_nota ien on (p.cod_produto = ien.cod_produto)
left join grade g on (p.cod_produto = g.cod_produto)