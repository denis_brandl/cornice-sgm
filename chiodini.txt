truncate cornize.Clientes;

insert
	into
	cornize.Clientes 
(RazaoSocial,
	Endereco,
	Bairro,
	Cidade,
	Estado,
	CEP,
	Telefone1,
	Telefone2,
	Observacoes,
	CGC,
	Contato,
	TipoCliente)
select
	nome_cliente,
	endereco_cliente,
	bairro_cliente,
	cidade_cliente,
	estado_cliente,
	cep_cliente,
	telefone1_cliente,
	telefone2_cliente,
	observacao_cliente,
	cnpj_cliente,
	pessoaContato_cliente,
	'F'
from
	chiodini.clientes;



desc cornize.Produtos;

truncate cornize.Produtos ;

ALTER TABLE cornize.Produtos AUTO_INCREMENT = 1;

insert
	into cornize.Produtos 
	(
	NovoCodigo,
	CodigoProdutoFabricante,
	DescricaoProduto,
	UnidadeProduto,
	PrecoCusto,
	PrecoVendaMaoObra,
	Quantidade,
	QuantidadeMinima
)
select 
codigo_madeira,codigo_madeira, descricao_madeira, '3', precoUnitario_madeira, precoUnitario_madeira, '1000', '5'
from
chiodini.madeiras
;