ALTER TABLE
  Configuracoes
MODIFY
  COLUMN valor TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL NULL;

INSERT INTO
  Configuracoes (nome, valor, descricao)
VALUES
  ('versao_banco', '20230927', 'Versão banco') ON DUPLICATE KEY
UPDATE
  nome = 'versao_banco',
  valor = '20230927';