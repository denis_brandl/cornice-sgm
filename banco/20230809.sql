CREATE TABLE IF NOT EXISTS  `bills` (
  `BillsId` int(5) NOT NULL AUTO_INCREMENT,
  `UserId` int(5) NOT NULL,
  `Title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Date` datetime NOT NULL,
  `CategoryId` int(5) NOT NULL,
  `AccountId` int(5) NOT NULL,
  `Amount` decimal(12,2) NOT NULL,
  `Description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `IDCONTA` int(11) NOT NULL,
  `cdtvencimento` date NOT NULL,
  `cdtpagamento` datetime DEFAULT NULL,
  `doc` varchar(50) NOT NULL,
  `ndoc` varchar(50) NOT NULL,
  `dtinclusao` datetime NOT NULL,
  `dtalteracao` datetime NOT NULL,
  PRIMARY KEY (`BillsId`),
  KEY `fk_testt` (`AccountId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS  `assets` (
  `AssetsId` int(5) NOT NULL AUTO_INCREMENT,
  `UserId` int(5) NOT NULL,
  `Title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Date` datetime NOT NULL,
  `CategoryId` int(5) NOT NULL,
  `AccountId` int(5) NOT NULL,
  `Amount` decimal(12,2) NOT NULL,
  `Description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `IDCONTA` int(11) NOT NULL,
  `cdtvencimento` date DEFAULT NULL,
  `cdtpagamento` datetime DEFAULT NULL,
  `doc` varchar(50) NOT NULL,
  `ndoc` varchar(50) NOT NULL,
  `idreceber` int(5) NOT NULL,
  `mes` varchar(50) NOT NULL,
  `dtinclusao` datetime NOT NULL,
  `dtalteracao` datetime NOT NULL,
  PRIMARY KEY (`AssetsId`),
  KEY `fk_test` (`AccountId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE IF NOT EXISTS  `balance_amount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) DEFAULT NULL,
  `amount` decimal(12,2) DEFAULT NULL,
  `UserId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `balance_amount_id_IDX` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS  `category` (
  `CategoryId` int(5) NOT NULL AUTO_INCREMENT,
  `UserId` int(5) NOT NULL,
  `CategoryName` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Level` int(2) NOT NULL,
  `IDCONTA` int(11) NOT NULL,
  `TIPO` int(11) NOT NULL,
  `idgeral` int(5) NOT NULL,
  `complemento` varchar(250) NOT NULL,
  PRIMARY KEY (`CategoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;


CREATE TABLE IF NOT EXISTS  `contas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `idtipo` int(11) NOT NULL,
  `idgeral` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE IF NOT EXISTS  `account` (
  `AccountId` int(5) NOT NULL AUTO_INCREMENT,
  `UserId` int(5) NOT NULL,
  `AccountName` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tipo` int(11) NOT NULL,
  PRIMARY KEY (`AccountId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

LOCK TABLES `account` WRITE;
INSERT INTO `account` VALUES (0,1,'Caixa',0),(0,1,'Banco',1),(0,1,'Cartão',2),(0,1,'Aplicação',3),(0,1,'Em aberto',4);
UNLOCK TABLES;

ALTER TABLE UnidadesMedida add column status tinyint(1) default 1;

INSERT INTO Configuracoes VALUES (0, 'habilita_financeiro', 'false', 'Habilita o módulo financeiro');

INSERT INTO `contas` (`id`, `nome`, `idtipo`, `idgeral`) VALUES
(1, 'Entrada Geral', 0, 0),
(2, 'Saídas Geral', 1, 0);

INSERT INTO `category` (`CategoryId`, `UserId`, `CategoryName`, `Level`, `IDCONTA`, `TIPO`, `idgeral`, `complemento`) VALUES
(1, 1, 'Entradas diversas', 0, 1, 0, 1, ''),
(2, 1, 'Saídas Diversas', 0, 2, 0, 1, '');

INSERT
	INTO
	balance_amount (UserId,
	tipo,
	amount)
SELECT
	CodigoEmpresa AS UserId,
	a.tipo AS tipo,
	0 AS amount
FROM
	Empresas e,
	account a
WHERE
	tipo <> 4;

INSERT INTO Configuracoes (nome, valor, descricao) VALUES('versao_banco', '20230809', 'Versão banco') ON DUPLICATE KEY UPDATE nome='versao_banco', valor = '20230809';