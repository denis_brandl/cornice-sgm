UPDATE
  UnidadesMedida
set
  NomeUnidadeMedida = 'Metro Linear',
  DescricaoUnidadeMedida = 'ML'
where
  MetragemLinear = 1;

ALTER TABLE
  Produtos CHANGE COLUMN DescricaoProduto DescricaoProduto VARCHAR(150);

INSERT INTO
  Configuracoes (nome, valor, descricao)
VALUES
  ('versao_banco', '20230817', 'Versão banco') ON DUPLICATE KEY
UPDATE
  nome = 'versao_banco',
  valor = '20230817';