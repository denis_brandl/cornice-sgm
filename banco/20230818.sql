ALTER TABLE
  Orcamento
ADD
  COLUMN tipo_desconto CHAR(1) DEFAULT 'M';

INSERT INTO
  Configuracoes (nome, valor, descricao)
VALUES
  ('versao_banco', '20230818', 'Versão banco') ON DUPLICATE KEY
UPDATE
  nome = 'versao_banco',
  valor = '20230818';