ALTER TABLE `Empresas` ADD COLUMN codigo_ncm int(8) DEFAULT 0;
ALTER TABLE `Empresas` ADD COLUMN codigo_cest int(7) DEFAULT 0;
ALTER TABLE `Empresas` ADD COLUMN codigo_cfop int(4) DEFAULT 0;
ALTER TABLE `Empresas` ADD COLUMN codigo_origem int(2) DEFAULT 0;
ALTER TABLE `Empresas` ADD COLUMN codigo_cst int(2) DEFAULT 0;
ALTER TABLE `Empresas` ADD COLUMN codigo_crt int(2) DEFAULT 0;
ALTER TABLE `Empresas` ADD COLUMN aliquota DECIMAL(5,2) DEFAULT 0;

SET FOREIGN_KEY_CHECKS=0;
TRUNCATE tributacao ;

TRUNCATE grupo_tributario;

ALTER TABLE grupo_tributario DROP column IF EXISTS codigo, DROP column IF EXISTS nome;

ALTER TABLE grupo_tributario ADD COLUMN codigo VARCHAR(2) DEFAULT '', add column nome VARCHAR(100) DEFAULT '';

ALTER TABLE grupo_tributario AUTO_INCREMENT = 1;

INSERT INTO grupo_tributario (nome, codigo, codigo_origem) 
VALUES 
 ('Tributada integralmente', '00','0'),
 ('Tributada e com cobrança do ICMS por substituição tributária', '10', '0'),
 ('Tributação com redução de base de cálculo', '20','0' ),
 ('Tributação Isenta ou não tributada e com cobrança do ICMS por substituição tributária', '30','0' ),
 ('Tributação Isenta, Não tributada ou Suspensão', '40','0' ),
 ('Tributação com Diferimento', '51','0' ),
 ('Tributação ICMS cobrado anteriormente por substituição tributária', '60','0' ),
 ('Tributação ICMS com redução de base de cálculo e cobrança do ICMS por substituição tributária', '70','0' );
 
SET FOREIGN_KEY_CHECKS=1;

ALTER TABLE Produtos ADD COLUMN id_grupo_tributario INT(11);

ALTER TABLE tributacao MODIFY COLUMN csosn int(4) unsigned zerofill NOT NULL;

ALTER TABLE pis ADD COLUMN id_grupo_tributario INT(11);
ALTER TABLE ipi ADD COLUMN id_grupo_tributario INT(11);
ALTER TABLE confins ADD COLUMN id_grupo_tributario INT(11);

INSERT INTO Configuracoes (nome, valor, descricao) VALUES('versao_banco', '20230626', 'Versão banco') ON DUPLICATE KEY UPDATE nome='versao_banco', valor = '20230626';