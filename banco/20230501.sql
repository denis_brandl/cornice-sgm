
ALTER TABLE Empresas ENGINE=INNODB;

ALTER TABLE estados ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `grupo_tributario` (
  `id_grupo_tributario` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) DEFAULT 1,
  `nome` varchar(45) NOT NULL,
  `codigo_origem` int(2) DEFAULT 0,
  PRIMARY KEY (`id_grupo_tributario`),
  KEY `fk_grupo_tributario_empresa_idx` (`id_empresa`),
  CONSTRAINT `fk_grupo_tributario_empresa` FOREIGN KEY (`id_empresa`) REFERENCES `Empresas` (`CodigoEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO grupo_tributario (id_empresa,nome) VALUES
	 (1,'Produto sem substituição tributária'),
	 (1,'Produto Com Substituição Tributária'),
	 (1,'Produto Teste'),
	 (1,'Grupo Tributário Especial');


CREATE TABLE IF NOT EXISTS `operacao_fiscal` (
  `id_operacao_fiscal` INT(11) auto_increment NOT NULL,
  `descricao` VARCHAR(150) NOT NULL,
  `descricao_cfop` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id_operacao_fiscal`))
ENGINE = InnoDB;

INSERT INTO operacao_fiscal (descricao,descricao_cfop) VALUES
	 ('Venda de mercadoria adquirida ou recebida de terceiros','Venda de mercadoria adquirida ou recebida de terceiros'),
	 ('Venda de produção do estabelecimento','Venda de produção do estabelecimento'),
	 ('Remessa em bonificação, doação ou brinde','Remessa em bonificação, doação ou brinde'),
	 ('Devolução de Venda','Devolução de Venda');


CREATE TABLE `tributacao` (
  `id_tributacao` int(11) NOT NULL AUTO_INCREMENT,
  `id_operacao_fiscal` int(11) NOT NULL,
  `id_grupo_tributario` int(11) NOT NULL,
  `icms` varchar(45) NOT NULL,
  `cfop` varchar(45) NOT NULL,
  `csosn` varchar(45) NOT NULL,
  `csticms` varchar(45) NOT NULL,
  `icmsst` varchar(45) NOT NULL,
  `id_estado` int(11) NOT NULL,
  PRIMARY KEY (`id_tributacao`),
  KEY `fk_tributacao_operacao_fiscal1_idx` (`id_operacao_fiscal`),
  KEY `fk_tributacao_grupo_tributario_idx` (`id_grupo_tributario`),
  KEY `fk_tributacao_estado` (`id_estado`),
  CONSTRAINT `fk_tributacao_estado` FOREIGN KEY (`id_estado`) REFERENCES `estados` (`codigo_uf`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tributacao_grupo_tributario` FOREIGN KEY (`id_grupo_tributario`) REFERENCES `grupo_tributario` (`id_grupo_tributario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tributacao_operacao_fiscal1` FOREIGN KEY (`id_operacao_fiscal`) REFERENCES `operacao_fiscal` (`id_operacao_fiscal`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `Configuracoes` ADD UNIQUE (`nome`); 

INSERT INTO Configuracoes (nome, valor, descricao) VALUES('versao_banco', '20230501', 'Versão banco') ON DUPLICATE KEY UPDATE    
nome='versao_banco', valor = '20230501';