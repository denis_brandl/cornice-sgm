CREATE TABLE `historicoPedido` (
  `id_historico_pedido` int(11) NOT NULL AUTO_INCREMENT,
  `id_pedido` int(11) NOT NULL,
  `id_situacao_de` int(11) NULL,
  `id_situacao_para` int(11) NULL,
  `observacao` text DEFAULT '',
  `id_usuario` INT(11) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id_historico_pedido`),
  CONSTRAINT `fk_historico_pedido`
    FOREIGN KEY (`id_pedido`)
    REFERENCES `Orcamento` (`Cd_Orcamento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE usuarios ADD login varchar(100) NULL;
ALTER TABLE usuarios ADD id_grupo_usuario INT(11) DEFAULT 0 NULL;
ALTER TABLE usuarios ADD situacao SMALLINT(1) DEFAULT 0 NULL;
ALTER TABLE usuarios ADD usuario_master SMALLINT(1) DEFAULT 0 NULL;
UPDATE usuarios SET login = NomeUsuario;
UPDATE usuarios SET usuario_master = 1, id_grupo_usuario = 1 WHERE login IN ('maestriasistema','fabiola');

CREATE TABLE `grupos_usuario` (
  `id_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) DEFAULT NULL,
  `situacao` smallint(1) DEFAULT 1,
  PRIMARY KEY (`id_grupo`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO grupos_usuario VALUES
(1,'Administrador',1),
(2,'Vendedor',1),
(3,'Gerente',1),
(4,'Produção',1);

ALTER TABLE Empresas ADD `status` smallint(1) DEFAULT 1 NULL;
ALTER TABLE Empresas ADD `padrao` smallint(1) DEFAULT 0 NULL;
ALTER TABLE Empresas ADD `filial` smallint(1) DEFAULT 0 NULL;
ALTER TABLE Empresas ADD `matriz` int(11) NULL;

UPDATE Empresas SET padrao = 1;


INSERT INTO Configuracoes (nome, valor, descricao) VALUES('versao_banco', '20230615', 'Versão banco') ON DUPLICATE KEY UPDATE    
nome='versao_banco', valor = '20230615';
