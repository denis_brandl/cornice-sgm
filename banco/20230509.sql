ALTER TABLE Orcamento ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `fila_documento_fiscal` (
  `id_fila` INT(11) NOT NULL AUTO_INCREMENT,
  `id_documento` VARCHAR(255) NOT NULL,
  `id_pedido` INT(11) NOT NULL,
  `payload` TEXT NULL,
  `retorno` TEXT NULL,
  `situacao` VARCHAR(45) NULL,
  PRIMARY KEY (`id_fila`),
  CONSTRAINT `fk_fila_documento_fiscal_pedido`
    FOREIGN KEY (`id_pedido`)
    REFERENCES `Orcamento` (`Cd_Orcamento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
  )
ENGINE = InnoDB;

INSERT INTO Configuracoes (nome, valor, descricao) VALUES('versao_banco', '20230501', 'Versão banco') ON DUPLICATE KEY UPDATE    
nome='versao_banco', valor = '20230509';