ALTER TABLE Produtos MODIFY COLUMN Desenho decimal(5,2) DEFAULT 0 NULL;

INSERT INTO Configuracoes (nome, valor, descricao) VALUES('versao_banco', '20230712', 'Versão banco') ON DUPLICATE KEY UPDATE nome='versao_banco', valor = '20230712';