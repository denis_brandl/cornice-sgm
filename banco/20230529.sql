DROP TABLE `Ipi`;
CREATE TABLE IF NOT EXISTS `ipi` (
  `id_ipi` INT(11) NOT NULL AUTO_INCREMENT,
  `ipi` VARCHAR(45) NOT NULL,
  `cstipi` VARCHAR(45) NOT NULL,
  `id_operacao_fiscal` INT(11) NOT NULL,
  PRIMARY KEY (`id_ipi`),
  INDEX `fk_ipi_operacao_fiscal1_idx` (`id_operacao_fiscal` ASC),
  CONSTRAINT `fk_ipi_operacao_fiscal1`
    FOREIGN KEY (`id_operacao_fiscal`)
    REFERENCES `operacao_fiscal` (`id_operacao_fiscal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `confins` (
  `id_confins` INT(11) NOT NULL AUTO_INCREMENT,
  `confins` VARCHAR(45) NOT NULL,
  `cstconfins` VARCHAR(45) NOT NULL,
  `id_operacao_fiscal` INT NOT NULL,
  PRIMARY KEY (`id_confins`),
  INDEX `fk_confins_operacao_fiscal1_idx` (`id_operacao_fiscal` ASC),
  CONSTRAINT `fk_confins_operacao_fiscal1`
    FOREIGN KEY (`id_operacao_fiscal`)
    REFERENCES `operacao_fiscal` (`id_operacao_fiscal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `pis` (
  `id_pis` INT(11) NOT NULL AUTO_INCREMENT,
  `pis` VARCHAR(45) NOT NULL,
  `cstpis` VARCHAR(45) NOT NULL,
  `id_operacao_fiscal` INT NOT NULL,
  PRIMARY KEY (`id_pis`),
  INDEX `fk_pis_operacao_fiscal1_idx` (`id_operacao_fiscal` ASC),
  CONSTRAINT `fk_pis_operacao_fiscal1`
    FOREIGN KEY (`id_operacao_fiscal`)
    REFERENCES `operacao_fiscal` (`id_operacao_fiscal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE grupo_tributario DROP FOREIGN KEY fk_grupo_tributario_empresa;
ALTER TABLE Empresas MODIFY COLUMN CodigoEmpresa int(11) auto_increment NOT NULL;;
ALTER TABLE Produtos MODIFY COLUMN codigo_ncm int(8) unsigned zerofill DEFAULT 00000000 NULL;
INSERT INTO Configuracoes (nome, valor, descricao) VALUES('versao_banco', '20230529', 'Versão banco') ON DUPLICATE KEY UPDATE nome='versao_banco', valor = '20230529';