ALTER TABLE fila_documento_fiscal ADD id_empresa INT(11) NULL;
ALTER TABLE fila_documento_fiscal ADD id_usuario_criacao int(11) NULL;
ALTER TABLE fila_documento_fiscal ADD id_usuario_edicao int(11) NULL;
ALTER TABLE fila_documento_fiscal ADD data_criacao TIMESTAMP NULL;
ALTER TABLE fila_documento_fiscal ADD data_atualizacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE COMPONENTES_ITEM_ORCAMENTO CHANGE COLUMN `VALOR_UNITARIO` `VALOR_UNITARIO` float(7,2) DEFAULT 0.00; 

INSERT INTO Configuracoes (nome, valor, descricao) VALUES('versao_banco', '20230705', 'Versão banco') ON DUPLICATE KEY UPDATE nome='versao_banco', valor = '20230705';