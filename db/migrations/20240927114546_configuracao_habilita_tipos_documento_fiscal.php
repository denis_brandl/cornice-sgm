<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ConfiguracaoHabilitaTiposDocumentoFiscal extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('Configuracoes');

        $singleRow = [
            [
                'nome'    => 'habilitar_emissao_nf',
                'valor'  => 'false',
                'descricao' => 'Habilitar emissão de nota fiscal de produto'
            ],
            [
                'nome'    => 'habilitar_emissao_nfs',
                'valor'  => 'false',
                'descricao' => 'Habilitar emissão de nota fiscal de serviço'
            ],
            [
                'nome'    => 'habilitar_emissao_nfc',
                'valor'  => 'false',
                'descricao' => 'Habilitar emissão de nota fiscal de consumidor'
            ]
        ];

        $table->insert($singleRow)->saveData();

        $sql = '
            UPDATE
                Configuracoes
            SET
                valor = (
                    SELECT
                        valor
                    FROM
                        Configuracoes
                    WHERE
                        nome = "habilita_tributacao"
                    )
            WHERE
                nome = "habilitar_emissao_nf"
        ';
        $this->execute($sql);

        $sql = '
            UPDATE
                Configuracoes
            SET
                valor = (
                    SELECT
                        valor
                    FROM
                        Configuracoes
                    WHERE
                        nome = "habilita_tributacao"
                    )
            WHERE
                nome = "habilitar_emissao_nfs"
        ';
        $this->execute($sql);
        
        $sql = '
            UPDATE
                Configuracoes
            SET
                valor = (
                    SELECT
                        valor
                    FROM
                        Configuracoes
                    WHERE
                        nome = "habilita_tributacao"
                    )
            WHERE
                nome = "habilitar_emissao_nfc"
        ';
        $this->execute($sql);        
    }
}
