<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateBloqueioConfiguracoesTable extends AbstractMigration
{
    public function change(): void
    {
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'sistema_bloqueado',
			'valor'  => 'false'
		];

		$table->insert($singleRow)->saveData();		
    }
}
