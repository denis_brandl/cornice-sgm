<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddPrecoCustoNoOrcamento extends AbstractMigration
{
    public function change(): void
    {
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'exibir_total_custo_item',
			'valor'  => 'false'
		];

		$table->insert($singleRow)->saveData();	



		$this->table('Item_Orcamento')
        ->addColumn('valor_custo', 'decimal', ['precision' => 7, 'scale' => 2])
        ->save();				
    }
}
