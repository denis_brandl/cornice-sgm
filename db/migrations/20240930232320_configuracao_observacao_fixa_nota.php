<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ConfiguracaoObservacaoFixaNota extends AbstractMigration
{
    public function change(): void
    {
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'observacao_fixa_nota',
			'valor'  => ''
		];

		$table->insert($singleRow)->saveData();	
    }
}
