<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddColumnCancelamentoFilaDocumentoFiscal extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('fila_documento_fiscal');

        $table->addColumn('devolucao','integer', [
           'default' => 0,
           'signed' => false,
           'limit' => 1,
           'comment' => '0 - Normal, 1 - Nota de devolução, 2 - Nota foi devolvida'
        ]);
       $table->save();
    }
}
