<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddConfiguracaoInformarValorDecimalParaMedidas extends AbstractMigration
{
    
    public function change(): void
    {

		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'permitir_informar_medidas_em_decimal',
			'valor'  => 'true'
		];

		$table->insert($singleRow)->saveData();	        

    }
}
