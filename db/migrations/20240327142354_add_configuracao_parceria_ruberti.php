<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddConfiguracaoParceriaRuberti extends AbstractMigration
{
   
    public function change(): void
    {
		
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'parceria_ruberti',
			'valor'  => 'false'
		];

		$table->insert($singleRow)->saveData();		

    }
}
