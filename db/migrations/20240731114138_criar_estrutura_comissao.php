<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarEstruturaComissao extends AbstractMigration
{
    public function change(): void
    {
        /**
         * Excluindo a tabela antiga (legado da base original)
         */
        $this->table('Vendedores')->drop()->save();

        $tabelaVendedor = $this->table('vendedor', ['id' => 'id_vendedor', 'primary_key' => ['id_vendedor']]);
        $tabelaVendedor->addColumn('nome_vendedor', 'string')
            ->addColumn('percentual_comissao', 'decimal', ['default' => 0, 'precision' => 7, 'scale' => 2])
            ->addColumn('id_usuario', 'integer', ['default' => 0])
            ->addColumn('afiliado', 'integer', ['default' => 0])
            ->addColumn('situacao', 'integer', ['limit' => 1, 'default' => 1])
            ->addColumn('criado_em', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('modificado_em', 'datetime', ['null' => true])
            ->addColumn('criado_por', 'integer', ['default' => 0, 'limit' => 11])
            ->addColumn('modificado_por', 'integer', ['default' => 0, 'limit' => 11])
            ->addIndex(['id_usuario'], ['unique' => true]);

        $tabelaVendedor->create();

        $table = $this->table('permissoes');    
        $rows = [
          [
            'entidade' => 'Vendedor',
            'descricao' => 'Vendedores',
            'permissao_complementar' => 0,
            'nome' => ''
          ]
        ];
        $table->insert($rows)->saveData();
    
        $sql = '
                SELECT
                    id_grupo
                FROM
                    grupos_usuario
            ';
    
        $rows = $this->fetchAll($sql);
    
        foreach ($rows as $row) {
    
          $sql = 'INSERT INTO grupos_permissoes
                          (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
                        SELECT
                            ' . $row['id_grupo'] . ',
                            id_permissao,
                            '.($row['id_grupo'] == 1 ? '1' : '0'). ' as cadastrar,
                            '.($row['id_grupo'] == 1 ? '1' : '0'). ' as editar,
                            '.($row['id_grupo'] == 1 ? '1' : '0'). ' as excluir,
                            '.($row['id_grupo'] == 1 ? '1' : '0'). ' as visualizar
                        FROM
                            permissoes
                        WHERE
                        entidade = "Vendedor"
                        ';
          $this->execute($sql);
        }        
    }
}
