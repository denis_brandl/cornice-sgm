<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarConfiguracaoComissao extends AbstractMigration
{
    public function change(): void
    {
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'relatorio_comissao_considerar_somente_pago',
			'valor'  => 'false'
		];

		$table->insert($singleRow)->saveData();	
    }
}
