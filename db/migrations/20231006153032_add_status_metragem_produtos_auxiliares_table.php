<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddStatusMetragemProdutosAuxiliaresTable extends AbstractMigration
{
    public function change(): void
    {
      $table = $this->table('Produtos_Auxiliares');
      $table->addColumn('status','integer', [
        'default' => 1,
        'limit' => 1
      ]);

      $table->addColumn('metragem','integer', [
        'default' => 1,
        'limit' => 1
      ]);

      $table->save();
    }
}
