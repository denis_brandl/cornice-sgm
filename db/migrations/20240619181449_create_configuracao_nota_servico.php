<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateConfiguracaoNotaServico extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('Empresas');

		$table->addColumn('numero_sequencia_nf','integer', [
			'default' => 0
		])->save();

		$table->addColumn('numero_sequencia_nfs','integer', [
			'default' => 0
		])->save();

		$table->addColumn('numero_sequencia_nfc','integer', [
			'default' => 0
		])->save();        

		$table->addColumn('serie_nf','string', ['limit' => 50])->save();

		$table->addColumn('serie_nfs','string', ['limit' => 50])->save();

		$table->addColumn('serie_nfc','string', ['limit' => 50])->save();        
    }
}
