<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddPermissaoModificarValoresPedido extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('permissoes');

        $rows = [
            [
                'entidade' => 'Pedidos',
                'descricao' => 'Permitir modificar valores após o pedido ser aprovado',
                'permissao_complementar' => 1,
                'nome' => 'MODIFICAR_VALORES_PEDIDO'
            ]
        ];
        $table->insert($rows)->saveData();

        $sql = '
                SELECT
                    id_grupo
                FROM
                    grupos_usuario
            ';

        $rows = $this->fetchAll($sql);

        foreach ($rows as $row) {

            $sql = 'INSERT INTO grupos_permissoes
                          (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar, conteudo)
                        SELECT
                            ' . $row['id_grupo'] . ',
                            id_permissao,
                            ' .($row['id_grupo'] == '1' || $row['id_grupo'] == '3' ? '1': '0') . ' as cadastrar,
                            ' .($row['id_grupo'] == '1' || $row['id_grupo'] == '3' ? '1': '0') . ' as editar,
                            ' .($row['id_grupo'] == '1' || $row['id_grupo'] == '3' ? '1': '0') . ' as excluir,
                            ' .($row['id_grupo'] == '1' || $row['id_grupo'] == '3' ? '1': '0') . ' as visualizar,
                            "" as conteudo
                        FROM
                            permissoes
                        WHERE
                            nome = "MODIFICAR_VALORES_PEDIDO"
                        ';
            $this->execute($sql);
        }
    }
}
