<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddColumnComponenteProdutoTable extends AbstractMigration
{
    public function change(): void
    {
		$table = $this->table('Produtos');
		$table->addColumn('componente','integer', [
			'default' => 0,
			'limit' => 1
		])->addColumn('definicao_preco','integer', [
			'default' => 1,
			'limit' => 1,
			'comment' => '1 - Preço Fixo, 2 - Percentual'
		])->addColumn('margem_venda','decimal', ['precision' => 7, 'scale' => 2])->save();

    }
}
