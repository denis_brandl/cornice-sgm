<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ConfiguracaoArredondarValorItem extends AbstractMigration
{

    public function change(): void
    {
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'arredondar_valor_item',
			'valor'  => 'false'
		];

		$table->insert($singleRow)->saveData();	
    }
}
