<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddPermissoesVendedores extends AbstractMigration
{
    
    public function change(): void
    {
        $table = $this->table('permissoes');    
        $rows = [
          [
            'entidade' => 'Pedidos',
            'descricao' => 'Visualizar pedidos de outros vendedores',
            'permissao_complementar' => 1,
            'nome' => 'VISUALIZAR_PEDIDOS_OUTROS_VENDEDORES'
          ]
        ];
        $table->insert($rows)->saveData();
    
        $sql = '
                SELECT
                    id_grupo
                FROM
                    grupos_usuario
            ';
    
        $rows = $this->fetchAll($sql);
    
        foreach ($rows as $row) {
    
          $sql = 'INSERT INTO grupos_permissoes
                          (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
                        SELECT
                            ' . $row['id_grupo'] . ',
                            id_permissao,
                            1 as cadastrar,
                            1 as editar,
                            1 as excluir,
                            1 as visualizar
                        FROM
                            permissoes
                        WHERE
                            nome = "VISUALIZAR_PEDIDOS_OUTROS_VENDEDORES"
                        ';
          $this->execute($sql);
        }        
    }
}
