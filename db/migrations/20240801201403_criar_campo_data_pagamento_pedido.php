<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarCampoDataPagamentoPedido extends AbstractMigration
{

    public function change(): void
    {
		$this->table('Orcamento')
		    ->addColumn('data_pagamento', 'date', ['null' => true])
            ->save();

        $this->table('assets')
        ->addColumn('data_pagamento_loja', 'date', ['null' => true])
        ->addColumn('situacao', 'integer', ['limit' => 1, 'default' => 0])
        ->save();

        $table = $this->table('permissoes');    
        $rows = [
          [
            'entidade' => 'Assets',
            'descricao' => 'Campo <strong>Conta</strong>',
            'permissao_complementar' => 1,
            'nome' => 'PERMITE_SALVAR_CAMPO_CONTA'
          ]
        ];
        $table->insert($rows)->saveData();
    
        $sql = '
                SELECT
                    id_grupo
                FROM
                    grupos_usuario
            ';
    
        $rows = $this->fetchAll($sql);
    
        foreach ($rows as $row) {
    
          $sql = 'INSERT INTO grupos_permissoes
                          (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
                        SELECT
                            ' . $row['id_grupo'] . ',
                            id_permissao,
                            '.($row['id_grupo'] == 1 ? '1' : '0'). ' as cadastrar,
                            '.($row['id_grupo'] == 1 ? '1' : '0'). ' as editar,
                            '.($row['id_grupo'] == 1 ? '1' : '0'). ' as excluir,
                            '.($row['id_grupo'] == 1 ? '1' : '0'). ' as visualizar
                        FROM
                            permissoes
                        WHERE
                            nome = "PERMITE_SALVAR_CAMPO_CONTA"
                        ';
          $this->execute($sql);
        }        
    }
}
