<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddColumnDescontinuadoAtProdutosTable extends AbstractMigration
{

    public function change(): void
    {
		
		$table = $this->table('Produtos');
		$table->addColumn('descontinuado','integer', [
			'default' => 0,
			'limit' => 1
		])->save();

		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'cor_produto_descontinuado',
			'valor'  => '#0772c2'
		];

		$table->insert($singleRow)->saveData();	

    }
}
