<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarNovaPermissaoAlterarDataVencimentoEPagamentoContaAReceber extends AbstractMigration
{

    public function change(): void
    {
        $table = $this->table('permissoes');
		$table->addColumn('nome','string', [
			'default' => '',
		])->addColumn('permissao_complementar','integer', [
			'default' => '0',
            'limit' => 1
		])->save();

        $rows = [
            [
                'entidade' => 'Assets',
                'descricao' => 'Campo Data de vencimento',
                'permissao_complementar' => 1,
                'nome' => 'PERMITE_SALVAR_DATA_DE_VENCIMENTO'
            ]
        ];
        $table->insert($rows)->saveData();

        $sql = '
            SELECT
                id_grupo
            FROM
                grupos_usuario
        ';

        $rows = $this->fetchAll($sql);

        foreach ($rows as $row) {

            $sql = 'INSERT INTO grupos_permissoes
                      (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
                    SELECT
                        '.$row['id_grupo'].',
                        id_permissao,
                        '.($row['id_grupo'] == 1 ? '1' : '0'). ' as cadastrar,
                        '.($row['id_grupo'] == 1 ? '1' : '0'). ' as editar,
                        '.($row['id_grupo'] == 1 ? '1' : '0'). ' as excluir,
                        '.($row['id_grupo'] == 1 ? '1' : '0'). ' as visualizar
                    FROM
                        permissoes
                    WHERE
                        nome = "PERMITE_SALVAR_DATA_DE_VENCIMENTO"
                    ';
            $this->execute($sql);
		}
    }
}
