<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarColunaIdEmpresaTabelaOrcamento extends AbstractMigration
{

    public function change(): void
    {
		$table = $this->table('Orcamento');
		$table->addColumn('id_empresa','integer', [
			'default' => 1
		])->save();

        $sql = '
            UPDATE
                Orcamento
            SET
                id_empresa = 
                    (SELECT CodigoEmpresa FROM Empresas WHERE padrao = 1 )
        ';
        $this->execute($sql);
    }
}
