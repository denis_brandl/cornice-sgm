<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateConfiguracaoUsaPerdaTotal extends AbstractMigration
{

    public function change(): void
    {
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'considera_soma_total_perdas',
			'valor'  => 'false'
		];

		$table->insert($singleRow)->saveData();	
    }
}
