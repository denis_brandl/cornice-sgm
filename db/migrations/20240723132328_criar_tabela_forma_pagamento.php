<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarTabelaFormaPagamento extends AbstractMigration
{

    public function change(): void
    {

        /**
         * Excluindo a tabela antiga (legado da base original)
         */
        $this->table('FormasPagamento')->drop()->save();

        $tabelaFormaPagamento = $this->table('forma_pagamento', ['id' => 'id_forma_pagamento', 'primary_key' => ['id_forma_pagamento']]);
        $tabelaFormaPagamento->addColumn('descricao', 'string')
            ->addColumn('parcelas', 'integer', ['default' => 1])
            ->addColumn('situacao', 'integer', ['limit' => 1, 'default' => 1])
            ->addColumn('criado_em', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('modificado_em', 'datetime', ['null' => true])
            ->addColumn('criado_por', 'integer', ['default' => 0, 'limit' => 11])
            ->addColumn('modificado_por', 'integer', ['default' => 0, 'limit' => 11]);

        $tabelaFormaPagamento->create();

        $rows = [
            [
                'descricao'  => 'Dinheiro'
            ],
            [
                'descricao'  => 'Cheque'
            ],
            [
                'descricao'  => 'Pix'
            ],
            [
                'descricao'  => 'Cartão de débito'
            ],
            [
                'descricao'  => 'Cartão de crédito'
            ],
            [
                'descricao'  => 'Crédito Loja'
            ]
        ];

        $tabelaFormaPagamento->insert($rows)->saveData();

        $table = $this->table('Orcamento');
        $table->addColumn('id_forma_pagamento_entrada', 'integer', [
            'default' => 1
        ])->addColumn('id_forma_pagamento_saldo', 'integer', [
            'default' => 1
        ])->save();

        $table->addColumn('numero_parcelas_entrada', 'integer', [
            'default' => 1
        ])->addColumn('numero_parcelas_saldo', 'integer', [
            'default' => 1
        ])->save();

        $rows = [
            ['entidade' => 'FormaPagamento', 'descricao' => 'Formas de pagamento'],
        ];

        $tabelaPermissoes = $this->table('permissoes');
        $tabelaPermissoes->insert($rows)->saveData();

        $sql = 'INSERT INTO grupos_permissoes
                    (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
                SELECT
                    1 as id_grupo,
                    id_permissao,
                    1 as cadastrar,
                    1 as editar,
                    1 as excluir,
                    1 as visualizar
                FROM
                    permissoes
                WHERE entidade = "FormaPagamento"';  
        $this->execute($sql);

        $sql = 'INSERT INTO grupos_permissoes
                    (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
                SELECT
                    2 as id_grupo,
                    id_permissao,
                    0 as cadastrar,
                    0 as editar,
                    0 as excluir,
                    0 as visualizar
                FROM
                    permissoes
                WHERE entidade = "FormaPagamento"';  
        $this->execute($sql);

        $sql = 'INSERT INTO grupos_permissoes
                    (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
                SELECT
                    4 as id_grupo,
                    id_permissao,
                    0 as cadastrar,
                    0 as editar,
                    0 as excluir,
                    0 as visualizar
                FROM
                    permissoes
                WHERE entidade = "FormaPagamento"';  
        $this->execute($sql);   
        
        $sql = 'INSERT INTO grupos_permissoes
                    (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
                SELECT
                    3 as id_grupo,
                    id_permissao,
                    1 as cadastrar,
                    1 as editar,
                    1 as excluir,
                    1 as visualizar
                FROM
                    permissoes
                WHERE entidade = "FormaPagamento"';  
        $this->execute($sql);        
    }
}
