<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ChangeTypeAlturaLargura extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('Item_Orcamento');
        $table->changeColumn('Md_Altura', 'decimal', ['precision' => 7, 'scale' => 2])
              ->update();
        $table->changeColumn('Md_Largura', 'decimal', ['precision' => 7, 'scale' => 2])
              ->update();              
    }
}
