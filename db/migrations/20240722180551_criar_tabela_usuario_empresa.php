<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarTabelaUsuarioEmpresa extends AbstractMigration
{

    public function change(): void
    {

        $tabelaUsuarioEmpresa = $this->table('usuario_empresa', ['id' => 'id_usuario_empresa', 'primary_key' => ['id_usuario_empresa']]);
        $tabelaUsuarioEmpresa->addColumn('id_usuario', 'integer')
             ->addColumn('id_empresa', 'integer');

        $tabelaUsuarioEmpresa->create();
    }
}
