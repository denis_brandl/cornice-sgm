<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class TabelaPermissoes extends AbstractMigration
{
    public function change(): void
    {

        $tabelaPermissoes = $this->table('permissoes', ['id' => 'id_permissao', 'primary_key' => ['id_permissao']]);
        $tabelaPermissoes->addColumn('entidade', 'string')
             ->addColumn('descricao', 'string');

        $tabelaPermissoes->create();

        $tabelaGruposPermissoes = $this->table('grupos_permissoes', ['id' => 'id_grupo_permissao', 'primary_key' => ['id_grupo_permissao']]);
        $tabelaGruposPermissoes->addColumn('id_grupo', 'integer');
        $tabelaGruposPermissoes->addColumn('id_permissao', 'integer');
        $tabelaGruposPermissoes->addColumn('cadastrar', 'integer', ['limit' => 1, 'default' => 0]);
        $tabelaGruposPermissoes->addColumn('editar', 'integer', ['limit' => 1, 'default' => 0] );
        $tabelaGruposPermissoes->addColumn('excluir', 'integer', ['limit' => 1, 'default' => 0] );
        $tabelaGruposPermissoes->addColumn('visualizar', 'integer', ['limit' => 1, 'default' => 0] );
        $tabelaGruposPermissoes->create();

        $rows = [
          ['entidade' => 'Cliente', 'descricao' => 'Clientes'],
          ['entidade' => 'ClienteGrupo', 'descricao' => 'Grupo de clientes'],
          ['entidade' => 'Configuracoes', 'descricao' => 'Configurações'],
          ['entidade' => 'Componente', 'descricao' => 'Componentes'],
          ['entidade' => 'ComposicaoPreco', 'descricao' => 'Composição de Preço'],
          ['entidade' => 'Empresa', 'descricao' => 'Empresa'],
          ['entidade' => 'Fornecedor', 'descricao' => 'Fornecedores'],
          ['entidade' => 'GrupoTributario', 'descricao' => 'Grupos Tributários'],
          ['entidade' => 'OperacaoFiscal', 'descricao' => 'Operação Fiscal'],
          ['entidade' => 'Tributacao', 'descricao' => 'Tributação'],
          ['entidade' => 'Complexidade', 'descricao' => 'Complexidades'],
          ['entidade' => 'Produto', 'descricao' => 'Produtos'],
          ['entidade' => 'ProdutoAuxiliar', 'descricao' => 'Objetos'],
          ['entidade' => 'Estado', 'descricao' => 'Estados'],
          ['entidade' => 'Producao', 'descricao' => 'Produção'],
          ['entidade' => 'Financeiro', 'descricao' => 'Financeiro > Saldo Inicial'],
          ['entidade' => 'Contas', 'descricao' => 'Financeiro > Contas'],
          ['entidade' => 'Category', 'descricao' => 'Financeiro > Categorias'],
          ['entidade' => 'Bills', 'descricao' => 'Contas a Pagar'],
          ['entidade' => 'Assets', 'descricao' => 'Contas a Receber'],
          ['entidade' => 'Vendedor', 'descricao' => 'Vendedores'],
          ['entidade' => 'Usuario', 'descricao' => 'Usuários'],
          ['entidade' => 'Permissoes', 'descricao' => 'Permissões'],
          ['entidade' => 'Pedidos', 'descricao' => 'Pedidos'],
          ['entidade' => 'DocumentosFiscais', 'descricao' => 'Documentos Fiscais'],
        ];

      $tabelaPermissoes->insert($rows)->saveData();

      $sql = 'INSERT INTO grupos_permissoes
                (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
              SELECT
                1 as id_grupo,
                id_permissao,
                1 as cadastrar,
                1 as editar,
                1 as excluir,
                1 as visualizar
              FROM
                permissoes';
      $this->execute($sql);

      $sql = 'INSERT INTO grupos_permissoes
                (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
              SELECT
                  3 as id_grupo,
                  id_permissao,
                  CASE 
                    WHEN entidade <> "Pedidos" THEN 0
                    ELSE 1
                  END as cadastrar,
                  CASE 
                    WHEN entidade <> "Pedidos" THEN 0
                    ELSE 1
                  END as editar,
                  CASE 
                    WHEN entidade <> "Pedidos" THEN 0
                    ELSE 1
                  END as excluir,
                  CASE 
                    WHEN entidade <> "Pedidos" THEN 0
                    ELSE 1
                  END as visualizar
              FROM
                permissoes';
      $this->execute($sql);
      
      $sql = 'INSERT INTO grupos_permissoes
                (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
              SELECT
                  2 as id_grupo,
                  id_permissao,
                  CASE 
                    WHEN entidade <> "Pedidos" THEN 0
                    ELSE 1
                  END as cadastrar,
                  CASE 
                    WHEN entidade <> "Pedidos" THEN 0
                    ELSE 1
                  END as editar,
                  CASE 
                    WHEN entidade <> "Pedidos" THEN 0
                    ELSE 1
                  END as excluir,
                  CASE 
                    WHEN entidade <> "Pedidos" THEN 0
                    ELSE 1
                  END as visualizar
              FROM
                permissoes';                
      $this->execute($sql);  
      
      $sql = 'INSERT INTO grupos_permissoes
                (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
              SELECT
                  4 as id_grupo,
                  id_permissao,
                  CASE 
                    WHEN entidade <> "Producao" THEN 0
                    ELSE 1
                  END as cadastrar,
                  CASE 
                    WHEN entidade <> "Producao" THEN 0
                    ELSE 1
                  END as editar,
                  CASE 
                    WHEN entidade <> "Producao" THEN 0
                    ELSE 1
                  END as excluir,
                  CASE 
                    WHEN entidade <> "Producao" THEN 0
                    ELSE 1
                  END as visualizar
              FROM
                permissoes';                
      $this->execute($sql);       
    }

    public function down()
    {
      $this->table('permissoes')->drop()->save();
      $this->table('grupos_permissoes')->drop()->save();
    }    
}
