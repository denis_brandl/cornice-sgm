<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddPermissaoVendedoresESituacao extends AbstractMigration
{
    public function change(): void
    {


        $this->table('grupos_permissoes')->addColumn('conteudo', 'string', [
            'default' => ''
        ])->save();

        $table = $this->table('permissoes');

        $rows = [
            [
                'entidade' => 'Pedidos',
                'descricao' => 'Situações que podem ser atribuídas',
                'permissao_complementar' => 1,
                'nome' => 'SITUACOES_PARA_SEREM_ATRIBUIDAS'
            ]
        ];
        $table->insert($rows)->saveData();

        $sql = '
                SELECT
                    id_grupo
                FROM
                    grupos_usuario
            ';

        $rows = $this->fetchAll($sql);

        foreach ($rows as $row) {

            $sql = 'INSERT INTO grupos_permissoes
                          (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar, conteudo)
                        SELECT
                            ' . $row['id_grupo'] . ',
                            id_permissao,
                            1 as cadastrar,
                            1 as editar,
                            1 as excluir,
                            1 as visualizar,
                            "1,2,3,4,5,6" as conteudo
                        FROM
                            permissoes
                        WHERE
                            nome = "SITUACOES_PARA_SEREM_ATRIBUIDAS"
                        ';
            $this->execute($sql);
        }

        $rows = [
            [
                'entidade' => 'Pedidos',
                'descricao' => 'Permitir retornar pedidos para situações anteriores',
                'permissao_complementar' => 1,
                'nome' => 'PERMITIR_RETORNAR_SITUACAO'
            ]
        ];
        $table->insert($rows)->saveData();

        $sql = '
                SELECT
                    id_grupo
                FROM
                    grupos_usuario
            ';

        $rows = $this->fetchAll($sql);

        foreach ($rows as $row) {

            $sql = 'INSERT INTO grupos_permissoes
                          (id_grupo, id_permissao, cadastrar, editar, excluir, visualizar)
                        SELECT
                            ' . $row['id_grupo'] . ',
                            id_permissao,
                            1 as cadastrar,
                            1 as editar,
                            1 as excluir,
                            1 as visualizar
                        FROM
                            permissoes
                        WHERE
                            nome = "PERMITIR_RETORNAR_SITUACAO"
                        ';
            $this->execute($sql);
        }
    }
}
