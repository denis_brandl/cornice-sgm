<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTipoDocumentoFiscal extends AbstractMigration
{
    public function change(): void
    {

        $tabelaTiposDocumentos = $this->table('tipos_documento', ['id' => 'id_tipo_documento', 'primary_key' => ['id_tipo_documento']]);
        $tabelaTiposDocumentos->addColumn('sigla', 'string', ['limit' => 10])
             ->addColumn('descricao', 'string', ['limit' => 50]);

        $tabelaTiposDocumentos->create();

        $rows = [
            [
              'sigla'    => 'NF',
              'descricao'  => 'Nota fiscal de produto'
            ],
            [
                'sigla'    => 'NFS',
                'descricao'  => 'Nota fiscal de serviço'
            ],
            [
                'sigla'    => 'NFC',
                'descricao'  => 'Nota Fiscal de consumidor'
            ]            
        ];

        
        $tabelaTiposDocumentos->insert($rows)->saveData();

        $table = $this->table('fila_documento_fiscal');

		$table->addColumn('id_tipo_documento','integer', [
			'default' => 1,
            'signed' => false,
            'null' => false
		])
        ->addForeignKey('id_tipo_documento', 'tipos_documento', 'id_tipo_documento')
        ->save();
        
    }
}
