<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateConfiguracaoArredondaValor extends AbstractMigration
{
    public function change(): void
    {
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'arredondar_medida_total_quadro',
			'valor'  => 'true'
		];

		$table->insert($singleRow)->saveData();	
    }
}
