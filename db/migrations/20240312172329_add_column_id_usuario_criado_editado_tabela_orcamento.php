<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddColumnIdUsuarioCriadoEditadoTabelaOrcamento extends AbstractMigration
{

    public function change(): void
    {
		
		$table = $this->table('Orcamento');
		$table->addColumn('criado_por','integer', [
			'default' => 0,
			'limit' => 11
		]);
		
		$table->addColumn('modificado_por','integer', [
			'default' => 0,
			'limit' => 11
		]);		
		
		$table->save();
		
		$this->execute('UPDATE Orcamento SET criado_por  = (SELECT CodigoUsuario FROM usuarios u WHERE u.usuario_master = 0 ORDER BY CodigoUsuario DESC LIMIT 1), modificado_por  = (SELECT CodigoUsuario FROM usuarios u WHERE u.usuario_master = 0 ORDER BY CodigoUsuario DESC LIMIT 1)');
    }
}
