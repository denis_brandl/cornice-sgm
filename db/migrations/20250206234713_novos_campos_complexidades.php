<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class NovosCamposComplexidades extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('Complexidades');

        $table->addColumn('aplicacao','string', [
           'default' => 'A',
           'limit' => 1,
           'comment' => 'A - Acréscimo, D - Desconto'
        ]);

        $table->addColumn('definicao_preco','integer', [
			'default' => 2,
			'limit' => 1,
			'comment' => '1 - Preço Fixo, 2 - Percentual'
		]);

        $table->addColumn('valor', 'decimal', ['default'=>0,'precision' => 7, 'scale' => 2]);

        $table->save();

        $tabelaPercentualCustoClienteGrupo = $this->table('ComplexidadeProduto');

        $tabelaPercentualCustoClienteGrupo->addColumn('aplicacao','string', [
           'default' => 'A',
           'limit' => 1,
           'comment' => 'A - Acréscimo, D - Desconto'
        ]);

        $tabelaPercentualCustoClienteGrupo->addColumn('definicao_preco','integer', [
			'default' => 2,
			'limit' => 1,
			'comment' => '1 - Preço Fixo, 2 - Percentual'
		]);

        $tabelaPercentualCustoClienteGrupo->renameColumn('percentual', 'valor')
              ->save();

        $tabelaPercentualCustoClienteGrupo->save(); 
    }
}
