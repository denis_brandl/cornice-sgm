<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class RemoveRestricaoComponenteItemOrcamentoTable extends AbstractMigration
{
    public function change(): void
    {
		
		$this->execute('ALTER TABLE COMPONENTES_ITEM_ORCAMENTO MODIFY COLUMN SEQUENCIA int(11) NOT NULL');
		$this->execute('ALTER TABLE COMPONENTES_ITEM_ORCAMENTO ADD id_componente_item_orcamento INT(11) auto_increment,  DROP INDEX `PRIMARY`, ADD PRIMARY KEY (id_componente_item_orcamento)');
    }
}
