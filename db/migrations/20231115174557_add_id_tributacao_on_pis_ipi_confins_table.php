<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddIdTributacaoOnPisIpiConfinsTable extends AbstractMigration
{
	public function change(): void
    {
	
	  $arrTables = ['pis', 'ipi', 'confins'];
	  
	  foreach ($arrTables as $current_table) {		  
		$table = $this->table($current_table);
		$table->truncate();
		$table->addColumn('id_tributacao','integer', [
			'default' => 0,
			'limit' => 11
		])
		->addForeignKey('id_tributacao', 'tributacao', 'id_tributacao', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
		->save();
	  }
  }
}
