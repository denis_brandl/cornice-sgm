<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarColunaSarrafoReforcoTabelaMolduraItemOrcamento extends AbstractMigration
{
    public function change(): void
    {

        $table = $this->table('Moldura_Item_Orcamento');
		$table->addColumn('sarrafo_reforco','string', [
			'default' => 'N',
            'limit' => 1
		])->save();
    }
}
