<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarCampoLinkPdfDocumentoFiscal extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('fila_documento_fiscal');

		$table->addColumn('link_pdf','string', [
			'default' => ''
		])
        ->save();

		$table->removeIndex(['id_pedido'])  
        ->dropForeignKey('id_pedido')
        ->save();        
    }
}
