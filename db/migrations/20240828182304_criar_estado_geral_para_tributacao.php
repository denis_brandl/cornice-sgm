<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarEstadoGeralParaTributacao extends AbstractMigration
{

    public function change(): void
    {
		$table = $this->table('estados');

		$singleRow = [
			'codigo_uf'    => '9999',
            'uf' => 'DE',
            'nome' => 'Demais Estados',
            'latitude' => 0,
            'longitude' => 0,
            'aliquota_icms' => 0,
            'aliquota_fcp' => 0
		];

		$table->insert($singleRow)->saveData();	

        $sql = '
            INSERT INTO tributacao (id_tributacao, id_operacao_fiscal, id_grupo_tributario, icms, cfop, csosn, csticms, icmsst, id_estado)
            SELECT 
                0 as id_tributacao,
                1 as id_operacao_fiscal,
                t.id_grupo_tributario,
                0 as icms,
                6102 as cfop,
                0 as csosn,
                0 as csticms,
                0 as icmsst,
                9999 as id_estado
            FROM tributacao t
        ';
        $this->execute($sql);
    }
}
