<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ChangeSizeFromQuantidadeMinimaCmAtProdutoTable extends AbstractMigration
{
    public function change(): void
    {
		
        $table = $this->table('Produtos');
        $table->changeColumn('QuantidadeMinimaCm', 'decimal', ['precision' => 7, 'scale' => 2])
              ->update();
    }
}
