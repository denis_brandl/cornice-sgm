<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarColunaMedidaFinalTabelaItemOrcamento extends AbstractMigration
{
    public function change(): void
    {
        $this->table('Item_Orcamento')
        ->addColumn('medida_final', 'string', ['null' => true])
        ->save();
    }
}
