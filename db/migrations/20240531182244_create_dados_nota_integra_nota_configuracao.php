<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateDadosNotaIntegraNotaConfiguracao extends AbstractMigration
{
    public function change(): void
    {
		
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'numero_sequencia_nf',
			'valor'  => ''
		];

		$table->insert($singleRow)->saveData();	
		
		$singleRow = [
			'nome'    => 'serie_nf',
			'valor'  => '1'
		];

		$table->insert($singleRow)->saveData();			
    }
}
