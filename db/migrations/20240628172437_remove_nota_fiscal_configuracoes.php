<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class RemoveNotaFiscalConfiguracoes extends AbstractMigration
{
    public function change(): void
    {
        $sqls = [
            'DELETE FROM `Configuracoes` WHERE `nome` = "token_integra_nota"',
            'DELETE FROM `Configuracoes` WHERE `nome` = "numero_sequencia_nf"',
            'DELETE FROM `Configuracoes` WHERE `nome` = "serie_nf"'
        ];

        foreach ($sqls as $sql) {
            $this->execute($sql);
        }
    }
}
