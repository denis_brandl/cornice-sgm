<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ConfiguracaoOcultaComponentesNotaImpressao extends AbstractMigration
{
    public function change(): void
    {
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'ocultar_componentes_impressao_pedido',
			'valor'  => 'false',
            'descricao' => 'Ocultar os componentes na impressão de pedidos'
		];

		$table->insert($singleRow)->saveData();	

		$singleRow = [
			'nome'    => 'ocultar_componentes_impressao_documento_fiscal',
			'valor'  => 'false',
            'descricao' => 'Ocultar os componentes na emissão de documentos fiscais'
		];

		$table->insert($singleRow)->saveData();	        
    }
}
