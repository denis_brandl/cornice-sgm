<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddConfiguracaoExibirMedidaFinal extends AbstractMigration
{
    public function change(): void
    {
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'exibir_medida_final_do_quadro',
			'valor'  => 'false'
		];

		$table->insert($singleRow)->saveData();	
    }
}
