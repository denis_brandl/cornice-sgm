<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddQuantidadeLadosCorteConfiguracao extends AbstractMigration
{

    public function change(): void
    {
        $table = $this->table('Configuracoes');

        $singleRow = [
            'nome'    => 'numero_cortes_considerar_perda',
            'valor'  => '4'
        ];

        $table->insert($singleRow)->saveData();
    }
}
