<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarCampoLinkXmlDocumentoFiscal extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('fila_documento_fiscal');

		$table->addColumn('link_xml','string', [
			'default' => ''
		])
        ->save();
    }
}
