<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarTabelaDescricaoProdutoAvulso extends AbstractMigration
{
    public function change(): void
    {
        $this->table('Item_Orcamento')
        ->addColumn('descricao_produto_servico', 'text', ['null' => true])
        ->save();
    }
}
