<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateMigrateDataBetweenComponentsAndProducts extends AbstractMigration
{
    public function change(): void
    {
		/**
		 * Migra os componentes cadastrados para a tabela `Produtos`
	    */ 
		$sql = '
			SELECT
				CONCAT("COMPONENTE-", IDCOMPONENTE) as NovoCodigo,
				DESCRICAO as descricao,
				(
					SELECT
						CodigoUnidadeMedida
					FROM
						UnidadesMedida
					WHERE
						DescricaoUnidadeMedida = "M2"
				) as UnidadeProduto,
				CUSTO as PrecoCusto,
				1 as componente,
				2 as definicao_preco,
				MARGEM_VENDA as margem_venda
			from
				COMPONENTES c ;		
		';
		$rows = $this->fetchAll($sql);
		foreach ($rows as $row) {
			$sql = sprintf('
				INSERT INTO Produtos
					(NovoCodigo, DescricaoProduto, UnidadeProduto, PrecoCusto, componente, definicao_preco, margem_venda)
					VALUES ("%s", "%s", "%s", "%s","%s","%s","%s")',
					$row['NovoCodigo'],
					$row['descricao'],
					$row['UnidadeProduto'],
					$row['PrecoCusto'],
					$row['componente'],
					$row['definicao_preco'],
					$row['margem_venda'],
				);
			$this->execute($sql);
		}
		
		$this->execute("UPDATE Produtos SET Quantidade = 99, QuantidadeMinima = 9, QuantidadeCm = 9999, QuantidadeMinimaCm = 99 WHERE componente = 1");
		
		/**
		 * Migra os componentes já utilizados para relacionar com o novo cadastro de produtos
		 */
		$sql = '
				SELECT
					CONCAT("UPDATE COMPONENTES_ITEM_ORCAMENTO SET CD_COMPONENTE = ", p.CodigoProduto," WHERE CD_COMPONENTE = ", CD_COMPONENTE,";") as cmd
				FROM
					COMPONENTES_ITEM_ORCAMENTO cio,
					Produtos p
				WHERE 
					p.NovoCodigo LIKE CONCAT("COMPONENTE-",cio.CD_COMPONENTE )
				GROUP BY
					cio.CD_COMPONENTE ;
			';
		$rows = $this->fetchAll($sql);
		foreach ($rows as $row) {
			$this->execute($row[0]);	
		}
    }
}
