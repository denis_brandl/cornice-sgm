<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTokenIntegraNotaPorEmpresa extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('Empresas');

		$table->addColumn('token_integra_nota','text', [
			'default' => ''
		])->save();

		$table->addColumn('ambiente_gerar_nota','integer', [
			'default' => 2,
			'limit' => 1,
            'comment' => '1 - Produção, 2 - Homologação'
		])->save();
    }
}
