<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AlterarCampoLinkXmlDocumentoFiscal extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('fila_documento_fiscal');

        $table->changeColumn('link_xml', 'text')
        ->update();
    }
}
