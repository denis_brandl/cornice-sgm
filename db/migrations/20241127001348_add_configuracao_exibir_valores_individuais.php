<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddConfiguracaoExibirValoresIndividuais extends AbstractMigration
{
    public function change(): void
    {
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'exibir_valores_individuais',
			'valor'  => 'false'
		];

		$table->insert($singleRow)->saveData();
    }
}
