<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AdicionarCamposFilaDocumentoFiscal extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {

        /**
         * ALTER TABLE quadrosrios.fila_documento_fiscal ADD numero_nota INT(6) DEFAULT 0 NULL;
         * ALTER TABLE quadrosrios.fila_documento_fiscal ADD motivo_cancelamento varchar(2550) NULL;
         * 
         */

		 $table = $this->table('fila_documento_fiscal');

		 $table->addColumn('numero_nota','integer', [
			'default' => 0,
            'signed' => false,
			'limit' => 6
		 ]);
		$table->addColumn('motivo_cancelamento','text');
        $table->save();
/*

         SELECT
	fila_documento_fiscal.numero_nota AS NFCRetornoId,
	fila_documento_fiscal.id_pedido AS PedidoId,
	'VENDA AO CONSUMIDOR' AS NaturezaOperacao,
	fila_documento_fiscal.id_tipo_documento,
	'1' AS FinalidadeEmissao,
	fila_documento_fiscal.retorno AS MensagemSEFAZ,
	fila_documento_fiscal.id_documento AS ChaveNfe,
	'' AS Numero,
	'' AS Serie,
	'' AS NumeroSerie,
	Orcamento.vl_liquido AS Valor,
	usuarios.login AS LoginEmissor,
	fila_documento_fiscal.data_atualizacao AS DataEmissao,
	fila_documento_fiscal.data_atualizacao AS DataHoraEmissao,
	Clientes.RazaoSocial AS ClienteNome,
	Clientes.EMail AS Email,
	'' AS QrcodeUrl,
	'' AS UrlConsultaNf,
	fila_documento_fiscal.link_pdf AS CaminhoDanfe,
	fila_documento_fiscal.link_xml AS CaminhoXmlNotaFiscal,
	'' AS CaminhoXmlCancelamento,
	Empresas.CGC AS CnpjEmitente,
	fila_documento_fiscal.id_pedido AS ref,
	fila_documento_fiscal.situacao AS Status,
	'100' AS StatusSefaz,
	'' AS Email1,
	'PRODUÇÃO' AS Ambiente,
	'' AS Consultas,
	'' AS DataCancelamento,
	'' AS Justificativa
FROM
	fila_documento_fiscal
INNER JOIN tipos_documento ON
	(fila_documento_fiscal.id_tipo_documento = tipos_documento.id_tipo_documento)
INNER JOIN Orcamento ON
	(Orcamento.Cd_Orcamento = fila_documento_fiscal.id_pedido)
INNER JOIN usuarios ON
	(fila_documento_fiscal.id_usuario_criacao = usuarios.CodigoUsuario)
INNER JOIN Clientes ON
	(Orcamento.Cd_Cliente = Clientes.CodigoCliente)
INNER JOIN Empresas ON
	(Orcamento.id_empresa = Empresas.CodigoEmpresa)	
WHERE
	fila_documento_fiscal.situacao IN ('Autorizado', 'Cancelado')
	AND fila_documento_fiscal.data_criacao BETWEEN '2024-08-01' AND '2024-08-31'
;
*/

/*

UPDATE fila_documento_fiscal SET numero_nota = 906 WHERE id_documento = '24240822196073000133550000000009061912426573';
UPDATE fila_documento_fiscal SET numero_nota = 181 WHERE id_documento = '24240822196073000133550000000001811426999629';
UPDATE fila_documento_fiscal SET numero_nota = 180 WHERE id_documento = '24240822196073000133550000000001801709696867';
UPDATE fila_documento_fiscal SET numero_nota = 905 WHERE id_documento = '24240822196073000133550000000009051350226618';
UPDATE fila_documento_fiscal SET numero_nota = 897 WHERE id_documento = '24240822196073000133550000000008971547292303';
UPDATE fila_documento_fiscal SET numero_nota = 896 WHERE id_documento = '24240822196073000133550000000008961142462605';
UPDATE fila_documento_fiscal SET numero_nota = 895 WHERE id_documento = '24240822196073000133550000000008951947480412';
UPDATE fila_documento_fiscal SET numero_nota = 178 WHERE id_documento = '24240822196073000133550000000001781632379694';
UPDATE fila_documento_fiscal SET numero_nota = 894 WHERE id_documento = '24240822196073000133550000000008941910647200';
UPDATE fila_documento_fiscal SET numero_nota = 893 WHERE id_documento = '24240822196073000133550000000008931202260815';

UPDATE fila_documento_fiscal SET numero_nota = 892 WHERE id_documento = '24240822196073000133550000000008921260345723';
UPDATE fila_documento_fiscal SET numero_nota = 886 WHERE id_documento = '24240822196073000133550000000008861894010116';
UPDATE fila_documento_fiscal SET numero_nota = 885 WHERE id_documento = '24240822196073000133550000000008851721895151';
UPDATE fila_documento_fiscal SET numero_nota = 884 WHERE id_documento = '24240822196073000133550000000008841968708800';


####

UPDATE fila_documento_fiscal SET numero_nota = 182 WHERE id_documento = '24240846423895000178550000000001821163511303'; 

*/


    }
}
