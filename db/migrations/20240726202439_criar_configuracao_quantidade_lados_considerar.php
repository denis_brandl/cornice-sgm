<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarConfiguracaoQuantidadeLadosConsiderar extends AbstractMigration
{

    public function change(): void
    {
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'lados_considerar_no_calculo_moldura',
			'valor'  => '2'
		];

		$table->insert($singleRow)->saveData();	
    }
}
