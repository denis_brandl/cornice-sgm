<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddColumnQtdItemComponenteItemOrcamentoTable extends AbstractMigration
{
    public function change(): void
    {
		
		$table = $this->table('COMPONENTES_ITEM_ORCAMENTO');
		$table->addColumn('quantidade','integer', [
			'default' => 0
		])->save();

    }
}
