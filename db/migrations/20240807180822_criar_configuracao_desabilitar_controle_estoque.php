<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CriarConfiguracaoDesabilitarControleEstoque extends AbstractMigration
{
    public function change(): void
    {
		$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'desabilitar_controle_estoque',
			'valor'  => 'false'
		];

		$table->insert($singleRow)->saveData();	
    }
}
