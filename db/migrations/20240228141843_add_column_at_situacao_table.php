<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddColumnAtSituacaoTable extends AbstractMigration
{
    public function change(): void
    {
		$table = $this->table('Situacao');
		$table->addColumn('notificacao','text')
		->save();
		
		$this->execute('UPDATE Situacao SET  notificacao = "Olá, tudo bem? Aqui é da [NOME_EMPRESA], gostaria de avisar que o seu pedido [NUMERO_ORCAMENTO] começou a ser produzido ;-)" WHERE idSituacao = 3 ');
		$this->execute('UPDATE Situacao SET  notificacao = "Olá, tudo bem? Aqui é da [NOME_EMPRESA], gostaria de avisar que o seu pedido [NUMERO_ORCAMENTO] está pronto ;-)" WHERE idSituacao = 4 ');
		$this->execute('UPDATE Situacao SET  notificacao = "Olá, tudo bem? Aqui é da [NOME_EMPRESA], gostaria só comunicar que o pedido [NUMERO_ORCAMENTO] foi entregue ;-)" WHERE idSituacao = 5 ');
    }
}
