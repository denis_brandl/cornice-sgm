<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTokenIntegraNotaConfiguracao extends AbstractMigration
{
    public function change(): void
    {
$table = $this->table('Configuracoes');

		$singleRow = [
			'nome'    => 'token_integra_nota',
			'valor'  => ''
		];

		$table->insert($singleRow)->saveData();		
    }
}
