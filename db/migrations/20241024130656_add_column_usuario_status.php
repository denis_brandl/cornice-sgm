<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddColumnUsuarioStatus extends AbstractMigration
{
    public function change(): void
    {
        $this->execute('UPDATE usuarios SET  situacao = 1');
    }
}
