<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddDefaultWhatsappMessage extends AbstractMigration
{
 /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('Configuracoes');
        
        $table->changeColumn('valor', 'text')
              ->save();

        $singleRow = [
            'nome'    => 'layout_mensagem_whatsapp',
            'valor'  => 'Segue [LABEL_ORCAMENTO_PEDIDO] número *[NUMERO_ORCAMENTO]*
            Cliente: [NOME_CLIENTE]
            Data do [LABEL_ORCAMENTO_PEDIDO]: [DATA_ORCAMENTO]
            *Itens do [LABEL_ORCAMENTO_PEDIDO]*
            [ITENS_ORCAMENTO]
  
            *Total do [LABEL_ORCAMENTO_PEDIDO]:* R$ [VALOR_BRUTO_PEDIDO]
            [VALOR_DESCONTO]    
            Subtotal: R$ [SUBTOTAL]
          Entrada : R$ [VALOR_ENTRADA]
          Saldo: R$ [VALOR_SALDO]
          Observação: [OBSERVACAO_CLIENTE]'
        ];

        $table->insert($singleRow)->saveData();
    }
}
