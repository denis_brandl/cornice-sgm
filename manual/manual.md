![](https://maestriasistema.com.br/wp-content/uploads/2023/01/cropped-Logo_Maestria-2.png)

Abaixo você encontra a documentação de como utilizar o sistema, e em caso de dúvidas, entre em contato conosco!

## Módulo de Administração

Se você está começando a utilizar o sistema agora, é recomendável que os cadastros iniciais sejam feitos na seguinte ordem:

- [Módulo de Administração](#módulo-de-administração)
  - [Fornecedores](#fornecedores)
  - [Complexidades](#complexidades)
  - [Grupos de cliente](#grupos-de-cliente)
  - [Produtos](#produtos)
    - [Aba Geral](#aba-geral)
      - [Unidade de Medida](#unidade-de-medida)
      - [Medida da vara](#medida-da-vara)
      - [Fator de perda](#fator-de-perda)
      - [Preço de venda](#preço-de-venda)
    - [Aba Complexidades](#aba-complexidades)
    - [Aba Grupo de Cliente](#aba-grupo-de-cliente)
    - [Aba Histórico de compras](#aba-histórico-de-compras)
  - [Componentes](#componentes)
  - [Objeto](#objeto)
  - [Clientes](#clientes)
  - [Composição de Preços](#composição-de-preços)
  - [Configurações](#configurações)
  - [Operações Fiscais](#operações-fiscais)

\pagebreak

### Fornecedores

No menu **Administração**, vá em **Fornecedores**.

Clique no botão **Cadastrar Novo** para cadastrar um novo fornecedor, ou clique em **Editar** na linha correspondente ao fornecedor que deseja fazer a edição do cadastro.

No formulário de cadastro que será exibido, faça a inserção dos dados referente ao fornecedor.

Os campos são auto explicativos, mas qualquer dúvida você pode entrar em contato conosco!

![Imagem do cadastro de fornecedores no Maestria Sistema](https://maestriasistema.com.br/wp-content/uploads/2023/03/maestria-sistema-para-molduraria-manual-fornecedor-1024x460.png)

\pagebreak

### Complexidades

Quando você vai realizar algum serviço, seja uma moldura em uma peça com um grau de dificuldade ou espelho arredondado, você pode ter um custo a mais devido a complexidade desse serviço e cobrar isso no serviço final.

O cadastro de complexidade serve para você ter esse grau de dificuldade pré definidas e que depois serão vinculados aos produtos e por fim, no momento de tirar um pedido, você poderá fazer essa relação.

No menu **Administração**, vá em **Complexidades**

Clique no botão **Cadastrar Novo** para cadastrar uma nova complexidade, ou clique em **Editar** na linha correspondente a complexidade que deseja fazer a edição do cadastro

![Imagem do cadastro de complexidades no Maestria Sistema](https://maestriasistema.com.br/wp-content/uploads/2023/03/maestria-sistema-para-molduraria-manual-cadastro-complexidades.png)

No formulário de cadastro que será exibido, faça a inserção dos dados referente a complexidade.  
Os campos são auto explicativos, mas qualquer dúvida você pode entrar em contato conosco!

\pagebreak

### Grupos de cliente

Você pode especificar um percentual de cobrança diferenciada por grupos de cliente, como terceiros, lojistas, cliente pessoa física, etc.

Quando o cliente for cadastrado, você pode especificar em qual grupo ele pertence. E na criação do orçamento, o percentual será aplicado automaticamente.

No menu **Administração**, vá em **Grupos de cliente**

Clique no botão **Cadastrar Novo** para cadastrar uma novo grupo de cliente, ou clique em **Editar** na linha correspondente ao grupo de cliente que deseja fazer a edição do cadastro

![Imagem do cadastro de grupos de cliente no Maestria Sistema](https://maestriasistema.com.br/wp-content/uploads/2023/03/maestria-sistema-para-molduraria-manual-cadastro-grupo-cliente.png)

No formulário de cadastro que será exibido, faça a inserção dos dados referente ao grupo de cliente.

Os campos são auto explicativos, mas qualquer dúvida você pode entrar em contato conosco!

\pagebreak

### Produtos

O cadastro de produtos é onde você irá cadastrar as molduras e outros produtos que serão calculados de acordo com a metragem do serviço. Além das molduras, você pode vincular espelhos, vidros, etc.

No menu **Administração**, vá em **Produtos**

Clique no botão **Cadastrar Novo** para cadastrar um novo material, ou clique em **Editar** na linha correspondente ao produto que deseja fazer a edição do cadastro

O formulário de cadastro de produtos possui 4 abas:

#### Aba Geral

Dados gerais do produto! Atente-se a alguns campos específicos:

##### Unidade de Medida

Por padrão são oferecidos duas unidades de medida: metro quadrado e varas.
Quando a unidade de medida for **Vara**, estamos se referindo as molduras.

##### Medida da vara

Esse campo é habilitado quando a **unidade de medida** for **Vara** e que será determinante no cálculo do estoque a ser utilizado no orçamento e no preço final do serviço.

##### Fator de perda

Esse campo é habilitado quando a **unidade de medida** for **Vara** e se refere a quanto centímetros são perdidos durante o corte da moldura.

##### Preço de venda

É possível cadastrar um valor fixo para a venda do produto por metro quadrado ou metragem linear (de acordo com a unidade de medida selecionada). Esse valor também é utilizado quando não é definido um percentual de cobrança por grupo de cliente.

![Imagem do cadastro de clientes, destacando a aba Geral no Maestria Sistema](https://maestriasistema.com.br/wp-content/uploads/2023/03/maestria-sistema-para-molduraria-manual-cadastro-produtos-aba-geral-1024x318.png)

#### Aba Complexidades

Aqui são definidos o percentual a ser acrescentado ao **preço de custo** de acordo com a complexidade do serviço a ser realizado.
Qualquer dúvida você pode entrar em contato conosco!

![Imagem do cadastro de clientes, destacando a aba Complexidades no Maestria Sistema](https://maestriasistema.com.br/wp-content/uploads/2023/03/maestria-sistema-para-molduraria-manual-cadastro-produtos-aba-complexidades.png)

#### Aba Grupo de Cliente

Aqui são definidos o percentual a ser acrescentado ao **preço de custo** (ou o preço cadastrado no campo **Preço de Venda)** de acordo com o grupo no qual o cliente pertence.
Qualquer dúvida você pode entrar em contato conosco!

![Imagem do cadastro de clientes, destacando a aba Grupo de Cliente no Maestria Sistema](https://maestriasistema.com.br/wp-content/uploads/2023/03/maestria-sistema-para-molduraria-manual-cadastro-produtos-aba-grupo-cliente.png)

#### Aba Histórico de compras

É possível manter um histórico das compras do produto.
Registre a data de compra, o valor pago e o fornecedor para seu controle.
Qualquer dúvida você pode entrar em contato conosco!

![Imagem do cadastro de clientes, destacando a aba Histórico de compras no Maestria Sistema](https://maestriasistema.com.br/wp-content/uploads/2023/03/maestria-sistema-para-molduraria-manual-cadastro-produtos-aba-historico-compras-1024x328.png)

\pagebreak

### Componentes

Os componentes são materiais utilizados junto ao produto para desenvolvimento do serviço. Alguns exemplos são Vidro, Espelho (aplicado a um produto moldurado por exemplo), Fundos de madeira, etc.

No menu **Administração**, vá em **Materiais**

Clique no botão **Cadastrar Novo** para cadastrar um novo material, ou clique em **Editar** na linha correspondente ao material que deseja fazer a edição do cadastro

No formulário de cadastro que será exibido, faça a inserção dos dados referente ao componente.
Os campos são auto explicativos, mas qualquer dúvida você pode entrar em contato conosco!

![Imagem do cadastro de Componentes no Maestria Sistema](https://maestriasistema.com.br/wp-content/uploads/2023/03/maestria-sistema-para-molduraria-manual-cadastro-componentes.png)

\pagebreak

### Objeto

È o **objeto** em si que será aplicado a moldura ou que será entregue ao cliente como um espelho ou prateleira de vido.

No menu **Administração**, vá em **Objeto**

Clique no botão **Cadastrar Novo** para cadastrar um novo objeto, ou clique em **Editar** na linha correspondente ao objeto que deseja fazer a edição do cadastro

No formulário de cadastro que será exibido, faça a inserção dos dados referente ao objeto.

Os campos são auto explicativos, mas qualquer dúvida você pode entrar em contato conosco!

![Imagem do cadastro de Objetos no Maestria Sistema](https://maestriasistema.com.br/wp-content/uploads/2023/03/maestria-sistema-para-molduraria-manual-cadastro-servico.png)

\pagebreak

### Clientes

Existem duas opções de cadastro: **Simples** e **Completo**.

Neste momento será apresentado o cadastro de forma **Completo**. A maneira de cadastro **Simples** é feito direto no cadastro de novo pedido.

No menu **Administração**, vá em **Clientes**

Clique no botão **Cadastrar Novo** para cadastrar um novo cliente, ou clique em **Editar** na linha correspondente ao cliente que deseja fazer a edição do cadastro.

No formulário de cadastro que será exibido, faça a inserção dos dados referente ao cliente:

![Imagem do cadastro de Clientes no Maestria Sistema](https://maestriasistema.com.br/wp-content/uploads/2023/03/maestria-sistema-para-molduraria-manual-cadastro-clientes.png)

Os campos são auto explicativos, mas qualquer dúvida você pode entrar em contato conosco!

\pagebreak

### Composição de Preços

Na tela de composição de preço é possível configurar os seguintes dados:

- Margem Bruta de Lucro: A margem de lucro a ser configurada para os produtos que não tiverem sido definidos individualmente
- Perda Média no Corte da Moldura: Perda média no corte para todas as molduras e que será considerado no cálculo em orçamento de produtos em que a unidade de medida seja **Vara**

\pagebreak

### Configurações

Tela auto explicativa, para configurar os dados da sua empresa e informações para constar nos pedidos.

### Operações Fiscais

Ordem:

1. Definir operação fiscal
2. Tributação (baseado na Operação Fiscal)
3. Grupo Tributário (vinculado ao Produto e será utilizado na venda)
